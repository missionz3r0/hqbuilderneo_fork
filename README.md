# README #

This readme is for getting HQ roster builder running

### Purpose of the project ###

This project is web-based roster builder for games by Games Workshop. We strive to keep it up-to-date to the best of our abilities.

Current version of the core is 2.

### Setting up ###

There are following steps in setting things up.

1. Install Python 2.x, virtualenv and pip

2. Create virtual environment and activate it

3. Install rest of dependencies with pip: *pip install -r requirements.txt*

4. Setup database scheme with alembic: *alembic -c db/alembic/alembic.ini -x mode=debug upgrade head*

5. Start server in debug mode: *python hq.py --debug*

### Author ###

Author of the project is Denis Romanov

Other contributors:

- Ivan Truskov
- Andrey Kiselev
- Ivan Berezhnoy