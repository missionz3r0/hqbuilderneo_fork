from sys import argv
from json import dumps

from flask import Flask, Markup
from flask.ext.assets import Environment, Bundle
from markdown import markdown

from _depricated import blog


app = Flask(__name__)
app.config.from_pyfile(argv[1])

import config
config.config = app.config

if app.config['DEBUG']:
    from werkzeug.wsgi import SharedDataMiddleware
    import os
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
        '/static/': os.path.join(os.path.dirname(__file__), '../static')
    })

assets = Environment(app)
js = Bundle(
    "extern/sprintf-0.6.js",
    "tools.js",
    "list.js",
    "auth.js",
    "roster.js",
    filters='jsmin',
    output='gen/_packed.js')
assets.register('js_all', js)

#
#import auth
#app.register_blueprint(auth.module)
#auth.login_manager.init_app(app)
#auth.mail.init_app(app)
#
#app.register_blueprint(blog.module)


#@app.template_filter('tojson|safe')
#def tojson|safe_filter(data):
#    try:
#        return Markup(dumps(data))
#    except TypeError as e:
#        print e
#        return Markup(dumps(None))


@app.template_filter('rate')
def rate_filter(data):
    if data == 0:
        return '0'
    return '%+d' % data


import _depricated.blog.locale as locale
@app.template_filter('local_date')
def local_date(data):
    return locale.local_date(data)


@app.template_filter('markdown')
def markdown_filter(data):
    return Markup(markdown(data))


if __name__ == '__main__':
    app.run()
