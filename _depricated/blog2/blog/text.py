# -*- coding: utf-8 -*-
import re
from jinja2 import Markup
from markdown import markdown
from markdown.inlinepatterns import LINK_RE, IMAGE_LINK_RE, AUTOLINK_RE

__author__ = 'dante'


read_more_code = '[read more]'


def get_read_more_head(text):
    pos = text.find(read_more_code)
    if pos == -1:
        return text, False
    return text[0:pos], True


def process_read_more_marker(text):
    return text.replace(read_more_code, '\n\n')


link_re = re.compile(LINK_RE)
image_re = re.compile(IMAGE_LINK_RE)
short_link_re = re.compile(AUTOLINK_RE)
i_frame_re = re.compile(r'(?:<iframe[^>]*)(?:(?:/>)|(?:>.*?</iframe>))')
url_re = re.compile(r'(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+'
                    r'|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,'
                    r'<>?«»“”‘’]))')
image_types = ['png', 'jpg', 'jpeg', 'gif']


def auto_mark_links(text, mark_links=True, mark_images=True):
    markers = []

    def collect(re):
        for i in re.finditer(text):
            markers.append([i.start(), i.end()])
    collect(link_re)
    collect(image_re)
    collect(short_link_re)
    collect(i_frame_re)

    def is_marked(start, end):
        for i in markers:
            if i[0] < start < i[1] or i[0] < end < i[1]:
                return True
        return False
    new_text = []
    start = 0
    for i in url_re.finditer(text):
        if not is_marked(i.start(), i.end()):
            new_text.append(text[start:i.start()])
            if mark_images and i.group()[i.group().rfind('.') + 1:] in image_types:
                new_text.append('![](%s)' % i.group())
            elif mark_links:
                new_text.append('<%s>' % i.group())
            else:
                new_text.append(i.group())
            start = i.end()
    new_text.append(text[start:])
    return ''.join(new_text)


def normalize_line_breaks(text):
    new_text = []
    for line in text.splitlines(False):
        if line and not line.isspace():
            if line.startswith('---') or line.startswith('==='):
                new_text[-1] = new_text[-1] + '\r\n' + line
            else:
                new_text.append(line)
    return '\r\n\r\n'.join(new_text)


def render_text(text):
    return Markup(markdown(auto_mark_links(normalize_line_breaks(text))))