from bson import ObjectId
from flask import url_for

from datetime import datetime
from auth.user import find_user
from .blog.text import render_text, process_read_more_marker, get_read_more_head


__author__ = 'dante'


class Comment(object):
    def __init__(self, text, post_id, author_id=None, author=None, time=None, parent_id=None, comment_id=None,
                 deleted=False):
        self.comment_id = comment_id  # ObjectID, can be None
        self.parent_id = parent_id  # ObjectID, can be None
        self.post_id = post_id  # ObjectID, can't be None
        self.text = text
        self.author = None
        if author:
            self.author = author
        elif author_id:
            self.author = find_user(user_id=author_id)
        self.time = time if time is not None else datetime.utcnow()
        self.deleted = deleted

    def dump(self):
        return dict(
            comment_id=str(self.comment_id),
            text=render_text(process_read_more_marker(self.text)) if not self.deleted else None,
            time=self.time.strftime("%Y-%m-%dT%H:%M:%S Z"),
            author=self.author.name,
            author_id=str(self.author.id),
            author_url=url_for('blog.main', user=str(self.author.id)),
            parent=str(self.parent_id),
            deleted=self.deleted
        )

    def save(self):
        comment_data = dict(
            text=self.text,
            time=self.time,
            author_id=ObjectId(self.author.id),
            post_id=self.post_id,
            parent=self.parent_id,
            deleted=self.deleted
        )
        if self.comment_id:
            comments.update({'_id': self.comment_id}, comment_data, upsert=True)
        else:
            self.comment_id = comments.insert(comment_data)


class Post(object):
    def __init__(self, text, post_id=None, author_id=None, author=None, time=None, tags=None, title=None,
                 deleted=False):
        self.post_id = post_id  # ObjectID
        self.text = text
        self.author = None
        if author:
            self.author = author
        elif author_id:
            self.author = find_user(user_id=author_id)
        self.time = time if time is not None else datetime.utcnow()
        self.title = title if title else None  # ''->None for title
        self.tags = tags if tags else []
        self.deleted = deleted

    def save(self):
        post_data = dict(
            title=self.title,
            text=self.text,
            time=self.time,
            tags=self.tags,
            author_id=ObjectId(self.author.id),
            deleted=self.deleted
        )
        if self.post_id:
            posts.update({'_id': self.post_id}, post_data, upsert=True)
        else:
            self.post_id = posts.insert(post_data)

    def dump(self):
        head, read_more = get_read_more_head(self.text)
        comments_count = comments.find({'post_id': self.post_id}).count() if self.post_id else 0
        return dict(
            post_id=str(self.post_id),
            post_url=url_for('blog.main', post=self.post_id) if self.post_id else '#',
            title=self.title,
            text=render_text(process_read_more_marker(self.text)),
            head=render_text(head),
            read_more=read_more,
            time=self.time.strftime("%Y-%m-%dT%H:%M:%S Z"),
            tags=self.tags,
            tag_url=url_for('blog.main', tag=''),
            author=self.author.name,
            author_id=str(self.author.id),
            author_url=url_for('blog.main', user=str(self.author.id)),
            comments_count=comments_count
        )


def get_comment_from_record(rec):
    return Comment(
        comment_id=rec['_id'],
        text=rec['text'],
        time=rec['time'],
        author_id=rec['author_id'],
        parent_id=rec.get('parent', None),
        post_id=rec['post_id'],
        deleted=rec.get('deleted', False)
    )


def get_post_from_record(rec):
    return Post(
        post_id=rec['_id'],
        title=rec['title'],
        text=rec['text'],
        time=rec['time'],
        tags=rec['tags'],
        author_id=rec['author_id'],
        deleted=rec.get('deleted', False)
    )
