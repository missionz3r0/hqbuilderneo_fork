# -*- coding: utf-8 -*-

post_per_page = 15
most_rated_limit = 10
most_commented_limit = 10
last_in_blogs_limit = 10
bookmarks_limit = 10
user_last_post_limit = 10
user_last_commented_limit = 10
users_per_page_limit = 100

max_avatar_size = 512 #kilobytes

read_more_code = '[read more]'

user_vote_base = 3
user_vote_step = 50

