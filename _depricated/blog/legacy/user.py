# -*- coding: utf-8 -*-

__author__ = 'dante'

from django.contrib import auth
from waaagh.multiblog.models import User, Vote
from datetime import date
from _depricated.blog.legacy.messages import restore_password_msg, restore_password_subj
from waaagh.multiblog.tools.mail import send_email
from waaagh.multiblog.tools.misc import get_random_key

def auth_user(request, data):
    auth.login(request, auth.authenticate(username=data['name'], password=data['password']))

def create_user(request, data):
    user = User.objects.create_user(username=data['name'], email=data['e_mail'], password=data['password'])
    if data['country'] != '':
        user.get_profile().country = data['country']
    if data['city'] != '':
        user.get_profile().city = data['city']
    if data['birth_year'] != '' and data['birth_month'] != '' and data['birth_day'] != '':
        user.get_profile().birth_date = date(year=int(data['birth_year']),
            month=int(data['birth_month']), day=int(data['birth_day']))
    user.save()
    user.get_profile().save()
    auth_user(request, data)

def restore_password(data):
    from django.core.mail import EmailMessage
    user = User.objects.get(email=data['e_mail'])
    dg = get_random_key()
    user.set_password(dg)
    user.save()
    send_email(EmailMessage(restore_password_subj,
        restore_password_msg % {'name' : user.username, 'password': dg}, to=[data['e_mail']]))


