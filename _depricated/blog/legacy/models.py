from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

# ***
class UserProfile(models.Model):
    user = models.ForeignKey(User, unique=True)

    country = models.CharField(null=True, max_length=120)
    city = models.CharField(null=True, max_length=120)
    birth_date = models.DateField(null=True)
    about = models.TextField(default='')

    rate = models.IntegerField(default=0)
    blog_title = models.CharField(null=True, max_length=120)
    blog_description = models.CharField(null=True, max_length=500)

    avatar = models.CharField(null=True, default=None, max_length=64)

    new_messages_notify = models.BooleanField(default=False)
    new_bookmarked_comments_notify = models.BooleanField(default=False)
    new_my_post_comments_notify = models.BooleanField(default=False)

    always_keep_bookmarks = models.BooleanField(default=False)
    keep_bookmarks = models.IntegerField(default=3)

    filter_all = 1
    filter_include = 2
    filter_exclude = 3

    filter_users = models.IntegerField(default=filter_all)
    filter_tags = models.IntegerField(default=filter_all)

# ***
class Tag(models.Model):
    name = models.CharField(max_length=120, default='') #TODO dummy
    title = models.CharField(max_length=120)
    description = models.CharField(max_length=500, default='') #TODO dummy
    parent = models.ForeignKey('self', null=True) #TODO dummy
    post_count = models.IntegerField(default=0)

    def __unicode__(self):
        return self.title

# ***
class UserTag(models.Model):
    title = models.CharField(max_length=120)
    post_count = models.IntegerField(default=0)
    user = models.ForeignKey(User)

# ***
class FilterUser(models.Model):
    stream_owner = models.ForeignKey(User, related_name='stream_owner')
    filtered_user = models.ForeignKey(User, related_name='filtered_user')

# ***
class FilterTag(models.Model):
    stream_owner = models.ForeignKey(User)
    filtered_tag = models.ForeignKey(Tag)

# ***
class Post(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=120)
    text = models.TextField()
    time = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField(default=0)
    comments = models.IntegerField(default=0)
    rubric = models.CharField(max_length=16, default='') #TODO dummy
    author = models.ForeignKey(User)
    tags = models.ManyToManyField(Tag, null=True)

    def __unicode__(self):
        return self.title


# ***
class Bookmark(models.Model):
    hit_time = models.DateTimeField(auto_now=True)
    post = models.ForeignKey(Post)
    user = models.ForeignKey(User)
    hit_comment_count = models.IntegerField()
    pinned = models.BooleanField(default=False)

    def __unicode__(self):
        return self.post.title


# ***
class PostHit(models.Model):
    hit_time = models.DateTimeField(auto_now=True)
    post = models.ForeignKey(Post)
    user = models.ForeignKey(User)
    hit_comment_count = models.IntegerField()


# ***
class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.TextField()
    time = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField(default=0)
    author = models.ForeignKey(User)
    post = models.ForeignKey(Post)
    parent = models.ForeignKey('self', null=True)
    level = models.IntegerField(default=0)

    def __unicode__(self):
        return self.text

# ***
class Vote(models.Model):
    voting_user = models.ForeignKey(User, related_name='voting_users') # This is author of the vote
    target_user = models.ForeignKey(User, related_name='target_users') # This is target of the vote
    post = models.ForeignKey(Post, null=True)
    comment = models.ForeignKey(Comment, null=True)
    value = models.IntegerField()
    time = models.DateTimeField(auto_now_add=True)
    up = 1
    down = -1


class PrivateMessage(models.Model):
    status_normal = 0
    status_trash = 1
    status_deleted = 2
    from_user = models.ForeignKey(User, related_name='from_users')
    to_user = models.ForeignKey(User, related_name='to_users')
    send_time = models.DateTimeField(auto_now_add=True)
    read_time = models.DateTimeField(null=True)
    subject = models.CharField(max_length=120)
    text = models.TextField()
    sender_status = models.IntegerField(default=status_normal)
    receiver_status = models.IntegerField(default=status_normal)

# ***
class Poll(models.Model):
    post = models.ForeignKey(Post)
    limit = models.IntegerField()
    deadline = models.DateTimeField()

# ***
class PollVariant(models.Model):
    poll = models.ForeignKey(Poll)
    text = models.CharField(max_length=500)
    hits = models.IntegerField(default=0)

# ***
class PollVote(models.Model):
    poll = models.ForeignKey(Poll)
    variant = models.ForeignKey(PollVariant, null=True, default=None)
    user = models.ForeignKey(User)


def create_profile(sender, **kw):
    user = kw["instance"]
    if kw["created"]:
        profile = UserProfile(user=user, rate=0)
        profile.save()

post_save.connect(create_profile, sender=User, dispatch_uid="users-profilecreation-signal")

# HQ
class Roster(models.Model):
    owner = models.ForeignKey(User)
    name = models.CharField(default='', max_length=120)
    guid = models.CharField(max_length=36)
    limit = models.IntegerField(null=True)
    system = models.CharField(max_length=8)
    army = models.CharField(max_length=8)

class Unit(models.Model):
    roster = models.ForeignKey(Roster)
    guid = models.CharField(max_length=36)
    name = models.CharField(default='', max_length=120)
    type = models.CharField(max_length=16)
    section = models.CharField(max_length=16)

class Option(models.Model):
    unit = models.ForeignKey(Unit)
    name = models.CharField(max_length=16)
    value = models.CharField(max_length=16)