__author__ = 'dante'

import uuid

from flask import Markup
from flask.ext.login import current_user
from markdown import markdown

from datetime import datetime, timedelta
from blog.text import auto_mark_links, normalize_line_breaks, process_read_more_marker
from db import posts, users, ObjectId
from .locale import local_date


# def get_new_post_data():
#     """Get data block for empty new post form"""
#     tags = [{'data': tag, 'selected': False} for tag in Tag.objects.order_by('-post_count')]
#     return {'title':'', 'text':'', 'tags': tags, 'edit': False}
#
# def get_edit_post_data(post):
#     """Get data block for editing post form"""
#     post_data = {'title': post.title,
#                  'text': post.text,
#                  'id': post.id,
#                  'tags': [{'data': tag, 'selected': False} for tag in Tag.objects.order_by('-post_count')],
#                  'edit': True }
#     for tag in post_data['tags']:
#         if tag['data'] in post.tags.all():
#             tag['selected'] = True
#     return post_data
#
# def delete_post(post):
#     Comment.objects.filter(post=post).delete()
#     PostHit.objects.filter(post=post).delete()
#     Bookmark.objects.filter(post=post).delete()
#     for tag in post.tags.all():
#         tag.post_count -= 1
#         tag.save()
#     post.delete()
#

def get_post_text(text, process_ref=True, process_images=True, process_lines=True):
    if process_ref or process_images:
        text = auto_mark_links(text, process_ref, process_images)
    if process_lines:
        text = normalize_line_breaks(text)
    return text

# def build_preview_poll(count, variants):
#     if count is 0:
#         return None
#     poll = []
#     for variant in variants:
#         poll.append({
#             'num': 1,
#             'percent': 100/len(variants),
#             'text': variant,
#         })
#     return poll
#
# def render_post_preview(post):
#     post_data = {
#         'title': post['title'],
#         'text': process_read_more_marker(get_post_text(post['text'], post['format']['ref'], post['format']['img'], post['format']['lines'])),
#         'poll': build_preview_poll(post['poll']['count'], post['poll']['variants']),
#         'poll_voted': True,
#     }
#     return render_to_string('post_text.html', {'post': post_data})
#
# def fix_tags(post, new_tag_names):
#     # Create old tags dict
#     old_tags = {}
#     for tag in post.tags.all():
#         old_tags[tag.title] = tag
#
#     # Create new tags dict
#     new_tags = {}
#     if len(new_tag_names) is not 0:
#         query = Q()
#         for tag in new_tag_names:
#             query = query | Q(title = tag)
#             new_tags[tag] = None
#         for tag in Tag.objects.filter(query):
#             new_tags[tag.title] = tag
#
#     # Update post tags from new tags dict
#     post.tags.clear()
#     for tag in new_tags.keys():
#         if new_tags[tag] is None:
#             new_tags[tag] = Tag(title=tag)
#             new_tags[tag].save()
#         post.tags.add(new_tags[tag])
#     post.save()
#
#     # Create only changed tags dict
#     fix_tags = old_tags
#     for tag in new_tags.keys():
#         if tag in fix_tags.keys():
#             fix_tags[tag] = None
#         else:
#             fix_tags[tag] = new_tags[tag]
#
#     # Update changed tags post counts
#     for tag in fix_tags.keys():
#         if fix_tags[tag] is not None:
#             fix_tags[tag].post_count = Post.objects.filter(tags__id = fix_tags[tag].id).count()
#             fix_tags[tag].save()
#             try:
#                 user_tag = UserTag.objects.get(title = fix_tags[tag].title, user = post.author)
#                 user_tag.post_count = Post.objects.filter(tags__id = fix_tags[tag].id, author = post.author).count()
#             except UserTag.DoesNotExist:
#                 user_tag = UserTag(title = fix_tags[tag].title, user = post.author, post_count = 1)
#             user_tag.save()
#
# def public_post(author, post_data):
#     post = Post(author=author)
#     post.title = post_data['title']
#     post.text = get_post_text(post_data['text'], post_data['format']['ref'], post_data['format']['img'], post_data['format']['lines'])
#     post.save()
#
#     fix_tags(post, post_data['tags'])
#
#     Bookmark(post=post, user=post.author, hit_comment_count=post.comments).save()
#     if post_data['poll']['count'] is not 0:
#         poll = Poll(post=post,
#             limit=post_data['poll']['count'],
#             deadline=datetime.now() + timedelta(days=post_data['poll']['time']))
#         poll.save()
#         for variant in post_data['poll']['variants']:
#             PollVariant(poll=poll, text=variant).save()
#     return post.id
#
# def can_edit_post(user, post):
#     if post is None:
#         return False
#     if post.author.username == user.username:
#         return True
#     if user.is_staff:
#         return True
#     return False
#
# def get_post(id):
#     try:
#         return Post.objects.get(id=int(id))
#     except Post.DoesNotExist:
#         pass
#
# def get_edited_post(user, id):
#     post = get_post(id)
#     if can_edit_post(user, post):
#         return post
#
# def edit_post(author, post_data):
#     post = Post.objects.get(id=post_data['id'])
#     if not can_edit_post(author, post):
#         return None
#     post.title = post_data['title']
#     post.text = get_post_text(post_data['text'], post_data['format']['ref'], post_data['format']['img'], post_data['format']['lines'])
#     post.save()
#
#     fix_tags(post, post_data['tags'])
#
#     try:
#         bm = Bookmark.objects.get(post=post, user=post.author)
#         bm.hit_comment_count = post.comments
#     except Bookmark.DoesNotExist:
#         bm = Bookmark(post=post, user=post.author, hit_comment_count=post.comments)
#     bm.save()
#     return post.id
#
# def modify_post(author, post):
#     if 'id' not in post.keys():
#         return public_post(author, post)
#     return edit_post(author, post)
#
# def build_poll(variants):
#     poll = []
#     hits = 0
#     for variant in variants:
#         poll.append({
#             'id': variant.id,
#             'num': variant.hits,
#             'text': variant.text,
#             })
#         hits += variant.hits
#     for variant in poll:
#         variant['percent'] = 100.0 * variant['num'] / hits if hits is not 0 else 0
#     return poll


def norm_user_description(user):
    return {
        'id': str(user['_id']),
        'name': user['name'],
        'avatar': user['personal_info']['avatar'],
        'rate': user['rate']
    }


def norm_edited_description(edited):
    return {
        'name': edited['name'],
        'time': local_date(edited['time']),
    }


def new_comment(data):
    comment = {
        'id': uuid.uuid4().get_hex(),
        'author': ObjectId(current_user.id),
        'text': get_post_text(data['comment']),
        'time': datetime.now(),
        'rate': {'pro': [], 'contra': [], 'value': 0}
    }
    if data['id'] != data['post']:
        comment['parent'] = data['id']

    res = posts.update({'_id': ObjectId(data['post'])}, {'$push': {'comments': comment}, '$inc': {'comments_count': 1}})

    if res['err']:
        return {'error': res['err']}

    return {
        'comment': {
            'id': comment['id'],
            'author': str(comment['author']),
            'text': Markup(markdown(comment['text'])),
            'time': local_date(comment['time']),
            'rate': 0,
            'parent': None,
            'pro': False,
            'contra': False
        },
        'author': norm_user_description(users.find_one({'_id': ObjectId(current_user.id)}))
    }

    # comment = Comment(
    #     text=get_comment_text(text, proc_ref, proc_images, proc_lines),
    #     author=author,
    #     post=post)
    # if parent is not None:
    #     comment.parent = parent
    #     comment.level = parent.level + 1
    # comment.save()
    # post.comments = Comment.objects.filter(post=post).count()
    # post.save()
    #
    # try:
    #     hit = PostHit.objects.get(post=post, user=author)
    #     hit.hit_comment_count += 1
    #     hit.save()
    # except Exception:
    #     pass
    #
    # try:
    #     bm = Bookmark.objects.get(post=post, user=author)
    #     bm.hit_comment_count += 1
    # except Exception:
    #     bm = Bookmark(post=post, user=author, hit_comment_count=post.comments)
    # bm.save()
    #
    # to = []
    # for notify in Bookmark.objects.filter(post=post):
    #     if notify.user.get_profile().new_bookmarked_comments_notify and notify.user.username != author.username:
    #         to.append(notify.user.email)
    # if len(to) is not 0:
    #     send_email(EmailMessage(new_comment_subj,
    #         new_comment_msg % {'author':author.username,
    #                            'text': text,
    #                            'title': post.title,
    #                            'post_id': post.id}, bcc=to))
    #
    # return comment


def comment_rate_weight():
    if current_user.rate < 100.0:
        return 1.0
    return 1.0 + (float(current_user.rate) - 100.0) / 200.0


def post_rate_weight():
    return comment_rate_weight() * 5.0


def rate_post(rec_id, rate, weight):
    if rate == 'pro':
        val = 1. * weight
    elif rate == 'contra':
        val = -1. * weight
    else:
        return

    if posts.find({
        '_id': ObjectId(rec_id),
        '$or': [
            {'rate.pro': ObjectId(current_user.id)},
            {'rate.contra': ObjectId(current_user.id)},
        ]
    }).count() > 0:
        return

    posts.update({
        '_id': ObjectId(rec_id),
    }, {
        '$addToSet': {'rate.' + rate: ObjectId(current_user.id)},
        '$inc': {'rate.value': val}
    })
    record = posts.find_one({'_id': ObjectId(rec_id)})
    users.update({'_id': ObjectId(record['author'])}, {'$inc': {'rate': val}})
    user_rate = users.find_one({'_id': ObjectId(record['author'])}, {'rate': 1})['rate']

    return {
        'author_rate': user_rate,
        'pro': ObjectId(current_user.id) in record['rate']['pro'],
        'contra': ObjectId(current_user.id) in record['rate']['contra'],
        'rate': record['rate']['value']
    }


def rate_comment(rec_id, rate, weight):
    if rate == 'pro':
        val = 1. * weight
    elif rate == 'contra':
        val = -1. * weight
    else:
        return

    user_object_id = ObjectId(current_user.id)
    comment = posts.find_one({'comments.id': rec_id}, {'comments.$': 1})['comments'][0]
    if user_object_id in comment['rate']['pro'] or user_object_id in comment['rate']['contra']:
        return

    posts.update({
        'comments.id': rec_id,
    }, {
        '$addToSet': {'comments.$.rate.' + rate: ObjectId(current_user.id)},
        '$inc': {'comments.$.rate.value': val}
    })
    users.update({'_id': ObjectId(comment['author'])}, {'$inc': {'rate': val}})
    user_rate = users.find_one({'_id': ObjectId(comment['author'])}, {'rate': 1})['rate']
    record = posts.find_one({'comments.id': rec_id}, {'comments.$': 1})['comments'][0]
    return {
        'author_rate': user_rate,
        'pro': ObjectId(current_user.id) in record['rate']['pro'],
        'contra': ObjectId(current_user.id) in record['rate']['contra'],
        'rate': record['rate']['value']
    }


def can_modify(user_id):
    return current_user.id == str(user_id)


def get_edit_text(data):
    comment = posts.find_one({'comments.id': data['id']}, {'comments.$': 1})['comments'][0]
    return {'text': comment['text']}


def edit_comment(data):
    comment = posts.find_one({'comments.id': data['id']}, {'comments.$': 1})['comments'][0]
    if not can_modify(comment['author']):
        return {'error': True}
    text = get_post_text(data['text'])
    edited = {
        'time': datetime.now(),
        'user': ObjectId(current_user.id),
        'name': current_user.name
    }
    res = posts.update({
        'comments.id': data['id'],
    }, {
        '$set': {
            'comments.$.text': text,
            'comments.$.edit': edited,
        }
    })
    if not res['updatedExisting']:
        return {'error': True}
    return {'text':  markdown(text), 'edited': norm_edited_description(edited)}


def update_post(data):
    if 'rate' in list(data.keys()) and data['id'] != data['post']:
        return rate_comment(data['id'], data['rate'], comment_rate_weight())
    elif 'rate' in list(data.keys()) and data['id'] == data['post']:
        return rate_post(data['id'], data['rate'], post_rate_weight())
    elif 'preview' in list(data.keys()):
        return {'text': markdown(get_post_text(data['preview']))}
    elif 'comment' in list(data.keys()):
        return new_comment(data)
    elif data['type'] == 'edit_mode':
        return get_edit_text(data)
    elif data['type'] == 'edit':
        return edit_comment(data)


def get_single_post(post_id):
    """Get data for display single post"""

    if current_user.is_authenticated():
        reader = {
            'name': current_user.name,
            'id': current_user.id,
        }
    else:
        reader = None

    post_data = posts.find_one({'_id': ObjectId(post_id)})
    post = {
        'id': str(post_data['_id']),
        'author': str(post_data['author']),
        'title': post_data['title'],
        'text':  Markup(markdown(process_read_more_marker(post_data['text']))),
        'time': local_date(post_data['time']),
        'tags': post_data['tags'],
        'rate': post_data['rate']['value'],
        'pro': ObjectId(reader['id']) in post_data['rate']['pro'] if reader else False,
        'contra': ObjectId(reader['id']) in post_data['rate']['contra'] if reader else False,
        'comments': [{
            'id': data['id'],
            'author': str(data['author']),
            'text': Markup(markdown(data['text'])),
            'time': local_date(data['time']),
            'rate': data['rate']['value'],
            'parent': str(data['parent']) if 'parent' in list(data.keys()) else None,
            'pro': ObjectId(reader['id']) in data['rate']['pro'] if reader else False,
            'contra': ObjectId(reader['id']) in data['rate']['contra'] if reader else False,
            'edit': norm_edited_description(data['edit']) if 'edit' in list(data.keys()) else None
        } for data in post_data['comments']]
    }

    authors = {ObjectId(post['author']): None}
    for comment in post['comments']:
        authors[ObjectId(comment['author'])] = None
    for auth in users.find({'_id': {'$in': list(authors.keys())}}):
        authors[auth['_id']] = auth

    author = authors[ObjectId(post['author'])]
    tags = [v_k[0] for v_k in sorted(iter(author['blog']['tags'].items()),
                                        key=lambda k_v: (k_v[1], k_v[0]), reverse=True)]

    data = {
        'post': post,
        'authors': [norm_user_description(authors[a]) for a in authors],
        'author': author,
        'tags': tags,
        'reader': reader
    }
    return data

        # tags = post.tags.all()
    # comment_list = post.comment_set.all().order_by('time')
    # if request.user.is_authenticated():
    #     votes = Vote.objects.filter(post=post, voting_user=request.user)
    #
    #     def mark_vote(data, vote):
    #         if vote.value == Vote.up:
    #             data.vote_up = True
    #         elif vote.value == Vote.down:
    #             data.vote_down = True
    #
    #     for vote in votes:
    #         if vote.comment is None:
    #             mark_vote(post, vote)
    #         else:
    #             for comment in comment_list:
    #                 if comment.id == vote.comment_id:
    #                     mark_vote(comment, vote)
    #
    #     new_count = 0
    #     try:
    #         hit = PostHit.objects.get(post=post, user=request.user)
    #         for comment in comment_list:
    #             if comment.time >= hit.hit_time and comment.author.username != hit.user.username:
    #                 comment.new = True
    #                 new_count += 1
    #     except PostHit.DoesNotExist:
    #         hit = PostHit(post=post, user=request.user)
    #     hit.hit_comment_count = len(comment_list)
    #     hit.save()
    #     post.new_count = new_count
    #
    #     try:
    #         bookmark = Bookmark.objects.get(post=post, user=request.user)
    #         bookmark.hit_comment_count = len(comment_list)
    #         bookmark.save()
    #         if bookmark.pinned:
    #             post.bookmarked = True
    #     except Bookmark.DoesNotExist:
    #         pass
    # post.text = process_read_more_marker(post.text)
    #
    # post_poll = post.poll_set.all()
    # if len(post_poll) is 1:
    #     post.poll = build_poll(post_poll[0].pollvariant_set.all())
    #     post.poll_limit = post_poll[0].limit
    #     post.poll_deadline = post_poll[0].deadline
    #     if post.poll_deadline < datetime.now():
    #         post.poll_finished = True
    #     if request.user.is_authenticated():
    #         if PollVote.objects.filter(user = request.user, poll = post_poll[0]).count() > 0:
    #             post.poll_voted = True
    # return {
    #     'post': post,
    #     'comments': comment_list,
    #     'tags': tags,
    #     'can_edit': can_edit_post(request.user, post) if request.user.is_authenticated() else False,
    # }
#
# def vote_poll(user, post, variants):
#     poll = Poll.objects.get(post=Post(id=post))
#     if len(variants) is 0:
#         PollVote(user=user, poll=poll).save()
#     else:
#         for variant_id in variants:
#             variant = PollVariant.objects.get(id=variant_id)
#             PollVote(user=user, variant=variant, poll=poll).save()
#             variant.hits = PollVote.objects.filter(poll=poll, variant=variant).count()
#             variant.save()
#     return render_to_string('post_poll.html', { 'post' : { 'poll' : build_poll(poll.pollvariant_set.all()) }} )
