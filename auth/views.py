__author__ = 'dante'

from flask import Blueprint, render_template, request, url_for, redirect, flash
from flask.views import View, MethodView
from flask.ext.login import current_user, logout_user, login_required
from flask.ext.wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import DataRequired, Email, Optional
from .user import LoginManager


class SignIn(Form):
    email = TextField('Email', validators=[Email()])
    password = PasswordField('Password', validators=[DataRequired()])


class SignUp(Form):
    email = TextField('Email', validators=[Email()])
    name = TextField('Name', validators=[DataRequired()])
    new_password = PasswordField('Password', validators=[DataRequired()])
    repeat_password = PasswordField('Repeat password', validators=[DataRequired()])


class Amnesia(Form):
    email = TextField('Email', validators=[Email()])


class Settings(Form):
    password = PasswordField('Password', validators=[DataRequired()])
    email = TextField('Email', validators=[Email()])
    name = TextField('Name', validators=[DataRequired()])
    new_password = PasswordField('New password', validators=[Optional()])
    repeat_password = PasswordField('Repeat password', validators=[Optional()])


class AuthBlueprint(Blueprint):
    def __init__(self, app):
        super(AuthBlueprint, self).__init__('auth', __name__)
        self.login_manager = LoginManager(app=app)
        self.add_url_rule('/welcome', view_func=self.SignInFormView.as_view('signin_page', manager=self.login_manager))
        self.add_url_rule('/signup', view_func=self.SignUpFormView.as_view('signup_page', manager=self.login_manager))
        self.add_url_rule('/signout', endpoint='signout_page', view_func=self.signout_page)
        self.add_url_rule('/donate', endpoint='donate', view_func=self.donate)
        self.add_url_rule('/help', endpoint='help', view_func=self.view_help)
        self.add_url_rule('/amnesia', view_func=self.Amnesia.as_view('amnesia', manager=self.login_manager))
        self.add_url_rule('/settings', view_func=self.Settings.as_view('settings_page', manager=self.login_manager))
        self.add_url_rule('/recovery/<key>', view_func=self.Recovery.as_view('recovery', manager=self.login_manager))
        app.register_blueprint(self)

    class LoginManagerView(object):
        def __init__(self, manager):
            super(AuthBlueprint.LoginManagerView, self).__init__()
            self.manager = manager

    class SignInFormView(View, LoginManagerView):

        methods = ['GET', 'POST']

        def dispatch_request(self):
            if current_user.is_authenticated:
                return redirect(url_for('builder.main'))
            form = SignIn()
            if form.validate_on_submit():
                try:
                    self.manager.process_login_user(form.email.data, form.password.data, True)
                    next_url = request.args.get('next', None)
                    if next_url:
                        return redirect(next_url)
                    return redirect(url_for('builder.main'))
                except RuntimeError as e:
                    flash(str(e), category='error')
            return render_template("auth/signin.html", form=form)

    class SignUpFormView(View, LoginManagerView):

        methods = ['GET', 'POST']

        def dispatch_request(self):
            if current_user.is_authenticated:
                return redirect(url_for('builder.main'))
            form = SignUp()
            if form.validate_on_submit():
                try:
                    self.manager.create_user(form.email.data, form.name.data, form.new_password.data,
                                             form.repeat_password.data)
                    next_url = request.args.get('next', None)
                    if next_url:
                        return redirect(next_url)
                    return redirect(url_for('builder.main'))
                except RuntimeError as e:
                    flash(e, category='error')
            return render_template("auth/signup.html", form=form)

    class Amnesia(View, LoginManagerView):

        methods = ['GET', 'POST']

        def dispatch_request(self):
            if current_user.is_authenticated:
                return redirect(url_for('builder.main'))
            form = Amnesia()
            if form.validate_on_submit():
                try:
                    flash(self.manager.create_recovery_key(form.email.data), category='success')
                except RuntimeError as e:
                    flash(e, category='error')
            return render_template("auth/amnesia.html", form=form)

    class Settings(View, LoginManagerView):
        decorators = [login_required]
        methods = ['GET', 'POST']

        def dispatch_request(self):
            form = Settings(name=current_user.name, email=current_user.email)
            if form.validate_on_submit():
                try:
                    flash(current_user.update_settings(form.data), category='success')
                except RuntimeError as e:
                    flash(e, category='error')
            return render_template("auth/settings.html", form=form)

    @staticmethod
    def signout_page():
        logout_user()
        return redirect(url_for('auth.signin_page'))

    @staticmethod
    def donate():
        return render_template("auth/donate.html")

    @staticmethod
    def view_help():
        return render_template("auth/help.html")

    class Recovery(MethodView, LoginManagerView):
        def get(self, key):
            try:
                flash(self.manager.process_recovery_key(key), category='success')
            except RuntimeError as e:
                flash(e, category='error')
            return redirect(url_for('auth.signin_page'))
