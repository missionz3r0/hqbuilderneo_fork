/**
 * Created by dromanow on 4/25/14.
 */


var SubUnit = function(opt) {
    this.type = 'sub_unit';
    this.name = opt.data.name;
    this.setupNode(opt.parent, opt.data);
    this.unit = new Unit({parent: this, data: opt.data.unit});
};

SubUnit.prototype = new Node();

SubUnit.prototype.updateUnit = function(obj) {
    this.parent.update({id: this.id, unit: obj})
};

SubUnit.prototype.pushChanges = function(data) {
    this.genericPush(data);
    if (data.unit && data.unit.id == this.unit.id)
        this.unit.pushChanges(data.unit);
};
