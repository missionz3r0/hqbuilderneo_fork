/**
 * Created by dante on 4/23/14.
 */

var OneOfSingleOption = function(parent, data) {
    this.setupNode(parent, data);
    this.name = data.name;
};

OneOfSingleOption.prototype = new Node();

OneOfSingleOption.prototype.pushChanges = function(data) {
    this.genericPush(data);
};

var OneOf = function(opt) {
    var self = this;
    this.setupNode(opt.parent, opt.data);
    this.type = 'one_of';
    this.section = opt.data.name;
    this.options = ko.observableArray(opt.data.options.map(function(opt) {
        return new OneOfSingleOption(self, opt);
    }));
    this.cur = ko.observable(this.getOption(opt.data.selected));
    this.cur.subscribe(function(value) {
        self.parent.update({id: self.id, selected: value.id});
    });
};

OneOf.prototype = new Node();

OneOf.prototype.getOption = function(id) {
    for(var i = 0; i < this.options().length; i++)
        if (this.options()[i].id == id)
            return this.options()[i];
    return this.options()[0];
};

OneOf.prototype.pushChanges = function(data) {
    var self = this;
    this.genericPush(data);
    if (data.selected)
        this.cur(this.getOption(data.selected));
    if (data.options) {
        $.each(data.options, function(n, option_data) {
            $.each(self.options(), function(n, option) {
                if (option_data.id == option.id)
                    option.pushChanges(option_data);
            });
        });
    }
};

OneOf.prototype.setOptionDisable = function(option, item) {
    ko.applyBindingsToNode(option, {enable: item.active, visible: item.visible}, item);
};
