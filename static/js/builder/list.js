var RosterEntity = function(data) {
    var self = this;
    self.id = data.id;
    self.url = data.url;
    self.type_name = data.type_name;
    self.game = data.game;
    self.name = data.name;
    self.points = data.points;
    self.limit = data.limit;
    self.description = data.description;
    self.tags = ko.observableArray(data.tags);
};

var RosterList = function(data) {
    this.loaded = ko.observable(false);
    var self = this;
    self.rosters = ko.observableArray(self.buildRosters(data.rosters));
    self.tagList = new TagListControl(self.rosters(), null);
    this.del = new DelSelector(this.rosters(), function(ids) { self.deleteRosters(ids); });
    this.delUrl = data.delUrl;
    this.loaded(true);
};

RosterList.prototype.buildRosters = function(rosters) {
    return rosters.map(function(r) {
        return new RosterEntity(r);
    });
};

RosterList.prototype.deleteRosters = function(ids) {
    var self = this;
    document.modalInfo.warning('Delete all selected rosters? Are you sure?', function() {
         new AjaxTransport(self.delUrl).send({'delete': ids}, function(data) {
            if (data.rosters) {
                var rosters = self.buildRosters(data.rosters);
                self.tagList.reset(rosters);
                self.del.reset(rosters);
                self.rosters(rosters);
            }
        });
    });
};
