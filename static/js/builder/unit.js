/**
 * Created by dromanow on 4/29/14.
 */


var Unit = function(unit) {
    var self = this;
    this.setupNode(unit.parent, unit.data);
    this.name = unit.data.description.name;
    this.user_name = new EditField({
        data: {value: unit.data.user_name, name: "Custom name", visible: false, active: true},
        update: function(value) { self.parent.updateUnit({id: self.id, user_name: value}); }
    });
    this.isActive = ko.observable(false);
    this.points = ko.observable(unit.data.description.points);
    this.descriptionObject = ko.observable(unit.data.description);
    this.errors = ko.observableArray(unit.data.errors);
    this.unique = unit.data.unique;
    this.count = ko.observable(unit.data.description.count);
    this.detachUnit = unit.data.detach_unit;
    this.link = (typeof unit.data.wikilink == "undefined")? false: unit.data.wikilink;
    this.optionsBlock = new OptionsBlock(this, unit.data.options);
    this.static_unit = ko.observable(!this.optionsBlock.hasVisible());
    this.star = ko.observable(unit.data.star? unit.data.star: 0);
};

Unit.prototype = new Node();

Unit.prototype.close = function() {
    this.isActive(false);
};

Unit.prototype.update = function(obj) {
    this.parent.updateUnit({id: this.id, options: [obj]});
};

Unit.prototype.toggle_custom_name = function() {
    this.user_name.toggle();
};

Unit.prototype.toggle_star = function() {
    this.parent.updateUnit({id: this.id, star: 3 - this.star()});    
}

Unit.prototype.edit = function() {
    this.isActive(!this.isActive());
};

Unit.prototype.add = function(unit) {
    this.parent.addUnit({id: this.id});
};

Unit.prototype.del = function(unit) {
    var self = this;
    document.modalInfo.warning("Delete " + this.name + "? Are you sure?", function() {
        self.parent.deleteUnit({id: self.id});
    });
};

Unit.prototype.clone = function() {
    this.parent.cloneUnit({id: this.id});
};

Unit.prototype.split = function() {
    this.parent.splitUnit({id: this.id});
};

Unit.prototype.pushChanges = function(data) {
    this.genericPush(data);
    if (data.count)
        this.count(data.count);
    if (data.description) {
        this.descriptionObject(data.description);
    }
    if (data.points !== undefined)
        this.points(data.points);
    if (data.user_name !== undefined)
        self.user_name.value(data.user_name);
    if (data.star !== undefined)
        this.star(data.star);

    if (data.errors)
        this.errors(data.errors);
    else
        this.errors([]);

    if (data.options)
    {
        this.optionsBlock.pushChanges(data.options);
        this.static_unit(!this.optionsBlock.hasVisible());
        if (this.static_unit())
            this.isActive(false);
    }
};
