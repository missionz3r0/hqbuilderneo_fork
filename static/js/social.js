/**
 * Created by dromanow on 2/3/14.
 */

/*
 data is like: 
 {
  id: "idvalue"
  variants:
    {
     {
       id: "id1",
       text: "text1"
     },
     {
       id: "id2",
       text: "text2"
     }
    }
 }
*/
var Poll = function(data) {
    var self = this;

    self.visible = ko.observable(false);
    self.visible.click = function() {
        self.visible(!self.visible());
    };
    if (data == null) {
        self.id = null;
        self.pollvariants = ko.observableArray();
    }
    else {
        self.id = data.id;        
        self.pollvariants = ko.observableArray(ko.utils.arrayMap(data.variants, function(pollvariant) {
            return { id: pollvariant.id, text: pollvariant.text};
    }));
    }
    

    if (self.pollvariants().length > 0) {
        self.visible(true);
    }

    self.add_pollvariant = function() {
        self.pollvariants.push({
            id: "",
            text: "variant"
        });
    };

    self.remove_pollvariant = function(pollvariant) {
        self.pollvariants.remove(pollvariant);
    };

    self.build_poll = function() {
        if (self.visible()) {
            return {
                variants: self.pollvariants()
            };
        } else {
            return null;
        }
    }

};

var PostForm = function(data) {
    var self = this;
    this.hello = ko.observable(true);
    this.hello.placeholder = "Hi {0}. Do you want post something?".format(data.name?data.name:'');

    this.title = ko.observable(data.title||'');
    this.title.placeholder = ko.computed(function() {
        return self.hello() ? self.hello.placeholder : 'Title';
    });
    this.title.click = function() {
        self.hello(false);
    };

    this.text = ko.observable(data.text);
    this.text.more = ko.observable(false);
    this.text.more.click = function() {
        self.text.more(!self.text.more());
    };

    this.poll = new Poll(data.poll);

    this.transport = data.transport;
    self.buildPost = function() {
        return {
            title: self.title(),
            text: self.text(),
            tags: self.tags(),
            poll: self.poll.build_poll()
        };
    };

    this.preview = function() {
        if (!self.text()) {
            document.modalInfo.error('Text field is empty, nothing to post!');
            return;
        }
        self.transport.send({preview: self.text()}, function(res) {
            self.preview.block(res.text);
            self.preview.show(true);
        });
    };
    this.send = function() {
        if (!self.text()) {
            document.modalInfo.error('Text field is empty, nothing to post!');
            return;
        }
        self.transport.send({send: self.buildPost()});
    };

    this.clearForm = function() {
        self.preview.show(false);
        self.title('');
        self.text('');
        self.tags([]);
        self.hello(true);
    };

    this.showForm = function(data) {
        self.title(data.title);
        self.text(data.text);
        self.tags(data.tags);
        self.hello(false);
    };

    this.preview.show = ko.observable(false);
    this.preview.close = function() { self.preview.show(false); };
    this.preview.block = ko.observable('');

    this.cancel = function() {
        document.modalInfo.warning('Cancel this editing, are you sure?', function() {
            self.clearForm();
            self.hello(true);
        });
    };

    this.tags = ko.observableArray(data.tags);
    this.removeTag = function(val) {
        self.tags.remove(val);
    };
    this.tag = ko.observable('');
    this.tag.add = function() {
        if ($.inArray(self.tag(), self.tags()) < 0 && self.tag())
        {
            self.tags.push(self.tag());
            self.tag('');
        }
    };
};

var Comment = function(data) {
    var self = this;
    self.id = data.id;
    self.author = data.author;
    self.text = ko.observable(data.text);
    self.created = ko.observable(moment(data.created).fromNow());
    self.deleted = ko.observable(data.deleted);
    self.readOnly = ko.observable(data.read_only);
    self.deleteComment = function() {
        self.parent.transport.send({'delete': self.id}, function(res) {
            if (res.deleted) {
                self.deleted(true);
                self.text('');
            }
        });
    };
    self.restoreComment = function() {
        self.parent.transport.send({'restore': self.id}, function(res) {
            if (res.text) {
                self.deleted(false);
                self.text(res.text);
            }
        });
    };
    self.editComment = function() {
        self.parent.commentForm.edit(self);
    };
};

var ROComment = function(data) {
    var self = this;
    self.id = data.id;
    self.author = data.author;
    self.text = ko.observable(data.text);
    self.created = ko.observable(moment(data.created).fromNow());
    self.deleted = ko.observable(data.deleted);
    self.post = data.post;
};

var CommentForm = function() {
    var self = this;
    self.text = ko.observable('');
    self.postAvalible = ko.computed(function() {
        return self.text() != '';
    });
    self.preview = function() {
        self.parent.transport.send({preview: self.text()}, function(res) {
            self.preview.text(res.text);
            self.preview.show(true);
        });
    };
    self.send = function() {
        self.parent.transport.send({send: self.text(), comments: self.parent.comments().length}, function(res) {
            self.parent.appendComments(res.comments);
            self.resetForm();
            scrollToElement($("#"+String(res.comments[0].id)));
        });
    };
    self.preview.text = ko.observable('');
    self.preview.show = ko.observable(false);
    self.preview.close = function() {
        self.preview.show(false);
    };
    self.edit = function(comment) {
        self.edit.show(true);
        self.edit.comment = comment;
        self.parent.transport.send({raw_text: comment.id}, function(res) {
            if (res.text)
                self.text(res.text);
        });
        scrollToElement($("#comment-form"));
    };
    self.edit.show = ko.observable(false);
    self.edit.comment = null;
    self.edit.send = function() {
        self.parent.transport.send({edit: self.edit.comment.id, text: self.text()}, function(res) {
            if (res.text) {
                self.edit.comment.text(res.text);
                scrollToElement($("#"+String(self.edit.comment.id)));
                self.resetForm();
            }
        });
    };
    self.resetForm = function() {
        self.edit.show(false);
        self.edit.comment = null;
        self.text('');
        self.preview.show(false);
    };
};

var CommentList = function(comments, commentForm, transport) {
    var self = this;
    self.comments = ko.observableArray([]);
    self.commentForm = commentForm;
    self.commentForm.parent = self;
    self.appendComments = function(comments) {
        self.comments(self.comments().concat(comments.map(function(c) {
            var comment = new Comment(c);
            comment.parent = self;
            return comment;
        })));
    };
    self.transport = transport;
    self.appendComments(comments);
};

var CommentFeed = function(comments) {
    var self = this;
    self.comments = ko.observableArray([]);
    self.appendComments = function(comments) {
        self.comments(self.comments().concat(comments.map(function(c) {
            var comment = new ROComment(c);
            comment.parent = self;
            return comment;
        })));
    };
    self.appendComments(comments);
};

var RateForm = function(data) {
    var self = this;
    self.value = ko.observable(data.value);
    self.plus = function() { self.rate(+1); };
    self.minus = function() { self.rate(-1); };
    self.rate = function(value) {
        data.transport.send({rate: value}, function(res) {
            self.plus.rated(res.plus);
            self.minus.rated(res.minus);
            self.value(res.value);
        });
    };
    self.plus.rated = ko.observable(data.plus);
    self.minus.rated = ko.observable(data.minus);
};
