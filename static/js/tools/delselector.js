/**
 * Created by dromanow on 5/22/14.
 */


var DelSelector = function(objs, delCallback) {
    var self = this;
    this.objs = ko.observableArray();
    this.reset(objs);
    this.deletingAllowed = ko.computed(function() {
        return self.objs().some(function(r) {
            return r.selected() && r.visible();
        });
    });
    this.delCallback = delCallback;
};

DelSelector.prototype.setSelection = function(val) {
    $.each(this.objs(), function(n, o) {
        if (o.visible())
            o.selected(val);
    });
};

DelSelector.prototype.selectAll = function(val) { this.setSelection(true); };

DelSelector.prototype.clear = function(val) { this.setSelection(false) };

DelSelector.prototype.deleteObjs = function() {
    this.delCallback(this.objs().reduce(function(prev, cur) {
        if (cur.selected() && cur.visible())
            prev.push(cur.id);
        return prev;
    }, []));
};

DelSelector.prototype.reset = function(objs) {
    objs.forEach(function(o) {
        o.selected = ko.observable(false);
    });
    this.objs(objs);
};
