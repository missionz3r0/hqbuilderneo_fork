import logging
from sys import argv
from flask import Flask, redirect, url_for, request, flash
from flask.ext.assets import Environment, Bundle
from flask.ext.markdown import Markdown
from raven.contrib.flask import Sentry

from config import ReleaseConfig, DebugConfig


class Hq(Flask):

    @staticmethod
    def _get_js_files():
        return [
            'js/tools/format.js',
            'js/tools/filter.js',
            'js/tools/indexof.js',
            'js/tools/taglist.js',
            'js/tools/delselector.js',
            'js/builder/options/node.js',
            'js/builder/options/oneof.js',
            'js/builder/options/optionslist.js',
            'js/builder/options/count.js',
            'js/builder/options/subunit.js',
            'js/builder/options/subroster.js',
            'js/builder/options/optionalsubunit.js',
            'js/builder/options/unitlist.js',
            'js/builder/options/block.js',
            'js/builder/options/editfield.js',
            'js/social.js',
            'js/tools.js',
            'js/builder/list.js',
            'js/builder/unit.js',
            'js/builder/section.js',
            'js/builder/roster.js',
        ]

    @staticmethod
    def _get_css_files():
        return [
            'css/doc.css',
        ]

    @staticmethod
    def unhandled_exception(exception):
        app.logger.exception("Unhadled exception for adress \"{}\"".format(request.path))
        flash(request.base_url)
        return redirect(url_for('error'))

    def __init__(self, debug_mode=False):
        super(Hq, self).__init__(__name__)

        if debug_mode:
            self.config.from_object(DebugConfig)
            self.logger.warning('Debug config loaded')
        else:
            self.config.from_object(ReleaseConfig)

        from db.session import db
        db.init_app(self)

        from auth.mail import Mailer
        self.mailer = Mailer(self)

        self.assets = Environment(self)
        self.assets.register(
            'js_all',
            Bundle(
                Bundle(
                    'extern/jquery.min.js',
                    'extern/bootstrap3/js/bootstrap.min.js',
                    'extern/knockout.min.js',
                    'extern/moment.min.js',
                    'extern/share42/share42.js',
                    'extern/bootstrap-select-1.12.0/js/bootstrap-select.min.js'
                ),
                Bundle(*self._get_js_files(), filters='jsmin'),
                output='gen/_packed.js'
            )
        )
        self.assets.register(
            'css_all',
            Bundle(
                Bundle(
                    'extern/bootstrap3/css/bootstrap.min.css',
                    'extern/bootstrap-select-1.12.0/css/bootstrap-select.min.css'
                ),
                Bundle(*self._get_css_files(), filters='cssmin'),
                output='gen/_packed.css'
            )
        )

        self.markdown = Markdown(self)

        from auth.views import AuthBlueprint
        from builder.views import BuilderBlueprint
        from admin import AdminBlueprint
        from social.views import SocialBlueprint
        from battles.views import BattlesBlueprint
        from feed import FeedBlueprint

        self.admin = AdminBlueprint(self)
        self.auth = AuthBlueprint(self)
        self.builder = BuilderBlueprint(self)
        self.social = SocialBlueprint(self)
        self.battles = BattlesBlueprint(self)
        self.feed = FeedBlueprint(self)

        self.add_template_filter(self.moment_js_utc, name='moment_js_utc')
        self.context_processor(lambda: dict(debug_mode=self.debug))

        # in debug mode error handlers are not installed
        # to get more information from failure
        if not debug_mode:

            def error_page(error_location):
                return self.render_template("builder/failure.html")

            self.add_url_rule('/error', endpoint='error', view_func=error_page)
            self.error_handler_spec[None][None] = [(Exception, Hq.unhandled_exception)]

            # we log to stderr
            handler = logging.StreamHandler()
            handler.setLevel(logging.ERROR)
            formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
            handler.setFormatter(formatter)
            self.logger.addHandler(handler)

    @staticmethod
    def moment_js_utc(date):
        return date.strftime("%Y-%m-%dT%H:%M:%S Z")

    @property
    def tornado_address(self):
        return self.config['TORNADO_ADDRESS']

    @property
    def tornado_port(self):
        return self.config['TORNADO_PORT']

    @property
    def sentry_dsn(self):
        return self.config['SENTY_DSN']


if __name__ == '__main__':
    debug = '--debug' in argv
    tornado = '--tornado' in argv
    app = Hq(debug_mode=debug)
    sentry = Sentry()
    sentry.init_app(app, dsn=app.sentry_dsn, logging=True, level=logging.ERROR, wrap_wsgi=True)

    if not tornado:
        app.run(app.tornado_address, app.tornado_port)
    else:
        from tornado.wsgi import WSGIContainer
        from tornado.httpserver import HTTPServer
        from tornado.ioloop import IOLoop
        http_server = HTTPServer(WSGIContainer(app))
        http_server.listen(address=app.tornado_address, port=app.tornado_port)
        IOLoop.instance().start()
