__author__ = 'Ivan Truskov'

import json
import io
from builder.games import Games
from functools32 import partial

roster_data = []
dump_name = 'roster_dump.json'


def setup():
    global roster_data
    fstream = io.open(dump_name)
    roster_data = json.load(fstream)
    fstream.close()


def test_roster_load():
    for single_roster in roster_data:
        test_part = partial(check_roster, single_roster)
        test_part.description = 'Load roster id#{}'.format(single_roster['id'])
        yield (test_part, )


def check_roster(data):
    games_object = Games(True, False)
    roster_object = games_object.load_roster(0, data['data'])
    # except Exception as e:
    #     # to turn errors into failures
    #     assert False, str(e)
