__author__ = 'dante'

import os
from fabric.api import run, env, cd, roles

env.roledefs['production'] = ['root@hq-builder.com:25108']


def production_env():
    env.user = 'root'
    env.project_root = '/home/web/hq'
    env.venv_root = '/home/web/hq.venv/bin'
    env.python = env.venv_root + '/python'
    env.pip = env.venv_root + '/pip'
    env.alembic = env.venv_root + '/alembic'


@roles('production')
def deploy(hg_user, hg_pass):
    production_env()
    with cd(env.project_root):
        res = run(
            'hg pull -u '
            '--config auth.x.prefix=* '
            '--config auth.x.username={0} '
            '--config auth.x.password={1}'.format(hg_user, hg_pass)
        )
        try:
            res.index('no changes found')
        except ValueError:
            run('supervisorctl restart hq')


@roles('production')
def pip_install():
    production_env()
    run('{pip} install --upgrade -r {filepath}'.format(pip=env.pip,
        filepath=os.path.join(env.project_root, 'requirements.txt')))


@roles('production')
def mount():
    production_env()
    run('mount -t davfs https://webdav.yandex.ru /mnt/yandex.disk')


@roles('production')
def alembic_head():
    production_env()
    with cd(env.project_root):
        run('{alembic} -c db/alembic/alembic.ini -x mode=release upgrade head'.format(alembic=env.alembic))
