# -*- coding: utf-8 -*-

# from flask import Flask, request
# import requests
# app = Flask(__name__)

import re

from builder.core.model_descriptor import ModelDescriptor

pattern = re.compile('[\W_]+')


def build_id(name):
    name = name.lower()
    return pattern.sub('', name)


class Unit(object):

    name = None
    base_points = 0
    gear = []

    def __init__(self):
        super(Unit, self).__init__()
        print('''class {class_name}(Unit):
    type_name = \'{unit_name}\'
    type_id = \'{type_name}_v1\'

    def __init__(self, parent):
        super({class_name}, self).__init__(parent=parent, points={points}, gear=[{gear}])

    '''.format(class_name=self.__class__.__name__, unit_name=self.name, type_name=build_id(self.name),
               points=self.base_points, gear=''.join('Gear(\'{0}\'), '.format(g) for g in self.gear)))

    def opt_count(self, name, min, max, points, help=None, id=None):
        print(('            self.{id_name} = Count(self, \'{name}\', min_limit={min_limit}, max_limit={max_limit}, points={points})'.format(
            id_name=build_id(name), name=name, min_limit=min, max_limit=max, points=points)))

    def opt_one_of(self, name, variants, id=None):
        print('''
    class {name}(OneOf):
        def __init__(self, parent):
            super({class_name}.{name}, self).__init__(parent=parent, name='{name}')
        '''.format(name=name, class_name=self.__class__.__name__))
        for o in variants:
            print(('            self.{0} = self.variant(\'{1}\', {2})'.format(build_id(o[0]), o[0], o[1])))

    def opt_options_list(self, name, variants, limit=None, id=None, points_limit=None):
        print('''
    class {name}(OptionsList):
        def __init__(self, parent):
            super({class_name}.{name}, self).__init__(parent=parent, name='{name}', limit={limit})
        '''.format(name=name, class_name=self.__class__.__name__, limit=limit))
        for o in variants:
            print(('            self.{0} = self.variant(\'{1}\', {2})'.format(build_id(o[0]), o[0], o[1])))

    def opt_sub_unit(self, unit, id=None):
        pass

    def opt_optional_sub_unit(self, name, units, id=None):
        pass

    def opt_units_list(self, name, unit_class, min, max, id=None):
        pass


class StaticUnit(object):

    name = None
    base_points = 0
    gear = []

    def __init__(self):
        super(StaticUnit, self).__init__()
        print('''class {class_name}(Unit):
    type_name = \'{unit_name}\'
    type_id = \'{type_name}_v1\'

    def __init__(self, parent):
        super({class_name}, self).__init__(parent=parent, points={points}, static=True, gear=[{gear}])

    '''.format(class_name=self.__class__.__name__, unit_name=self.name, type_name=build_id(self.name),
               points=self.base_points, gear=''.join('Gear(\'{0}\'), '.format(g) for g in self.gear)))

    #def opt_count(self, name, min, max, points, help = None, id = None):
    #    print ('            self.{id_name} = Count(self, \'{name}\', min_limit={min_limit}, max_limit={max_limit}, points={points})'.format(
    #        id_name=build_id(name), name=name, min_limit=min, max_limit=max, points=points))


class OgrynSquad(Unit):
    name = 'Ogryn Squad'
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Ogryns', 2, 9, 40)
        # self.transport = self.opt_optional_sub_unit('Transport', Chimera())
        self.base_gear = ['Flak armour', 'Ripper gun', 'Frag grenades']

    def get_count(self):
        return self.count.get() + 1

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Ogryn Bone \'ead', count=1, gear=self.base_gear, points=50).build())
        self.description.add(ModelDescriptor('Ogryn', count=self.count.get(), gear=self.base_gear, points=40).build())
        self.description.add(self.transport.get_selected())


OgrynSquad()


class PsykerSquad(Unit):
    name = 'Psyker Battle Squad'
    base_points = 20

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Sanctioned Psykers', 4, 9, 10)
        # self.transport = self.opt_optional_sub_unit('Transport', Chimera())
        self.base_gear = ['Flak armour', 'Laspistol', 'Closed combat weapon ']

    def get_count(self):
        return self.count.get() + 1

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Overseer', count=1, gear=self.base_gear, points=20).build())
        self.description.add(ModelDescriptor('Sanctioned Psyker', count=self.count.get(), gear=self.base_gear,
                                             points=10).build())
        self.description.add(self.transport.get_selected())


PsykerSquad()


class RatlingSquad(Unit):
    name = 'Ratling Squad'

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Ratlings', 3, 10, 10)
        self.base_gear = ['Flak armour', 'Sniper rifle', 'Laspistol']

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Ratling', count=self.count.get(), gear=self.base_gear, points=10).build())


RatlingSquad()


for opt in []:
# for opt in [special_gear, ride, reward, artefact_weapon, artefact_option, marks]:
    print('')
    for o in opt:
        # print ('self.{0} = self.variant(\'{1}\', {2})'.format(build_id(o[0]), o[0], o[1]))
        print(('self.variant(\'{1}\', {2}) '.format(build_id(o[0]), o[0], o[1])))
        # print ('self.variant(\'{1}\', {2}), '.format(build_id(o[0]), o[0], o[1]))

#
#
# order = 1000
#
# for o in opt:
#     print ('self.{0} = self.variant(\'{1}\', {2})'.format(build_id(o[0]), o[0], o[1]))
#
#
#
#     gear = ['Crozius arcanum', 'Rosarius', 'Power armour', 'Master-crafter plasma pistol', 'Frag grenades',
#             'Krak grenades']
#
#
#
#
# for g in gear:
#     print ('Gear(\'{0}\'), '.format(g))


#import json
#
#
#from db.models import User, Roster as MyRoster
#
#
#
##import time
##import json
##
#start = time.time()
#for roster in MyRoster.find_by_owner('51251e7be75a916405817261'):
##start = time.time()
#    print roster.data['army_id']
#    #user = User()
#print time.time() - start, orm

#CrisisTeam_v2.CrisisSuit()
#
#
#
#gear=['Scout armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Camo cloak', 'Quietus']
##
##
##
#for g in gear:
#    print ('Gear(\'{0}\'), '.format(g))
##

#
#
#@app.route("/_subscribe_ipn/<user_id>", methods=['POST'])
## rom flask import Flask, request
## import requests
## from werkzeug.datastructures import ImmutableOrderedMultiDict
## app = Flask(__name__)
## @app.route('/ipn',methods=['POST'])
#def ipn(user_id):
#    arg = ''
#    #: We use an ImmutableOrderedMultiDict item because it retains the order.
#    # request.parameter_storage_class = ImmutableOrderedMultiDict()
#    params = {'cmd': '_notify-validate'}
#    # values = request.form
#    for x, y in request.form.iteritems():
#        params = {x: y}
#        # arg += "&{x}={y}".format(x=x, y=y)
#        # print x, '=', y
#    # validate_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_notify-validate{arg}'.format(arg=arg)
#
#    # print 'Old one {url}'.format(url=validate_url)
#    # params = request.form
#    # params = dict(request.form)
#    # params.update({'cmd': '_notify-validate'})
#    # , params={'cmd':'_notify-validate'}
#    r = requests.get('https://www.sandbox.paypal.com/cgi-bin/webscr', params=params)
#    print 'New one {url}'.format(url=r.url)
#    # r = requests.get(validate_url)
#
#    if r.text == 'VERIFIED':
#        print "PayPal transaction was verified successfully."
#        # Do something with the verified transaction details.
#        payment_status = request.form.get('payment_status', None)
#        if payment_status == 'Completed':
#            print 'custom', request.form.get('custom')
#            print 'payment_gross', request.form.get('payment_gross')
#            print 'user_id', user_id
#            # JSON.stringify(data)
#        # print "Pulled {email} from transaction".format(email=payer_email)
#    else:
#        print 'Paypal IPN string {arg} did not validate'.format(arg=arg)
#
#    return r.text

#if __name__ == "__main__":
#    app.debug = True
#    app.run(port=5001)
#
# from db import users, db

# print db.command("collstats", "users")

# print users.update({'email': 'trus19@gmail.com'}, {'$set': {'premium': True}}, upset=True)


# last_name = Romanov
# business = denis.romanow-facilitator@gmail.com
# receiver_email = denis.romanow-facilitator@gmail.com
# payer_id = 2G5926FBYWWFJ
# residence_country = US
# period3 = 1 M
# payer_status = verified
# txn_type = subscr_signup
# verify_sign = ANPGIWJTzDiYhQWIGfdX7Ovj1acdALOneemUB-a77ww-xHW-705W5.Qm
# subscr_id = I-WBT53CX0WXD5
# first_name = Denis
# payer_email = denis@hq-builder.com
# item_name = HeadQuarters 2.0 Subscription
# charset = windows-1252
# mc_amount3 = 5.00
# mc_currency = USD
# subscr_date = 15:11:09 Apr 20, 2013 PDT
# amount3 = 5.00
# notify_version = 3.7
# recurring = 1
# ipn_track_id = 129d24add5935
# test_ipn = 1
# reattempt = 1
# Old one https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_notify-validate&last_name=Romanov&business=denis.romanow-facilitator@gmail.com&receiver_email=denis.romanow-facilitator@gmail.com&payer_id=2G5926FBYWWFJ&residence_country=US&period3=1 M&payer_status=verified&txn_type=subscr_signup&verify_sign=ANPGIWJTzDiYhQWIGfdX7Ovj1acdALOneemUB-a77ww-xHW-705W5.Qm&subscr_id=I-WBT53CX0WXD5&first_name=Denis&payer_email=denis@hq-builder.com&item_name=HeadQuarters 2.0 Subscription&charset=windows-1252&mc_amount3=5.00&mc_currency=USD&subscr_date=15:11:09 Apr 20, 2013 PDT&amount3=5.00&notify_version=3.7&recurring=1&ipn_track_id=129d24add5935&test_ipn=1&reattempt=1
# PayPal transaction was verified successfully.
# Pulled denis@hq-builder.com from transaction
# 127.0.0.1 - - [21/Apr/2013 00:11:25] "POST /paypal_callback HTTP/1.0" 200 -
# protection_eligibility = Ineligible
# receiver_id = ZZELRSALZQCZQ
# last_name = Romanov
# txn_id = 0NM23254VH376422X
# business = denis.romanow-facilitator@gmail.com
# receiver_email = denis.romanow-facilitator@gmail.com
# payment_status = Completed
# payment_gross = 5.00
# mc_fee = 0.45
# payer_id = 2G5926FBYWWFJ
# residence_country = US
# payer_status = verified
# txn_type = subscr_payment
# verify_sign = AODlYbPor92Nk1v074sB3k61I8.uAeTeDA9DY-RHgPanwvayBDVI9VSP
# subscr_id = I-WBT53CX0WXD5
# payment_fee = 0.45
# payment_date = 15:11:10 Apr 20, 2013 PDT
# first_name = Denis
# item_name = HeadQuarters 2.0 Subscription
# charset = windows-1252
# mc_currency = USD
# payer_email = denis@hq-builder.com
# notify_version = 3.7
# payment_type = instant
# transaction_subject = HeadQuarters 2.0 Subscription
# mc_gross = 5.00
# ipn_track_id = 129d24add5935
# test_ipn = 1
# Old one https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_notify-validate&protection_eligibility=Ineligible&receiver_id=ZZELRSALZQCZQ&last_name=Romanov&txn_id=0NM23254VH376422X&business=denis.romanow-facilitator@gmail.com&receiver_email=denis.romanow-facilitator@gmail.com&payment_status=Completed&payment_gross=5.00&mc_fee=0.45&payer_id=2G5926FBYWWFJ&residence_country=US&payer_status=verified&txn_type=subscr_payment&verify_sign=AODlYbPor92Nk1v074sB3k61I8.uAeTeDA9DY-RHgPanwvayBDVI9VSP&subscr_id=I-WBT53CX0WXD5&payment_fee=0.45&payment_date=15:11:10 Apr 20, 2013 PDT&first_name=Denis&item_name=HeadQuarters 2.0 Subscription&charset=windows-1252&mc_currency=USD&payer_email=denis@hq-builder.com&notify_version=3.7&payment_type=instant&transaction_subject=HeadQuarters 2.0 Subscription&mc_gross=5.00&ipn_track_id=129d24add5935&test_ipn=1
# PayPal transaction was verified successfully.
# Pulled denis@hq-builder.com from transaction
# 127.0.0.1 - - [21/Apr/2013 00:12:14] "POST /paypal_callback HTTP/1.0" 200 -

# import uuid
#
# for _ in range(20):
#     print uuid.uuid4().hex

# __author__ = 'dromanov'
#
# from blog.post_list import *
# from blog.post import *
# # from blog.post import get_single_post
# # print post_list(tag='Warhammer 40k', author='Dante')
#
#
# posts.update({
#     '_id': ObjectId('5120cbebe75a9119f9dd3ef2'),
#     'comments.id': '68d36f26e7c14a2dadc5cea4a8875fe5'
# }, {
#     '$addToSet': {'comments.$.rate.pro': ObjectId('5120cbc4e75a9119f9dd38c3')}
# })
#
# print posts.find_one({
#     '_id': ObjectId('5120cbebe75a9119f9dd3ef2'),
#     'comments.id': '68d36f26e7c14a2dadc5cea4a8875fe5',
#     'comments.rate.pro': ObjectId('5120cbc4e75a9119f9dd38c3')
# }, {'comments.$': 1})
#
# # res = posts.find_one({'_id': ObjectId('5120cbebe75a9119f9dd3ef2')})
# # print res['author']
# #
# # for c in res['comments']:
# #     print c['id']
# #     print 'pro', c['rate']['pro']
# #     print 'contra', c['rate']['contra']
# # print get_posts_list(tag=None, author='Dante', page=1)
# # tags = blog.find_one()['tags']
# # tags.sort(key=lambda val: val['count'], reverse=True)
# # print tags
# # print get_single_post('511e8fe6e75a91192f2ba0e3')
#
# # from hq import tojson|safe_filter
## #
## # print tojson|safe_filter('test\"')
#import requests
#url = 'http://throne-bms.com/protest/forum/insertusertoforum'
## lang=en&login=ithilm%40gmail.com&real_name=Maria+Romanova&pass=12345
#user_params = {
#'login': 'ithilm@gmail.com',
#'pass': '12345',
#'real_name': 'Maria Romanova',
#'lang': 'en'}
#try:
#    r = requests.post(url, data=user_params)
#    print r.status_code
#    print r.text
#    #print r.raw.read()
#    print('Sent HTTP POST request: {0}. Return code: {1}.'.format(r.url, r.text, ))
#except requests.RequestException, error:
#    print(error)
