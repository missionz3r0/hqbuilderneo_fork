__author__ = 'Ivan Truskov'
__summary__ = 'Load N rosters from database and write them into file'

from config import ReleaseConfig, DebugConfig
from db.session import db
from db.models import Roster
from sqlalchemy.sql.expression import desc
import json
import sys
import getopt
import io


def print_usage():
    print('This program retrieves last changed rosters from database and dumps them to json file')
    print('Entries are taken ordered by the last change date')
    print('Json contains a list of {data: ...} entries')
    print('The program takes following parameters:')
    print('\t-h|--help: print this message and exit')
    print('\t-N <number>: how many rosters to dump')
    print('\t-o|--output-file <filename>: name of json file to dump; alternately, it can be supplied as parameter (after all options)')
    print('\t-d|--debug: use debug config')


def parse_arguments(argv):
    # default configuration options
    debug = False
    roster_number = 10
    output_name = 'roster_dump.json'
    try:
        argpairs, rest = getopt.getopt(argv, 'N:o:hd', ['output-file=', 'debug', 'help'])
        if rest and len(rest[0]):
            output_name = rest[0]
        for opt, val in argpairs:
            if opt in ('-N'):
                roster_number = val
            elif opt in ('-d', '--debug'):
                debug = True
            elif opt in ('-o', '--output-file'):
                output_name = val
            elif opt in ('-h', '--help'):
                print_usage()
                sys.exit(0)
    except getopt.GetoptError as err:
        print((str(err)))
        print_usage()
        sys.exit(1)
    return (debug, roster_number, output_name)


def save_dump(filename, roster_data):
    try:
        fstream = io.open(filename, 'wb')
        json.dump(roster_data, fstream)
        fstream.close()
    except IOError as err:
        print((str(err)))
        sys.exit(2)


def print_state(debug, number, filename):
    if debug:
        print('Using debug config')
    else:
        print('Using release config')
    print(('Dumping {} entries to {}'.format(number, filename)))


class FakeApp(object):
    from flask.config import Config
    config = Config('.', {})
    extensions = {}
    debug = True
    import_name = 'fake'

    @staticmethod
    def teardown_request(func):
        return func


if __name__ == '__main__':
    debug, roster_number, output_name = parse_arguments(sys.argv[1:])
    # testargs = '-d -N 2'.split()
    # debug, roster_number, output_name = parse_arguments(testargs)
    print_state(debug, roster_number, output_name)

    dump_data = []
    dump_number = 0

    # initialise db
    if debug:
        configobject = DebugConfig()
    else:
        configobject = ReleaseConfig()
    app = FakeApp()
    app.config.from_object(configobject)
    db.init_app(app)
    # really?..
    db.app = app
    for roster in Roster.query.order_by(desc(Roster.last_update))\
                              .limit(roster_number):
        dump_number += 1
        dump_data.append({'id': roster.id,
                          'data': roster.data})

    print(('{} rosters loaded'.format(dump_number)))
    save_dump(output_name, dump_data)
    sys.exit(0)
