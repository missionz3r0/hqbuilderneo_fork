__author__ = 'Denis Romanow'

import json
import re
from flask import Blueprint, render_template, jsonify, request, url_for, redirect
from flask.views import MethodView
from flask.ext.login import current_user, login_required
from db.models import *
from datetime import date
from social.views import SocialBlueprint


class BattlesBlueprint(Blueprint):

    def __init__(self, app):
        super(BattlesBlueprint, self).__init__('battles', __name__, url_prefix='/battles')
        self.builder = app.builder
        self.add_url_rule('/', view_func=self.Battles.as_view('list', builder=app.builder.builder))
        self.add_url_rule('/<int:user_id>', view_func=self.BattlesList.as_view('view', builder=app.builder.builder))
        self.add_url_rule(
            '/battle/<int:battle_id>',
            view_func=self.Battle.as_view('battle', builder=app.builder.builder))
        self.add_url_rule('/new', view_func=self.CreateBattle.as_view('new'))
        self.add_url_rule(
            '/_roster_info',
            view_func=self.FindRoster.as_view('roster_info', builder=app.builder.builder)
        )
        self.add_url_rule(
            '/delete_battle/<int:battle_id>',
            view_func=self.DeleteBattle.as_view('delete', builder=app.builder.builder)
        )
        self.add_url_rule(
            '/_delete_battles',
            view_func=self.DeleteBattles.as_view('delete_many', builder=app.builder.builder)
        )
        app.register_blueprint(self)

    class BuilderView(object):
        def __init__(self, builder):
            self.builder = builder
            super(BattlesBlueprint.BuilderView, self).__init__()

        def build_roster_info(self, roster_id):
            roster = Roster.query.filter_by(id=roster_id).first()
            if roster:
                header = self.builder.build_header(roster)
                return {
                    'id': roster.id,
                    'url': url_for('builder.edit_roster', roster_id=roster.id),
                    'type_name': header.army_name,
                    'game': header.game_name,
                    'name': header.name,
                    'points': header.points,
                    'limit': header.limit,
                    'description': header.description,
                    'tags': header.tags
                }

        def dump_battle(self, battle_id):
            return self.dump_battle_obj(Battle.query.filter(Battle.id == battle_id).first_or_404())

        def dump_battle_obj(self, battle, get_roster=True, format_text=False):
            res = {
                'id': battle.id,
                'result': Battle.Result.get_text(battle.result),
                'date': {
                    'day': battle.played.day,
                    'month': battle.played.month,
                    'year': battle.played.year,
                },
                'description': SocialBlueprint.comment_text(battle.description)
                if format_text and battle.description else battle.description,
                'tags': battle.data['tags'],
                'sides': [{
                    'name': v['name'],
                    'roster': get_roster and 'roster' in v and self.build_roster_info(v['roster']),
                    'roster_url': 'roster' in v and url_for('builder.edit_roster', roster_id=v['roster'])
                } for v in battle.data['sides']],
                'url': url_for('battles.battle', battle_id=battle.id)
            }
            return res

        def dump_battles(self, user_id):
            return [
                self.dump_battle_obj(b, get_roster=False, format_text=True)
                for b in Battle.query.filter_by(owner_id=user_id,
                                                state=Battle.State.PRIVATE).order_by(Battle.played.desc())
            ]

    class Battle(MethodView, BuilderView):

        def get(self, battle_id):
            return render_template("battles/battle.html", battle=self.dump_battle(battle_id))

        @staticmethod
        def post(battle_id):
            battle = Battle.query.filter(Battle.id == battle_id, Battle.owner_id == current_user.id).first()
            if not battle:
                return jsonify(error='Battle not found')
            data = json.loads(request.form.get('data'))
            # added bounds to avoid failures
            battle.played = date(year=min(9999, max(1, data['date']['year'])),
                                 day=min(31, max(1, data['date']['day'])),
                                 month=min(12, max(1, data['date']['month'])))
            battle.description = data['description']
            battle.result = Battle.Result.get_res_id(data['result'])
            battle.data = data['data']
            db.session.commit()
            return jsonify()

    class FindRoster(MethodView, BuilderView):
        regex = re.compile(r'(.*)(?<=\D)(\d+)')

        def post(self):
            data = json.loads(request.form.get('data'))
            res = self.regex.match(data['url'])
            roster = res and self.build_roster_info(int(res.groups()[1]))
            if roster:
                return jsonify(roster=roster)
            else:
                return jsonify(notfound=True)

    class CreateBattle(MethodView):
        decorators = [login_required]

        @staticmethod
        def get():
            battle = Battle(
                ow=current_user.id, result=Battle.Result.WIN, played=date.today(), description=None,
                data=dict(sides=[dict(name=current_user.name), dict(name='Opponent')], tags=[])
            )
            db.session.add(battle)
            db.session.commit()
            return redirect(url_for('battles.battle', battle_id=battle.id))

    class DeleteBattle(MethodView, BuilderView):
        decorators = [login_required]

        @staticmethod
        def _delete_list(ids):
            db.session.query(Battle).filter(Battle.id.in_(ids)).filter(Battle.owner_id == int(current_user.id)).\
                update({
                    Battle.state: Battle.State.DELETED,
                }, synchronize_session=False)
            db.session.commit()

        def get(self, battle_id):
            self._delete_list([battle_id])
            return redirect(url_for('battles.list'))

    class DeleteBattles(DeleteBattle):
        decorators = [login_required]

        def post(self):
            self._delete_list(json.loads(request.form.get('data'))['delete'])
            return jsonify(battles=self.dump_battles(current_user.id))

    class Battles(MethodView, BuilderView):
        decorators = [login_required]

        @staticmethod
        def get():
            return redirect(url_for('battles.view', user_id=current_user.get_id()))

    class BattlesList(MethodView, BuilderView):
        def get(self, user_id):
            return render_template(
                "battles/battles.html",
                battles=self.dump_battles(user_id),
                read_only=not current_user.get_id() or current_user.id != user_id
            )
