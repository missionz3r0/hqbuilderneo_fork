__author__ = 'Denis Romanov'

import json
import bleach
from flask import Blueprint, render_template, jsonify, request, url_for, redirect, current_app, abort
from flask.views import MethodView
from flask.ext.login import current_user, login_required
from flask.ext.sqlalchemy import Pagination
from sqlalchemy import func
from db.models import *
from .text import *


class SocialBlueprint(Blueprint):

    class TagList(object):

        def __init__(self):
            super(SocialBlueprint.TagList, self).__init__()
            self.tags = []

        def update(self):
            self.tags = [tag.title for tag in Tag.query.order_by(Tag.count.desc()).limit(100)]

    tags = None

    @classmethod
    def update_tags(cls):
        if not cls.tags:
            cls.tags = cls.TagList()
        cls.tags.update()

    @classmethod
    def tag_list(cls):
        if not cls.tags:
            cls.update_tags()
        return dict(autocomplete_tags=cls.tags.tags)

    def __init__(self, app):
        super(SocialBlueprint, self).__init__('social', __name__, url_prefix='/timeline')

        self.add_app_template_filter(SocialBlueprint.comment_text, name='comment_text')
        self.add_app_template_filter(SocialBlueprint.comment_anchor, name='comment_anchor')
        self.add_app_template_filter(SocialBlueprint.comment_json, name='comment_json')
        self.add_app_template_filter(SocialBlueprint.comment_json_feed, name='comment_json_feed')
        self.add_app_template_filter(SocialBlueprint.rate_text, name='rate_text')
        self.add_app_template_filter(SocialBlueprint.post_text, name='post_text')
        self.add_app_template_filter(SocialBlueprint.post_tags, name='post_tags')
        self.add_app_template_filter(SocialBlueprint.post_header, name='post_header')

        self.app_context_processor(self.tag_list)
        self.app_context_processor(self.latest_public_blog)
        self.app_context_processor(self.latest_public_rosters)

        timeline_view = self.TimeLine.as_view('timeline')
        self.add_url_rule('/', view_func=timeline_view)
        self.add_url_rule('/page/<int:page>', view_func=timeline_view)
        self.add_url_rule('/filter/<string:post_filter>', view_func=timeline_view)
        self.add_url_rule('/filter/<string:post_filter>/page/<int:page>', view_func=timeline_view)
        self.add_url_rule('/tag/<string:tag>', view_func=timeline_view)
        self.add_url_rule('/tag/<string:tag>/page/<int:page>', view_func=timeline_view)
        self.add_url_rule('/user/<string:user>', view_func=timeline_view)
        self.add_url_rule('/user/<string:user>/page/<int:page>', view_func=timeline_view)

        comment_feed_view = self.Messages.as_view('messages')
        self.add_url_rule('/messages/', view_func=comment_feed_view)
        self.add_url_rule('/messages/page/<int:page>', view_func=comment_feed_view)

        post_view = self.Post.as_view('post')
        self.add_url_rule('/post/<int:post_id>', view_func=post_view)
        self.add_url_rule('/post/<int:post_id>/comment/<int:comment_id>', view_func=post_view)

        vote_handler = self.Vote.as_view('vote')
        self.add_url_rule('/vote/<int:post_id>', view_func=vote_handler)

        self.add_url_rule('/public/<int:roster_id>',
                          view_func=self.PublicRoster.as_view('public_roster', builder=app.builder.builder))
        self.add_url_rule('/_comment_update/<int:post_id>', view_func=self.CommentUpdate.as_view('comment_update'))
        self.add_url_rule('/_post_rate/<int:post_id>', view_func=self.Post.as_view('post_rate'))
        self.add_url_rule('/_post_update', view_func=self.PublicPost.as_view('post_update'))
        self.add_url_rule('/delete_post/<int:post_id>', view_func=self.DeletePost.as_view('delete_post'))
        self.add_url_rule('/edit_post/<int:post_id>', view_func=self.EditPost.as_view('edit_post'))

        app.register_blueprint(self)

    @staticmethod
    def comment_text(text):
        p_text = '<p>{}</p>'.format('</p><p>'.join(text.splitlines()))
        return bleach.linkify(bleach.clean(p_text, tags=bleach.ALLOWED_TAGS + ['p']), parse_email=True)

    @staticmethod
    def post_header(post):
        if post.roster:
            roster = current_app.builder.builder.build_header(post.roster)
            return dict(title=roster.title, text=roster.description, read_more=False)
        else:
            head, read_more = get_read_more_head(post.text)
            return dict(title=post.title, text=SocialBlueprint.post_text(head), read_more=read_more)

    @staticmethod
    def post_text(text):
        return bleach.linkify(current_app.markdown(normalize_line_breaks(process_read_more_marker(text))),
                              parse_email=True)

    @staticmethod
    def post_tags(post):
        return [tag.title for tag in post.tags]

    @staticmethod
    def post_text_header(text):
        return SocialBlueprint.post_text(get_read_more_head(text))

    @staticmethod
    def comment_anchor(comment_id):
        return 'comment_{}'.format(comment_id)

    @staticmethod
    def comment_json(comment):
        return dict(
            id=comment.id,
            author=dict(
                name=comment.author.name,
                url=url_for('social.timeline', user=comment.author.name),
            ),
            text=SocialBlueprint.comment_text(comment.text),
            created=current_app.moment_js_utc(comment.created),
            deleted=comment.deleted,
            read_only=not current_user.is_authenticated or comment.author_id != current_user.id
        )

    @staticmethod
    def comment_json_feed(comment):
        return dict(
            id=comment.id,
            author=dict(
                name=comment.author.name,
                url=url_for('social.timeline', user=comment.author.name),
            ),
            text=SocialBlueprint.comment_text(comment.text),
            created=current_app.moment_js_utc(comment.created),
            post=dict(id="#{}".format(comment.post_id),
                      url=url_for('social.post', post_id=comment.post_id))
        )

    @staticmethod
    def latest_public_rosters():
        return dict(
            latest_public_rosters=Post.query.filter(Post.roster_id != None).order_by(Post.created.desc()).limit(6)
        )

    @staticmethod
    def latest_public_blog():
        return dict(
            latest_public_blog=Post.query.filter(Post.roster_id == None).order_by(Post.created.desc()).limit(6)
        )

    @staticmethod
    def rate_text(rate):
        if rate <= 0:
            return str(rate)
        else:
            return '+{}'.format(rate)

    class BuilderView(object):
        def __init__(self, builder):
            self.builder = builder
            super(SocialBlueprint.BuilderView, self).__init__()

    class PublicRoster(MethodView, BuilderView):
        decorators = [login_required]

        def get(self, roster_id):
            post = Post.query.filter(Post.roster_id == roster_id).first()
            if not post:
                header = self.builder.build_header(Roster.query.filter_by(id=roster_id).first_or_404())
                tag_map = {}
                for tag in db.session.query(Tag).filter(Tag.title.in_(header.tags)):
                    tag_map[tag.title] = tag
                if len(tag_map) != len(header.tags):
                    for tag in header.tags:
                        if tag not in tag_map:
                            tag_object = Tag(tag)
                            db.session.add(tag_object)
                            tag_map[tag.title] = tag_object
                    db.session.commit()
                post = Post(author=current_user.id, roster=roster_id)
                post.tags.extend(iter(tag_map.values()))
                db.session.add(post)
                db.session.query(Tag).filter(Tag.title.in_(header.tags)).\
                    update({Tag.count: Tag.count + 1}, synchronize_session=False)
                db.session.commit()
                SocialBlueprint.update_tags()
            return redirect(url_for('social.post', post_id=post.id))

    class TimeLine(MethodView):

        @staticmethod
        def paginate(query, page, per_page=20, error_out=False):
            # Workaround, BaseQuery.paginate from Flask-sqlalchemy does not work with relationship
            if error_out and page < 1:
                abort(404)
            if query:
                items = query.limit(per_page).offset((page - 1) * per_page).all()
                if not items and page != 1 and error_out:
                    abort(404)

                # No need to count if we're on the first page and there are fewer
                # items than we expected.
                if page == 1 and len(items) < per_page:
                    total = len(items)
                else:
                    total = query.order_by(None).count()
            else:
                items = []
                total = 0
            return Pagination(query, page, per_page, total, items)

        def get(self, tag=None, user=None, post_filter='', page=1):
            """
            May return these different views:
            1) all posts, if no tag, user or post filter are supplied
            2) posts restricted to blog posts or to roster posts, with post_filter equal to 'roster' or 'blog'
            3) posts containing a given tag
            4) posts by a certain user
            All those views support pagination
            """
            try:
                if tag:
                    records = Tag.query.filter(Tag.title == tag).first().posts
                elif user:
                    records = User.query.filter(User.name == user).first().posts
                else:
                    if post_filter == 'roster':
                        records = Post.query.filter(Post.roster_id != None).order_by(Post.created.desc())
                    elif post_filter == 'blog':
                        records = Post.query.filter(Post.roster_id == None).order_by(Post.created.desc())
                    else:
                        records = Post.query.order_by(Post.created.desc())
                return render_template(
                    "social/list.html",
                    records=self.paginate(records, page),
                    tag=tag, user=user
                )
            except AttributeError:
                # presumably query returned zero results
                return render_template(
                    "social/list.html",
                    records=self.paginate(None, page),
                    tag=tag, user=user
                )
            except Exception:
                abort(404)

    class Messages(MethodView):
        @staticmethod
        def paginate(query, page, per_page=20):
            if page < 1:
                abort(404)
            if query:
                items = query.limit(per_page).offset((page - 1) * per_page).all()
                if not items and page != 1:
                    abort(404)

                # No need to count if we're on the first page and there are fewer
                # items than we expected.
                if page == 1 and len(items) < per_page:
                    total = len(items)
                else:
                    total = query.order_by(None).count()
            else:
                items = []
                total = 0
            return Pagination(query, page, per_page, total, items)

        def get(self, page=1):
            """
            Return page with comments that were made to posts user is interested in
            """
            try:
                if current_user.is_authenticated:
                    feed_query = Comment.find_interesting_comments(current_user.id)
                    return render_template(
                        "social/comment_feed.html",
                        comments=self.paginate(feed_query, page)
                    )
                else:
                    return redirect(url_for('builder.main'))
            except Exception:
                abort(404)

    class CommentUpdate(MethodView):

        @staticmethod
        def post(post_id):
            if not current_user.is_authenticated:
                return jsonify(redirect=url_for('social.post', post_id=post_id))
            data = json.loads(request.form.get('data'))
            if 'preview' in data:
                return jsonify(text=SocialBlueprint.comment_text(data['preview']))
            if 'send' in data:
                comment = Comment(author=current_user.id, post=post_id, text=data.get('send'))
                db.session.add(comment)
                db.session.query(Post).filter(Post.id == post_id).\
                    update({Post.comments_count: Post.comments_count + 1}, synchronize_session=False)
                db.session.commit()
                return jsonify(comments=[
                    SocialBlueprint.comment_json(c)
                    for c in Comment.query.filter(Comment.post_id == post_id).order_by(Comment.created)
                    .offset(data.get('comments'))
                ])
            if 'delete' in data:
                db.session.query(Comment).filter(Comment.id == data.get('delete'),
                                                 Comment.author_id == current_user.id).\
                    update({Comment.deleted: True}, synchronize_session=False)
                db.session.commit()
                return jsonify(deleted=True)
            if 'restore' in data:
                comment_id = data.get('restore')
                db.session.query(Comment).filter(Comment.id == comment_id,
                                                 Comment.author_id == current_user.id).\
                    update({Comment.deleted: False}, synchronize_session=False)
                db.session.commit()
                return jsonify(
                    text=SocialBlueprint.comment_text(Comment.query.filter(Comment.id == comment_id).first().text)
                )
            if 'raw_text' in data:
                return jsonify(text=Comment.query.filter(Comment.id == data.get('raw_text')).first().text)
            if 'edit' in data:
                db.session.query(Comment).filter(Comment.id == data.get('edit'),
                                                 Comment.author_id == current_user.id).\
                    update({Comment.text: data.get('text')}, synchronize_session=False)
                db.session.commit()
                return jsonify(text=SocialBlueprint.comment_text(data.get('text')))

    class Post(MethodView):

        @staticmethod
        def post(post_id):
            if not current_user.is_authenticated:
                return jsonify(redirect=url_for('social.post', post_id=post_id))
            data = json.loads(request.form.get('data'))
            if 'rate' in data:
                rate = Rate.query.filter(Rate.post_id == post_id, Rate.user_id == current_user.id).first()
                value = data.get('rate')
                plus = minus = False
                if rate and rate.value == value:
                    db.session.delete(rate)
                else:
                    if rate and rate.value != value:
                        rate.value = value
                    else:
                        db.session.add(Rate(user=current_user.id, post=post_id, value=value))
                    plus = value > 0
                    minus = value < 0
                db.session.commit()
                rate_count = db.session.query(func.sum(Rate.value).label('sum')).filter(Rate.post_id == post_id)\
                    .scalar()
                if rate_count is None:
                    rate_count = 0
                db.session.query(Post).filter(Post.id == post_id).update({Post.rate: rate_count},
                                                                         synchronize_session=False)
                db.session.commit()
                return jsonify(plus=plus, minus=minus, value=rate_count)

        @staticmethod
        def get_poll(post):
            if len(post.poll) == 0:
                return None
            poll = post.poll[0]
            if len(poll.variants) == 0:
                return None
            return poll

        @staticmethod
        def get(post_id, comment_id=None):
            post = Post.query.filter(Post.id == post_id).first_or_404()

            if current_user.is_authenticated and post.author_id != current_user.id:
                rate = Rate.query.filter(Rate.post_id == post_id, Rate.user_id == current_user.id).first()
                rate_data = dict(
                    plus=rate and rate.value > 0,
                    minus=rate and rate.value < 0
                )
            else:
                rate_data = None
            poll = SocialBlueprint.Post.get_poll(post)

            result_flag = False
            if poll is not None:
                if 'result' in request.args:
                    result_flag = request.args['result']
                else:
                    if not current_user.is_authenticated:
                        result_flag = True
                    else:
                        result_flag = PollVote.query.filter(PollVote.user == current_user.get_id(), PollVote.poll == poll.id).count() > 0
            poll_results = None
            if result_flag and poll is not None:
                poll_results = dict()
                poll_results['total'] = 0
                poll_results['variants'] = []
                for variant in poll.variants:
                    poll_results['variants'].append(dict(
                        id=variant.id,
                        text=variant.text,
                        votes=variant.hits,
                        percentage='0%'
                    ))
                    poll_results['total'] += variant.hits
                if poll_results['total'] > 0:
                    for variant in poll_results['variants']:
                        variant['percentage'] = "{:.1%}".format(
                            float(variant['votes']) / poll_results['total']
                        )
            return render_template("social/post.html", post=post,
                                   comment_id=comment_id, rate=rate_data,
                                   poll=poll, result=poll_results)

    class Vote(MethodView):

        @staticmethod
        def post(post_id):
            variant_id = request.form['variantRadios']
            variant = PollVariant.query.get(variant_id)
            if variant is not None and current_user.id is not None:
                vote = PollVote(current_user.id, variant.poll_id, variant.id)
                db.session.add(vote)
                variant.make_vote()
                db.session.commit()
            return redirect(url_for('social.post', post_id=post_id) + '?result=true')

    class DeletePost(MethodView):

        @staticmethod
        def get(post_id):
            post = Post.query.filter(Post.id == post_id, Post.author_id == current_user.id).first_or_404()
            db.session.query(Tag).filter(Tag.id.in_(tag.id for tag in post.tags)).\
                update({Tag.count: Tag.count - 1}, synchronize_session=False)
            post.tags[:] = []
            db.session.delete(post)
            db.session.commit()
            SocialBlueprint.update_tags()
            return redirect(url_for('social.timeline'))

    class EditPost(MethodView):
        decorators = [login_required]

        @staticmethod
        def post(post_id):
            data = json.loads(request.form.get('data'))
            if 'preview' in data:
                return jsonify(text=SocialBlueprint.post_text(data['preview']))
            if 'send' in data:
                tags = data['send']['tags']
                text = data['send']['text']
                title = data['send']['title']
                poll_data = data['send']['poll']
                post = Post.query.filter(Post.id == post_id, Post.author_id == current_user.id).first()
                if not post:
                    return redirect(url_for('social.timeline'))

                # TODO merge with public roster
                tag_map = {}
                for tag in db.session.query(Tag).filter(Tag.title.in_(tags)):
                    tag_map[tag.title] = tag
                if len(tag_map) != len(tags):
                    for tag in tags:
                        if tag not in tag_map:
                            tag_object = Tag(tag)
                            db.session.add(tag_object)
                            tag_map[tag.title] = tag_object
                    db.session.commit()
                # post = Post(author=current_user.id, text=text, title=title if title else None)
                post.text = text
                post.title = title
                old_tags = SocialBlueprint.post_tags(post)
                post.tags[:] = iter(tag_map.values())
                # db.session.add(post)
                db.session.query(Tag).filter(Tag.title.in_(tags)).\
                    update({Tag.count: Tag.count + 1}, synchronize_session=False)
                db.session.query(Tag).filter(Tag.title.in_(old_tags)).\
                    update({Tag.count: Tag.count - 1}, synchronize_session=False)

                SocialBlueprint.update_tags()
                old_poll = SocialBlueprint.Post.get_poll(post)

                if poll_data:
                    if len(poll_data['variants']) > 0:
                        if old_poll is None:
                            new_poll = Poll(post.id)
                            db.session.add(new_poll)
                            for variant in poll_data['variants']:
                                new_variant = PollVariant(variant['text'], new_poll)
                                db.session.add(new_variant)
                        else:
                            varlist = old_poll.variants[:]
                            for variant in poll_data['variants']:
                                if len(str(variant['id'])) == 0:
                                    new_variant = PollVariant(variant['text'], old_poll)
                                    db.session.add(new_variant)
                                else:
                                    old_variant = next(varobj for varobj in varlist if varobj.id == variant['id'])
                                    old_variant.text = variant['text']
                                    varlist.remove(old_variant)
                            for old_variant in varlist:
                                PollVote.query.filter_by(variant=old_variant.id).delete()
                                PollVariant.query.filter_by(id=old_variant.id).delete()
                    else:
                        post.poll[:] = []
                else:
                    if old_poll is not None:
                        post.poll[:] = []

                # import pdb; pdb.set_trace()
                db.session.commit()
                return jsonify(redirect=url_for('social.post', post_id=post.id))

        @staticmethod
        def make_poll_data(poll):
            if poll is None:
                return None
            result = {
                'id': poll.id,
                'variants': [
                    {'id': variant.id, 'text': variant.text} for variant in poll.variants
                ]
            }
            return result

        @staticmethod
        def get(post_id):
            post = Post.query.filter(Post.id == post_id, Post.author_id == current_user.id).first_or_404()
            if post.roster_id:
                return redirect(url_for('builder.edit_roster', roster_id=post.roster_id))
            poll = SocialBlueprint.EditPost.make_poll_data(SocialBlueprint.Post.get_poll(post))
            return render_template("social/edit_post.html", post=post, poll=poll)

    class PublicPost(MethodView):

        @staticmethod
        def post():
            if not current_user.is_authenticated:
                return jsonify(redirect=url_for('social.timeline'))
            data = json.loads(request.form.get('data'))
            if 'preview' in data:
                return jsonify(text=SocialBlueprint.post_text(data['preview']))
            if 'send' in data:
                tags = data['send']['tags']
                text = data['send']['text']
                title = data['send']['title']
                poll = data['send']['poll']

                # TODO merge with public roster
                tag_map = {}
                for tag in db.session.query(Tag).filter(Tag.title.in_(tags)):
                    tag_map[tag.title] = tag
                if len(tag_map) != len(tags):
                    for tag in tags:
                        if tag not in tag_map:
                            tag_object = Tag(tag)
                            db.session.add(tag_object)
                            tag_map[tag.title] = tag_object
                    db.session.commit()
                post = Post(author=current_user.id, text=text, title=title if title else None)
                post.tags.extend(iter(tag_map.values()))
                db.session.add(post)
                db.session.query(Tag).filter(Tag.title.in_(tags)).\
                    update({Tag.count: Tag.count + 1}, synchronize_session=False)
                db.session.commit()
                if poll is not None and len(poll['variants']) > 0:
                    # import pdb; pdb.set_trace()
                    new_poll = Poll(post.id)
                    db.session.add(new_poll)
                    for variant in poll['variants']:
                        new_variant = PollVariant(variant['text'], new_poll)
                        db.session.add(new_variant)

                    db.session.commit()
                SocialBlueprint.update_tags()
                return jsonify(redirect=url_for('social.post', post_id=post.id))
