from functools import lru_cache

from .session import db
from sqlalchemy import Column, Integer, String, Boolean, DateTime, Index,\
    ForeignKey, Text, Table, Date, DateTime, or_, and_, select, union
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSON
from datetime import datetime


class User(db.Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(120), unique=True)
    email = Column(String(120), unique=True, index=True)
    password = Column(String(90))
    active = Column(Boolean, default=True)
    admin = Column(Boolean, default=False)
    recovery = Column(String(32), nullable=True, default=None)
    created = Column(DateTime(), default=datetime.utcnow)
    legacy_key = Column(String(24), nullable=True, default=None, index=True)

    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = password

    def __repr__(self):
        return '<User %r>' % self.name


class Roster(db.Model):
    __tablename__ = 'rosters'

    class State(object):
        PRIVATE = 1
        DELETED = 2

    id = Column(Integer, primary_key=True)
    owner_id = Column(Integer, ForeignKey('users.id'), index=True)
    data = Column(JSON)
    state = Column(Integer, default=State.PRIVATE, index=True)
    created = Column(DateTime, default=datetime.utcnow)
    last_update = Column(DateTime, default=datetime.utcnow)
    legacy_key = Column(String(24), nullable=True, default=None, index=True)

    def __init__(self, ow, data):
        self.owner_id = ow
        self.data = data


class Battle(db.Model):
    __tablename__ = 'battles'

    class State(object):
        PRIVATE, DELETED = list(range(1, 3))

    class Result(object):
        WIN, DRAW, LOSE = list(range(1, 4))
        text2id = (
            (WIN, 'Win'),
            (DRAW, 'Draw'),
            (LOSE, 'Lose')
        )

        @classmethod
        def _get(cls, k, v, val):
            for pair in cls.text2id:
                if pair[k] == val:
                    return pair[v]

        @classmethod
        def get_text(cls, res_id):
            return cls._get(0, 1, res_id)

        @classmethod
        def get_res_id(cls, text):
            return cls._get(1, 0, text)

    id = Column(Integer, primary_key=True)
    owner_id = Column(Integer, ForeignKey('users.id'), index=True)
    state = Column(Integer, default=State.PRIVATE, index=True)
    created = Column(DateTime, default=datetime.utcnow)
    result = Column(Integer, index=True)
    played = Column(Date)
    description = Column(Text, nullable=True)
    data = Column(JSON)

    def __init__(self, ow, result, played, description, data):
        self.owner_id = ow
        self.result = result
        self.played = played
        self.description = description
        self.data = data


tags_association_table = Table(
    'tags_association', db.Model.metadata,
    Column('post_id', Integer, ForeignKey('posts.id')),
    Column('tag_id', Integer, ForeignKey('tags.id'))
)


class Comment(db.Model):
    __tablename__ = 'comments'

    id = Column(Integer, primary_key=True)
    author_id = Column(Integer, ForeignKey('users.id'), index=True)
    author = relationship('User', backref='comments', lazy=False)
    post_id = Column(Integer, ForeignKey('posts.id'), index=True)
    created = Column(DateTime, default=datetime.utcnow)
    text = Column(Text)
    deleted = Column(Boolean, default=False)
    parent_id = Column(Integer, ForeignKey('comments.id'), nullable=True, default=None)

    def __init__(self, author, post, text):
        self.author_id = author
        self.post_id = post
        self.text = text

    @staticmethod
    def find_interesting_comments(author):
        """Make a query that contains comments to post of user or to posts he commented"""
        # first, find posts of interest
        # posts made by user
        own_posts = db.session.query(Post.id).filter(Post.author_id == author)
        # posts user has commented
        commented_posts = db.session.query(Comment.post_id).filter(Comment.author_id == author).distinct()
        return Comment.query.filter(Comment.post_id.in_(union(own_posts, commented_posts))).\
            order_by(Comment.created.desc())


class Post(db.Model):
    __tablename__ = 'posts'

    id = Column(Integer, primary_key=True)
    author_id = Column(Integer, ForeignKey('users.id'), index=True)
    roster_id = Column(Integer, ForeignKey('rosters.id'), nullable=True)
    created = Column(DateTime, default=datetime.utcnow)
    comments_count = Column(Integer, default=0)
    rate = Column(Integer, default=0)
    title = Column(String(256), nullable=True)
    text = Column(Text, nullable=True)

    roster = relationship('Roster', backref='posts')
    author = relationship('User', backref=backref('posts', order_by=created.desc(), lazy='dynamic'), lazy=False)
    tags = relationship(
        'Tag',
        secondary=tags_association_table,
        single_parent=True,
        backref=backref('posts', order_by=created.desc(), lazy='dynamic')
    )
    comments = relationship('Comment', backref='post', order_by=Comment.created, cascade='all, delete, delete-orphan')
    rates = relationship('Rate', backref='post', cascade='all, delete, delete-orphan')
    poll = relationship('Poll', backref='post', cascade='all, delete, delete-orphan')

    def __init__(self, author, roster=None, title=None, text=None):
        self.author_id = author
        self.roster_id = roster
        self.title = title
        self.text = text


class Tag(db.Model):
    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)
    title = Column(String(64), unique=True)
    count = Column(Integer, default=0)

    def __init__(self, title):
        self.title = title


class Rate(db.Model):
    __tablename__ = 'rates'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), index=True)
    post_id = Column(Integer, ForeignKey('posts.id'), index=True)
    value = Column(Integer)

    # post = relationship('Post', backref='rates')

    def __init__(self, user, post, value):
        self.user_id = user
        self.post_id = post
        self.value = value


class Poll(db.Model):
    __tablename__ = 'polls'

    id = Column(Integer, primary_key=True)
    post_id = Column(Integer, ForeignKey('posts.id'), name='post', index=True)
    limit = Column(Integer)
    deadline = Column(DateTime)
    variants = relationship('PollVariant', backref='poll', cascade='all, delete, delete-orphan')

    def __init__(self, post):
        "actial values for limit and deadline will be used/added later, if ever"
        self.limit = 0
        self.deadline = datetime.min
        self.post_id = post


class PollVariant(db.Model):
    __tablename__ = 'pollvariants'

    id = Column(Integer, primary_key=True)
    poll_id = Column(Integer, ForeignKey('polls.id'), name='poll', index=True)
    text = Column(String(128))
    hits = Column(Integer, default=0)
    votes = relationship('PollVote', cascade='all, delete, delete-orphan')

    def __init__(self, text, poll):
        self.text = text
        self.hits = 0
        self.poll = poll

    def make_vote(self):
        self.hits += 1


class PollVote(db.Model):
    __tablename__ = 'pollvotes'

    poll = Column(Integer, ForeignKey('polls.id'), index=True, primary_key=True)
    variant = Column(Integer, ForeignKey('pollvariants.id'), nullable=True, default=None, index=True)
    user = Column(Integer, ForeignKey('users.id'), primary_key=True)

    def __init__(self, user, poll, variant):
        self.user = user
        self.poll = poll
        self.variant = variant


class Lookup(db.Model):
    __tablename__ = 'lookup'

    id = Column('id', Integer(), nullable=False, primary_key=True)
    faction = Column('faction', String(64), nullable=False)
    name = Column('name', String(64), nullable=False)
    alias = Column('alias', String(64))
    link = Column('link', String(256), nullable=False)

    @staticmethod
    @lru_cache(1024)
    def find_link(faction, name):
        FAC = faction.lower()
        NAME = name.lower()
        query = Lookup.query.from_self(Lookup.link).filter(Lookup.faction == FAC)\
                                                   .filter(or_(Lookup.name == NAME,
                                                               Lookup.alias == NAME))
        return query.first()
