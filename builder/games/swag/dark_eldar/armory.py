__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Plasma grenades', 25, var_dicts=[
            {'name': 'Weapon reload', 'points': 13}
        ])


class H2H(OptionsList):
    def __init__(self, parent, syren=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Wych knife", 5)
        self.variant("Chainhook", 10)
        if syren:
            self.variant("Agonizer", 45)
            self.variant("Power sword", 50)


class Pistols(OptionsList):
    def __init__(self, parent, syren=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.variants("Splinter pistol", 20, var_dicts=[
            {'name': 'Soul-seeker rounds', 'points': 15},
            {'name': 'Weapon reload', 'points': 10}
        ])
        if syren:
            self.variants("Blast pistol", 50, var_dicts=[
                {'name': 'Weapon reload', 'points': 25}
            ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Blade venom', 10)
        self.variant('Mirrorhelm', 20)


class Gladiatorial(OptionsList):
    def __init__(self, parent):
        super(Gladiatorial, self).__init__(parent, 'Gladiatorial Weapons', limit=1)
        self.variant('Hydra Gauntlets', 30)
        self.variant('Razorflail', 30)
        self.variant('Shardnet and impaler', 35, gear=[Gear('Shardnet'),
                                                       Gear('Impaler')])
