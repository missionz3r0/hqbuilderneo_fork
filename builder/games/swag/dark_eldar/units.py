__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Syren(Unit):
    type_name = 'Syren'
    type_id = 'de_syren_v1'

    def __init__(self, parent):
        super(Syren, self).__init__(parent, points=150, gear=[
            Gear('Wych knife'), Gear('Wychsuit')
        ])
        self.hth = H2H(self, syren=True)
        self.glad = Gladiatorial(self)
        self.pist = Pistols(self, syren=True)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)

    def check_rules(self):
        super(Syren, self).check_rules()
        self.hth.used = self.hth.visible = self.pist.used = self.pist.visible = not self.glad.any


class Wych(Unit):
    type_name = 'Wych'
    type_id = 'de_wych_v1'

    def __init__(self, parent):
        super(Wych, self).__init__(parent, points=80, gear=[
            Gear('Wych knife'), Gear('Wychsuit')])
        H2H(self)
        Pistols(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Debutante(Unit):
    type_name = 'Debutante'
    type_id = 'de_debutante_v1'

    def __init__(self, parent):
        super(Debutante, self).__init__(parent, points=70, gear=[
            Gear('Wych knife'), Gear('Wychsuit')])
        H2H(self)
        Pistols(self)
        Grenades(self)
        Misc(self)


class Bloodbride(Unit):
    type_name = 'Bloodbride'
    type_id = 'de_bride_v1'

    def __init__(self, parent):
        super(Bloodbride, self).__init__(parent, points=100, gear=[
            Gear('Wych knife'), Gear('Wychsuit')])
        Gladiatorial(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)
