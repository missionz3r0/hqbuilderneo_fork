__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Frag grenades', 25, var_dicts=[
            {'name': 'Weapon reload', 'points': 13}
        ])
        self.variants('Melta bombs', 30, var_dicts=[
            {'name': 'Weapon reload', 'points': 15}
        ])
        self.variants('Rad grenades', 35, var_dicts=[
            {'name': 'Weapon reload', 'points': 18}
        ])
        self.variants('Krak grenades', 40, var_dicts=[
            {'name': 'Weapon reload', 'points': 20}
        ])


class H2H(OptionsList):
    def __init__(self, parent, inquisitor=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        self.variant("Chainsword", 25)
        if inquisitor:
            self.variant("Scythian venom talon", 30)
        self.variant('Power maul', 50)
        self.variant('Power sword', 50)


class Pistols(OptionsList):
    def __init__(self, parent, inquisitor=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.variants("Autopistol", 15, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 8}
        ])
        self.lp = self.variants("Laspistol", 15, var_dicts=[
            {'name': 'Hotshot laser power pack', 'points': 15},
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 8}
        ])
        self.variants("Bolt pistol", 25, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 13}
        ])
        if inquisitor:
            self.variants("Needle pistol", 30, var_dicts=[
                {'name': 'Toxic rounds', 'points': 20},
                {'name': 'Red-dot laser sight', 'points': 20},
                {'name': 'Weapon reload', 'points': 15}
            ])
        self.variants("Plasma pistol", 50, var_dicts=[
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Weapon reload', 'points':25}
        ])


class Basic(OptionsList):
    def __init__(self, parent):
        super(Basic, self).__init__(parent, 'Basic weapons')
        self.variants("Autogun", 20, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 10}
        ])
        self.variants("Shotgun", 20, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 10}
        ])
        self.variants("Lasgun", 25, var_dicts=[
            {'name': 'Hotshot laser power pack', 'points': 15},
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 13}
        ])
        self.variants("Boltgun", 35, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 18}
        ])
        self.variants("Sniper rifle", 40, var_dicts=[
            {'name': 'Toxic rounds', 'points': 20},
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 20}
        ])


class Misc(OptionsList):
    def __init__(self, parent, inquisitor=False, crusader=False):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Camo gear', 5)
        self.variant('Clip harness', 10)
        if inquisitor:
            self.variant('Digital weapons', 10)
            self.variant('Power armour', 10)
            self.variant('Ulumeathi plasma syphon', 10)
        self.variant('Photo visor', 15)
        if inquisitor or crusader:
            self.variant('Storm shield', 50)


class Special(OptionsList):
    def __init__(self, parent, inquisitor=False):
        super(Special, self).__init__(parent, 'Special Weapons')
        if inquisitor:
            self.variants("Combi-flamer", 55, var_dicts=[
                {'name': 'Red-dot laser sight', 'points': 20},
                {'name': 'Telescopic sight', 'points': 20},
                {'name': 'Weapon reload', 'points': 28}
            ])
        self.variants("Storm bolter", 55, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 28}
        ])
        if inquisitor:
            self.variants("Combi-melta", 65, var_dicts=[
                {'name': 'Red-dot laser sight', 'points': 20},
                {'name': 'Telescopic sight', 'points': 20},
                {'name': 'Weapon reload', 'points': 33}
            ])
