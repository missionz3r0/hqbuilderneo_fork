__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Inquisitor, Acolyte, Initiate, Crusader


class InqLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(InqLeader, self).__init__(*args, **kwargs)
        UnitType(self, Inquisitor)


class InqTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(InqTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Acolyte)


class InqNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(InqNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Initiate)


class InqSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(InqSpecialists, self).__init__(*args, max_limit=3, **kwargs)
        UnitType(self, Crusader)


class InquisitionKillTeam(SWAGRoster):
    army_name = 'Inquisition Kill Team'
    army_id = 'inquisition_swag_v1'

    def __init__(self):
        super(InquisitionKillTeam, self).__init__(
            InqLeader, InqTroopers, InqSpecialists, InqNewRecruits)
