__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Biomorphs(OptionsList):
    def __init__(self, parent):
        super(Biomorphs, self).__init__(parent, 'Boimorphs')
        self.variant('Adrenal glands', 10)
        self.variant('Toxin sacs', 10)
        self.variant('Extended chitin caparace', 15)
        self.variant('Flesh hooks', 15)
        self.variant('Acid blood', 20)


class H2H(OptionsList):
    def __init__(self, parent):
        super(H2H, self).__init__(parent, 'Hand-to-Hand Bio-weapons', limit=1)
        self.variant('Pair of scything talons', 10, gear=[Gear('Scything talon', count=2)])
        self.variant('Pair of rending claws', 25, gear=[Gear('Rending claw', count=2)])
        self.variant('Lash whip and bonesword', 100, gear=[Gear('Lash whip'), Gear('Bonesword')])
        self.variant('Pair of boneswords', 125, gear=[Gear('Bonesword', count=2)])


class Basic(OptionsList):
    def __init__(self, parent):
        super(Basic, self).__init__(parent, 'Basic Bio-weapons', limit=1)
        self.variant('Pair of spinefists', 25, gear=[Gear('Spinefist', count=2)])
        self.variant('Deathspitter', 45)


class Cannons(OptionsList):
    def __init__(self, parent):
        super(Cannons, self).__init__(parent, 'Bio-cannons', limit=1)
        self.variant('Barbed strangler', 150)
        self.variant('Venom cannon', 165)
