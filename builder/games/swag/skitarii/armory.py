__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent, alpha=False):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Frag grenades', 25, var_dicts=[
            {'name': 'Weapon reload', 'points': 13}
        ])
        if alpha:
            self.variants('Melta bombs', 30, var_dicts=[
                {'name': 'Weapon reload', 'points': 15}
            ])
        self.variants('Krak grenades', 40, var_dicts=[
            {'name': 'Weapon reload', 'points': 20}
        ])


class H2H(OptionsList):
    def __init__(self, parent, alpha=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        self.variant("Taser goad", 45)
        if alpha:
            self.variant('Arc maul', 50)
            self.variant("Power sword", 50)


class Pistols(OptionsList):
    def __init__(self, parent, alpha=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.variants("Radium pistol", 20, var_dicts=[
            {'name': 'Weapon reload', 'points': 10}
        ])
        self.variants("Arc pistol", 30, var_dicts=[
            {'name': 'Weapon reload', 'points': 11}
        ])
        if alpha:
            self.variants("Phosphor blast pistol", 40, var_dicts=[
                {'name': 'Weapon reload', 'points': 20}
            ])


class Basic(OptionsList):
    def __init__(self, parent):
        super(Basic, self).__init__(parent, 'Basic weapons')
        self.variants("Galvanic rifle", 35, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 18}
        ])
        self.variants("Radium carbine", 35, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 18}
        ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Clip harness', 10)
        self.variant('Photo-visor', 15)
        self.variant('Enchanced data-tether', 35)
        self.variant('Omnispex', 40)


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Special Weapons')
        self.variants('Plasma caliver', 75, var_dicts=[{'name': 'Weapon reload', 'points': 38}])
        self.variants('Arc rifle', 40, var_dicts=[{'name': 'Weapon reload', 'points': 20}])
        self.variants('Transuranic arquebuse', 180,
                      var_dicts=[{'name': 'Red-dot laser sight', 'points': 20},
                                 {'name': 'Telescopic sight', 'points': 20},
                                 {'name': 'Weapon reload', 'points': 90}])
