__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Alpha(Unit):
    type_name = 'Skitarii Ranger Alpha'
    type_id = 'sk_alpha_v1'

    def __init__(self, parent):
        super(Alpha, self).__init__(parent, points=150, gear=[
            Gear('Combat blade'), Gear('Skitarii war plate')
        ])
        H2H(self, alpha=True)
        Pistols(self, alpha=True)
        Basic(self)
        Grenades(self, alpha=True)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Ranger(Unit):
    type_name = 'Skitaroo Ranger'
    type_id = 'sk_ranger_v1'

    def __init__(self, parent):
        super(Ranger, self).__init__(parent, points=80, gear=[
            Gear('Combat blade'), Gear('Skitarii war plate')])
        Basic(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Fresh(Unit):
    type_name = 'Skitarii Fresh-forged'
    type_id = 'sk_fresh_v1'

    def __init__(self, parent):
        super(Fresh, self).__init__(parent, points=65, gear=[
            Gear('Combat blade'), Gear('Skitarii war plate')])
        Basic(self)
        Grenades(self)
        Misc(self)


class Specialist(Unit):
    type_name = 'Skitarii Specialist'
    type_id = 'sk_specialist_v1'

    def __init__(self, parent):
        super(Specialist, self).__init__(parent, points=90, gear=[
            Gear('Combat blade'), Gear('Skitarii war plate')])
        Pistols(self)
        Special(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)
