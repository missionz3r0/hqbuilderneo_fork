__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Justicar(Unit):
    type_name = 'Justicar'
    type_id = 'gk_justicar_v1'

    def __init__(self, parent):
        super(Justicar, self).__init__(parent, points=250, gear=[
            Gear('Power armour')
        ])
        Basic(self)
        self.hth = H2H(self)
        Grenades(self, justicar=True)
        Misc(self)
        Characteristics(self)
        Skills(self)

    def check_rules(self):
        super(Justicar, self).check_rules()
        if not self.hth.any:
            self.error('Justicar must be armed with one Hand-to-Hand weapon')


class Knight(Unit):
    type_name = 'Grey Knight'
    type_id = 'gk_knight_v1'

    def __init__(self, parent):
        super(Knight, self).__init__(parent, points=175, gear=[
            Gear('Power armour')])
        Basic(self)
        self.hth = H2H(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)

    def check_rules(self):
        super(Knight, self).check_rules()
        if not self.hth.any:
            self.error('Grey Knight must be armed with one Hand-to-Hand weapon')


class Gunner(Unit):
    type_name = 'Grey Knight Gunner'
    type_id = 'gk_gunner_v1'

    def __init__(self, parent):
        super(Gunner, self).__init__(parent, points=200, gear=[
            Gear('Power armour')])
        self.rng = Special(self)
        self.hth = H2H(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)

    def check_rules(self):
        super(Gunner, self).check_rules()
        if not any(opt.value for opt in self.rng.weapons):
            self.error("Grey Knight Gunner must carry a ranged weapon")
        else:
            self.hth.used = self.hth.visible = self.rng.has_bolter()
            if self.rng.has_bolter() and not self.hth.any:
                self.error('Grey Knight Gunner with storm bolter must be armed with one Hand-to-Hand weapon')
