__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent, justicar=False):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Frag grenades', 25, var_dicts=[
            {'name':'Weapon reload', 'points':13}
        ])
        if justicar:
            self.variants('Melta bombs', 30, var_dicts=[
                {'name':'Weapon reload', 'points':15}
            ])
        self.variants('Krak grenades', 40, var_dicts=[
            {'name':'Weapon reload', 'points':20}
        ])


class H2H(OptionsList):
    def __init__(self, parent):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons", limit=1)
        self.variant("Pair of Nemesis falchions", 30, gear=[Gear('Nemesis falchion', count=2)])
        self.variant("Nemesis force sword", 60)
        self.variant("Nemesis force halberd", 80)
        self.variant("Nemesis Daemon hammer", 100)
        self.variant("Nemesis warding stave", 25)


class Basic(OptionsList):
    def __init__(self, parent):
        super(Basic, self).__init__(parent, '')
        self.sb = self.variants("Storm bolter", 0, var_dicts=[
            {'name': 'Psybolts', 'points': 30},
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 25}
        ])
        self.sb.value = True
        self.sb.active = False


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Special weapons')
        self.sb = self.variants("Storm bolter", 0, var_dicts=[
            {'name': 'Psybolts', 'points': 30},
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 25}
        ])
        inc = self.variants("Incinerator", 75, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 38}
        ])
        psl = self.variants("Psilencer", 150, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 75}
        ])
        psc = self.variants("Psycannon", 175, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 88}
        ])
        self.weapons = [self.sb, inc, psl, psc]
        self.sb.value = True

    def check_rules(self):
        super(Special, self).check_rules()
        OptionsList.process_limit(self.weapons, 1)

    def has_bolter(self):
        return self.sb.value


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Clip harness', 10)
        self.variant('Photo-visor', 15)

