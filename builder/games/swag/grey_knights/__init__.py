__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    SWAGRoster
from .units import Justicar, Knight, Gunner


class GKLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(GKLeader, self).__init__(*args, **kwargs)
        UnitType(self, Justicar)


class GKTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(GKTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Knight)


class GKSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(GKSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Gunner)


class GreyKnightsKillTeam(SWAGRoster):
    army_name = 'Grey Knights Kill Team'
    army_id = 'grey_knights_swag_v1'

    def __init__(self):
        super(GreyKnightsKillTeam, self).__init__(
            GKLeader, GKTroopers, GKSpecialists, None, max_limit=5)
