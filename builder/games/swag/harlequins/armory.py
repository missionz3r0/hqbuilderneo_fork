__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Prismatic grenades', 40, var_dicts=[
            {'name': 'Weapon reload', 'points': 20}
        ])


class H2H(OptionsList):
    def __init__(self, parent, master=False, virtuoso=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Concealed blade", 5)
        self.variant("Harlequin's blade", 15)
        if master or virtuoso:
            self.variant("Haqlequin's caress", 20)
        self.variant("Harlequin's kiss", 25)
        self.variant('Monomolecular blade', 30)
        if master or virtuoso:
            self.variant("Haqlequin's embrace", 40)
        if master:
            self.variant("Power sword", 50)


class Pistols(OptionsList):
    def __init__(self, parent, master=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.variants("Shuriken pistol", 35, var_dicts=[
            {'name': 'Weapon reload', 'points': 18}
        ])
        if master:
            self.variants("Neuro distruptor", 85, var_dicts=[
                {'name': 'Weapon reload', 'points': 43}
            ])
            self.variants("Fusion pistol", 100, var_dicts=[
                {'name': 'Weapon reload', 'points': 50}
            ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Photo-visor', 15)
