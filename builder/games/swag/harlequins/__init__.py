__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Master, Player, Mime, Virtuoso


class HLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(HLeader, self).__init__(*args, **kwargs)
        UnitType(self, Master)


class HTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(HTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Player)


class HNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(HNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Mime)


class HSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(HSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Virtuoso)


class HKillTeam(SWAGRoster):
    army_name = 'Harlequin Troupe Kill Team'
    army_id = 'clown_swag_v1'

    def __init__(self):
        super(HKillTeam, self).__init__(
            HLeader, HTroopers, HSpecialists,
            HNewRecruits, max_limit=6)
