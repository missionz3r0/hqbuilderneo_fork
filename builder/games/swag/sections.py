__author__ = 'Ivan Truskov'

from builder.core2.section import Section


class Leader(Section):
    def __init__(self, parent, min_limit=1, max_limit=1):
        super(Leader, self).__init__(parent, 'ld', 'Kill Team Leader',
                                     min_limit, max_limit)


class Troopers(Section):
    def __init__(self, parent, other_sections=[], max_limit=10):
        self.peer_sections = other_sections
        self.total_max = max_limit
        super(Troopers, self).__init__(parent, 'troop', 'Troopers',
                                       None, self.total_max)

    def check_rules(self):
        super(Troopers, self).check_rules()
        max_number = self.total_max - sum(len(s.units) for s in self.peer_sections)
        self.set_limits(0, max_number)


class Specialists(Section):
    def __init__(self, parent, max_limit=2):
        super(Specialists, self).__init__(parent, 'spec', 'Specialists',
                                          None, max_limit)


class NewRecruits(Section):
    def __init__(self, parent):
        self.peer_sections = []
        super(NewRecruits, self).__init__(parent, 'new', 'New Recruits',
                                          0, 0)

    def check_rules(self):
        super(NewRecruits, self).check_rules()
        max_number = sum(len(s.units) for s in self.peer_sections)
        self.set_limits(0, max_number)
