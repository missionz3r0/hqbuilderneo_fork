__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent, superior=False):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Frag grenades', 25, var_dicts=[
            {'name': 'Weapon reload', 'points': 13}
        ])
        if superior:
            self.variants('Melta bombs', 30, var_dicts=[
                {'name': 'Weapon reload', 'points': 15}
            ])
        self.variants('Krak grenades', 40, var_dicts=[
            {'name': 'Weapon reload', 'points': 20}
        ])


class H2H(OptionsList):
    def __init__(self, parent, superior=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        if superior:
            self.variant("Chainsword", 25)
            self.variant("Power maul", 50)
            self.variant("Power sword", 50)


class Pistols(OptionsList):
    def __init__(self, parent, superior=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.bp = self.variants("Bolt Pistol", 25, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 13}
        ])
        if superior:
            self.variants("Plasma pistol", 50, var_dicts=[
                {'name': 'Red-dot laser sight', 'points': 20},
                {'name': 'Weapon reload', 'points': 25}
            ])


class Basic(OptionsList):
    def __init__(self, parent, sergeant=False):
        super(Basic, self).__init__(parent, 'Basic weapons')
        self.variants("Boltgun", 35, var_dicts=[
            {'name': 'Red-dot laser sight', 'points': 20},
            {'name': 'Telescopic sight', 'points': 20},
            {'name': 'Weapon reload', 'points': 18}
        ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Simulacrum Imperialis', 50)


class Special(OptionsList):
    def __init__(self, parent, specialist=False, superior=False):
        super(Special, self).__init__(parent, 'Special Weapons')
        opts = [{'name': 'Red-dot laser sight', 'points': 20},
                {'name': 'Telescopic sight', 'points': 20}]
        if specialist:
            self.variants('Flamer', 40, var_dicts=[{'name': 'Weapon reload', 'points': 20}])
        self.variants('Storm bolter', 55, var_dicts=opts + [{'name': 'Weapon reload', 'points': 28}])
        if superior:
            self.variants('Combi-flamer', 55, var_dicts=opts + [{'name': 'Weapon reload', 'points': 28}])
            self.variants('Condemnor boltgun', 55, var_dicts=opts + [{'name': 'Weapon reload', 'points': 28}])
            self.variants('Combi-melta', 65, var_dicts=opts + [{'name': 'Weapon reload', 'points': 33}])
        if specialist:
            self.variants('Meltagun', 95, var_dicts=opts[0:1] + [{'name': 'Weapon reload', 'points': 48}])
            self.variants('Heavy flamer', 100, var_dicts=[{'name': 'Weapon reload', 'points': 50}])


class Heavy(OptionsList):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent, 'Heavy Weapons')
        w1 = self.variants('Heavy bolter', 180, var_dicts=[{'name': 'Weapon reload', 'points': 90}])
        w2 = self.variants('Multi-melta', 190, var_dicts=[{'name': 'Weapon reload', 'points': 95}])
        self.weapons = [w1, w2]

    def check_rules(self):
        super(Heavy, self).check_rules()
        OptionsList.process_limit(self.weapons, 1)
    
