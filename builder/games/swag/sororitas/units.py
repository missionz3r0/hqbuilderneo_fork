__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Superior(Unit):
    type_name = 'Sister Superior'
    type_id = 'as_superior_v1'

    def __init__(self, parent):
        super(Superior, self).__init__(parent, points=175, gear=[
            Gear('Combat blade'), Gear('Power armour')
        ])
        H2H(self, superior=True)
        Pistols(self, superior=True)
        Basic(self)
        Special(self, superior=True)
        Grenades(self, superior=True)
        Characteristics(self)
        Skills(self)


class Sister(Unit):
    type_name = 'Battle Sister'
    type_id = 'as_sister_v1'

    def __init__(self, parent):
        super(Sister, self).__init__(parent, points=90, gear=[
            Gear('Combat blade'), Gear('Power armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        self.misc = Misc(self)
        Characteristics(self)
        Skills(self)

    def get_unique_gear(self):
        return self.misc.description


class Novitiate(Unit):
    type_name = 'Novitiate'
    type_id = 'as_novitiate_v1'

    def __init__(self, parent):
        super(Novitiate, self).__init__(parent, points=80, gear=[
            Gear('Combat blade'), Gear('Power armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)


class Gunner(Unit):
    type_name = 'Gunner'
    type_id = 'as_gunner_v1'

    def __init__(self, parent):
        super(Gunner, self).__init__(parent, points=100, gear=[
            Gear('Combat blade'), Gear('Power armour')])
        H2H(self)
        Pistols(self)
        Special(self, specialist=True)
        Heavy(self)
        Grenades(self)
        Characteristics(self)
        Skills(self)
