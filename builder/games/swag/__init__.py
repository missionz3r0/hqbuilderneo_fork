__author__ = 'Ivan Truskov'

from .space_marines import SpaceMarinesKillTeam, SpaceWolvesKillTeam
from .astra_militarum import AstraMilitarumKillTeam
from .orks import OrkKillTeam
from .necrons import NecronKillTeam
from .grey_knights import GreyKnightsKillTeam
from .inquisition import InquisitionKillTeam
from .chaos_marines import CSMKillTeam
from .dark_eldar import DEKillTeam
from .craftworld_eldar import CEKillTeam
from .harlequins import HKillTeam
from .tyranids import TKillTeam
from .genestealer_cult import GenestealerCultKillTeam
from .skitarii import SkitariiKillTeam
from .tau import TauKillTeam
from .sororitas import SororitasKillTeam

armies = [
    SpaceMarinesKillTeam, SpaceWolvesKillTeam,
    AstraMilitarumKillTeam,
    OrkKillTeam,
    NecronKillTeam,
    GreyKnightsKillTeam,
    InquisitionKillTeam,
    CSMKillTeam,
    DEKillTeam,
    CEKillTeam,
    HKillTeam,
    TKillTeam,
    GenestealerCultKillTeam,
    SkitariiKillTeam,
    TauKillTeam,
    SororitasKillTeam
]
