__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Champion(Unit):
    type_name = 'Aspiring Champion'
    type_id = 'cm_champion_v1'

    def __init__(self, parent):
        super(Champion, self).__init__(parent, points=225, gear=[
            Gear('Combat blade'), Gear('Power armour')
        ])
        mark = Mark(self)
        MarineH2H(self, champion=True)
        MarinePistols(self, mark, champion=True)
        MarineBasic(self, mark)
        Grenades(self, mark, champion=True)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Marine(Unit):
    type_name = 'Chaos Space Marine'
    type_id = 'cm_marine_v1'

    def __init__(self, parent):
        super(Marine, self).__init__(parent, points=120, gear=[
            Gear('Combat blade'), Gear('Power armour')])
        mark = Mark(self)
        MarineH2H(self)
        MarinePistols(self, mark)
        MarineBasic(self, mark)
        Grenades(self, mark)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Cultist(Unit):
    type_name = 'Chaos Cultist'
    type_id = 'cm_cultist_v1'

    def __init__(self, parent):
        super(Cultist, self).__init__(parent, points=40, gear=[
            Gear('Combat blade'), Gear('Improvised armour')])
        CultH2H(self)
        CultPistol(self)
        CultBasic(self)


class Gunner(Unit):
    type_name = 'Chaos Gunner'
    type_id = 'cm_gunner_v1'

    def __init__(self, parent):
        super(Gunner, self).__init__(parent, points=130, gear=[
            Gear('Combat blade'), Gear('Power armour')])
        mark = Mark(self)
        MarineH2H(self)
        MarinePistols(self, mark)
        Heavy(self)
        Special(self)
        Grenades(self, mark)
        Misc(self)
        Characteristics(self)
        Skills(self)
