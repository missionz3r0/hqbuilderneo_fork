__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Exarch, Avenger, Guardian, Gunner, WeaponPlatform


class CELeader(Leader):
    def __init__(self, *args, **kwargs):
        super(CELeader, self).__init__(*args, **kwargs)
        UnitType(self, Exarch)


class CETroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(CETroopers, self).__init__(*args, **kwargs)
        UnitType(self, Avenger)


class CENewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(CENewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Guardian)


class CESpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(CESpecialists, self).__init__(*args, **kwargs)
        self.gunner = UnitType(self, Gunner)
        self.platform = UnitType(self, WeaponPlatform, slot=0)

    def check_rules(self):
        super(CESpecialists, self).check_rules()
        if self.platform.count > 1:
            self.error("Kill team may not include more then one Heavy Weapon Platform")
        elif self.platform.count == 1 and self.gunner.count < 1:
            self.error("At least one Guardian Defender Gunner is needed to man the platform")


class CEKillTeam(SWAGRoster):
    army_name = 'Craftworld Eldar Kill Team'
    army_id = 'ce_swag_v1'

    def __init__(self):
        super(CEKillTeam, self).__init__(
            CELeader, CETroopers, CESpecialists,
            CENewRecruits)
