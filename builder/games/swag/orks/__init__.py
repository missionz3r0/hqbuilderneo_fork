__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Nob, Boy, Yoof, Spanner


class OrkLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(OrkLeader, self).__init__(*args, **kwargs)
        UnitType(self, Nob)


class OrkTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(OrkTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Boy)


class OrkNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(OrkNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Yoof)


class OrkSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(OrkSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Spanner)


class OrkKillTeam(SWAGRoster):
    army_name = 'Ork Boyz Kill Team'
    army_id = 'orks_swag_v1'

    def __init__(self):
        super(OrkKillTeam, self).__init__(
            OrkLeader, OrkTroopers, OrkSpecialists,
            OrkNewRecruits, max_limit=20)
