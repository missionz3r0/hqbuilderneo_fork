__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Nob(Unit):
    type_name = 'Boss Nob'
    type_id = 'ork_nob_v1'

    def __init__(self, parent):
        super(Nob, self).__init__(parent, points=160, gear=[
            Gear('Shank'), Gear('Squig-hide armour')
        ])
        H2H(self, nob=True)
        Pistols(self)
        Basic(self, nob=True)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Boy(Unit):
    type_name = 'Boy'
    type_id = 'ork_boy_v1'

    def __init__(self, parent):
        super(Boy, self).__init__(parent, points=60, gear=[
            Gear('Shank'), Gear('Squig-hide armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Yoof(Unit):
    type_name = 'Yoof'
    type_id = 'ork_yoof_v1'

    def __init__(self, parent):
        super(Yoof, self).__init__(parent, points=30, gear=[
            Gear('Shank'), Gear('Squig-hide armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)


class Spanner(Unit):
    type_name = 'Spanner Boy'
    type_id = 'ork_spanner_v1'

    def __init__(self, parent):
        super(Spanner, self).__init__(parent, points=70, gear=[
            Gear('Shank'), Gear('Squig-hide armour')])
        H2H(self)
        Pistols(self)
        Special(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)
