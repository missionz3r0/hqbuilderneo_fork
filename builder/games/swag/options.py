__author__ = 'Ivan Truskov'

from builder.core2 import Count


class Characteristics(Count):
    def __init__(self, parent):
        super(Characteristics, self).__init__(
            parent, 'Characteristic advance', 0, 4, 15
        )


class Skills(Count):
    def __init__(self, parent):
        super(Skills, self).__init__(
            parent, 'Skill', 0, 2, 25
        )
