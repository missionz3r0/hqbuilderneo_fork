__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Scout, ScoutSergeant, ScoutGunner,\
    TacticalMarine, TacticalMarineGunner, TacticalMarineSergeant,\
    Reiver, ReiverSergeant, Intercessor, IntercessorSergeant
from .commanders import PrimarisCaptain, PrimarisChaplain, PrimarisLieutenant, PrimarisLibrarian
from builder.games.killteam.astra_militarum import JanusDraik, Eisenhorn


class AdeptusAstartesKT(KTRoster):
    army_name = 'Adeptus Astartes Kill Team'
    army_id = 'adeptus_astartes_kt_v1'
    unit_types = [Scout, ScoutSergeant, ScoutGunner, TacticalMarine,
                  TacticalMarineGunner, TacticalMarineSergeant, Reiver,
                  ReiverSergeant, Intercessor, IntercessorSergeant]


class ComAdeptusAstartesKT(KTCommanderRoster, AdeptusAstartesKT):
    army_name = 'Adeptus Astartes Kill Team'
    army_id = 'adeptus_astartes_kt_v2_com'
    commander_types = [PrimarisCaptain, PrimarisLieutenant,
                       PrimarisChaplain, PrimarisLibrarian, JanusDraik,
                       Eisenhorn]
