__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class PrimarisCaptain(KTCommander):
    desc = points.PrimarisCaptain
    type_id = 'primaris_captain_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Strategist', 'Strength']
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(PrimarisCaptain.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.MCAutoBoltRifle)
            self.variant(*points.MCStalkerBoltRifle)

    class Sword(OptionsList):
        def __init__(self, parent):
            super(PrimarisCaptain.Sword, self).__init__(parent, '')
            self.variant(*points.CapPowerSword)

    def __init__(self, parent):
        super(PrimarisCaptain, self).__init__(parent)
        self.Weapon(self)
        self.Sword(self)


class PrimarisLieutenant(KTCommander):
    desc = points.PrimarisLieutenant
    type_id = 'primaris_lieutenant_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Stealth', 'Strategist', 'Strength']
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(PrimarisLieutenant.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.MCAutoBoltRifle)
            self.variant(*points.LieutPowerSword)
            self.variant(*points.MCStalkerBoltRifle)

    def __init__(self, parent):
        super(PrimarisLieutenant, self).__init__(parent)
        self.Weapon(self)


class PrimarisChaplain(KTCommander):
    desc = points.PrimarisChaplain
    type_id = 'primaris_chaplain_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Melee', 'Shooting', 'Strength']
    gears = [points.CroziusArcanum, points.AbsolvorBoltPistol, points.FragGrenade, points.KrakGrenade]


class PrimarisLibrarian(KTCommander):
    desc = points.PrimarisLibrarian
    type_id = 'primaris_librarian_v1'
    specs = ['Fortitude', 'Melee', 'Psyker', 'Shooting', 'Strength']
    gears = [points.ForceSword, points.BoltPistol, points.FragGrenade, points.KrakGrenade]
