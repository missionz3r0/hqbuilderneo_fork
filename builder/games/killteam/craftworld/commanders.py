__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Amallyn(KTCommander):
    desc = points.AmallynShadowguide
    type_id = 'amallyn_v1'
    gears = [points.RangerLongRifle, points.PowerBlade, points.PlasmaGrenade]
    specs = ['Stealth']


class Autarch(KTCommander):
    desc = points.Autarch
    type_id = 'autarch_v1'
    gears = [points.PlasmaGrenade]
    specs = ['Ferocity', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Stealth', 'Strategist']

    class Configuration(OneOf):
        def __init__(self, parent):
            super(Autarch.Configuration, self).__init__(parent, 'Configuration')
            gear = [points.StarGlaive]
            self.variant('Infantry', points_price(0, *gear),
                         gear=create_gears(*gear))
            gear = [points.PowerSword, points.FusionPistol, points.Wings]
            self.variant('Flying', points_price(0, *gear),
                         gear=create_gears(*gear))

    def __init__(self, parent):
        super(Autarch, self).__init__(parent)
        self.Configuration(self)


class Warlock(KTCommander):
    desc = points.Warlock
    type_id = 'warlock_v1'
    gears = [points.ShurikenPistol]
    specs = ['Logistics', 'Melee', 'Psyker', 'Shooting']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Warlock.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Witchblade)
            self.variant(*points.WSingingSpear)

    def __init__(self, parent):
        super(Warlock, self).__init__(parent)
        self.Weapon(self)


class Warlock(KTCommander):
    desc = points.Warlock
    type_id = 'warlock_v1'
    gears = [points.ShurikenPistol]
    specs = ['Logistics', 'Melee', 'Psyker', 'Shooting']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Warlock.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Witchblade)
            self.variant(*points.WSingingSpear)

    def __init__(self, parent):
        super(Warlock, self).__init__(parent)
        self.Weapon(self)


class Farseer(KTCommander):
    desc = points.Farseer
    type_id = 'farseer_v1'
    gears = [points.ShurikenPistol]
    specs = ['Logistics', 'Melee', 'Psyker', 'Shooting']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Farseer.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Witchblade)
            self.variant(*points.FSingingSpear)

    def __init__(self, parent):
        super(Farseer, self).__init__(parent)
        self.Weapon(self)
