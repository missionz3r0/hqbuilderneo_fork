__author__ = 'Ivan Truskov'

# units
DireAvenger = ('Dire Avenger', 10)
DireAvengerExarch = ('Dire Avenger Exarch', 11)
GuardianDefender = ('Guardian Defender', 7)
HeavyWeaponPlatform = ('Heavy Weapon Platform', 8)
Ranger = ('Ranger', 11)
StormGuardian = ('Storm Guardian', 6)
StormGuardianGunner = ('Storm Guardian Gunner', 7)

AmallynShadowguide = ('Amallyn Shadowguide', [30, 35, 50, 60])
Autarch = ('Autarch', [55, 70, 85, 110])
Warlock = ('Warlock', [20, 25, 40, 60])
Farseer = ('Farseer', [55, 70, 85, 110])

# Ranged
AeldariMissileLauncher = ('Aeldari missile launcher', 5)
AvengerShurikenCatapult = ('Avenger shuriken catapult', 0)
BrightLance = ('Bright lance', 4)
Flamer = ('Flamer', 3)
FusionGun = ('Fusion gun', 3)
PlasmaGrenade = ('Plasma grenade', 0)
RangerLongRifle = ('Ranger long rifle', 0)
ScatterLaser = ('Scatter laser', 2)
ShurikenCannon = ('Shuriken cannon', 2)
ShurikenCatapult = ('Shuriken catapult', 0)
ShurikenPistol = ('Shuriken pistol', 0)
Starcannon = ('Starcannon', 3)

FusionPistol = ('Fusion pistol', 10)
# Melee
AeldariBlade = ('Aeldari blade', 0)
Chainsword = ('Chainsword', 0)
Diresword = ('Diresword', 2)
PowerGlaive = ('Power glaive', 1)
PowerBlade = ('Power blade', 0)

StarGlaive = ('Star glaive', 0)
PowerSword = ('Power sword', 0)
WSingingSpear = ('Singing spear', 3)
FSingingSpear = ('Singing spear', 5)
Witchblade = ('Witchblade', 0)

# wargear
Shimmershield = ('Shimmershield', 4)
Forceshield = ('Forceshield', 0)
Wings = ('Swooping Hawk wings', 20)
