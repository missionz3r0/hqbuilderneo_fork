__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel, KTModelNoSpec
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Guardian(KTModel):
    desc = points.GuardianDefender
    type_id = 'guardian_v1'
    gears = [points.ShurikenCatapult, points.PlasmaGrenade]
    specs = ['Leader', 'Comms', 'Medic', 'Scout', 'Veteran']


class GPlatform(KTModelNoSpec):
    desc = points.HeavyWeaponPlatform
    type_id = 'platform_v1'
    datasheet = 'Guardian Defender'
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(GPlatform.Weapon, self).__init__(parent, 'Heavy Weapon')
            self.variant(*points.ShurikenCannon)
            self.variant(*points.AeldariMissileLauncher)
            self.variant(*points.BrightLance)
            self.variant(*points.ScatterLaser)
            self.variant(*points.Starcannon)

    def __init__(self, parent):
        super(GPlatform, self).__init__(parent)
        self.Weapon(self)


class StormGuardian(KTModel):
    desc = points.StormGuardian
    type_id = 'storm_guardian_v1'
    gears = [points.ShurikenPistol, points.PlasmaGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Medic', 'Scout', 'Veteran']

    class Blade(OneOf):
        def __init__(self, parent):
            super(StormGuardian.Blade, self).__init__(parent, 'Sword')
            self.variant(*points.AeldariBlade)
            self.variant(*points.Chainsword)

    def __init__(self, parent):
        super(StormGuardian, self).__init__(parent)
        self.Blade(self)


class StormGuardianGunner(KTModel):
    desc = points.StormGuardianGunner
    type_id = 'storm_guardian_gunner_v1'
    datasheet = 'Storm Guardian'
    gears = [points.PlasmaGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Medic', 'Scout', 'Veteran']
    limit = 2

    class Gun(OneOf):
        def __init__(self, parent):
            super(StormGuardianGunner.Gun, self).__init__(parent, 'Gun')
            self.pistol = self.variant(*points.ShurikenPistol)
            self.variant(*points.Flamer)
            self.variant(*points.FusionGun)

    def __init__(self, parent):
        super(StormGuardianGunner, self).__init__(parent)
        self.gun = self.Gun(self)
        self.sword = StormGuardian.Blade(self)

    def check_rules(self):
        super(StormGuardianGunner, self).check_rules()
        self.sword.used = self.sword.visible = self.gun.cur == self.gun.pistol


class Ranger(KTModel):
    desc = points.Ranger
    type_id = 'ranger_v1'
    gears = [points.ShurikenPistol, points.RangerLongRifle]
    specs = ['Leader', 'Comms', 'Medic', 'Scout', 'Sniper', 'Veteran']


class DireAvenger(KTModel):
    desc = points.DireAvenger
    type_id = 'dire_avenger_v1'
    gears = [points.AvengerShurikenCatapult, points.PlasmaGrenade]
    specs = ['Combat', 'Comms', 'Medic', 'Scout', 'Veteran']


class DireAvengerExarch(KTModel):
    desc = points.DireAvengerExarch
    datasheet = 'Dire Avenger'
    type_id = 'dire_avenger_exarch_v1'
    gears = [points.PlasmaGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Medic', 'Scout', 'Veteran']
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DireAvengerExarch.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.AvengerShurikenCatapult)
            self.variant('Two avenger shuriken catapults', 2 * get_cost(points.AvengerShurikenCatapult),
                         gear=2 * create_gears(points.AvengerShurikenCatapult))
            self.variant('Shuriken pistol and power glaive',
                         points_price(0, points.ShurikenPistol, points.PowerGlaive),
                         gear=create_gears(points.ShurikenPistol, points.PowerGlaive))
            self.variant('Shuriken pistol and diresword',
                         points_price(0, points.ShurikenPistol, points.Diresword),
                         gear=create_gears(points.ShurikenPistol, points.Diresword))
            self.variant('Shimmershield and power glaive',
                         points_price(0, points.Shimmershield, points.PowerGlaive),
                         gear=create_gears(points.Shimmershield, points.PowerGlaive))

    def __init__(self, parent):
        super(DireAvengerExarch, self).__init__(parent)
        self.Weapon(self)
