__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Guardian, GPlatform, StormGuardian,\
    StormGuardianGunner, Ranger, DireAvenger, DireAvengerExarch
from .commanders import Amallyn, Autarch, Warlock, Farseer


class AsuryaniKT(KTRoster):
    army_name = 'Asuryani Kill Team'
    army_id = 'asuryani_kt_v1'
    unit_types = [Guardian, GPlatform, StormGuardian,
                  StormGuardianGunner, Ranger, DireAvenger, DireAvengerExarch]


class ComAsuryaniKT(KTCommanderRoster, AsuryaniKT):
    army_name = 'Asuryani Kill Team'
    army_id = 'asuryani_kt_v2_com'
    commander_types = [Autarch, Warlock, Farseer, Amallyn]
