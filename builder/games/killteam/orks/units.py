__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class OrkBoy(KTModel):
    desc = points.OrkBoy
    type_id = 'ork_boy_v1'
    specs = ['Combat', 'Demolitions', 'Veteran']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(OrkBoy.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Slugga and choppa',
                         gear=create_gears(points.Slugga, points.Choppa))
            self.variant(*points.Shoota)

    def __init__(self, parent):
        super(OrkBoy, self).__init__(parent)
        self.Weapon(self)


class OrkBoyGunner(KTModel):
    desc = points.OrkBoyGunner
    datasheet = 'Ork Boy'
    type_id = 'ork_boy_gunner_v1'
    specs = ['Heavy', 'Combat', 'Demolitions', 'Veteran']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(OrkBoyGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Slugga and choppa',
                         gear=create_gears(points.Slugga, points.Choppa))
            self.variant(*points.BigShoota)
            self.variant(*points.RokkitLauncha)

    def __init__(self, parent):
        super(OrkBoyGunner, self).__init__(parent)
        self.gun = self.Weapon(self)


class BossNob(KTModel):
    desc = points.BossNob
    datasheet = 'Ork Boy'
    type_id = 'boss_nob_v1'
    specs = ['Leader', 'Demolitions', 'Combat', 'Veteran']
    limit = 1

    class Gun(OneOf):
        def __init__(self, parent):
            super(BossNob.Gun, self).__init__(parent, 'Dakka')
            self.variant(*points.Slugga)
            self.variant(*points.KombiRokkitLauncha)
            self.variant(*points.KombiSkorcha)

    class Choppa(OneOf):
        def __init__(self, parent):
            super(BossNob.Choppa, self).__init__(parent, 'Choppa')
            self.variant(*points.Choppa)
            self.variant(*points.BigChoppa)
            self.variant(*points.PowerKlaw)

    def __init__(self, parent):
        super(BossNob, self).__init__(parent)
        self.Gun(self)
        self.Choppa(self)


class Gretchin(KTModel):
    desc = points.Gretchin
    type_id = 'gretchin_v1'
    gears = [points.GrotBlasta]
    specs = ['Leader', 'Scout']


class Kommando(KTModel):
    desc = points.Kommando
    type_id = 'kommando_v1'
    gears = [points.Slugga, points.Choppa, points.Stikkbomb]
    specs = ['Combat', 'Demolitions', 'Scout', 'Veteran']


class KommandoNob(KTModel):
    desc = points.KommandoBossNob
    type_id = 'kommando_nob_v1'
    datasheet = 'Kommando'
    gears = [points.Slugga, points.Stikkbomb]
    specs = ['Leader', 'Combat', 'Demolitions', 'Scout', 'Veteran']
    limit = 1

    class Choppa(OneOf):
        def __init__(self, parent):
            super(KommandoNob.Choppa, self).__init__(parent, 'Choppa')
            self.variant(*points.Choppa)
            self.variant(*points.PowerKlaw)

    def __init__(self, parent):
        super(KommandoNob, self).__init__(parent)
        self.Choppa(self)


class BurnaBoy(KTModel):
    desc = points.BurnaBoy
    type_id = 'burna_v1'
    gears = [points.Burna, points.Stikkbomb]
    specs = ['Leader', 'Combat', 'Demolitions', 'Veteran', 'Zealot']


class BurnaSpanner(KTModel):
    desc = points.BurnaSpanner
    type_id = 'burna_spanner_v1'
    datasheet = 'Burna Boy'
    gears = [points.Stikkbomb]
    specs = ['Comms', 'Leader', 'Combat', 'Demolitions', 'Veteran', 'Zealot']
    limit = 3

    class Dakka(OneOf):
        def __init__(self, parent):
            super(BurnaSpanner.Dakka, self).__init__(parent, 'Dakka')
            self.variant(*points.BigShoota)
            self.variant(*points.KustomMegaBlasta)
            self.variant(*points.RokkitLauncha)

    def __init__(self, parent):
        super(BurnaSpanner, self).__init__(parent)
        self.Dakka(self)


class Loota(KTModel):
    desc = points.Loota
    type_id = 'loota_v1'
    gears = [points.Deffgun, points.Stikkbomb]
    specs = ['Leader', 'Combat', 'Demolitions', 'Heavy', 'Veteran']


class LootaSpanner(KTModel):
    desc = points.LootaSpanner
    type_id = 'Loota_spanner_v1'
    datasheet = 'Loota'
    gears = [points.Stikkbomb]
    specs = ['Comms', 'Leader', 'Combat', 'Demolitions', 'Heavy', 'Veteran']
    limit = 3

    def __init__(self, parent):
        super(LootaSpanner, self).__init__(parent)
        BurnaSpanner.Dakka(self)
