__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Scion, ScionGunner, Tempestor,\
    SWGuardsman, SWGunner, Guardsman, ISGunner,\
    Sergeant, PiousVorne, Rein, Raus
from .commanders import JanusDraik, Taddeus, EspernLocarno, Commissar,\
    LordCommissar, PlatoonCommander, CompanyCommander, TempestorPrime,\
    Severina, Eisenhorn


class AstraMilitarumKT(KTRoster):
    army_name = 'Astra Militarum Kill Team'
    army_id = 'astra_militarum_kt_v1'
    unit_types = [Scion, ScionGunner, Tempestor, SWGuardsman,
                  SWGunner, Guardsman, ISGunner, Sergeant, PiousVorne,
                  Rein, Raus]


class ComAstraMilitarumKT(KTCommanderRoster, AstraMilitarumKT):
    army_name = 'Astra Militarum Kill Team'
    army_id = 'astra_militarum_kt_v2_com'
    commander_types = [JanusDraik, Taddeus, EspernLocarno, Commissar,
                       LordCommissar, PlatoonCommander,
                       CompanyCommander, TempestorPrime, Severina,
                       Eisenhorn]
