__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class JanusDraik(KTCommander):
    desc = points.JanusDraik
    type_id = 'janus_draik_v1'
    gears = [points.MonomolecularRapier, points.HeirloomPistol, points.ArcheotechGrenade]
    specs = ['Leadership']


class Taddeus(KTCommander):
    desc = points.Taddeus
    type_id = 'taddeus_v1'
    gears = [points.Laspistol, points.ServoStubber, points.PowerMaul]
    specs = ['Ferocity']


class EspernLocarno(KTCommander):
    desc = points.EspernLocarno
    type_id = 'espern_locarno_v1'
    gears = [points.ForceOrbCane, points.Laspistol]
    specs = ['Psyker']


class Commissar(KTCommander):
    desc = points.Commissar
    type_id = 'commissar_v1'
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Strategist']

    class Pistol(OneOf):
        def __init__(self, parent):
            super(Commissar.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.BoltPistol)
            self.variant(*points.ComPlasmaPistol)

    class Melee(OptionsList):
        def __init__(self, parent):
            super(Commissar.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*points.ComPowerFist)
            self.variant(*points.ComPowerSword)

    def __init__(self, parent):
        super(Commissar, self).__init__(parent)
        self.Pistol(self)
        self.Melee(self)


class LordCommissar(KTCommander):
    desc = points.LordCommissar
    type_id = 'lord_commissar_v1'
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Strategist']

    def __init__(self, parent):
        super(LordCommissar, self).__init__(parent)
        Commissar.Pistol(self)
        Commissar.Melee(self)


class Severina(KTCommander):
    desc = points.Severina
    type_id = 'severina_v1'
    datasheet = 'Commissar'
    gears = [('Penance', 0), ('Evenfall', 0)]
    specs = ['Leadership']


class Eisenhorn(KTCommander):
    desc = points.Eisenhorn
    type_id = 'eisenhorn_v1'
    gears = [('Barbarisater', 0), ('Runestaff', 0), points.ArtificerBoltPistol]
    specs = ['Strategist']


class PlatoonCommander(KTCommander):
    desc = points.PlatoonCommander
    type_id = 'pcommander_v1'
    specs = ['Leadership', 'Logistics', 'Shooting', 'Strength', 'Strategist']

    class Pistol(OneOf):
        def __init__(self, parent):
            super(PlatoonCommander.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.Laspistol)
            self.variant(*points.Boltgun)
            self.variant(*points.BoltPistol)
            self.variant(*points.ComPlasmaPistol)

    class Melee(OptionsList):
        def __init__(self, parent):
            super(PlatoonCommander.Melee, self).__init__(parent, 'Melee weapon', limit=1)
            self.variant(*points.ComChainsword)
            self.variant(*points.ComPowerFist)
            self.variant(*points.ComPowerSword)

    def __init__(self, parent):
        super(PlatoonCommander, self).__init__(parent)
        self.Pistol(self)
        self.Melee(self)


class CompanyCommander(KTCommander):
    desc = points.CompanyCommander
    type_id = 'ccommander_v1'
    specs = ['Leadership', 'Logistics', 'Shooting', 'Strength', 'Strategist']

    def __init__(self, parent):
        super(CompanyCommander, self).__init__(parent)
        PlatoonCommander.Pistol(self)
        PlatoonCommander.Melee(self)


class TempestorPrime(KTCommander):
    desc = points.TempestorPrime
    type_id = 'tempestor_prime_v1'
    specs = ['Leadership', 'Logistics', 'Shooting', 'Stealth', 'Strategist']

    class Pistol(OneOf):
        def __init__(self, parent):
            super(TempestorPrime.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.HotShotLaspistol)
            self.variant(*points.CommandRod)
            self.variant(*points.BoltPistol)
            self.variant(*points.ComPlasmaPistol)

    class Melee(OptionsList):
        def __init__(self, parent):
            super(TempestorPrime.Melee, self).__init__(parent, 'Melee weapon', limit=1)
            self.variant(*points.ComChainsword)
            self.variant(*points.PrimePowerFist)
            self.variant(*points.ComPowerSword)

    def __init__(self, parent):
        super(TempestorPrime, self).__init__(parent)
        TempestorPrime.Pistol(self)
        TempestorPrime.Melee(self)
