__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel, KTModelNoSpec
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Scion(KTModel):
    desc = points.MilitarumTempestusScion
    datasheet = 'Militarum Tempestus Scion'
    type_id = 'scion_v1'
    gears = [points.HotShotLasgun, points.FragGrenade, points.KrakGrenade]
    specs = ['Comms', 'Medic', 'Scout', 'Sniper', 'Veteran']

    class Caster(OptionsList):
        def __init__(self, parent):
            super(Scion.Caster, self).__init__(parent, 'Options')
            self.variant(*points.VoxCaster)

    def __init__(self, parent):
        super(Scion, self).__init__(parent)
        self.vox = self.Caster(self)

    def get_unique_gear(self):
        if self.vox.any:
            return ['Militarum Tempestus Vox-caster']
        else:
            pass


class ScionGunner(KTModel):
    desc = points.ScionGunner
    datasheet = 'Militarum Tempestus Scion'
    type_id = 'scion_gunner_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Demolitions', 'Heavy', 'Comms', 'Medic', 'Scout', 'Sniper', 'Veteran']
    limit = 4

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScionGunner.Weapon, self).__init__(parent, 'Gun')
            self.variant(*points.HotShotLasgun)
            self.variant(*points.Flamer)
            self.variant(*points.Meltagun)
            self.variant(*points.PlasmaGun)
            self.variant(*points.HotShotVolleyGun)

    def __init__(self, parent):
        super(ScionGunner, self).__init__(parent)
        self.Weapon(self)


class Tempestor(KTModel):
    desc = points.Tempestor
    datasheet = 'Militarum Tempestus Scion'
    type_id = 'tempestor_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Leader', 'Comms', 'Medic', 'Scout', 'Sniper', 'Veteran']
    limit = 1

    class Pistol(OneOf):
        def __init__(self, parent):
            super(Tempestor.Pistol, self).__init__(parent, 'Gun')
            self.variant(*points.HotShotLaspistol)
            self.variant(*points.BoltPistol)
            self.variant(*points.PlasmaPistol)

    class Sword(OneOf):
        def __init__(self, parent):
            super(Tempestor.Sword, self).__init__(parent, 'Sword')
            self.variant(*points.Chainsword)
            self.variant(*points.PowerSword)
            self.variant(*points.PowerFist)

    def __init__(self, parent):
        super(Tempestor, self).__init__(parent)
        self.Pistol(self)
        self.Sword(self)


class SWGuardsman(KTModel):
    desc = points.SpecialWeaponsSquadGuardsman
    datasheet = 'Special Weapons Squad Guardsman'
    type_id = 'swep_guardsman_v1'
    gears = [points.Lasgun, points.FragGrenade]
    specs = ['Leader', 'Comms', 'Demolitions', 'Scout', 'Sniper', 'Veteran']


class SWGunner(KTModel):
    desc = points.SpecialWeaponsGunner
    datasheet = 'Special Weapons Squad Guardsman'
    type_id = 'swep_gunner_v1'
    gears = [points.FragGrenade]
    specs = ['Heavy', 'Leader', 'Comms', 'Demolitions', 'Scout', 'Sniper', 'Veteran']
    limit = 3

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SWGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Lasgun)
            self.variant(*points.Flamer)
            self.variant(*points.GrenadeLauncher)
            self.variant(*points.Meltagun)
            self.variant(*points.PlasmaGun)
            self.variant(*points.SniperRifle)

    def __init__(self, parent):
        super(SWGunner, self).__init__(parent)
        self.Weapon(self)


class Guardsman(KTModel):
    desc = points.InfantrySquadGuardsman
    datasheet = 'Infantry Squad Guardsman'
    type_id = 'guardsman_v1'
    gears = [points.Lasgun, points.FragGrenade]
    specs = ['Comms', 'Demolitions', 'Scout', 'Sniper', 'Veteran']

    class Caster(OptionsList):
        def __init__(self, parent):
            super(Guardsman.Caster, self).__init__(parent, 'Options')
            self.variant(*points.VoxCaster)

    def __init__(self, parent):
        super(Guardsman, self).__init__(parent)
        self.vox = self.Caster(self)

    def check_rules(self):
        super(Guardsman, self).check_rules()
        self.spec.toggle_spec('Comms', self.vox.any)

    def get_unique_gear(self):
        if self.vox.any:
            return ['Infantry Vox-caster']
        else:
            pass


class ISGunner(KTModel):
    desc = points.GuardsmanGunner
    datasheet = 'Infantry Squad Guardsman'
    type_id = 'guardsman_gunner_v1'
    gears = [points.FragGrenade]
    specs = ['Heavy', 'Demolitions', 'Scout', 'Sniper', 'Veteran']
    limit = 1

    def __init__(self, parent):
        super(ISGunner, self).__init__(parent)
        SWGunner.Weapon(self)


class Sergeant(KTModel):
    desc = points.Sergeant
    datasheet = 'Infantry Squad Guardsman'
    type_id = 'sergeant_v1'
    gears = [points.FragGrenade]
    specs = ['Leader', 'Demolitions', 'Scout', 'Sniper', 'Veteran']
    limit = 1

    class Pistol(OneOf):
        def __init__(self, parent):
            super(Sergeant.Pistol, self).__init__(parent, 'Gun')
            self.variant(*points.Laspistol)
            self.variant(*points.BoltPistol)
            self.variant(*points.PlasmaPistol)

    class Sword(OneOf):
        def __init__(self, parent):
            super(Sergeant.Sword, self).__init__(parent, 'Sword')
            self.variant(*points.Chainsword)
            self.variant(*points.PowerSword)

    def __init__(self, parent):
        super(Sergeant, self).__init__(parent)
        self.Pistol(self)
        self.Sword(self)


class PiousVorne(KTModel):
    desc = points.PiousVorne
    type_id = 'pious_vorne_v1'
    gears = [('Vindicator', 0)]
    specs = ['Zealot']
    force_spec = True
    limit = 1


class Rein(KTModel):
    desc = points.Rein
    datasheet = 'Rein and Raus'
    type_id = 'rein_v1'
    gears = [('Sniper rifle', 0), points.StubPistol]
    specs = ['Sniper']
    force_spec = True
    limit = 1

    def check_rules(self):
        super(Rein, self).check_rules()
        if not any(isinstance(u, Raus) for u in self.roster.fire_teams.units):
            self.error("Where is brother Raus?")


class Raus(KTModelNoSpec):
    desc = points.Raus
    datasheet = 'Rein and Raus'
    type_id = 'raus_v1'
    gears = [points.StubPistol, points.DemolitionCharge]
    limit = 1

    def check_rules(self):
        super(Raus, self).check_rules()
        if not any(isinstance(u, Rein) for u in self.roster.fire_teams.units):
            self.error("Where is brother Rein?")
