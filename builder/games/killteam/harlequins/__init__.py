__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Player
from .commanders import TroupeMaster, Shadowseer, DeathJester


class HarlequinKT(KTRoster):
    army_name = 'Harlequin Kill Team'
    army_id = 'harlequin_kt_v1'
    unit_types = [Player]


class ComHarlequinKT(KTCommanderRoster, HarlequinKT):
    army_name = 'Harlequin Kill Team'
    army_id = 'harlequin_kt_v2_com'
    commander_types = [TroupeMaster, Shadowseer, DeathJester]
