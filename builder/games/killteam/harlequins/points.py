__author__ = 'Ivan Truskov'

# units
Player = ('Player', 12)

TroupeMaster = ('Troupe Master', [50, 65, 80, 105])
Shadowseer = ('Shadowseer', [65, 80, 95, 120])
DeathJester = ('Death Jester', [45, 60, 75, 100])

# Ranged
FusionPistol = ('Fusion pistol', 3)
NeuroDisruptor = ('Neuro disruptor', 2)
PlasmaGrenade = ('Plasma grenade', 0)
ShurikenPistol = ('Shuriken pistol', 0)

CFusionPistol = ('Fusion pistol', 10)
CNeuroDistruptor = ('Neuro distruptor', 6)
Hallicunogen = ('Hallucinogen grenade launcher', 0)
ShriekerCannon = ('Shrieker cannon', 0)

# Melee
HarlequinBlade = ("Harlequin's blade", 0)
HarlequinCaress = ("Harlequin's caress", 3)
HarlequinEmbrace = ("Harlequin's embrace", 2)
HarlequinKiss = ("Harlequin's kiss", 4)

CHarlequinCaress = ("Harlequin's caress", 7)
CHarlequinEmbrace = ("Harlequin's embrace", 6)
CHarlequinKiss = ("Harlequin's kiss", 9)
PowerSword = ('Power sword', 4)
Miststave = ('Miststave', 0)
