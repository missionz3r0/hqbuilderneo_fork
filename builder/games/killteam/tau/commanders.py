__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class DahyakGrekh(KTCommander):
    desc = points.DahyakGrekh
    type_id = 'grekh_v1'
    gears = [points.KrootRifle, points.KrootPistol]
    specs = ['Stealth']


class Fireblade(KTCommander):
    desc = points.Fireblade
    type_id = 'fireblade_v1'
    gears = [points.Markerlight, points.PulseRifle, points.PhotonGrenade]
    specs = ['Leadership', 'Logistics', 'Shooting', 'Stealth', 'Strategist']


class Ethereal(KTCommander):
    desc = points.Ethereal
    type_id = 'ethereal_v1'
    specs = ['Leadership', 'Logistics', 'Strategist']

    class Blade(OneOf):
        def __init__(self, parent):
            super(Ethereal.Blade, self).__init__(parent, 'Melee weapon')
            self.variant(*points.HonourBlade)
            self.variant(*points.Equalizers)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Ethereal.Options, self).__init__(parent, 'Options')
            self.variant(*points.HoverDrone)

    def __init__(self, parent):
        super(Ethereal, self).__init__(parent)
        self.Blade(self)
        self.Options(self)
