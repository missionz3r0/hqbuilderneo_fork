__author__ = 'Ivan Truskov'

# units
Deathmark = ('Deathmark', 15)
FlayedOne = ('Flayed One', 10)
Immortal = ('Immortal', 16)
NecronWarrior = ('Necron Warrior', 12)

# Commanders
Overlord = ('Overlord', [86, 106, 126, 151])
Cryptek = ('Cryptek', [44, 59, 74, 99])

# ranged weapons
GaussBlaster = ('Gauss blaster', 0)
GaussFlayer = ('Gauss flayer', 0)
SynapticDisintegrator = ('Synaptic disintegrator', 0)
TeslaCarbine = ('Tesla carbine', 0)

StaffOfLight = ('Staff of Light', 0)

# melee weapons
FlayerClaws = ('Flayer claws', 0)

Warscythe = ('Warscythe', 0)
Voidscythe = ('Voidscythe', 0)

# Wargear
CanoptekCloak = ('Canoptek cloak', 10)
