__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import NecronWarrior, FlayedOne, Deathmark, Immortal
from .commanders import Cryptek, Overlord


class NecronKT(KTRoster):
    army_name = 'Necron Kill Team'
    army_id = 'necron_kt_v1'
    unit_types = [NecronWarrior, FlayedOne, Deathmark, Immortal]


class ComNecronKT(KTCommanderRoster, NecronKT):
    army_name = 'Necron Kill Team'
    army_id = 'necron_kt_v2_com'
    commander_types = [Overlord, Cryptek]
