__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class Overlord(KTCommander):
    desc = points.Overlord
    type_id = 'overlord_v1'
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Strategist', 'Strength']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Overlord.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Voidscythe)
            self.variant(*points.Warscythe)
            self.variant(*points.StaffOfLight)

    def __init__(self, parent):
        super(Overlord, self).__init__(parent)
        self.Weapon(self)


class Cryptek(KTCommander):
    desc = points.Cryptek
    type_id = 'cryptek_v1'
    specs = ['Fortitude', 'Logistics', 'Melee', 'Shooting', 'Strategist', 'Strength']

    class Option(OneOf):
        def __init__(self, parent):
            super(Cryptek.Option, self).__init__(parent, 'Options')
            self.variant(*points.CanoptekCloak)

    def __init__(self, parent):
        super(Cryptek, self).__init__(parent)
        self.Option(self)
