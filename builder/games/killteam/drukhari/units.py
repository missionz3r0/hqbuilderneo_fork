__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Kabalite(KTModel):
    desc = points.KabaliteWarrior
    type_id = 'kabalite_v1'
    gears = [points.SplinterRifle]
    specs = ['Comms', 'Scout', 'Veteran', 'Zealot']


class KabGunner(KTModel):
    desc = points.KabaliteGunner
    datasheet = 'Kabalite Warrior'
    type_id = 'kabalite_gunner_v1'
    specs = ['Sniper', 'Comms', 'Scout', 'Veteran', 'Zealot']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(KabGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.SplinterRifle)
            self.group1 = [
                self.variant(*points.SplinterCannon),
                self.variant(*points.DarkLance)
            ]
            self.group2 = [
                self.variant(*points.Shredder),
                self.variant(*points.Blaster)
            ]

    def __init__(self, parent):
        super(KabGunner, self).__init__(parent)
        self.gun = self.Weapon(self)

    def get_unique_gear(self):
        if self.gun.cur in self.gun.group1:
            return ['Splinter cannon or dark lance']
        elif self.gun.cur in self.gun.group2:
            return ['Shredder or blaster']
        pass


class Sybarite(KTModel):
    desc = points.Sybarite
    datasheet = 'Kabalite Warrior'
    type_id = 'sybarite_v1'
    specs = ['Leader', 'Comms', 'Scout', 'Veteran', 'Zealot']
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Sybarite.Weapon, self).__init__(parent, 'Ranged weapon')
            self.variant(*points.SplinterRifle)
            self.variant(*points.SplinterPistol)
            self.variant(*points.BlastPistol)

    class Melee(OptionsList):
        def __init__(self, parent):
            super(Sybarite.Melee, self).__init__(parent, 'Melee weapon',
                                                 limit=1)
            self.variant(*points.PowerSword)
            self.variant(*points.Agoniser)

    class Option(OptionsList):
        def __init__(self, parent):
            super(Sybarite.Option, self).__init__(parent, '')
            self.variant(*points.PhantasmGrenadeLauncher)

    def __init__(self, parent):
        super(Sybarite, self).__init__(parent)
        self.Weapon(self)
        self.Melee(self)
        self.Option(self)


class Wych(KTModel):
    desc = points.Wych
    type_id = 'wych_v1'
    gears = [points.SplinterPistol, points.HekatariiBlade, points.PlasmaGrenade]
    specs = ['Combat', 'Scout', 'Veteran', 'Zealot']


class WychFighter(KTModel):
    desc = points.WychFighter
    datasheet = 'Wych'
    type_id = 'wych_fighter_v1'
    specs = ['Combat', 'Scout', 'Veteran', 'Zealot']
    gears = [points.PlasmaGrenade]
    limit = 3

    class Weapon(OneOf):
        def __init__(self, parent):
            super(WychFighter.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Splinter pistol and Hekatrii blade',
                         gear=create_gears(points.SplinterPistol, points.HekatariiBlade))
            self.variant(*points.HydraGauntlets)
            self.variant(*points.Razorflails)
            self.variant(*points.ShardnetAndImpaler)

    def __init__(self, parent):
        super(WychFighter, self).__init__(parent)
        self.Weapon(self)


class Hekatrix(KTModel):
    desc = points.Hekatrix
    datasheet = 'Wych'
    type_id = 'hekatrix_v1'
    specs = ['Leader', 'Combat', 'Scout', 'Veteran', 'Zealot']
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hekatrix.Weapon, self).__init__(parent, 'Ranged weapon')
            self.variant(*points.SplinterPistol)
            self.variant(*points.BlastPistol)

    class Melee(OneOf):
        def __init__(self, parent):
            super(Hekatrix.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*points.HekatariiBlade)
            self.variant(*points.PowerSword)
            self.variant(*points.Agoniser)

    def __init__(self, parent):
        super(Hekatrix, self).__init__(parent)
        self.Weapon(self)
        self.Melee(self)
        Sybarite.Option(self)
