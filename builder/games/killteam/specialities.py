__author__ = 'Ivan Truskov'


from builder.core2.options import Option, Count, OptionsList, Gear, OneOf
from builder.core2 import UnitDescription


specialist_meta = {
    'Leader': [
        'Resourceful',
        {
            'Bold': 'Resourceful',
            'Inspiring': 'Resourceful'
        },
        {
            'Paragon': 'Bold',
            'Tyrant': 'Bold',
            'Tactician': 'Inspiring',
            'Mentor': 'Inspiring'
        }
    ],
    'Combat': [
        'Expert Fighter',
        {
            'Warrior Adept': 'Expert Fighter',
            'Deadly Counter': 'Expert Fighter'
        },
        {
            'Deathblow': 'Warrior Adept',
            'Combat Master': 'Warrior Adept',
            'Killer Instinct': 'Deadly Counter',
            'Bloodlust': 'Deadly Counter'
        }
    ],
    'Comms': [
        'Scanner',
        {
            'Expert': 'Scanner',
            'Static Screech': 'Scanner'
        },
        {
            'Vox ghost': 'Expert',
            'Command Relay': 'Expert',
            'Triangulator': 'Static Screech',
            'Vox Hacker': 'Static Screech'
        }
    ],
    'Demolitions': [
        'Breacher',
        {
            'Pyromaniac': 'Breacher',
            'Grenadier': 'Breacher'
        },
        {
            'Saboteur': 'Pyromaniac',
            'Sapper': 'Pyromaniac',
            'Siege master': 'Grenadier',
            'Ammo Hound': 'Grenadier'
        }
    ],
    'Heavy': [
        'Relentless',
        {
            'Suppressor': 'Relentless',
            'Extra Armour': 'Relentless'
        },
        {
            'Devastator': 'Suppressor',
            'Rigorous': 'Suppressor',
            'Indomitable': 'Extra Armour',
            'Heavy Muscled': 'Extra Armour'
        }
    ],
    'Medic': [
        'Reassuring',
        {
            'Field Medic': 'Reassuring',
            'Anatomist': 'Reassuring'
        },
        {
            'Trauma Specialist': 'Field Medic',
            'Triage Expert': 'Field Medic',
            'Interrogator': 'Anatomist',
            'Toxin Synthesiser': 'Anatomist'
        }
    ],
    'Scout': [
        'Swift',
        {
            'Forward Scout': 'Swift',
            'Pathfinder': 'Swift'
        },
        {
            'Skirmisher': 'Forward Scout',
            'Vanguard': 'Forward Scout',
            'Observer': 'Pathfinder',
            'Explorer': 'Pathfinder'
        }
    ],
    'Sniper': [
        'Marksman',
        {
            'Assassin': 'Marksman',
            'Sharpshooter': 'Marksman'
        },
        {
            'Deadeye': 'Assassin',
            'Armour piercing': 'Assassin',
            'Mobile': 'Sharpshooter',
            'Eagle-eye': 'Sharpshooter'
        }
    ],
    'Veteran': [
        'Grizzled',
        {
            'Practiced': 'Grizzled',
            'Seen it All': 'Grizzled'
        },
        {
            'Survivor': 'Practiced',
            'One-man Army': 'Practiced',
            'Battle Scarred': 'Seen it All',
            'Nerves of Steel': 'Seen it All'
        }
    ],
    'Zealot': [
        'Frenzied',
        {
            'Exultant': 'Frenzied',
            'Flagellant': 'Frenzied'
        },
        {
            'Puritan': 'Exultant',
            'Rousing': 'Exultant',
            'Fanatical': 'Flagellant',
            'Strength of Spirit': 'Flagellant'
        }
    ]
}

# commander specialties
specialist_meta.update({
    'Shooting': [
        'Shootist',
        {
            'Trick-Shooter': 'Shootist',
            'Pistoleer': 'Shootist'
        },
        {
            'Targeting Weak spots': 'Trick-Shooter',
            'Precision strike': 'Trick-Shooter',
            'Hip shooter': 'Pistoleer',
            'Long bomb': 'Pistoleer'
        }
    ],
    'Fortitude': [
        'Hardy Constitution',
        {
            'INDOMITABLE': 'Hardy Constitution',
            'Hard To Kill': 'Hardy Constitution'
        },
        {
            'Feel no Pain': 'INDOMITABLE',
            'Unyielding will': 'INDOMITABLE',
            'True Grit': 'Hard To Kill',
            'Iron Constitution': 'Hard To Kill'
        }
    ],
    'Strategist': [
        'Symbol Of Courage',
        {
            'Aura Of Command': 'Symbol Of Courage',
            'INSPIRING': 'Symbol Of Courage'
        },
        {
            'Master of War': 'Aura Of Command',
            'Heroic': 'Aura Of Command',
            'Grim determination': 'INSPIRING',
            'Tenacious': 'INSPIRING'
        }
    ],
    'Leadership': [
        'Resourceful',
        {
            'Advisor': 'Resourceful',
            'Feigned Retreat': 'Resourceful'
        },
        {
            'Counter-strategist': 'Advisor',
            'Master tactician': 'Advisor',
            'Famed commander': 'Feigned Retreat',
            'Mission-critical Mastermind': 'Feigned Retreat'
        }
    ],
    'Ferocity': [
        'Counter-Attack',
        {
            'BLOODLUST': 'Counter-Attack',
            'Ignore Pain': 'Counter-Attack'
        },
        {
            'Fearsome War Cry': 'BLOODLUST',
            'Berserker': 'BLOODLUST',
            'Death Frenzy': 'Ignore Pain',
            'Indignant Rampage': 'Ignore Pain'
        }
    ],
    'Logistics': [
        'Extra Armour',
        {
            'Quartermaster': 'Extra Armour',
            'Armed to the Teeth': 'Extra Armour'
        },
        {
            'Scavenger': 'Quartermaster',
            'Master of Sabotage': 'Quartermaster',
            'Master Artisan': 'Armed to the Teeth',
            'Rangefinder Scope': 'Armed to the Teeth'
        }
    ],
    'Melee': [
        'Expert Fighter',
        {
            'Warrior Born': 'Expert Fighter',
            'Swift Parry': 'Expert Fighter'
        },
        {
            'Duellist': 'Warrior Born',
            'Precision Strike': 'Warrior Born',
            'Impenetrable Defence': 'Swift Parry',
            'Lightning Reflexes': 'Swift Parry'
        }
    ],
    'Psyker': [
        'Student of the Arcane',
        {
            'Psychic Onslaught': 'Student of the Arcane',
            'Warp Drain': 'Student of the Arcane'
        },
        {
            'Psionic Potency': 'Psychic Onslaught',
            'Omniscience': 'Psychic Onslaught',
            'Protective Wards': 'Warp Drain',
            'Witchbane': 'Warp Drain'
        }
    ],
    'Stealth': [
        'Steady Aim',
        {
            'Skulker': 'Steady Aim',
            'Climber': 'Steady Aim'
        },
        {
            'One with the Shadows': 'Skulker',
            'Lurker': 'Skulker',
            'Prowler': 'Climber',
            'Sure-footed': 'Climber'
        }
    ],
    'Strength': [
        'Muscular',
        {
            'Juggernaut': 'Muscular',
            'Brutal Strikes': 'Muscular'
        },
        {
            'Bull Charge': 'Juggernaut',
            'Sunderer': 'Juggernaut',
            'Devastating power': 'Brutal Strikes',
            'Crusher': 'Brutal Strikes'
        }
    ]

})

ability_levels = {}
sorted_abilities = {'': []}
dependencies = {}

for s in list(specialist_meta.keys()):
    ability_levels[specialist_meta[s][0]] = 1
    ability_levels.update({k:2 for k in list(specialist_meta[s][1].keys())})
    ability_levels.update({k:3 for k in list(specialist_meta[s][2].keys())})
    abilities = [specialist_meta[s][0]] + list(specialist_meta[s][1].keys()) + list(specialist_meta[s][2].keys())
    sorted_abilities[s] = abilities
    dependencies.update(specialist_meta[s][1])
    dependencies.update(specialist_meta[s][2])
    dependencies[abilities[0]] = abilities[0]


class Speciality(Option):
    class SpecOptional(OptionsList):
        def __init__(self, parent, allowed_specs):
            super(Speciality.SpecOptional, self).__init__(parent, 'Specialty', limit=1, order=19)
            for v in allowed_specs:
                self.variant(v)

        @property
        def chosen(self):
            if not self.any:
                return ''
            opt = [v for v in self.options if v.value][0]
            return opt.title

        @property
        def description(self):
            return []

        def toggle_spec(self, spec, flag):
            target = [o for o in self.options if o.title == spec]
            if len(target):
                target = target[0]
            else:
                return
            if flag:
                target.active = True
                OptionsList.process_limit(self.options, 1)
            else:
                target.value = False
                OptionsList.process_limit(self.options, 1)
                target.active = False

    class SpecMandatory(OneOf):
        def __init__(self, parent, allowed_specs):
            super(Speciality.SpecMandatory, self).__init__(parent, 'Specialty', order=19)
            for v in allowed_specs:
                self.variant(v)

        @property
        def chosen(self):
            return self.cur.title

        @property
        def description(self):
            return []

        def toggle_spec(self, spec, flag):
            pass

        @property
        def any(self):
            return True


    class Abilities(OptionsList):
        def __init__(self, parent, allowed_specs):
            super(Speciality.Abilities, self).__init__(parent, 'Abilities', order=21)
            for spec in allowed_specs:
                for ability in sorted_abilities[spec]:
                    self.variant(ability)

        def refresh_abilities(self, spec, level):
            current = []
            for op in self.options:
                if not op.title in sorted_abilities[spec]:
                    op.used = op.visible = False
                else:
                    op.used = op.visible = True
                    current.append(op)
            default = [o for o in current if ability_levels[o.title] == 1][0]
            default.value = True
            default.active = False
            selected = 1
            if level == 4:
                bad_choice = 0
                for o in current:
                    if o != default:
                        o.active = True
                        if o.value:
                            selected += 1
                            dep = [d for d in current if d.title == dependencies[o.title]][0]
                            if not dep.value:
                                bad_choice += 1
                if bad_choice > 1:
                    self.parent.error('Ability dependencies broken')
            else:
                # import pdb; pdb.set_trace()
                for o in current:
                    lvl = ability_levels[o.title]
                    if lvl == 1:
                        continue
                    else:
                        dep = [d for d in current if d.title == dependencies[o.title]][0]
                        o.active = lvl <= level and dep.value and lvl > selected
                        if not o.active:
                            o.value = False
                        selected += o.value
            if selected != level:
                self.parent.error('Exactly {} abilites should be taken'.format(level))

        def check_rules(self):
            super(Speciality.Abilities, self).check_rules()
            if self.parent.spec.opt.any:
                self.refresh_abilities(self.parent.spec.value, self.parent.spec.lvl.cur)

    class Level(Count):
        @property
        def description(self):
            return [Gear('Level {} {}'.format(self.cur, self.parent.spec.opt.chosen))] if self.used else []

        @property
        def points(self):
            return self.parent.spec.costs[self.cur - 1]

    def __init__(self, parent, allowed_specs, force=False, level_cost=None):
        super(Speciality, self).__init__(parent)
        self.free = force
        if force:
            self.opt = self.SpecMandatory(parent, allowed_specs)
        else:
            self.opt = self.SpecOptional(parent, allowed_specs)
        if level_cost is None:
            max_level = 4
        else:
            max_level = len(level_cost)
        self.lvl = self.Level(parent, 'Level', 1, max_level, order=20)
        self.abilities = self.Abilities(parent, allowed_specs)
        if level_cost is None:
            self.costs = [4 * i for i in range(0, 4)]
        else:
            self.costs = level_cost[:]

    def toggle_spec(self, spec, flag):
        self.opt.toggle_spec(spec, flag)

    def check_rules(self):
        self.lvl.used = self.lvl.visible = self.opt.any
        self.abilities.used = self.abilities.visible = self.opt.any

    def is_leader(self):
        return 'Leader' == self.opt.chosen

    def is_spec(self):
        return self.opt.chosen not in ['', 'Leader']

    @property
    def value(self):
        return self.opt.chosen

    @property
    def level(self):
        return self.lvl.cur


class EmptySpecialty(Option):
    free = False
    def is_leader(self):
        return False

    def is_spec(self):
        return False

    @property
    def value(self):
        return ''

    @property
    def level(self):
        return 0
