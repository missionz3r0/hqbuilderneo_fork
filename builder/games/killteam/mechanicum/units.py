__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel, KTModelNoSpec
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class SkitariiRanger(KTModel):
    desc = points.SkitariiRanger
    type_id = 'sk_ranger_v1'
    gears = [points.GalvanicRifle]
    specs = ['Comms', 'Scout', 'Sniper', 'Zealot']

    class Options(OptionsList):
        def __init__(self, parent):
            super(SkitariiRanger.Options, self).__init__(parent, 'Options', limit=1)
            self.variant(*points.EnhancedDataTether)
            self.variant(*points.Omnispex)

    def __init__(self, parent):
        super(SkitariiRanger, self).__init__(parent)
        self.vox = self.Options(self)

    def get_unique_gear(self):
        if self.vox.any:
            return ['Ranger data tether or omnispex']
        else:
            pass


class RangerGunner(KTModel):
    desc = points.RangerGunner
    datasheet = 'Skitarii Ranger'
    type_id = 'sk_ranger_gunner_v1'
    specs = ['Heavy', 'Comms', 'Scout', 'Sniper', 'Zealot']
    limit = 3

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RangerGunner.Weapon, self).__init__(parent, 'Gun')
            self.variant(*points.GalvanicRifle)
            self.variant(*points.ArcRifle)
            self.variant(*points.PlasmaCaliver)
            self.variant(*points.TransuranicArquebus)

    def __init__(self, parent):
        super(RangerGunner, self).__init__(parent)
        self.Weapon(self)


class RangerAlpha(KTModel):
    desc = points.RangerAlpha
    datasheet = 'Skitarii Ranger'
    type_id = 'sk_ranger_alpha_v1'
    specs = ['Leader', 'Comms', 'Scout', 'Sniper', 'Zealot']
    limit = 1

    class Pistol(OneOf):
        def __init__(self, parent):
            super(RangerAlpha.Pistol, self).__init__(parent, 'Gun')
            self.gun = self.variant(*points.GalvanicRifle)
            self.variant(*points.ArcPistol)
            self.variant(*points.PhosphorBlastPistol)
            self.variant(*points.RadiumPistol)

    class Sword(OneOf):
        def __init__(self, parent):
            super(RangerAlpha.Sword, self).__init__(parent, 'Melee weapon')
            self.variant(*points.ArcMaul)
            self.variant(*points.PowerSword)
            self.variant(*points.TaserGoad)

    def __init__(self, parent):
        super(RangerAlpha, self).__init__(parent)
        self.gun = self.Pistol(self)
        self.sword = self.Sword(self)

    def check_rules(self):
        super(RangerAlpha, self).check_rules()
        self.sword.used = self.sword.visible = not self.gun.cur == self.gun.gun


class SkitariiVanguard(KTModel):
    desc = points.SkitariiVanguard
    type_id = 'sk_vanguard_v1'
    gears = [points.RadiumCarbine]
    specs = ['Comms', 'Scout', 'Sniper', 'Zealot']

    def __init__(self, parent):
        super(SkitariiVanguard, self).__init__(parent)
        self.vox = SkitariiRanger.Options(self)

    def get_unique_gear(self):
        if self.vox.any:
            return ['Vanguard data tether or omnispex']
        else:
            pass


class VanguardGunner(KTModel):
    desc = points.VanguardGunner
    datasheet = 'Skitarii Vanguard'
    type_id = 'sk_vanguard_gunner_v1'
    specs = ['Heavy', 'Comms', 'Scout', 'Sniper', 'Zealot']
    limit = 3

    class Weapon(OneOf):
        def __init__(self, parent):
            super(VanguardGunner.Weapon, self).__init__(parent, 'Gun')
            self.variant(*points.RadiumCarbine)
            self.variant(*points.ArcRifle)
            self.variant(*points.PlasmaCaliver)
            self.variant(*points.TransuranicArquebus)

    def __init__(self, parent):
        super(VanguardGunner, self).__init__(parent)
        self.Weapon(self)


class VanguardAlpha(KTModel):
    desc = points.VanguardAlpha
    datasheet = 'Skitarii Vanguard'
    type_id = 'sk_vanguard_alpha_v1'
    specs = ['Leader', 'Comms', 'Scout', 'Sniper', 'Zealot']
    limit = 1

    class Pistol(OneOf):
        def __init__(self, parent):
            super(VanguardAlpha.Pistol, self).__init__(parent, 'Gun')
            self.gun = self.variant(*points.RadiumCarbine)
            self.variant(*points.ArcPistol)
            self.variant(*points.PhosphorBlastPistol)
            self.variant(*points.RadiumPistol)

    def __init__(self, parent):
        super(VanguardAlpha, self).__init__(parent)
        self.gun = self.Pistol(self)
        self.sword = RangerAlpha.Sword(self)

    def check_rules(self):
        super(VanguardAlpha, self).check_rules()
        self.sword.used = self.sword.visible = not self.gun.cur == self.gun.gun


class Ruststalker(KTModel):
    desc = points.SicarianRuststalker
    type_id = 'ruststalker_v1'
    specs = ['Combat', 'Comms', 'Scout', 'Veteran', 'Zealot']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Ruststalker.Weapon, self).__init__(parent, 'Weapons')
            self.variant('Transonic razor and chordclaw',
                         points_price(0, points.TransonicRazor, points.Chordclaw),
                         gear=create_gears(points.TransonicRazor, points.Chordclaw))
            self.variant(*points.TransonicBlades)

    def __init__(self, parent):
        super(Ruststalker, self).__init__(parent)
        self.Weapon(self)


class RuststalkerPrinceps(KTModel):
    desc = points.RuststalkerPrinceps
    datasheet = get_name(points.SicarianRuststalker)
    type_id = 'ruststalker_princeps_v1'
    specs = ['Leader', 'Combat', 'Comms', 'Scout', 'Veteran', 'Zealot']
    gears = [points.Chordclaw]
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RuststalkerPrinceps.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*points.TransonicRazor)
            self.variant(*points.TransonicBlades)

    def __init__(self, parent):
        super(RuststalkerPrinceps, self).__init__(parent)
        self.Weapon(self)


class Infiltrator(KTModel):
    desc = points.SicarianInfiltrator
    type_id = 'infiltrator_v1'
    specs = ['Combat', 'Comms', 'Scout', 'Veteran', 'Zealot']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Infiltrator.Weapon, self).__init__(parent, 'Weapons')
            self.variant('Stubcarbine and power sword',
                         points_price(0, points.Stubcarbine, points.PowerSword),
                         gear=create_gears(points.Stubcarbine, points.PowerSword))
            self.variant('Flechette blaster and taser goad',
                         points_price(0, points.FlechetteBlaster, points.TaserGoad),
                         gear=create_gears(points.FlechetteBlaster, points.TaserGoad))

    def __init__(self, parent):
        super(Infiltrator, self).__init__(parent)
        self.Weapon(self)


class InfiltratorPrinceps(KTModel):
    desc = points.InfiltratorPrinceps
    datasheet = get_name(points.SicarianInfiltrator)
    type_id = 'infiltrator_princeps_v1'
    specs = ['Leader', 'Combat', 'Comms', 'Scout', 'Veteran', 'Zealot']
    limit = 1

    def __init__(self, parent):
        super(InfiltratorPrinceps, self).__init__(parent)
        Infiltrator.Weapon(self)


class UR025(KTModel):
    desc = points.UR025
    type_id = 'ur-025_v1'
    gears = [('Mk 1 assault cannon', 0), ('Power claw', 0)]
    specs = ['Heavy']
    force_spec = True
    limit = 1
