__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class Manipulus(KTCommander):
    desc = points.Manipulus
    type_id = 'manipulus_v1'
    gears = [points.OmnissiahStaff, points.Mechadendrites]
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Shooting', 'Strategist', 'Strength']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Manipulus.Weapon, self).__init__(parent, 'Ranged weapon')
            self.variant(*points.MagnarailLance)
            self.variant(*points.TransonicCannon)

    def __init__(self, parent):
        super(Manipulus, self).__init__(parent)
        self.Weapon(self)


class Enginseer(KTCommander):
    desc = points.Enginseer
    type_id = 'enginseer_v1'
    gears = [points.Laspistol, points.OmnissiahAxe, points.ServoArm]
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Shooting', 'Strategist', 'Strength']


class Dominus(KTCommander):
    desc = points.Dominus
    type_id = 'dominus_v1'
    gears = [points.OmnissiahAxe]
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Shooting', 'Strategist', 'Strength']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Dominus.Weapon1, self).__init__(parent, 'Ranged weapons')
            self.variant(*points.VolkiteBlaster)
            self.variant(*points.EradicationRay)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Dominus.Weapon2, self).__init__(parent, '')
            self.variant(*points.Macrostubber)
            self.variant(*points.PhosphorSerpentra)

    def __init__(self, parent):
        super(Dominus, self).__init__(parent)
        self.Weapon1(self)
        self.Weapon2(self)
