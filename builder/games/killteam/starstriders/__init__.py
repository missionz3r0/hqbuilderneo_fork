__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Voidsman, VoidsmanGunner, VoidmasterNitsch, Aximillion,\
    KnossoPrond, Larsen, SanistasiaMinst
from .commanders import Elucia


class StarstridersKT(KTRoster):
    army_name = 'Elucidian Starstriders Kill Team'
    army_id = 'starstriders_kt_v1'
    unit_types = [Voidsman, VoidsmanGunner, VoidmasterNitsch,
                  Aximillion, KnossoPrond, Larsen, SanistasiaMinst]


class ComStarstridersKT(KTCommanderRoster, StarstridersKT):
    army_name = 'Elucidian Starstriders Kill Team'
    army_id = 'starstriders_kt_v2_com'
    commander_types = [Elucia]
