__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class Elucia(KTCommander):
    desc = points.EluciaVhane
    type_id = 'elucia_v1'
    gears = [points.HeirloomPistol, points.MonomolecularCaneRapier, points.ConcussionGrenades]
    specs = ['Strategist', 'Combat', 'Scout', 'Veteran', 'Zealot']

    class ExtraTraits(OptionsList):
        def __init__(self, parent):
            super(Elucia.ExtraTraits, self).__init__(parent, 'Extra traits', order=18)
            self.variant('Explorator fleetmaster', 10)
            self.variant('Trader militant', 15)

    def __init__(self, parent):
        super(Elucia, self).__init__(parent)
        self.ExtraTraits(self)
