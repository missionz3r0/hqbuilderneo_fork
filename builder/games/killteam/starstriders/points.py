__author__ = 'Ivan Truskov'

# units
EluciaVhane = ('Elucia Vhane', [45, 60, 75, 100])
KnossoPrond = ('Knosso Prond', 25)
Larsen = ('Larsen van der Grauss', 22)
SanistasiaMinst = ('Sanistasia Minst', 17)
Voidsman = ('Voidsman', 6)
Aximillion = ('Aximillion', 6)
VoidsmanGunner = ('Voidsman Gunner', 6)
VoidmasterNitsch = ('Voidmaster Nitsch', 6)

# Ranged
ArtificerShotgun = ('Artificer shotgun', 0)
ConcussionGrenades = ('Concussion grenades', 0)
Dartmask = ('Dartmask', 0)
HeirloomPistol = ('Heirloom pistol', 0)
Laspistol = ('Laspistol', 0)
Lasgun = ('Lasgun', 0)
RotorCannon = ('Rotor cannon', 0)
VoltaicPistol = ('Voltaic pistol', 0)

# Melee
DeathCultPowerBlade = ('Death cult power blade', 0)
MonomolecularCaneRapier = ('monomolecular cane-rapier', 0)
ScalpelClaw = ('Scalpel claw', 0)
ViciousBite = ('Vicious bite', 0)
