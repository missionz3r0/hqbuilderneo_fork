__author__ = 'Ivan Truskov'

from builder.core2.section import Section
from builder.core2 import UnitType


class FireTeams(Section):
    def unit_type_add(self, ut, **kwargs):
        return UnitType(self, ut, group=ut.datasheet, **kwargs)

    def __init__(self, parent, min_limit=3, max_limit=20):
        super(FireTeams, self).__init__(parent, 'ft', 'Kill Team',
                                        min_limit, max_limit)

    def check_rules(self):
        super(FireTeams, self).check_rules()
        for t in self.types:
            if hasattr(t.unit_class, 'limit'):
                if t.unit_class.limit < t.count:
                    self.error('Should not include more then {} of {} in kill team; taken: {}'.format(t.unit_class.limit, t.unit_class.type_name, t.count))


class Commanders(Section):
    def unit_type_add(self, ut, **kwargs):
        return UnitType(self, ut, group=ut.datasheet, **kwargs)

    def __init__(self, parent, min_limit=1, max_limit=1):
        super(Commanders, self).__init__(parent, 'com', 'Commanders',
                                        min_limit, max_limit)
