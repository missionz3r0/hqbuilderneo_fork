__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class Broodlord(KTCommander):
    desc = points.Broodlord
    type_id = 'broodlord_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Melee', 'Psyker', 'Stealth', 'Strategist', 'Strength']
    gears = [points.MonstrousRendingClaws]


class TPrime(KTCommander):
    desc = points.TyranidPrime
    type_id = 'tprime_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Melee', 'Strategist', 'Strength']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TPrime.Weapon1, self).__init__(parent, 'Weapon 1')
            self.variant(*points.ScythingTalons)
            self.variant(*points.RendingClaws)
            self.variant(*points.PBoneswords,
                         gear=[Gear('Bonesword', count=2)])
            self.variant(*points.PLashWhipAndBonesword,
                         gear=[Gear('Bonesword'), Gear('Lash Whip')])

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TPrime.Weapon2, self).__init__(parent, 'Weapon 2')
            self.variant(*points.DevourerWarrior)
            self.variant(*points.PDeathspitter)
            self.variant(*points.ScythingTalons)
            self.variant(*points.PBoneswords,
                         gear=[Gear('Bonesword', count=2)])
            self.variant(*points.Spinefists)
            self.variant(*points.RendingClaws)
            self.variant(*points.PLashWhipAndBonesword,
                         gear=[Gear('Bonesword'), Gear('Lash Whip')])

    class Options(OptionsList):
        def __init__(self, parent):
            super(TPrime.Options, self).__init__(parent, 'Options')
            self.variant(*points.PFleshHooks)
            self.variant(*points.PToxinSacs)
            self.variant(*points.AdrenalGlands)

    def __init__(self, parent):
        super(TPrime, self).__init__(parent)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Options(self)
