__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Termagant(KTModel):
    desc = points.Termagant
    type_id = 'termagant_v1'
    specs = ['Veteran', 'Scout']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Termagant.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Fleshborer)
            self.variant(*points.DevourerTermagant)
            self.variant(*points.Spinefists)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Termagant.Options, self).__init__(parent, 'Options')
            self.variant(*points.ToxinSacs)
            self.variant(*points.AdrenalGlands)

    def __init__(self, parent):
        super(Termagant, self).__init__(parent)
        self.Weapon(self)
        self.Options(self)


class Hormagaunt(KTModel):
    desc = points.Hormagaunt
    type_id = 'hormogaunt_v1'
    gears = [points.ScythingTalons]
    specs = ['Combat', 'Veteran', 'Scout']

    def __init__(self, parent):
        super(Hormagaunt, self).__init__(parent)
        Termagant.Options(self)


class Lictor(KTModel):
    desc = points.Lictor
    type_id = 'lictor_v1'
    gears = [points.FleshHooks, points.GraspingTalons, points.RendingClaws]
    specs = ['Leader', 'Combat', 'Comms', 'Veteran', 'Scout']


class TWarrior(KTModel):
    desc = points.TyranidWarrior
    type_id = 'twarrior_v1'
    specs = ['Leader', 'Combat', 'Comms', 'Veteran']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TWarrior.Weapon1, self).__init__(parent, 'Weapon 1')
            self.variant(*points.ScythingTalons)
            self.variant(*points.RendingClaws)
            self.variant(*points.Boneswords,
                         gear=[Gear('Bonesword', count=2)])
            self.variant(*points.LashWhipAndBonesword,
                         gear=[Gear('Bonesword'), Gear('Lash Whip')])

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TWarrior.Weapon2, self).__init__(parent, 'Weapon 2')
            self.variant(*points.DevourerWarrior)
            self.variant(*points.Deathspitter)
            self.variant(*points.Spinefists)
            self.variant(*points.RendingClaws)
            self.variant(*points.Boneswords,
                         gear=[Gear('Bonesword', count=2)])
            self.variant(*points.ScythingTalons)
            self.variant(*points.LashWhipAndBonesword,
                         gear=[Gear('Bonesword'), Gear('Lash Whip')])

    class Options(OptionsList):
        def __init__(self, parent):
            super(TWarrior.Options, self).__init__(parent, 'Options')
            self.variant(*points.FleshHooks)
            self.variant(*points.ToxinSacs)
            self.variant(*points.AdrenalGlands)

    def __init__(self, parent):
        super(TWarrior, self).__init__(parent)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Options(self)


class TGunner(KTModel):
    desc = points.TyranidWarriorGunner
    datasheet = 'Tyranid Warrior'
    type_id = 'tgunner_v1'
    specs = ['Heavy', 'Leader', 'Combat', 'Comms', 'Veteran']
    limit = 1

    class GunnerWeapon(OneOf):
        def __init__(self, parent):
            super(TGunner.GunnerWeapon, self).__init__(parent, 'Weapon 2')
            self.variant(*points.DevourerWarrior)
            self.variant(*points.BarbedStrangler)
            self.variant(*points.VenomCannon)
            self.variant(*points.Deathspitter)
            self.variant(*points.Spinefists)
            self.variant(*points.RendingClaws)
            self.variant(*points.Boneswords,
                         gear=[Gear('Bonesword', count=2)])
            self.variant(*points.ScythingTalons)
            self.variant(*points.LashWhipAndBonesword,
                         gear=[Gear('Bonesword'), Gear('Lash Whip')])

    def __init__(self, parent):
        super(TGunner, self).__init__(parent)
        TWarrior.Weapon1(self)
        self.GunnerWeapon(self)
        TWarrior.Options(self)


class Genestealer(KTModel):
    desc = points.Genestealer
    type_id = 'genestealer_v1'
    gears = [points.RendingClaws]
    specs = ['Leader', 'Combat', 'Veteran', 'Scout']

    class Options(OptionsList):
        def __init__(self, parent):
            super(Genestealer.Options, self).__init__(parent, 'Options')
            self.variant(*points.ScythingTalons)
            self.variant(*points.ToxinSacs)
            self.variant(*points.ExtendedCarapace)

    class RestrictedOptions(OptionsList):
        def __init__(self, parent):
            super(Genestealer.RestrictedOptions, self).__init__(parent, '', limit=1)
            self.variant(*points.AcidMaw)
            self.variant(*points.FleshHooks)

    def __init__(self, parent):
        super(Genestealer, self).__init__(parent)
        self.Options(self)
        self.limited = self.RestrictedOptions(self)

    def get_unique_gear(self):
        return self.limited.description
