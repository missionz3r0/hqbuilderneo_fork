__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel, KTModelNoSpec
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Aberrant(KTModel):
    desc = points.Aberrant
    type_id = 'aberrant_v1'
    gears = [points.RendingClaw]
    specs = ['Leader', 'Combat', 'Demolitions', 'Zealot']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Aberrant.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.PowerPick)
            self.variant(*points.PowerHammer)

    def __init__(self, parent):
        super(Aberrant, self).__init__(parent)
        self.Weapon(self)


class AcolyteHybrid(KTModel):
    desc = points.AcolyteHybrid
    type_id = 'acolyte_hubrid_v1'
    gears = [points.CultistKnife, points.RendingClaw, points.BlastingCharge]
    specs = ['Combat', 'Comms', 'Zealot']

    class Pistol(OneOf):
        def __init__(self, parent):
            super(AcolyteHybrid.Pistol, self).__init__(parent, 'Handgun')
            self.variant(*points.Autopistol)
            self.variant(*points.HandFlamer)

    class Icon(OptionsList):
        def __init__(self, parent):
            super(AcolyteHybrid.Icon, self).__init__(parent, 'Options')
            self.variant(*points.CultIcon)

    def __init__(self, parent):
        super(AcolyteHybrid, self).__init__(parent)
        self.Pistol(self)
        self.icon = self.Icon(self)

    def get_unique_gear(self):
        if self.icon.any:
            return ['Acolyte Cult icon']


class AcolyteFighter(KTModel):
    desc = points.AcolyteFighter
    type_id = 'acolyte_fighter_v1'
    datasheet = 'Acolyte Hybrid'
    gears = [points.BlastingCharge]
    specs = ['Demolitions', 'Combat', 'Comms', 'Zealot']
    limit = 4

    class Weapon(OneOf):
        def __init__(self, parent):
            super(AcolyteFighter.Weapon, self).__init__(parent, 'Weapons')
            self.variant('Cultist knife and rending claw',
                         points_price(0, points.CultistKnife, points.RendingClaw),
                         gear=create_gears(points.CultistKnife, points.RendingClaw))
            self.variant(*points.HeavyRockDrill)
            self.variant(*points.HeavyRockSaw)
            self.variant(*points.HeavyRockCutter)
            self.variant(*points.DemolitionCharge)

    def __init__(self, parent):
        super(AcolyteFighter, self).__init__(parent)
        self.Weapon(self)
        AcolyteHybrid.Pistol(self)


class AcolyteLeader(KTModel):
    desc = points.AcolyteLeader
    type_id = 'acolyte_Leader_v1'
    datasheet = 'Acolyte Hybrid'
    gears = [points.BlastingCharge]
    specs = ['Leader', 'Combat', 'Comms', 'Zealot']
    limit = 1

    class Pistol(OneOf):
        def __init__(self, parent):
            super(AcolyteLeader.Pistol, self).__init__(parent, 'Handgun')
            self.variant(*points.Autopistol)
            self.variant(*points.HandFlamer)
            self.whip = self.variant(*points.LashWhipAndBonesword,
                                     gear=[Gear('Lash whip'), Gear('Bonesword')])

    class Knife(OneOf):
        def __init__(self, parent):
            super(AcolyteLeader.Knife, self).__init__(parent, 'Knife')
            self.variant(*points.CultistKnife)
            self.variant(*points.Bonesword)

    def __init__(self, parent):
        super(AcolyteLeader, self).__init__(parent)
        self.gun = self.Pistol(self)
        self.knife = self.Knife(self)

    def check_rules(self):
        super(AcolyteLeader, self).check_rules()
        self.knife.used = self.knife.visible = not self.gun.cur == self.gun.whip


class NeophyteHybrid(KTModel):
    desc = points.NeophyteHybrid
    type_id = 'neophyte_hubrid_v1'
    gears = [points.Autopistol, points.BlastingCharge]
    specs = ['Demolitions', 'Medic', 'Scout', 'Zealot']

    class Gun(OneOf):
        def __init__(self, parent):
            super(NeophyteHybrid.Gun, self).__init__(parent, 'Gun')
            self.variant(*points.Autogun)
            self.variant(*points.Shotgun)

    class Icon(OptionsList):
        def __init__(self, parent):
            super(NeophyteHybrid.Icon, self).__init__(parent, 'Options')
            self.variant(*points.CultIcon)

    def __init__(self, parent):
        super(NeophyteHybrid, self).__init__(parent)
        self.Gun(self)
        self.icon = self.Icon(self)

    def get_unique_gear(self):
        if self.icon.any:
            return ['Neophyte Cult icon']


class NeophyteGunner(KTModel):
    desc = points.NeophyteGunner
    type_id = 'neophyte_gunner_v1'
    datasheet = 'Neophyte Hybrid'
    gears = [points.Autopistol, points.BlastingCharge]
    specs = ['Heavy', 'Demolitions', 'Medic', 'Scout', 'Zealot']
    limit = 4

    class Weapon(OneOf):
        def __init__(self, parent):
            super(NeophyteGunner.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*points.Autogun)
            self.variant(*points.Shotgun)
            self.group1 = [
                self.variant(*points.Flamer),
                self.variant(*points.GrenadeLauncher),
                self.variant(*points.Webber)
            ]
            self.group2 = [
                self.variant(*points.HeavyStubber),
                self.variant(*points.MiningLaser),
                self.variant(*points.SeismicCannon)
            ]

    def __init__(self, parent):
        super(NeophyteGunner, self).__init__(parent)
        self.gun = self.Weapon(self)

    def build_statistics(self):
        res = super(NeophyteGunner, self).build_statistics()
        if self.gun.cur in self.gun.group1:
            res['Special'] = 1
        elif self.gun.cur in self.gun.group2:
            res['Heavy'] = 1
        return res


class NeophyteLeader(KTModel):
    desc = points.NeophyteLeader
    type_id = 'neophyte_Leader_v1'
    datasheet = 'Neophyte Hybrid'
    gears = [points.BlastingCharge]
    specs = ['Leader', 'Demolitions', 'Scout', 'Medic', 'Zealot']
    limit = 1

    class Pistol(OneOf):
        def __init__(self, parent):
            super(NeophyteLeader.Pistol, self).__init__(parent, 'Handgun')
            self.auto = self.variant(*points.Autopistol)
            self.variant(*points.BoltPistol)
            self.variant(*points.WebPistol)

    class Gun(OneOf):
        def __init__(self, parent):
            super(NeophyteLeader.Gun, self).__init__(parent, 'Weapon')
            self.guns = [
                self.variant(*points.Autogun),
                self.variant(*points.Shotgun)
            ]
            self.variant(*points.Chainsword)
            self.variant(*points.PowerMaul)
            self.variant(*points.PowerPick)

    def __init__(self, parent):
        super(NeophyteLeader, self).__init__(parent)
        self.gun = self.Pistol(self)
        self.wep = self.Gun(self)

    def check_rules(self):
        super(NeophyteLeader, self).check_rules()
        flag = self.gun.cur == self.gun.auto
        for v in self.wep.guns:
            v.active = flag


class HybridMetamorph(KTModel):
    desc = points.HybridMetamorph
    type_id = 'hubrid_metamorph_v1'
    gears = [points.BlastingCharge]
    specs = ['Combat', 'Comms', 'Demolitions', 'Zealot']

    class Weapons(OneOf):
        def __init__(self, parent):
            super(HybridMetamorph.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Rending claw and Metamorph Talon',
                         points_price(0, points.RendingClaw, points.MetamorphTalon),
                         gear=create_gears(points.RendingClaw, points.MetamorphTalon))
            self.variant('Two Metamorph Talons',
                         points_price(0, points.MetamorphTalon, points.MetamorphTalon),
                         gear=create_gears(points.MetamorphTalon, points.MetamorphTalon))
            self.variant('Rending claw and Metamorph Whip',
                         points_price(0, points.RendingClaw, points.MetamorphWhip),
                         gear=create_gears(points.RendingClaw, points.MetamorphWhip))
            self.variant(*points.MetamorphClaw)

    def __init__(self, parent):
        super(HybridMetamorph, self).__init__(parent)
        AcolyteHybrid.Pistol(self)
        self.Weapons(self)
        self.icon = AcolyteHybrid.Icon(self)

    def get_unique_gear(self):
        if self.icon.any:
            return ['Matamorph Cult icon']


class HybridMetamorphLeader(KTModel):
    desc = points.MetamorphLeader
    datasheet = 'Hybrid Metamorph'
    type_id = 'metamorph_leader_v1'
    gears = [points.BlastingCharge]
    specs = ['Leader', 'Combat', 'Comms', 'Demolitions', 'Zealot']
    limit = 1

    class Sword(OptionsList):
        def __init__(self, parent):
            super(HybridMetamorphLeader.Sword, self).__init__(parent, '')
            self.variant(*points.Bonesword)

    def __init__(self, parent):
        super(HybridMetamorphLeader, self).__init__(parent)
        AcolyteHybrid.Pistol(self)
        HybridMetamorph.Weapons(self)
        self.Sword(self)
