__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Aberrant, AcolyteHybrid, AcolyteFighter, AcolyteLeader,\
    NeophyteHybrid, NeophyteGunner, NeophyteLeader, HybridMetamorph, HybridMetamorphLeader
from .commanders import Magus, Primus, Patriarch, AcolyteIconward


class GenestealerKT(KTRoster):
    army_name = 'Genestealer Cult Kill Team'
    army_id = 'genestealers_kt_v1'
    unit_types = [Aberrant, AcolyteHybrid, AcolyteFighter,
                  AcolyteLeader, NeophyteHybrid, NeophyteGunner, NeophyteLeader,
                  HybridMetamorph, HybridMetamorphLeader]

    def build_statistics(self):
        res = super(GenestealerKT, self).build_statistics()
        res.pop('Special', 0)
        res.pop('Heavy', 0)
        return res

    def check_rules(self):
        super(GenestealerKT, self).check_rules()
        stat = super(GenestealerKT, self).build_statistics()
        cnt = stat.pop('Special', 0)
        if cnt > 2:
            self.error('No more then 2 special weapons may be taken by neophite hybrids; taken: %d' % cnt)
        cnt = stat.pop('Heavy', 0)
        if cnt > 2:
            self.error('No more then 2 heavy weapons may be taken by neophite hybrids; taken: %d', cnt)


class ComGenestealerKT(KTCommanderRoster, GenestealerKT):
    army_name = 'Genestealer Cult Kill Team'
    army_id = 'genestealer_kt_v2_com'
    commander_types = [Magus, Primus, Patriarch, AcolyteIconward]
