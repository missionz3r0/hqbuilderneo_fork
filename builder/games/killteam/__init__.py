__author__ = 'Ivan Truskov'

from .necrons import NecronKT, ComNecronKT
from .grey_knights import GreyKnightKT, ComGreyKnightKT
from .astra_militarum import AstraMilitarumKT, ComAstraMilitarumKT
from .drukhari import DrukhariKT, ComDrukhariKT
from .harlequins import HarlequinKT, ComHarlequinKT
from .heretic_astartes import HereticAstartesKT, ComHereticAstartesKT
from .death_guard import DeathGuardKT, ComDeathGuardKT
from .adeptus_astartes import AdeptusAstartesKT, ComAdeptusAstartesKT
from .thousand_sons import ThousandSonsKT, ComThousandSonsKT
from .orks import OrksKT, ComOrksKT
from .tyranids import TyranidKT, ComTyranidKT
from .servants import ServantKT, ComServantKT
from .craftworld import AsuryaniKT, ComAsuryaniKT
from .deathwatch import DeathwatchKT, ComDeathwatchKT
from .genestealers import GenestealerKT, ComGenestealerKT
from .tau import TauKT, KrootKT, ComTauKT, ComKrootKT
from .gellerpox import GellerpoxKT, ComGellerpoxKT
from .starstriders import StarstridersKT, ComStarstridersKT
from .mechanicum import AdeptusMechanicumKT, ComAdeptusMechanicumKT
from .daemons import DaemonKT

armies = [NecronKT, GreyKnightKT, AstraMilitarumKT, DrukhariKT,
          HarlequinKT, HereticAstartesKT, DeathGuardKT,
          AdeptusAstartesKT, ComNecronKT, ThousandSonsKT, OrksKT,
          ComDrukhariKT, TyranidKT, ServantKT, ComServantKT,
          AsuryaniKT, DeathwatchKT, GenestealerKT, TauKT, KrootKT,
          GellerpoxKT, ComGellerpoxKT, StarstridersKT,
          ComStarstridersKT, ComTauKT, ComKrootKT, ComAsuryaniKT,
          ComAstraMilitarumKT, ComAdeptusAstartesKT,
          AdeptusMechanicumKT, ComAdeptusMechanicumKT,
          ComDeathwatchKT, ComGreyKnightKT, ComHarlequinKT,
          ComHereticAstartesKT, ComDeathGuardKT, ComThousandSonsKT,
          ComOrksKT, ComTyranidKT, ComGenestealerKT, DaemonKT]
