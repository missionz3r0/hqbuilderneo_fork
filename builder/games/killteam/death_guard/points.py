__author__ = 'Ivan Truskov'

# units
PlagueMarine = ('Plague Marine', 14)
PlagueMarineGunner = ('Plague Marine Gunner', 15)
PlagueMarineFighter = ('Plague Marine Fighter', 15)
PlagueChampion = ('Plague Champion', 15)
Poxwalker = ('Poxwalker', 3)

FoulBlightspawn = ('Foul Blightspawn', [80, 100, 120, 145])
Tallyman = ('Tallyman', [45, 60, 75, 100])
BiologusPutrifier = ('Biologus putrifier', [50, 65, 80, 105])
PlagueSurgeon = ('Plague Surgeon', [45, 60, 75, 100])

# Ranged
BlightGrenade = ('Blight grenade', 0)
BlightLauncher = ('Blight launcher', 3)
BoltPistol = ('Bolt pistol', 0)
Boltgun = ('Boltgun', 0)
KrakGrenade = ('Krak grenade', 0)
Meltagun = ('Meltagun', 3)
PlagueBelcher = ('Plague belcher', 3)
PlagueSpewer = ('Plague spewer', 4)
PlasmaGun = ('Plasma gun', 3)
PlasmaPistol = ('Plasma pistol', 1)

PlagueSprayer = ('Plague Sprayer', 0)
TPlasmaPistol = ('Plasma pistol', 0)
HyperBlightGrenades = ('Hyper blight grenades', 0)
InjectorPistol = ('Injector pistol', 0)

# Melee
BuboticAxe = ('Bubotic axe', 2)
FlailOfCorruption = ('Flail of corruption', 4)
GreatPlagueCleaver = ('Great plague cleaver', 4)
ImprovisedWeapon = ('Improvised weapon', 0)
MaceOfContagion = ('Mace of contagion', 3)
PlagueKnife = ('Plague knife', 0)
Plaguesword = ('Plaguesword', 0)
PowerFist = ('Power fist', 4)

Balesword = ('Balesword', 0)

# gear
IconOfDespair = ('Icon of Despair', 3)
