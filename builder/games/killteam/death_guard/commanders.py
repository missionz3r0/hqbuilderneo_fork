__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from . import points


class FoulBlightspawn(KTCommander):
    desc = points.FoulBlightspawn
    type_id = 'foul_blightspawn_v1'
    specs = ['Fortitude', 'Logistics', 'Melee', 'Strength']
    gears = [points.PlagueSprayer,
             points.BlightGrenade, points.KrakGrenade]


class Tallyman(KTCommander):
    desc = points.Tallyman
    type_id = 'tallyman_v1'
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Strategist', 'Strength']
    gears = [points.TPlasmaPistol,
             points.BlightGrenade, points.KrakGrenade]


class BiologusPutrifier(KTCommander):
    desc = points.BiologusPutrifier
    type_id = 'biologus_putrifier_v1'
    specs = ['Fortitude', 'Logistics', 'Melee', 'Shooting', 'Strength']
    gears = [points.PlagueKnife, points.InjectorPistol,
             points.HyperBlightGrenades, points.KrakGrenade]


class PlagueSurgeon(KTCommander):
    desc = points.PlagueSurgeon
    type_id = 'plague_surgeon_v1'
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Strength']
    gears = [points.BoltPistol, points.Balesword,
             points.BlightGrenade, points.KrakGrenade]
