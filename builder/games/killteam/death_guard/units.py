__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Poxwalker(KTModel):
    desc = points.Poxwalker
    type_id = 'poxwalker_v1'
    gears = [points.ImprovisedWeapon]
    specs = ['Combat', 'Zealot']


class PlagueMarine(KTModel):
    desc = points.PlagueMarine
    type_id = 'plagmarine_v1'
    gears = [points.PlagueKnife, points.Boltgun, points.BlightGrenade, points.KrakGrenade]
    specs = ['Combat', 'Demolitions', 'Veteran']

    class ChaosIcon(OptionsList):
        def __init__(self, parent):
            super(PlagueMarine.ChaosIcon, self).__init__(parent, 'Icon of Chaos', limit=1)
            self.variant(*points.IconOfDespair)

    def __init__(self, parent):
        super(PlagueMarine, self).__init__(parent)
        self.icon = self.ChaosIcon(self)

    def get_unique_gear(self):
        return ['Icon of Despair'] if self.icon.any else []


class PlagueMarineGunner(KTModel):
    desc = points.PlagueMarineGunner
    datasheet = 'Plague Marine'
    type_id = 'plague_guner_v1'
    gears = [points.PlagueKnife, points.BlightGrenade, points.KrakGrenade]
    specs = ['Heavy', 'Combat', 'Demolitions', 'Veteran']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(PlagueMarineGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.PlagueSpewer)
            self.variant(*points.PlagueBelcher)
            self.variant(*points.BlightLauncher)
            self.variant(*points.Meltagun)
            self.variant(*points.PlasmaGun)

    def __init__(self, parent):
        super(PlagueMarineGunner, self).__init__(parent)
        self.gun = self.Weapon(self)


class PlagueMarineFighter(KTModel):
    desc = points.PlagueMarineFighter
    datasheet = 'Plague Marine'
    type_id = 'plague_fighter_v1'
    gears = [points.PlagueKnife, points.BlightGrenade, points.KrakGrenade]
    specs = ['Zealot', 'Combat', 'Demolitions', 'Veteran']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(PlagueMarineFighter.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.BuboticAxe)
            self.variant(*points.GreatPlagueCleaver)
            self.variant(*points.FlailOfCorruption)
            self.variant(*points.PlagueKnife)
            self.variant('Mace of contagion and Bubotic axe',
                         points_price(0, points.BuboticAxe, points.MaceOfContagion),
                         gear=create_gears(points.BuboticAxe, points.MaceOfContagion))

    def __init__(self, parent):
        super(PlagueMarineFighter, self).__init__(parent)
        self.Weapon(self)


class PlagueChampion(KTModel):
    desc = points.PlagueChampion
    datasheet = 'Plague Marine'
    type_id = 'plague_champion_v1'
    gears = [points.BlightGrenade, points.KrakGrenade]
    specs = ['Leader', 'Demolitions', 'Combat', 'Veteran']
    limit = 1

    class Knife(OneOf):
        def __init__(self, parent):
            super(PlagueChampion.Knife, self).__init__(parent, 'Secondary weapon')
            self.variant(*points.PlagueKnife)
            self.variant(*points.Plaguesword)

    class Gun(OneOf):
        def __init__(self, parent):
            super(PlagueChampion.Gun, self).__init__(parent, 'Primary weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.BoltPistol)
            self.variant(*points.PlasmaPistol)
            self.variant(*points.PlasmaGun)

    class Fist(OptionsList):
        def __init__(self, parent):
            super(PlagueChampion.Fist, self).__init__(parent, '')
            self.variant(*points.PowerFist)

    def __init__(self, parent):
        super(PlagueChampion, self).__init__(parent)
        self.Gun(self)
        self.Knife(self)
        self.Fist(self)
