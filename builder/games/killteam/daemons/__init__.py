from builder.games.killteam.roster import KTRoster
from .units import Bloodletter, BloodletterHornblower, BloodletterIconBearer,\
    Bloodreaper, Daemonette, DaemonetteHornblower, DaemonetteIconBearer, Alluress,\
    Horror, HorrorHornblower, HorrorIconBearer, IridescentHorror, Plaguebearer,\
    PlaguebearerIconBearer, PlaguebearerHornblower, Plagueridden


class DaemonKT(KTRoster):
    army_name = 'Chaos Daemons Kill Team'
    army_id = 'daemon_kt_v1'
    unit_types = [Bloodletter, BloodletterHornblower,
                  BloodletterIconBearer, Bloodreaper, Daemonette,
                  DaemonetteHornblower, DaemonetteIconBearer,
                  Alluress, Horror, HorrorHornblower,
                  HorrorIconBearer, IridescentHorror, Plaguebearer,
                  PlaguebearerIconBearer, PlaguebearerHornblower,
                  Plagueridden]

