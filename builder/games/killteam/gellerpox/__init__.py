__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import NightmareHulk, GnasherScreamer, GellerpoxMutant, Glitchling,\
    EyestingerSwarm, Cursemite, SludgeGrub
from .commanders import Vulgar


class GellerpoxKT(KTRoster):
    army_name = 'Gellerpox Infected Kill Team'
    army_id = 'gellerpox_kt_v1'
    unit_types = [NightmareHulk, GnasherScreamer, GellerpoxMutant,
                  Glitchling, EyestingerSwarm, Cursemite, SludgeGrub]


class ComGellerpoxKT(KTCommanderRoster, GellerpoxKT):
    army_name = 'Gellerpox Infected Kill Team'
    army_id = 'gellerpox_kt_v2_com'
    commander_types = [Vulgar]
