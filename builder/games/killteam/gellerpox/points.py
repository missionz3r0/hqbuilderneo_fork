__author__ = 'Ivan Truskov'

# units
Cursemite = ('Cursemite', 4)
EyestingerSwarm = ('Eyestinger Swarm', 5)
GellerpoxMutant = ('Gellerpox Mutant', 8)
Glitchling = ('Glitchling', 5)
NightmareHulk = ('Nightmare Hulk', 31)
GnasherScreamer = ('Gnasher-Screamer', 31)
SludgeGrub = ('Sludge-grub', 5)
VulgarThriceCursed = ('Vulgar Thrice-Cursed', [65, 85, 105, 130])

# Ranged
AcidSpit = ('Acid spit', 0)
BellyFlamer = ('Belly-Flamer', 0)
FragGrenades = ('Frag grenades', 0)

# Melee
BloodsuckingProboscis = ('Bloodsucking proboscis', 0)
DiseasedClawsAndFangs = ('Diseased claws and fangs', 0)
FangedMawAndStinger = ('Fanged maw and stinger', 0)
FleshripperClaws = ('Fleshripper claws', 0)
HidedousMutations = ('Hidedous mutations', 0)
MutatedLimbsAndImprovisedWeapons = ('Mutated limbs and improvised weapons', 0)
PlagueCleaver = ('Plague cleaver', 0)
SpawningBarb = ('Spawning barb', 0)
