__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel, KTModelNoSpec
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class NightmareHulk(KTModel):
    desc = points.NightmareHulk
    type_id = 'nightmare_hulk_v1'
    gears = [points.HidedousMutations]
    specs = ['Combat', 'Veteran', 'Zealot']


class GnasherScreamer(KTModel):
    desc = points.GnasherScreamer
    datasheet = 'Nightmare Hulk'
    type_id = 'gnasher_v1'
    gears = [points.HidedousMutations, points.PlagueCleaver]
    specs = ['Leader', 'Combat', 'Veteran', 'Zealot']
    limit = 1


class GellerpoxMutant(KTModel):
    desc = points.GellerpoxMutant
    type_id = 'gellerpox_mutant_v1'
    gears = [points.FragGrenades, points.MutatedLimbsAndImprovisedWeapons]
    specs = ['Leader', 'Combat', 'Demolitions', 'Scout', 'Veteran', 'Zealot']


class Glitchling(KTModel):
    desc = points.Glitchling
    type_id = 'glitchling_v1'
    gears = [points.DiseasedClawsAndFangs]
    specs = ['Combat', 'Scout', 'Zealot']


class EyestingerSwarm(KTModelNoSpec):
    desc = points.EyestingerSwarm
    type_id = 'eyestingers_v1'
    gears = [points.SpawningBarb]


class Cursemite(KTModelNoSpec):
    desc = points.Cursemite
    type_id = 'cursemite_v1'
    gears = [points.BloodsuckingProboscis]


class SludgeGrub(KTModelNoSpec):
    desc = points.SludgeGrub
    type_id = 'sludge_grub_v1'
    gears = [points.AcidSpit, points.FangedMawAndStinger]
