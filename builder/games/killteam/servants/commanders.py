__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class Obsidius(KTCommander):
    desc = points.ObsidiusMallex
    type_id = 'obsidius_mallex_v1'
    gears = [points.ThunderHammer, points.PlasmaPistol]
    specs = ['Fortitude']
