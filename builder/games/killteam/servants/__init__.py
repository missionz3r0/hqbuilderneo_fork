__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import TraitorGuardsman, TraitorGuardsmanGunner, TraitorGuardsmanSergeant,\
    ChaosBeastman, NegavoltCultist, RoguePsyker, BlackLegionnaire
from .commanders import Obsidius


class ServantKT(KTRoster):
    army_name = 'Servants of the Abyss Kill Team'
    army_id = 'servant_kt_v1'
    unit_types = [TraitorGuardsman, TraitorGuardsmanGunner,
                  TraitorGuardsmanSergeant, ChaosBeastman, NegavoltCultist,
                  RoguePsyker, BlackLegionnaire]


class ComServantKT(KTCommanderRoster, ServantKT):
    army_name = 'Servants of the Abyss Kill Team'
    army_id = 'servant_kt_v2_com'
    commander_types = [Obsidius]
