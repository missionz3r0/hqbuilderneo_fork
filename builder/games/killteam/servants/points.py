__author__ = 'Ivan Truskov'

# units
BlackLegionnaire = ('Black Legionnaire', 12)
ChaosBeastman = ('Chaos Beastman', 7)
NegavoltCultist = ('Negavolt Cultist', 9)
ObsidiusMallex = ('Obsidius Mallex', [125, 140, 155, 170])
RoguePsyker = ('Rogue Psyker', 20)
TraitorGuardsman = ('Traitor Guardsman', 5)
TraitorGuardsmanGunner = ('Traitor Guardsman Gunner', 5)
TraitorSergeant = ('Traitor Sergeant', 5)

# Ranged
Autopistol = ('Autopistol', 0)
Boltgun = ('Boltgun', 0)
BoltPistol = ('Bolt pistol', 0)
Flamer = ('Flamer', 3)
FragGrenades = ('Frag grenades', 0)
KrakGrenades = ('Krak grenades', 0)
Lasgun = ('Lasgun', 0)
Laspistol = ('Laspistol', 0)
PlasmaPistol = ('Plasma pistol', 0)

# Melee
BrutalAssaultWeapon = ('Brutal assault weapon', 0)
Chainsword = ('Chainsword', 0)
ChaosStave = ('Chaos stave', 0)
ElectroGoads = ('Electro-goads', 0)
ThunderHammer = ('Thunder hammer', 0)
