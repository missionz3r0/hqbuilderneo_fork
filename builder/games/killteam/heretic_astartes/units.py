__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Cultist(KTModel):
    desc = points.ChaosCultist
    type_id = 'ccultist_v1'
    specs = ['Combat', 'Demolitions', 'Veteran', 'Zealot']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Cultist.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Autogun)
            self.variant('Brutal assault weapon and autopistol',
                         gear=create_gears(points.BrutalAssaultWeapon, points.Autopistol))

    def __init__(self, parent):
        super(Cultist, self).__init__(parent)
        self.Weapon(self)


class CultistGunner(KTModel):
    desc = points.ChaosCultistGunner
    datasheet = 'Chaos Cultist'
    type_id = 'ccultist_gunner_v1'
    specs = ['Heavy', 'Combat', 'Demolitions', 'Veteran', 'Zealot']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CultistGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Autogun)
            self.variant(*points.Flamer)
            self.variant(*points.HeavyStubber)

    def __init__(self, parent):
        super(CultistGunner, self).__init__(parent)
        self.Weapon(self)


class CultistChampion(KTModel):
    desc = points.CultistChampion
    datasheet = 'Chaos Cultist'
    type_id = 'ccultist_champion_v1'
    specs = ['Leader', 'Combat', 'Demolitions', 'Veteran', 'Zealot']
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CultistChampion.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Autogun)
            self.variant(*points.Shotgun)
            self.variant('Brutal assault weapon and autopistol',
                         gear=create_gears(points.BrutalAssaultWeapon, points.Autopistol))

    def __init__(self, parent):
        super(CultistChampion, self).__init__(parent)
        self.Weapon(self)


class ChaosMarine(KTModel):
    desc = points.ChaosSpaceMarine
    type_id = 'csm_v1'
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Demolitions', 'Sniper', 'Veteran', 'Zealot']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ChaosMarine.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.Chainsword)

    class ChaosIcon(OptionsList):
        def __init__(self, parent):
            super(ChaosMarine.ChaosIcon, self).__init__(parent, 'Icon of Chaos', limit=1)
            self.variant(*points.IconOfWrath)
            self.variant(*points.IconOfExcess)
            self.variant(*points.IconOfDespair)
            self.variant(*points.IconOfFlame)
            self.variant(*points.IconOfVengeance)

    def __init__(self, parent):
        super(ChaosMarine, self).__init__(parent)
        self.Weapon(self)
        self.icon = self.ChaosIcon(self)

    def get_unique_gear(self):
        return ['Icon of Chaos'] if self.icon.any else []


class ChaosMarineGunner(KTModel):
    desc = points.ChaosSpaceMarineGunner
    datasheet = 'Chaos Space Marine'
    type_id = 'csm_guner_v1'
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Heavy', 'Demolitions', 'Sniper', 'Veteran', 'Zealot']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ChaosMarineGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.group1 = [
                self.variant(*points.Flamer),
                self.variant(*points.Meltagun),
                self.variant(*points.PlasmaGun)
            ]
            self.group2 = [
                self.variant(*points.HeavyBolter)
            ]

    def __init__(self, parent):
        super(ChaosMarineGunner, self).__init__(parent)
        self.gun = self.Weapon(self)

    def get_unique_gear(self):
        if self.gun.cur in self.gun.group1:
            return ['Special weapon']
        elif self.gun.cur in self.gun.group2:
            return ['Heavy Bolter']
        pass


class AspiringChampion(KTModel):
    desc = points.AspiringChampion
    datasheet = 'Chaos Space Marine'
    type_id = 'aspiring_champion_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Leader', 'Demolitions', 'Sniper', 'Veteran', 'Zealot']
    limit = 1

    class Pistol(OneOf):
        def __init__(self, parent):
            super(AspiringChampion.Pistol, self).__init__(parent, 'Secondary weapon')
            self.variant(*points.BoltPistol)
            self.variant(*points.PlasmaPistol)

    class Sword(OneOf):
        def __init__(self, parent):
            super(AspiringChampion.Sword, self).__init__(parent, 'Primary weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.Chainsword)
            self.variant(*points.PowerSword)
            self.variant(*points.PowerFist)

    def __init__(self, parent):
        super(AspiringChampion, self).__init__(parent)
        self.Sword(self)
        self.Pistol(self)
