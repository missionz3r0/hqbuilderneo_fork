__author__ = 'Ivan Truskov'

# units
ChaosCultist = ('Chaos Cultist', 4)
ChaosCultistGunner = ('Chaos Cultist Gunner', 5)
CultistChampion = ('Cultist Champion', 5)
ChaosSpaceMarine = ('Chaos Space Marine', 12)
ChaosSpaceMarineGunner = ('Chaos Space Marine Gunner', 13)
AspiringChampion = ('Aspiring Champion', 13)

ExaltedChampion = ('Exalted Champion', [30, 35, 50, 70])
Sorcerer = ('Sorcerer', [65, 80, 95, 120])

# Ranged
Autogun = ('Autogun', 0)
Autopistol = ('Autopistol', 0)
BoltPistol = ('Bolt pistol', 0)
Boltgun = ('Boltgun', 0)
Flamer = ('Flamer', 3)
FragGrenade = ('Frag grenade', 0)
HeavyBolter = ('Heavy bolter', 3)
HeavyStubber = ('Heavy stubber', 0)
KrakGrenade = ('Krak grenade', 0)
Meltagun = ('Meltagun', 3)
PlasmaGun = ('Plasma gun', 3)
PlasmaPistol = ('Plasma pistol', 1)
Shotgun = ('Shotgun', 0)

ExPlasmaPistol = ('Plasma Pistol', 4)

# Melee
BrutalAssaultWeapon = ('Brutal assault weapon', 0)
Chainsword = ('Chainsword', 0)
PowerFist = ('Power fist', 4)
PowerSword = ('Power sword', 2)

ExPowerAxe = ('Power axe', 5)
ExPowerFist = ('Power fist', 12)
ExPowerSword = ('Power sword', 4)
ForceSword = ('Force sword', 0)
ForceStave = ('Forcer stave', 0)
# gear
IconOfWrath = ('Icon of Wrath', 5)
IconOfFlame = ('Icon of Flame', 1)
IconOfDespair = ('Icon of Despair', 3)
IconOfExcess = ('Icon of Excess', 5)
IconOfVengeance = ('Icon of Vengeance', 1)
