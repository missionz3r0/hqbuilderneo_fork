__author__ = 'Ivan Truskov'


from builder.core2 import Roster
from .section import FireTeams, Commanders


class KTRoster(Roster):
    game_name = 'Kill Team'
    base_tags = ['Kill team']
    roster_type = 'Standard'
    unit_types = []

    def __init__(self, parent=None, limit=100, secs=[]):
        self.fire_teams = FireTeams(self)
        super(KTRoster, self).__init__(secs + [self.fire_teams], parent,
                                       limit=limit)
        for t in self.unit_types:
            self.fire_teams.unit_type_add(t)

    def check_rules(self):
        super(KTRoster, self).check_rules()
        leader_cnt = sum(u.spec.is_leader() for u in self.fire_teams.units)
        if leader_cnt != 1:
            self.error('Kill team must include exactly one leader')

        specs = [u.spec.value for u in self.fire_teams.units if not u.spec.free and u.spec.is_spec()]
        if len(specs) > 3:
            self.error('No more then 3 specialists may be included in Kill Team')
        if len(set(specs)) != len(specs):
            self.error('Specialists in the kill team must not repeat')

    def dump(self):
        res = super(KTRoster, self).dump()
        res.update({
            'statistics': self.build_statistics()
        })
        return res

    @property
    def delta(self):
        res = super(KTRoster, self).delta
        res.update({
            'statistics': self.build_statistics()
        })
        return res


class KTCommanderRoster(KTRoster):
    roster_type = 'Commander'
    commander_types = []

    def __init__(self, parent=None):
        coms = Commanders(self)
        super(KTCommanderRoster, self).__init__(parent, 200, [coms])
        for t in self.commander_types:
            coms.unit_type_add(t)
