__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class DWPrimarisCaptain(KTCommander):
    desc = points.PrimarisCaptain
    type_id = 'dw_primaris_captain_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Strategist', 'Strength']
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DWPrimarisCaptain.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.MCAutoBoltRifle)
            self.variant(*points.MCStalkerBoltRifle)

    class Sword(OptionsList):
        def __init__(self, parent):
            super(DWPrimarisCaptain.Sword, self).__init__(parent, '')
            self.variant(*points.CapPowerSword)

    def __init__(self, parent):
        super(DWPrimarisCaptain, self).__init__(parent)
        self.Weapon(self)
        self.Sword(self)


class DWPrimarisChaplain(KTCommander):
    desc = points.PrimarisChaplain
    type_id = 'dw_primaris_chaplain_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Melee', 'Shooting', 'Strength']
    gears = [points.CroziusArcanum, points.AbsolvorBoltPistol, points.FragGrenade, points.KrakGrenade]


class DWPrimarisLibrarian(KTCommander):
    desc = points.PrimarisLibrarian
    type_id = 'dw_primaris_librarian_v1'
    specs = ['Fortitude', 'Melee', 'Psyker', 'Shooting', 'Strength']
    gears = [points.ForceSword, points.BoltPistol, points.FragGrenade, points.KrakGrenade]


class WatchMaster(KTCommander):
    desc = points.WatchMaster
    type_id = 'watch_master_v1'
    specs = ['Fortitude', 'Leadership', 'Logistics', 'Melee', 'Shooting', 'Strategist', 'Strength']
    gears = [points.GuardianSpear, points.FragGrenade, points.KrakGrenade]
