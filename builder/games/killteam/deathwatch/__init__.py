from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import DeathwatchVeteran, DeathwatchVeteranGunner, DeathwatchBlackShield,\
    DeathwatchVeteranSergeant, DWIntercessor, DWIntercessorGunner, DWIntercessorSergeant,\
    DWReiver, DWReiverSergeant
from .commanders import DWPrimarisCaptain, DWPrimarisChaplain, DWPrimarisLibrarian, WatchMaster
from builder.games.killteam.astra_militarum import Eisenhorn


class DeathwatchKT(KTRoster):
    army_name = 'Deathwatch Kill Team'
    army_id = 'deathwatch_kt_v1'
    unit_types = [DeathwatchVeteran, DeathwatchVeteranGunner,
                  DeathwatchBlackShield, DeathwatchVeteranSergeant, DWIntercessor,
                  DWIntercessorGunner, DWIntercessorSergeant, DWReiver,
                  DWReiverSergeant]


class ComDeathwatchKT(KTCommanderRoster, DeathwatchKT):
    army_name = 'Deathwatch Kill Team'
    army_id = 'deathwatch_kt_v2_com'
    commander_types = [DWPrimarisCaptain, DWPrimarisChaplain,
                       DWPrimarisLibrarian, WatchMaster, Eisenhorn]
