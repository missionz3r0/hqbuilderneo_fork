__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class TzaangorShaman(KTCommander):
    desc = points.TzaangorShaman
    type_id = 'tzaangor_shaman_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Melee', 'Psyker', 'Strategist', 'Strength']
    gears = [points.ForceStave, points.ShamanDisc]


class ExaltedSorcerer(KTCommander):
    desc = points.ExaltedSorcerer
    type_id = 'exalted_sorcerer_v1'
    specs = ['Ferocity', 'Fortitude', 'Leadership', 'Logistics', 'Melee', 'Psyker', 'Shooting', 'Strategist', 'Strength']
    gears = [points.ForceStave, points.FragGrenades, points.KrakGrenades]

    class Ride(OptionsList):
        def __init__(self, parent):
            super(ExaltedSorcerer.Ride, self).__init__(parent, 'Mount')
            self.variant(*points.Disc)

    class Pistol(OneOf):
        def __init__(self, parent):
            super(ExaltedSorcerer.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.InfernoBoltPistol)
            self.variant(*points.PlasmaPistol)
            self.variant(*points.ExWarpflamePistol)

    class Sword(OptionsList):
        def __init__(self, parent):
            super(ExaltedSorcerer.Sword, self).__init__(parent, 'Sword')
            self.variant(*points.PowerSword)

    def __init__(self, parent):
        super(ExaltedSorcerer, self).__init__(parent)
        self.Storm(self)
        self.Pistol(self)
        self.Ride(self)
