__author__ = 'Ivan Truskov'

# units
RubricMarine = ('Rubric Marine', 16)
RubricMarineGunner = ('Rubric Marine Gunner', 16)
AspiringSorcerer = ('Asdpiring Sorcerer', 17)
Tzaangor = ('Tzaangor', 7)
Twistbray = ('Twistbray', 8)

ExaltedSorcerer = ('Exalted Sorcerer', [81, 101, 121, 146])
TzaangorShaman = ('Tzaangor Shaman', [40, 55, 70, 95])

# Ranged
Autopistol = ('Autopistol', 0)
InfernoBoltPistol = ('Inferno bolt pistol', 0)
InfernoBoltgun = ('Inferno boltgun', 0)
SoulreaperCannon = ('Soulreaper cannon', 4)
WarpflamePistol = ('Warpflame pistol', 1)
Warpflamer = ('Warpflamer', 4)

PlasmaPistol = ('Plasma pistol', 7)
ExWarpflamePistol = ('Warpflame pistol', 7)
FragGrenades = ('Frag grenades', 0)
KrakGrenades = ('Krak grenades', 0)
# Melee
Chainsword = ('Chainsword', 0)
ForceStave = ('Force stave', 0)
TzaangorBlades = ('Tzaangor blades', 0)

PowerSword = ('Power sword', 0)
# gear
Brayhorn = ('Brayhorn', 3)
IconOfFlame = ('Icon of Flame', 1)

Disc = ('Disc of Tzeentch', 20)
ShamanDisc = ('Disc of Tzeentch', 0)
