__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class RubricMarine(KTModel):
    desc = points.RubricMarine
    type_id = 'rubric_marine_v1'
    specs = ['Combat', 'Demolitions', 'Veteran']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RubricMarine.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.InfernoBoltgun)
            self.variant(*points.Warpflamer)

    class ChaosIcon(OptionsList):
        def __init__(self, parent):
            super(RubricMarine.ChaosIcon, self).__init__(parent, 'Icon of Chaos', limit=1)
            self.variant(*points.IconOfFlame)

    def __init__(self, parent):
        super(RubricMarine, self).__init__(parent)
        self.Weapon(self)
        self.icon = self.ChaosIcon(self)

    def get_unique_gear(self):
        return ['Icon of Flame'] if self.icon.any else []


class RubricMarineGunner(KTModel):
    desc = points.RubricMarineGunner
    datasheet = 'Rubric Marine'
    type_id = 'rubric_guner_v1'
    specs = ['Heavy', 'Combat', 'Demolitions', 'Veteran']
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RubricMarineGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.InfernoBoltgun)
            self.variant(*points.SoulreaperCannon)

    def __init__(self, parent):
        super(RubricMarineGunner, self).__init__(parent)
        self.gun = self.Weapon(self)


class AspiringSorcerer(KTModel):
    desc = points.AspiringSorcerer
    datasheet = 'Rubric Marine'
    type_id = 'aspiring_sorcerer_v1'
    gears = [points.ForceStave]
    specs = ['Leader', 'Demolitions', 'Combat', 'Veteran']
    limit = 1

    class Gun(OneOf):
        def __init__(self, parent):
            super(AspiringSorcerer.Gun, self).__init__(parent, 'Pistol')
            self.variant(*points.InfernoBoltPistol)
            self.variant(*points.WarpflamePistol)

    def __init__(self, parent):
        super(AspiringSorcerer, self).__init__(parent)
        self.Gun(self)


class Tzaangor(KTModel):
    desc = points.Tzaangor
    type_id = 'tzaangor_v1'
    specs = ['Comms', 'Combat', 'Medic', 'Veteran', 'Zealot']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Tzaangor.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*points.TzaangorBlades)
            self.variant('Chainsword and autopistol',
                         gear=create_gears(points.Chainsword, points.Autopistol))

    class Horn(OptionsList):
        def __init__(self, parent):
            super(Tzaangor.Horn, self).__init__(parent, 'Options')
            self.variant(*points.Brayhorn)

    def __init__(self, parent):
        super(Tzaangor, self).__init__(parent)
        self.Weapon(self)
        self.horn = self.Horn(self)

    def check_rules(self):
        super(Tzaangor, self).check_rules()
        self.spec.toggle_spec('Comms', self.horn.any)

    def get_unique_gear(self):
        return self.horn.description


class Twistbray(KTModel):
    desc = points.Twistbray
    datasheet = 'Tzaangor'
    type_id = 'twistbray_v1'
    specs = ['Leader', 'Combat', 'Medic', 'Veteran', 'Zealot']
    limit = 1

    def __init__(self, parent):
        super(Twistbray, self).__init__(parent)
        Tzaangor.Weapon(self)
