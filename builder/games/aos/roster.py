__author__ = 'Ivan Truskov'

from builder.core2 import Roster, OneOf
from .sections import LeaderSection, BattlelineSection, ArtillerySection,\
    BehemothSection, OtherSection, WarscrollSection, BattlefieldRole


class AosRoster(Roster):
    game_name = 'Age of Sigmar'
    base_tags = ['Age of Sigmar']
    development = True

    def __init__(self, limit=None):
        self.leaders = LeaderSection(self)
        self.battleline = BattlelineSection(self)
        self.artillery = ArtillerySection(self)
        self.behemoths = BehemothSection(self)
        self.other = OtherSection(self)
        self.batallions = WarscrollSection(self)
        self.short_sec = {}
        for sec in [self.leaders, self.battleline,
                    self.artillery, self.behemoths,
                    self.other]:
            self.short_sec[sec.role] = sec
        super(AosRoster, self).__init__([self.leaders, self.battleline,
                                         self.artillery, self.behemoths,
                                         self.other, self.batallions],
                                        parent=None, limit=limit)
        self.cur_allegiance = OneOf(self, 'Allegiance', used=False)
        self.make_property(self.cur_allegiance)
        self.allegiance = None

    def check_rules(self):
        super(AosRoster, self).check_rules()
        # first, find out current allegiance
        allegiances = set()
        for sec in self.sections:
            for unit in sec.units:
                allegiances.add(unit.allegiance)
        if len(allegiances) == 1:
            self.allegiance = allegiances.pop()
        else:
            self.allegiance = None

        # move units between battleline and others
        to_move = []
        for unit in self.other.units:
            if 'battleline' in unit.get_roles():
                to_move.append(unit)
        self.battleline.move_units(to_move)
        to_move = []
        for unit in self.battleline.units:
            if not('battleline' in unit.get_roles()):
                to_move.append(unit)
        self.other.move_units(to_move)
        self.check_battleroles()
        self.apply_allegiance()

    def check_battleroles(self):

        # count by roles and check limits
        rolecheck = {}
        for k in list(self.short_sec.keys()):
            rolecheck[k] = 0

        for sec in list(self.short_sec.values()):
            for u in sec.units:
                for r in u.get_roles():
                    rolecheck[r] += 1
        for bat in self.batallions.units:
            for u in bat.sub_roster.roster.units:
                for r in self.get_roles():
                    rolecheck[r] += 1
        for k in list(self.short_sec.keys()):
            sec = self.short_sec[k]
            if sec.min and rolecheck[k] < sec.min:
                self.error("There must be no less then {0} units with battlefield role {1}" \
                           .format(sec.min, sec.name))
            if sec.max and rolecheck[k] > sec.max:
                self.error("There must be no more then {0} units with battlefield role {1}" \
                           .format(sec.max, sec.name))

    def apply_allegiance(self):
        # set current allegiance to be visible
        for sec in self.sections:
            sec.show_allegiance(self.cur_allegiance.cur.title)

    def collect_allegiances(self):
        allegiances = set()
        for sec in self.sections:
            for ut in sec.types:
                if ut.unit_class.allegiance:
                    allegiances.add(ut.unit_class.allegiance)
        for a in list(allegiances):
            self.cur_allegiance.variant(a)
        self.apply_allegiance()

    def add_units(self, unit_types):
        for ut in unit_types:
            role = ut.roles[0] if ut.roles else None
            BattlefieldRole.build_unit_type(self.short_sec[role], ut)

    def add_batallions(self, warscroll_batallions):
        for wb in warscroll_batallions:
            WarscrollSection.build_description(self.batallions, wb)


class AosVanguard(AosRoster):
    roster_type = 'Vanguard'

    def __init__(self):
        super(AosVanguard, self).__init__(limit=1000)


class AosBattlehost(AosRoster):
    roster_type = 'Battlehost'

    def __init__(self):
        super(AosBattlehost, self).__init__(limit=2000)
        self.leaders.max = 6
        self.battleline.min = 3
        self.artillery.max = 4
        self.behemoths.max = 4


class AosWarhost(AosRoster):
    roster_type = 'Warhost'

    def __init__(self):
        super(AosWarhost, self).__init__(limit=2500)
        self.leaders.max = 8
        self.battleline.min = 4
        self.artillery.max = 5
        self.behemoths.max = 5


class AosPointsOnly(AosRoster):
    roster_type = 'Points only army'

    def __init__(self):
        super(AosPointsOnly, self).__init__(limit=1500)

    def check_battleroles(self):
        pass
