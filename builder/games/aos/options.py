author = 'Ivan Truskov'

from builder.core2 import Count, UnitDescription


class ModelCount(Count):
    def __init__(self, parent, name, min_limit, max_limit, points):
        flag = max_limit != min_limit
        super(ModelCount, self).__init__(
            parent=parent, name=name, points=points, min_limit=min_limit,
            max_limit=max_limit, node_id=None, active=flag,
            visible=flag, used=True, per_model=min_limit == 1,
            order=0, gear=UnitDescription(name=parent.model_name))

    def dump(self):
        res = super(ModelCount, self).dump()
        res.update({
            'type': 'model_count'
        })
        return res

    @property
    def points(self):
        return (self.cur / self.min) * self.option_points
