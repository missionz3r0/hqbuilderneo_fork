from builder.core2 import Gear, UnitDescription
from builder.games.aos.unit import Unit

__author__ = 'Ivan Truskov'

com_al = 'Stormcast Eternals'


class Judicitators(Unit):
    type_name = 'Judicitators'
    type_id = 'jud_v1'

    model_name = 'Judicitator'
    min_size = 5
    max_size = 20
    default_points = 160
    allegiance = com_al

    roles = ['battleline']


class Liberators(Unit):
    type_name = 'Liberators'
    type_id = 'lib_v1'

    model_name = 'Liberator'
    min_size = 5
    max_size = 20
    default_points = 100
    allegiance = com_al

    roles = ['battleline']


class Prime(Unit):
    type_name = 'Celestant-Prime, Hammer of Sigmar'
    type_id = 'prime_v1'

    model_name = 'Celestant Prime'
    default_points = 360
    allegiance = com_al

    roles = ['leader']

    def __init__(self, parent):
        super(Prime, self).__init__(parent, unique=True)


class Azyros(Unit):
    type_name = 'Knight-Azyros'
    type_id = 'azyr_v1'

    model_name = type_name
    default_points = 100
    allegiance = com_al

    roles = ['leader']


class Heraldor(Unit):
    type_name = 'Knight-Heraldor'
    type_id = 'heraldor_v1'

    model_name = type_name
    default_points = 120
    allegiance = com_al

    roles = ['leader']


class Venator(Unit):
    type_name = 'Knight-Venator'
    type_id = 'vena_v1'

    model_name = type_name
    default_points = 120
    allegiance = com_al

    roles = ['leader']


class Vexillor(Unit):
    type_name = 'Knight-Vexillor'
    type_id = 'vexi_v1'

    model_name = type_name
    default_points = 200
    allegiance = com_al

    roles = ['leader']


class Castellant(Unit):
    type_name = 'Lord-Castellant'
    type_id = 'castel_v1'

    model_name = type_name
    default_points = 100
    allegiance = com_al

    roles = ['leader']


class Celestant(Unit):
    type_name = 'Lord-Celestant'
    type_id = 'celes_v1'

    model_name = type_name
    default_points = 100
    allegiance = com_al

    roles = ['leader']


class CelestantDracoth(Unit):
    type_name = 'Lord-Celestant on Dracoth'
    type_id = 'celes_drac_v1'

    model_name = 'Lord-Celestant'
    default_points = 220
    allegiance = com_al

    roles = ['leader']

    def __init__(self, parent):
        super(CelestantDracoth, self).__init__(parent,
                                               gear=[UnitDescription('Dracoth')])


class Relictor(Unit):
    type_name = 'Lord-Relictor'
    type_id = 'relic_v1'

    model_name = type_name
    default_points = 80
    allegiance = com_al

    roles = ['leader']


class Decimators(Unit):
    type_name = 'Decimators'
    type_id = 'dec_v1'

    model_name = 'Decimator'
    min_size = 5
    max_size = 20
    default_points = 200
    allegiance = com_al


class GryphHounds(Unit):
    type_name = 'Gryph-hounds'
    type_id = 'gryph_v1'

    model_name = 'Gryph-hound'
    min_size = 1
    max_size = 4
    default_points = 40
    allegiance = com_al


class HammerProsecutors(Unit):
    type_name = 'Prosecutors with Celestial Hammers'
    type_id = 'prosec_ham_v1'

    model_name = 'Prosecutor'
    min_size = 3
    max_size = 12
    default_points = 100
    allegiance = com_al

    def __init__(self, parent):
        super(HammerProsecutors, self).__init__(parent)
        self.models.gear.add(Gear('Celestial Hammer'))


class JavelinProsecutors(Unit):
    type_name = 'Prosecutors with Stormcall Javelins'
    type_id = 'prosec_jav_v1'

    model_name = 'Prosecutor'
    min_size = 3
    max_size = 12
    default_points = 80
    allegiance = com_al

    def __init__(self, parent):
        super(JavelinProsecutors, self).__init__(parent)
        self.models.gear.add(Gear('Stormcall Javelin'))


class Protectors(Unit):
    type_name = 'Protectors'
    type_id = 'prot_v1'

    model_name = 'Protector'
    min_size = 5
    max_size = 20
    default_points = 200
    allegiance = com_al


class Retributors(Unit):
    type_name = 'Retributors'
    type_id = 'prot_v1'

    model_name = 'Retributor'
    min_size = 5
    max_size = 20
    default_points = 220
    allegiance = com_al
