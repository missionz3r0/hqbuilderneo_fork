__author__ = 'Ivan Truskov'

from builder.core2.section import Section
from builder.core2 import SubRoster, Unit, UnitType


class BattlefieldRole(Section):
    def check_limits(self):
        return True             # limits checks are done by roster

    def __init__(self, *args, **kwargs):
        role = kwargs.pop('role', None)
        super(BattlefieldRole, self).__init__(*args, **kwargs)
        self.role = role

    @staticmethod
    def build_unit_type(instance, unit_class, *args, **kwargs):
        return UnitType(instance, unit_class, *args, **kwargs)

    def show_allegiance(self, allegiance):
        for ut in self.types:
            ut.visible = ut.unit_class.allegiance == allegiance


class LeaderSection(BattlefieldRole):
    def __init__(self, parent, min_limit=1, max_limit=4):
        super(LeaderSection, self).__init__(parent, 'ld', 'Leaders',
                                            min_limit, max_limit,
                                            role='leader')


class BattlelineSection(BattlefieldRole):
    def __init__(self, parent, min_limit=2):
        super(BattlelineSection, self).__init__(parent, 'bl', 'Battleline',
                                                min_limit, None,
                                                role='battleline')


class ArtillerySection(BattlefieldRole):
    def __init__(self, parent, max_limit=2):
        super(ArtillerySection, self).__init__(parent, 'art', 'Artillery',
                                               0, max_limit,
                                               role='artillery')


class BehemothSection(BattlefieldRole):
    def __init__(self, parent, max_limit=2):
        super(BehemothSection, self).__init__(parent, 'bh', 'Behemoths',
                                              0, max_limit,
                                              role='behemoth')


class OtherSection(BattlefieldRole):
    def __init__(self, parent):
        super(OtherSection, self).__init__(parent, 'oth', 'Other Units',
                                           None, None)


class WarscrollSection(BattlefieldRole):
    def __init__(self, parent):
        super(WarscrollSection, self).__init__(parent, 'scr', 'Warscroll Batallions',
                                               None, None)

    @staticmethod
    def build_batallion(instance, detach_class, *args, **kwargs):
        class DetachUnit(Unit):
            type_id = detach_class.army_id
            type_name = detach_class.army_name
            allegiance = detach_class.allegiance

            def __init__(self, parent):
                super(DetachUnit, self).__init__(parent)
                detach = detach_class()
                detach.parent = self
                self.sub_roster = SubRoster(self, detach)

            def dump(self):
                data = super(DetachUnit, self).dump()
                data['detach_unit'] = True
                return data

            def get_unique_map(self, names_getter):
                return self.sub_roster.roster._build_unique_map(names_getter)

            def build_statistics(self):
                return self.sub_roster.roster.build_statistics()

        return UnitType(instance, DetachUnit, *args, **kwargs)
