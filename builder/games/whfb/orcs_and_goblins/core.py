__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.core.unit import OptionsList, OneOf
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.orcs_and_goblins import orcs_magic_standards, filter_items


class Boyz(FCGUnit):
    name = 'Orc Boyz'

    def __init__(self):
        self.big = OptionsList('opt', '', [['Big \'Un', 2, 'bu']])
        FCGUnit.__init__(
            self, model_name='Orc Boy', model_price=6, min_models=10,
            musician_price=10, standard_price=10, champion_name='Orc Boss', champion_price=15,
            base_gear=['Light armour', 'Hand weapon'],
            magic_banners=filter_items(orcs_magic_standards, 50), magic_banner_check=lambda: self.big.get('bu'),
            options=[OptionsList('wep', 'Weapon', [
                ['Spear', 1, 'sp'],
                ['Hand weapon', 1, 'hw'],
            ], limit=1), OptionsList('arm', '', [
                ['Shield', 1, 'sh'],
            ]), self.big]
        )

    def is_big_uns(self):
        return self.big.get('bu')


class ArrerBoyz(FCGUnit):
    name = 'Orc Arrer Boyz'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Orc Arrer Boy', model_price=7, min_models=10,
            musician_price=10, standard_price=10, champion_name='Orc Arrer Boy Boss', champion_price=10,
            base_gear=['Light armour', 'Hand weapon', 'Bow'],
        )


class SavageBoyz(FCGUnit):
    name = 'Savage Orcs'

    def __init__(self):
        self.big = OptionsList('opt', '', [['Big \'Un', 2, 'bu']])
        FCGUnit.__init__(
            self, model_name='Savage Orc', model_price=8, min_models=10,
            musician_price=10, standard_price=10, champion_name='Savage Orc Boss', champion_price=15,
            base_gear=['Hand weapon'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Spear', 1, 'sp'],
                    ['Hand weapon', 1, 'hw'],
                    ['Bow', 1, 'hw'],
                ], limit=1),
                self.big,
                OptionsList('opt', 'Options', [['Big \'Un', 2, 'bu']])]
        )
        self.stabba = self.opt_options_list('', [['Big Stabbas', 20, 'bs']])

    def check_rules(self):
        FCGUnit.check_rules(self)
        self.description.add(self.stabba.get_selected())

    def is_big_uns(self):
        return self.big.get('bu')


class Goblins(FCGUnit):
    name = 'Goblins'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Goblin', model_price=3, min_models=20,
            musician_price=10, standard_price=10, champion_name='Goblin Boss', champion_price=10,
            base_gear=['Hand weapon', 'Light armour'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Spear', 0.5, 'sp'],
                    ['Short Bow', 0.5, 'hw'],
                ], limit=1),
                OptionsList('arm', '', [['Shield', 0.5, 'sh']])
            ]
        )
        self.skul = self.opt_count('Nasty Skulker', 0, 3, 10)

    def check_rules(self):
        FCGUnit.check_rules(self)
        self.description.add(
            ModelDescriptor('Nasty Skulker', points=10,
                            gear=['Hand weapon', 'Hand weapon', 'Light armour']).build(self.skul.get()))


class NightGoblins(FCGUnit):
    name = 'Night Goblins'

    def __init__(self):
        self.wep = OneOf('wep', 'Weapon', [
            ['Shield', 0, 'sh'],
            ['Short Bow', 0, 'hw'],
        ])
        self.sp = OptionsList('sp', '', [
            ['Spear', 0, 'sp'],
        ])
        FCGUnit.__init__(
            self, model_name='Night Goblin', model_price=3, min_models=20,
            musician_price=10, standard_price=10, champion_name='Night Goblin Boss', champion_price=10,
            base_gear=['Hand weapon'],
            options=[self.wep, self.sp]
        )
        self.fan = self.opt_count('Fanatic', 0, 3, 25, id='fan')
        self.net = self.opt_options_list('', [['Netters', 45, 'net']], id='net')

    def check_rules(self):
        self.sp.set_active_options(['sp'], self.wep.get_cur() == 'sh')
        self.wep.set_active_options(['hw'], not self.sp.get('sp'))
        FCGUnit.check_rules(self)
        self.description.add(self.net.get_selected())
        self.description.add(
            ModelDescriptor('Night Goblin Fanatic', points=25, gear=['Iron ball']).build(self.fan.get()))


class WolfRaiders(FCGUnit):
    name = 'Goblin Wolf Raiders'

    def __init__(self):
        self.up = OptionsList('up', 'Gitilla\'s upgrade', [['Da Howlerz', 0, 'how']])
        FCGUnit.__init__(
            self, model_name='Goblin Wolf Raider', model_price=10, min_models=5,
            musician_price=10, standard_price=10, champion_name='Goblin Wolf Raider Boss', champion_price=10,
            base_gear=['Hand weapon', 'Light armour'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Spear', 1, 'sp'],
                    ['Short Bow', 1, 'hw'],
                    ['Shield', 1, 'sh']
                ])
            ], unit_options=[self.up]
        )

    def check_rules(self):
        self.up.set_active(self.get_roster().has_gitilla())
        FCGUnit.check_rules(self)

    def is_howlerz(self):
        return self.up.get('how')


class SpiderRaiders(FCGUnit):
    name = 'Forest Goblin Spider Raiders'

    def __init__(self):
        self.up = OptionsList('up', 'Snagla\'s upgrade', [['Deff Crippers', 0, 'dc']])
        FCGUnit.__init__(
            self, model_name='Spider Raider', model_price=13, min_models=5,
            musician_price=10, standard_price=10, champion_name='Spider Raider Boss', champion_price=10,
            base_gear=['Spear', 'Shield'],
            options=[OptionsList('wep', 'Weapon', [['Short Bow', 1, 'hw']])],
            unit_options=[self.up]
        )

    def check_rules(self):
        self.up.set_active(self.get_roster().has_snagla())
        FCGUnit.check_rules(self)

    def is_creepers(self):
        return self.up.get('dc')
