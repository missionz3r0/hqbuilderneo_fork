__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit


class RockLobber(Unit):
    name = 'Goblin Rock Lobber'
    base_points = 85

    def __init__(self):
        Unit.__init__(self)
        self.add = self.opt_options_list('Options', [
            ['Orc Bully', 10, 'orc'],
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Rock Lobber', count=1).build())
        self.description.add(ModelDescriptor(name='Goblin Crew', count=3, gear=['Hand weapon']).build())
        if self.add.get('orc'):
            self.description.add(ModelDescriptor(name='Orc Bully', count=1,
                                                 gear=['Hand weapon', 'Light armour']).build())


class DoomDiver(Unit):
    name = 'Doom Diver Catapult'
    base_points = 80
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Catapult', count=1).build())
        self.description.add(ModelDescriptor(name='Goblin Crew', count=3, gear=['Hand weapon']).build())


class Pump(Unit):
    name = 'Snotling Pump Wagon'
    base_points = 45
    gear = ['Mass of Snotligs', 'Hand weapon', 'Explodin\' Spores']

    def __init__(self):
        Unit.__init__(self)
        self.add = self.opt_options_list('Options', [
            ['Spiky Roller', 15, 'sr'],
            ['Out-rigga', 10, 'or'],
            ['Flappas', 5, 'fl'],
            ['Giant Explodin\' Spores', 15, 'ges'],
        ])


class Mangler(Unit):
    name = 'Mangler Squigs'
    base_points = 65
    static = True


class Arachnarok(Unit):
    name = 'Arachnarok Spider'
    base_points = 290

    def __init__(self, boss=False):
        Unit.__init__(self)
        opt = [['Flinger', 30, 'fl']]
        if boss:
            opt.append(['Catchweb Spidershrine', 40, 'cs'])

        self.opt = self.opt_options_list('Options', opt)

    def check_rules(self):
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Forest Goblin Crew', count=8, gear=['Spear', 'Short Bow']).build())


class StoneTrolls(FCGUnit):
    name = 'Stone Trolls'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Stone Troll', model_price=45, min_models=1,
                         base_gear=['Bone, club or bit of tree'])


class RiverTrolls(FCGUnit):
    name = 'River Trolls'

    def __init__(self):
        FCGUnit.__init__(self, model_name='River Troll', model_price=45, min_models=1,
                         base_gear=['Bone, club or bit of tree'])


class Giant(Unit):
    name = 'Giant'
    base_points = 200
    gear = ['A tree-trunk club or or other impressive large blunt implement']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Savage Shaman\'s upgrade', [
            ['Warpaint', 20, 'wp'],
        ])

    def check_rules(self):
        self.opt.set_active(self.get_roster().has_savage_shaman())
        Unit.check_rules(self)
