__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.orcs_and_goblins.heroes import *
from builder.games.whfb.orcs_and_goblins.lords import *
from builder.games.whfb.orcs_and_goblins.core import *
from builder.games.whfb.orcs_and_goblins.special import *
from builder.games.whfb.orcs_and_goblins.rare import *


class Orcs(LegacyFantasy):
    army_name = 'Orcs & Goblins'
    army_id = 'c4e6f17058464a4ebfb1582f605056f5'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Gorbad, Azhag, Grimgor, Wurrzag, Grom, Skarsnik, OrcWarboss, SavageOrcWarboss, BlackOrcWarboss,
                   GoblinWarboss, NightGoblinWarboss, OrcGreatShaman, SavageOrcGreatShaman, GoblinGreatShaman,
                   NightGoblinGreatShaman],
            heroes=[Gitilla, Snagla, OrcBigBoss, SavageOrcBigBoss, BlackOrcBigBoss, GoblinBigBoss, NightGoblinBigBoss,
                    OrcShaman, SavageOrcShaman, GoblinShaman, NightGoblinShaman],
            core=[Boyz, ArrerBoyz, SavageBoyz, Goblins, WolfRaiders, NightGoblins, SpiderRaiders],
            special=[BlackOrcs, BoarBoyz, SavageBoarBoyz, BoarChariot, WolfChariot, SpearChukka, SquigHopper,
                     SquigHerds, Snotling, Trolls],
            rare=[RockLobber, DoomDiver, Pump, Arachnarok, Mangler, StoneTrolls, RiverTrolls, Giant]
        )

    def has_savage_shaman(self):
        return (self.lords.count_unit(SavageOrcGreatShaman) + self.heroes.count_unit(SavageOrcShaman)) > 0

    def has_grimgor(self):
        return self.lords.count_unit(Grimgor) > 0

    def has_gitilla(self):
        return self.heroes.count_unit(Gitilla) > 0

    def has_snagla(self):
        return self.heroes.count_unit(Snagla) > 0

    def check_rules(self):
        LegacyFantasy.check_rules(self)

        immortulz = sum((1 for squad in self.special.get_units([BlackOrcs]) if squad.is_immortulz()))
        if immortulz != 1 and self.has_grimgor():
            self.error("You must take one, and only one Da Immortulz Black Orcs in Grimgors's army "
                       "(taken: {})".format(immortulz))

        howlerz = sum((1 for squad in self.core.get_units([WolfRaiders]) if squad.is_howlerz()))
        if howlerz != 1 and self.has_gitilla():
            self.error("You must take one, and only one Da Howlerz Wolf Raiders in Gitilla's army "
                       "(taken: {})".format(howlerz))

        creepers = sum((1 for squad in self.core.get_units([SpiderRaiders]) if squad.is_creepers()))
        if creepers != 1 and self.has_snagla():
            self.error("You must take one, and only one Deff Creepers Spider Raiders in Snagla's army "
                       "(taken: {})".format(creepers))

        big_units = self.core.get_units([Boyz, SavageBoyz]) + self.special.get_units([BoarBoyz, SavageBoarBoyz])
        big = sum((1 for squad in big_units if squad.is_big_uns()))
        if big > 1:
            self.error("You can't take more then one Big 'Uns of any type "
                       "(taken: {})".format(big))

    def check_special_limit(self, name, group, limit, mul):
        if name == SpearChukka.name:
            limit = 6
        return LegacyFantasy.check_special_limit(self, name, group, limit, mul)

    def check_rare_limit(self, name, group, limit, mul):
        if name == Pump.name:
            limit = 4
        return LegacyFantasy.check_rare_limit(self, name, group, limit, mul)
