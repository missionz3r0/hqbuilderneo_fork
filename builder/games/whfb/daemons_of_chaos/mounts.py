__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.core.model_descriptor import ModelDescriptor


class BloodThrone(Unit):
    name = 'Blood Throne of Khorne'
    base_points = 160

    def __init__(self, points=160):
        Unit.__init__(self)
        self.base_points = points

    def check_rules(self):
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Bloodletter', count=2, gear=['Hellblade']).build())


class Juggernaut(StaticUnit):
    name = 'Juggernaut of Khorne'
    base_points = 50


class SteedOfSlaanesh(Unit):
    name = 'Steed of Slaanesh'
    base_points = 25
    static = True


class DiskOfTzeentch(Unit):
    name = 'Disk of Tzeentch'
    base_points = 20
    static = True


class PalanquinOfNurgle(Unit):
    name = 'Palanquin of Nurgle'
    base_points = 50
    static = True
