__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.legacy_armory import magic_standards, filter_items

unit_standards = filter_items(magic_standards, 50)


class Bloodcrushers(FCGUnit):
    name = 'Bloodcrushers of Khorne'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Bloodletter', model_price=65, min_models=3,
            musician_price=10, standard_price=10, champion_name='Bloodhunter', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Hellblade'],
        )


class FlashHounds(FCGUnit):
    name = 'Flash Hounds of Khorne'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Flash Hound', model_price=30, min_models=5,
            base_gear=['Collar of Khorne'],
            options=[
                OptionsList('opt', 'Options', [
                    ['Ambushers', 3, 'amb'],
                ])
            ]
        )


class Flamers(FCGUnit):
    name = 'Flamers of Tzeentch'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Flamer', model_price=40, min_models=3, max_model=6,
            champion_name='Pyrocaster', champion_price=10,
        )


class Screamers(FCGUnit):
    name = 'Screamers of Tzeentch'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Screamer', model_price=40, min_models=3
        )


class Nurglings(FCGUnit):
    name = 'Nurglings'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Nurgling', model_price=40, min_models=2, max_model=12
        )


class BeastsOfNurgle(FCGUnit):
    name = 'Beasts of Nurgle'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Beast of Nurgle', model_price=60, min_models=1
        )


class FiendsOfSlaanesh(FCGUnit):
    name = 'Fiends of Slaanesh'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Fiends of Slaanes', model_price=65, min_models=3
        )


class Seekers(FCGUnit):
    name = 'Seekers of Slaanesh'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Seeker', model_price=20, min_models=5,
            musician_price=10, standard_price=10, champion_name='Heartseeker', champion_price=10,
            magic_banners=filter_items(magic_standards, 25),
        )


class Furies(FCGUnit):
    name = 'Chaos Furies'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Chaos Fury', model_price=12, min_models=5,
            options=[
                OptionsList('opt', 'Options', [
                    ['Mark of Khorne', 2, 'kh'],
                    ['Mark of Tzeentch', 2, 'tz'],
                    ['Mark of Nurgle', 2, 'ng'],
                    ['Mark of Slaanesh', 2, 'sl'],
                ], limit=1)
            ]
        )


class SeekerChariot(Unit):
    name = 'Seeker Chariot of Slaanesh'
    base_points = 130
    gear = ['Scythes']
    static = True

    def __init__(self, boss=False, points=130):
        Unit.__init__(self)
        self.boss = boss
        self.base_points = points

    def check_rules(self):
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Daemonette Crew', count=1 if self.boss else 2).build())
        self.description.add(ModelDescriptor(name='Steeds of Slaanesh', count=2).build())
