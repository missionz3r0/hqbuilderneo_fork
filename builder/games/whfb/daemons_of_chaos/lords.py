__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.legacy_armory import build_magic_levels
from builder.core.options import norm_points


class Scarbrand(StaticUnit):
    name = 'Skarbrand'
    base_points = 610
    gear = ['Heavy armour', 'Slaughter and Carnage']


class Fateweaver(StaticUnit):
    name = "Kairos Fateweaver"
    base_points = 565
    gear = ['Level 4 Wizard', 'Staff of Tomorrow']


class Kugath(StaticUnit):
    name = 'Ku\'gath Plaguefather'
    base_points = 625
    gear = ['Level 1 Wizard', 'Lore of Nurgle', 'Necrotic Missiles']


class Gifted(Unit):
    def __init__(self):
        Unit.__init__(self)
        self.les = self.opt_count("Lesser Gifts", 0, 2, 25)
        self.grt = self.opt_count("Greater Gifts", 0, 2, 50)
        self.exlt = self.opt_count("Exalted Gifts", 0, 1, 75)

    def check_rules(self):
        norm_points(100, [self.les, self.grt, self.exlt])
        if self.les.get_max() > 2:
            self.les.update_range(0, 2)
        Unit.check_rules(self)


class Bloodthirster(Gifted):
    name = 'Bloodthirster'
    base_points = 400
    gear = ['Heavy Armour', 'Lash', 'Axe']


class LordOfChange(Gifted):
    name = 'Lord of Change'
    base_points = 400
    gear = ['Hand weapon']

    def __init__(self):
        Gifted.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(2, 3, 35))
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Tzeentch', 0, 'tzeentch'],
        ], limit=1)


class Unclean(Gifted):
    name = 'Great Unclean One'
    base_points = 375
    gear = ['Hand weapon']

    def __init__(self):
        Gifted.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 4, 35))
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Death', 0, 'death'],
            ['The Lore of Nurgle', 0, 'nurgle'],
        ], limit=1)


class Keeper(Gifted):
    name = 'Keeper of Secrets'
    base_points = 375
    gear = ['Hand weapon']

    def __init__(self):
        Gifted.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 4, 35))
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Slaanesh', 0, 'slaanesh'],
        ], limit=1)


class DaemonPrince(Unit):
    name = "Daemon Prince"
    base_points = 250

    def __init__(self):
        Unit.__init__(self)
        self.marks = self.opt_one_of('', [
            ['Daemon of Khorne', 5, 'kh'],
            ['Daemon of Tzeench', 10, 'tz'],
            ['Daemon of Nurgle', 5, 'ng'],
            ['Daemon of Slaanesh', 0, 'sl']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Daemonic flight', 40, 'fl'],
            ['Chaos armour', 20, 'ca']
        ])
        self.mage = self.opt_options_list('Magic level', build_magic_levels(1, 4, 35, price_offset=35), limit=1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
            ['The Lore of Tzeentch', 0, 'tzeentch'],
            ['The Lore of Nurgle', 0, 'nurgle'],
            ['The Lore of Slaanesh', 0, 'slaanesh'],
        ], limit=1)
        self.les = self.opt_count("Lesser Gifts", 0, 4, 25)
        self.grt = self.opt_count("Greater Gifts", 0, 2, 50)
        self.exlt = self.opt_count("Exalted Gifts", 0, 1, 75)

    def check_rules(self):
        self.mage.set_active(not self.marks.get_cur() == 'kh')
        self.lores.set_active_options(['metal', 'tzeentch'], self.marks.get_cur() == 'tz')
        self.lores.set_active_options(['shadow', 'slaanesh'], self.marks.get_cur() == 'sl')
        self.lores.set_active_options(['death', 'nurgle'], self.marks.get_cur() == 'ng')

        norm_points(75, [self.les, self.grt, self.exlt])
        Unit.check_rules(self)
