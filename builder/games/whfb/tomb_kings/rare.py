__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor


class Colossus(Unit):
    name = 'Necrolith Colossus'
    base_points = 170
    gear = ['Hand weapon']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 5],
            ['Great weapon', 10],
            ['Bow of the desert', 20],
        ])


class Hierotitan(Unit):
    name = 'Hierotitan'
    base_points = 175
    gear = ['Icon of Ptra', 'Scales of Usirian']
    static = True


class Necrosphinx(Unit):
    name = 'Hecrosphinx'
    base_points = 225

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Envenomed Sting', 10, 'es'],
        ])


class Catapult(Unit):
    name = 'Screaming Skull Catapult'
    base_points = 90

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Skulls of the Foe', 30, 'sk'],
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description()
        self.description.add(ModelDescriptor(name='Screaming Skull Catapult', count=1).build())
        self.description.add(ModelDescriptor(name='Skeleton Crew', count=3,
                                             gear=['Hand weapon', 'Light armour']).build())


class Casket(Unit):
    name = 'Casket of Souls'
    base_points = 135
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Casket of Souls', count=1).build())
        self.description.add(ModelDescriptor(name='Keeper of the Casket', count=1,
                                             gear=['Hand weapon', 'Light armour']).build())
        self.description.add(ModelDescriptor(name='Casket Guard', count=2,
                                             gear=['Great weapon', 'Light armour']).build())
