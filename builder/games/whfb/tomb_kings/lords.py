__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.tomb_kings.mounts import *
from builder.games.whfb.tomb_kings.armory import *
from builder.games.whfb.tomb_kings.special import Warsphinx
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Settra(StaticUnit):
    name = 'Settra the Imperishable'
    base_points = 475
    gear = ['Level 1 Wizard', 'The Lore of Nehekhara', 'Blessed Blade of Ptra', 'Light Armour',
            'The Crown of Nehekhara', 'The Scarab Brooch of Usirian']

    def __init__(self):
        StaticUnit.__init__(self)
        char = ModelDescriptor(name='Chariot of the Gods', count=1)
        char.add_sub_unit(ModelDescriptor(name='Skeletal Steed').build(count=4))
        self.description.add(char.build())


class Khalida(StaticUnit):
    name = 'High Queen Khalida'
    base_points = 365
    gear = ['Venom Staff', 'Light armour']


class Arkhan(Unit):
    name = 'Arkhan the Black'
    base_points = 360
    gear = ['Tomb Blade of Arkhan', 'Light armour', 'The Liber Mortis', 'The Staff of Nagash']

    class Chariot(Unit):
        name = 'Skeleton Chariot'
        base_points = 55

        def __init__(self):
            Unit.__init__(self)
            self.steed = self.opt_count('Skeletal Steed', 2, 4, 15)
            self.opt = self.opt_options_list('Options', [['Fly', 30]])

        def check_rules(self):
            self.set_points(self.build_points(base_points=25))
            self.build_description(options=[self.opt])
            self.description.add(ModelDescriptor(name='Skeletal Steed', count=self.steed.get()).build())

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [self.Chariot()])


class Khater(StaticUnit):
    name = 'Grand Hierophant Khater'
    base_points = 330
    gear = ['Level 4 Wizard', 'The Lore of Nehekhara', 'Hand weapon', 'The Liche Staff', 'Scroll of Cursing Words']


class TombKing(Unit):
    name = 'Tomb King'
    gear = ['Hand Weapon']
    base_points = 170

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Flail', 3, 'hw'],
            ['Spear', 3, 'sp'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [KingChariot(), Warsphinx(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', king_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', tk_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', tk_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', tk_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(tk_shields_ids))
        self.gear = ['Hand weapon'] + (['Light armour'] if not self.magic_arm.is_selected(tk_armour_suits_ids) else [])
        self.magic_wep.set_visible_options(['tk_doe'], self.steed.get_count() == 0)
        self.magic_enc.set_visible_options(['tk_cloak'], self.steed.get_count() == 0)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class HighPriest(Unit):
    name = 'Liche High Priest'
    gear = ['Hand Weapon']
    base_points = 175

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [SkeletalSteed()])
        self.magic_wep = self.opt_options_list('Magic weapon', tk_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', tk_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', tk_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', tk_arcane, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Nehekhara', 0, 'neh'],
            ['The Lore of Light', 0, 'light'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.magic_enc.set_visible_options(['tk_cloak'], self.steed.get_count() == 0)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])
