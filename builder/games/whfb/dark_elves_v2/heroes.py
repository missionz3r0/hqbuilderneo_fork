from builder.games.whfb.roster import BSB, Character
from .lords import DarkPegasus, Manticore, ColdOneChariot, Cauldron, Mount, SupremeSorceress
from .armory import *
from builder.core2 import *

__author__ = 'Denis Romanov'


class Shadowblade(Unit):
    type_name = 'Shadowblade'
    type_id = 'shadowblade_v1'

    def __init__(self, parent):
        super(Shadowblade, self).__init__(parent, points=245, unique=True, gear=[
            Gear('Hand weapon', count=2),
            Gear('Throwing weapon'),
            Gear('Heart of Woe'),
            Gear('Potion of Diabolic Strength'),
            Gear('Dark Venom'),
            Gear('Black lotus'),
            Gear('Manbane'),
        ])


class Lokhir(Unit):
    type_name = 'Lokhir Fellheart'
    type_id = 'lokhir_v1'

    def __init__(self, parent):
        super(Lokhir, self).__init__(parent, points=235, unique=True, gear=[
            Gear('The Red Blades'),
            Gear('Sea Dragon Cloak'),
            Gear('Helm of Kraken'),
            Gear('Heavy armour'),
        ])


class Kouran(Unit):
    type_name = 'Kouran Darkhand'
    type_id = 'kouran_v1'

    def __init__(self, parent):
        super(Kouran, self).__init__(parent, points=180, unique=True, gear=[
            Gear('Crimson Death'),
            Gear('The Armour of Grief'),
        ])


class Tullaris(Unit):
    type_name = 'Tullaris Dreadbringer'
    type_id = 'tullaris_v1'

    def __init__(self, parent):
        super(Tullaris, self).__init__(parent, points=155, unique=True, gear=[
            Gear('The First Draich'),
            Gear('Heavy armour'),
        ])


class ColdOne(Mount):
    def __init__(self, parent):
        super(ColdOne, self).__init__(parent)
        self.simple_mount('Cold One', 12)


class DarkSteed(Mount):
    def __init__(self, parent):
        super(DarkSteed, self).__init__(parent)
        self.simple_mount('Dark Steed', 10)


class Sorceress(Character):
    type_name = 'Sorceress'
    type_id = 'sorceress_v1'

    class Mount(DarkPegasus, ColdOne, DarkSteed):
        pass

    class MagicLevel(OneOf):
        def __init__(self, parent):
            super(Sorceress.MagicLevel, self).__init__(parent, 'Wizard')
            self.variant('Level 1 Wizard')
            self.variant('Level 2 Wizard', 35)

    def __init__(self, parent):
        super(Sorceress, self).__init__(parent, points=80, gear=[Gear('Hand weapon')], magic_limit=50)

        self.Mount(self)
        self.MagicLevel(self)
        SupremeSorceress.Lore(self)

        self.magic = [
            Weapon(self, limit=50),
            Enchanted(self, limit=50),
            Arcane(self, limit=50),
            Talismans(self, limit=50),
        ]


class Master(Character):
    type_name = 'Master'
    type_id = 'master_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Master.Weapon, self).__init__(parent, name='Weapon')
            self.hw = self.variant('Additional hand weapon', 2, gear=Gear('Hand weapon'))
            self.halberd = self.variant('Halberd', 2)
            self.lance = self.variant('Lance', 6)
            self.gw = self.variant('Great weapon', 4)
            self.melee = [self.hw, self.halberd, self.lance, self.gw]
            self.ranged = [
                self.variant('Repeater crossbow', 5),
                self.variant('Repeater handbow', 5),
                self.variant('Brace of repeater handbows', 10, gear=Gear('Repeater handbow', count=2))
            ]

        def check_rules(self):
            has_mount = self.parent.mount.has_mount()
            if has_mount and self.hw.value:
                self.hw.value = False
            if not has_mount and self.lance.value:
                self.hw.value = False
            self.process_limit(self.melee, 1)
            self.process_limit(self.ranged, 1)
            self.hw.active = self.hw.used = not has_mount
            self.lance.active = self.lance.used = has_mount

    class ArmourSuit(OneOf):
        def __init__(self, parent):
            super(Master.ArmourSuit, self).__init__(parent, name='Armour')
            self.armour = self.variant('Light armour', 0)
            self.armour = self.variant('Heavy armour', 4)

        def check_rules(self):
            super(Master.ArmourSuit, self).check_rules()
            self.used = self.active = not self.parent.magic_armour.has_suit()

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Master.Armour, self).__init__(parent, name='')
            self.shield = self.variant('Shield', 2)
            self.armour = self.variant('Sea Dragon cloak', 6)

        def check_rules(self):
            super(Master.Armour, self).check_rules()
            self.shield.used = self.shield.active = not self.parent.magic_armour.has_shield()

    class Mount(Manticore, ColdOneChariot, DarkPegasus, ColdOne, DarkSteed):
        pass

    def __init__(self, parent):
        super(Master, self).__init__(parent, points=70, gear=[Gear('Hand weapon')], magic_limit=50)
        self.Weapon(self)
        self.ArmourSuit(self)
        self.Armour(self)
        self.mount = self.Mount(self)

        self.bsb = BSB(self, Standards)

        self.magic_weapon = Weapon(self, limit=50)
        self.magic_armour = Armour(self, limit=50)
        self.magic = [
            self.magic_weapon,
            self.magic_armour,
            Enchanted(self, limit=50),
            Talismans(self, limit=50),
        ]


class Assassin(Character):
    type_name = 'Khainite Assassin'
    type_id = 'assassin_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Assassin.Weapon, self).__init__(parent, name='Weapon', limit=1)
            self.hw = self.variant('Additional hand weapon', 2, gear=Gear('Hand weapon'))
            self.variant('Repeater handbow', 5),

    class Options(OptionsList):
        def __init__(self, parent):
            super(Assassin.Options, self).__init__(parent, name='Options', limit=1)
            self.shield = self.variant('Black Lotus', 15)
            self.shield = self.variant('Dark Venom', 20)
            self.shield = self.variant('Manbane', 20)

    def __init__(self, parent):
        super(Assassin, self).__init__(parent, points=90, gear=[Gear('Hand weapon'), Gear('Throwing weapon')],
                                       magic_limit=50)
        self.Weapon(self)
        self.Options(self)

        self.magic = [
            Weapon(self, limit=50),
            Armour(self, limit=50, suits=False, shields=False),
            Enchanted(self, limit=50),
            Talismans(self, limit=50),
        ]


class Hag(Character):
    type_name = 'Death Hag'
    type_id = 'hag_v1'

    class Mount(Cauldron):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Hag.Options, self).__init__(parent, name='Options', limit=1)
            self.variant('Cry of War', 15)
            self.variant('Witchbrew', 30)
            self.variant('Rune of Khaine', 40)

    def __init__(self, parent):
        super(Hag, self).__init__(parent, points=85, gear=[Gear('Hand weapon', count=2)], magic_limit=50)
        self.Options(self)
        self.mount = self.Mount(self)
        self.bsb = BSB(self, Standards)
        self.magic = [Weapon(self, limit=50)]
