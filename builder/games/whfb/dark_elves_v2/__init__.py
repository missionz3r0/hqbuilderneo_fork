from builder.games.whfb.roster import (
    LordsSection as BaseLordsSection,
    HeroesSection as BaseHeroesSection,
    CoreSection as BaseCoreSection,
    SpecialSection as BaseSpecialSection,
    RareSection as BaseRareSection,
    FantasyRoster
)

from .lords import *
from .heroes import *
from .core import *
from .special import *
from .rare import *

__author__ = 'Denis Romanov'


class LordsSection(BaseLordsSection):
    def __init__(self, parent):
        super(LordsSection, self).__init__(parent=parent)
        UnitType(self, Malekith)
        UnitType(self, Morathi)
        UnitType(self, Crone)
        UnitType(self, Malus)
        UnitType(self, Dreadlord)
        UnitType(self, SupremeSorceress)
        UnitType(self, HighBeastmaster)
        UnitType(self, BlackArkFleetmaster)


class HeroesSection(BaseHeroesSection):
    def __init__(self, parent):
        super(HeroesSection, self).__init__(parent=parent)
        UnitType(self, Shadowblade)
        UnitType(self, Lokhir)
        UnitType(self, Kouran)
        UnitType(self, Tullaris)
        UnitType(self, Sorceress)
        UnitType(self, Master)
        UnitType(self, Hag)
        UnitType(self, Assassin)


class CoreSection(BaseCoreSection):
    def __init__(self, parent):
        super(CoreSection, self).__init__(parent=parent)
        UnitType(self, Dreadspears)
        UnitType(self, Blackswords)
        UnitType(self, Darkshards)
        UnitType(self, Corsairs)
        UnitType(self, DarkRaiders)
        UnitType(self, WitchElves)


class SpecialSection(BaseSpecialSection):
    def __init__(self, parent):
        super(SpecialSection, self).__init__(parent=parent)
        UnitType(self, ColdOneKnights)
        UnitType(self, BlackGuard)
        UnitType(self, Shades)
        UnitType(self, ColdOneChariot)
        UnitType(self, Executioners)
        UnitType(self, ReaperBoltThrower)
        UnitType(self, Harpies)
        UnitType(self, ScourgeRunnerChariot)
        UnitType(self, WarHydra)


class RareSection(BaseRareSection):
    def __init__(self, parent):
        super(RareSection, self).__init__(parent=parent)
        UnitType(self, Warlocks)
        UnitType(self, Medusa)
        UnitType(self, Kharibdyss)
        UnitType(self, Shrine)
        UnitType(self, SistersOfSlaughter)


class DarkElvesV2(FantasyRoster):
    army_name = 'Dark Elves'
    army_id = 'dark_elves_v2'

    def __init__(self):
        super(DarkElvesV2, self).__init__(
            lords=LordsSection(parent=self),
            heroes=HeroesSection(parent=self),
            core=CoreSection(parent=self),
            special=SpecialSection(parent=self),
            rare=RareSection(parent=self)
        )

    def check_special_limit(self, name, group, limit, mul):
        if name == ReaperBoltThrower.type_name:
            limit = 6
        return super(DarkElvesV2, self).check_special_limit(name, group, limit, mul)
