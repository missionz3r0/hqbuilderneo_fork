__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor


class Giant(Unit):
    name = 'Giant'
    base_points = 200
    gear = ['Hand weapon']
    static = True


class Scraplauncher(Unit):
    name = 'Gnoblar Scraplauncher'
    base_points = 130
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Scraplauncher', count=1).build())
        self.description.add(ModelDescriptor(name='Gnoblar Scrapper', count=7, gear=['Hand weapon']).build())
        self.description.add(ModelDescriptor(name='Rhinox', count=1).build())


class Stonehorn(Unit):
    name = 'Stonehorn'
    base_points = 250

    def __init__(self, boss=False):
        Unit.__init__(self)
        self.static = boss
        self.boss = boss
        if not self.boss:
            self.opt = self.opt_one_of('Weapon', [
                ['Chaintrap', 0],
                ['Harpoon launcher', 0]
            ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        if not self.boss:
            self.description.add(ModelDescriptor(name='Ogre Beast Raider', count=1).add_gear_opt(self.opt).build())


class Thundertusk(Unit):
    name = 'Thundertusk'
    base_points = 250
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Ogre Beast Raider', count=1, gear=['Chaintrap']).build())
        self.description.add(ModelDescriptor(name='Ogre Beast Raider', count=1, gear=['Harpoon launcher']).build())


class Ironblaster(Unit):
    name = 'Ironblaster'
    base_points = 170
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Ironblaster', count=1).build())
        self.description.add(ModelDescriptor(name='Leadbelcher', count=1, gear=['Hand weapon']).build())
        self.description.add(ModelDescriptor(name='Gnoblar Scrapper', count=1, gear=['Hand weapon']).build())
        self.description.add(ModelDescriptor(name='Rhinox', count=1).build())
