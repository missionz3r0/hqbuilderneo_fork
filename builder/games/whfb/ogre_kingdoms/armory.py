__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *


ok_weapon = [
    ['Thunder mace', 85, 'ok_tm'],
    ['Siegebreaker', 85, 'ok_sb'],
] + magic_weapons

ok_armour = [
    ['Gut Maw', 45, 'ok_maw']
] + magic_armours

ok_armour_suits_ids = ['ok_maw'] + magic_armour_suits_ids
ok_shields_ids = magic_shields_ids

ok_talisman = [
    ['Gnoblar Thiefstone', 45, 'ok_gt'],
    ['Greedy First', 40, 'ok_gf']
] + talismans

ok_enchanted = [
    ['Rock Eye', 5, 'ok_rock'],
] + enchanted_items

ok_arcane = [
    ['Grut\'s Sickle', 50, 'ok_gs'],
    ['Hellheart', 50, 'ok_hh'],
] + arcane_items

ok_standard = [
    ['Rune Maw', 60, 'ok_maw'],
    ['Dragonhide Banner of Rage', 50, 'ok_hide'],
] + magic_standards

big_names = [
    ['Wallcrusher', 30, 'wall'],
    ['Mountaineater', 25, 'me'],
    ['Giantbreaker', 25, 'gb'],
    ['Deathcheater', 20, 'dc'],
    ['Longstrider', 20, 'ls'],
    ['Brawlerguts', 15, 'bg'],
]

tyrant_names = [
    ['Mawseeker', 40, 'seek'],  # tyrant only
    ['Kineater', 25, 'ke'],  # tyrant only
] + big_names

hunter_names = [
    ['Beastkiller', 20, 'bk'], #hunter only
] + big_names