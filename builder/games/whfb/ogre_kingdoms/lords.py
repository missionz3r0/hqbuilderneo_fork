__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.ogre_kingdoms.armory import *
from builder.core.options import norm_point_limits


class Greasus(StaticUnit):
    name = 'Greasus Goldtooth'
    base_points = 545
    gear = ['Sceptre of the Titans', 'Light armour', 'Overtyrant\'s Crown']


class Skrag(StaticUnit):
    name = 'Skrag the Slaughterer'
    base_points = 425
    gear = ['Level 4 Wizard', 'Lore of the Great Maw', 'Hand weapon', 'Hand weapon', 'Cauldron of the Great Maw']


class Tyrant(Unit):
    name = 'Tyrant'
    gear = ['Hand weapon']
    base_points = 210

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 3, 'hw'],
            ['Ironfist', 5, 'if'],
            ['Great weapon', 12, 'gw'],
            ['Ogre pistol', 8, 'op'],
            ['Brace of Ogre pistols', 14, 'brace'],
        ], limit=1)
        self.eq = self.opt_one_of('Armour', [
            ['Light armour', 0, 'la'],
            ['Heavy armour', 5, 'la'],
        ])
        self.big_names = self.opt_options_list('Big names', tyrant_names, 1)
        self.magic_wep = self.opt_options_list('Magic weapon', ok_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', ok_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', ok_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', ok_enchanted, 1)
        self.magic = [self.big_names, self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_visible(not self.magic_arm.is_selected(ok_armour_suits_ids))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), [])


class Slaughtermaster(Unit):
    name = 'Slaughtermaster'
    gear = ['Hand weapon']
    base_points = 250

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 3, 'hw'],
            ['Ironfist', 5, 'if'],
            ['Great weapon', 10, 'gw'],
        ], limit=1)
        self.magic_wep = self.opt_options_list('Magic weapon', ok_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', ok_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', ok_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', ok_arcane, 1)
        self.magic = [self.magic_wep, self.magic_tal, self.magic_enc, self.magic_arc]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of the Great Maw', 0, 'maw'],
            ['The Lore of Heavens', 0, 'heavens'],
            ['The Lore of Beast', 0, 'beast'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)

    def check_rules(self):
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), [])
