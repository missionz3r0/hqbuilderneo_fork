__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.ogre_kingdoms.armory import *
from builder.core.options import norm_point_limits
from builder.games.whfb.ogre_kingdoms.rare import Stonehorn


heroes_weapon = filter_items(ok_weapon, 50)
heroes_armour = filter_items(ok_armour, 50)
heroes_talisman = filter_items(ok_talisman, 50)
heroes_arcane = filter_items(ok_arcane, 50)
heroes_enchanted = filter_items(ok_enchanted, 50)


class Golgfag(StaticUnit):
    name = 'Golgfag Maneater'
    base_points = 260
    gear = ['Hand weapon', 'Hand weapon', 'Light armour', 'Ogre pistol']


class Bragg(StaticUnit):
    name = 'Bragg the Gutsman'
    base_points = 210
    gear = ['Great Gutgouger', 'Light armour']


class Bruiser(Unit):
    name = 'Bruiser'
    gear = ['Hand weapon']
    base_points = 105

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 3, 'hw'],
            ['Ironfist', 4, 'if'],
            ['Great weapon', 11, 'gw'],
            ['Ogre pistol', 6, 'op'],
            ['Brace of Ogre pistols', 10, 'brace'],
        ], limit=1)
        self.eq = self.opt_one_of('Armour', [
            ['Light armour', 0, 'la'],
            ['Heavy armour', 4, 'ha'],
        ])
        self.big_names = self.opt_options_list('Big names', big_names, 1)
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', ok_standard, 1)
        self.magic = [self.big_names, self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_visible(not self.magic_arm.is_selected(ok_armour_suits_ids))
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.is_used()), [])


class Hunter(Unit):
    name = 'Hunter'
    base_gear = ['Hand weapon', 'Great throwing spear']
    base_points = 130

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 3, 'hw'],
            ['Ironfist', 4, 'if'],
            ['Great weapon', 11, 'gw'],
            ['Harpoon launcher', 10, 'hp'],
            ['Blood vulture', 10, 'vul'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [Stonehorn(boss=True)])
        self.big_names = self.opt_options_list('Big names', hunter_names, 1)
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic = [self.big_names, self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.gear = self.base_gear + (['Light armour'] if not self.magic_arm.is_selected(ok_armour_suits_ids) else [])
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), [])


class Butcher(Unit):
    name = 'Butcher'
    gear = ['Hand weapon']
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 2, 'hw'],
            ['Ironfist', 3, 'if'],
            ['Great weapon', 9, 'gw'],
        ], limit=1)
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.magic = [self.magic_wep, self.magic_tal, self.magic_enc, self.magic_arc]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of the Great Maw', 0, 'maw'],
            ['The Lore of Heavens', 0, 'heavens'],
            ['The Lore of Beast', 0, 'beast'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)

    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), [])


class Firebelly(Unit):
    name = 'Firebelly'
    gear = ['The Lore of Fire', 'Hand weapon']
    base_points = 120

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 2, 'hw'],
            ['Great weapon', 9, 'gw'],
        ], limit=1)
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.magic = [self.magic_wep, self.magic_tal, self.magic_enc, self.magic_arc]

    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), [])
