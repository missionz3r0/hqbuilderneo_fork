__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.core.unit import OptionsList
from .armory import ok_standard, filter_items


unit_standards = filter_items(ok_standard, 50)


class Ogres(FCGUnit):
    name = 'Ogres'

    def __init__(self):
        self.gnoblar = OptionsList('opt', '', [
            ['Look-out Gnoblar', 5, 'log'],
        ])
        FCGUnit.__init__(
            self, model_name='Ogre', model_price=30, min_models=3,
            musician_price=10, musician_name='Bellower', standard_price=10, champion_name='Crusher', champion_price=10,
            base_gear=['Hand weapon', 'Light armour'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Hand weapon', 1, 'hw'],
                    ['Ironfist', 2, 'if'],
                ], limit=1),
            ],
            unit_options=[self.gnoblar]
        )

    def check_rules(self):
        self.gnoblar.set_visible(self.has_flag())
        FCGUnit.check_rules(self)


class Ironguts(FCGUnit):
    name = 'Ironguts'

    def __init__(self):
        self.gnoblar = OptionsList('opt', '', [
            ['Look-out Gnoblar', 5, 'log'],
        ])
        FCGUnit.__init__(
            self, model_name='Irongut', model_price=43, min_models=3,
            musician_price=10, musician_name='Bellower', standard_price=10, champion_name='Gutlord', champion_price=10,
            base_gear=['Great weapon', 'Heavy armour'],
            unit_options=[self.gnoblar], magic_banners=unit_standards
        )

    def check_rules(self):
        self.gnoblar.set_visible(self.has_flag())
        FCGUnit.check_rules(self)


class Gnoblars(FCGUnit):
    name = 'Gnoblars'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Gnoblar', model_price=2.5, min_models=10,
            musician_price=10, standard_price=10, champion_name='Groinbiter', champion_price=5,
            base_gear=['Hand weapon', 'Throwing armour'],
            unit_options=[
                OptionsList('wep', 'Options', [
                    ['Gnoblar Trappers', 25, 'tr'],
                ], limit=1)
            ]
        )
