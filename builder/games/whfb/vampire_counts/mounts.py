__author__ = 'Ivan Truskov'
from builder.core.model_descriptor import ModelDescriptor
from builder.core.unit import Unit, StaticUnit


class Nightmare(Unit):
    name = 'Barded Nightmare'
    static = True

    def __init__(self, disc=0):
        Unit.__init__(self)
        self.base_points = 24 - disc


class Hellsteed(StaticUnit):
    name = 'Hellsteed'
    base_points = 30


class SceletalSteed(Unit):
    name = 'Sceletal Steed'
    base_points = 12

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Barding', 8, 'brd']
        ])


class AbyssalTerror(Unit):
    name = 'Abyssal Terror'
    base_points = 120

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Poisonous Tail', 15, 'pt'],
            ['Sword-claws', 10, 'sc']
        ])


class ZombieDragon(StaticUnit):
    name = 'Zombie Dragon'
    base_points = 245


class CovenThrone(StaticUnit):
    name = 'Coven Throne'
    base_points = 230

    def check_rules(self):
        self.build_description()
        self.description.add(ModelDescriptor(name='Pallid handmaiden', count=2, gear=['Hand weapon']).build())
        self.description.add(ModelDescriptor(name='Spirit Horde', count=1).build())
