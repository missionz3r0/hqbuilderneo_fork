from builder.games.whfb.unit import FCGUnit

__author__ = 'Ivan Truskov'
from builder.core.unit import OptionsList
from builder.games.whfb.vampire_counts.armory import vampire_magic_standards, filter_items


class Zombies(FCGUnit):
    name = 'Zombies'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Zombie', model_price=3, min_models=20, musician_price=5, standard_price=5)


class SkeletonWarriors(FCGUnit):
    name = 'Skeleton Warriors'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Skeleton Warrior', model_price=5, min_models=10,
            musician_price=10, standard_price=10, champion_name='Skeleton Champion', champion_price=10,
            magic_banners=filter_items(vampire_magic_standards, 25),
            base_gear=['Light armour', 'Hand weapon', 'Shield'],
            options=[OptionsList('wep', 'Weapon', [['Spear', 0, 'sp']])]
        )


class Ghouls(FCGUnit):
    name = 'Crypt Ghouls'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Crypt Ghoul', model_price=10, min_models=10, champion_price=10,
                         champion_name='Crypt Ghast')


class Wolves(FCGUnit):
    name = 'Dire Wolves'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Dire Wolf', model_price=8, min_models=5, max_model=20,  champion_price=10,
                         champion_name='Doom Wolf')
