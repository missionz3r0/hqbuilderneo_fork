__author__ = 'Denis Romanov'


from builder.games.whfb.armory import *


class Weapon(BaseWeapon):
    def __init__(self, parent, name='Magic weapon', limit=None):
        super(Weapon, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('The Blade of Realities', 100)
        self.variant('The Piranha Blade', 50)
        self.add_base()


class Standards(BaseStandards):
    def __init__(self, parent, name='Magic standards', limit=None):
        super(Standards, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('Skavenpelt Banner', 65)
        self.variant('The Jaguar Standard', 50)
        self.add_base()


class Armour(BaseArmour):
    def __init__(self, parent, name='Magic armour', suits=True, shields=True, limit=None):
        super(Armour, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('Sacred Stegadon Helm of Itza', 40)
        self.add_base(suits=suits, shields=shields)


class Arcane(BaseArcane):
    def __init__(self, parent, name='Arcane items', limit=None):
        super(Arcane, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('Cube of Darkness', 30)
        self.variant('Plaque of Dominion', 25)
        self.add_base()


class Enchanted(BaseEnchanted):
    def __init__(self, parent, name='Enchanted items', limit=None):
        super(Enchanted, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('The Cloak of Feathers', 35)
        self.variant('The Horn of Kygor', 35)
        self.variant('The Egg of Quango', 30)
        self.add_base()


class Talismans(BaseTalismans):
    def __init__(self, parent, name='Talismans', limit=None):
        super(Talismans, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.add_base()
