__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *

bret_weapon = [
    ['The Silver Lance of Blessed', 65, 'bret_slb'],  # mounted only
    ['Sword of the Quest', 50, 'bret_quest'],  # questing vow
    ['Sword of Lady\'s Champion', 40, 'bret_lc'],  # grail vow
    ['Sword of Heroes', 35, 'bret_soh'],
    ['The Heartwood Lance', 35, 'bret_hwd'],  # mounted only
    ['Birth-sword of Carcassonne', 35, 'bret_carcas'],
    ['Morning Star of Fracasse', 25, 'bret_frac'],
    ['The Lance of Artois', 25, 'bret_loa'],  # mounted only
    ['The Wyrmlance', 20, 'bret_wl'],  # mounted only
] + magic_weapons

bret_armour = [
    ['Gilded Cuirass ', 55, 'bret_gil'],
    ['Armour of Midsummer Sun', 45, 'bret_mid'],
    ['The Grail Shield', 35, 'bret_gs'],  # grial vow
    ['Gromril Great Helm', 30, 'bret_grom'],
    ['Armour of Agilulf', 25, 'bret_agi'],
    ['Cuirass of Fortune', 20, 'bret_for'],
    ['Orcbane Shield', 15, 'bret_orc'],
] + magic_armours

bret_armour_suits_ids = magic_armour_suits_ids + ['bret_gil', 'bret_mid', 'bret_agi', 'bret_for']
bret_shields_ids = magic_shields_ids + ['bret_gs', 'bret_orc']

bret_talisman = [
    ['Sirienne\'s Locket', 55, 'bret_siri'],  # lord only
    ['Token of Damsel', 35, 'bret_tok'],  # lord and paladin only
    ['Insignia on the Quest', 30, 'bret_ins'],  # questing vow
    ['Braid of Bordeleaux', 25, 'bret_bb'],
    ['Dradon\'s Claw', 25, 'bret_dc'],
    ['Mantle of Damsel Elena', 20, 'bret_elen'],
] + talismans

bret_enchanted = [
    ['Falcon Horn of Fredemund', 45, 'bret_fhf'],
    ['Holy icon', 40, 'bret_hi'],
    ['The Ruby Goblet', 30, 'bret_ruby'],
    ['The Mane of Purebreed', 25, 'bret_mane'],  # on warhorse only
    ['Antlers of the Great Hunt', 25, 'bret_hunt'],  # lord and paladin only
    ['Tress of Isoulde', 20, 'bret_tress'],
    ['Gauntlet of the Duel', 10, 'bret_gd'],
] + enchanted_items

bret_arcane = [
    ['The Silver Mirror', 40, 'bret_mir'],
    ['Sacrament of the Lady', 40, 'bret_sac'],
    ['The Verdant Heart', 40, 'bret_vh'],
    ['Prayer Icon of Quenelles', 25, 'bret_piq'],
    ['Chalice of Malfleaur', 20, 'bret_chal'],
    ['Potion Sacre', 10, 'bret_pot'],
] + arcane_items

bret_standard = [
    ['Banner of the Lady', 100, 'bret_lady'],  # foot or warhorse
    ['Valorous Standard', 50, 'bret_val'],  # Knights only
    ['Banner of Defence', 30, 'bret_def'],
    ['Twilight Banner', 25, 'bret_tw'],
    ['Conqueror\'s Tapestry', 25, 'bret_tap'],
    ['Errantry Banner', 20, 'bret_err'],  # Knights errant only
    ['Banner of Chalons',10, 'bret_chal'],
] + magic_standards

virtue = [
    ['Virtue of the Penitent', 40, 'pen'],
    ['Virtue of Knighly', 40, 'pen'],
]
