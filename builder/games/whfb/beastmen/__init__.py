__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.beastmen.heroes import *
from builder.games.whfb.beastmen.lords import *
from builder.games.whfb.beastmen.core import *
from builder.games.whfb.beastmen.special import *
from builder.games.whfb.beastmen.rare import *


class Beastmen(LegacyFantasy):
    army_name = 'Beastmen'
    army_id = '1a1b21268a8a4375a9681b0b7ac0e00a'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Khazrak, Gorgoth, Malagor, Taurox, Beastlord, Doombull, GreatBrayShaman],
            heroes=[Morghul, Slugtongue, Moonclaw, Wargor, Gorebull, BrayShaman],
            core=[GorHerd, UngorHerd, UngorRaiders, TuskgorChariot, Warhounds, Centigors],
            special=[Minotaurs, Centigors, Harpies, Bestigor, RazorgorChariot, RazorgorHerd],
            rare=[Cygor, Ghorgon, Spawn, Giant, Jabberslythe]
        )

    def check_rules(self):
        ghorros = any(u.has_ghorros() for u in self.special.get_units([Centigors]) + self.core.get_units([Centigors]))
        if ghorros and self.special.count_unit(Centigors):
            self.error("You must take Centigors as Core if you have Ghorros Warhoof")
        elif not ghorros and self.core.count_unit(Centigors):
            self.error("You must take Centigors as Special if you have not Ghorros Warhoof")

