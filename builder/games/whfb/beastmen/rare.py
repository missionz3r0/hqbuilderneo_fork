__author__ = 'Denis Romanov'

from builder.core.unit import Unit


class Cygor(Unit):
    name = 'Cygor'
    base_points = 275
    static = True
    gear = ['Claws and horns']


class Ghorgon(Unit):
    name = 'Ghorgon'
    base_points = 275
    static = True
    gear = ['Cleaver-limbs']


class Spawn(Unit):
    name = 'Chaos Spawn'
    base_points = 55
    static = True
    gear = ['Claws and teeth']


class Giant(Unit):
    name = 'Giant'
    base_points = 225
    static = True
    gear = ['Hand weapon']


class Jabberslythe(Unit):
    name = 'Jabberslythe'
    base_points = 275
    static = True
    gear = ['Hand weapon']
