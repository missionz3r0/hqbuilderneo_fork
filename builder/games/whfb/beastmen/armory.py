__author__ = 'Denis Romanov'

from builder.games.whfb.legacy_armory import *

beast_weapon = [
    ['Primeval Club', 100, 'beast_club'],
    ['Axe of Man', 75, 'beast_axe'],
    ['Stonecrusher mace', 65, 'beast_mace'],
    ['Mangelder', 50, 'beast_mang'],
    ['Hunting Spear', 50, 'beast_spear'],
    ['Axes of Khorgor', 40, 'beast_axes'],
    ['The Steel-Claws', 35, 'beast_claws'],
    ['The Brass Cleaver', 30, 'beast_cleaver'],
    ['Everbleed', 25, 'beast_bleed'],
] + magic_weapons

beast_armour = [
    ['Blade-blunter Armour', 50, 'beast_bb'],
    ['Trollhide', 50, 'beast_hide'],
    ['Pelt of the Shadowgave', 45, 'beast_pelt'],
    ['Blackened Plate', 20, 'beast_plate'],
    ['Ramhorn Helm', 15, 'beast_ram'],
] + magic_armours

beast_armour_suits_ids = magic_armour_suits_ids + ['beast_bb', 'beast_hide', 'beast_pelt', 'beast_plate']
beast_shields_ids = magic_shields_ids

beast_enchanted = [
    ['Shard of the Herdstone', 50, 'beast_shard'],
    ['Horn of the Great Hunt', 50, 'beast_hunt'],  # Beast lord or wargor only
    ['Horn of the First Beast', 50, 'beast_first'],  # Beast lord or wargor only
    ['Stone of Spite', 25, 'beast_stone'],
    ['Skin of Man', 15, 'beast_skin'],  # Shaman only
    ['Cacophonous Dirge', 15, 'beast_dirge'],
] + enchanted_items

beast_talisman = [
    ['Eye of Night', 45, 'beast_eye'],
    ['Chalice of Dark Rain', 40, 'beast_rain'],
] + talismans

beast_arcane = [
    ['Skull of Rarkos', 60, 'beast_rar'],
    ['Staff of Darkoth', 50, 'beast_staff'],
    ['Hagtree Fetith', 20, 'beast_fetish'],
    ['Jagged Dagger', 10, 'beast_dagger'],
] + arcane_items

beast_standard = [
    ['The Beast Banner', 75, 'beast_banner'],
    ['Totem of Rust', 50, 'beast_rust'],
    ['Manbane Standard', 35, 'beast_manbane'],
    ['The Banner of Outrage', 20, 'beast_outrage'],
] + magic_standards

gifts = [
    ['Crown of Horns', 75, 'horns'],
    ['Slug-Skin', 30, 'slug'],
    ['Many-Limbed Fiend', 20, 'fiend'],
    ['Gouge-Tusks', 15, 'gouge'],
    ['Gnarled Hide', 15, 'hide'],
    ['Rune of the True Beast', 15, 'rune'],
    ['Uncanny Senses', 10, 'uncanny'],
    ['Shadow-Hide', 5, 'sh'],
]
