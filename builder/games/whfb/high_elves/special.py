__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.high_elves.armory import *
from builder.core.options import norm_point_limits

unit_standards = filter_items(he_standard, 50)


class LionChariot(Unit):
    name = 'Lion Chariot of Chrace'
    base_points = 120
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Lion Charioteer', count=2, gear=['Great weapon']).build())
        self.description.add(ModelDescriptor(name='War Lion', count=2).build())


class Skycutter(Unit):
    name = 'Lothern Skycutter'
    base_points = 95

    def __init__(self, boss=False):
        Unit.__init__(self)
        self.boss = boss
        self.static = self.boss
        if not self.boss:
            self.opt = self.opt_options_list('Options', [['Eagle Eye Bolt Thrower', 25, 'bolt']])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description()
        crew = 1 if self.boss else 3 if not self.opt.get('bolt') else 2
        self.description.add(ModelDescriptor(name='Sea Guard Crew', count=crew,
                                             gear=['Hend weapon', 'Spear', 'Bow']).build())
        self.description.add(ModelDescriptor(name='Swiftfeather Roc', count=1).build())


class DragonPrinces(Unit):
    name = 'Dragon Princes of Caledor'
    base_gear = ['Hand weapon', 'Lance', 'Dragon armour', 'Shield', 'Ithilmar barding']

    model_name = 'Dragon Prince'
    model_price = 29
    musician_price = 10
    standard_price = 10
    champion_price = 10
    champion_name = 'Drakemaster'
    champion_gear_limit = 50

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.model_name, 5, 100, self.model_price)
        fcg = [
            [self.champion_name, self.champion_price, 'chmp'],
            ['Standard bearer', self.standard_price, 'sb'],
            ['Musician', self.musician_price, 'mus']
        ]
        self.command_group = self.opt_options_list('Command group', fcg)
        self.magic_wep = self.opt_options_list('Drakemaster\'s magic weapon',
                                               filter_items(he_weapon, self.champion_gear_limit), 1)
        self.magic_arm = self.opt_options_list('Drakemaster\'s Magic armour',
                                               filter_items(he_armour, self.champion_gear_limit), 1)
        self.magic = [self.magic_wep, self.magic_arm]
        self.ban = self.opt_options_list('Magic Standard', filter_items(he_standard, 75), limit=1)

    def get_unique_gear(self):
        if self.ban:
            return self.ban.get_selected()

    def check_rules(self):
        self.ban.set_visible(self.command_group.get('sb'))
        for opt in self.magic:
            opt.set_visible(self.command_group.get('chmp'))
        norm_point_limits(self.champion_gear_limit, self.magic)
        self.points.set(self.build_points(count=1))
        self.build_description(options=[])
        d = ModelDescriptor(self.model_name, gear=self.base_gear, points=self.model_price)
        if self.command_group.get('chmp'):
            rg = ModelDescriptor(self.champion_name, points=self.model_price + self.champion_price,
                                 gear=['Hand weapon', 'Lance', 'Ithilmar barding'])
            for opt in self.magic:
                rg.add_gear_opt(opt)
            if not self.magic_arm.is_selected(he_shields_ids):
                rg.add_gear('Shield')
            if not self.magic_arm.is_selected(he_armour_suits_ids):
                rg.add_gear('Dragon armour')
            self.description.add(rg.build(1))
        if self.command_group.get('sb'):
            self.description.add(d.clone().add_gear('Standard bearer',
                                                    self.standard_price).add_gear_opt(self.ban).build(1))
        if self.command_group.get('mus'):
            self.description.add(d.clone().add_gear('Musician', self.musician_price).build(1))
        command_count = len(self.command_group.get_all())
        self.description.add(d.build(self.count.get() - command_count))

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.ban] if m.get_selected()), [])


class WhiteLions(FCGUnit):
    name = 'White Lions of Chrace'

    def __init__(self):
        self.magic = OptionsList('magic', 'Guardian\'s weapon', filter_items(he_weapon, 25), limit=1)
        FCGUnit.__init__(
            self, model_name='White Lion', model_price=13, min_models=10,
            musician_price=10, standard_price=10,
            champion_name='Guardian', champion_price=10, champion_options=[self.magic],
            base_gear=['Great weapon', 'Heavy armour', 'Lion cloak'],
            magic_banners=unit_standards
        )

    def get_unique_gear(self):
        def safe_list(items):
            return items if items is not None else []
        return safe_list(FCGUnit.get_unique_gear(self)) + safe_list(self.magic.get_selected())


class Swordmasters(WhiteLions):
    name = 'Swordmasters of Hoeth'

    def __init__(self):
        self.magic = OptionsList('magic', 'Bladelord\'s weapon', filter_items(he_weapon, 25), limit=1)
        FCGUnit.__init__(
            self, model_name='Swordmaster', model_price=13, min_models=5,
            musician_price=10, standard_price=10,
            champion_name='Bladelord', champion_price=10, champion_options=[self.magic],
            base_gear=['Great weapon', 'Heavy armour'],
            magic_banners=unit_standards
        )


class PhoenixGuard(WhiteLions):
    name = 'Phoenix Guard'

    def __init__(self):
        self.magic = OptionsList('magic', 'Keeper\'s weapon', filter_items(he_weapon, 25), limit=1)
        FCGUnit.__init__(
            self, model_name='Phoenix Guard', model_price=15, min_models=10,
            musician_price=10, standard_price=10,
            champion_name='Keeper of the Flame', champion_price=10, champion_options=[self.magic],
            base_gear=['Halberd', 'Heavy armour'],
            magic_banners=unit_standards
        )


class Shadow(FCGUnit):
    name = 'Shadow Warriors'

    def __init__(self):
        self.magic = OptionsList('magic', 'Shadow-walker\'s weapon', filter_items(he_weapon, 25), limit=1)
        FCGUnit.__init__(
            self, model_name='Shadow Warrior', model_price=14, min_models=5,
            champion_name='Shadow-walker', champion_price=10, champion_options=[self.magic],
            base_gear=['Hand weapon', 'Long bow', 'Light armour'],
        )

    def get_unique_gear(self):
        return self.magic.get_selected()


class TiranocChariot(Unit):
    name = 'Tiranoc Chariot'
    base_points = 70
    static = True

    def __init__(self, boss=False):
        Unit.__init__(self)
        if not boss:
            self.count = self.opt_count('Tiranoc Chariot', 1, 3, self.base_points)
        self.boss = boss

    def check_rules(self):
        exclude = [self.count.id] if not self.boss else []
        self.set_points(self.build_points(exclude=exclude))
        self.build_description(options=[])
        crew = 2 - (1 if self.boss else 0)
        self.description.add(ModelDescriptor(name='Tiranoc Charioteer', count=crew,
                                             gear=['Hand weapon', 'Spear', 'Long bow']).build())
        self.description.add(ModelDescriptor(name='Elven Steed', count=2).build())
