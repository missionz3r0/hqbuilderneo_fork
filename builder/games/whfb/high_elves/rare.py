__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.high_elves.armory import *
from builder.core.unit import Unit, OptionsList, StaticUnit
from builder.core.model_descriptor import ModelDescriptor


class BoltThrower(Unit):
    name = 'Eagle Claw Bolt Thrower'
    base_points = 70
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Repeater Bolt Thrower', count=1).build())
        self.description.add(ModelDescriptor(name='Sea Guard Crew', count=2,
                                             gear=['Hand weapon', 'Light armour']).build())


class GreatEagles(Unit):
    name = 'Great Eagles'
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Great Eagle', 1, 100, self.base_points)
        self.opt = self.opt_options_list('Options', [
            ['Swiftsense', 10, 'sw'],
            ['Shredding Talons', 5, 'st'],
        ])

    def check_rules(self):
        self.points.set(self.build_points(exclude=[self.count.id ]))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Great Eagle', points=50).add_gear_opt(self.opt).build(self.get_count()))


class Sisters(FCGUnit):
    name = 'Sisters '

    def __init__(self):
        self.magic = OptionsList('magic', 'High sister\'s weapon', filter_items(he_weapon, 25), limit=1)
        FCGUnit.__init__(
            self, model_name='Sister of Avelorn', model_price=14, min_models=5,
            champion_name='High sister', champion_price=10, champion_options=[self.magic],
            base_gear=['Hand weapon', 'Bow of Avelorn', 'Light armour'],
            )

    def get_unique_gear(self):
        return self.magic.get_selected()


class FlamePhoenix(Unit):
    name = 'Flamespyre Phoenix'
    base_points = 225
    static = True


class FrostPhoenix(Unit):
    name = 'Frostheart Phoenix'
    base_points = 240
    static = True
