__author__ = 'Denis Romanov'
from builder.games.whfb.obsolete.wood_elves.mounts import *
from builder.games.whfb.obsolete.wood_elves.armory import *
from builder.core.options import norm_point_limits


heroes_weapon = filter_items(we_weapon, 50)
heroes_armour = filter_items(we_armour, 50)
heroes_talisman = filter_items(we_talisman, 50)
heroes_arcane = filter_items(we_arcane, 50)
heroes_enchanted = filter_items(we_enchanted, 50)


class Noble(Unit):
    name = 'Wood Elf Noble'
    gear = ['Long bow', 'Hand Weapon']
    base_points = 75

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 4, 'hw'],
            ['Spear', 2, 'sp'],
            ['Great weapon', 4, 'gw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
            ['Light armour', 2, 'la'],
        ])
        self.kind = self.opt_options_list('Kindreds', [
            ['Wardancer', 30, 'wd'],
            ['Wild Raider', 35, 'wr'],
            ['Waywacher', 35, 'ww'],
            ['Alter', 25, 'al'],
            ['Scout', 15, 'sc'],
            ['Eternal', 5, 'et'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [ElvenSteed(points=12), GreatEagle(), GreatStag()])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.spites = self.opt_options_list('Spites', spites, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc, self.spites]
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 15, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', we_standard, 1)

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(we_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(we_armour_suits_ids))
        self.magic_arm.set_visible_options(['we_bh'], self.steed.get_count() == 0)
        self.magic_tal.set_visible_options(['we_amber'], self.steed.get_count() == 0)
        self.magic_wep.set_visible_options(['we_bol'], self.kind.get('wd'))
        self.steed.set_visible_options([GreatStag.name], self.kind.get('wr'))
        self.magban.set_active(self.banner.get('bsb'))
        self.wep.set_visible(not self.banner.get('bsb'))
        self.eq.set_active_options(['sh'], not self.banner.get('bsb'))
        self.kind.set_visible(not self.banner.get('bsb'))
        self.gear = ['Hand weapon'] + (['Long bow'] if not self.banner.get('bsb') else [])
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class Spellsinger(Unit):
    name = 'Spellsinger'
    gear = ['Long bow', 'The Lore of Athel Loren']
    base_points = 90

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.kind = self.opt_options_list('Kindreds', [
            ['Glamourweave', 20, 'gw'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [ElvenSteed(12), Unicorn()])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.spites = self.opt_options_list('Spites', spites, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc, self.spites]

    def check_rules(self):
        self.magic_tal.set_visible_options(['we_amber'], self.steed.get_count() == 0)
        self.magic_wep.set_visible_options(['we_bol'], False)
        self.steed.set_visible_options([Unicorn.name], self.kind.get('gw'))
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Branchwraith(Unit):
    name = 'Branchwraith'
    base_points = 60

    def __init__(self):
        Unit.__init__(self)
        self.kind = self.opt_options_list('', [
            ['Level 1 Wizard', 50, 'wiz'],
        ], limit=1)
        self.spites = self.opt_options_list('Spites', spites, points_limit=50)

    def get_unique_gear(self):
        return self.spites.get_selected()
