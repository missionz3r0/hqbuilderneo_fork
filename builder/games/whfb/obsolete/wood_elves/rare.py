__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit, Unit


class Waywachers(FCGUnit):
    name = 'Waywacher'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Wardancer', model_price=24, min_models=5, max_model=10,
            champion_name='Shadow Sentinel', champion_price=8,
            base_gear=['Long bow', 'Hand Weapon', 'Hand Weapon'],
        )


class Treeman(Unit):
    name = 'Treeman'
    base_points = 285
    static = True
