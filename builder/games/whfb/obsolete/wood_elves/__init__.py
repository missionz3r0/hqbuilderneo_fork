__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.obsolete.wood_elves.heroes import *
from builder.games.whfb.obsolete.wood_elves.lords import *
from builder.games.whfb.obsolete.wood_elves.core import *
from builder.games.whfb.obsolete.wood_elves.special import *
from builder.games.whfb.obsolete.wood_elves.rare import *


class WoodElves(LegacyFantasy):
    army_name = 'Wood Elves'
    army_id = 'e45f42ba29cf470697e69bdc17232cd1'
    obsolete = True

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Orion, Drycha, Sisters, Highborn, Spellweaver, Ancient],
            heroes=[Noble, Spellsinger, Branchwraith],
            core=[GladeGuard, Scouts, GladeRaiders, dict(unit=EternalGuard, active=False), Dryads],
            special=[Wardancers, Warhawks, Wild, TreeKin, EternalGuard],
            rare=[Waywachers, Treeman, GreatEagle]
        )

    def check_rules(self):
        LegacyFantasy.check_rules(self)
        highborn = self.lords.count_units([Highborn]) > 0
        self.core.set_active_types([EternalGuard], highborn)
        self.special.set_active_types([EternalGuard], not highborn)
        if not highborn and self.core.count_units([EternalGuard]) > 0:
            self.error("You can't have Eternal Guard in Core without Highborn")

        if self.lords.count_units([GladeGuard]) < self.lords.count_units([Scouts]):
            self.error("You can't have more Scouts units then Glade Guard units")
