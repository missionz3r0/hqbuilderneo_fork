__author__ = 'Denis Romanov'
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.obsolete.wood_elves.armory import *


class Wardancers(FCGUnit):
    name = 'Wardancer Troupe'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Wardancer', model_price=18, min_models=5, max_model=15,
            champion_name='Bladesinger', champion_price=14, musician_price=7,
            base_gear=['Wardancer weapon'],
        )


class Warhawks(FCGUnit):
    name = 'Warhawk Riders'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Warhawk Rider', model_price=40, min_models=3, max_model=12,
            champion_name='Wind Rider', champion_price=20,
            base_gear=['Spear', 'Long bow'],
        )


class Wild(FCGUnit):
    name = 'Wild Raiders of Kurnous'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Wild Rider', model_price=26, min_models=5,
            champion_name='Wild Hunter', champion_price=18, musician_price=0, standard_price=18,
            base_gear=['Spear', 'Light armour'],
            magic_banners=filter_items(we_standard, 50)
        )


class TreeKin(FCGUnit):
    name = 'Tree Kin'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Tree Kin', model_price=65, min_models=3, max_model=12,
            champion_name='Tree Kin Elder', champion_price=20,
            base_gear=['Branchlike limbs'],
            )
