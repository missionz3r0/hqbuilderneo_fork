__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *

spites = [
    ['A Blight of Terrors', 50, 'we_bot'],
    ['A Befuddlement of Mischiefs', 25, 'we_bom'],
    ['A Murder of Spites', 25, 'we_mos'],
    ['A Muster of Malevolents', 25, 'we_mom'],
    ['An Annoyance of Netlings', 25, 'we_aon'],
    ['A Resplendence of Luminescents', 25, 'we_rol'],
    ['A Lamentation of Despair', 25, 'we_lod'],
    ['A Cluster of Radiants', 25, 'we_lod'],
    ['A Pageant of Shrikes ', 25, 'we_pos'],
]

we_weapon = [
    ['The Spirit Sword', 55, 'we_ss'],
    ['Daith\'s Reaper', 50, 'we_dr'],
    ['Blades of Loec', 35, 'we_bol'],  # wardancers only
    ['The Dawn Spear', 35, 'we_ds'],
    ['The Spear of Twilight', 30, 'we_sot'],
    ['The Callach\'s Claw', 25, 'we_claw'],
    ['The Sword of Thousand Winters', 20, 'we_wint'],
    ['Rageth\'s Wildfire Blades', 10, 'we_wild'],
    ['Asyensi\'s Bane', 10, 'we_bane'],
] + magic_weapons

we_armour = [
    ['The Oaken Armour', 50, 'we_oaken'],
    ['Railarian\'s Mantle', 50, 'we_rail'],
    ['Armour of the Fey', 35, 'we_fey'],
    ['The Helm of the Hunt', 20, 'we_hunt'],
    ['Briarsheath', 15, 'we_bh'],  # foot only
] + magic_armours

we_armour_suits_ids = magic_armour_suits_ids + ['we_oaken', 'we_rail', 'we_fey', 'we_bh']
we_shields_ids = magic_shields_ids

we_talisman = [
    ['The Rhymer\'s Harp', 75, 'we_harp'],
    ['Amaranthine Brooch', 35, 'we_ab'],
    ['Amber Pendant', 35, 'we_amber'],  # foot only
    ['Stone of the Crystal Mere', 30, 'we_socm'],
    ['Glamourweave', 30, 'we_gw'],
    ['Stone of Rebirth', 30, 'we_sor'],
    ['The Fimbulwinter Shard', 25, 'we_shard'],
    ['Merciw\'s Locus', 20, 'we_locus'],
] + talismans

we_enchanted = [
    ['Wraithstone', 50, 'we_stone'],
    ['Hagbane Arrows', 35, 'we_arr'],
    ['Moonstone of Hidden Ways', 35, 'we_ways'],
    ['Hail of Doom Arrow', 30, 'we_doom'],
    ['Starfire Arrows', 25, 'we_star'],
    ['The Horn of the Ashai', 25, 'we_horn'],
    ['Arcane Bodkins', 25, 'we_ab'],
    ['Elynett\'s Brooch', 20, 'we_el'],
    ['Dragontooth Arrows', 20, 'we_drag'],
    ['Gwytherc\'s Horn', 15, 'we_horn'],
] + enchanted_items

we_arcane = [
    ['Wand of Wych Eml', 55, 'we_wand'],
    ['Calaingor\'s Stave', 25, 'we_calain'],
    ['Divination Orb', 25, 'we_orb'],
    ['The Deepwood Sphere', 25, 'we_deep'],
    ['Ranu\'s Heartstone', 20, 'we_ranu'],
] + arcane_items

we_standard = [
    ['The Royal Standard of Ariel', 100, 'we_royal'],
    ['Gaemrath - the Banner of Midwinter', 50, 'we_mid'],
    ['Faoghir - the Banner of Dwindling', 50, 'we_dwin'],
    ['Saemrath - the Banner of the Zenith', 25, 'we_zenith'],
    ['Aech - the Banner of Springtide', 25, 'we_spring'],
] + magic_standards
