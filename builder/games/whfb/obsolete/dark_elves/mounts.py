__author__ = 'Denis Romanov'
from builder.core.unit import Unit


class Steed(Unit):
    name = 'Dark Steed'
    base_points = 20
    static = True

    def __init__(self, points=20):
        self.base_points = points
        Unit.__init__(self)


class ColdOne(Unit):
    name = 'Cold One'
    base_points = 30
    static = True

    def __init__(self, points=30):
        self.base_points = points
        Unit.__init__(self)


class DarkPegasus(Unit):
    name = 'Dark Pegasus'
    base_points = 50
    static = True


class Manticore(Unit):
    name = 'Manticore'
    base_points = 200
    static = True


class BlackDragon(Unit):
    name = 'Black Dragon'
    base_points = 320
    static = True
