__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor


class BoltThrower(Unit):
    name = 'Reaper Bolt Thrower'
    base_points = 100
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Reaper', count=1).build())
        self.description.add(ModelDescriptor(name='Dark Elf', count=2,
                                             gear=['Hand weapon', 'Light armour']).build())


class Hydra(Unit):
    name = 'War Hydra'
    base_points = 175
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='War Hydra', count=1).build())
        self.description.add(ModelDescriptor(name='Beastmaster', count=2,
                                             gear=['Scourge', 'Hand weapon']).build())
