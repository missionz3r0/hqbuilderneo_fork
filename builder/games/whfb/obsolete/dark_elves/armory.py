__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *

de_weapon = [
    ['Executioner\'s Axe', 80, 'de_ex'],  # on foot only
    ['Sword of Ruin', 50, 'de_sr'],
    ['Web of Shadows', 50, 'de_wos'],
    ['Chillblade', 50, 'de_chill'],
    ['Heartseeker', 35, 'de_hs'],
    ['Hydra blade', 35, 'de_hb'],
    ['Caledor\'s Bane', 35, 'de_cal'],
    ['Dagger of Hotek', 30, 'de_hot'],
    ['Lifetaker', 30, 'de_lt'],
    ['Crimson Death', 25, 'de_crd'],
    ['Deathpiercer', 25, 'de_pirc'],
    ['Whip of Agony', 25, 'de_whip'],
    ['Soulrender', 15, 'de_slr'],
] + magic_weapons

de_armour = [
    ['Armour of the Living Death', 60, 'de_ald'],
    ['Armour of Eternal Servitude', 35, 'de_aes'],
    ['Cloak of the Hag Graef', 25, 'de_chg'],
    ['Armour of Darkness', 25, 'de_aod'],
    ['Shield of Ghrond', 25, 'de_ghrond'],
    ['Blood Armour', 15, 'de_blood'],
] + magic_armours

de_armour_suits_ids = magic_armour_suits_ids + ['de_ald', 'de_aes', 'de_aod', 'de_blood']
de_shields_ids = magic_shields_ids + ['de_ghrond']

de_talisman = [
    ['Black Amulet', 70, 'de_ba'],
    ['Ring of Darkness', 40, 'de_rd'],
    ['Pendant of Khaeleth', 35, 'de_khael'],
    ['Seal of Ghrond', 30, 'de_sog'],
    ['Pearl if Infinity Blackness', 25, 'de_pib'],
    ['Ring of Hotek', 25, 'de_ring'],
    ['Null Talisman', 15, 'de_nut'],
] + talismans

de_enchanted = [
    ['Deathmask', 50, 'de_mask'],
    ['The Hydra\'s Teeth', 40, 'de_ht'],
    ['Crystal of Midnight', 35, 'de_cm'],
    ['Black Dragon Egg', 30, 'de_egg'],
    ['Potion of Strength', 30, 'de_pos'],
    ['Gem of Nightmares', 25, 'de_gem'],
    ['Guiding Eye', 25, 'de_eye'],
] + enchanted_items

de_arcane = [
    ['Black Staff', 55, 'de_bs'],
    ['Focus Familiar', 25, 'de_ff'],
    ['Darkstar Cloak', 25, 'de_cloak'],
    ['Sacrificial Dagger', 25, 'de_sac'],
    ['Tome of Furion', 15, 'de_tome'],
] + arcane_items

de_standard = [
    ['Banner of Nagarythe', 125, 'de_nag'],
    ['Hydra Banner', 75, 'de_hydra'],
    ['Dread Banner', 40, 'de_dread'],
    ['Standard of Hag Graef', 35, 'de_hag'],
    ['Banner of Murder', 25, 'de_mur'],
    ['Banner of Cold Blood', 15, 'de_cold'],
] + magic_standards

gifts = [
    ['Venom sword', 75, 'de_ven'],
    ['Dance of Doom', 30, 'de_doom'],
    ['Touch of Death', 30, 'de_touch'],
    ['Rune of Khaine', 25, 'de_rune'],
    ['Hand of Khaine', 15, 'de_hand'],
    ['Cry of War', 10, 'de_cry'],
    ['Manbane', 25, 'de_mb'],
    ['Black Lotus', 20, 'de_bl'],
    ['Dark Venom', 10, 'de_dv'],
]

assassin_gifts = [
    ['Rending stars', 30, 'de_stars'],
    ['Cloak of Twilight', 20, 'de_ct'],
] + gifts

hag_gifts = [
    ['Witchbrew', 25, 'de_brew']
] + gifts
