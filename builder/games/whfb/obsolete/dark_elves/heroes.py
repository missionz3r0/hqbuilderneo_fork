__author__ = 'Denis Romanov'
from builder.core.unit import StaticUnit
from builder.games.whfb.obsolete.dark_elves.mounts import *
from builder.games.whfb.obsolete.dark_elves.armory import *
from builder.games.whfb.obsolete.dark_elves.special import ColdOneChariot
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


heroes_weapon = filter_items(de_weapon, 50)
heroes_armour = filter_items(de_armour, 50)
heroes_talisman = filter_items(de_talisman, 50)
heroes_arcane = filter_items(de_arcane, 50)
heroes_enchanted = filter_items(de_enchanted, 50)


class Sorceress(Unit):
    name = 'Sorceress'
    gear = ['Hand Weapon']
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [Steed(points=12), ColdOne(points=20), DarkPegasus()])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Dark Magic', 0, 'dark'],
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Master(Unit):
    name = 'Master'
    gear = ['Hand Weapon']
    base_points = 80

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Halberd', 4, 'hb'],
            ['Great weapon', 4, 'gw'],
            ['Hand weapon', 4, 'hw'],
            ['Lance', 4, 'ln'],
            ['Beastmaster\'s scourge', 6, 'sc'],
        ], limit=1)
        self.wep_1 = self.opt_options_list('', [
            ['Repeater crossbow', 10, 'rc'],
            ['Repeater handbow', 8, 'rh'],
            ['Pair of repeater handbows', 16, 'prh'],
        ])
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
            ['Sea Dragon cloak', 4, 'lc'],
        ])
        self.arm = self.opt_options_list('', [
            ['Light armour', 2, 'la'],
            ['Heavy armour', 4, 'ha'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [Steed(points=12), ColdOne(points=20), DarkPegasus(),
                                                          Manticore(), ColdOneChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', de_standard, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(de_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(de_armour_suits_ids))
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class DeathHag(Unit):
    name = 'Death Hag'
    gear = ['Hand Weapon' for _ in range(2)]
    base_points = 90

    class Cauldron(Unit):
        name = 'Cauldron of Blood'
        base_points = 110
        static = True

        def check_rules(self):
            Unit.check_rules(self)
            self.description.add(ModelDescriptor('Hag', gear=['Hand Weapon' for _ in range(2)]).build(2))

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [self.Cauldron()])
        self.magic_wep = self.opt_options_list('Gifts of Khaine', filter_items(hag_gifts, 50), points_limit=50)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', de_standard, 1)

    def check_rules(self):
        self.magban.set_active(self.banner.get('bsb'))
        Unit.check_rules(self)

    def get_unique_gear(self):
        return self.magban.get_selected()


class Malus(StaticUnit):
    name = 'Malus Darkblade, Scion of Hag Graef'
    base_points = 275
    gear = ['Heavy armour', 'Wharpsword of Khaine']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor('Spite').build(1))


class Shadowblade(StaticUnit):
    name = 'Shadowblade, Master of Assassins'
    base_points = 300
    gear = ['Hand weapon', 'Hand weapon', 'Heart of Woe', 'Potion of Strength', 'Rending Stars', 'Dance of Death',
            'Touch of Doom', 'Hand of Khaine', 'Dark Venom']


class Lokhir(StaticUnit):
    name = 'Lokhir the Fellheart, Caption of the Tower of Blessed Dread'
    base_points = 250
    gear = ['The Red Blades', 'Sea Dragon Cloak', 'Helm of Kraken']
