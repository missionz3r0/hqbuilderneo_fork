__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *

_weapon = [
    ['Blade of Realities', 75, 'liz_bor'],
    ['Blade of Revered Tzunki', 65, 'liz_rev'],
    ['Scimitar of the Sun Resplendent', 50, 'liz_scim'],
    ['Staff of the lost Sun', 35, 'liz_staff'],
    ['Piranha Blade', 35, 'liz_pira'],
    ['Dagger of Sotek', 25, 'liz_dac'],
    ['Sword of Hornet', 25, 'liz_horn'],
    ['Burning blade of Crotec', 20, 'liz_burn'],
]

_armour = [
    ['Shield of the Mirrored Pool', 30, 'liz_mirr'],
    ['The Maiming Shield', 30, 'liz_maim'],
]

liz_magic_weapons = _weapon + magic_weapons
heroes_liz_magic_weapons = filter_items(liz_magic_weapons, 50)
skink_magic_weapons = _weapon + [['Stegadon War-Spear', 50, 'liz_steg']] + magic_weapons
skink_magic_armour = [['Sacred Stegadon Helm', 50, 'liz_helm']] + _armour + magic_armours
heroes_skink_magic_weapons = filter_items(skink_magic_weapons, 50)
heroes_skink_magic_armour = filter_items(skink_magic_armour, 50)
saurus_magic_armour = [['Hide of the Cold Ones', 50, 'liz_hide']] + _armour + magic_armours
heroes_saurus_magic_armour = filter_items(saurus_magic_armour, 50)
liz_magic_shields_ids = ['liz_mirr', 'liz_maim'] + magic_shields_ids
liz_magic_armour_suits_ids = ['liz_hide'] + magic_armour_suits_ids

liz_talismans = [
    ['Aura of Quetzl', 40, 'liz_aura'],
    ['Glyph Necklace', 30, 'liz_glyph'],
    ['Amulet of Itzl', 30, 'liz_itzl'],
] + talismans
heroes_liz_talismans = filter_items(liz_talismans, 50)

liz_arcane_items = [
    ['Cupped Hands of the Old Ones', 45, 'liz_hands'],
    ['Cube of Darkness', 40, 'liz_cube'],
    ['Rod of Storm', 25, 'liz_rod'],
    ['Diadem of Rower', 25, 'liz_diad'],
    ['Itxi Grubs', 25, 'liz_itxi'],
    ['Plaque of Tepok', 15, 'liz_tepok'],
] + arcane_items
heroes_liz_arcane_items = filter_items(liz_arcane_items, 50)

# ['Horned One', 35, 'liz_ho'],  # Saurus or Skink only
# ['Divine Plaque of Protection', 30, 'liz_dpp'],  # Slann only
# ['Cloak of Feathers', 25, 'liz_cof'],  # Skink on Foot only
# ['Carnosaur Pendant', 20, 'liz_cp'],  # Saurus or Skink only

liz_enchanted_items = [
    ['The Horn of Kygor', 100, 'liz_horn'],
    ['War Drum of Xahitec', 30, 'liz_wdx'],
    ['Charm of Jaguar Warrior', 25, 'liz_cjw'],  # Foot only
    ['Curse-Charm of Tepok', 20, 'liz_cct'],
    ['Bane Head', 15, 'liz_head'],
    ['Dragonfly of Quicksilver', 10, 'liz_doq'],
    ['Venom of the Firefly Fog', 10, 'liz_vff']
] + enchanted_items

slann_enchanted_items = [['Divine Plaque of Protection', 30, 'liz_dpp']] + liz_enchanted_items
saurus_enchanted_items = [
    ['Horned One', 35, 'liz_ho'],  # Saurus or Skink only
    ['Carnosaur Pendant', 20, 'liz_cp'],  # Saurus or Skink only
] + liz_enchanted_items

skink_enchanted_items = [
    ['Horned One', 35, 'liz_ho'],  # Saurus or Skink only
    ['Cloak of Feathers', 25, 'liz_cof'],  # Skink on Foot only
    ['Carnosaur Pendant', 20, 'liz_cp'],  # Saurus or Skink only
] + liz_enchanted_items
heroes_saurus_enchanted_items = filter_items(saurus_enchanted_items, 50)
heroes_skink_enchanted_items = filter_items(skink_enchanted_items, 50)

# orcs_armour_suits_ids = ['orcs_ag'] + magic_armour_suits_ids
#
# orcs_arcane_items = [
#     ['Lucky Srhunken Head', 50, 'orcs_lsh'],
# ] + arcane_items
#
#
# orcs_enchanted_items = [
#     ['Skull Wand of Kaloth', 75, 'orcs_swk']
# ] + enchanted_items
#
liz_magic_standards = [
    ['Totem of Prophecy', 50, 'liz_tp'],
    ['Plaque of Dominion', 50, 'liz_pd'],
    ['Sun Standard of Chotec', 40, 'liz_ssc'],
    ['Huanchui\'s Blessed Totem', 25, 'liz_hbt'],
    ['Jaguar Standard', 25, 'liz_js'],
] + magic_standards

skink_magic_standards = [
    ['Skavenpelt Banner', 25, 'liz_spb'],  # Skinks only
] + liz_magic_standards

#
#
# orcs_armours_heroes = filter_items(orcs_armours, 50)
# orcs_magic_weapons_heroes = filter_items(orcs_magic_weapons, 50)
# magic_shields_heroes = filter_items(magic_shields, 50)
# talismans_heroes = filter_items(talismans, 50)
# orcs_arcane_items_heroes = filter_items(orcs_arcane_items, 50)
# orcs_enchanted_items_heroes = filter_items(orcs_enchanted_items, 50)
