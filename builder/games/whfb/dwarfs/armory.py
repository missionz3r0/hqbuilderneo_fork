from builder.games.whfb.legacy_armory import multiple_id

__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *


def multiple_runes(name, points, item_id, count=3):
    return [[name, points, multiple_id(item_id, i)] for i in range(count)]


def process_base_runes(rune_list, rune_id, count=3):
    selected = sum((1 for i in range(count) if rune_list.get(multiple_id(rune_id, i))))
    for i in range(count):
        full_id = multiple_id(rune_id, i)
        if rune_list.get(full_id) or selected >= 0:
            rune_list.set_visible_options([multiple_id(rune_id, i)], True)
        else:
            rune_list.set_visible_options([multiple_id(rune_id, i)], False)
        selected -= 1


def process_base_rune_list(rune_list, id_list):
    for rune_id in id_list:
        process_base_runes(rune_list, rune_id)


def get_master_runes_ids(runes):
    return {v[2]: v[0] for v in runes}


master_standard_runes = [
    ['Master Rune of Groth One-Eye', 75, 'dw_oneeye'],
    ['Master Rune of Stromni Redbeard', 75, 'dw_Stromni'],
    ['Master Rune of Valaya', 65, 'dw_valaya'],
    ['Master Rune of Fear', 75, 'dw_fear'],
    ['Master Rune of Grungni', 60, 'dw_Grungni'],
]

base_standard_runes = [
    ['Rune of Battle', 35, 'dw_rob'],
    ['Rune of Battle (2)', 70, 'dw_rob2'],
    ['Rune of Battle (3)', 125, 'dw_rob3'],
    ['Rune of Slowness', 35, 'dw_slowness'],
    ['Rune of Slowness (2)', 50, 'dw_slowness2'],
    ['Rune of Slowness (3)', 80, 'dw_slowness3'],
    ['Rune of Sanctuary', 15, 'dw_ros'],
    ['Rune of Sanctuary (2)', 30, 'dw_ros2'],
    ['Rune of Sanctuary (3)', 45, 'dw_ros3'],
    ['Rune of Stoicism', 35, 'dw_stoicism'],
    ['Strolazz\'s Rune', 35, 'dw_strolazz'],
    ['Rune of Slowness', 50, 'dw_rosl'],
    ['Rune of Courage', 20, 'dw_roc'],
    ['Rune of Guarding', 30, 'dw_rogrd'],
    ['Rune of Determination', 20, 'dw_det'],
    ['Ancestor Rune', 20, 'dw_ancestor']
]

standard_runes = master_standard_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_standard_runes), [])
master_standard_runes_id = get_master_runes_ids(master_standard_runes)
base_standard_runes_id = get_ids(base_standard_runes)

lesser_standard_runes = filter_items(standard_runes, 50)
lesser_runic_standards = sum((multiple_runes(v[0], v[1], v[2]) for v in lesser_standard_runes), [])
lesser_runic_standards_ids = get_ids(lesser_standard_runes)


master_eng_runes = [
    ['Master Rune of Defence', 40, 'dw_mrod'],
    ['Master Rune of Immolation', 30, 'dw_mroim'],
    ['Master Rune of Disguise', 25, 'dw_mrodis'],
]

base_eng_runes = [
    ['Rune of Fortune', 25, 'dw_rofort'],
    ['Rune of Penetrating', 40, 'dw_ropen'],
    ['Rune of Penetrating (2)', 50, 'dw_ropen2'],
    ['Valiant Rune', 20, 'dw_val'],
    ['Stalwart Rune', 15, 'dw_str'],
    ['Stalwart Rune (2)', 30, 'dw_str2'],
    ['Rune of Accuracy', 25, 'dw_roa'],
    ['Rune of Burning', 5, 'dw_rbur'],
]

base_cannon_runes = base_eng_runes + [
    ['Rune of Forging', 25, 'dw_rof'],
    ['Rune of Reloading', 30, 'dw_ror']
]

base_stone_thrower_runes = base_eng_runes + [

]

base_bolt_thrower_runes = base_eng_runes + [
    ['Flakkson\'s Rune of Seeking', 15, 'dw_fros']
]

master_bolt_thrower_runes = master_eng_runes + [
    ['Master Rune of Skewering', 30, 'dw_mrosk']
]

master_cannon_runes = master_eng_runes
master_stone_thrower_runes = master_eng_runes

cannon_runes = master_cannon_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_cannon_runes), [])
master_cannon_runes_id = get_master_runes_ids(master_cannon_runes)
base_cannon_runes_id = get_ids(base_cannon_runes)


bolt_thrower_runes = master_bolt_thrower_runes + sum((multiple_runes(v[0], v[1], v[2])
                                                      for v in base_bolt_thrower_runes), [])
master_bolt_thrower_runes_id = get_master_runes_ids(master_bolt_thrower_runes)
base_bolt_thrower_runes_id = get_ids(base_bolt_thrower_runes)


stone_thrower_runes = master_stone_thrower_runes + sum((multiple_runes(v[0], v[1], v[2])
                                                        for v in base_stone_thrower_runes), [])
master_stone_thrower_runes_id = get_master_runes_ids(master_stone_thrower_runes)
base_stone_thrower_runes_id = get_ids(base_stone_thrower_runes)

master_weapon_runes = [
    ['Master Rune of Skalf Blackhammer', 75, 'dw_skalf'],
    ['Master Rune of Smiting', 60, 'dw_smiting'],
    ['Master Rune of Alaric the Mad', 50, 'dw_alaric'],
    ['Master Rune of Breaking', 50, 'dw_breaking'],
    ['Master Rune of Dragon Slaying', 50, 'dw_slaying'],
    ['Master Rune of Flight', 30, 'dw_flight'],
    ['Master Rune of Swiftness', 25, 'dw_swiftness'],
    ['Master Rune of Snorri Spangelhelm', 25, 'dw_snorri'],
    ['Master Rune of Krag the Grim', 20, 'dw_krag'],
]

base_weapon_runes = [
    ['Rune of Daemon Slaying', 25, 'dw_daemon_slay'],
    ['Rune of Daemon Slaying (2)', 50, 'dw_daemon_slay2'],
    ['Rune of Daemon Slaying (3)', 125, 'dw_daemon_slay3'],
    ['Rune of Fire', 10, 'dw_fire'],
    ['Rune of Fire (2)', 50, 'dw_fire2'],
    ['Rune of Fire (3)', 125, 'dw_fire3'],
    ['Rune of Fury', 25, 'dw_fury'],
    ['Rune of Fury (2)', 60, 'dw_fury2'],
    ['Rune of Fury (3)', 100, 'dw_fury3'],
    ['Rune of Dismay', 20, 'dw_dismay'],
    ['Rune of Dismay (2)', 45, 'dw_dismay2'],
    ['Rune of Dismay (3)', 80, 'dw_dismay3'],
    ['Rune of Cleaving', 10, 'dw_cleaving'],
    ['Rune of Cleaving (2)', 35, 'dw_cleaving2'],
    ['Rune of Cleaving (3)', 65, 'dw_cleaving3'],
    ['Rune of Might', 25, 'dw_might'],
    ['Rune of Might (2)', 60, 'dw_might2'],
    ['Rune of Striking', 10, 'dw_striking'],
    ['Rune of Striking (2)', 35, 'dw_striking2'],
    ['Rune of Striking (3)', 60, 'dw_striking3'],
    ['Grudge Rune', 15, 'dw_grudge'],
    ['Rune of Parrying', 25, 'dw_parry'],
    ['Rune of Speed', 5, 'dw_speed'],
]

weapon_runes = master_weapon_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_weapon_runes), [])
master_weapon_runes_id = get_master_runes_ids(master_weapon_runes)
base_weapon_runes_id = get_ids(base_weapon_runes)


master_armour_runes = [
    ['Master Rune of Steel', 50, 'dw_steel'],
    ['Master Rune of Adamant', 100, 'dw_adamant'],
    ['Master Rune of Gromril', 30, 'dw_gromril'],
]

base_armour_runes = [
    ['Rune of Fortitude', 35, 'dw_fortitude'],
    ['Rune of Fortitude (2)', 50, 'dw_fortitude2'],
    ['Rune of Fortitude (3)', 75, 'dw_fortitude3'],
    ['Rune of Iron', 20, 'dw_iron'],
    ['Rune of Iron (2)', 45, 'dw_iron2'],
    ['Rune of Iron (3)', 70, 'dw_iron3'],
    ['Rune of Shielding', 25, 'dw_shielding'],
    ['Rune of Resistance', 25, 'dw_resistance'],
    ['Rune of Preservation', 25, 'dw_pres'],
    ['Rune of Impact', 10, 'dw_impact'],
    ['Rune of Stone', 5, 'dw_stone'],

]

armour_runes = master_armour_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_armour_runes), [])
master_armour_runes_id = get_master_runes_ids(master_armour_runes)
base_armour_runes_id = get_ids(base_armour_runes)

master_talisman_runes = [
    ['Master Rune of Kingship', 100, 'dw_kingship'],
    ['Master Rune of Spite', 25, 'dw_spite'],
    ['Master Rune of Passage', 10, 'dw_passage'],
    ['Master Rune of Challenge', 25, 'dw_challenge'],
    ['Master Rune of Dismay', 25, 'dw_dismay'],
]

runesmith_master_talisman_runes = [
    ['Master Rune of Balance', 50, 'dw_balance'],  # Runesmith only
    ['Master Rune of Spellbinding', 50, 'dw_spellbinding'],  # Runesmith only
]

base_talisman_runes = [
    ['Spelleater Rune', 50, 'dw_spelleater'],
    ['Rune of Fate', 35, 'dw_fate'],
    ['Rune of Spellbreaking', 25, 'dw_spellbreaking'],
    ['Rune of Spellbreaking (2)', 45, 'dw_spellbreaking2'],
    ['Rune of Brotherhood', 20, 'dw_brotherhood'],
    ['Rune of Luck', 15, 'dw_luck'],
    ['Rune of Warding', 15, 'dw_warding'],
    ['Rune of the Furnace', 5, 'dw_furnace'],
]

talisman_runes = master_talisman_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_talisman_runes), [])
master_talisman_runes_id = get_master_runes_ids(master_talisman_runes)
base_talisman_runes_id = get_ids(base_talisman_runes)

runesmith_talisman_runes = master_talisman_runes + runesmith_master_talisman_runes + \
                           sum((multiple_runes(v[0], v[1], v[2]) for v in base_talisman_runes), [])
runesmith_master_talisman_runes_id = get_master_runes_ids(runesmith_master_talisman_runes)
runesmith_base_talisman_runes_id = get_ids(base_talisman_runes)

