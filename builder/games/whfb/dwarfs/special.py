__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList, OneOf
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.dwarfs.armory import *


class Hammerers(FCGUnit):
    name = 'Hammerers'

    def __init__(self):
        self.runic = OptionsList('opt', 'Runic standard', lesser_runic_standards, points_limit=50)
        FCGUnit.__init__(
            self, model_name='Hammerer', model_price=12, min_models=5,
            musician_price=6, standard_price=12, champion_name='Gate Keeper', champion_price=12,
            base_gear=['Heavy armour', 'Hand weapon', 'Great weapon'],
            options=[OptionsList('arm', 'Armour', [
                ['Shield', 1, 'sh'],
            ])],
            unit_options=[self.runic]
        )

    def check_rules(self):
        self.runic.set_visible(self.has_flag())
        process_base_rune_list(self.runic, lesser_runic_standards_ids)
        FCGUnit.check_rules(self)


class Ironbreakers(FCGUnit):
    name = 'Ironbreakers'

    def __init__(self):
        self.runic = OptionsList('opt', 'Runic standard', lesser_runic_standards, points_limit=50)
        FCGUnit.__init__(
            self, model_name='Ironbreaker', model_price=13, min_models=5,
            musician_price=6, standard_price=12, champion_name='Ironbeard', champion_price=12,
            base_gear=['Gromril armour', 'Hand weapon', 'Shield'],
            unit_options=[self.runic]
        )

    def check_rules(self):
        self.runic.set_visible(self.has_flag())
        process_base_rune_list(self.runic, lesser_runic_standards_ids)
        FCGUnit.check_rules(self)


class Slayers(Unit):
    name = 'Slayers'
    unique = True
    base_gear = ['Slayer axes']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Troll Slayer', 10, 30, 11)
        fcg = [
            ['Standard bearer', 12, 'sb'],
            ['Musician', 6, 'mus']
        ]
        self.command_group = self.opt_options_list('Command group', fcg)
        self.gs = self.opt_count('Giant slayer', 0, 10, 15)

    def check_rules(self):
        self.gs.update_range(0, self.count.get())
        self.points.set(self.build_points(count=1))
        self.build_description(options=[])
        ts_desc = ModelDescriptor('Troll Slayer', gear=self.base_gear, points=11)
        gs_desc = ModelDescriptor('Giant Slayer', gear=self.base_gear, points=11 + 15)
        ts = self.count.get() - self.gs.get()
        gs = self.gs.get()
        if self.command_group.get('sb'):
            if ts > 0:
                d = ts_desc
                ts -= 1
            else:
                d = gs_desc
                gs -= 1
            self.description.add(d.clone().add_gear('Standard bearer', 12).build(1))
        if self.command_group.get('mus'):
            if ts > 0:
                d = ts_desc
                ts -= 1
            else:
                d = gs_desc
                gs -= 1
            self.description.add(d.clone().add_gear('Musician', 6).build(1))
        self.description.add(ts_desc.build(ts))
        self.description.add(gs_desc.build(gs))


class WarMachine(Unit):
    name = 'War Machine'

    def __init__(self, runes=None, ids=None, master_ids=None):
        Unit.__init__(self)
        self.base_runes_ids = ids
        self.master_runes_ids = master_ids
        self.rune_list = runes
        self.runes = self.opt_options_list('Runes', runes, limit=3) if runes else None
        self.eng = self.opt_options_list('Crew', [['Engineer', 15, 'eng']])
        self.wep = self.opt_options_list('', [
            ['Dwarf handgun', 5, 'hg'],
            ['Brace of pistols', 5, 'bp']
        ])

    def check_rules(self):
        self.wep.set_visible(self.eng.get('eng'))
        if self.runes and sum((1 for rune_id in list(self.master_runes_ids.keys()) if self.runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on {0}'.format(self.name))
        if self.base_runes_ids:
            process_base_rune_list(self.runes, self.base_runes_ids)
        self.set_points(self.build_points())
        self.build_description(options=[self.runes])
        self.description.add(ModelDescriptor(name=self.name, count=1).build())
        self.description.add(ModelDescriptor(name='Crew', count=3, gear=['Hand weapon', 'Light armour']).build())
        if self.eng.get('eng'):
            self.description.add(ModelDescriptor(name='Engineer',
                                                 count=1,
                                                 gear=['Hand weapon', 'Light armour'],
                                                 points=15).add_gear_opt(self.wep).build(1))

    def get_unique_gear(self):
        return sum(([self.master_runes_ids[rune_id]]
                    for rune_id in list(self.master_runes_ids.keys()) if self.runes.get(rune_id)), [])


class Cannon(WarMachine):
    name = 'Cannon'
    base_points = 90

    def __init__(self):
        WarMachine.__init__(self, runes=cannon_runes, ids=base_cannon_runes_id, master_ids=master_cannon_runes_id)


class BoltThrower(WarMachine):
    name = 'Bolt Thrower'
    base_points = 45

    def __init__(self):
        WarMachine.__init__(self, runes=bolt_thrower_runes, ids=base_bolt_thrower_runes_id,
                            master_ids=master_bolt_thrower_runes_id)


class StoneThrower(WarMachine):
    name = 'Grudge Thrower'
    base_points = 80

    def __init__(self):
        WarMachine.__init__(self, runes=stone_thrower_runes, ids=base_stone_thrower_runes_id,
                            master_ids=master_stone_thrower_runes_id)


class Miners(FCGUnit):
    name = 'Miners'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Miner', model_price=11, min_models=5,
            musician_price=5, standard_price=10, champion_name='Prospector', champion_price=10,
            base_gear=['Hand weapon', 'Heavy armor'], rank_gear=['Pick'],
            champion_options=[OneOf('arm', 'Prospector\'s weapon', [
                ['Pick', 0, 'hg'],
                ['Steam drill', 25, 'bp']
            ])],
            unit_options=[OptionsList('opt', 'Options', [['Blasting charges', 30, 'bch']])]
        )
