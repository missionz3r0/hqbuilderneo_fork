__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.dwarfs.armory import *
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Thorgrim(StaticUnit):
    name = 'Thorgrim Grudgebearer'
    base_points = 780
    gear = ['The Armour of Skaldour', 'The Throne of Power', 'The Great Book of Grudges', 'The Dragon Crown of Karaz',
            'The Axe of Grimnir', 'Thronebearers']


class Thorek(StaticUnit):
    name = 'Thorek Ironbrow'
    base_points = 505
    gear = ['Klad Brakak', 'Thorek\'s Rune Armour']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Anvil of Doom').build(1))
        self.description.add(ModelDescriptor(name='Kraggi', gear=['Hand weapon', 'Light armour']).build(1))
        self.description.add(ModelDescriptor(name='Anvil Guard',
                                             gear=['Hand weapon', 'Gromril armour', 'Shield']).build(2))


class Lord(Unit):
    name = 'Lord'
    gear = ['Hand Weapon', 'Gromril armour']
    base_points = 145

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Pistol', 10, 'pis'],
        ], limit=1)
        self.wep_1 = self.opt_options_list('', [
            ['Crossbow', 10, 'cb'],
            ['Dwarf handgun', 15, 'hg'],
        ], limit=1)
        self.sh = self.opt_options_list('', [
            ['Shield', 3, 'sh'],
        ])
        self.opt = self.opt_options_list('', [
            ['Oath stone', 30, 'os'],
            ['Shieldbearers', 25, 'sb'],
        ], limit=1)
        self.wep_runes = self.opt_options_list('Weapon runes', weapon_runes, limit=3)
        self.arm_runes = self.opt_options_list('Armour runes', armour_runes, limit=3)
        self.tal_runes = self.opt_options_list('Talisman runes', talisman_runes, limit=3)
        self.magic = [self.wep_runes, self.arm_runes, self.tal_runes]

    def check_rules(self):
        if sum((1 for rune_id in list(master_weapon_runes_id.keys()) if self.wep_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on weapon')
        if sum((1 for rune_id in list(master_armour_runes_id.keys()) if self.arm_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on armour')
        if sum((1 for rune_id in list(master_talisman_runes_id.keys()) if self.tal_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on talisman')
        process_base_rune_list(self.wep_runes, base_weapon_runes_id)
        process_base_rune_list(self.arm_runes, base_armour_runes_id)
        process_base_rune_list(self.tal_runes, base_talisman_runes_id)
        norm_point_limits(125, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        def check_runes(ids, opt):
            return sum(([ids[rune_id]] for rune_id in list(ids.keys()) if opt.get(rune_id)), [])
        return sum((check_runes(ids, opt) for ids, opt in [
            (master_weapon_runes_id, self.wep_runes),
            (master_armour_runes_id, self.arm_runes),
            (master_talisman_runes_id, self.tal_runes),
        ]), [])


class Runelord(Unit):
    name = 'Runelord'
    gear = ['Hand Weapon', 'Gromril armour']
    base_points = 140

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
        ], limit=1)
        self.sh = self.opt_options_list('', [
            ['Shield', 3, 'sh'],
        ])
        self.opt = self.opt_options_list('', [
            ['Anvil of Doom', 175, 'ad'],
        ], limit=1)
        self.wep_runes = self.opt_options_list('Weapon runes', weapon_runes, limit=3)
        self.arm_runes = self.opt_options_list('Armour runes', armour_runes, limit=3)
        self.tal_runes = self.opt_options_list('Talisman runes', runesmith_talisman_runes, limit=3)
        self.magic = [self.wep_runes, self.arm_runes, self.tal_runes]

    def check_rules(self):
        if sum((1 for rune_id in list(master_weapon_runes_id.keys()) if self.wep_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on weapon')
        if sum((1 for rune_id in list(master_armour_runes_id.keys()) if self.arm_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on armour')
        if sum((1 for rune_id in list(runesmith_master_talisman_runes_id.keys()) if self.tal_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on talisman')
        process_base_rune_list(self.wep_runes, base_weapon_runes_id)
        process_base_rune_list(self.arm_runes, base_armour_runes_id)
        process_base_rune_list(self.tal_runes, base_talisman_runes_id)
        norm_point_limits(150, self.magic)
        self.points.set(self.build_points())
        self.build_description(exclude=[self.opt.id])
        if self.opt.get('ad'):
            self.description.add(ModelDescriptor(name='Anvil of Doom').build(1))
            self.description.add(ModelDescriptor(name='Anvil Guard',
                                                 gear=['Hand weapon', 'Gromril armour', 'Shield']).build(2))

    def get_unique_gear(self):
        def check_runes(ids, opt):
            return sum(([ids[rune_id]] for rune_id in list(ids.keys()) if opt.get(rune_id)), [])
        return sum((check_runes(ids, opt) for ids, opt in [
            (master_weapon_runes_id, self.wep_runes),
            (master_armour_runes_id, self.arm_runes),
            (runesmith_master_talisman_runes_id, self.tal_runes),
        ]), [])

    def has_anvil(self):
        return self.opt.get('ad')


class DaemonSlayer(Unit):
    name = 'Daemon Slayer'
    gear = ['Slayer axes']
    base_points = 110

    def __init__(self):
        Unit.__init__(self)
        self.wep_runes = self.opt_options_list('Weapon runes', weapon_runes, limit=3, points_limit=100)

    def check_rules(self):
        if sum((1 for rune_id in list(master_weapon_runes_id.keys()) if self.wep_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on weapon')
        process_base_rune_list(self.wep_runes, base_weapon_runes_id)
        Unit.check_rules(self)

    def get_unique_gear(self):
        def check_runes(ids, opt):
            return sum(([ids[rune_id]] for rune_id in list(ids.keys()) if opt.get(rune_id)), [])
        return sum((check_runes(ids, opt) for ids, opt in [
            (master_weapon_runes_id, self.wep_runes),
        ]), [])
