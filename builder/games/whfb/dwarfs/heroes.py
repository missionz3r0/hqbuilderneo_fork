__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.dwarfs.armory import *
from builder.core.options import norm_point_limits


class Bugman(StaticUnit):
    base_points = 155
    name = 'Josef Bugman'
    gear = ['Runic Axe', 'Crossbow', 'Gromril armour', 'Shield', 'Bugman\'s Tankard']


class Thane(Unit):
    name = 'Thane'
    gear = ['Hand Weapon', 'Gromril armour']
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Pistol', 5, 'pis'],
        ], limit=1)
        self.wep_1 = self.opt_options_list('', [
            ['Crossbow', 5, 'cb'],
            ['Dwarf handgun', 10, 'hg'],
        ], limit=1)
        self.sh = self.opt_options_list('', [
            ['Shield', 2, 'sh'],
        ])
        self.opt = self.opt_options_list('', [
            ['Oath stone', 20, 'os'],
        ], limit=1)
        self.wep_runes = self.opt_options_list('Weapon runes', filter_items(weapon_runes, 75), limit=3)
        self.arm_runes = self.opt_options_list('Armour runes', filter_items(armour_runes, 75), limit=3)
        self.tal_runes = self.opt_options_list('Talisman runes', filter_items(talisman_runes, 75), limit=3)
        self.magic = [self.wep_runes, self.arm_runes, self.tal_runes]
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Standard Runes', standard_runes, limit=3)

    def check_rules(self):
        if sum((1 for rune_id in list(master_weapon_runes_id.keys()) if self.wep_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on weapon')
        if sum((1 for rune_id in list(master_armour_runes_id.keys()) if self.arm_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on armour')
        if sum((1 for rune_id in list(master_talisman_runes_id.keys()) if self.tal_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on talisman')
        process_base_rune_list(self.wep_runes, base_weapon_runes_id)
        process_base_rune_list(self.arm_runes, base_armour_runes_id)
        process_base_rune_list(self.tal_runes, base_talisman_runes_id)
        process_base_rune_list(self.magban, base_standard_runes_id)

        self.wep.set_visible(not self.banner.get('bsb'))
        self.wep_1.set_visible(not self.banner.get('bsb'))
        self.sh.set_visible(not self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(75, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        def check_runes(ids, opt):
            return sum(([ids[rune_id]] for rune_id in list(ids.keys()) if opt.get(rune_id)), [])
        return sum((check_runes(ids, opt) for ids, opt in [
            (master_weapon_runes_id, self.wep_runes),
            (master_armour_runes_id, self.arm_runes),
            (master_talisman_runes_id, self.tal_runes),
            (master_standard_runes_id, self.magban),
        ]), [])


class MasterEngineer(Unit):
    name = 'Master Engineer'
    gear = ['Hand Weapon', 'Gromril armour']
    base_points = 70

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_count('Pistol', 0, 2, 5)
        self.wep_1 = self.opt_options_list('', [
            ['Great weapon', 4, 'gw'],
            ['Dwarf handgun', 10, 'hg'],
        ], limit=1)
        self.wep_runes = self.opt_options_list('Weapon runes', filter_items(weapon_runes, 50), limit=3)
        self.arm_runes = self.opt_options_list('Armour runes', filter_items(armour_runes, 50), limit=3)
        self.tal_runes = self.opt_options_list('Talisman runes', filter_items(talisman_runes, 50), limit=3)
        self.magic = [self.wep_runes, self.arm_runes, self.tal_runes]

    def check_rules(self):
        if sum((1 for rune_id in list(master_weapon_runes_id.keys()) if self.wep_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on weapon')
        if sum((1 for rune_id in list(master_armour_runes_id.keys()) if self.arm_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on armour')
        if sum((1 for rune_id in list(master_talisman_runes_id.keys()) if self.tal_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on talisman')
        process_base_rune_list(self.wep_runes, base_weapon_runes_id)
        process_base_rune_list(self.arm_runes, base_armour_runes_id)
        process_base_rune_list(self.tal_runes, base_talisman_runes_id)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        def check_runes(ids, opt):
            return sum(([ids[rune_id]] for rune_id in list(ids.keys()) if opt.get(rune_id)), [])
        return sum((check_runes(ids, opt) for ids, opt in [
            (master_weapon_runes_id, self.wep_runes),
            (master_armour_runes_id, self.arm_runes),
            (master_talisman_runes_id, self.tal_runes),
        ]), [])


class Runesmith(Unit):
    name = 'Runesmith'
    gear = ['Hand Weapon', 'Gromril armour']
    base_points = 70

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
        ], limit=1)
        self.sh = self.opt_options_list('', [
            ['Shield', 2, 'sh'],
        ])
        self.wep_runes = self.opt_options_list('Weapon runes', filter_items(weapon_runes, 75), limit=3)
        self.arm_runes = self.opt_options_list('Armour runes', filter_items(armour_runes, 75), limit=3)
        self.tal_runes = self.opt_options_list('Talisman runes', filter_items(runesmith_talisman_runes, 75), limit=3)
        self.magic = [self.wep_runes, self.arm_runes, self.tal_runes]

    def check_rules(self):
        if sum((1 for rune_id in list(master_weapon_runes_id.keys()) if self.wep_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on weapon')
        if sum((1 for rune_id in list(master_armour_runes_id.keys()) if self.arm_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on armour')
        if sum((1 for rune_id in list(runesmith_master_talisman_runes_id.keys()) if self.tal_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on talisman')
        process_base_rune_list(self.wep_runes, base_weapon_runes_id)
        process_base_rune_list(self.arm_runes, base_armour_runes_id)
        process_base_rune_list(self.tal_runes, base_talisman_runes_id)
        norm_point_limits(75, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        def check_runes(ids, opt):
            return sum(([ids[rune_id]] for rune_id in list(ids.keys()) if opt.get(rune_id)), [])
        return sum((check_runes(ids, opt) for ids, opt in [
            (master_weapon_runes_id, self.wep_runes),
            (master_armour_runes_id, self.arm_runes),
            (master_talisman_runes_id, self.tal_runes),
        ]), [])


class DragonSlayer(Unit):
    name = 'Dragon Slayer'
    gear = ['Slayer axes']
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.wep_runes = self.opt_options_list('Weapon runes', filter_items(weapon_runes, 75), limit=3, points_limit=75)

    def check_rules(self):
        if sum((1 for rune_id in list(master_weapon_runes_id.keys()) if self.wep_runes.get(rune_id))) > 1:
            self.error('No more than one master rune can be inscribed on weapon')
        process_base_rune_list(self.wep_runes, base_weapon_runes_id)
        Unit.check_rules(self)

    def get_unique_gear(self):
        def check_runes(ids, opt):
            return sum(([ids[rune_id]] for rune_id in list(ids.keys()) if opt.get(rune_id)), [])
        return sum((check_runes(ids, opt) for ids, opt in [
            (master_weapon_runes_id, self.wep_runes),
        ]), [])
