from builder.games.whfb.dwarfs.special import Miners

__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.dwarfs.heroes import *
from builder.games.whfb.dwarfs.lords import *
from builder.games.whfb.dwarfs.core import *
from builder.games.whfb.dwarfs.special import *
from builder.games.whfb.dwarfs.rare import *


class Dwarfs(LegacyFantasy):
    army_name = 'Dwarfs'
    army_id = 'd9f8be799db14815860e7811e0cc5466'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Thorgrim, Thorek, Lord, Runelord, DaemonSlayer],
            heroes=[Bugman, Thane, Runesmith, MasterEngineer, DragonSlayer],
            core=[Warriors, Longbeards, Crossbowmen, Thunderers],
            special=[Hammerers, Cannon, BoltThrower, Miners, Ironbreakers, StoneThrower, Slayers],
            rare=[OrganGun, FlameCannon, Gyrocopter]
        )

    def check_special_limit(self, name, group, limit, mul):
        if name == BoltThrower.name:
            limit *= 2
        return LegacyFantasy.check_special_limit(self, name, group, limit, mul)

    def check_rules(self):
        LegacyFantasy.check_rules(self)
        if self.core.count_unit(Warriors) < self.core.count_unit(Longbeards):
            self.error("You can\'t has more Longbeards then Warriors")

        anvil = sum((1 for squad in self.lords.get_units([Runelord]) if squad.has_anvil()))
        if anvil > 1:
            self.error("You can take only one Anvil of Doom in army (taken: {})".format(anvil))

        rangers = sum((1 for squad in self.core.get_units([Warriors, Longbeards, Crossbowmen]) if squad.is_rangers()))
        if rangers > 1:
            self.error("You can take only one unit of Rangers in army (taken: {})".format(rangers))
