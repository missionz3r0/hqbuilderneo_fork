from builder.games.whfb.roster import BSB, Character, Lore
from .armory import *
from builder.core2 import *

__author__ = 'Denis Romanov'


class GladeCaptain(Character):
    type_name = 'Glade Captain'
    type_id = 'gladecaptain_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(GladeCaptain.Weapon, self).__init__(parent, name='Weapon', limit=1)
            self.spear = self.variant('Asrai spear', 2)
            self.hw = self.variant('Additional hand weapon', 2, gear=Gear('Hand weapon'))
            self.gw = self.variant('Great weapon', 4)

        def check_rules(self):
            has_mount = self.parent.mount.has_mount()
            if has_mount and self.hw.value:
                self.hw.value = False
            self.process_limit([self.spear, self.hw, self.gw], 1)
            if has_mount:
                self.hw.active = self.hw.used = False

    class Armour(OptionsList):
        def __init__(self, parent):
            super(GladeCaptain.Armour, self).__init__(parent, name='')
            self.shield = self.variant('Shield', 2)

        def check_rules(self):
            super(GladeCaptain.Armour, self).check_rules()
            self.shield.used = self.shield.active = not self.parent.magic_armour.has_shield()

    class Mount(OptionsList):
        def __init__(self, parent):
            super(GladeCaptain.Mount, self).__init__(parent, 'Mount', limit=1)
            for i in [
                ('Elven Steed', 10),
                ('Great Eagle', 50),
                ('Great Stag', 65),
            ]:
                self.variant(i[0], i[1], gear=[UnitDescription(i[0], points=i[1])])

        def has_mount(self):
            return self.any

    def __init__(self, parent):
        super(GladeCaptain, self).__init__(
            parent, points=75,
            gear=[Gear('Hand weapon'), Gear('Asrai longbow')],
            magic_limit=50
        )
        self.Weapon(self)
        Arrows(self, per_model=False)
        self.Armour(self)
        self.mount = self.Mount(self)

        self.bsb = BSB(self, Standards)

        self.magic_weapon = Weapon(self, limit=50)
        self.magic_armour = Armour(self, limit=50)
        self.magic = [
            self.magic_weapon,
            self.magic_armour,
            Enchanted(self, limit=50),
            Talismans(self, limit=50),
        ]

    def build_description(self):
        desc = super(GladeCaptain, self).build_description()
        if not self.magic_armour.has_suit():
            desc.add(Gear('Light armour'))
        return desc


class SpellSigner(Character):
    type_name = 'Spellsigner'
    type_id = 'spellsigner_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(SpellSigner.Weapon, self).__init__(parent, name='Weapon')
            self.spear = self.variant('Asrai longbow', 5)

    class Mount(OptionsList):
        def __init__(self, parent):
            super(SpellSigner.Mount, self).__init__(parent, 'Mount', limit=1)
            for i in [
                ('Elven Steed', 10),
                ('Great Eagle', 50),
                ('Unicorn', 60),
            ]:
                self.variant(i[0], i[1], gear=[UnitDescription(i[0], points=i[1])])

    class MagicLevel(OneOf):
        def __init__(self, parent):
            super(SpellSigner.MagicLevel, self).__init__(parent, 'Wizard')
            self.variant('Level 1 Wizard')
            self.variant('Level 2 Wizard', 35)

    def __init__(self, parent):
        super(SpellSigner, self).__init__(parent, points=80, gear=[Gear('Hand weapon')], magic_limit=50)
        self.Weapon(self)
        self.Mount(self)
        self.MagicLevel(self)
        Lore(self)

        self.magic = [
            Weapon(self, limit=50),
            Enchanted(self, limit=50),
            Arcane(self, limit=50),
            Talismans(self, limit=50),
        ]


class ShadowDancer(Character):
    type_name = 'Shadowdancer'
    type_id = 'shadowdancer_v1'

    class MagicLevel(OptionsList):
        def __init__(self, parent):
            super(ShadowDancer.MagicLevel, self).__init__(parent, 'Wizard')
            self.variant('Level 1 Wizard', 60, gear=[Gear('Level 1 Wizard'), Gear('Lore of Shadow')])

    def __init__(self, parent):
        super(ShadowDancer, self).__init__(parent, points=100, gear=[Gear('Hand weapon', count=2)], magic_limit=25)
        self.wizard = self.MagicLevel(self)

        self.weapon = Weapon(self, limit=25)
        self.armour = Armour(self, limit=25, suits=False, shields=False)
        self.arcane = Arcane(self, limit=25)
        self.talismans = Talismans(self, limit=25)
        self.enchanted = Enchanted(self, limit=25)

    def check_rules(self):
        is_wizard = self.wizard.any
        if is_wizard:
            self.magic = [self.weapon, self.arcane, self.talismans, self.enchanted]
        else:
            self.magic = [self.weapon, self.armour, self.talismans, self.enchanted]
        self.armour.used = self.armour.visible = not is_wizard
        self.arcane.used = self.arcane.visible = is_wizard
        super(ShadowDancer, self).check_rules()


class Waystalker(Character):
    type_name = 'Waystalker'
    type_id = 'waystalker_v1'

    def __init__(self, parent):
        super(Waystalker, self).__init__(
            parent, points=90,
            gear=[Gear('Hand weapon', count=2), Gear('Asrai longbow')],
            magic_limit=25
        )
        self.weapon = Weapon(self, limit=25)
        self.armour = Armour(self, limit=25, shields=False)
        self.talismans = Talismans(self, limit=25)
        self.enchanted = Enchanted(self, limit=25)
        self.magic = [self.weapon, self.armour, self.talismans, self.enchanted]


class BranchWraith(Unit):
    type_name = 'Branchwraith'
    type_id = 'branchwraith_v1'

    def __init__(self, parent):
        super(BranchWraith, self).__init__(parent, points=75, unique=True, gear=[
            Gear('Hand weapon'), Gear('Level 1 Wizard'), Gear('Lore of Life')
        ])


class Drycha(Unit):
    type_name = 'Drycha'
    type_id = 'drycha_v1'

    def __init__(self, parent):
        super(Drycha, self).__init__(parent, points=255, unique=True, gear=[
            Gear('Hand weapon'), Gear('Level 2 Wizard'), Gear('Lore of Shadow')
        ])


class NaestraArahan(Unit):
    type_name = 'Naestra & Arahan'
    type_id = 'naestraarahan_v1'

    class Mount(OneOf):
        def __init__(self, parent):
            super(NaestraArahan.Mount, self).__init__(parent, 'Mount')
            for i in [
                ('Gwindalor (Great Eagle)', 0),
                ('Ceithin-Har (Forest Dragon)', 220),
            ]:
                self.variant(i[0], i[1], gear=[UnitDescription(i[0], points=i[1])])

    def __init__(self, parent):
        gear = [Gear('Hand weapon'), Gear('Light armour'), Gear('Asrai spear')]
        super(NaestraArahan, self).__init__(parent, points=275, unique=True, gear=[
            UnitDescription('Naestra', options=gear + [Gear('Talon of Dawn')]),
            UnitDescription('Arahan', options=gear + [Gear('Talon of Dusk')]),
        ])
        self.Mount(self)
