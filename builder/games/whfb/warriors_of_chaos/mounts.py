__author__ = 'Denis Romanov'
from builder.core.unit import Unit


class ChaosSteed(Unit):
    name = 'Chaos Steed'
    base_points = 24
    gear = ['Barding']
    static = True

    def __init__(self, points=24):
        self.base_points = points
        Unit.__init__(self)


class SteedOfSlaanesh(Unit):
    name = 'Steed of Slaanesh'
    base_points = 25
    static = True


class DiskOfTzeentch(Unit):
    name = 'Disk of Tzeentch'
    base_points = 30
    static = True


class PalanquinOfNurgle(Unit):
    name = 'Palanquin of Nurgle'
    base_points = 40
    static = True


class JuggernautOfKhorne(Unit):
    name = 'Juggernaut of Khorne'
    base_points = 55
    static = True


class DemonicMount(Unit):
    name = 'Demonic Mount'
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [['Barding', 15]])


class Manticore(Unit):
    name = 'Manticore'
    base_points = 150

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Iron Hard Skin', 25],
            ['Venom Tail', 10],
        ])


class Dragon(Unit):
    name = 'Chaos Dragon'
    base_points = 330
    static = True
