__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.warriors_of_chaos.heroes import *
from builder.games.whfb.warriors_of_chaos.lords import *
from builder.games.whfb.warriors_of_chaos.core import *
from builder.games.whfb.warriors_of_chaos.special import *
from builder.games.whfb.warriors_of_chaos.rare import *


class WarriorsOfChaos(LegacyFantasy):
    army_name = 'Warriors of Chaos'
    army_id = 'cfc4501f8f7c4421a2af3ce03f1e8c0c'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Archaon, Galrauch, Kholek, Sigvald, Valkia, Vilitch, ChaosLord, ChaosSorcererLord, DaemonPrince],
            heroes=[Wulfric, Throgg, Festus, Scyla, ExaltedHero, ChaosSorcerer],
            core=[Warriors, Marauders, Forsaken, Warhound, ChaosChariot, MaraudersHorsemen],
            special=[Hellstriders, Chosen, Knights, Ogres, DragonOgres, Trolls, Chimera, GorebeastChariot, Warshine],
            rare=[Hellcannon, Shaggoth, Giant, Spawn, Skullcrushers, Slaughterbrute, Mutalith]
        )

    def has_archaon(self):
        return self.lords.count_unit(Archaon) > 0

    def check_rules(self):
        LegacyFantasy.check_rules(self)
        soc = sum((1 for ck in self.special.get_units([Knights]) if ck.is_soc))
        if soc > 1:
            self.error('Only one units of Chaos Knight may be Swords of Chaos')