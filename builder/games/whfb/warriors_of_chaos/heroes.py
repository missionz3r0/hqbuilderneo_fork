__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.warriors_of_chaos.mounts import *
from builder.games.whfb.warriors_of_chaos.armory import *
from builder.core.options import norm_point_limits
from builder.games.whfb.warriors_of_chaos.special import GorebeastChariot, Warshine
from builder.games.whfb.warriors_of_chaos.core import ChaosChariot


heroes_weapon = filter_items(woc_weapon, 50)
heroes_armour = filter_items(woc_armour, 50)
heroes_talisman = filter_items(woc_talisman, 50)
heroes_arcane = filter_items(woc_arcane, 50)
heroes_enchanted = filter_items(woc_enchanted, 50)
heroes_rewards = filter_items(rewards, 25)


class Wulfric(StaticUnit):
    name = 'Wulfric the Wanderer'
    base_points = 180
    gear = ['Hand weapon', 'Chaos armour', 'Shield']


class Throgg(StaticUnit):
    name = 'Throgg'
    base_points = 195
    gear = ['Great weapon', 'The Wintertooth Crown']


class Festus(StaticUnit):
    name = 'Festus the Leechlord'
    base_points = 190
    gear = ['Level 2 Wizard', 'Lore of Nurgle', 'Hand weapon', 'Prestilent Poisons']


class Scyla(StaticUnit):
    name = 'Scyla Anfingrim'
    base_points = 105
    gear = ['Mark of Khorne', 'Brass Collar of Khorne']


class ExaltedHero(Unit):
    name = 'Exalted Hero'
    gear = ['Hand weapon']
    base_points = 110

    def __init__(self):
        Unit.__init__(self)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 10, 'kh'],
            ['Mark of Tzeentch', 10, 'tz'],
            ['Mark of Nurgle', 10, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ], limit=1)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 3, 'hw'],
            ['Flail', 5, 'fl'],
            ['Great weapon', 6, 'gw'],
            ['Halberd', 6, 'hl'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
        ])
        self.rewards = self.opt_options_list('Rewards', heroes_rewards, points_limit=25)
        self.steed = self.opt_optional_sub_unit('Steed', [ChaosSteed(), SteedOfSlaanesh(), DiskOfTzeentch(),
                                                          DemonicMount(), PalanquinOfNurgle(), JuggernautOfKhorne(),
                                                          ChaosChariot(boss=self), GorebeastChariot(boss=self),
                                                          Warshine(boss=self)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', woc_standard, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    @property
    def mark(self):
        if not self.marks.is_any_selected():
            return
        return self.marks.get_all()[0]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(woc_shields_ids))
        self.gear = ['Hand weapon'] + (['Chaos armour'] if not self.magic_arm.is_selected(woc_armour_suits_ids) else [])

        self.magic_wep.set_active_options(['woc_fm'], self.marks.get('ng'))
        self.magic_enc.set_active_options(['woc_sl'], self.marks.get('sl'))
        self.rewards.set_active_options(['all'], self.marks.get('sl'))
        self.rewards.set_active_options(['eye'], self.marks.get('tz'))
        self.rewards.set_active_options(['rot'], self.marks.get('ng'))
        self.rewards.set_visible_options(['cf'], False)  # Arcane item - mage only

        self.steed.set_active_options([SteedOfSlaanesh.name], self.marks.get('sl'))
        self.steed.set_active_options([DiskOfTzeentch.name], self.marks.get('tz'))
        self.steed.set_active_options([PalanquinOfNurgle.name], self.marks.get('nl'))
        self.steed.set_active_options([JuggernautOfKhorne.name], self.marks.get('kh'))

        self.wep.set_active_options(['hw'], self.steed.get_count() == 0)

        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        self.magban.set_visible_options(['woc_blas'], self.marks.get('tz'))
        self.magban.set_visible_options(['woc_rage'], self.marks.get('kh'))

        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban]
                    if m.is_used()), []) + self.rewards.get_selected()


class ChaosSorcerer(Unit):
    name = 'Chaos Sorcerer'
    gear = ['Hand weapon', 'Chaos armour']
    base_points = 110

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Tzeentch', 15, 'tz'],
            ['Mark of Nurgle', 10, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ], limit=1)
        self.rewards = self.opt_options_list('Rewards', heroes_rewards, points_limit=25)
        self.steed = self.opt_optional_sub_unit('Steed', [ChaosSteed(), SteedOfSlaanesh(), DiskOfTzeentch(),
                                                          DemonicMount(), PalanquinOfNurgle(),
                                                          ChaosChariot(boss=self), GorebeastChariot(boss=self),
                                                          Warshine(boss=self)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', woc_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.magic = [self.magic_wep, self.magic_tal, self.magic_enc, self.magic_arc, self.magic_arm]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
            ['The Lore of Tzeentch', 0, 'tzeentch'],
            ['The Lore of Nurgle', 0, 'nurgle'],
            ['The Lore of Slaanesh', 0, 'slaanesh'],
        ], limit=1)

    @property
    def mark(self):
        if not self.marks.is_any_selected():
            return
        return self.marks.get_all()[0]

    def check_rules(self):
        self.magic_wep.set_active_options(['woc_fm'], self.marks.get('ng'))
        self.magic_enc.set_active_options(['woc_sl'], self.marks.get('sl'))
        self.rewards.set_active_options(['all'], self.marks.get('sl'))
        self.rewards.set_active_options(['eye'], self.marks.get('tz'))
        self.rewards.set_active_options(['rot'], self.marks.get('ng'))
        self.rewards.set_active_options(['cf'], not self.magic_arc.is_any_selected())
        self.magic_arc.set_active(not self.rewards.get('cf'))

        self.steed.set_active_options([SteedOfSlaanesh.name], self.marks.get('sl'))
        self.steed.set_active_options([DiskOfTzeentch.name], self.marks.get('tz'))
        self.steed.set_active_options([PalanquinOfNurgle.name], self.marks.get('nl'))

        self.gear = ['Hand weapon'] + (['Chaos armour'] if not self.magic_arm.is_selected(woc_armour_suits_ids) else [])

        self.lores.set_active_options(['fire'], not self.marks.is_any_selected())
        self.lores.set_active_options(['metal'], not self.marks.is_any_selected() or self.marks.get('tz'))
        self.lores.set_active_options(['shadow'], not self.marks.is_any_selected() or self.marks.get('sl'))
        self.lores.set_active_options(['death'], not self.marks.is_any_selected() or self.marks.get('ng'))
        self.lores.set_active_options(['tzeentch'], self.marks.get('tz'))
        self.lores.set_active_options(['nurgle'], self.marks.get('ng'))
        self.lores.set_active_options(['slaanesh'], self.marks.get('sl'))

        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), []) + self.rewards.get_selected()
