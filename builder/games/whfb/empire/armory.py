__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *

empire_weapon = [
    ['Runefang', 85, 'empire_fang'],
    ['Mace of Helsturm', 50, 'empire_hel'],
] + magic_weapons

empire_armour = [
    ['Armour of Meteoric Iron', 50, 'empire_iron'],
    ['Helm of the Skavenslayer', 15, 'empire_skaven'],
] + magic_armours

empire_armour_suits_ids = magic_armour_suits_ids + ['empire_iron']
empire_shields_ids = magic_shields_ids

empire_talisman = [
    ['The White Cloak of Ulric', 50, 'empire_ulric']
] + talismans

empire_enchanted = [
    ['Van Horstmanm\'s Speculum', 40, 'empire_van'],
    ['Ring of Volans', 30, 'empire_volans'],
] + enchanted_items

empire_arcane = arcane_items

empire_standard = [
    ['Griffon Banner', 60, 'empire_griffon'],
    ['Steel Standard', 35, 'empire_steel']
] + magic_standards
