__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.empire.mounts import *
from builder.games.whfb.empire.armory import *
from builder.games.whfb.empire.rare import Hurricanum, Luminark
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class KarlFranz(Unit):
    name = 'Karl Franz, The Emperor'
    base_points = 340
    gear = ['Full plate armour', 'The Silver Seal']

    class Deathclaw(StaticUnit):
        name = 'Deathclaw'
        base_points = 215

    class Dragon(StaticUnit):
        name = 'The Imperial Dragon'
        base_points = 300

    def __init__(self):
        Unit.__init__(self)
        self.arm = self.opt_one_of('Weapon', [
            ['The Reikland Runefang', 0, 'rr'],
            ['Ghal Maraz', 30, 'gm']
        ])

        self.steed = self.opt_optional_sub_unit('Steed', [self.Deathclaw(), self.Dragon(), Pegasus(), Warhorse()])


class KurtHelborg(StaticUnit):
    name = 'Kurt Helborg, Reiksmarshal of the Empire'
    base_points = 320
    gear = ['Full plate armour', 'The Solland Runefang', 'Laurels of Victory']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Krieglust', count=1, gear=['Barding']).build())


class MariusLeitdorf(StaticUnit):
    name = 'Marius Leitdorf'
    base_points = 220
    gear = ['Full plate armour', 'The Averland Runefang', 'Hand weapon']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Daisy', count=1, gear=['Barding']).build())


class Balthasar(Unit):
    name = 'Balthasar Gelt, The Supreme Patriarch'
    base_points = 360 - Pegasus.base_points
    gear = ['Hand weapon', 'Amulet of Sea Gold', 'Cloak of Molten Metal', 'Staff of Volans', 'Level 4 Wizard']

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_sub_unit(Pegasus())


class Volkmar(Unit):
    name = 'Volkmar the Grim, The Grand Theogonist'
    base_points = 190
    gear = ['Light Armour', 'Jade of Griffon', 'Staff of Commnand']

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [Altar()])

    def get_unique_gear(self):
        return [Altar.name] if self.steed.get(Altar.name) else []


class General(Unit):
    name = 'General of the Empire'
    gear = ['Hand Weapon']
    base_points = 95

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Hand weapon', 3, 'hw'],
            ['Lance', 7, 'ln'],
            ['Handgun', 6, 'hg'],
            ['Long Bow', 5, 'lb'],
            ['Pistol', 5, 'ps'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
        ])
        self.arm = self.opt_one_of('', [
            ['Light armour', 0, 'la'],
            ['Heavy armour', 4, 'ha'],
            ['Full plate armour', 9, 'da'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Warhorse(), ImperialGriffon(), Pegasus()])
        self.magic_wep = self.opt_options_list('Magic weapon', empire_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', empire_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', empire_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', empire_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(empire_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(empire_armour_suits_ids))
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['ln'], self.steed.get_count() > 0)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Lector(Unit):
    name = 'Arch Lector'
    gear = ['Hand Weapon']
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 5, 'gw'],
            ['Hand weapon', 2, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
        ])
        self.arm = self.opt_one_of('', [
            ['Light armour', 0, 'la'],
            ['Heavy armour', 4, 'ha'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Warhorse(), Altar()])
        self.magic_wep = self.opt_options_list('Magic weapon', empire_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', empire_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', empire_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', empire_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(empire_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(empire_armour_suits_ids))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), []) + ([Altar.name] if self.steed.get(Altar.name) else [])


class Master(Unit):
    name = 'Grand Master'
    base_gear = ['Hand Weapon']
    base_points = 155

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Lance', 7, 'ln'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 4, 'sh'],
        ])
        self.magic_wep = self.opt_options_list('Magic weapon', empire_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', empire_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', empire_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', empire_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(empire_shields_ids))
        self.gear = self.base_gear
        if not self.magic_arm.is_selected(empire_armour_suits_ids):
            self.gear = self.gear + ['Full Plate armour']
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Warhorse', count=1, gear=['Barding']).build())

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class BattleWizard(Unit):
    name = 'Battle Wizard Lord'
    gear = ['Hand Weapon']
    base_points = 165

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Light', 0, 'light'],
            ['The Lore of Life', 0, 'life'],
            ['The Lore of Heavens', 0, 'heavens'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [Pegasus(), Warhorse(), Hurricanum(boss=True),
                                                          Luminark(boss=True), ImperialGriffon()])
        self.magic_wep = self.opt_options_list('Magic weapon', empire_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', empire_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', empire_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', empire_arcane, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.steed.set_visible_options([Hurricanum.name], self.lores.get('heavens'))
        self.steed.set_visible_options([Luminark.name], self.lores.get('light'))
        self.steed.set_visible_options([ImperialGriffon.name], self.lores.get('beast'))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


#
#
# class Anointed(Unit):
#     name = 'Anointed of Asuryan'
#     base_gear = ['Halberd']
#     base_points = 210
#
#     def __init__(self):
#         Unit.__init__(self)
#         self.steed = self.opt_optional_sub_unit('Steed', [FlamePhoenix(), FrostPhoenix()])
#         self.magic_wep = self.opt_options_list('Magic weapon', he_weapon, 1)
#         self.magic_arm = self.opt_options_list('Magic armour', he_armour, 1)
#         self.magic_tal = self.opt_options_list('Talisman', he_talisman, 1)
#         self.magic_enc = self.opt_options_list('Enchanted item', he_enchanted, 1)
#         self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
#
#     def check_rules(self):
#         self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(he_armour_suits_ids) else [])
#         self.magic_wep.set_visible_options(['he_star'], self.steed.get_count() > 0)
#         self.magic_enc.set_visible_options(['he_mor'], self.steed.get_count() == 0)
#         norm_point_limits(100, self.magic)
#         Unit.check_rules(self)
#
#     def get_unique_gear(self):
#         return sum((m.get_selected() for m in self.magic), [])
#
#
# class HoethLoremaster(Unit):
#     name = 'Loremaster of Hoeth'
#     base_gear = ['Level 2 Wizard', 'Great weapon']
#     base_points = 230
#
#     def __init__(self):
#         Unit.__init__(self)
#         self.magic_wep = self.opt_options_list('Magic weapon', he_weapon, 1)
#         self.magic_arm = self.opt_options_list('Magic armour', he_armour, 1)
#         self.magic_tal = self.opt_options_list('Talisman', he_talisman, 1)
#         self.magic_enc = self.opt_options_list('Enchanted item', he_enchanted, 1)
#         self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
#
#     def check_rules(self):
#         self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(he_armour_suits_ids) else [])
#         self.magic_wep.set_visible_options(['he_star'], False)
#         norm_point_limits(100, self.magic)
#         Unit.check_rules(self)
#
#     def get_unique_gear(self):
#         return sum((m.get_selected() for m in self.magic), [])
#
#
