from builder.core2 import *


__author__ = 'dante'


class FilteredList(OptionsList):
    def __init__(self, *args, **kwargs):
        self.filter_limit = kwargs.pop('filter_limit', None)
        super(FilteredList, self).__init__(*args, **kwargs)

    def variant(self, name, points=None, *args, **kwargs):
        if self.filter_limit is not None and points > self.filter_limit:
            return
        return super(FilteredList, self).variant(name, points, *args, **kwargs)


class BaseWeapon(FilteredList):
    def add_base(self):
        return [
            self.variant('Giant Blade', 60),
            self.variant('Sword of Bloodshed', 60),
            self.variant('Obsidian Blade', 50),
            self.variant('Ogre blade', 40),
            self.variant('Sword of Strife', 40),
            self.variant('Fencer\'s blades', 35),
            self.variant('Sword of Anti-Heroes', 30),
            self.variant('Spellthieving sword', 25),
            self.variant('Sword of Swift Slaying', 25),
            self.variant('Sword of Battle', 20),
            self.variant('Berserker sword', 20),
            self.variant('Sword of Might', 20),
            self.variant('Gold Sigil sword', 15),
            self.variant('Sword of Striking', 15),
            self.variant('Biting Blade', 10),
            self.variant('Relic sword', 10),
            self.variant('Shrieking Blade', 10),
            self.variant('Tormentor sword', 5),
            self.variant('Warrior Bane', 5),
        ]


class BaseArmour(FilteredList):
    def __init__(self, *args, **kwargs):
        super(BaseArmour, self).__init__(*args, **kwargs)
        self.shields = []
        self.suits = []

    def add_base(self, suits=True, shields=True):
        self.suits.extend([
            self.variant('Armour of Destiny', 50),
            self.variant('Armour of Silvered Steel', 45),
            self.variant('Armour of Fortune', 35),
            self.variant('Glittering Scales', 25),
            self.variant('Gambler\'s Armour', 20),
        ] if suits else [])
        self.shields.extend([
            self.variant('Spellshield', 20),
            self.variant('Enchanted shield', 5),
            self.variant('Charmed shield', 5),
            self.variant('Shield of Ptolos', 25)
        ] if shields else [])
        other = [
            self.variant('Trickster\'s Helm', 50),
            self.variant('Helm of Discord', 30),
            self.variant('Dragonhelm', 10),
        ]
        return self.suits + self.shields + other

    def has_suit(self):
        return any(o.used and o.value for o in self.suits)

    def has_shield(self):
        return any(o.used and o.value for o in self.shields)


class BaseArcane(FilteredList):
    def add_base(self):
        return [
            self.variant('Book of Ashur', 70),
            self.variant('Feedback scroll', 50),
            self.variant('Scroll of Leeching', 50),
            self.variant('Sivejir\'s Hex scroll', 50),
            self.variant('Power scroll', 35),
            self.variant('Wand of Jet', 35),
            self.variant('Forbidden rod', 35),
            self.variant('Trickster\'s shard', 25),
            self.variant('Earthing Rod', 25),
            self.variant('Dispel scroll', 25),
            self.variant('Power stone', 20),
            self.variant('Sceptre of stability', 15),
            self.variant('Channeling staff', 15),
            self.variant('Scroll of Shielding', 15),
        ]


class BaseEnchanted(FilteredList):
    def add_base(self):
        return [
            self.variant('Wizarding Hat', 100),
            self.variant('Fozzrik\'s Folding Fortress', 100),
            self.variant('Arabyan carpet', 50),
            self.variant('Crown of Command', 35),
            self.variant('Healing potion', 35),
            self.variant('Featherfoe torc', 35),
            self.variant('Ruby Ring of Ruin', 25),
            self.variant('The Terrifying Mask of EEE!', 25),
            self.variant('Potion of Strength', 20),
            self.variant('Potion of toughness', 20),
            self.variant('The other Trickster\'s Shard', 15),
            self.variant('Ironcurse icon', 5),
            self.variant('Potion of Foolhardiness', 5),
            self.variant('Potion of Speed', 5),
        ]


class BaseTalismans(FilteredList):
    def add_base(self):
        return [
            self.variant('Talisman of Preservations', 45),
            self.variant('Obsidian Lodestone', 45),
            self.variant('Talisman of Endurance', 30),
            self.variant('Obsidian Amulet', 30),
            self.variant('Dawnstone', 25),
            self.variant('Opal Amulet', 15),
            self.variant('Obsidian Trinket', 15),
            self.variant('Talisman of Protection', 15),
            self.variant('Seed of Rebirth', 10),
            self.variant('Dragonbane Gem', 5),
            self.variant('Pigeon Plucker pendant', 5),
            self.variant('Luckstone', 5),
        ]


class BaseStandards(FilteredList):
    def add_base(self):
        return [
            self.variant('Rampager\'s Standard', 55),
            self.variant('Wailing Banner', 50),
            self.variant('Ranger\'s Standard', 50),
            self.variant('Razor Standard', 45),
            self.variant('War Banner', 35),
            self.variant('Banner of Swiftness', 15),
            self.variant('Lichebone Pennant', 15),
            self.variant('Standard of Discipline', 15),
            self.variant('Banner of Eternal Flame', 10),
            self.variant('Gleaming Pennant', 5),
            self.variant('Scarecrow Banner', 5),
        ]
