__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.skaven.armory import *
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Thanquol(Unit):
    name = 'Thanquol & Boneripper'
    base_points = 450

    def get_unique(self):
        return self.name

    def __init__(self):
        Unit.__init__(self)
        self.lores = self.opt_options_list('Lores', [
            ['Skaven Spells of Ruin', 0, 'ruin'],
            ['Skaven Spells of Plague', 0, 'plague'],
        ], limit=1)

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Thanquol', gear=['Level 4 Wizard', 'Sword', 'Warp-amulet',
                                                                    'Staff of Horned One', 'D6 + 2 Warpsone Tokens'],
                                             count=1).add_gear_opt(self.lores).build())
        self.description.add(ModelDescriptor(name='Boneripper', gear=['Talons and teeth', 'Warpfire Thrower'],
                                             count=1).build())


class Skrolk(StaticUnit):
    name = 'Lord Skrolk'
    base_points = 470
    gear = ['Level 3 Wizard', 'Skaven Spells of Plague', 'Rod of Corruption', 'The Liber Bubonicus']


class Ikik(StaticUnit):
    name = 'Ikik Claw'
    base_points = 395
    gear = ['Level 3 Wizard', 'Skaven Spells of Ruin', 'Storm Daemon', 'Warplock Pistol', 'Iron Frame']


class Throt(StaticUnit):
    name = 'Throt the Unclean'
    base_points = 225
    gear = ['Light armour', 'Creature-killer', 'Whip of Domination']


class Queek(StaticUnit):
    name = 'Queek Headtaker'
    base_points = 215
    gear = ['Dwarf Gouger', 'Hand weapon', 'Warp-shard armour']


class Warlord(Unit):
    name = 'Warlord'
    base_gear = ['Hand Weapon']
    base_points = 90

    class Bonebreaker(Unit):
        static = True
        name = 'Rat Ogre Bonebreaker'
        base_points = 65

    class Warlitter(Unit):
        static = True
        name = 'War-litter'
        base_points = 35

    class Rat(Unit):
        static = True
        name = 'Great Pox Rat'
        base_points = 30

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 6, 'gw'],
            ['Halberd', 3, 'hb'],
            ['Hand weapon', 3, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [self.Bonebreaker(), self.Warlitter(), self.Rat()])
        self.magic_wep = self.opt_options_list('Magic weapon', skaven_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', skaven_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', skaven_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', skaven_enchanted, 1)
        self.pile = self.opt_options_list('Scavenge-pile', skaven_pile)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(skaven_shields_ids))
        self.gear = self.base_gear + ([] if self.magic_arm.is_selected(skaven_armour_suits_ids) else ['Heavy armour'])
        norm_point_limits(100, self.magic + [self.pile])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class GreySeer(Unit):
    name = 'Grey Seer'
    gear = ['Level 4 Wizard', 'D3 Warpstone Tokens', 'Hand Weapon']
    base_points = 240

    class Bell(Unit):
        name = 'The Screaming Bell'
        base_points = 200
        static = True

        def check_rules(self):
            self.set_points(self.build_points())
            self.build_description(options=[])
            self.description.add(ModelDescriptor(name='Rat Ogre Crew', count=1).build())

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [self.Bell()])
        self.magic_wep = self.opt_options_list('Magic weapon', skaven_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', skaven_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', skaven_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', skaven_arcane, 1)
        self.stones = self.opt_count('Warpstone Token', 0, 1, 15)
        self.pile = self.opt_options_list('Scavenge-pile', skaven_pile)
        self.lores = self.opt_options_list('Lores', [
            ['Skaven Spells of Ruin', 0, 'ruin'],
            ['Skaven Spells of Plague', 0, 'plague'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        total = norm_point_limits(100 - self.stones.points(), self.magic + [self.pile])
        self.stones.update_range(0, int((100 - total)/15))
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class VerminLord(Unit):
    name = 'Vermin Lord'
    base_points = 500
    gear = ['Level 4 Wizard', 'Doom Glave']

    def __init__(self):
        Unit.__init__(self)
        self.lores = self.opt_options_list('Lores', [
            ['Skaven Spells of Ruin', 0, 'ruin'],
            ['Skaven Spells of Plague', 0, 'plague'],
        ], limit=1)

#
#
# class Anointed(Unit):
#     name = 'Anointed of Asuryan'
#     base_gear = ['Halberd']
#     base_points = 210
#
#     def __init__(self):
#         Unit.__init__(self)
#         self.steed = self.opt_optional_sub_unit('Steed', [FlamePhoenix(), FrostPhoenix()])
#         self.magic_wep = self.opt_options_list('Magic weapon', skaven_weapon, 1)
#         self.magic_arm = self.opt_options_list('Magic armour', skaven_armour, 1)
#         self.magic_tal = self.opt_options_list('Talisman', skaven_talisman, 1)
#         self.magic_enc = self.opt_options_list('Enchanted item', skaven_enchanted, 1)
#         self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
#
#     def check_rules(self):
#         self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(skaven_armour_suits_ids) else [])
#         self.magic_wep.set_visible_options(['skaven_star'], self.steed.get_count() > 0)
#         self.magic_enc.set_visible_options(['skaven_mor'], self.steed.get_count() == 0)
#         norm_point_limits(100, self.magic)
#         Unit.check_rules(self)
#
#     def get_unique_gear(self):
#         return sum((m.get_selected() for m in self.magic), [])
#
#
# class HoethLoremaster(Unit):
#     name = 'Loremaster of Hoeth'
#     base_gear = ['Level 2 Wizard', 'Great weapon']
#     base_points = 230
#
#     def __init__(self):
#         Unit.__init__(self)
#         self.magic_wep = self.opt_options_list('Magic weapon', skaven_weapon, 1)
#         self.magic_arm = self.opt_options_list('Magic armour', skaven_armour, 1)
#         self.magic_tal = self.opt_options_list('Talisman', skaven_talisman, 1)
#         self.magic_enc = self.opt_options_list('Enchanted item', skaven_enchanted, 1)
#         self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
#
#     def check_rules(self):
#         self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(skaven_armour_suits_ids) else [])
#         self.magic_wep.set_visible_options(['skaven_star'], False)
#         norm_point_limits(100, self.magic)
#         Unit.check_rules(self)
#
#     def get_unique_gear(self):
#         return sum((m.get_selected() for m in self.magic), [])


