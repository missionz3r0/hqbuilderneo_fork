__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.skaven.lords import Warlord
from builder.games.whfb.skaven.armory import *
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Assassin(Unit):
    name = 'Assassin'
    gear = ['Hand Weapon' for _ in range(2)] + ['Throwing stars']
    base_points = 120

    def __init__(self):
        Unit.__init__(self)
        self.magic_wep = self.opt_options_list('Magic weapon', eshin_weapon + heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', eshin_enchanted + heroes_enchanted, 1)
        self.pile = self.opt_options_list('Scavenge-pile', skaven_pile)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(50, self.magic + [self.pile])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.get_selected()), [])


class Warlock(Unit):
    name = 'Warlock Engineer'
    gear = ['Hand Weapon']
    base_points = 15

    def __init__(self):
        Unit.__init__(self)
        self.wiz = self.opt_options_list('Magic level', [
            ['Level 1 Wizard', 50, 'lvl1'],
            ['Level 2 Wizard', 85, 'lvl2'],
        ], limit=1)
        self.wep = self.opt_options_list('Weapon', [
            ['Warlock-Augmented Weapon', 45, 'waw'],
            ['Warplock Pistol', 8, 'wp'],
            ['Warpmusket', 15, 'wm'],
        ])
        self.magic_wep = self.opt_options_list('Magic weapon', skrire_weapon + heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', skrire_enchanted + heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.pile = self.opt_options_list('Scavenge-pile', skaven_pile)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc, self.magic_arc]

    def check_rules(self):
        self.magic_arm.set_visible(not self.wiz.is_any_selected())
        self.magic_arc.set_visible(self.wiz.is_any_selected())
        norm_point_limits(50, self.magic + [self.pile])
        if self.wiz.is_any_selected():
            self.gear = ['Hand Weapon', 'Skaven Spells of Ruin']
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.get_selected()), [])


class Chieftain(Unit):
    name = 'Chieftain'
    base_gear = ['Hand Weapon']
    base_points = 45

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Halberd', 2, 'hb'],
            ['Hand weapon', 2, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
        ])
        self.magic_wep = self.opt_options_list('Magic weapon', skaven_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', skaven_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', skaven_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', skaven_enchanted, 1)
        self.pile = self.opt_options_list('Scavenge-pile', skaven_pile)

        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', skaven_standard, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(skaven_shields_ids))
        self.gear = self.base_gear + ([] if self.magic_arm.is_selected(skaven_armour_suits_ids) else ['Heavy armour'])
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class Priest(Unit):
    name = 'Plague Priest'
    gear = ['Hand Weapon', 'Skaven Spells of Plague']
    base_points = 100

    class Furnace(Unit):
        name = 'Plague Furnace'
        base_points = 150
        static = True

        def check_rules(self):
            self.set_points(self.build_points())
            self.build_description(options=[])
            self.description.add(ModelDescriptor(name='Plague Monk Crew', count=1).build())

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.wep_1 = self.opt_options_list('Weapon', [
            ['Plague Cancer', 16, 'pc'],
            ['Flail', 4, 'fl'],
            ['Hand weapon', 2, 'hw'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Warlord.Rat(), self.Furnace()])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', pest_arcane + heroes_arcane, 1)
        self.pile = self.opt_options_list('Scavenge-pile', skaven_pile)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(50, self.magic + [self.pile])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Snikch(StaticUnit):
    base_points = 270
    name = 'Deathmaster Snikch'
    gear = ['Throwing stars', 'The Cloak of Shadows', 'Whirl of Weeping Blades']


class Tretch(StaticUnit):
    base_points = 145
    name = 'Tretch Craventail'
    gear = ['Heavy armour', 'Hand weapon', 'Hand weapon', 'Tail blade', 'Lucky Skullhelm']
