from builder.core2 import OneOf, Gear, ListSubUnit, OptionsList
from builder.games.wh40k.genestealers.armory import DemolitionCharges, CultVehicleEquipment
from builder.games.wh40k.genestealers.fast import Squadron
from builder.games.wh40k.unit import Unit


class GoliathRockgrinder(ListSubUnit):
    type_name = 'Goliath Rockgrinder'
    type_id = 'gs_goliath_rockgrinders_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(GoliathRockgrinder.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Heavy mining laser', 0)
            self.variant('Clearance incinerator', 5)
            self.variant('Heavy seismic cannon', 10)

    def __init__(self, parent):
        super(GoliathRockgrinder, self) .__init__(parent=parent, points=75, gear=[
            Gear('Heavy stubber'), Gear('Drilldoser blade')])

        self.Weapon(self)

        self.demolition = DemolitionCharges(self, name='Cult vehicle equipment')

    def has_demolition_charges(self):
        return self.demolition.charges.value


class LemanRuss(Unit):
    type_name = 'Leman Russ'
    type_id = 'gs_lemanruss_v1'

    def __init__(self, parent, command=False):
        super(LemanRuss, self).__init__(parent=parent, points=120,
                                        gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.type = self.MainWeapon(self)
        self.Weapon(self)
        self.Sponsons(self)
        CultVehicleEquipment(self, name='Cult vehicle equipment')

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(LemanRuss.MainWeapon, self).__init__(parent=parent, name='MainWeapon')
            self.variant('Eradicator nova cannon', 0)
            self.variant('Exterminator autocannon', 10)
            self.variant('Vanquisher battle cannon', 15)
            self.variant('Battle cannon', 30)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LemanRuss.Weapon, self).__init__(parent=parent, name='Weapon')

            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.lascannon = self.variant('Lascannon', 10)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)

            self.variant('Heavy flamers', 10, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Multi-meltas', 20, gear=[Gear('Multimelta', count=2)])
            self.variant('Plasma cannons', 30, gear=[Gear('Plasma cannon', count=2)])


class LemanRussSquadron(Squadron):
    type_name = 'Leman Russ Squadron'
    type_id = 'gs_leman_russ_squadron_v1'

    class Tank(LemanRuss, ListSubUnit):
        pass

    unit_class = Tank


class GoliathRockgrinderSquad(Squadron):
    type_name = 'Goliath Rockgrinder Squadron'
    type_id = 'gs_goliath_rockgrinders_squad_v1'
    unit_class = GoliathRockgrinder
