from builder.core2 import OneOf, OptionsList, Count


class BaseWeapon(OneOf):
    def __init__(self, parent, name, *args, **kwargs):
        self.relic_options = []

        super(BaseWeapon, self).__init__(parent, name, *args, **kwargs)

    def get_unique(self):
        if self.used and self.cur in self.relic_options:
            return self.description
        return []


class Autogun(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(Autogun, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Autogun', 0)


class Shotgun(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(Shotgun, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Shotgun', 0)


class Lasgun(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(Lasgun, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Lasgun', 0)


class SpecialWeapons(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(SpecialWeapons, self).__init__(parent, name=name, *args, **kwargs)
        self.f = self.variant('Flamer', 5)
        self.gl = self.variant('Grenade launcher', 5)
        self.web = self.variant('Webber', 10)

        self.special = [self.f, self.gl, self.web]

    def is_special(self):
        return self.cur in self.special


class HeavyMiningWeapons(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(HeavyMiningWeapons, self).__init__(parent, name=name, *args, **kwargs)
        self.hs = self.variant('Heavy stubber', 5)
        self.ml = self.variant('Mining laser', 15)
        self.sc = self.variant('Seismic cannon', 20)

        self.heavy = [self.hs, self.ml, self.sc]

    def is_heavy(self):
        return self.cur in self.heavy


class HeavyWeapons(BaseWeapon):

    class Flakk(OptionsList):
        def __init__(self, parent):
            super(HeavyWeapons.Flakk, self).__init__(parent=parent, name='', limit=None)
            self.flakkmissiles = self.variant('Flakk missiles', 10)

    def __init__(self, parent, *args, **kwargs):
        super(HeavyWeapons, self).__init__(parent, *args, **kwargs)

        self.mortar = self.variant('Mortar', 5)
        self.autocannon = self.variant('Autocannon', 10)
        self.heavybolter = self.variant('Heavy bolter', 10)
        self.missilelauncher = self.variant('Missile launcher', 15)
        self.lascannon = self.variant('Lascannon', 20)

        self.heavy = [self.mortar, self.autocannon, self.heavybolter, self.missilelauncher, self.lascannon]
        self.flakk = self.Flakk(parent=parent)

    def is_heavy(self):
        return self.cur in self.heavy

    def check_rules(self):
        if self.flakk:
            self.flakk.visible = self.flakk.used = self.cur == self.missilelauncher


class AutoPistol(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(AutoPistol, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Autopistol', 0)


class NeedlePistol(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(NeedlePistol, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Needle pistol', 0)


class ForseStave(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(ForseStave, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Force stave', 0)


class RendingClaws(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(RendingClaws, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Rending claws', 0)


class BlastingCharges(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(BlastingCharges, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Blasting charges', 0)


class ToxinIngector(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(ToxinIngector, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Toxin ingector', 0)


class SacredCultBanner(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(SacredCultBanner, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Sacred cult banner', 0)


class Bonesword(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(Bonesword, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Bonesword', 0)


class PistolWeapons(AutoPistol):
    def __init__(self, parent, name, *args, **kwargs):
        super(PistolWeapons, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Laspistol', 0)
        self.variant('Bolt pistol', 1)
        self.variant('Web pistol', 5)


class MeleeWeapons(BaseWeapon):
    def __init__(self, parent, name, *args, **kwargs):
        super(MeleeWeapons, self).__init__(parent, name=name, *args, **kwargs)
        self.variant('Close combat weapons', 0)
        self.variant('Chainsword', 0)
        self.variant('Power maul', 15)
        self.variant('Power pick', 15)


class CultVehicleEquipment(OptionsList):
    def __init__(self, parent, name):
        super(CultVehicleEquipment, self).__init__(parent, name=name)
        self.variant('Dozer blade', 5)
        self.variant('Heavy stubber', 5)
        self.variant('Storm bolter', 5)
        self.variant('Hunter-killer missile', 10)


class SacredRelic(BaseWeapon):
    def __init__(self, parent, name, magus=False, primus=False, iconward=False, *args, **kwargs):
        super(SacredRelic, self).__init__(parent=parent, name=name, *args, **kwargs)
        self.dagger = self.variant('Dagger of Swift Sacrifice', 15)
        self.scourge = self.variant('Scourge of Distant Stars', 15)
        self.relic_options += [self.dagger, self.scourge]
        if magus:
            self.staff = self.variant('Staff of the Subterran Master', 20)
            self.crouch = self.variant('The Crouchling', 20)
            self.relic_options += [self.staff, self.crouch]
        if primus:
            self.sword = self.variant('Sword of the Void\'s Eye', 15)
            self.relic_options += [self.sword]
        if iconward:
            self.icon = self.variant('Icon of the Cult Ascendant', 30)
            self.relic_options += [self.icon]


class Familliars(object):
    def __init__(self, parent, name):
        Count(parent, 'Familliar', 0, 2, points=5)


class Vehicle(OptionsList):
    def __init__(self, parent):
        super(Vehicle, self).__init__(parent, name='Options')

        self.variant('Searchlight', 1)
        self.variant('Smoke launchers', 5)
        self.variant('Hunter-killer missile', 10)


class DemolitionCharges(OptionsList):
    def __init__(self, parent, name):
        super(DemolitionCharges, self).__init__(parent, name=name)
        if hasattr(parent.roster, 'free_charges') and parent.roster.free_charges is True:
            self.freeflag = 0
        else:
            self.freeflag = 1

        self.charges = self.variant('Cache of demolition charges', 20 * self.freeflag)
