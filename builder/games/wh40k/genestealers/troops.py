__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UnitDescription, OneOf, UnitList, OptionalSubUnit, ListSubUnit, OptionsList, SubUnit
from builder.games.wh40k.roster import Unit
from builder.games.wh40k.genestealers.fast import GoliathTruck, Chimera
from builder.games.wh40k.genestealers.armory import HeavyWeapons, HeavyMiningWeapons, SpecialWeapons, Autogun, Shotgun, \
    Lasgun, MeleeWeapons, PistolWeapons


class Favoured(Unit):
    type_name = 'The Favoured Disciples'
    type_id = 'gs_disciples_v1'

    def __init__(self, parent):
        super(Favoured, self).__init__(parent, points=85, gear=[
            UnitDescription('Acolyte Hybrid', options=[
                Gear('Autopistol'), Gear('Close combat weapon'),
                Gear('Rending claws'), Gear('Blasting charges')
            ], count=12)
        ], static=True)

    def get_count(self):
        return 12


class Faithful(Unit):
    type_name = 'The Faithful Throng'
    type_id = 'gs_neophytes_v1'

    def __init__(self, parent):
        super(Faithful, self).__init__(parent, points=110, gear=[
            UnitDescription('Neophyte Hybrid', options=[
                Gear('Blasting charges'), Gear('Close combat weapon'),
                Gear('Autogun')], count=12),
            UnitDescription('Neophyte Hybrid', options=[
                Gear('Blasting charges'), Gear('Close combat weapon'),
                Gear('Grenade launcher')], count=2),
            UnitDescription('Neophyte Hybrid', options=[
                Gear('Blasting charges'), Gear('Close combat weapon'),
                Gear('Mining laser')], count=2)
        ], static=True)

    def get_count(self):
        return 16


class AcolyteHybrids(Unit):
    type_name = 'Acolyte Hybrid'
    type_id = 'gs_acolyte_hybrids_v1'

    model_points = 8
    model_gear = [Gear('Blasting charges')]

    class Acolyte(ListSubUnit):

        class CloseCombatWeapon(OneOf):
            def __init__(self, parent, name, *args, **kwargs):
                super(AcolyteHybrids.Acolyte.CloseCombatWeapon, self).__init__(parent, name=name, *args, **kwargs)
                self.ccw = self.variant('Close combat weapon', 0)

        class Claws(OneOf):
            def __init__(self, parent, name, *args, **kwargs):
                super(AcolyteHybrids.Acolyte.Claws, self).__init__(parent, name=name, *args, **kwargs)
                self.claws = self.variant('Rending claws', 0)
                self.charge = self.variant('Demolition charge', 20)
                self.variant('Heavy rock drill', 20)
                self.variant('Heavy rock cutter', 25)
                self.variant('Heavy rock saw', 25)

        class Pistols(OneOf):
            def __init__(self, parent, name, *args, **kwargs):
                super(AcolyteHybrids.Acolyte.Pistols, self).__init__(parent, name=name, *args, **kwargs)
                self.variant('Autopistol', 0)
                self.variant('Hand flamer', 5)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(AcolyteHybrids.Acolyte.Banners, self).__init__(parent, "Icon", limit=1)
                self.variant('Cult Icon', 10)

        def __init__(self, parent):
            super(AcolyteHybrids.Acolyte, self).__init__(
                name='Acolyte',
                parent=parent, points=AcolyteHybrids.model_points,
                gear=AcolyteHybrids.model_gear
            )
            self.ccw = self.CloseCombatWeapon(self, name='Close combat weapon')
            self.claws = self.Claws(self, name='Weapons')
            self.pistols = self.Pistols(self, name='Pistols')
            self.ban = self.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.ban.any

        @ListSubUnit.count_gear
        def count_special(self):
            return self.claws.cur != self.claws.claws

        @ListSubUnit.count_gear
        def count_charges(self):
            return self.claws.cur == self.claws.charge

        def check_rules(self):
            self.ccw.visible = self.ccw.used = self.count_special() == 0

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(AcolyteHybrids.Transport, self).__init__(parent, 'Transport', limit=1)
            self.models = [
                SubUnit(self, GoliathTruck(parent=None)),
            ]

    class AcolyteLeader(Unit):

        class CloseCombatWeapon(OneOf):
            def __init__(self, parent, name, *args, **kwargs):
                super(AcolyteHybrids.AcolyteLeader.CloseCombatWeapon, self).__init__(parent, name=name, *args, **kwargs)
                self.ccw = self.variant('Close combat weapon', 0)
                self.bonesword = self.variant('Bonesword', 20)

        class Pistols(OneOf):
            def __init__(self, parent, name, *args, **kwargs):
                super(AcolyteHybrids.AcolyteLeader.Pistols, self).__init__(parent, name=name, *args, **kwargs)
                self.variant('Autopistol', 0)
                self.variant('Hand flamer', 5)
                self.whip = self.variant('Lash whip', 5)

        def __init__(self, parent):
            super(AcolyteHybrids.AcolyteLeader, self).__init__(parent, name='Acolyte Leader', points=8 + 10,
                                                               gear=[Gear('Blasting charges'), Gear('Rending claws')])
            self.ccw = self.CloseCombatWeapon(self, 'Close combat weapon')
            self.pistols = self.Pistols(self, 'Pistols')

        def check_rules(self):
            if self.pistols.cur == self.pistols.whip:
                self.ccw.bonesword.used = True
                self.ccw.cur = self.ccw.bonesword

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(AcolyteHybrids.Leader, self).__init__(parent=parent, name='Acolyte Leader', limit=1)
            SubUnit(self, AcolyteHybrids.AcolyteLeader(parent=None))

    def __init__(self, parent):
        super(AcolyteHybrids, self).__init__(parent=parent)
        self.acolythes = UnitList(self, self.Acolyte, 5, 20)
        self.leader = self.Leader(self)
        self.transport = self.Transport(parent=self)

    def get_count(self):
        return self.acolythes.count + self.leader.count

    def check_rules(self):
        super(AcolyteHybrids, self).check_rules()
        self.acolythes.update_range(
            5 - self.leader.count,
            20 - self.leader.count
        )

        spec_total = sum(acolythe.count_special() for acolythe in self.acolythes.units)
        if 5 * spec_total > 2 * self.get_count():
            self.error('In Acolyte squad may be only 2 special weapon per 5 models (taken: {0})'.format(spec_total))
        flags = sum(pal.count_banners() for pal in self.acolythes.units)
        if flags > 1:
            self.error('Only one banner per unit may be taken; taken: {}'.format(flags))

    def build_statistics(self):
        return self.note_transport(super(AcolyteHybrids, self).build_statistics())


class NeophyteWeaponTeam(Unit):
    type_name = 'Neophyte Weapon Team'

    def __init__(self, parent, root_unit):
        super(NeophyteWeaponTeam, self).__init__(parent, gear=[Gear('Autogun'), Gear('Autopistol'), Gear('Blasting Charges')])
        self.base_unit = root_unit
        self.wep = HeavyWeapons(self, name='Weapon')


class NeophyteHybrids(Unit):
    type_name = 'Neophyte Hybrid'
    type_id = 'gs_neophyte_hybrids_v1'

    model_points = 5

    class Hybrid(ListSubUnit):

        class Weapon(HeavyMiningWeapons, SpecialWeapons, Lasgun, Shotgun, Autogun):
            pass

        class Banners(OptionsList):
            def __init__(self, parent):
                super(NeophyteHybrids.Hybrid.Banners, self).__init__(parent, "Icon", limit=1)
                self.variant('Cult Icon', 10)

        def __init__(self, parent):
            super(NeophyteHybrids.Hybrid, self).__init__(
                parent=parent, points=NeophyteHybrids.model_points, name='Hybrid',
                gear=[Gear('Blasting charges'), Gear('Autopistol')]
            )
            self.wep = self.Weapon(self, 'Weapon')
            self.ban = self.Banners(self)

        @ListSubUnit.count_gear
        def count_heavy_weapon(self):
            return self.wep.is_heavy()

        @ListSubUnit.count_gear
        def count_special_weapon(self):
            return self.wep.is_special()

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.ban.any

    class NeophyteLeader(Unit):
        def __init__(self, parent):
            super(NeophyteHybrids.NeophyteLeader, self).__init__(parent, name='Neophyte Leader', points=5 + 10,
                                                               gear=[Gear('Blasting charges'), ])
            self.ccw = MeleeWeapons(self, 'Melee weapons')
            self.pistols = PistolWeapons(self, 'Pistols')

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(NeophyteHybrids.Leader, self).__init__(parent=parent, name='Neophyte Leader', limit=1)
            SubUnit(self, NeophyteHybrids.NeophyteLeader(parent=None))

    class Members(OptionalSubUnit):
        def __init__(self, parent):
            super(NeophyteHybrids.Members, self).__init__(parent, name='', limit=1)
            self.heavy = SubUnit(self, NeophyteWeaponTeam(None, parent))

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(NeophyteHybrids.Transport, self).__init__(parent, 'Transport', limit=1)
            self.truck = SubUnit(self, GoliathTruck(parent=None))
            self.chimera = SubUnit(self, Chimera(parent=None))
            self.models = [
                self.truck,
                self.chimera,
            ]

    def __init__(self, parent):
        super(NeophyteHybrids, self).__init__(parent)
        self.leader = self.Leader(self)
        self.hybrids = UnitList(self, self.Hybrid, 10, 20)
        self.members = self.Members(self)
        self.transport = self.Transport(parent=self)

    def check_rules(self):
        heavy_total = sum(u.count_heavy_weapon() for u in self.hybrids.units)
        if 2 < heavy_total:
            self.error('Neophyte Hybrid can take only 2 heavy weapon (taken: {0})'.format(heavy_total))

        special_total = sum(u.count_special_weapon() for u in self.hybrids.units)
        if 2 < special_total:
            self.error('Neophyte Hybrid can take only 2 special weapon (taken: {0})'.format(special_total))

        flags = sum(pal.count_banners() for pal in self.hybrids.units)
        if flags > 1:
            self.error('Only one banner per unit may be taken; taken: {}'.format(flags))

        self.hybrids.update_range(
            10 - self.members.count * 2 - self.leader.count,
            20 - self.members.count * 2 - self.leader.count
        )

    def get_count(self):
        return self.leader.count + self.hybrids.count + self.members.count * 2

    def build_statistics(self):
        return self.note_transport(super(NeophyteHybrids, self).build_statistics())
