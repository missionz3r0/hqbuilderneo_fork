__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import Unit


class Belakor(Unit):
    type_name = 'Be\'lakor (Dataslate)'
    type_id = 'belakor_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Be-lakor')

    def __init__(self, parent):
        super(Belakor, self).__init__(parent, name='Be\'lakor', points=350, unique=True,
                                      gear=[Gear('The Blade of Shadow')])

    def count_charges(self):
        return 3

    def build_statistics(self):
        return self.note_charges(super(Belakor, self).build_statistics())
