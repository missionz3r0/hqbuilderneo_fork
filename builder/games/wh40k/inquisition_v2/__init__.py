from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, Wh40kImperial,\
    Wh40kKillTeam, PrimaryDetachment
from builder.games.wh40k.section import HQSection, ElitesSection
from builder.games.wh40k.imperial_armour.volume5_7 import InquisitorCharacters
from .hq import *
from .elites import *
from builder.games.wh40k.adepta_sororitas_v4.troops import Sisters
from builder.games.wh40k.adepta_sororitas_v4.elites import Crusaders, DeathCultAssasins, ArcoFlagellants
from builder.games.wh40k.adepta_sororitas_v4.hq import Priest
from builder.games.wh40k.grey_knights_v3.troops import TerminatorSquad
from builder.games.wh40k.deathwatch.troops import Veterans
from builder.games.wh40k.astra_telepathica.hq import Astropath
from builder.games.wh40k.cult_mechanicus.hq import Techpriest


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.coteaz = UnitType(self, Coteaz)
        self.karamazov = UnitType(self, Karamazov)
        UnitType(self, Greyfax)
        self.malleus = UnitType(self, Malleus)
        self.hereticus = UnitType(self, Hereticus)
        self.xenos = UnitType(self, Xenos)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        self.acolytes = UnitType(self, Acolytes)
        self.daemonhost = UnitType(self, Daemonhost)
        self.jokaero = UnitType(self, Jokaero)


class HQ(InquisitorCharacters, BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class InquisitionRepresentative(Wh40kBase):
    army_id = 'inquisition_det_v2'
    army_name = 'Inquisition Representative'

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        super(InquisitionRepresentative, self).__init__(
            hq=self.hq, elites=self.elites
        )


class InquisitionV2KillTeam(Wh40kKillTeam):
    army_id = 'inquisition_v2_kt'
    army_name = 'Inquisition'

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(InquisitionV2KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Acolytes],
                                                    lambda u: u.transport.transports[2:])

    def __init__(self):
        super(InquisitionV2KillTeam, self).__init__(None, self.KTElites(self), None)


class Warband(Formation):
    army_id = 'inquisition_v2_Warband'
    army_name = 'Inquisitorial Henchmen Warband'

    def __init__(self):
        super(Warband, self).__init__()
        self.coteaz = UnitType(self, Coteaz)
        self.karamazov = UnitType(self, Karamazov)
        self.greyfax = UnitType(self, Greyfax)
        self.malleus = UnitType(self, Malleus)
        self.hereticus = UnitType(self, Hereticus)
        self.xenos = UnitType(self, Xenos)
        self.inq = [self.coteaz, self.karamazov, self.malleus, self.hereticus, self.xenos,
                    self.greyfax]
        self.add_type_restriction(self.inq, 1, 1)

        UnitType(self, Acolytes, min_limit=1, max_limit=1)
        UnitType(self, Priest, min_limit=0, max_limit=1)
        UnitType(self, Crusaders, min_limit=0, max_limit=1)
        UnitType(self, Daemonhost, min_limit=0, max_limit=6)
        UnitType(self, ArcoFlagellants, min_limit=0, max_limit=1)
        UnitType(self, DeathCultAssasins, min_limit=0, max_limit=1)
        UnitType(self, Techpriest, min_limit=0, max_limit=1)
        UnitType(self, Jokaero, min_limit=0, max_limit=6)
        UnitType(self, Astropath, min_limit=0, max_limit=1)

        self.terms = UnitType(self, TerminatorSquad)
        self.sisters = UnitType(self, Sisters)
        self.vets = UnitType(self, Veterans)

    def check_rules(self):
        if (self.coteaz.count + self.malleus.count) > 0:
            self.terms.min_limit, self.terms.max_limit = (0, 1)
            self.sisters.min_limit, self.sisters.max_limit = (0, 0)
            self.vets.min_limit, self.vets.max_limit = (0, 0)
        elif (self.karamazov.count + self.hereticus.count) > 0:
            self.terms.min_limit, self.terms.max_limit = (0, 0)
            self.sisters.min_limit, self.sisters.max_limit = (0, 1)
            self.vets.min_limit, self.vets.max_limit = (0, 0)
        else:
            self.terms.min_limit, self.terms.max_limit = (0, 0)
            self.sisters.min_limit, self.sisters.max_limit = (0, 0)
            self.vets.min_limit, self.vets.max_limit = (0, 1)
        super(Warband, self).check_rules()


faction = 'Inquisition'


class InquisitionV2(Wh40kImperial, Wh40k7ed):
    army_id = 'inquisition_v2'
    army_name = 'Inquisition'
    faction = faction

    def __init__(self):
        super(InquisitionV2, self).__init__([InquisitionRepresentative, Warband])


detachments = [InquisitionRepresentative, Warband]


unit_types = [
    Coteaz, Karamazov, Malleus, Hereticus, Xenos, Acolytes,
    Daemonhost, Jokaero, Greyfax
]
