from builder.core2 import Gear, ListSubUnit, UnitList, SubUnit,\
    OneOf, OptionalSubUnit
from builder.games.wh40k.ynnari.armory import YnnariBlade, YnnariPistol
from builder.games.wh40k.roster import Unit
from .armory import YnnariEnigmas, Haywire
from .fast import Starweaver

__author__ = 'Ivan Truskov'


class Troupe(Unit):
    type_name = 'Troupe'
    type_id = 'troupe_v1'
    transport_allowed = True

    model_gear = [Gear('Holo-suit'), Gear('Plasma grenades'), Gear('Flip belt')]

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Troupe.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant('Shuriken pistol', 0)
            self.variant('Neuro distruptor', 10)
            self.variant('Fusion pistol', 15)

    class Melee(OneOf):
        def __init__(self, parent):
            super(Troupe.Melee, self).__init__(parent, 'Melee weapon')
            self.variant('Close combat weapon', 0)
            self.variant('Harlequin\'s embrace', 5)
            self.variant('Harlequin\'s kiss', 5)
            self.variant('Harlequin\'s caress', 8)

    class Player(ListSubUnit):
        def __init__(self, parent):
            super(Troupe.Player, self).__init__(parent, 'Player', 15, Troupe.model_gear)
            Troupe.Melee(self)
            Troupe.Ranged(self)

    class MasterMelee(Melee):
        def __init__(self, parent):
            super(Troupe.MasterMelee, self).__init__(parent)
            self.variant('Power sword', 15)
            self.rose = self.variant('Cegorach\'s Rose', 15)
            self.story = self.variant('The Storied Sword', 25)
            self.relic_options = [self.rose, self.story]

        def get_unique(self):
            if self.cur in self.relic_options:
                return self.description
            return []

    class ArtefactMelee(YnnariBlade, MasterMelee):
        def __init__(self, parent):
            super(Troupe.ArtefactMelee, self).__init__(parent)
            self.relic_options.append(self.hblade)

    class MasterRanged(Ranged):
        def __init__(self, parent):
            super(Troupe.MasterRanged, self).__init__(parent)
            self.crescendo = self.variant('Crescendo', 5)
            self.relic_options = [self.crescendo]

        def get_unique(self):
            if self.cur in self.relic_options:
                return self.description
            return []

    class ArtefactRanged(YnnariPistol, MasterRanged):
        def __init__(self, parent):
            super(Troupe.ArtefactRanged, self).__init__(parent)
            self.relic_options.append(self.ysong)

    class TroupeMaster(Unit):

        def __init__(self, parent):
            super(Troupe.TroupeMaster, self).__init__(parent, 'Troupe Master', 35, Troupe.model_gear)
            self.mle = Troupe.ArtefactMelee(self)
            self.rng = Troupe.ArtefactRanged(self)
            self.enigmas = YnnariEnigmas(self)
            Haywire(self)

        def get_unique_gear(self):
            return self.mle.get_unique() + self.rng.get_unique() + self.enigmas.get_unique()

        def check_rules(self):
            super(Troupe.TroupeMaster, self).check_rules()
            if len(self.get_unique_gear()) > 1:
                self.error('Only one Enigma of the Black Library may be carried by model')

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Troupe.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Starweaver(parent=None))

    def __init__(self, parent):
        super(Troupe, self).__init__(parent, 'Troupe')
        self.master = SubUnit(self, self.TroupeMaster(parent=None))
        self.troupe = UnitList(self, self.Player, 4, 11)
        if self.transport_allowed:
            self.transport = self.Transport(self)

    def get_unique_gear(self):
        return self.master.unit.get_unique_gear()

    def get_count(self):
        return self.troupe.count + 1

    def build_statistics(self):
        return self.note_transport(super(Troupe, self).build_statistics())
