from builder.core2 import Gear, UnitDescription
from builder.games.wh40k.roster import Unit


class DeathsCompanions(Unit):
    type_name = 'Death\'s Companions'
    type_id = 'deaths_companions_v1'

    def __init__(self, parent):
        gear = [Gear('Holo-suit'), Gear('Plasma grenades'), Gear('Flip belt')]
        super(DeathsCompanions, self).__init__(parent, points=135, gear=[
            UnitDescription('Dusk', options=gear + [Gear('Shuriken pistol'), Gear('Close combat weapon')]),
            UnitDescription('Player', options=gear + [Gear('Shuriken pistol'), Gear('Close combat weapon')]),
            UnitDescription('Player', count=2, options=gear + [Gear('Shuriken pistol'), Gear('Harlequin\'s kiss')]),
            UnitDescription('Player', options=gear + [Gear('Neuro distruptor'), Gear('Harlequin\'s caress')]),
            UnitDescription('Player', options=gear + [Gear('Shuriken pistol'), Gear('Harlequin\'s caress')]),
        ], unique=True, static=True)

    def get_count(self):
        return 6

    def build_description(self):
        result = super(DeathsCompanions, self).build_description()
        result.count = 1
        return result


class CompanyOfTheThreefoldStranger(Unit):
    type_name = 'Company of the Threefold Stranger'
    type_id = 'company_threefold_stranger_v1'

    def __init__(self, parent):
        gear = [Gear('Holo-suit'), Gear('Plasma grenades'), Gear('Flip belt')]
        super(CompanyOfTheThreefoldStranger, self).__init__(parent, points=155, gear=[
            UnitDescription('The Lambent Prince', options=gear + [
                Gear('Shuriken pistol'), Gear('Power Sword'), Gear('Haywire grenades')
            ]),
            UnitDescription('Player', count=2, options=gear + [Gear('Shuriken pistol'), Gear('Close combat weapon')]),
            UnitDescription('Player', options=gear + [Gear('Shuriken pistol'), Gear('Harlequin\'s kiss')]),
            UnitDescription('Player', options=gear + [Gear('Neuro distruptor'), Gear('Close combat weapon')]),
            UnitDescription('Player', options=gear + [Gear('Neuro distruptor'), Gear('Harlequin\'s kiss')]),
        ], unique=True, static=True)

    def get_count(self):
        return 6

    def build_description(self):
        result = super(CompanyOfTheThreefoldStranger, self).build_description()
        result.count = 1
        return result


class Inriam(Unit):
    type_name = 'Inriam\'s Spectre'
    type_id = 'inriam_v1'

    def __init__(self, parent):
        super(Inriam, self).__init__(parent, points=70, gear=[
            Gear('Holo-suit'), Gear('Shrieker cannon'), Gear('Flip belt'), Gear('Haywire grenades')
        ], static=True, unique=True)


class TheBladesOfFate(Unit):
    type_name = 'The Blades of Fate'
    type_id = 'blades_of_fate_v1'

    def __init__(self, parent):
        super(TheBladesOfFate, self).__init__(parent, points=125, gear=[
            UnitDescription('Blade of Fate', count=2, options=[
                Gear('Holo-suit'), Gear('Mirage launchers'), Gear('Zephyrglaive'), Gear('Skyweaver jetbike')
            ]),
        ], unique=True, static=True)

    def get_count(self):
        return 2

    def build_description(self):
        result = super(TheBladesOfFate, self).build_description()
        result.count = 1
        return result


class SerpentsBreath(Unit):
    type_name = 'The Serpent\'s Breath'
    type_id = 'serpents_breath_v1'

    def __init__(self, parent):
        super(SerpentsBreath, self).__init__(parent, 'Serpents Breath', 90, gear=[
            Gear('Prismatic cannon'), Gear('Shuriken cannon', count=2),
            Gear('Holo-fields'), Gear('Mirage launchers')], static=True,
                                      unique=True)
