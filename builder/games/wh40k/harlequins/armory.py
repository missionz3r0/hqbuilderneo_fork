from builder.core2 import OptionsList
from builder.games.wh40k.ynnari.armory import YnnariArtefacts

__author__ = 'Ivan Truskov'


class CommonEnigmas(OptionsList):
    def __init__(self, parent, seer=False):
        super(CommonEnigmas, self).__init__(parent,
                                            'Enigmas of the black Library',
                                            limit=1)
        self.variant('The Laughing God\'s Eye', 20)
        self.variant('The Starmist Raiment', 25)
        if seer:
            self.variant('The Mask of Secrets', 15)

    def get_unique(self):
        if self.used:
            return self.description
        return []


class YnnariEnigmas(YnnariArtefacts, CommonEnigmas):
    pass


class Haywire(OptionsList):
    def __init__(self, parent):
        super(Haywire, self).__init__(parent, 'Options')
        self.variant('Haywire grenades', 5)
