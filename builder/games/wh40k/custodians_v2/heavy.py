from builder.core2 import Gear, OptionsList
from builder.games.wh40k.unit import Unit


class VenLandRaider(Unit):
    type_name = 'Venerable Land Raider'
    type_id = 'vland_raider_v2'

    class Options(OptionsList):
        def __init__(self, parent):
            super(VenLandRaider.Options, self).__init__(parent, 'Options')
            self.variant('Storm bolter', 5)
            self.variant('Extra armour', 10)
            self.variant('Hunter-killer missile', 10)

    def __init__(self, parent):
        super(VenLandRaider, self).__init__(parent, points=275, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Searchlight'), Gear('Smoke launchers')
        ])
        self.Options(self)
