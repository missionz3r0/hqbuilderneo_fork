__author__ = 'Ivan Truskov'

from builder.games.wh40k.section import ElitesSection, TroopsSection, UnitType, HQSection, HeavySection
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from builder.games.wh40k.roster import Wh40k7ed, Wh40kImperial, Wh40kBase, CombinedArmsDetachment, AlliedDetachment, \
    PrimaryDetachment, PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as BaseLords
from builder.games.wh40k.fortifications import Fort
from .troops import CustodianSquad
from .elites import VenDread
from .heavy import VenLandRaider


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class CustTroops(TroopsSection):
    def __init__(self, parent):
        super(CustTroops, self).__init__(parent, 1, 3)
        UnitType(self, CustodianSquad)


class CustElites(ElitesSection):
    def __init__(self, parent):
        super(CustElites, self).__init__(parent, 0, 1)
        UnitType(self, VenDread)


class CustHeavy(HeavySection):
    def __init__(self, parent):
        super(CustHeavy, self).__init__(parent)
        UnitType(self, VenLandRaider)


class LordsOfWar(CerastusSection, BaseLords):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class GoldenLegion(Wh40kBase):
    army_name = 'Golden Legion Task Force'
    army_id = 'custodians_v2_task'

    def __init__(self):
        super(GoldenLegion, self).__init__(sections=[
            CustTroops(self), CustElites(self)
        ])


class CustodesV2Base(Wh40kBase):

    def __init__(self):
        super(CustodesV2Base, self).__init__(
            hq=HQ(parent=self),
            elites=CustElites(parent=self), troops=CustTroops(parent=self),
            fast=None, heavy=CustHeavy(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )


class CustodesV2CAD(CustodesV2Base, CombinedArmsDetachment):
    army_id = 'custodians_v2_cad'
    army_name = 'Adeptus Custodes (Combined arms detachment)'


class CustodesV2AD(CustodesV2Base, AlliedDetachment):
    army_id = 'custodians_v2_ad'
    army_name = 'Adeptus Custodes (Allied detachment)'


class CustodesV2PA(CustodesV2Base, PlanetstrikeAttacker):
    army_id = 'custodians_v2_pa'
    army_name = 'Adeptus Custodes (Planetstrike attacker detachment)'


class CustodesV2PD(CustodesV2Base, PlanetstrikeDefender):
    army_id = 'custodians_v2_pd'
    army_name = 'Adeptus Custodes (Planetstrike defender detachment)'


class CustodesV2SA(CustodesV2Base, SiegeAttacker):
    army_id = 'custodians_v2_sa'
    army_name = 'Adeptus Custodes (Siege War attacker detachment)'


class CustodesV2SD(CustodesV2Base, SiegeDefender):
    army_id = 'custodians_v2_sd'
    army_name = 'Adeptus Custodes (Siege War defender detachment)'


faction = 'Custodian Guard'


class CustodianGuardV2(Wh40k7ed, Wh40kImperial):
    army_id = 'custodians_v2'
    army_name = 'Custodian Guard'
    faction = faction

    def __init__(self):
        super(CustodianGuardV2, self).__init__([GoldenLegion, CustodesV2CAD])


class CustodianGuardV2Missions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'custodians_v2_mis'
    army_name = 'Custodian Guard'
    faction = faction

    def __init__(self):
        super(CustodianGuardV2Missions, self).__init__([GoldenLegion, CustodesV2CAD, 
                                                        CustodesV2PA, CustodesV2PD, CustodesV2SA, CustodesV2SD])


detachments = [
    GoldenLegion,
    CustodesV2CAD,
    CustodesV2AD,
    CustodesV2PA,
    CustodesV2PD,
    CustodesV2SA,
    CustodesV2SD
]
unit_types = [CustodianSquad, VenDread, VenLandRaider]
