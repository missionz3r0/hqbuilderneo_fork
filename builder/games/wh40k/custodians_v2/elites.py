from builder.core2 import Gear, OneOf
from builder.games.wh40k.unit import Unit


class VenDread(Unit):
    type_name = 'Venerable Contemptor Dreadnought'
    type_id = 'vdread_v2'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(VenDread.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Multi-melta')
            self.variant('Kheres pattern assault cannon', 15)

    def __init__(self, parent):
        super(VenDread, self).__init__(parent, points=200, gear=[
            Gear('Power fist'), Gear('Combi-bolter'),
            Gear('Atomantic Shielding')
        ])
        self.Weapon(self)
