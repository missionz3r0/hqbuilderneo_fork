__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'
__summary__ = ['Codes Tyranids 6th Edition', 'Shield of Ball: Leviathan', 'Shield of Baal: Deathstorm']

from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, Formation, CompositeFormation
from builder.games.wh40k.fortifications import Fort
from builder.games.wh40k.imperial_armour.volume4 import AnphelionFast, AnphelionElites,\
    AnphelionHeavy, AnphelionLords
from builder.games.wh40k.escalation.tyranids import LordsOfWar as EscalationLords


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.hive_tyrant = UnitType(self, HiveTyrant)
        self.swarmlord = UnitType(self, TheSwarmlord)
        self.guard_brood = UnitType(self, GuardBrood, slot=0)
        self.old = UnitType(self, OldOneEye)
        self.tervigon = UnitType(self, Tervigon)
        self.prime = UnitType(self, TyranidPrime)
        self.leaper = UnitType(self, Deathleaper)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, HiveGuard)
        UnitType(self, Zoanthrope)
        UnitType(self, Venomethrope)
        UnitType(self, Lictor)
        UnitType(self, Haruspex)
        UnitType(self, Pyrovore)
        UnitType(self, Maleceptor)


class Elites(AnphelionElites, BaseElites):
    pass


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, TyranidWarriorBrood)
        UnitType(self, Genestealer)
        self.termagant = UnitType(self, Termagant)
        UnitType(self, Hormagant)
        UnitType(self, Ripper)
        self.tervigon = UnitType(self, Tervigon)
        UnitType(self, MucolidSpores)
        UnitType(self, CryptusChildren)
        UnitType(self, PhodianWarriors)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, TyranidShrikeBrood)
        UnitType(self, RavenerBrood)
        UnitType(self, SkySlasherBrood)
        UnitType(self, Gargoyle)
        UnitType(self, Harpy)
        UnitType(self, HiveCrone)
        UnitType(self, SporeMine)


class FastAttack(AnphelionFast, BaseFastAttack):
    pass


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, CarnifexBrood)
        UnitType(self, Biovore)
        UnitType(self, Trygon)
        UnitType(self, TrygonPrime)
        UnitType(self, Mawloc)
        UnitType(self, Exocrine)
        UnitType(self, Tyrannofex)
        UnitType(self, Toxicrene)
        UnitType(self, Tyrannocyte, slot=0)
        UnitType(self, Sporocyst)
        UnitType(self, BeastOfPhodia)


class HeavySupport(AnphelionHeavy, BaseHeavySupport):
    pass


class LordsOfWar(AnphelionLords, EscalationLords):
    pass


class TyranidsV2Common(Roster):
    def check_rules(self):
        super(TyranidsV2Common, self).check_rules()

        com = sum(ut.count for ut in [self.hq.hive_tyrant, self.hq.swarmlord])
        self.hq.guard_brood.active = com > 0
        if com < self.hq.guard_brood.count:
            self.error("You may include only one Tyrant Guard Brood for each Hive Tyrant (including the Swarmlord) in "
                       "your army.")

        ter = sum(u.get_count() >= 30 for u in self.troops.termagant.units)
        self.troops.tervigon.active = ter > 0
        if ter < self.troops.tervigon.count:
            self.error("For every Termagant Brood of 30 models included in your army, you can include one Tervigon as "
                       "a troops choice instead of an HQ choice.")


class TyranidsV2Base(Wh40kBase, TyranidsV2Common):
    army_id = 'tyranids_v2_base'
    army_name = 'Tyranids'

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(TyranidsV2Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )


class HypertoxicNode(Formation):
    army_id = 'tyranids_v2_toxic'
    army_name = 'Hypertoxic Node'

    class HiveTyrant(HiveTyrant):
        tyrant_gear = [Gear('Toxic Miasma')]

    def __init__(self):
        super(HypertoxicNode, self).__init__()
        self.tyrant = UnitType(self, self.HiveTyrant, min_limit=1, max_limit=1)
        UnitType(self, Toxicrene, min_limit=1, max_limit=1)
        UnitType(self, Venomethrope, min_limit=3, max_limit=3)

    def check_rules(self):
        super(HypertoxicNode, self).check_rules()
        if not all([u.morphs.sacs.value for u in self.tyrant.units]):
            self.error('The Hive Tyrant must take the Toxin Sacs biomorph')


class NeuralNode(Formation):
    army_id = 'tyranids_v2_neural'
    army_name = 'Neural Node'

    def __init__(self):
        super(NeuralNode, self).__init__()
        UnitType(self, Maleceptor, min_limit=1, max_limit=1)
        self.zoans = UnitType(self, Zoanthrope, min_limit=3, max_limit=3)

    def check_rules(self):
        super(NeuralNode, self).check_rules()
        if not all([u.has_neurothrope() for u in self.zoans.units]):
            self.error('Each Zoanthrope Brood must include a Neurothrope')


class SkytyrantSwarm(Formation):
    army_id = 'tyranids_v2_skytyrant'
    army_name = 'Skytyrant Swarm'

    def __init__(self):
        super(SkytyrantSwarm, self).__init__()
        self.tyrant = UnitType(self, HiveTyrant, min_limit=1, max_limit=1)
        UnitType(self, Gargoyle, min_limit=2, max_limit=2)

    def check_rules(self):
        super(SkytyrantSwarm, self).check_rules()
        if not all([u.opt.wings.value for u in self.tyrant.units]):
            self.error('The Hive Tyrant must take the Wings biomorph')


class SkyblightSwarm(Formation):
    army_id = 'tyranids_v2_skyblight'
    army_name = 'Skyblight Swarm'

    def __init__(self):
        super(SkyblightSwarm, self).__init__()
        self.tyrant = UnitType(self, HiveTyrant, min_limit=1, max_limit=1)
        UnitType(self, HiveCrone, min_limit=1, max_limit=1)
        UnitType(self, Harpy, min_limit=2, max_limit=2)
        UnitType(self, Gargoyle, min_limit=3, max_limit=3)

    def check_rules(self):
        super(SkyblightSwarm, self).check_rules()
        if not all([u.opt.wings.value for u in self.tyrant.units]):
            self.error('The Hive Tyrant must take the Wings biomorph')


class Sporefield(Formation):
    army_id = 'tyranids_v2_spores'
    army_name = 'Sporefield'

    def __init__(self):
        super(Sporefield, self).__init__()
        UnitType(self, MucolidSpores, min_limit=3, max_limit=3)
        UnitType(self, SporeMine, min_limit=3, max_limit=3)


class Skytide(CompositeFormation):
    army_id = 'tyranids_v2_skytide'
    army_name = 'Skytide'

    def __init__(self):
        super(Skytide, self).__init__()
        Detachment.build_detach(self, SkytyrantSwarm, min_limit=1, max_limit=1)
        Detachment.build_detach(self, SkyblightSwarm, min_limit=3, max_limit=3)
        Detachment.build_detach(self, Sporefield, min_limit=1, max_limit=1)


class ForestBrood(Formation):
    army_id = 'tyranids_v2_forest'
    army_name = 'Lictor Forest Brood'

    class ForestLictor(Lictor):
        def __init__(self, parent):
            super(ForestBrood.ForestLictor, self).__init__(parent)
            self.models.min, self.models.max = (5, 5)
            self.models.cur = 5

    def __init__(self):
        super(ForestBrood, self).__init__()
        UnitType(self, self.ForestLictor, min_limit=1, max_limit=1)


class Manufactorum(Formation):
    army_id = 'tyranids_v2_manofactorum'
    army_name = 'Manufactorum Genestealers'

    def __init__(self):
        super(Manufactorum, self).__init__()
        self.broods = UnitType(self, Genestealer, min_limit=5, max_limit=5)

    def check_rules(self):
        super(Manufactorum, self).check_rules()
        for unit in self.broods.units:
            if unit.models.cur > unit.models.min:
                self.error('Manufactorum genestealer Broods may not include additional Genestealers')
                break


class Asassin(Formation):
    army_id = 'tyranids_v2_asassin'
    army_name = "Deathleaper's Assassin Brood"

    class AssassinLictor(Lictor):
        def __init__(self, parent):
            super(Asassin.AssassinLictor, self).__init__(parent)
            self.models.min, self.models.max = (1, 1)

    def __init__(self):
        super(Asassin, self).__init__()
        UnitType(self, Deathleaper, min_limit=1, max_limit=1)
        UnitType(self, self.AssassinLictor, min_limit=5, max_limit=5)


class HuntingPack(Formation):
    army_id = 'tyranids_v2_hunting'
    army_name = "Broodlord's Hunting Pack"

    def __init__(self):
        super(HuntingPack, self).__init__()
        self.broods = UnitType(self, Genestealer, min_limit=3, max_limit=3)

    def check_rules(self):
        super(HuntingPack, self).check_rules()
        lordcount = sum(u.leader.count for u in self.broods.units)
        if lordcount < 1:
            self.error('One Genesteaer Brood must include a Broodlord')
        elif lordcount > 1:
            self.error('Only one Broodlord can be taken in this Formation')


class BioBombs(Formation):
    army_id = 'tyranids_v2_bombs'
    army_name = 'Gargoyle Bio-Bombs'

    def __init__(self):
        super(BioBombs, self).__init__()
        UnitType(self, Gargoyle, min_limit=3, max_limit=3)
        UnitType(self, SporeMine, min_limit=3, max_limit=3)


class IncubatorNode(Formation):
    army_id = 'tyranids_v2_incubator'
    army_name = 'Incubator Node'

    def __init__(self):
        super(IncubatorNode, self).__init__()
        UnitType(self, Tervigon, min_limit=1, max_limit=1)
        UnitType(self, Termagant, min_limit=3, max_limit=3)


class SynapricSwarm(Formation):
    army_id = 'tyranids_v2_synaptic'
    army_name = 'Synaptic Swarm'

    def __init__(self):
        super(SynapricSwarm, self).__init__()
        UnitType(self, TyranidPrime, min_limit=1, max_limit=1)
        UnitType(self, TyranidWarriorBrood, min_limit=3, max_limit=3)


class LivingArtillery(Formation):
    army_id = 'tyranids_v2_artillery'
    army_name = 'Living Artillery Node'

    def __init__(self):
        super(LivingArtillery, self).__init__()
        UnitType(self, Exocrine, min_limit=1, max_limit=1)
        UnitType(self, Biovore, min_limit=3, max_limit=3)
        self.wars = UnitType(self, TyranidWarriorBrood, min_limit=1, max_limit=1)

    def check_rules(self):
        super(LivingArtillery, self).check_rules()
        can_any = any(u.count_cannons() for u in self.wars.units)
        if not can_any:
            self.error('The Tyranid Warrior Brood from this formation must include a model that has taken an item from Basic Bio-cannons list')


class EndlessSwarm(Formation):
    army_id = 'tyranids_v2_endless'
    army_name = 'Endless Swarm'

    def __init__(self):
        super(EndlessSwarm, self).__init__()
        UnitType(self, Hormagant, min_limit=3, max_limit=3)
        UnitType(self, Termagant, min_limit=2, max_limit=2)
        UnitType(self, TyranidWarriorBrood, min_limit=1, max_limit=1)


class BioblastNode(Formation):
    army_id = 'tyranids_v2_bioblast'
    army_name = 'Bioblast Node'

    def __init__(self):
        super(BioblastNode, self).__init__()
        self.wars = UnitType(self, TyranidWarriorBrood, min_limit=1, max_limit=1)
        self.fexes = UnitType(self, CarnifexBrood, min_limit=3, max_limit=3)
        UnitType(self, Tyrannofex, min_limit=1, max_limit=1)

    def check_rules(self):
        super(BioblastNode, self).check_rules()
        can_any = any([u.count_cannons() for u in self.wars.units])
        if not can_any:
            self.error('The Tyranid Warrior Brood from this formation must include a model that has taken an item from Basic Bio-cannons list')
        can_all = all([u.all_have_cannons() for u in self.fexes.units])
        if not can_all:
            self.error('Carnifex models in this Formation must take at least one option from Monstrous Bio-cannons list')


class WreckerNode(Formation):
    army_id = 'tyranids_v2_wrecker'
    army_name = 'Wrecker Node'

    def __init__(self):
        super(WreckerNode, self).__init__()
        self.wars = UnitType(self, TyranidWarriorBrood, min_limit=1, max_limit=1)
        self.fexes = UnitType(self, CarnifexBrood, min_limit=3, max_limit=3)

    def check_rules(self):
        super(WreckerNode, self).check_rules()
        can_any = any([u.count_cannons() for u in self.wars.units])
        if can_any:
            self.error('The Tyranid Warrior Brood from this formation cannot take items from Basic Bio-cannons list')
        can_all = any([u.all_have_cannons() for u in self.fexes.units])
        if can_all:
            self.error('Carnifex models in this Formation cannot take options from Monstrous Bio-cannons list')


class TyrantNode(Formation):
    army_id = 'tyranids_v2_tyrant'
    army_name = 'Tyrant Node'

    class ThreeGuards(GuardBrood):
        def __init__(self, parent):
            super(TyrantNode.ThreeGuards, self).__init__(parent)
            self.models.min = 3
            self.models.cur = 3

    def __init__(self):
        super(TyrantNode, self).__init__()
        self.tyrant = UnitType(self, HiveTyrant, min_limit=1, max_limit=1)
        UnitType(self, self.ThreeGuards, min_limit=1, max_limit=1)
        UnitType(self, Venomethrope, min_limit=1, max_limit=1)

    def check_rules(self):
        super(TyrantNode, self).check_rules()
        if any([u.opt.wings.value for u in self.tyrant.units]):
            self.error('The Hive Tyrant cannot be equipped with Wings biomorph')


class Subterranean(Formation):
    army_id = 'tyranids_v2_subterranean'
    army_name = 'Subterranean Swarm'

    def __init__(self):
        super(Subterranean, self).__init__()
        UnitType(self, TrygonPrime, min_limit=1, max_limit=1)
        UnitType(self, Mawloc, min_limit=1, max_limit=1)
        UnitType(self, Trygon, min_limit=1, max_limit=1)
        UnitType(self, RavenerBrood, min_limit=3, max_limit=3)


class LivingTide(CompositeFormation):
    army_id = 'tyranids_v2_livetide'
    army_name = 'Living Tide'

    def __init__(self):
        super(LivingTide, self).__init__()
        Detachment.build_detach(self, TyrantNode, min_limit=1, max_limit=1)
        Detachment.build_detach(self, SynapricSwarm, min_limit=1, max_limit=1)
        Detachment.build_detach(self, EndlessSwarm, min_limit=3, max_limit=3)
        Detachment.build_detach(self, WreckerNode, min_limit=1, max_limit=1)
        Detachment.build_detach(self, SkyblightSwarm, min_limit=1, max_limit=1)


class PhodiaSwarm(Formation):
    army_id = 'tyranids_v2_phodia'
    army_name = 'Phodian Annihilation Swarm'

    def __init__(self):
        super(PhodiaSwarm, self).__init__()
        UnitType(self, CryptusChildren, min_limit=1, max_limit=1)
        UnitType(self, PhodianWarriors, min_limit=1, max_limit=1)
        UnitType(self, BeastOfPhodia, min_limit=1, max_limit=1)


class HiveVanguard(Formation):
    army_id = 'tyranids_v2_vanguard'
    army_name = 'Hive Vanguard'

    def __init__(self):
        super(HiveVanguard, self).__init__()
        UnitType(self, HiveTyrant, min_limit=1, max_limit=1)
        UnitType(self, TyranidWarriorBrood, min_limit=1, max_limit=1)
        UnitType(self, Gargoyle, min_limit=1, max_limit=1)


class TyranidsV2CAD(TyranidsV2Base, CombinedArmsDetachment):
    army_id = 'tyranids_v2_cad'
    army_name = 'Tyranids (Combined arms detachment)'


class TyranidsV2AD(TyranidsV2Base, AlliedDetachment):
    army_id = 'tyranids_v2_ad'
    army_name = 'Tyranids (Allied detachment)'


class TyranidsV2PA(TyranidsV2Base, PlanetstrikeAttacker):
    army_id = 'tyranids_v2_pa'
    army_name = 'Tyranids (Planetstrike attacker detachment)'


class TyranidsV2PD(TyranidsV2Base, PlanetstrikeDefender):
    army_id = 'tyranids_v2_pd'
    army_name = 'Tyranids (Planetstrike defender detachment)'


class TyranidsV2SA(TyranidsV2Base, SiegeAttacker):
    army_id = 'tyranids_v2_sa'
    army_name = 'Tyranids (Siege War attacker detachment)'


class TyranidsV2SD(TyranidsV2Base, SiegeDefender):
    army_id = 'tyranids_v2_sd'
    army_name = 'Tyranids (Siege War defender detachment)'


class HiveFleet(Wh40kBase, TyranidsV2Common):
    army_id = 'tyranids_v2_hive'
    army_name = 'Tyranids (Hive Fleet Detachment)'

    def __init__(self):
        from builder.games.wh40k.escalation.tyranids import LordsOfWar
        self.hq = HQ(parent=self)
        self.hq.max = 3
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.troops.min, self.troops.max = (3, 9)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(HiveFleet, self).__init__(
            sections=[self.hq, self.elites, self.troops, self.fast, self.heavy, LordsOfWar(parent=self)]
        )


class TyranidsV2KillTeam(Wh40kKillTeam):
    army_id = 'tyranids_v2_kt'
    army_name = 'Tyranids'

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(TyranidsV2KillTeam.KTElites, self).__init__(parent)
            UnitType(self, HiveGuard)
            UnitType(self, Zoanthrope)
            UnitType(self, Venomethrope)
            UnitType(self, Lictor)
            UnitType(self, Pyrovore)

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(TyranidsV2KillTeam.KTFastAttack, self).__init__(parent)
            UnitType(self, TyranidShrikeBrood)
            UnitType(self, RavenerBrood)
            UnitType(self, SkySlasherBrood)
            UnitType(self, Gargoyle)
            UnitType(self, SporeMine)

    def __init__(self):
        super(TyranidsV2KillTeam, self).__init__(
            Troops(self), self.KTElites(self),
            self.KTFastAttack(self)
        )


faction = 'Tyranids'


class TyranidsV2(Wh40k7ed, Wh40kImperial):
    army_id = 'tyranids_v2'
    army_name = 'Tyranids'
    faction = faction

    def __init__(self):
        super(TyranidsV2, self).__init__([TyranidsV2CAD, HiveFleet, HypertoxicNode,
                                          NeuralNode, SkyblightSwarm, Sporefield, SkytyrantSwarm, Skytide,
                                          ForestBrood, Manufactorum, Asassin, HuntingPack,
                                          BioBombs, IncubatorNode, LivingArtillery,
                                          SynapricSwarm, EndlessSwarm, BioblastNode,
                                          WreckerNode, TyrantNode, Subterranean, LivingTide,
                                          PhodiaSwarm, HiveVanguard])



class TyranidsV2Missions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'tyranids_v2_mis'
    army_name = 'Tyranids'
    faction = faction

    def __init__(self):
        super(TyranidsV2Missions, self).__init__([TyranidsV2CAD, TyranidsV2PA, TyranidsV2PD,
                                                  TyranidsV2SA, TyranidsV2SD, HiveFleet, HypertoxicNode,
                                                  NeuralNode, SkyblightSwarm, Sporefield, SkytyrantSwarm, Skytide,
                                                  ForestBrood, Manufactorum, Asassin, HuntingPack,
                                                  BioBombs, IncubatorNode, LivingArtillery,
                                                  SynapricSwarm, EndlessSwarm, BioblastNode,
                                                  WreckerNode, TyrantNode, Subterranean, LivingTide,
                                                  PhodiaSwarm, HiveVanguard])


detachments = [
    TyranidsV2CAD,
    TyranidsV2AD,
    TyranidsV2PA,
    TyranidsV2PD,
    TyranidsV2SA,
    TyranidsV2SD,
    HiveFleet,
    HypertoxicNode,
    NeuralNode,
    SkyblightSwarm,
    Sporefield,
    SkytyrantSwarm,
    Skytide,
    ForestBrood,
    Manufactorum,
    Asassin,
    HuntingPack,
    BioBombs,
    IncubatorNode,
    LivingArtillery,
    SynapricSwarm,
    EndlessSwarm,
    BioblastNode,
    WreckerNode,
    TyrantNode,
    Subterranean,
    LivingTide,
    PhodiaSwarm,
    HiveVanguard
]


unit_types = [
    HiveTyrant, TheSwarmlord, GuardBrood, OldOneEye,
    Tervigon, TyranidPrime, Deathleaper, HiveGuard, Zoanthrope,
    Venomethrope, Lictor, Haruspex, Pyrovore, Maleceptor,
    TyranidWarriorBrood, Genestealer, Termagant, Hormagant, Ripper,
    MucolidSpores, CryptusChildren, PhodianWarriors,
    TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle,
    Harpy, HiveCrone, SporeMine, CarnifexBrood, Biovore, Trygon,
    TrygonPrime, Mawloc, Exocrine, Tyrannofex, Toxicrene, Tyrannocyte,
    Sporocyst, BeastOfPhodia
]
