__author__ = 'Denis Romanow'

from builder.core2 import OneOf, OptionsList, Gear


class Devourer(OneOf):
    def __init__(self, parent, name):
        super(Devourer, self).__init__(parent, name=name)
        self.variant('Devourer', 0)


#Basic Bio-weapons
#A model may replace its devourer with one of the following:

class BasicBioWeapons(OneOf):
    def __init__(self, parent, name):
        super(BasicBioWeapons, self).__init__(parent, name=name)
        self.variant('Scything talons', 0)
        self.variant('Spinefists', 0)
        self.variant('Deathspitter', 5)


#Basic Bio-cannons
#A model may replace its devourer with one of the following:

class BasicBioCannons(OneOf):
    def __init__(self, parent, name):
        super(BasicBioCannons, self).__init__(parent, name=name)
        self.barbed = self.variant('Barbed strangler', 10)
        self.venom = self.variant('Venom cannon', 10)

    def has_heavy(self):
        return self.cur in [self.barbed, self.venom]


class MonstrousWeapon(OneOf):
    def __init__(self, parent, name):
        super(MonstrousWeapon, self).__init__(parent, name=name)

    def get_model_unique_gear(self):
        return []


class ScythingTalons(MonstrousWeapon):
    def __init__(self, parent, name):
        super(ScythingTalons, self).__init__(parent, name=name)
        self.variant('Scything Talons', 0)


#Monstrous Bio-cannons
#A model may replace any pair of scything talons with one of the following:

class MonstrousBioCannons(MonstrousWeapon):
    def __init__(self, parent, name):
        super(MonstrousBioCannons, self).__init__(parent, name=name)
        self.cannons = [
            self.variant('Twin-linked deathspitter', 5),
            self.variant('Twin-linked devourer with brainleech worms', 15)
        ]
        self.str_cannon = self.variant('Stranglethorn cannon', 15)
        self.heavy_venom = self.variant('Heavy venom cannon', 20)
        self.cannons += [self.str_cannon, self.heavy_venom]

    def get_model_unique_gear(self):
        return super(MonstrousBioCannons, self).get_model_unique_gear() + [self.str_cannon, self.heavy_venom]

    def is_monstrous_cannon(self):
        return self.cur in self.cannons


#Melee Bio-weapons
#A model may replace any pair of scything talons with one of the following:

class MeleeBioWeapons(OneOf):
    def __init__(self, parent, name):
        super(MeleeBioWeapons, self).__init__(parent, name=name)
        self.variant('Rending claws', 5)
        self.variant('Boneswords', 15)
        self.variant('Lash whip and bonesword', 20, gear=[Gear('Lash whip'), Gear('Bonesword')])


#Biomorphs
#A model may take up to one of each of the following:

class Biomorphs(OptionsList):
    def __init__(self, parent, acid_blood=True):
        super(Biomorphs, self).__init__(parent, name='Biomorphs')
        self.sacs = self.variant('Toxin sacs', points=10)
        if acid_blood:
            self.variant('Acid blood', points=15)
        self.variant('Adrenal glands', points=15)
        self.variant('Regeneration', points=30)


#Thorax Biomorphs
#A model may take up to one of the following:

class ThoraxBiomorphs(OptionsList):
    def __init__(self, parent):
        super(ThoraxBiomorphs, self).__init__(parent, name='Thorax Biomorphs', limit=1)
        self.variant('Electroshock grubs', points=10)
        self.variant('Desiccator larvae', points=10)
        self.variant('Shreddershard beetles', points=10)


#Tyranid Bio-artefacts
#A model may replace any pair of scything talons with one of the following.
#Only one of each Tyranid Bio-artefact may be taken per army.

class BioArtefacts(OptionsList):
    def __init__(self, parent):
        super(BioArtefacts, self).__init__(parent, name='Bio-artefacts')
        self.variant('The Norn Crown', points=40)
        self.variant('The Ymgarl Factor', points=40)

    def get_unique_gear(self):
        return self.description


class BioArtefactsWeapons(MonstrousWeapon):
    def __init__(self, parent, name):
        super(BioArtefactsWeapons, self).__init__(parent, name=name)
        self.maw_claws = self.variant('The Maw-claws of Thyrax', 10)
        self.miasma = self.variant('The Miasma Cannon', 25)
        self.reaper = self.variant('The Reaper of Obliterax', 45)

    def get_model_unique_gear(self):
        return super(BioArtefactsWeapons, self).get_model_unique_gear() + [self.maw_claws, self.miasma, self.reaper]

    def get_unique_gear(self):
        if self.cur in [self.maw_claws, self.miasma, self.reaper]:
            return self.description
        return []
