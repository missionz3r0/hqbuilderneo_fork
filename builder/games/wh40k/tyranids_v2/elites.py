__author__ = 'dante'

from builder.core2 import *
from builder.games.wh40k.roster import Unit
from .armory import Biomorphs


class HiveGuard(Unit):
    type_name = 'Hive Guard Brood'
    type_id = 'hive_guard_brood_v1'

    model_name = 'Hive Guard'
    model_points = 55
    model = UnitDescription(model_name, points=model_points)

    class Options(OptionsList):
        def __init__(self, parent):
            super(HiveGuard.Options, self).__init__(parent, name='Options')
            self.variant('Toxin sacs', points=3, per_model=True)
            self.variant('Adrenal glands', points=5, per_model=True)

        @property
        def points(self):
            return super(HiveGuard.Options, self).points * self.parent.models.cur

    class GuardCount(Count):
        @property
        def description(self):
            return [HiveGuard.model.clone().add(Gear('Impaler cannon')).set_count(self.cur - self.parent.shock.cur)]

    def __init__(self, parent):
        super(HiveGuard, self).__init__(parent)
        self.models = self.GuardCount(self, self.model_name, 1, 3, self.model_points, per_model=True)
        self.shock = Count(
            self, name='Shockcannon', points=5, min_limit=0, max_limit=1, per_model=True,
            gear=self.model.clone().add(Gear('Shockcannon'))
        )
        self.Options(self)

    def check_rules(self):
        super(HiveGuard, self).check_rules()
        self.shock.max = self.models.cur

    def get_count(self):
        return self.models.cur


class Zoanthrope(Unit):
    type_name = 'Zoanthrope Brood'
    type_id = 'zoanthrope_v1'

    model_name = 'Zoanthrope'
    model_points = 50
    model = UnitDescription(model_name, points=model_points)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Zoanthrope.Options, self).__init__(parent, 'Options')
            self.variant('Neurothrope', 25, gear=[])

    def __init__(self, parent):
        super(Zoanthrope, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 1, 6, self.model_points, per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur

    def check_rules(self):
        super(Zoanthrope, self).check_rules()
        self.opt.used = self.opt.visible = self.models.cur >= 3

    def build_description(self):
        desc = UnitDescription(self.name, self.points)
        diff = 1 if self.has_neurothrope() else 0
        desc.add(self.model.clone().set_count(self.models.cur - diff))
        if diff > 0:
            desc.add(UnitDescription(name='Neurothrope', points=self.model_points + self.opt.points))
        return desc

    def has_neurothrope(self):
        return self.opt.used and self.opt.any

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Zoanthrope, self).build_statistics())


class Venomethrope(Unit):
    type_name = 'Venomethrope Brood'
    type_id = 'venomethrope_v1'

    model_name = 'Venomethrope'
    model_points = 45

    def __init__(self, parent):
        super(Venomethrope, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 1, 3, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name, points=self.model_points,
                                 options=[Gear('Lash whips'), Gear('Toxic miasma')])
        )

    def get_count(self):
        return self.models.cur


class Lictor(Unit):
    type_name = 'Lictor Brood'
    type_id = 'lictor_v1'

    model_name = 'Lictor'
    model_points = 50

    def __init__(self, parent):
        super(Lictor, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 1, 3, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name, points=self.model_points,
                                 options=[Gear('Rending claws'), Gear('Scything talons'), Gear('Flesh hooks')])
        )

    def get_count(self):
        return self.models.cur


class Haruspex(Unit):
    type_name = 'Haruspex'
    type_id = 'haruspex_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Haruspex.Options, self).__init__(parent, name='Weapon')
            self.variant('Thresher scythe', points=10)

    def __init__(self, parent):
        super(Haruspex, self).__init__(parent, name='Haruspex', points=160, gear=[
            Gear('Grasping tongue'),
            Gear('Crushing claws'),
            Gear('Acid blood'),
        ])
        self.Options(self)
        Biomorphs(self, acid_blood=False)


class Pyrovore(Unit):
    type_name = 'Pyrovore Brood'
    type_id = 'pyrovore_v1'

    model_name = 'Pyrovore'
    model_points = 40

    def __init__(self, parent):
        super(Pyrovore, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 1, 3, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name, points=self.model_points,
                                 options=[Gear('Flamespurt'), Gear('Acid blood'), Gear('Acid maw')])
        )

    def get_count(self):
        return self.models.cur


class Maleceptor(Unit):
    type_name = 'Maleceptor'
    type_id = 'maleceptor_v1'

    def __init__(self, parent):
        super(Maleceptor, self).__init__(parent, self.type_name, 205,
                                         [Gear('Scything Talons')], static=True)

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Maleceptor, self).build_statistics())
