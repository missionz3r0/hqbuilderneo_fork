__author__ = 'Ivan Truskov'

from builder.core2 import Gear, Count, UnitDescription, OneOf
from .fast import Transport
from builder.games.wh40k.roster import Unit


class Warriors(Unit):
    type_name = 'Warriors'
    type_id = 'warriors_v3'

    def __init__(self, parent):
        super(Warriors, self).__init__(parent)
        self.models = Count(self, 'Necron Warrior', 10, 20, 13, True,
                            gear=UnitDescription('Necron Warrior', 13,
                                                 options=[Gear('Gauss flayer')]))
        self.transport = Transport(self, True)

    def get_count(self):
        return self.models.cur

    def build_statistics(self):
        return self.note_transport(super(Warriors, self).build_statistics())


class Immortals(Unit):
    type_name = 'Immortals'
    type_id = 'immortals_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Immortals.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Gauss blaster', 0)
            self.variant('Tesla carbine', 0)

    def __init__(self, parent):
        super(Immortals, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Immortal', 5, 10, 17, True)
        self.wep = self.Weapon(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Immortal', 17,
                                self.models.cur, self.wep.description)
        desc.add(model)
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Immortals, self).build_statistics())
