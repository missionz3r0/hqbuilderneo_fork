__author__ = 'Ivan Truskov'

from builder.games.wh40k.section import Formation, UnitType, Detachment
from .hq import Overlord, Lord, Cryptek, DestroyerLord, CommandBarge,\
    Illuminor, Orikan, Nemesor, Vargard, Anrakyr, Trazyn
from .elites import Lychguard, Deathmarks, Praetorians,\
    Stalkers, Nightbringer, Deciever
from .troops import Warriors, Immortals
from .fast import TombBlades, Destroyers, NightScythe, GhostArk, Wraiths,\
    Scarabs
from .heavy import HeavyDestroyers, Spyder, DoomScythe,\
    AnnihilationBarge, DoomsdayArk, TranscendentCtan, Monolith,\
    DoomScythes
from .lords import Obelisk, Imotekh


class BaseFormation(Formation):
    @property
    def supplement(self):
        return None


class ReclamationLegion(BaseFormation):
    army_id = 'necron_reclamation_v3'
    army_name = 'Reclamation Legion'

    def __init__(self):
        super(ReclamationLegion, self).__init__()
        overlord = UnitType(self, Overlord)
        nemesor = UnitType(self, Nemesor)
        trazyn = UnitType(self, Trazyn)
        traveller = UnitType(self, Anrakyr)
        barge = UnitType(self, CommandBarge)
        self.add_type_restriction([overlord, nemesor, trazyn, traveller, barge], 1, 1)
        UnitType(self, Lychguard, max_limit=2)
        UnitType(self, Immortals, min_limit=1, max_limit=4)
        UnitType(self, Warriors, min_limit=2, max_limit=8)
        UnitType(self, TombBlades, min_limit=1, max_limit=3)
        UnitType(self, Monolith, max_limit=3)


class JudicatorBatallion(BaseFormation):
    army_id = 'necron_judicator_v3'
    army_name = 'Judicator batallion'

    def __init__(self):
        super(JudicatorBatallion, self).__init__()
        UnitType(self, Stalkers, min_limit=1, max_limit=1)
        UnitType(self, Praetorians, min_limit=2, max_limit=2)


class DestroyerCult(BaseFormation):
    army_id = 'necron_destroyers_v3'
    army_name = 'Destroyer Cult'

    def __init__(self):
        super(DestroyerCult, self).__init__()
        UnitType(self, DestroyerLord, min_limit=1, max_limit=1)
        self.destroyers = UnitType(self, Destroyers, min_limit=3, max_limit=3)
        UnitType(self, HeavyDestroyers, max_limit=1)

    def check_rules(self):
        super(DestroyerCult, self).check_rules()
        if any([u.count < 3 for u in self.destroyers.units]):
            self.errors.append('All Destroyer Units must include at least 3 models')


class DeathbringerFlight(BaseFormation):
    army_id = 'necron_deathbringer_v3'
    army_name = 'Deathbringer Flight'

    def __init__(self):
        super(DeathbringerFlight, self).__init__()
        UnitType(self, DoomScythe, min_limit=2, max_limit=4)


class LivingTomb(BaseFormation):
    army_id = 'necron_tomb_v3'
    army_name = 'Living Tomb'

    def __init__(self):
        super(LivingTomb, self).__init__()
        UnitType(self, Obelisk, min_limit=1, max_limit=1)
        UnitType(self, Monolith, max_limit=2)


class AnnihilationNexus(BaseFormation):
    army_id = 'necron_nexus_v3'
    army_name = 'Annihilation Nexus'

    def __init__(self):
        super(AnnihilationNexus, self).__init__()
        UnitType(self, DoomsdayArk, min_limit=1, max_limit=1)
        UnitType(self, AnnihilationBarge, min_limit=2, max_limit=2)


class CanoptekHarvest(BaseFormation):
    army_id = 'necron_harvest_v3'
    army_name = 'Canoptek Harvest'

    def __init__(self):
        super(CanoptekHarvest, self).__init__()
        UnitType(self, Spyder, min_limit=1, max_limit=1)
        UnitType(self, Scarabs, min_limit=1, max_limit=1)
        UnitType(self, Wraiths, min_limit=1, max_limit=1)


class RoyalCourt(BaseFormation):
    army_id = 'necron_court_v3'
    army_name = 'Royal Court'

    def __init__(self):
        super(RoyalCourt, self).__init__()
        overlord = UnitType(self, Overlord)
        stormlord = UnitType(self, Imotekh)
        nemesor = UnitType(self, Nemesor)
        trazyn = UnitType(self, Trazyn)
        traveller = UnitType(self, Anrakyr)
        barge = UnitType(self, CommandBarge)
        self.add_type_restriction([overlord, stormlord, nemesor, trazyn, traveller, barge], 1, 1)
        lord = UnitType(self, Lord)
        vargard = UnitType(self, Vargard)
        self.add_type_restriction([lord, vargard], 1, 3)
        cryptek = UnitType(self, Cryptek)
        seras = UnitType(self, Illuminor)
        orikan = UnitType(self, Orikan)
        self.add_type_restriction([cryptek, seras, orikan], 1, 3)


class MephritFormation(Formation):
    @property
    def supplement(self):
        return 'mephrit'


class Conclave(MephritFormation):
    army_id = 'necron_v3_conclave'
    army_name = 'Conclave of the Burning One'

    def __init__(self):
        super(Conclave, self).__init__()
        dec = UnitType(self, Deciever)
        night = UnitType(self, Nightbringer)
        trans = UnitType(self, TranscendentCtan)
        self.add_type_restriction([dec, night, trans], 1, 1)
        UnitType(self, Cryptek, min_limit=2, max_limit=2)


class RoyalDecurion(MephritFormation):
    army_id = 'necron_v3_zaratusra'
    army_name = 'Zarathusa\'s Royal Decurion'

    def __init__(self):
        super(RoyalDecurion, self).__init__()
        UnitType(self, Overlord, min_limit=1, max_limit=1)
        UnitType(self, Immortals, min_limit=1, max_limit=1)
        UnitType(self, Warriors, min_limit=2, max_limit=2)
        UnitType(self, GhostArk, min_limit=1, max_limit=1)
        UnitType(self, DoomScythe, min_limit=1, max_limit=1)
        UnitType(self, Praetorians, min_limit=1, max_limit=1)
        UnitType(self, Stalkers.SingleStalker, min_limit=1, max_limit=1)
        UnitType(self, Deathmarks, min_limit=1, max_limit=1)
        UnitType(self, Wraiths, min_limit=2, max_limit=2)


class StrategicDecurion(MephritFormation):
    army_id = 'necron_v3_anrakyr'
    army_name = 'Anrakyr\'s Strategic Decurion'

    def __init__(self):
        super(StrategicDecurion, self).__init__()
        UnitType(self, Anrakyr, min_limit=1, max_limit=1)
        UnitType(self, Immortals, min_limit=1, max_limit=1)
        UnitType(self, Warriors, min_limit=2, max_limit=2)
        UnitType(self, GhostArk, min_limit=1, max_limit=1)
        UnitType(self, DoomScythe, min_limit=1, max_limit=1)
        UnitType(self, Deathmarks, min_limit=1, max_limit=1)


class Guardians(MephritFormation):
    army_id = 'necron_v3_perdita'
    army_name = 'The Guardians of Perdita'

    def __init__(self):
        super(Guardians, self).__init__()
        Detachment.build_detach(self, Conclave, min_limit=1, max_limit=1)
        Detachment.build_detach(self, RoyalDecurion, min_limit=1, max_limit=1)
        Detachment.build_detach(self, StrategicDecurion, min_limit=2, max_limit=2)


class MephritResurgence(MephritFormation):
    army_id = 'necron_v3_resurgence'
    army_name = 'Mephrit Dynasty Resurgence Decurion'

    def __init__(self):
        super(MephritResurgence, self).__init__()
        UnitType(self, Warriors, min_limit=2, max_limit=2)
        UnitType(self, Immortals, min_limit=2, max_limit=2)
        UnitType(self, Monolith, min_limit=1, max_limit=1)


class RetributionPhalanx(BaseFormation):
    army_id = 'necron_v3_retribution'
    army_name = 'Retribution Phalanx'

    def __init__(self):
        super(RetributionPhalanx, self).__init__()
        UnitType(self, Overlord, min_limit=1, max_limit=1)
        UnitType(self, Warriors, min_limit=1, max_limit=1)
        UnitType(self, Scarabs, min_limit=1, max_limit=1)
        UnitType(self, Stalkers.SingleStalker, min_limit=1, max_limit=1)


class OppressorFlight(BaseFormation):
    army_id = 'necron_v3_oppressor'
    army_name = 'Oppressor Flight'

    def __init__(self):
        super(OppressorFlight, self).__init__()

        class Doom(DoomScythes):
            unit_min = 2
            unit_max = 3

        UnitType(self, Doom, min_limit=1, max_limit=1)
        UnitType(self, NightScythe, min_limit=1, max_limit=1)
