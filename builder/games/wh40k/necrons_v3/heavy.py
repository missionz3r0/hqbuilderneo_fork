__author__ = 'Ivan Truskov'

from builder.core2 import Gear, Count, UnitDescription, OneOf,\
    OptionsList, ListSubUnit, UnitList
from builder.games.wh40k.unit import Unit, Squadron


class HeavyDestroyers(Unit):
    type_name = 'Heavy Destroyers'
    type_id = 'heavy_destroyers_v3'

    def __init__(self, parent):
        super(HeavyDestroyers, self).__init__(parent)
        self.models = Count(self, 'Heavy Destroyer', 1, 3, 50, True,
                            gear=UnitDescription('Heavy Destroyer', 50,
                                                 options=[Gear('Heavy gauss cannon')]))

    def get_count(self):
        return self.models.cur


class Spyder(Unit):
    type_name = 'Canoptek Spyder'
    type_id = 'spyder_v3'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Spyder.Options, self).__init__(parent, 'Options')
            self.variant('Fabricator claw array', 5)
            self.variant('Gloom prism', 10)
            self.variant('Twin-linked particle beamer', 10)

    def __init__(self, parent):
        super(Spyder, self).__init__(parent, self.type_name, 50)
        self.Options(self)


class Spyders(Unit):
    type_name = 'Canoptek Spyders'
    type_id = 'spyders_v3'

    class Spyder(Spyder, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Spyders, self).__init__(parent)
        self.models = UnitList(self, self.Spyder, 1, 3)

    def get_count(self):
        return self.models.count


class DoomScythe(Unit):
    type_name = 'Doom Scythe'
    type_id = 'doom_scythe_v3'

    def __init__(self, parent):
        super(DoomScythe, self).__init__(parent, self.type_name, 160, gear=[
            Gear('Death ray'), Gear('Twin-linked tesla destructor')],
            static=True)


class DoomScythes(Squadron):
    type_name = 'Doom Scythes'
    type_id = 'doom_scythes_v3'
    unit_class = DoomScythe
    unit_max = 4


class Monolith(Unit):
    type_name = 'Monolith'
    type_id = 'monolith_v3'

    def __init__(self, parent):
        super(Monolith, self).__init__(parent, self.type_name, 200, gear=[
            Gear('Gauss flux arc', count=4),
            Gear('Particle whip'),
            Gear('Eternity gate')],
            static=True)


class AnnihilationBarge(Unit):
    type_name = 'Annihilation Barge'
    type_id = 'ann_barge_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(AnnihilationBarge.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Gauss cannon', 0)
            self.variant('Tesla cannon', 0)

    def __init__(self, parent):
        super(AnnihilationBarge, self).__init__(parent, self.type_name, 120,
                                                gear=[Gear('Twin-linked tesla destructor')])
        self.wep = self.Weapon(self)


class DoomsdayArk(Unit):
    type_name = 'Doomsday Ark'
    type_id = 'doomsday_v3'

    def __init__(self, parent):
        super(DoomsdayArk, self).__init__(parent, self.type_name, 170, gear=[
            Gear('Gauss flayer array', count=2),
            Gear('Doomsday cannon'),
            Gear('Quantum shielding')],
            static=True)


class TranscendentCtan(Unit):
    type_name = 'Transcendent C\'tan'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/necrons/profiles#TOC-Transcendent-C-tan'
    type_id = 'ctan_v3'

    def __init__(self, parent):
        super(TranscendentCtan, self).__init__(parent, self.type_name, 250, gear=[
            Gear('Powers of the C\'tan')],
            static=True)
