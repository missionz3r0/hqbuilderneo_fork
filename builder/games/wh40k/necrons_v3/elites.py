__author__ = 'Ivan Truskov'

from builder.core2 import Gear, Count, UnitDescription, OneOf,\
    UnitList, ListSubUnit
from builder.games.wh40k.roster import Unit
from .fast import Transport


class Lychguard(Unit):
    type_name = 'Lychguard'
    type_id = 'lychguard_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Lychguard.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Warscythe', 0)
            self.variant('Hyperphase sword and dispersion shield', 5,
                         gear=[Gear('Hyperphase sword'), Gear('Dispersion shield')])

    def __init__(self, parent):
        super(Lychguard, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Lychguard', 5, 10, 25, True)
        self.wep = self.Weapon(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return self.models.points + self.transport.points +\
            self.models.cur * self.wep.points

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Lychguard', 25 + self.wep.points,
                                self.models.cur, self.wep.description)
        desc.add(model)
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Lychguard, self).build_statistics())


class Deathmarks(Unit):
    type_name = 'Deathmarks'
    type_id = 'deathmarks_v3'

    def __init__(self, parent):
        super(Deathmarks, self).__init__(parent)
        self.models = Count(self, 'Deathmark', 5, 10, 18, True,
                            gear=UnitDescription('Deathmark', 18,
                                                 options=[Gear('Synaptic disintegrator')]))
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur

    def build_statistics(self):
        return self.note_transport(super(Deathmarks, self).build_statistics())


class FlayedOnes(Unit):
    type_name = 'Flayed Ones'
    type_id = 'flayed_v3'

    def __init__(self, parent):
        super(FlayedOnes, self).__init__(parent)
        self.models = Count(self, 'Flayed One', 5, 20, 13, True,
                            gear=UnitDescription('Flayed One', 13,
                                                 options=[Gear('Flayer claw', count=2)]))

    def get_count(self):
        return self.models.cur


class Praetorians(Unit):
    type_name = 'Triarch Praetorians'
    type_id = 'praetorians_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Praetorians.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Rod of covenant', 0)
            self.variant('Voidblade and particle caster', 0, gear=[
                Gear('Voidblade'), Gear('Particle caster')])

    def __init__(self, parent):
        super(Praetorians, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Triarch Praetorian', 5, 10, 28, True)
        self.wep = self.Weapon(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Triarch Praetorian', 28,
                                self.models.cur, self.wep.description)
        desc.add(model)
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Praetorians, self).build_statistics())


class Stalkers(Unit):
    type_name = 'Triarch Stalkers'
    type_id = 'stalkers_v3'

    class SingleStalker(Unit):
        type_name = 'Triarch Stalker'
        type_id = 'stalker_single_v3'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Stalkers.SingleStalker.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Heat ray', 0)
                self.variant('Particle shredder', 5)
                self.variant('Twin-linked heavy gauss cannon', 10)

        def __init__(self, parent):
            super(Stalkers.SingleStalker, self).__init__(parent, 'Triarch Stalker',
                                                   125, [Gear('Quantum shielding')])
            self.Weapon(self)

    class Stalker(SingleStalker, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Stalkers, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Stalker, 1, 3)

    def get_count(self):
        return self.models.count


class Nightbringer(Unit):
    type_name = 'C\'tan Shard of the Nightbringer'
    type_id = 'nightbringer_v3'

    def __init__(self, parent):
        super(Nightbringer, self).__init__(parent, self.type_name, 240,
                                           [Gear('Powers of the C\'tan')],
                                           unique=True, static=True)


class Deciever(Unit):
    type_name = 'C\'tan Shard of the Deciever'
    type_id = 'deciever_v3'

    def __init__(self, parent):
        super(Deciever, self).__init__(parent, self.type_name, 240,
                                       [Gear('Powers of the C\'tan')],
                                       unique=True, static=True)
