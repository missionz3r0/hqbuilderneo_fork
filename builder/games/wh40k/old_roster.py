from builder.core2 import section
from builder.core2 import Roster, Unit, SubRoster, OptionsList, OneOf, UnitType

__author__ = 'Denis Romanov'


class Ally(section.Section):
    SOB, BA, CD, CSM, DA, DE, ELD, GK, IG, IK, INQ, LOD, NEC, ORCS, SM, SW, TAU, TYR, MT, OA = list(range(20))

    matrix = {
        SOB: [OA, BA, DA, DE, ELD, GK, IG, NEC, SM, SW, TAU, INQ, LOD, IK, MT],
        BA: [OA, SOB, DA, ELD, GK, IG, NEC, SM, SW, TAU, INQ, LOD, IK, MT],
        CD: [CSM, DE, IG, ORCS, TAU],
        CSM: [CD, DE, IG, NEC, ORCS, TAU, CSM],
        DA: [OA, SOB, BA, ELD, GK, IG, NEC, ORCS, SM, SW, TAU, INQ, LOD, IK, MT],
        DE: [SOB, CD, CSM, ELD, GK, IG, ORCS, SM, SW, TAU, INQ, LOD, IK, MT],
        ELD: [SOB, DA, DE, ELD, GK, IG, ORCS, SM, SW, TAU, INQ, LOD, IK, MT],
        GK: [OA, SOB, BA, DA, DE, ELD, IG, NEC, ORCS, SM, SW, TAU, INQ, LOD, IK, MT],
        IG: [OA, SOB, BA, CD, CSM, DA, DE, ELD, GK, NEC, ORCS, SM, SW, TAU, INQ, LOD, IK, MT],
        IK: [OA, SOB, BA, DA, DE, ELD, GK, IG, INQ, SM, SW, TAU, MT],
        INQ: [OA, SOB, BA, DA, DE, ELD, GK, IG, SM, SW, TAU, LOD, IK, MT],
        NEC: [SOB, BA, CSM, DA, GK, IG, ORCS, SM, SW, TAU],
        ORCS: [SOB, CD, CSM, DA, DE, ELD, GK, IG, NEC, ORCS, SM, SW, TAU],
        SM: [OA, SOB, BA, DA, DE, ELD, GK, IG, IK, INQ, LOD, NEC, ORCS, SM, SW, TAU, IK, MT],
        SW: [OA, SOB, BA, DA, DE, ELD, GK, IG, IK, INQ, LOD, NEC, ORCS, SM, TAU, IK, MT],
        TAU: [SOB, BA, CD, CSM, DA, DE, ELD, GK, IG, IK, INQ, LOD, NEC, ORCS, SM, SW, TAU, IK, MT],
        LOD: [OA, SOB, BA, DA, DE, ELD, GK, IG, IK, INQ, SM, SW, TAU, IK, LOD, MT],
        MT: [OA, SOB, IG, BA, DA, DE, ELD, GK, SM, SW, TAU, INQ, LOD, IK],
        OA: [SOB, BA, DA, DE, ELD, GK, IG, INQ, SM, SW, MT],
    }

    def build_ally(self, ally_class, visible=True, slot=1):
        class AllyUnit(Unit):
            type_id = ally_class.army_id
            type_name = ally_class.army_name

            def __init__(self, parent):
                super(AllyUnit, self).__init__(parent=parent, unique=True)
                if issubclass(ally_class, Wh40k):
                    ally = ally_class(secondary=True)
                else:
                    ally = ally_class()
                ally.parent = self
                self.sub_roster = SubRoster(self, ally)

            def dump(self):
                data = super(AllyUnit, self).dump()
                data['detach_unit'] = True
                return data

        return UnitType(self, AllyUnit, visible=visible, slot=slot)

    def __init__(self, parent, ally_type):

        super(Ally, self).__init__(parent, 'ally', 'Secondary Detachment', min_limit=0, max_limit=1)
        ally = self.matrix[ally_type]

        # Space Marines
        if self.SM in ally:
            from builder.games.wh40k.obsolete.space_marines import SpaceMarines
            from builder.games.wh40k.obsolete.space_marines_v2 import SpaceMarinesV2
            self.build_ally(SpaceMarines, visible=False)
            self.build_ally(SpaceMarinesV2)

        # Blood Angels
        if self.BA in ally:
            from builder.games.wh40k.obsolete.blood_angels import BloodAngels
            self.build_ally(BloodAngels)

        # Dark Angels
        if self.DA in ally:
            from builder.games.wh40k.obsolete.dark_angels_v2 import DarkAngelsV2
            self.build_ally(DarkAngelsV2)

        # Space Wolves
        if self.SW in ally:
            from builder.games.wh40k.obsolete.space_wolves import SpaceWolves
            self.build_ally(SpaceWolves)

        # Grey Knights
        if self.GK in ally:
            from builder.games.wh40k.obsolete.grey_knights import GreyKnights
            from builder.games.wh40k.obsolete.grey_knights_v2 import GreyKnightsV2
            self.build_ally(GreyKnights, visible=False)
            self.build_ally(GreyKnightsV2)

        # Imperial Guard
        if self.IG in ally:
            from builder.games.wh40k.obsolete.imperial_guard import ImperialGuard
            from builder.games.wh40k.obsolete.imperial_guard_v2 import ImperialGuardV2
            from .astra_militarum import AstraMilitarum
            self.build_ally(ImperialGuard, visible=False)
            self.build_ally(ImperialGuardV2, visible=False)
            self.build_ally(AstraMilitarum)

        # Imperial Knights
        if self.IK in ally:
            from builder.games.wh40k.obsolete.imperial_knight import ImperialKnights
            self.build_ally(ImperialKnights, slot=0)

        #Officio Assasinorum
        if self.OA in ally:
            from builder.games.wh40k.officio_assasinorum import OfficioAssasinorum
            self.build_ally(OfficioAssasinorum, slot=0)

        # Militarum Tempestus
        if self.MT in ally:
            from .militarum_tempestus import MilitarumTempestusAD
            self.build_ally(MilitarumTempestusAD)

        # Adepta Sororitas
        if self.SOB in ally:
            from builder.games.wh40k.obsolete.adepta_sororitas import AdeptaSororitas
            from builder.games.wh40k.obsolete.adepta_sororitas_v2 import AdeptaSororitasV2
            self.build_ally(AdeptaSororitas, visible=False)
            self.build_ally(AdeptaSororitasV2)

        # Inquisition
        if self.INQ in ally:
            from .inquisition import InquisitionDetachment
            self.build_ally(InquisitionDetachment, slot=0)

        # Damned
        if self.LOD in ally:
            from .legion_of_damned import DamnedDetachment
            self.build_ally(DamnedDetachment, slot=0)

        # Tau
        if self.TAU in ally:
            from builder.games.wh40k.obsolete.tau import Tau, FarsightEnclaves
            from builder.games.wh40k.obsolete.tau_v2 import TauV2
            self.build_ally(Tau, visible=False)
            self.build_ally(FarsightEnclaves, visible=False)
            self.build_ally(TauV2)

        # Eldar
        if self.ELD in ally:
            from builder.games.wh40k.obsolete.eldar import Eldar, Iyanden
            from builder.games.wh40k.obsolete.corsairs import Corsairs
            from builder.games.wh40k.obsolete.eldar_v2 import EldarV2
            self.build_ally(Eldar, visible=False)
            self.build_ally(Iyanden, visible=False)
            self.build_ally(Corsairs)
            self.build_ally(EldarV2)

        # Dark Eldar
        if self.DE in ally:
            from builder.games.wh40k.obsolete.dark_eldar_v1 import DarkEldarV1
            self.build_ally(DarkEldarV1)

        # Orks
        if self.ORCS in ally:
            from builder.games.wh40k.obsolete.dreadmob import Dreadmob
            from builder.games.wh40k.obsolete.orks_v2 import OrksV2
            from builder.games.wh40k.obsolete.orks import Orks
            self.build_ally(Orks, visible=False)
            self.build_ally(OrksV2)
            self.build_ally(Dreadmob)

        # Necron
        if self.NEC in ally:
            from builder.games.wh40k.obsolete.necrons import Necrons
            from builder.games.wh40k.obsolete.necrons_v2 import NecronV2
            self.build_ally(Necrons, visible=False)
            self.build_ally(NecronV2)

        # Chaos Space Marines
        if self.CSM in ally:
            from builder.games.wh40k.obsolete.chaos_marines import BlackLegion
            from .chaos_marines_v2 import CsmV2
            self.build_ally(BlackLegion, visible=False)
            self.build_ally(CsmV2)

        # Chaos Daemons
        if self.CD in ally:
            from builder.games.wh40k.obsolete.chaos_daemons import ChaosDaemons
            from builder.games.wh40k.obsolete.chaos_daemons_v2 import ChaosDaemonsV2
            self.build_ally(ChaosDaemons, visible=False)
            self.build_ally(ChaosDaemonsV2)


class Wh40k(Roster):
    game_name = 'Warhammer 40k'
    imperial_armour = False
    base_tags = ['Warhammer 40k']

    @classmethod
    def post_process(cls):
        cls.base_tags = [cls.army_name] + cls.base_tags
        if cls.imperial_armour:
            cls.base_tags += ['Imperial armour']
            cls.game_name = '{0}: Imperial armour'.format(cls.game_name)

    class FocOptions(OneOf):
        def __init__(self, parent):
            super(Wh40k.FocOptions, self).__init__(parent=parent, name='Force organisation chart')
            self.custom = self.variant(name='Custom')
            self.std = self.variant(name='Battle-Forged')
            self.double = self.variant(name='Double')
            self.unlimited = self.variant(name='Unbound')
            self.ps_att = self.variant(name='Planet Strike Attacker\'s forces')
            self.ps_def = self.variant(name='Planet Strike Defender\'s forces')
            self.zm_att = self.variant(name='Zone Mortalis Attacker\'s forces')
            self.zm_def = self.variant(name='Zone Mortalis Defender\'s forces')
            self.zm_combat = self.variant(name='Zone Mortalis Combatant\'s forces')
            self.cur = self.std

    class UnitOptions(OptionsList):
        def __init__(self, parent, ia_units):
            self.ia_units = ia_units
            super(Wh40k.UnitOptions, self).__init__(parent=parent, name='Options')
            if not self.ia_units:
                self.visible = False
            self.ia = self.variant(name='Imperial armour')
            self.ia.value = True

        def check_rules(self):
            super(Wh40k.UnitOptions, self).check_rules()
            for u in self.ia_units:
                u.visible = self.ia.value

    def __init__(self, hq=None, elites=None, troops=None, fast=None, heavy=None, fort=None, ally=None, lords=None,
                 super_heavy=None, secondary=False, added=()):
        self.secondary = secondary

        self.hq = hq
        self.elites = elites
        self.troops = troops
        self.fast = fast
        self.heavy = heavy
        self.fortification = not secondary and fort
        self.lords = not secondary and lords
        self.super_heavy = super_heavy
        self.ally = not secondary and ally
        sections = [self.hq, self.elites, self.troops, self.fast, self.heavy] + \
            list(added) + [self.fortification, self.lords, self.super_heavy, self.ally]

        super(Wh40k, self).__init__(sections, limit=1850)
        if not self.secondary:
            self.foc = self.FocOptions(self)
        self.unit_options = self.UnitOptions(
            self, ia_units=sum((s.ia_units for s in sections if s and hasattr(s, 'ia_units')), [])
        )

    @staticmethod
    def move_units(move, from_units, to_units):
        to_units.active = move
        from_units.active = not move

        if move:
            to_units.parent.move_units(from_units.units)
        else:
            from_units.parent.move_units(to_units.units)

    def check_unit_limit(self, count, units, message):
        units.active = count > 0
        if count < units.count:
            self.error(message)

    @property
    def ia_enabled(self):
        return self.unit_options.ia.value

    def check_rules_chain(self):
        if not self.secondary:
            if self.foc.cur == self.foc.unlimited:
                self.unlimited_foc()
            elif self.foc.cur == self.foc.std:
                self.std_foc()
            elif self.foc.cur == self.foc.ps_att:
                self.ps_att_foc()
            elif self.foc.cur == self.foc.ps_def:
                self.ps_def_foc()
            elif self.foc.cur == self.foc.zm_att:
                self.zm_att_foc()
            elif self.foc.cur == self.foc.zm_def:
                self.zm_def_foc()
            elif self.foc.cur == self.foc.zm_combat:
                self.zm_combat_foc()
        super(Wh40k, self).check_rules_chain()

    def limits_updated(self, section):
        super(Wh40k, self).limits_updated(section)
        root = self.root
        root.foc.cur = root.foc.custom

    def unlimited_foc(self):
        self.set_foc(
            hq=(None, None), el=(None, None), tr=(None, None), fa=(None, None), hs=(None, None), sh=(None, None),
            sec_hq=(None, None), sec_el=(None, None), sec_tr=(None, None), sec_fa=(None, None), sec_hs=(None, None),
            sec_sh=(None, None), fort=(None, None), ally=(None, None),
        )

    def std_foc(self):
        self.set_foc(
            hq=(1, 2), el=(0, 3), tr=(2, 6), fa=(0, 3), hs=(0, 3), sh=(None, None),
            sec_hq=(1, 1), sec_el=(0, 1), sec_tr=(1, 2), sec_fa=(0, 1), sec_hs=(0, 1),
            sec_sh=(None, None), fort=(0, 1), ally=(0, 1),
        )

    def ps_att_foc(self):
        self.set_foc(
            hq=(1, 3), el=(0, 6), tr=(0, 6), fa=(0, 6), hs=(0, 3), sh=(None, None),
            sec_hq=(1, 1), sec_el=(0, 1), sec_tr=(1, 2), sec_fa=(0, 1), sec_hs=(0, 1),
            sec_sh=(None, None), fort=(0, 1), ally=(0, 1),
        )

    def ps_def_foc(self):
        self.set_foc(
            hq=(1, 3), el=(0, 3), tr=(2, 8), fa=(0, 3), hs=(0, 6), sh=(None, None),
            sec_hq=(1, 1), sec_el=(0, 1), sec_tr=(1, 2), sec_fa=(0, 1), sec_hs=(0, 1),
            sec_sh=(None, None), fort=(0, 1), ally=(0, 1),
        )

    def zm_att_foc(self):
        self.set_foc(
            hq=(1, 2), el=(1, 3), tr=(0, 3), fa=(0, 2), hs=(0, 1), sh=(None, None),
            sec_hq=(1, 1), sec_el=(0, 1), sec_tr=(1, 2), sec_fa=(0, 1), sec_hs=(0, 1),
            sec_sh=(None, None), fort=(0, 1), ally=(0, 1),
        )

    def zm_def_foc(self):
        self.set_foc(
            hq=(1, 2), el=(0, 2), tr=(1, 4), fa=(0, 1), hs=(0, 2), sh=(None, None),
            sec_hq=(1, 1), sec_el=(0, 1), sec_tr=(1, 2), sec_fa=(0, 1), sec_hs=(0, 1),
            sec_sh=(None, None), fort=(0, 1), ally=(0, 1),
        )

    def zm_combat_foc(self):
        self.set_foc(
            hq=(1, 2), el=(0, 2), tr=(1, 3), fa=(0, 2), hs=(0, 1), sh=(None, None),
            sec_hq=(1, 1), sec_el=(0, 1), sec_tr=(1, 2), sec_fa=(0, 1), sec_hs=(0, 1),
            sec_sh=(None, None), fort=(0, 1), ally=(0, 1),
        )

    @staticmethod
    def update_foc(roster, hq, el, tr, fa, hs, sh):
        if hasattr(roster, 'hq') and roster.hq:
            roster.hq.min, roster.hq.max = hq
        if hasattr(roster, 'elites') and roster.elites:
            roster.elites.min, roster.elites.max = el
        if hasattr(roster, 'troops') and roster.troops:
            roster.troops.min, roster.troops.max = tr
        if hasattr(roster, 'fast') and roster.fast:
            roster.fast.min, roster.fast.max = fa
        if hasattr(roster, 'heavy') and roster.heavy:
            roster.heavy.min, roster.heavy.max = hs
        if hasattr(roster, 'super_heavy') and roster.super_heavy:
            roster.super_heavy.min, roster.super_heavy.max = sh

    def set_foc(self, hq, el, tr, fa, hs, sh, sec_hq, sec_el, sec_tr, sec_fa, sec_hs, sec_sh, fort, ally):
        self.update_foc(self, hq, el, tr, fa, hs, sh)
        self.fortification.min, self.fortification.max = fort
        if self.ally:
            self.ally.min, self.ally.max = ally
            for unit in self.ally.units:
                self.update_foc(unit.sub_roster.roster, sec_hq, sec_el, sec_tr, sec_fa, sec_hs, sec_sh)
