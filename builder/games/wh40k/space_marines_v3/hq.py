__author__ = 'Denis Romanov'

from builder.core2 import Gear, Count, UnitDescription, OneOf,\
    OptionsList, OptionalSubUnit, SubUnit
from .armory import *
from builder.games.wh40k.roster import Unit


class MarineUnit(Unit):
    chapter = None


class RelicBearer(Unit):

    def has_nonbase(self):
        """
        Return true if model is bearing a non-base ("pseudo ultramarine") relic
        """
        relic_armour = self.armour.nonbase_relic()
        relic_movement = len(self.opt.get_unique()) > 0
        relic_weapon_1 = (self.weapon1.cur in self.weapon1.relic_options) and\
                         not (self.weapon1.cur in self.weapon1.relic_weapon)
        relic_weapon_2 = (self.weapon2.cur in self.weapon2.relic_options) and\
                         not (self.weapon2.cur in self.weapon2.relic_weapon)
        return relic_armour or relic_movement or relic_weapon_1 or relic_weapon_2\
            or (not self.relic.armour_flag)

    def check_rules(self):
        # models in terminator armour do not have bolt pistol
        # so they cannot take relic raven claws
        self.relic.swiftstrike.active = not self.armour.is_tda()
        if self.relic.swiftstrike.value and self.armour.is_tda():
            self.relic.swiftstrike.value = False
        # in self.relics only relics from supplements can be found,
        # so we disable them for main codex
        # unless they are techmarines
        self.relic.used = self.relic.visible = any([
            self.relic.techmarine, self.parent.roster.is_hands(),
            self.parent.roster.is_fists(), self.parent.roster.is_salamanders(),
            self.parent.roster.is_ravens(), self.parent.roster.is_scars(),
            self.parent.roster.is_ultra(),
            any(opt.visible for opt in self.relic.cadia_holy + self.relic.cadia_arcana)])
        # we do not mix artefacts from supplements and main codex on one model
        # also, only one of weapons can be replaced for a relic
        for r1, r2 in zip(self.weapon1.relic_options, self.weapon2.relic_options):
            r2.active = not (self.weapon1.cur == r1)
            r1.active = not (self.weapon2.cur == r2)
        # raven guard relic claws take precedense
        self.weapon1.visible = self.weapon1.used = not self.relic.swiftstrike.value
        self.weapon2.visible = self.weapon2.used = not self.relic.swiftstrike.value
        nonbase = self.has_nonbase()
        self.armour.enable_indomitus(not nonbase)
        self.weapon1.enable_base(not nonbase)
        self.weapon2.enable_base(not nonbase)
        if self.relic.techmarine:
            for opt in self.relic.base:
                opt.used = opt.visible = not nonbase

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return sum((option.get_unique() for option in
                   [self.weapon1, self.weapon2, self.armour, self.relic, self.opt]),
                   [])

    def has_bike(self):
        return self.opt.has_bike()

    def has_jump(self):
        return self.opt.has_jump()


class Sicarius(MarineUnit):
    type_id = 'captain_sicarius_v3'
    type_name = 'Captain Sicarius'
    chapter = 'ultra'

    def __init__(self, parent):
        super(Sicarius, self).__init__(parent=parent, points=175, unique=True, static=True, gear=[
            Gear('Talassarian Tempest Blade'),
            Gear('Plasma pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron Halo'),
        ])


class Tigurius(MarineUnit):
    type_id = 'tigurius_v3'
    type_name = 'Chief Librarian Tigurius'
    chapter = 'ultra'

    def __init__(self, parent):
        super(Tigurius, self).__init__(parent=parent, points=165, unique=True, static=True, gear=[
            Gear('Power armour'),
            Gear('Bolt pistol'),
            Gear('Hood of Hellfire'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Rod of Tigurius'),
        ])

    def count_charges(self):
        return 3

    def build_statistics(self):
        return self.note_charges(super(Tigurius, self).build_statistics())


class Cassius(MarineUnit):
    type_id = 'cassius_v3'
    type_name = 'Chaplain Cassius'
    chapter = 'ultra'

    def __init__(self, parent):
        super(Cassius, self).__init__(parent=parent, points=130, unique=True, static=True, gear=[
            Gear('Power armour'),
            Gear('Bolt pistol'),
            Gear('Crozius Arcanum'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Rosarius'),
            Gear('Infernus'),
        ])


class Telion(MarineUnit):
    type_id = 'telion_v3'
    type_name = 'Sergeant Telion'
    chapter = 'ultra'

    def __init__(self, parent):
        super(Telion, self).__init__(parent, 'Sergeant Telion', 50, [
                    Gear('Bolt pistol'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Camo cloak'),
                    Gear('Quietus'),
                ], static=True, unique=True)


class Chronus(MarineUnit):
    type_id = 'chronus_v3'
    type_name = 'Sergeant Chronus'
    chapter = 'ultra'

    def __init__(self, parent):
        super(Chronus, self).__init__(parent, 'Sergeant Chronus', 50, [
                    Gear('Bolt pistol'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Servo-arm')
        ], static=True, unique=True)


class Khan(MarineUnit):
    type_id = 'khan_v3'
    type_name = 'Kor\'sarro Khan'
    chapter = 'scar'

    class Bike(OptionsList):
        def __init__(self, parent):
            super(Khan.Bike, self).__init__(parent, 'Options')
            self.moondrakkan = self.variant('Moondrakkan', 25)

    def __init__(self, parent):
        super(Khan, self).__init__(parent=parent, points=125, unique=True, gear=[
            Gear('Bolt Pistol'),
            Gear('Moonfang'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron Halo'),
        ])
        self.bike = self.Bike(parent=self)

    def has_bike(self):
        return self.bike.moondrakkan.value


class Vulkan(MarineUnit):
    type_id = 'vulkan_v3'
    type_name = 'Vulkan He\'Stan'
    chapter = 'salamander'

    def __init__(self, parent):
        super(Vulkan, self).__init__(parent=parent, points=190, unique=True, static=True, gear=[
            Gear('Bolt Pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Kesare\'s Mantle'),
            Gear('The Spear of Vulkan'),
            Gear('The Gauntlet of the Forge'),
        ])


class Solaq(MarineUnit):
    type_id = 'solaq_v3'
    type_name = 'Shadow Captain Solaq'
    chapter = 'raven'

    def __init__(self, parent):
        super(Solaq, self).__init__(parent=parent, points=130, unique=True, static=True, gear=[
            Gear('Plasma pistol'),
            Gear('Power sword'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron Halo'),
            Gear('The Raven Helm')
        ])


class Shrike(MarineUnit):
    type_id = 'shrike_v3'
    type_name = 'Shadow Captain Shrike'
    chapter = 'raven'

    def __init__(self, parent):
        super(Shrike, self).__init__(parent=parent, points=185, unique=True, static=True, gear=[
            Gear('Bolt Pistol'),
            Gear('The Raven\'s Talons'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Jump pack'),
            Gear('Iron Halo'),
        ])


class Lysander(MarineUnit):
    type_id = 'lysander_v3'
    type_name = 'Captain Lysander'
    chapter = 'fist'

    def __init__(self, parent):
        super(Lysander, self).__init__(parent=parent, points=230, unique=True, static=True, gear=[
            Gear('Terminator armour'),
            Gear('The Fist of Dorn'),
            Gear('Storm shield'),
            Gear('Iron Halo'),
        ])


class Cantor(MarineUnit):
    type_id = 'cantor_v3'
    type_name = 'Pedro Cantor'
    chapter = 'fist'

    def __init__(self, parent):
        super(Cantor, self).__init__(parent=parent, points=185, unique=True, static=True, gear=[
            Gear('Dorn\'s arrow'),
            Gear('Power fist'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron Halo'),
            Gear('Orbital Strike')
        ])


class Helbrecht(MarineUnit):
    type_id = 'helbrecht_v3'
    type_name = 'High Marshall Helbrecht'
    chapter = 'templar'

    def __init__(self, parent):
        super(Helbrecht, self).__init__(parent=parent, points=180, unique=True, static=True, gear=[
            Gear('Iron halo'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Combi-melta'),
            Gear('Sword of High Marshals'),
        ])


class Champion(MarineUnit):
    type_id = 'champion_v3'
    type_name = 'The Emperors\'s Champion'
    chapter = 'templar'

    def __init__(self, parent):
        super(Champion, self).__init__(parent=parent, points=140, unique=True, static=True, gear=[
            Gear('Black Sword'),
            Gear('The Armour of Faith'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ])


class Grimaldus(MarineUnit):
    type_id = 'grimaldus_v3'
    type_name = 'Chaplain Grimaldus'
    chapter = 'templar'

    def __init__(self, parent):
        super(Grimaldus, self).__init__(parent=parent, points=150, unique=True, gear=[
            Gear('Crozius arcanum'),
            Gear('Rosarius'),
            Gear('Master-crafter plasma pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ])
        self.serv_units = Count(self, name='Cenobyte Servitors', min_limit=0, max_limit=5,
                                points=15, per_model=True,
                                gear=UnitDescription(name='Cenobyte Servitor', points=15))


class Captain(RelicBearer):
    type_id = 'captain_v3'
    type_name = 'Captain'

    class CaptainOptions(OptionsList):
        def __init__(self, parent):
            super(Captain.CaptainOptions, self).__init__(parent, '')
            self.shield = self.variant('Storm Shield', 15)
            self.up = self.variant('Upgrade to Chapter Master', 40,
                                   gear=[Gear('Orbital strike')])

    def __init__(self, parent):
        super(Captain, self).__init__(parent=parent, points=90, gear=Gear('Iron halo'))

        self.armour = Armour(self, art=20, tda=30, relic=True)

        class Weapon1(ArcanaRelicWeapon, TerminatorRanged, Ranged, Melee, Boltgun, BoltPistol):
            pass

        class Weapon2(HolyRelicWeapon, TerminatorMelee, Ranged, Melee, RelicBlade, Chainsword):
            terminator_relic = 10

        self.weapon1 = Weapon1(self, 'Weapon', armour=self.armour)
        self.weapon2 = Weapon2(self, '', armour=self.armour)
        self.relic = HolyRelic(self, captain=True)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True)
        self.opt2 = self.CaptainOptions(self)

    def check_rules(self):
        super(Captain, self).check_rules()
        self.opt2.shield.active = not self.armour.is_tda()

    def build_description(self):
        desc = super(Captain, self).build_description()
        if self.opt2.up.value:
            desc.name = 'Chapter Master'
        return desc

    def is_master(self):
        return self.opt2.up.value


class TerminatorCaptain(RelicBearer):
    type_id = 'terminator_captain_v3'
    type_name = 'Terminator Captain'

    class Armour(OneOf):
        def __init__(self, parent):
            super(TerminatorCaptain.Armour, self).__init__(parent, 'Armour')
            self.tda = self.variant('Terminator armour', 0)
            self.cda = self.variant('Cataphractii Terminator armour', 0)

        def nonbase_relic(self):
            return False

        def is_tda(self):
            return True

        def get_unique(self):
            return []

        def enable_indomitus(self, flag):
            pass

    def __init__(self, parent):
        super(TerminatorCaptain, self).__init__(parent=parent, points=120, gear=Gear('Iron halo'))

        self.armour = self.Armour(self)

        class Weapon1(ArcanaRelicWeapon, TerminatorRanged):
            pass

        class Weapon2(HolyRelicWeapon, TerminatorMelee):
            default_weapon = 'Power Sword'
            terminator_relic = 10

        self.weapon1 = Weapon1(self, 'Weapon', armour=self.armour)
        self.weapon2 = Weapon2(self, '', armour=self.armour)

        self.relic = HolyRelic(self)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True)


class Librarian(RelicBearer):
    type_name = 'Librarian'
    type_id = 'librarian_v3'

    class TdaWeapon(OptionsList):
        def __init__(self, parent, armour):
            super(Librarian.TdaWeapon, self).__init__(parent=parent, name='', limit=1)
            self.armour = armour
            self.stormbolter = self.variant('Storm Bolter', 5)
            self.combimelta = self.variant('Combi-melta', 10)
            self.combiflamer = self.variant('Combi-flamer', 10)
            self.combiplasma = self.variant('Combi-plasma', 10)
            self.stormshield = self.variant('Storm shield', 10)

        def check_rules(self):
            super(Librarian.TdaWeapon, self).check_rules()
            self.visible = self.used = self.armour.is_tda()

    class Level(OneOf):
        def __init__(self, parent):
            super(Librarian.Level, self).__init__(parent, 'Psyker')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    def __init__(self, parent):
        super(Librarian, self).__init__(parent=parent, points=65, gear=Gear('Psychic hood'))

        self.armour = Armour(self, tda=25,
                             relic=self.roster.is_base_codex())

        class Weapon1(HolyRelicWeapon, ForceWeapon):
            pass

        class Weapon2(ArcanaRelicWeapon, Ranged, Boltgun, BoltPistol):
            pass

        self.weapon1 = Weapon1(self, 'Weapon', armour=self.armour, librarian=True)
        self.weapon2 = Weapon2(self, '', armour=self.armour)
        self.tda_weapon = self.TdaWeapon(self, armour=self.armour)
        self.relic = HolyRelic(self, librarian=True)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True)
        self.psy = self.Level(self)

    def check_rules(self):
        super(Librarian, self).check_rules()
        self.weapon2.visible = self.weapon2.used = not self.armour.is_tda()
        if not self.weapon2.used:
            for r in self.weapon1.relic_weapon:
                r.active = True

    def count_charges(self):
        return self.psy.count_charges() + self.relic.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Librarian, self).build_statistics())


class Servitors(Unit):
    type_name = "Servitors"
    type_id = 'servitors_v1'
    model_points = 10

    def __init__(self, parent):
        super(Servitors, self).__init__(parent=parent)
        self.models = Count(self, self.name, points=self.model_points, min_limit=1, max_limit=5, per_model=True)
        self.hw = [
            Count(self, 'Heavy bolter', 0, 2, 10),
            Count(self, 'Multi-melta', 0, 2, 10),
            Count(self, 'Plasma cannon', 0, 2, 20)
        ]

    def check_rules(self):
        Count.norm_counts(0, min(2, self.get_count()), self.hw)

    def build_description(self):
        desc = UnitDescription(self.name, points=self.points, count=self.get_count())
        serv = UnitDescription('Servitor', points=10)
        arms_count = self.get_count()
        for wep in self.hw:
            hw_serv = serv.clone()
            hw_serv.add(Gear(wep.name))
            hw_serv.points += wep.option_points
            hw_serv.count = wep.cur
            desc.add(hw_serv)
            arms_count -= wep.cur
        serv.add(Gear('Servo-Arm'))
        serv.count = arms_count
        desc.add(serv)
        return desc

    def get_count(self):
        return self.models.cur


class Techmarine(RelicBearer):
    type_name = 'Techmarine'
    type_id = 'techmarine_v3'

    class Special(OneOf):
        def __init__(self, parent):
            super(Techmarine.Special, self).__init__(parent, 'Special')
            self.servoarm = self.variant('Servo-arm', 0)
            self.variant('Conversion beamer', 20)
            self.servoharness = self.variant('Servo-harness', 25)

    class Servitors(OptionalSubUnit):
        def __init__(self, parent):
            super(Techmarine.Servitors, self).__init__(parent, 'Servitors')
            SubUnit(self, Servitors(parent=None))

    def __init__(self, parent):
        super(Techmarine, self).__init__(parent=parent, points=65, gear=Armour.artificer_armour_set)

        class Weapon1(ArcanaRelicWeapon, Melee, Ranged, Boltgun, BoltPistol):
            pass

        class Weapon2(HolyRelicWeapon, PowerAxe):
            pass

        # for RelicBearer checks
        class DummyArmour:
            def is_tda(self):
                return False

            def get_unique(self):
                return []

            def enable_indomitus(self, flag):
                pass

            def nonbase_relic(self):
                return False

        self.armour = DummyArmour()
        self.weapon1 = Weapon1(self, 'Weapon')
        self.weapon2 = Weapon2(self, '')
        self.Special(self)
        self.opt = Options(self, bike=True)
        self.relic = HolyRelic(self, techmarine=True)
        self.Servitors(self)


class Chaplain(RelicBearer):
    type_name = 'Chaplain'
    type_id = 'chaplain_v3'

    def __init__(self, parent):
        super(Chaplain, self).__init__(parent=parent, points=90, gear=Gear('Rozarius'))

        self.armour = Armour(self, tda=30,
                             relic=self.roster.is_base_codex())

        class Weapon1(HolyRelicWeapon, Crozius):
            pass

        class Weapon2(ArcanaRelicWeapon, TerminatorComby, Ranged, PowerFist, Boltgun, BoltPistol):
            pass

        self.weapon1 = Weapon1(self, 'Weapon', armour=self.armour, chaplain=True)
        self.weapon2 = Weapon2(self, '', armour=self.armour)
        self.relic = HolyRelic(self)
        self.opt = Options(self, armour=self.armour, jump=True, bike=True)
