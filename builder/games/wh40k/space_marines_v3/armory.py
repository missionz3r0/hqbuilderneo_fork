__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear
from itertools import chain
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianRelicsHoly, CadianWeaponHoly,\
    CadianWeaponArcana


class Vehicle(OptionsList):
    def __init__(self, parent, blade=True, melta=False, shield=False):
        super(Vehicle, self).__init__(parent, 'Options')
        self.sbgun = self.Variant(self, 'Storm bolter', 5)
        if blade:
            self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
        self.exarm = self.Variant(self, 'Extra armour', 10)
        if melta:
            self.melta = self.Variant(self, 'Multi melta', 10)
        if shield:
            self.shield = self.Variant(self, 'Siege shield', 10)


class BaseWeapon(OneOf):
    def __init__(self, parent, name, **kwargs):
        self.armour = kwargs.pop('armour', None)
        self.pwr_weapon = []
        self.tda_weapon = []
        self.relic_options = []

        super(BaseWeapon, self).__init__(parent, name, **kwargs)

    def check_rules(self):
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.used and self.cur in self.relic_options:
            return self.description
        return []


class BoltPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(BoltPistol, self).__init__(*args, **kwargs)
        self.boltpistol = self.variant('Bolt pistol', 0)
        self.pwr_weapon += [self.boltpistol]


class Boltgun(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Boltgun, self).__init__(*args, **kwargs)
        self.boltgun = self.variant('Boltgun', 0)
        self.pwr_weapon += [self.boltgun]


class Comby(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Comby, self).__init__(*args, **kwargs)

        self.stormbolter = self.variant('Storm bolter', 5)
        self.combimelta = self.variant('Combi-melta', 10)
        self.combiflamer = self.variant('Combi-flamer', 10)
        self.combigrav = self.variant('Combi-grav', 10)
        self.combiplasma = self.variant('Combi-plasma', 10)
        self.pwr_weapon += [self.stormbolter, self.combiflamer, self.combigrav, self.combimelta, self.combiplasma]


class PowerPistols(BaseWeapon):
    def __init__(self, *args, **kwargs):
        damned = kwargs.pop('damned', False)
        super(PowerPistols, self).__init__(*args, **kwargs)
        if not damned:
            self.gravpistol = self.variant('Grav-pistol', 15)
            self.pwr_weapon += [self.gravpistol]
        self.plasmapistol = self.variant('Plasma pistol', 15)
        self.pwr_weapon += [self.plasmapistol]


class Ranged(PowerPistols, Comby):
    def __init__(self, *args, **kwargs):
        super(Ranged, self).__init__(*args, **kwargs)
        self.hand_flamer = self.variant('Hand flamer', 5)

    def check_rules(self):
        super(Ranged, self).check_rules()
        self.hand_flamer.used = self.hand_flamer.visible = self.parent.roster.is_hawks()


class Chainsword(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Chainsword, self).__init__(*args, **kwargs)
        self.chainsword = self.variant('Chainsword', 0)
        self.pwr_weapon += [self.chainsword]


class LightningClaw(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(LightningClaw, self).__init__(*args, **kwargs)
        self.claw = self.variant('Lightning claw', 15)
        self.pwr_weapon += [self.claw]


class RelicBlade(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(RelicBlade, self).__init__(*args, **kwargs)
        self.relic_blade = self.variant('Relic blade', 25)
        self.scorpion_tear = self.variant('Tears of the Scorpion', 35)
        self.pwr_weapon += [self.relic_blade, self.scorpion_tear]

    def check_rules(self):
        super(RelicBlade, self).check_rules()
        self.scorpion_tear.used = self.scorpion_tear.visible = self.parent.roster.is_scorpions()


class PowerWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerWeapon, self).__init__(*args, **kwargs)
        self.pwr = self.variant('Power weapon', 15)
        self.pwr_weapon += [self.pwr]


class PowerFist(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerFist, self).__init__(*args, **kwargs)
        self.fist = self.variant('Power fist', 25)
        self.pwr_weapon += [self.fist]


class ThunderHammer(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ThunderHammer, self).__init__(*args, **kwargs)
        self.hummer = self.variant('Thunder hammer', 30)
        self.pwr_weapon += [self.hummer]


class Melee(ThunderHammer, PowerFist, PowerWeapon, LightningClaw):
    pass


class PowerAxe(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerAxe, self).__init__(*args, **kwargs)
        self.power_axe = self.variant('Power axe', 0)


class ForceWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ForceWeapon, self).__init__(*args, **kwargs)
        self.force = self.variant('Force weapon', 0)


class Crozius(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Crozius, self).__init__(*args, **kwargs)
        self.roz = self.variant('Crozius Arcanum', 0)


class TerminatorComby(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorComby, self).__init__(*args, **kwargs)
        self.tda_stormbolter = self.variant('Storm Bolter', 0)
        self.tda_combimelta = self.variant('Combi-melta', 5)
        self.tda_combiflamer = self.variant('Combi-flamer', 5)
        self.tda_combiplasma = self.variant('Combi-plasma', 5)
        self.tda_weapon += [self.tda_stormbolter, self.tda_combiflamer, self.tda_combimelta, self.tda_combiplasma]


class TerminatorRanged(TerminatorComby):
    def __init__(self, *args, **kwargs):
        super(TerminatorRanged, self).__init__(*args, **kwargs)
        self.tda_lightningclaw = self.variant('Lightning claw', 10)
        self.tda_thunderhammer = self.variant('Thunder hammer', 25)
        self.tda_weapon += [self.tda_lightningclaw, self.tda_thunderhammer]


class TerminatorMelee(BaseWeapon):
    terminator_relic = 0
    default_weapon = 'Power Weapon'

    def __init__(self, *args, **kwargs):
        super(TerminatorMelee, self).__init__(*args, **kwargs)
        self.tda2_powerweapon = self.variant(self.default_weapon, 0)
        self.tda2_lightningclaw = self.variant('Lightning claw', 5)
        self.tda2_stormshield = self.variant('Storm shield', 5)
        self.tda2_powerfist = self.variant('Power fist', 10)
        self.tda2_chainfist = self.variant('Chainfist', 15)
        self.tda2_thunderhammer = self.variant('Thunder hammer', 15)
        self.tda_weapon += [self.tda2_powerweapon, self.tda2_lightningclaw, self.tda2_stormshield,
                            self.tda2_powerfist, self.tda2_chainfist, self.tda2_thunderhammer]
        if self.terminator_relic:
            self.tda2_rblade = self.variant('Relic blade', 10)
            self.tda_weapon += [self.tda2_rblade]


class Heavy(BaseWeapon):

    class Flakk(OptionsList):
        def __init__(self, parent):
            super(Heavy.Flakk, self).__init__(parent=parent, name='', limit=None)
            self.flakkmissiles = self.variant('Flakk missiles', 10)

    def __init__(self, parent, *args, **kwargs):
        heavyflamer = kwargs.pop('heavy_flamer', False)
        gravcannon = kwargs.pop('grav_cannon', True)
        super(Heavy, self).__init__(parent, *args, **kwargs)

        self.heavybolter = self.variant('Heavy bolter', 10)
        self.heavyflamer = heavyflamer and self.variant('Heavy flamer', 10)
        self.multimelta = self.variant('Multi-melta', 10)
        self.missilelauncher = self.variant('Missile launcher', 15)
        self.plasmacannon = self.variant('Plasma cannon', 15)
        self.lascannon = self.variant('Lascannon', 20)
        self.grav = gravcannon and self.variant('Grav-cannon and grav-amp', 35,
                                                gear=[Gear('Grav-cannon'), Gear('Grav-amp')])
        self.heavy = [self.heavybolter, self.heavyflamer, self.missilelauncher,
                      self.multimelta, self.plasmacannon, self.grav,
                      self.lascannon]
        self.flakk = self.Flakk(parent=parent)

    def is_heavy(self):
        return self.cur in self.heavy

    def check_rules(self):
        if self.flakk:
            self.flakk.visible = self.flakk.used = self.cur == self.missilelauncher

    @property
    def description(self):
        desc = super(Heavy, self).description
        desc += self.flakk.description
        return desc


class HeavyList(OptionsList):
    def __init__(self, parent, limit=1, heavy_flamer=False, gravcannon=True):
        super(HeavyList, self).__init__(parent=parent, name='Heavy weapon')
        self.weapon_limit = limit
        self.heavy = []
        self.hbgun = self.variant('Heavy bolter', 10)
        if heavy_flamer:
            self.heavyflamer = self.variant('Heavy flamer', 10)
            self.heavy += [self.heavyflamer]
        self.mulmgun = self.variant('Multi-melta', 10)
        self.mlaunch = self.variant('Missile launcher', 15)
        self.flakkmissiles = self.variant('Flakk missiles', 10, visible=False)
        self.pcannon = self.variant('Plasma cannon', 15)
        self.lcannon = self.variant('Lascannon', 20)
        if gravcannon:
            self.grav = self.variant('Grav-cannon and grav-amp', 35,
                                     gear=[Gear('Grav-cannon'), Gear('Grav-amp')])
            self.heavy += [self.grav]
        self.heavy += [self.hbgun, self.mulmgun, self.mlaunch, self.pcannon, self.lcannon]

    def check_rules(self):
        super(HeavyList, self).check_rules()
        self.flakkmissiles.visible = self.flakkmissiles.used = self.mlaunch.value
        self.process_limit(self.heavy, self.weapon_limit)


class Special(BaseWeapon):
    def __init__(self, *args, **kwargs):
        gravgun = kwargs.pop('grav_gun', True)
        super(Special, self).__init__(*args, **kwargs)
        self.flamer = self.variant('Flamer', 5)
        self.meltagun = self.variant('Meltagun', 10)
        self.gravgun = gravgun and self.variant('Grav-gun', 15)
        self.plasmagun = self.variant('Plasma gun', 15)
        self.spec = [self.flamer, self.meltagun, self.gravgun, self.plasmagun]

    def is_spec(self):
        return self.cur in self.spec


class SpecialList(OptionsList):
    def __init__(self, parent, limit=1, grav=True):
        super(SpecialList, self).__init__(parent=parent, name='Special weapon', limit=limit)
        self.flame = self.variant('Flamer', 5)
        self.mgun = self.variant('Meltagun', 10)
        self.gravgun = grav and self.variant('Grav-gun', 15)
        self.pgun = self.variant('Plasmagun', 15)

        self.fury = self.variant('Nocturne\'s Fury', 20)

    def get_unique(self):
        if self.fury and self.fury == self.cur:
            return self.description[0:1]
        else:
            return []

    def check_rules(self):
        super(SpecialList, self).check_rules()
        if self.fury:
            self.fury.visible = self.fury.used = self.parent.roster.is_salamanders()


class Armour(OneOf):
    power_armour_set = [Gear('Frag grenades'), Gear('Krak grenades')]
    artificer_armour_set = [Gear('Artificer armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    scout_armour_set = [Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent, art=0, tda=0, relic=False):
        super(Armour, self).__init__(parent, 'Armour')
        self.pwr = self.variant('Power armour', 0, gear=self.power_armour_set)
        self.art = art and self.variant('Artificer armour', art, gear=self.artificer_armour_set)
        self.tda = tda and self.variant('Terminator armour', tda)
        self.rel = relic and self.variant('The Armour Indomitus', 60, gear=[
            Gear('The Armour Indomitus')] + self.scout_armour_set)
        self.sharmour = relic and self.variant('The Armour of Shadows', 35, gear=[
            Gear('The Armour of Shadows')] + self.scout_armour_set)

    def is_tda(self):
        return self.used and self.cur == self.tda

    def nonbase_relic(self):
        return self.sharmour and (self.cur == self.sharmour)

    def enable_indomitus(self, flag):
        if self.rel:
            self.rel.active = flag

    def get_unique(self):
        if (self.rel and self.rel == self.cur) or (self.sharmour and self.sharmour == self.cur):
            return self.description[0:1]
        else:
            return []

    def check_rules(self):
        if self.sharmour:
            self.sharmour.visible = self.sharmour.used = self.parent.roster.is_ravens()
        # in general case relic armour should be visible, unless specifically disabled with enable_indomitus
        # if self.rel:
        #     self.rel.visible = self.rel.used = self.parent.roster.is_base_codex()


class Options(OptionsList):
    def __init__(self, parent, armour=None, bike=False, jump=False):
        super(Options, self).__init__(parent, 'Special issue wargear')
        self.armour = armour
        self.bike_option = bike
        self.jump_option = jump
        self.ride = []

        self.auspex = self.variant('Auspex', 5)
        self.meltabombs = self.variant('Melta bombs', 5)
        self.teleporthomer = self.variant('Teleport homer', 5)
        self.digitalweapons = self.variant('Digital weapons', 10)
        if jump:
            self.jumppack = self.variant('Jump pack', 15)
            self.fury = self.variant('Raven\'s Fury', 15)
            self.ride += [self.jumppack, self.fury]
        if bike:
            self.spacemarinebike = self.variant('Space Marine Bike', 20)
            self.heaven = self.variant('Wrath or the Heavens', 25)
            self.ride += [self.spacemarinebike, self.heaven]

    def check_rules(self):
        show_ride = True
        if self.armour:
            show_ride = not self.armour.is_tda()
            for opt in self.ride:
                opt.visible = opt.used = not self.armour.is_tda()
        if self.bike_option:
            self.heaven.visible = self.heaven.used = self.parent.roster.is_scars() and show_ride
        if self.jump_option:
            self.fury.visible = self.fury.used = self.parent.roster.is_ravens() and show_ride
        self.process_limit(self.ride, 1)

    def has_bike(self):
        return self.bike_option and (self.spacemarinebike.value or self.heaven.value)

    def has_jump(self):
        return self.jump_option and (self.jumppack.value or self.fury.value)

    def get_unique(self):
        if (self.bike_option and self.heaven.value) or (self.jump_option and self.fury.value):
            return self.description
        else:
            return []


class Relic(OptionsList):
    def __init__(self, parent, librarian=False, techmarine=False, chaplain=False, captain=False):
        super(Relic, self).__init__(parent, 'Relic', limit=1)
        self.base = []
        self.hands = []
        self.fists = []
        self.scars = []
        self.ravens = []
        self.salamanders = []
        self.ultramarines = []
        self.techmarine = techmarine
        if techmarine:
            self.base += [self.variant('The Armour Indomitus', 60)]

        #Hands
        self.ironstone = self.variant('The Ironstone', 30)
        self.chain = self.variant('The Gorgon\'s Chain', 45)
        self.helm = self.variant('The Tempered Helm', 35)
        self.hands += [self.ironstone, self.chain, self.helm]

        #Fists
        self.theeye = self.variant('The Eye of Hypnoth', 15)
        self.fists += [self.theeye]
        if librarian:
            self.bones = self.variant('The Bones of Osrak', 25)  # Librarian only
            self.fists.append(self.bones)

        #white scars
        self.hunter_eye = self.variant('The Hunter\'s Eye', 20)
        self.scars += [self.hunter_eye]
        if librarian:
            self.mantle = self.variant('Mantle of the Stormseer', 20)  # Librarian only
            self.scars.append(self.mantle)

        # raven guards
        self.scull = self.variant('The Raven Skull of Korvaad', 15)
        self.swiftstrike = self.variant('Swiftstrike and Murder', 35)
        self.ravens += [self.scull, self.swiftstrike]

        # salamanders
        self.salamander_mantle = self.variant('The Salamander\'s Mantle', 30)
        if chaplain:
            self.sigil = self.variant('Vulkan\'s Sigil', 30)
            self.salamanders.append(self.sigil)
        self.salamanders += [self.salamander_mantle]
        if librarian:
            self.tom = self.variant('The Tome of Vel\'cona', 25)
            self.salamanders.append(self.tom)

        # ultramarines
        self.tarent = self.variant('Tarentian cloak', 35)
        self.censure = self.variant('Helm of Censure', 30)
        self.ultramarines += [self.tarent, self.censure]
        if captain:
            self.ultramarines.append(self.variant('Sanctic Halo', 15))

    @property
    def armour_flag(self):
        # self.parent.roster.is_base_codex() and\
        armour_flag = not any(opt.value for opt in chain(
            self.scars, self.ravens, self.salamanders, self.hands, self.fists
        ))
        return armour_flag

    def check_rules(self):
        super(Relic, self).check_rules()
        for o in self.hands:
            o.visible = o.used = self.parent.roster.is_hands()
        for o in self.fists:
            o.visible = o.used = self.parent.roster.is_fists()
        for o in self.scars:
            o.visible = o.used = self.parent.roster.is_scars()
        for o in self.ravens:
            o.visible = o.used = self.parent.roster.is_ravens()
        for o in self.salamanders:
            o.visible = o.used = self.parent.roster.is_salamanders()
        for o in self.ultramarines:
            o.visible = o.used = self.parent.roster.is_ultra()

    def get_unique(self):
        if self.used:
            return self.description
        return []

    def count_charges(self):
        if self.bones.used and self.bones.value:
            return 1
        return 0


class HolyRelic(CadianRelicsHoly, Relic):
    pass


class RelicWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        self.librarian = kwargs.pop('librarian', False)
        self.chaplain = kwargs.pop('chaplain', False)
        melee = kwargs.pop('melee', True)
        super(RelicWeapon, self).__init__(*args, **kwargs)
        self.theprimarchswrath = self.variant('The Primarch\'s Wrath', 20)
        self.teethofterra = self.variant('Teeth of Terra', 35)
        self.theshieldeternal = self.variant('The Shield Eternal', 50)
        self.theburningblade = self.variant('The Burning Blade', 55)
        self.relic_weapon = [self.theprimarchswrath, self.teethofterra, self.theshieldeternal, self.theburningblade]
        self.relic_options = self.relic_weapon[:]

        # scars
        self.glave = self.variant('The Glaive of Vengeance', 30)
        self.scimitar = self.variant('Scimitar of the Great Khan', 20)
        self.scars = [self.glave, self.scimitar]
        self.relic_options.extend(self.scars)

        # Hands
        self.axe = self.variant('The Axe of Medusa', 25)
        self.bane = self.variant('Betrayer\'s Bane', 25)
        self.hands = [self.axe, self.bane]
        if self.librarian:
            self.stave = self.variant('The Mindforge Stave', 15)  # Librarian only
            self.hands.append(self.stave)
        self.relic_options.extend(self.hands)

        # Fists
        self.spartean = self.variant('The Spartean', 5)
        self.fists = [self.spartean]
        # note: only crozius should be replaceable
        if self.chaplain:
            self.angel = self.variant('The Angel of Sacrifice', 10)  # Chaplain only
            self.fists.append(self.angel)
        self.relic_options.extend(self.fists)

        # Salamanders
        self.wrath = self.variant('Wrath of Prometheus', 10)
        self.drake = self.variant('Drake-smiter', 50)
        self.salamanders = [self.wrath, self.drake]
        self.relic_options.extend(self.salamanders)

        # Ravens
        self.tenebris = self.variant('Ex Tenebris', 10)
        self.ravens = [self.tenebris]
        self.relic_options.extend(self.ravens)

        # Ultramarines
        self.ultramarines = []
        if melee:
            self.ultramarines.append(self.variant("Soldier's Blade", 20))
        else:
            self.ultramarines.append(self.variant('Vengeance of Ultramar', 20))
        self.relic_options.extend(self.ultramarines)

    def enable_base(self, flag):
        for o in self.relic_weapon:
            o.visible = o.used = flag

    def check_rules(self):
        super(RelicWeapon, self).check_rules()
        for wep in self.hands:
            wep.used = wep.visible = self.parent.roster.is_hands()
        for wep in self.scars:
            wep.used = wep.visible = self.parent.roster.is_scars()
        for wep in self.fists:
            wep.used = wep.visible = self.parent.roster.is_fists()
        for wep in self.salamanders:
            wep.used = wep.visible = self.parent.roster.is_salamanders()
        for wep in self.ravens:
            wep.used = wep.visible = self.parent.roster.is_ravens()
        for wep in self.ultramarines:
            wep.used = wep.visible = self.parent.roster.is_ultra()


class HolyRelicWeapon(CadianWeaponHoly, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(HolyRelicWeapon, self).__init__(*args, melee=True, **kwargs)
        self.relic_options.append(self.worthy_blade)


class ArcanaRelicWeapon(CadianWeaponArcana, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(ArcanaRelicWeapon, self).__init__(*args, melee=False, **kwargs)
        self.relic_options.append(self.qann)


class Standards(OptionsList):
    def __init__(self, parent, honour=False, company=False):
        super(Standards, self).__init__(parent, 'Space marine standard', limit=1)
        self.company = company and self.variant('Company standard', 15)
        self.staganda = company and self.variant('The Banner of Staganda', 25)
        self.banner = honour and self.variant('Chapter banner', 20)
        self.emperor = self.variant('Standard of the Emperor Ascendant', 60)
        self.eagle = self.variant('The Banner of the Eagle', 30)
        self.macragge = honour and self.variant('The Standard of Macragge Inviolate', 60)

    def check_rules(self):
        super(Standards, self).check_rules()
        if self.staganda:
            self.staganda.used = self.staganda.visible = self.parent.roster.is_fists()
        if self.eagle:
            self.eagle.used = self.eagle.visible = self.parent.roster.is_scars()
        if self.macragge:
            self.macragge.used = self.macragge.visible = self.parent.roster.is_ultra()

    def get_unique(self):
        if (self.banner and self.banner.value) or self.emperor.value or\
           (self.staganda and self.staganda.value) or\
           (self.macragge and self.macragge.value) or\
           self.eagle.value:
            return self.description
        else:
            return []
