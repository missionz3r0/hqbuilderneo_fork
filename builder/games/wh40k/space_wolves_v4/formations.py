from builder.games.wh40k.roster import Formation, UnitType, Detachment,\
    CompositeFormation
from builder.games.wh40k.imperial_armour import volume2
from .hq import WolfLord, Ragnar, Harald, Canis, RunePriest, WolfPriest,\
    Njal, Ulrik, BattleLeader, Bjorn, IronPriest, Krom, KromDragongaze
from .elites import WolfScouts, Dreadnought,\
    Murderfang, WolfGuards, WolfGuardTerminators, Arjac, LoneWolf, Wulfen,\
    FierceEyeWolfGuard
from .troops import BloodClaws, Lucas, GreyHunters, IronaxeGreyHunters,\
    RedfistBloodClaws
from .fast import DropPod, Stormwolf, Swiftclaws,\
    Skyclaws, WolfPack, ThunderwolfCavalry, LandSpeederSquadron, Stormwolves
from .heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer,\
    Stormfang, Whirlwinds, Vindicators, Predators, LongFangs, Stormfangs
from .lords import Logan


class ChampionFormation(volume2.SpaceMarinesLegaciesFormation):

    '''
    @note None of these formations have grey claws or lone wolves,
    so we can safely ignore checking for them
    '''

    def is_champion(self):
        return True


class WulfenFormation(volume2.SpaceMarinesLegaciesFormation):

    '''
    @note None of these formations have grey claws or lone wolves,
    so we can safely ignore checking for them
    '''

    def is_champion(self):
        return False


class GreatCompany(WulfenFormation):
    army_id = 'space_wolves_v4_great_company_wulfen'
    army_name = 'Great Company'

    def __init__(self):
        super(GreatCompany, self).__init__()
        UnitType(self, WolfLord, min_limit=1, max_limit=1)
        UnitType(self, BattleLeader, min_limit=1, max_limit=1)
        guards = [
            UnitType(self, WolfGuards),
            UnitType(self, WolfGuardTerminators)
        ]
        self.add_type_restriction(guards, 1, 1)
        self.hunters = UnitType(self, GreyHunters, min_limit=5, max_limit=5)
        UnitType(self, WolfScouts, min_limit=1, max_limit=1)

        claws = [
            UnitType(self, BloodClaws),
            UnitType(self, Swiftclaws),
            UnitType(self, Skyclaws)
        ]
        self.add_type_restriction(claws, 3, 3)

        UnitType(self, LongFangs, min_limit=2, max_limit=2)

    def check_rules(self):
        super(GreatCompany, self).check_rules()
        banners = sum(u.has_banner() for u in self.hunters.units)
        if banners != 1:
            self.error("One unit of Grey Hunters must include a wolf standart.")


class Firehowlers(WulfenFormation):
    army_id = 'space_wolves_v4_firehowlers'
    army_name = 'The Firehowlers'

    def __init__(self):
        super(Firehowlers, self).__init__()
        self.lord = UnitType(self, WolfLord)
        self.leader = UnitType(self, BattleLeader)
        hq = [
            self.lord,
            self.leader
        ]
        self.add_type_restriction(hq, 1, 1)
        self.guards = UnitType(self, WolfGuards, min_limit=1, max_limit=1)

        claws = [
            UnitType(self, Swiftclaws),
            UnitType(self, Skyclaws)
        ]
        self.add_type_restriction(claws, 2, 4)

        troops = [
            UnitType(self, GreyHunters),
            UnitType(self, LandSpeederSquadron)
        ]
        self.add_type_restriction(troops, 2, 4)
        UnitType(self, LongFangs, min_limit=0, max_limit=1)
        UnitType(self, WolfScouts, min_limit=0, max_limit=1)
        UnitType(self, LoneWolf, min_limit=0, max_limit=2)

    def check_rules(self):
        super(Firehowlers, self).check_rules()
        for unit in self.lord.units:
            if not unit.has_bike() and not unit.has_jump():
                self.error("Wolf Lord must be equipped with jump pack or ride Space Marine bike")

        for unit in self.leader.units:
            if not unit.has_bike() and not unit.has_jump():
                self.error("Wolf Guard Battle Leader must be equipped with jump pack or ride Space Marine bike")

        for unit in self.guards.units:
            if not unit.has_bike() and not unit.has_jump():
                self.error("Wolf Guard must be equipped with jump pack or ride Space Marine bike")


class Ironwolves(WulfenFormation):
    army_id = 'space_wolves_v4_ironwolves'
    army_name = 'The Ironwolves'
    ironwolves = True

    def __init__(self):
        super(Ironwolves, self).__init__()
        self.lord = UnitType(self, WolfLord)
        self.leader = UnitType(self, BattleLeader)
        hq = [
            self.lord,
            self.leader
        ]
        self.add_type_restriction(hq, 1, 1)
        self.guards = UnitType(self, WolfGuards)
        self.termoguards = UnitType(self, WolfGuardTerminators)
        self.add_type_restriction([self.guards, self.termoguards], 0, 1)

        self.claws = UnitType(self, BloodClaws, min_limit=2, max_limit=3)

        self.hunters = UnitType(self, GreyHunters)
        troops = [
            self.hunters,
            UnitType(self, LandSpeederSquadron)
        ]
        self.add_type_restriction(troops, 2, 4)
        self.fangs = UnitType(self, LongFangs, min_limit=1, max_limit=2)
        self.scouts = UnitType(self, WolfScouts, min_limit=0, max_limit=1)
        UnitType(self, LoneWolf, min_limit=0, max_limit=2)

    def check_rules(self):
        super(Ironwolves, self).check_rules()
        units = []
        for u in (self.guards, self.termoguards, self.claws, self.hunters, self.fangs, self.scouts):
            units.extend(u.units)

        for unit in units:
            if not unit.has_transport():
                self.error("You must include enough Dedicated Transport with sufficient Transport Capacity"
                           "to carry all non-vehicle models from this formation")
                break


class Drakeslayers(WulfenFormation):
    army_id = 'space_wolves_v4_drakeslayers'
    army_name = 'The Drakeslayers'

    def __init__(self):
        super(Drakeslayers, self).__init__()
        hq = [
            UnitType(self, Krom),
            UnitType(self, BattleLeader)
        ]
        self.add_type_restriction(hq, 1, 1)
        self.guards = UnitType(self, WolfGuards)
        self.termoguards = UnitType(self, WolfGuardTerminators)
        self.add_type_restriction([self.guards, self.termoguards], 1, 2)

        UnitType(self, BloodClaws, min_limit=2, max_limit=3)

        troops = [
            UnitType(self, GreyHunters),
            UnitType(self, LandSpeederSquadron)
        ]
        self.add_type_restriction(troops, 2, 4)
        UnitType(self, LongFangs, min_limit=1, max_limit=2)
        UnitType(self, WolfScouts, min_limit=0, max_limit=1)
        UnitType(self, LoneWolf, min_limit=0, max_limit=2)


class Deathwolves(WulfenFormation):
    army_id = 'space_wolves_v4_deathwolves'
    army_name = 'The Deathwolves'

    def __init__(self):
        super(Deathwolves, self).__init__()
        hq = [
            UnitType(self, Harald),
            UnitType(self, BattleLeader)
        ]
        self.add_type_restriction(hq, 1, 1)
        UnitType(self, Canis, min_limit=0, max_limit=1)
        UnitType(self, ThunderwolfCavalry, min_limit=1, max_limit=1)

        claws = [
            UnitType(self, BloodClaws),
            UnitType(self, Swiftclaws),
            UnitType(self, Skyclaws)
        ]
        self.add_type_restriction(claws, 2, 3)

        troops = [
            UnitType(self, GreyHunters),
            UnitType(self, LandSpeederSquadron)
        ]
        self.add_type_restriction(troops, 2, 4)
        UnitType(self, LongFangs, min_limit=1, max_limit=2)
        UnitType(self, WolfScouts, min_limit=1, max_limit=1)
        UnitType(self, LoneWolf, min_limit=0, max_limit=2)
        UnitType(self, WolfPack, min_limit=0, max_limit=1)


class Blackmanes(WulfenFormation):
    army_id = 'space_wolves_v4_blackmanes'
    army_name = 'The Blackmanes'
    discount = True

    def __init__(self):
        super(Blackmanes, self).__init__()
        hq = [
            UnitType(self, Ragnar),
            UnitType(self, BattleLeader)
        ]
        self.add_type_restriction(hq, 1, 1)
        UnitType(self, Lucas, min_limit=0, max_limit=1)
        self.guards = UnitType(self, WolfGuards)
        elita = [
            self.guards,
            UnitType(self, WolfGuardTerminators),
            UnitType(self, ThunderwolfCavalry)
        ]
        self.add_type_restriction(elita, 1, 1)

        self.claws = UnitType(self, BloodClaws)
        claws = [
            self.claws,
            UnitType(self, Swiftclaws),
            UnitType(self, Skyclaws)
        ]
        self.add_type_restriction(claws, 3, 5)

        self.hunters = UnitType(self, GreyHunters)

        troops = [
            self.hunters,
            UnitType(self, LandSpeederSquadron)
        ]
        self.add_type_restriction(troops, 4, 6)
        self.fangs = UnitType(self, LongFangs, min_limit=1, max_limit=2)
        UnitType(self, WolfScouts, min_limit=1, max_limit=1)
        UnitType(self, LoneWolf, min_limit=0, max_limit=2)

    def check_rules(self):
        super(Blackmanes, self).check_rules()
        units = []
        for u in (self.guards, self.claws, self.hunters, self.fangs, ):
            units.extend(u.units)
        for unit in units:
            if unit.roster.discount != self.discount:
                unit.roster.discount = self.discount
                unit.check_rules_chain()


class ChampionsWulfen(WulfenFormation):
    army_id = 'space_wolves_v4_champions_wulfen'
    army_name = 'The Champions of Fenris (Wulfen)'

    def __init__(self):
        super(ChampionsWulfen, self).__init__()
        hq = [
            UnitType(self, Logan),
            UnitType(self, BattleLeader)
        ]
        self.add_type_restriction(hq, 1, 1)
        UnitType(self, Arjac, min_limit=0, max_limit=1)

        elita = [
            UnitType(self, WolfGuards),
            UnitType(self, WolfGuardTerminators),
            UnitType(self, ThunderwolfCavalry)
        ]
        self.add_type_restriction(elita, 2, 3)

        claws = [
            UnitType(self, BloodClaws),
            UnitType(self, Swiftclaws),
            UnitType(self, Skyclaws)
        ]
        self.add_type_restriction(claws, 3, 6)

        troops = [
            UnitType(self, GreyHunters),
            UnitType(self, LandSpeederSquadron)
        ]
        self.add_type_restriction(troops, 4, 8)
        UnitType(self, LongFangs, min_limit=2, max_limit=3)
        UnitType(self, WolfScouts, min_limit=1, max_limit=2)
        UnitType(self, LoneWolf, min_limit=0, max_limit=3)


class Greatpack(WulfenFormation):
    army_id = 'space_wolves_v4_wulfengreatpack'
    army_name = 'Greatpack'

    def __init__(self):
        super(Greatpack, self).__init__()
        lords = [
            UnitType(self, WolfLord),
            UnitType(self, Logan),
            UnitType(self, Ragnar),
            UnitType(self, Harald),
            UnitType(self, Krom)
        ]
        leader = [
            UnitType(self, BattleLeader)
        ]
        self.add_type_restriction(lords + leader, 1, 1)

        UnitType(self, Lucas, min_limit=0, max_limit=1)

        elita = [
            UnitType(self, WolfGuards),
            UnitType(self, WolfGuardTerminators),
            UnitType(self, ThunderwolfCavalry)
        ]
        self.add_type_restriction(elita, 0, 1)

        claws = [
            UnitType(self, BloodClaws),
            UnitType(self, Swiftclaws),
            UnitType(self, Skyclaws)
        ]
        self.add_type_restriction(claws, 1, 3)

        troops = [
            UnitType(self, GreyHunters),
            UnitType(self, LandSpeederSquadron)
        ]
        self.add_type_restriction(troops, 3, 5)
        UnitType(self, LongFangs, min_limit=1, max_limit=2)
        UnitType(self, WolfScouts, min_limit=0, max_limit=1)
        UnitType(self, LoneWolf, min_limit=0, max_limit=2)


class WulfenMurderpack(WulfenFormation):
    army_id = 'space_wolves_v4wulfen_murderpack'
    army_name = 'Wulfen Murderpack'

    def __init__(self):
        super(WulfenMurderpack, self).__init__()
        UnitType(self, Wulfen, min_limit=2, max_limit=5)


class Spear(WulfenFormation):
    army_id = 'space_wolves_v4_spear'
    army_name = 'Spear of Russ'

    def __init__(self):
        super(Spear, self).__init__()
        UnitType(self, IronPriest, min_limit=1, max_limit=3)
        raiders = [
            UnitType(self, Vindicators),
            UnitType(self, Predators),
            UnitType(self, Whirlwinds)
        ]
        self.add_type_restriction(raiders, 1, 3)

        landraiders = [
            UnitType(self, LandRaider),
            UnitType(self, LandRaiderCrusader),
            UnitType(self, LandRaiderRedeemer)
        ]
        self.add_type_restriction(landraiders, 1, 3)


class Wyrdstorm(WulfenFormation):
    army_id = 'space_wolves_v4_wyrdstorm'
    army_name = 'Wyrdstorm Brotherhood'

    def __init__(self):
        super(Wyrdstorm, self).__init__()
        prists = [
            UnitType(self, Njal),
            UnitType(self, RunePriest)
        ]
        self.add_type_restriction(prists, 2, 5)


class Ancients(WulfenFormation):
    army_id = 'space_wolves_v4_ancients'
    army_name = 'Ancients of the Fang'

    def __init__(self):
        super(Ancients, self).__init__()
        UnitType(self, IronPriest, min_limit=1, max_limit=1)
        UnitType(self, Dreadnought, min_limit=2, max_limit=5)


class Heralds(WulfenFormation):
    army_id = 'space_wolves_v4_heralds'
    army_name = 'Heralds of the Great Wolf'

    def __init__(self):
        super(Heralds, self).__init__()
        wolf = [
            UnitType(self, Ulrik),
            UnitType(self, WolfPriest)
        ]
        self.add_type_restriction(wolf, 1, 1)

        prists = [
            UnitType(self, Njal),
            UnitType(self, RunePriest)
        ]
        self.add_type_restriction(prists, 1, 1)
        UnitType(self, IronPriest, min_limit=1, max_limit=1)


class Wolfkin(WulfenFormation):
    army_id = 'space_wolves_v4_wulfen_wolfkin'
    army_name = 'Wolfkin'

    def __init__(self):
        super(Wolfkin, self).__init__()
        UnitType(self, WolfPack, min_limit=2, max_limit=5)


class Daggerfist(WulfenFormation):
    army_id = 'space_wolves_v4_daggerfist'
    army_name = 'Strike Force Daggerfist'

    def __init__(self):
        super(Daggerfist, self).__init__()
        UnitType(self, Murderfang, min_limit=1, max_limit=1)
        UnitType(self, Wulfen, min_limit=1, max_limit=1)
        UnitType(self, WolfGuards, min_limit=1, max_limit=1)
        UnitType(self, WolfGuardTerminators, min_limit=3, max_limit=3)
        UnitType(self, Stormfang, min_limit=1, max_limit=1)


class Deathpack(WulfenFormation):
    army_id = 'space_wolves_v4_deathpack'
    army_name = 'Space Wolves Deathpack'

    def __init__(self):
        super(Deathpack, self).__init__()
        UnitType(self, WolfLord, min_limit=1, max_limit=1)
        UnitType(self, GreyHunters, min_limit=1, max_limit=1)
        UnitType(self, ThunderwolfCavalry, min_limit=1, max_limit=1)


class Stormforce(ChampionFormation):
    army_id = 'space_wolves_v4_stormforce'
    army_name = 'Kingsguard Stormforce'

    class NoTransportGuard(WolfGuardTerminators):
        transport_allowed = False

    def __init__(self):
        super(Stormforce, self).__init__()
        self.logan = UnitType(self, Logan, min_limit=1, max_limit=1)
        self.terms = UnitType(self, self.NoTransportGuard, min_limit=1, max_limit=1)
        self.raiders = [
            UnitType(self, LandRaider),
            UnitType(self, LandRaiderRedeemer),
            UnitType(self, LandRaiderCrusader)
        ]
        self.add_type_restriction(self.raiders, 1, 1)
        UnitType(self, Stormfang, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Stormforce, self).check_rules()
        for log in self.logan.units:
            if not log.ride.any:
                self.error("Logan Grimnar must be embarked on his pimp ride")


class Brethen(ChampionFormation):
    army_id = 'space_wolves_v4_brethen'
    army_name = 'Brethen of the Fell-Handed'

    def __init__(self):
        super(Brethen, self).__init__()
        UnitType(self, Bjorn, min_limit=1, max_limit=1)
        self.dreds = UnitType(self, Dreadnought, min_limit=2, max_limit=2)

    def check_rules(self):
        super(Brethen, self).check_rules()
        for dred in self.dreds.units:
            if not dred.opt.venerable.value:
                self.error("Dreadnought must be upgraded to Venerable")


class Voidclaws(ChampionFormation):
    army_id = 'space_wolves_v4_voidclaws'
    army_name = 'Wolf Guard Void Claws'

    def __init__(self):
        super(Voidclaws, self).__init__()
        self.terms = UnitType(self, Stormforce.NoTransportGuard, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Voidclaws, self).check_rules()
        for term in self.terms.units:
            if not term.all_claws():
                self.error("All models in the Wolf Guard Terminators unit must be equipped with pair of claws")


class Council(ChampionFormation):
    army_id = 'space_wolves_v4_council'
    army_name = 'Grimnar\'s War Council'

    def __init__(self):
        super(Council, self).__init__()
        UnitType(self, Ulrik, min_limit=1, max_limit=1)
        UnitType(self, Njal, min_limit=1, max_limit=1)
        UnitType(self, RunePriest, min_limit=1, max_limit=1)
        UnitType(self, IronPriest, min_limit=1, max_limit=1)


class Shieldbrothers(ChampionFormation):
    army_id = 'space_wolves_v4_shieldbrothers'
    army_name = 'Arjac\'s Shieldbrothers'

    def __init__(self):
        super(Shieldbrothers, self).__init__()
        UnitType(self, Arjac, min_limit=1, max_limit=1)
        UnitType(self, Stormforce.NoTransportGuard, min_limit=1, max_limit=1)
        UnitType(self, LandRaiderCrusader, min_limit=1, max_limit=1)


class Thunderstrike(ChampionFormation):
    army_id = 'space_wolves_v4_thunderstrike'
    army_name = 'Wolf Guard Thunderstrike'

    class DropPodGuard(WolfGuards):
        transport_allowed = False

        def __init__(self, parent):
            super(Thunderstrike.DropPodGuard, self).__init__(parent)
            # drop pod cannot carry jet packers, correct?
            self.ride.used = self.ride.visible = False

    def __init__(self):
        super(Thunderstrike, self).__init__()
        UnitType(self, self.DropPodGuard, min_limit=1, max_limit=1)
        UnitType(self, DropPod, min_limit=1, max_limit=1)
        UnitType(self, Stormforce.NoTransportGuard, min_limit=1, max_limit=1)


class Champions(ChampionFormation, CompositeFormation):
    army_id = 'space_wolves_v4_champions'
    army_name = 'The Champions of Fenris'

    def __init__(self):
        super(Champions, self).__init__()
        Detachment.build_detach(self, Stormforce, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Brethen, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Voidclaws, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Council, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Shieldbrothers, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Thunderstrike, min_limit=1, max_limit=1)


class IceStorm(WulfenFormation):
    army_id = 'space_wolves_v4_ice_storm'
    army_name = 'Ice Storm Assault Pack'

    def __init__(self):
        super(IceStorm, self).__init__()

        class Wolves(Stormwolves):
            unit_min = 2
            unit_max = 2

        class Fangs(Stormfangs):
            unit_min = 2
            unit_max = 2

        UnitType(self, Wolves, min_limit=1, max_limit=1)
        UnitType(self, Stormfangs, min_limit=1, max_limit=1)


class FierceEyeFinest(Formation):
    army_name = 'The Fierce-Eye\'s Finest'
    army_id = 'space_wolves_v4_finest'

    def __init__(self):
        super(FierceEyeFinest, self).__init__()
        UnitType(self, KromDragongaze, min_limit=1, max_limit=1)
        UnitType(self, FierceEyeWolfGuard, min_limit=1, max_limit=1)
        UnitType(self, IronaxeGreyHunters, min_limit=1, max_limit=1)
        UnitType(self, RedfistBloodClaws, min_limit=1, max_limit=1)


class IronclawStrikeForce (Formation):
    army_name = 'Ironclaw Strike Force'
    army_id = 'space_wolves_v4_ironclaw_strike_force'

    def __init__(self):
        super(IronclawStrikeForce, self).__init__()
        UnitType(self, IronPriest, min_limit=1, max_limit=1)
        UnitType(self, Stormwolf, min_limit=1, max_limit=1)
        UnitType(self, WolfGuardTerminators, min_limit=1, max_limit=1)
        UnitType(self, BloodClaws, min_limit=1, max_limit=1)
        self.dreds = UnitType(self, Dreadnought, min_limit=1, max_limit=1)
        UnitType(self, WolfPack, min_limit=1, max_limit=1)

    def check_rules(self):
        super(IronclawStrikeForce, self).check_rules()
        for dred in self.dreds.units:
            if not dred.opt.venerable.value:
                self.error("Dreadnought must be upgraded to Venerable")
