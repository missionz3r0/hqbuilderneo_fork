__author__ = 'Denis Romanov'

from builder.core2 import OptionsList, Gear, OneOf,\
    SubUnit, ListSubUnit, UnitList, UpgradeUnit
from builder.games.wh40k.roster import Wh40kKillTeam
from builder.games.wh40k.imperial_armour.volume2.options\
    import SpaceMarinesBaseVehicle, SpaceMarinesBaseSquadron
from builder.games.wh40k.imperial_armour.volume2.transport\
    import IATransportedUnit
from .fast import WolfTransport, DropPod, Rhino, Razorback
from .armory import VehicleEquipment, BoltPistol, Chainsword,\
    PowerFist, PowerWeapon, PlasmaPistol, Special, Heavy, Armour,\
    TerminatorMelee, TerminatorRanged, Ranged, Melee, MeltaBombs
from builder.games.wh40k.unit import Unit


class LandRaider(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider'
    type_id = 'landraider_v4'

    def __init__(self, parent):
        super(LandRaider, self).__init__(
            parent=parent, points=250, tank=True, transport=True,
            gear=[
                Gear('Twin-linked Heavy Bolter'),
                Gear('Twin-linked Lascannon', count=2),
                Gear('Smoke Launchers'),
                Gear('Searchlight')
            ]
        )
        self.Options(self)

    class Options(VehicleEquipment):
        def __init__(self, parent):
            super(LandRaider.Options, self).__init__(parent=parent, blade=False)
            self.multimelta = self.variant('Multi-melta', 10 * self.freeflag)


class LandRaiderCrusader(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider Crusader'
    type_id = 'landraidercrusader_v4'

    def __init__(self, parent):
        super(LandRaiderCrusader, self).__init__(
            parent=parent, points=250, tank=True, transport=True,
            gear=[
                Gear('Twin-linked Assault Cannon'),
                Gear('Hurricane Bolter', count=2),
                Gear('Smoke Launchers'),
                Gear('Frag Assault Launcher'),
                Gear('Searchlight')
            ]
        )
        LandRaider.Options(self)


class LandRaiderRedeemer(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider Redeemer'
    type_id = 'landraiderredeemer_v4'

    def __init__(self, parent):
        super(LandRaiderRedeemer, self).__init__(
            parent=parent, points=240, tank=True, transport=True,
            gear=[
                Gear('Twin-linked Assault Cannon'),
                Gear('Flamestorm Cannon', count=2),
                Gear('Frag Assault Launcher'),
                Gear('Smoke Launchers'),
                Gear('Searchlight')
            ]
        )
        LandRaider.Options(self)


class Vindicators(Unit):
    type_name = "Vindicators"
    type_id = "vindicators_v4"

    class Vindicator(SpaceMarinesBaseVehicle, ListSubUnit):
        type_name = "Vindicator"
        type_id = "vindicator_v4"

        class Options(VehicleEquipment):
            def __init__(self, parent):
                super(Vindicators.Vindicator.Options, self).__init__(parent=parent)
                self.shield = self.variant('Siege shield', 10)

        def __init__(self, parent):
            super(Vindicators.Vindicator, self).__init__(parent=parent, points=120, gear=[
                Gear('Demolisher cannon'),
                Gear('Smoke launchers'),
                Gear('Searchlight'),
            ], tank=True)
            self.Options(self)

    def __init__(self, parent):
        super(Vindicators, self).__init__(parent)
        self.models = UnitList(self, self.Vindicator, 1, 3)

    def get_count(self):
        return self.models.count

    def get_unique_gear(self):
        return sum([unit.get_unique_gear() * unit.count for unit in self.models.units], [])


class Whirlwinds(Unit):
    type_name = "Whirlwinds"
    type_id = "Whirlwinds_v4"

    class Whirlwind(SpaceMarinesBaseVehicle, ListSubUnit):
        type_name = "Whirlwind"
        type_id = "Whirlwind_v4"

        def __init__(self, parent):
            super(Whirlwinds.Whirlwind, self).__init__(parent=parent, points=65, gear=[
                Gear('Whirlwind multiple missile launcher'),
                Gear('Smoke launchers'),
                Gear('Searchlight'),
            ], tank=True)
            self.opt = VehicleEquipment(self)

    def __init__(self, parent):
        super(Whirlwinds, self).__init__(parent)
        self.models = UnitList(self, self.Whirlwind, 1, 3)

    def get_count(self):
        return self.models.count

    def get_unique_gear(self):
        return sum([unit.get_unique_gear() * unit.count for unit in self.models.units], [])


class Predators(Unit):
    type_name = "Predators"
    type_id = "predators_v4"

    class Predator(SpaceMarinesBaseVehicle, ListSubUnit):
        type_name = "Predator"
        type_id = "predator_v3"

        class Turret(OneOf):
            def __init__(self, parent):
                super(Predators.Predator.Turret, self).__init__(parent=parent, name='Turret')
                self.autocannon = self.variant('Autocannon', 0)
                self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

        class Sponsons(OptionsList):
            def __init__(self, parent):
                super(Predators.Predator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
                self.sponsonswithheavybolters = self.variant('Sponsons with heavy bolters', 20,
                                                             gear=Gear('Heavy bolter', count=2))
                self.sponsonswithlascannons = self.variant('Sponsons with lascannons', 40,
                                                           gear=Gear('Lascannon', count=2))

        def __init__(self, parent):
            super(Predators.Predator, self).__init__(parent=parent, points=75, gear=[
                Gear('Smoke launchers'),
                Gear('Searchlight'),
            ], tank=True)
            self.turret = self.Turret(self)
            self.side = self.Sponsons(self)
            self.opt = VehicleEquipment(self)

    def __init__(self, parent):
        super(Predators, self).__init__(parent)
        self.models = UnitList(self, self.Predator, 1, 3)

    def get_count(self):
        return self.models.count

    def get_unique_gear(self):
        return sum([unit.get_unique_gear() * unit.count for unit in self.models.units], [])


class Stormfang(SpaceMarinesBaseVehicle):
    type_name = 'Stormfang'
    type_id = 'storfang_v4'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Stormfang.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Two Twin-linked heavy bolters', 0,
                         gear=[Gear('Twin-linked heavy bolter', count=2)])
            self.variant('Skyhammer missile launcher', 0)
            self.variant('Two twin-linked Multi-meltas', 20,
                         gear=[Gear('Twin-linked multi-melta', count=2)])

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Stormfang.Weapon2, self).__init__(parent, '')
            self.variant("Two stormstrike missiles", 0,
                         gear=[Gear("Stormstrike missile", count=2)])
            self.variant("Twin-linked lascannon", 15)

    def __init__(self, parent):
        super(Stormfang, self).__init__(parent=parent, points=220, gear=[
            Gear('Ceramite plating'),
            Gear('Helfrost destructor')
        ], transport=True)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class Stormfangs(SpaceMarinesBaseSquadron):
    type_name = 'Stormfang Gunships'
    type_id = 'stormfangs_v3'
    unit_class = Stormfang
    unit_max = 4


class WolfGuard(UpgradeUnit):

    class WeaponMelee(TerminatorMelee, Ranged, Melee, Chainsword):
        pass

    class WeaponRanged(TerminatorRanged, Ranged, Melee, BoltPistol):
        pass

    def make_upgraded_options(self):
        super(WolfGuard, self).make_upgraded_options()
        self.ar = Armour(self, tda=15)
        mle = self.WeaponMelee(self, name="Weapon", armour=self.ar)
        rng = self.WeaponRanged(self, name="Weapon", armour=self.ar)
        opt = MeltaBombs(self)
        self.upgraded_options += [self.ar, mle, rng, opt]

    def __init__(self, parent):
        super(WolfGuard, self).__init__(
            parent, upgraded_name='Wolf Guard Pack Leader',
            upgrade_cost=10)

    def check_rules(self):
        super(WolfGuard, self).check_rules()
        if isinstance(self.parent.roster, Wh40kKillTeam):
            self.ar.tda.used = self.ar.tda.active = False


class LongFangs(IATransportedUnit):
    type_name = "Long Fangs"
    type_id = "long_fangs_v4"

    model_gear = [Gear('Power Armour'), Gear('Frag Grenades'), Gear('Krak Grenades')]
    model_points = 15

    class LongFangAncient(Unit):
        type_name = 'Long Fang Ancient'

        class Weapon1(PowerFist, PowerWeapon,
                      PlasmaPistol, BoltPistol):
            pass

        class Weapon2(Special, PowerFist, PowerWeapon,
                      PlasmaPistol, Chainsword):
            pass

        def __init__(self, parent):
            super(LongFangs.LongFangAncient, self).__init__(parent, self.type_name,
                                                            15, gear=LongFangs.model_gear)
            self.wep1 = self.Weapon1(self, "Weapon")
            self.wep2 = self.Weapon2(self, "")
            MeltaBombs(self)

    class LongFang(ListSubUnit):
        type_name = "Long Fang"

        class Weapon(Heavy, BoltPistol):
            pass

        def __init__(self, parent):
            super(LongFangs.LongFang, self).__init__(parent, self.type_name,
                                                     15,
                                                     gear=LongFangs.model_gear + [Gear("Chainsword")])
            self.wep = self.Weapon(self, name="Ranged weapon")

    class GuardAncient(WolfGuard, LongFangAncient):
        pass

    class GuardFang(WolfGuard, LongFang):
        @ListSubUnit.count_gear
        def is_guard(self):
            return self.up.any

    def __init__(self, parent):
        super(LongFangs, self).__init__(parent)
        self.leader = SubUnit(self, self.GuardAncient(None))
        self.marines = UnitList(self, self.GuardFang, 1, 5)
        self.transport = WolfTransport(self, transport_types=[
            Rhino,
            Razorback,
            DropPod
        ])

    def get_count(self):
        return self.marines.count + 1

    def check_rules(self):
        super(LongFangs, self).check_rules()
        guards = self.leader.unit.up.any +\
                 sum(u.is_guard() for u in self.marines.units)
        if guards > 1:
            self.error("Only one model can be upgraded to Wolf Guard Pack Leader")


from builder.games.wh40k.imperial_armour.dataslates.space_marines import BaseDeimos


class DeimosVindicator(SpaceMarinesBaseVehicle, BaseDeimos):
    def get_options(self):
        class Options(VehicleEquipment):
            def __init__(self, parent):
                super(Options, self).__init__(parent)
                self.variant('Siege shield', 10)
        return Options
