from builder.core2 import OneOf, OptionsList

__author__ = 'Denis Romanow'


from builder.core2 import *
from builder.games.wh40k.roster import Unit


class Vehicle(OptionsList):
    def __init__(self, parent, sentinel=False, open_tank=False, lights=True, smoke=True):
        super(Vehicle, self).__init__(parent, name='Options')
        if lights:
            self.variant('Searchlight', 1)
        self.variant('Relic plating', 3)
        self.dozen = not sentinel and self.variant('Dozer blade', 5)
        self.sb = not sentinel and self.variant('Storm bolter', 5)
        self.hs = not sentinel and self.variant('Heavy stubber', 5)
        self.variant('Recovery gear', 5)
        if smoke:
            self.variant('Smoke launchers', 5)
        self.variant('Extra armour', 10)
        self.fb = not sentinel and self.variant('Fire barrels', 10)
        self.variant('Hunter-killer missile', 10)
        self.variant('Camo netting', 15)
        self.ecc = open_tank and self.variant('Enclosed crew compartment', 15)
        self.variant('Augur array', 25)

    def check_rules(self):
        super(Vehicle, self).check_rules()
        if self.sb and self.hs:
            self.process_limit([self.hs, self.sb], 1)


class Chimera(Unit):
    type_name = 'Chimera'
    type_id = 'chimera_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Chimera')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Multi-laser', 0)
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)

    def __init__(self, parent):
        super(Chimera, self) .__init__(parent=parent, points=65, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.Weapon1(self)
        self.Weapon2(self)
        Vehicle(self, lights=False, smoke=False)


class Taurox(Unit):
    type_name = 'Taurox'
    type_id = 'taurox_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Taurox')

    def __init__(self, parent):
        super(Taurox, self) .__init__(parent=parent, points=50, gear=[Gear('Twin-linked autocannon')])
        Vehicle(self)


class TauroxPrime(Unit):
    type_name = 'Taurox Prime'
    type_id = 'taurox_prime_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Taurox-Prime')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TauroxPrime.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Taurox battle cannon', 0)
            self.variant('Twin-linked Taurox gatling cannon', 10)
            self.variant('Taurox missile launcher', 20)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TauroxPrime.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Twin-linked hot-shot volley gun', 0)
            self.variant('Twin-linked autocannon', 0)

    def __init__(self, parent):
        super(TauroxPrime, self) .__init__(parent=parent, points=80)
        self.Weapon1(self)
        self.Weapon2(self)
        Vehicle(self)


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(parent=parent, name='Transport', limit=1)
        SubUnit(self, Chimera(parent=None))
        SubUnit(self, Taurox(parent=None))


class TempestusTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(TempestusTransport, self).__init__(parent=parent, name='Transport', limit=1)
        SubUnit(self, Chimera(parent=None))
        SubUnit(self, TauroxPrime(parent=None))


class Melee(OneOf):
    def __init__(self, parent, name, relic=False):
        super(Melee, self).__init__(parent, name=name)
        self.variant('Close combat weapon', 0)
        self.variant('Power weapon', 15)
        self.variant('Power fist', 25)
        self.blade = relic and self.variant('The Blade of Conquest', 25)
        self.relics = [self.blade] if relic else []

    def get_unique_gear(self):
        if self.cur in self.relics:
            return self.description
        return []


class HeavyWeapon(OptionsList):
    def __init__(self, parent, name):
        super(HeavyWeapon, self).__init__(parent, name=name)
        self.heavy = [
            self.variant('Mortar', 5),
            self.variant('Autocannon', 10),
            self.variant('Heavy bolter', 10),
        ]
        self.ml = self.variant('Missile launcher', 15)
        self.heavy.append(self.ml)
        self.flakk = self.variant('Flakk missiles', 10)
        self.heavy.append(self.variant('Lascannon', 20))

    def check_rules(self):
        super(HeavyWeapon, self).check_rules()
        self.process_limit(self.heavy, 1)
        self.flakk.visible = self.flakk.used = self.ml.value
        if not self.any:
            self.parent.error('Weapons Team must take one item from the Heavy Weapons list.')


class SpecWeapon(OneOf):
    weapon = [
        dict(name='Sniper rifle', points=2),
        dict(name='Flamer', points=5),
        dict(name='Grenade launcher', points=5),
        dict(name='Meltagun', points=10),
        dict(name='Plasma gun', points=15),
    ]

    def __init__(self, parent, name):
        super(SpecWeapon, self).__init__(parent, name=name)
        self.spec = [self.variant(o['name'], o['points']) for o in self.weapon]


class Ranged(OneOf):
    def __init__(self, parent, name):
        super(Ranged, self).__init__(parent, name=name)
        self.variant('Laspistol', 0)
        self.variant('Bolt pistol', 1)
        self.variant('Boltgun', 1)
        self.variant('Plasma pistol', 15)
