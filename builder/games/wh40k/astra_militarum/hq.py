__author__ = 'Denis Romanow'
__maintainer__ = 'Ivan Truskov'

from builder.games.wh40k.astra_militarum.armory import Melee, HeavyWeapon, SpecWeapon, Ranged
from builder.games.wh40k.imperial_armour.volume1.heavy import Trojan
from builder.core2 import *
from .armory import Transport
from .heavy import LemanRuss
from builder.games.wh40k.roster import Unit
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianRelicsHoly, CadianWeaponHoly,\
    CadianWeaponArcana


class HolyMelee(CadianWeaponHoly, Melee):
    def __init__(self, *args, **kwargs):
        super(HolyMelee, self).__init__(*args, **kwargs)
        self.relics += [self.worthy_blade]


class Yarrick(Unit):
    type_name = 'Commissar Yarrick'
    type_id = 'commissaryarrick_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Commissar-Yarrick')

    def __init__(self, parent):
        super(Yarrick, self).__init__(parent=parent, points=145, unique=True, gear=[
            Gear('Carapace armour'),
            Gear('Storm bolter'),
            Gear('Power klaw'),
            Gear('Close combat weapon'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Force field'),
            Gear('Bale eye')
        ])


class SpecIssues(OptionsList):
    def __init__(self, parent, name):
        super(SpecIssues, self).__init__(parent, name=name)
        self.variant('Krak grenades', 2)
        self.variant('Melta bombs', 5)
        self.variant('Camo gear', 10)


class Armour(OneOf):
    def __init__(self, parent):
        super(Armour, self).__init__(parent, 'Armour')
        self.variant('Flak armour', 0)
        self.variant('Carapace armour', 5)


class AMMelee(CadianWeaponHoly, Melee):
    def __init__(self, *args, **kwargs):
        super(AMMelee, self).__init__(*args, **kwargs)
        self.relics += self.worthy_blade


class CadianHeirlooms(OptionsList):
    def __init__(self, parent, commander):
        super(CadianHeirlooms, self).__init__(parent,
                                              name='Heirlooms of Cadia',
                                              limit=1)
        self.variant('Celeritas', 10)
        self.variant('The Iron Left', 25)
        if commander:
            self.variant("Volkov's Cane", 10)
        self.variant('Wrath of Cadia', 5)


class CommanderHeirlooms(OptionsList):
    def __init__(self, parent):
        super(CommanderHeirlooms, self).__init__(parent=parent,
                                                 name='Heirlooms of Conquest',
                                                 limit=1)
        self.variant('The Laurels of Command', 25)
        self.variant('The Tactical Auto-Reliquary of Tyberius', 25)
        self.variant('The Deathmask of Ollanius', 30)
        self.variant('Kurov\'s Aquila', 60)


class CommissarHeirlooms(OptionsList):
    def __init__(self, parent):
        super(CommissarHeirlooms, self).__init__(parent=parent, name='Heirlooms of Conquest', limit=1)
        self.variant('The Deathmask of Ollanius', 30)


class Commander(Unit):
    type_name = 'Company Commander'

    class Ranged(Ranged):
        def __init__(self, parent, name):
            super(Commander.Ranged, self).__init__(parent, name=name)
            self.variant('Shotgun', 0)

    class ArcanaRanged(CadianWeaponArcana, Ranged):
        def get_unique_gear(self):
            if self.cur == self.qann:
                return self.description
            return []

    def __init__(self, parent):
        super(Commander, self).__init__(parent, gear=[Gear('Refractor field'), Gear('Frag grenades')])
        self.melee = HolyMelee(self, 'Weapon', relic=True)
        self.ranged = self.ArcanaRanged(self, '')
        self.armour = Armour(self)
        self.iss = SpecIssues(self, name='Options')
        self.relic = CommanderHeirlooms(self)
        self.relic2 = CadianHeirlooms(self, True)
        self.relic3 = CadianRelicsHoly(self, 'Cadian Relics', limit=1)

    def check_rules(self):
        super(Commander, self).check_rules()
        self.relic2.used = self.relic2.visible = self.roster.is_cadia()
        self.relic3.used = self.relic3.visible = any(op.visible for op in self.relic3.options)

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.melee.get_unique_gear() + self.relic.description +\
            self.relic2.description + self.relic3.description +\
            self.ranged.get_unique_gear()


class BaseVeteran(Unit):

    base_unit = None

    def build_description(self):
        desc = super(BaseVeteran, self).build_description()
        desc.add(self.base_unit.vet_opt.description)
        if not self.base_unit.vet_opt.carapace.value:
            desc.add(Gear('Flak armour'))
        return desc

    def build_points(self):
        return super(BaseVeteran, self).build_points() + self.base_unit.vet_opt.points


class Veteran(BaseVeteran, ListSubUnit):
    type_name = 'Veteran'

    class BaseWeapon(OneOf):
        def __init__(self, parent, name):
            super(Veteran.BaseWeapon, self).__init__(parent, name=name)
            self.variant('Lasgun', 0)
            self.variant('Laspistol and close combat weapon', 0, gear=[Gear('Laspistol'), Gear('Close combat weapon')])
            self.hf = self.variant('Heavy flamer', 10)

    class Weapon(SpecWeapon, BaseWeapon):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Veteran.Options, self).__init__(parent, name='Options', limit=1)
            self.vox = self.variant('Vox-caster', 5)
            self.banner = self.variant('Regimental standard', 15)
            self.relic_banner = self.variant('Standard of the Lost 113', 30)
            self.medic = self.variant('Medi-pack', 15)

        def check_rules(self):
            Veteran.Options.process_limit([self.banner, self.relic_banner], 1)
            self.relic_banner.used = self.relic_banner.visible =\
                                     self.parent.roster.is_cadia()

    def __init__(self, parent):
        super(Veteran, self).__init__(parent, gear=[Gear('Frag grenades')])
        self.wep = self.Weapon(self, name='Weapon')
        self.opt = self.Options(self)
        self.base_unit = self.root_unit

    def check_rules(self):
        super(Veteran, self).check_rules()
        for o in [self.wep.hf] + self.wep.spec:
            o.active = not self.opt.any
        self.opt.visible = self.opt.used = self.wep.cur not in [self.wep.hf] + self.wep.spec

    @ListSubUnit.count_gear
    def count_vox(self):
        return self.opt.vox.value

    @ListSubUnit.count_gear
    def count_flame(self):
        return self.wep.hf == self.wep.cur

    @ListSubUnit.count_gear
    def count_banner(self):
        return self.opt.banner.value or self.opt.relic_banner.value

    @ListSubUnit.count_unique
    def count_relic(self):
        return ['Standard of the Lost 113'] if self.opt.relic_banner.value else []

    @ListSubUnit.count_gear
    def count_medic(self):
        return self.opt.medic.value


class VeteranWeaponTeam(BaseVeteran):
    type_name = 'Veteran Weapon Team'

    def __init__(self, parent, root_unit):
        super(VeteranWeaponTeam, self).__init__(parent, gear=[Gear('Frag grenades'), Gear('Lasgun')])
        self.base_unit = root_unit
        self.wep = HeavyWeapon(self, name='Weapon')


class Astropath(Unit):
    type_name = 'Astropath'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Astropath.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Close combat weapon', 0)
            self.variant('Laspistol', 0)

    def __init__(self, parent, points=25):
        super(Astropath, self).__init__(parent, points=points, gear=[Gear('Flak armour'), Gear('Frag grenades')])
        self.Weapon(self)


class MasterOfOrdnance(Astropath):
    type_name = 'Master of Ordnance'

    def __init__(self, parent):
        super(MasterOfOrdnance, self).__init__(parent, points=20)


class OfficerFleet(Astropath):
    type_name = 'Officer of the Fleet'

    def __init__(self, parent):
        super(OfficerFleet, self).__init__(parent, points=20)


class CommandSquad(Unit):
    type_name = 'Company Command Squad'
    type_id = 'company_command_squad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Company-Command-Squad')

    allow_creed = True

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent, points=60)
        self.commander = SubUnit(self, Commander(self))
        self.chars = self.Commanders(self)
        self.veterans = UnitList(self, Veteran, 2, 4, start_value=4)
        self.vet_opt = self.VeteranOptions(self)
        self.members = self.Members(self)
        self.transport = Transport(self)

    class Commanders(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.Commanders, self).__init__(parent, name='')
            if parent.allow_creed:
                self.creed = self.variant(
                    'Lord Castellan Creed', 80, gear=[UnitDescription(
                        'Lord Castellan Creed', points=80, options=[
                            Gear('Carapace armour'),
                            Gear('Hot-shot laspistol', count=2),
                            Gear('Frag grenades'),
                            Gear('Refractor field'),
                        ]
                    )])
                self.kell = self.variant(
                    'Colour Sergeant Kell', 75, gear=[UnitDescription(
                        'Colour Sergeant Kell', points=75, options=[
                            Gear('Carapace armour'),
                            Gear('Laspistol'),
                            Gear('Power fist'),
                            Gear('Power sword'),
                            Gear('Frag grenades'),
                            Gear('Regimental standard'),
                        ]
                    )])
            self.straken = self.variant(
                'Colonel \'Iron Hand\' Straken', 130, gear=[UnitDescription(
                    'Colonel \'Iron Hand\' Straken', points=130, options=[
                        Gear('Flak armour'),
                        Gear('Plasma pistol'),
                        Gear('Shotgun'),
                        Gear('Close combat weapon'),
                        Gear('Frag grenades'),
                        Gear('Refractor Field'),
                    ]
                )])
            self.nork = self.variant('Nork Deddog', 85, gear=[UnitDescription(
                'Nork Deddog', points=85, options=[
                    Gear('Carapace armour'),
                    Gear('Ripper gun'),
                    Gear('Frag grenades'),
                ]
            )])

        def check_rules(self):
            super(CommandSquad.Commanders, self).check_rules()
            if self.parent.allow_creed:
                self.process_limit([self.creed, self.straken], limit=1)
                self.kell.visible = self.kell.used = self.creed.value
            self.straken.visible = self.straken.used = not self.roster.is_cadia()
            self.straken.value = self.straken.value and not self.roster.is_cadia()
            self.nork.visible = self.nork.used = not self.roster.is_cadia()
            self.nork.value = self.nork.value and not self.roster.is_cadia()
            if self.parent.allow_creed:
                self.parent.commander.visible = self.parent.commander.used = not (self.straken.value or self.creed.value)

    class Members(OptionalSubUnit):
        def __init__(self, parent):
            super(CommandSquad.Members, self).__init__(parent, name='')
            self.heavy = SubUnit(self, VeteranWeaponTeam(None, parent))
            self.astropath = SubUnit(self, Astropath(None))
            SubUnit(self, MasterOfOrdnance(None))
            SubUnit(self, OfficerFleet(None))

    class VeteranOptions(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.VeteranOptions, self).__init__(parent, name='Veteran options', used=False)
            self.variant('Krak grenades', 1, per_model=True)
            self.carapace = self.variant('Carapace armour', 2, per_model=True)
            self.variant('Camo gear', 2, per_model=True)

    def check_rules(self):
        super(CommandSquad, self).check_rules()
        kell = 0
        if self.allow_creed:
            kell = self.chars.kell.used and self.chars.kell.value
        if sum(u.count_banner() for u in self.veterans.units) + kell > 1:
            self.error('Only one veteran may have regimental standard')
        if sum(u.count_medic() for u in self.veterans.units) > 1:
            self.error('Only one veteran may have medi-pack')
        if sum(u.count_vox() for u in self.veterans.units) > 1:
            self.error('Only one veteran may have vox-caster')
        if sum(u.count_flame() for u in self.veterans.units) > 1:
            self.error('Only one veteran may have heavy flamer')
        if self.members.heavy.used and self.veterans.count != 2 - kell:
            self.error('Company Command Squad must include 1 Veteran Weapon Team and {} Veterans'.format(2 - kell))
        elif not self.members.heavy.used and self.veterans.count != 4 - kell:
            self.error('Company Command Squad must include {} Veterans'.format(4 - kell))

    def get_count(self):
        return 1 + self.veterans.count + self.members.count\
            + self.chars.nork.value

    def get_unique_gear(self):
        return (self.commander.unit.get_unique_gear() if self.commander.used else []) + self.chars.description + sum([u.count_relic() for u in self.veterans.units], [])

    def count_charges(self):
        if self.members.astropath.used:
            return 1
        else:
            return 0

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(CommandSquad, self).build_statistics()))


class Commissar(Unit):
    type_id = 'commissar_v1'
    type_name = 'Commissar'

    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Commissar')

    class Pistol(OneOf):
        def __init__(self, parent, name):
            super(Commissar.Pistol, self).__init__(parent, name=name)
            self.variant('Bolt pistol', 0)
            self.variant('Boltgun', 0)
            self.variant('Plasma pistol', 15)

    def __init__(self, parent):
        super(Commissar, self).__init__(parent=parent, points=25, gear=[
            Gear('Flak armour'),
            Gear('Frag grenades'),
            Gear('Krak grenades')
        ])

        Melee(self, 'Weapon')
        self.Pistol(self, '')

    def build_statistics(self):
        return {'Models': 1}


class LordCommissar(Unit):
    type_name = 'Lord Commissar'
    type_id = 'lord_commissar_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Lord-Commissar')

    class Pistol(Commissar.Pistol):
        def __init__(self, parent, name):
            super(LordCommissar.Pistol, self).__init__(parent, name=name)
            self.variant('The Emperor\'s Benediction', 5)

    class ArcanaRanged(CadianWeaponArcana, Pistol):
        def get_unique_gear(self):
            if self.cur == self.qann:
                return self.description
            return []

    def __init__(self, parent):
        super(LordCommissar, self).__init__(parent=parent, points=65, gear=[
            Gear('Refractor field'),
            Gear('Frag grenades'),
            Gear('Krak grenades')
        ])

        self.melee = HolyMelee(self, 'Weapon', relic=True)
        self.ranged = self.ArcanaRanged(self, '')
        Armour(self)
        SpecIssues(self, name='Options')
        self.relic = CommissarHeirlooms(self)
        self.relic2 = CadianHeirlooms(self, False)
        self.relic3 = CadianRelicsHoly(self, 'Cadian Relics', limit=1)

    def check_rules(self):
        super(LordCommissar, self).check_rules()
        self.relic2.used = self.relic2.visible = self.roster.is_cadia()
        self.relic3.used = self.relic3.visible = any(op.visible for op in self.relic3.options)

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.melee.get_unique_gear() + self.relic.description +\
            self.relic2.description + self.relic3.description +\
            self.ranged.get_unique_gear()


class Priest(Unit):
    type_name = 'Ministorum Priest'
    type_id = 'ministorumpriest_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Ministorum-Priest')

    def __init__(self, parent):
        super(Priest, self).__init__(parent=parent, points=25, gear=[
            Gear('Flak armour'),
            Gear('Frag grenades'),
            Gear('Close combat weapon'),
            Gear('Rosarius'),
            Gear('Laspistol')
        ])
        self.Weapon(self)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Priest.Weapon, self).__init__(parent=parent, name='Weapon', limit=1)
            self.variant('Autogun', 0)
            self.variant('Plasma gun', 15)


class PrimarisPsyker(Unit):
    type_name = 'Primaris Psyker'
    type_id = 'primarispsyker_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Primaris-Psyker')

    def __init__(self, parent):
        super(PrimarisPsyker, self).__init__(parent=parent, points=50, gear=[
            Gear('Flak armour'),
            Gear('Laspistol'),
            Gear('Force weapon'),
            Gear('Frag grenades'),
            Gear('Refractor field')
        ])
        self.psy = self.Level(self)

    class Level(OneOf):
        def __init__(self, parent):
            super(PrimarisPsyker.Level, self).__init__(parent=parent, name='Psyker')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(PrimarisPsyker, self).build_statistics())


class Techpriest(Unit):
    type_name = 'Enginseer'
    type_id = 'techpriestenginseer_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Enginseer')

    def __init__(self, parent):
        super(Techpriest, self).__init__(parent=parent, points=40, gear=[
            Gear('Power armour'),
            Gear('Laspistol'),
            Gear('Power axe'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Servo-arm')
        ])
        self.Options(self)
        self.transport = self.Transport(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Techpriest.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Trojan(parent=None))

        def check_rules(self):
            super(Techpriest.Transport, self).check_rules()
            try:
                self.used = self.visible = self.root.imperial_armour_on
            except Exception:
                self.used = self.visible = False

    class Options(OptionsList):
        def __init__(self, parent):
            super(Techpriest.Options, self).__init__(parent=parent, name='Options')
            self.variant('Melta bombs', 5)

    def build_statistics(self):
        return self.note_transport(super(Techpriest, self).build_statistics())


class Servitors(Unit):
    type_name = 'Servitors'
    type_id = 'servitors_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Servitors')

    model = UnitDescription('Servitor', points=10)

    def __init__(self, parent):
        super(Servitors, self).__init__(parent=parent)
        self.serv = self.Servitor(self, 'Servitor', 1, 5, 10)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=2, points=g['points'],
                  gear=Servitors.model.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Heavy bolter', points=10),
                dict(name='Multi-melta', points=10),
                dict(name='Plasma cannon', points=15),
            ]
        ]

    class Servitor(Count):
        @property
        def description(self):
            return Servitors.model.clone().add(Gear('Servo-arm')).set_count(self.cur -
                                                                            sum(o.cur for o in self.parent.spec))

    def check_rules(self):
        super(Servitors, self).check_rules()
        Count.norm_counts(0, min(2, self.serv.cur), self.spec)

    def get_count(self):
        return self.serv.cur


class TankCommander(Unit):
    type_name = 'Tank Commander'
    type_id = 'tank_commander_v1'
    allow_pask = False
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Tank-Commander')

    class Options(OptionsList):
        def __init__(self, parent):
            super(TankCommander.Options, self).__init__(parent=parent, name='')
            self.variant('Knight Commander Pask', 40, gear=[])

    class CadianRelic(OptionsList):
        def __init__(self, parent):
            super(TankCommander.CadianRelic, self).__init__(parent, 'Heirlooms of Cadia')
            self.variant("Kabe's Herald", 20)

    class Tank(LemanRuss, ListSubUnit):
        pass

    def __init__(self, parent):
        super(TankCommander, self).__init__(parent, points=30)
        self.pask = self.Options(self)
        self.herald = self.CadianRelic(self)
        self.command = SubUnit(self, LemanRuss(None, command=True))
        self.tanks = UnitList(self, self.Tank, 1, 2)

    def check_rules(self):
        super(TankCommander, self).check_rules()
        self.pask.used = self.pask.visible = self.allow_pask
        self.herald.used = self.herald.visible = self.roster.is_cadia()

    def get_count(self):
        return self.tanks.count + 1

    def build_description(self):
        desc = super(TankCommander, self).build_description()
        if self.pask.any:
            desc.name = 'Knight Commander Pask'
        desc.count = 1
        return desc

    def get_unique(self):
        return 'Knight Commander Pask' if self.pask.any else None

    def get_unique_gear(self):
        return self.herald.description
