__author__ = 'Ivan Truskov'

from builder.core2 import Unit, Gear
from .armory import RuntsSquigs


class Ghazghkull(Unit):
    type_name = 'Ghazghkull Thraka, The Beast of Armageddon'
    type_id = 'ghazghkull_v3'

    def __init__(self, parent):
        super(Ghazghkull, self).__init__(parent, 'Ghazghkull Thraka', 225, gear=[
            Gear('Cybork body'), Gear('Mega armour'),
            Gear('Big shoota'), Gear('Power klaw'),
            Gear('Bosspole'), Gear('Stikkbombs')
        ], unique=True)
        RuntsSquigs(self)
