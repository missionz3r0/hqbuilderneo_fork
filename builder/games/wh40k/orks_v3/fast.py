__author__ = 'Denis Romanow'

from builder.core2 import OneOf, Gear, OptionsList, Count,\
    UnitDescription, SubUnit, OptionalSubUnit, ListSubUnit,\
    UnitList
from builder.games.wh40k.unit import Unit, Squadron
from .armory import VehicleEquipment, Melee, Choppa


class Trukk(Unit):
    type_id = 'trukk_v1'
    type_name = 'Trukk'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Trukk.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Big shoota', 0)
            self.variant('Rokkit launcha', 0)

    def __init__(self, parent):
        super(Trukk, self).__init__(parent, name='Trukk', points=30)
        self.wep = self.Weapon(self)
        self.opt = VehicleEquipment(self)


class Stormboyz(Unit):
    type_id = 'storm_v1'
    type_name = 'Stormboyz'
    default_min = 5
    default_max = 30
    model_points = 9
    model_gear = [Gear('Slugga'), Gear('Choppa'), Gear('Stikkbombs'), Gear('Rokkit pack')]

    class Nob(Unit):
        class NobMelee(Melee, Choppa):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Stormboyz.Nob.Options, self).__init__(parent, 'Options')
                self.ha = self.variant('Bosspole', 5)

        def __init__(self, parent):
            super(Stormboyz.Nob, self).__init__(parent, name='Boss Nob', points=9 + 10, gear=[Gear('Slugga'), Gear('Stikkbombs'), Gear('Rokkit pack')])
            self.NobMelee(self, 'Melee weapon')
            self.Options(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Stormboyz.Boss, self).__init__(parent=parent, name='Boss', limit=1)
            SubUnit(self, Stormboyz.Nob(parent=None))

    def __init__(self, parent):
        super(Stormboyz, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Stormboy', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Stormboy', self.model_points, options=self.model_gear), per_model=True)
        self.boss = self.Boss(self)

    def get_count(self):
        return self.models.cur + self.boss.count

    def check_rules(self):
        self.models.min = self.default_min - self.boss.count
        self.models.max = self.default_max - self.boss.count


class Deffkoptas(Unit):
    type_id = 'koptas_v1'
    type_name = 'Deffkoptas'
    model_points = 30

    class Deffkopta(ListSubUnit):
        name = 'Deffkopta'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Deffkoptas.Deffkopta.Weapon, self).__init__(parent, 'Ranged weapon')
                self.variant("Twin-linked Big Shoota", 0)
                self.variant("Twin-linked Rokkit Launcha", 0)
                self.variant("Kustom Mega-Blasta", 0)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Deffkoptas.Deffkopta.Options, self).__init__(parent, 'Options')
                self.variant("Bigbomm", 15)
                self.variant("Buzzsaw", 25)

        def __init__(self, parent):
            super(Deffkoptas.Deffkopta, self).__init__(parent, self.name, gear=[Gear('Choppa')])
            self.wep = self.Weapon(self)
            self.opt = self.Options(self)
            self.base_points = self.root_unit.model_points

    def __init__(self, parent):
        super(Deffkoptas, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Deffkopta, 1, 5)

    def get_count(self):
        return self.models.count


class Dakkajet(Unit):
    type_id = 'dkjet_v1'
    type_name = 'Dakkajet'
    base_points = 110

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dakkajet.Options, self).__init__(parent, 'Options')
            self.wep = self.variant('Additional twin-linked supa-shoota', 20, gear=[Gear('Twin-linked Supa Shoota')])
            self.variant("Red paint job", 5)
            self.boss = self.variant("Flyboss", 15)

    def __init__(self, parent):
        super(Dakkajet, self).__init__(parent, self.type_name, self.base_points, gear=[Gear('Twin-linked Supa Shoota', count=2)])
        self.opt = self.Options(self)


class Dakkajets(Unit):
    type_name = 'Dakkajets'
    type_id = 'dkjets_v3'

    class Jet(Dakkajet, ListSubUnit):
        @ListSubUnit.count_gear
        def boss_flag(self):
            return self.opt.boss.value

    unit_min = 1
    unit_max = 4

    def __init__(self, parent):
        super(Dakkajets, self).__init__(parent)
        self.vehicles = UnitList(self, self.Jet, self.unit_min, self.unit_max)

    def get_count(self):
        return self.vehicles.count

    @property
    def boss_count(self):
        return sum(u.boss_flag() for u in self.vehicles.units)

    def check_rules(self):
        super(Dakkajets, self).check_rules()
        if self.boss_count > 1:
            self.error("No more then one Dakkajet may be upgraded to Flyboss")


class BlitzaBommer(Unit):
    type_id = 'blitza_v1'
    type_name = 'Blitza-Bommer'
    base_points = 135

    class Options(OptionsList):
        def __init__(self, parent):
            super(BlitzaBommer.Options, self).__init__(parent, 'Options')
            self.variant("Red paint job", 5)

    def __init__(self, parent):
        super(BlitzaBommer, self).__init__(parent, self.type_name, self.base_points, gear=[Gear('Big shoota'), Gear('Twin-linked supa shoota'), Gear('Boom bomb', count=2)])
        self.opt = self.Options(self)


class BlitzaBommers(Squadron):
    type_name = 'Blitza-Bommers'
    type_id = 'blitzas_v3'
    unit_class = BlitzaBommer
    unit_max = 4


class BurnaBommer(Unit):
    type_id = 'burnabmb_v1'
    type_name = 'Burna-Bommer'
    base_points = 115

    class Options(OptionsList):
        def __init__(self, parent):
            super(BurnaBommer.Options, self).__init__(parent, 'Options')
            self.variant("Red paint job", 5)

    def __init__(self, parent):
        super(BurnaBommer, self).__init__(parent, self.type_name, self.base_points, gear=[Gear('Twin-linked big shoota'), Gear('Twin-linked supa shoota'), Gear('Burna bomb', count=2)])
        self.wep = Count(self, 'Skorcha Missile', 0, 6, 10)
        self.opt = self.Options(self)


class BurnaBommers(Squadron):
    type_name = 'Burna-Bommers'
    type_id = 'burnabmbs_v3'
    unit_class = BurnaBommer
    unit_max = 4


class Warbikers(Unit):
    type_id = 'bikes_v1'
    type_name = 'Warbikers'
    default_min = 3
    default_max = 15
    model_points = 18
    model_gear = [Gear('Slugga'), Gear('Choppa'), Gear('Warbike')]

    class Nob(Unit):
        class NobMelee(Melee, Choppa):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Warbikers.Nob.Options, self).__init__(parent, 'Options')
                self.ha = self.variant('Bosspole', 5)

        def __init__(self, parent):
            super(Warbikers.Nob, self).__init__(parent, name='Boss Nob', points=18 + 10, gear=[Gear('Slugga'), Gear('Warbike')])
            self.NobMelee(self, 'Melee weapon')
            self.Options(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Warbikers.Boss, self).__init__(parent=parent, name='Boss', limit=1)
            SubUnit(self, Warbikers.Nob(parent=None))

    def __init__(self, parent):
        super(Warbikers, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Warbiker', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Warbiker', self.model_points, options=self.model_gear), per_model=True)
        self.boss = self.Boss(self)

    def get_count(self):
        return self.models.cur + self.boss.count

    def check_rules(self):
        self.models.min = self.default_min - self.boss.count
        self.models.max = self.default_max - self.boss.count


class Warbuggies(Unit):
    type_id = 'buggy_v1'
    type_name = 'Warbuggies'

    class Buggy(ListSubUnit):
        base_points = 25

        class Type(OneOf):
            def __init__(self, parent):
                super(Warbuggies.Buggy.Type, self).__init__(parent, 'Vehicle type')
                self.variant('Warbuggy', 0)
                self.variant('Wartrakk', 5)
                self.scr = self.variant('Skorcha', 10)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Warbuggies.Buggy.Options, self).__init__(parent, name='Options')
                self.variant('Red paint job', 5)
                self.variant('Extra armour', 10)
                self.variant('Grot riggers', 10)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Warbuggies.Buggy.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Twin-linked big shoota', 0)
                self.variant('Twin-linked rokkit launcha', 0)

        def __init__(self, parent):
            super(Warbuggies.Buggy, self).__init__(parent, points=self.base_points)
            self.buggy = self.Type(self)
            self.wep = self.Weapon(self)
            self.opt = self.Options(self)

        def check_rules(self):
            self.wep.used = self.wep.visible = self.buggy.cur != self.buggy.scr

        def build_description(self):
            desc = UnitDescription(self.buggy.cur.title, self.points, count=self.count)
            desc.add(self.opt.description)
            if self.buggy.cur == self.buggy.scr:
                desc.add(Gear('Skorcha'))
            else:
                desc.add(self.wep.description)
            return desc

    def __init__(self, parent):
        super(Warbuggies, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Buggy, 1, 5)

    def get_count(self):
        return self.models.count


class WazbomBlastajets(Unit):
    type_name = 'Wazbom Blastajets'
    type_id = 'wazboms_v3'

    class SingleWazbom(Unit):
        type_name = 'Wazbom Blastajet'
        type_id = 'wazbom_v3'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(WazbomBlastajets.SingleWazbom.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Twin-linked kustom mega-kannon')
                self.variant('Twin-linked tellyport mega-blasta', 5)

        class Equipment(OneOf):
            def __init__(self, parent):
                super(WazbomBlastajets.SingleWazbom.Equipment, self).__init__(parent, '')
                self.variant('Stikkbomb flinga')
                self.field = self.variant('Kustom force field', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(WazbomBlastajets.SingleWazbom.Options, self).__init__(parent, 'Options')
                self.variant('Gitbusta turret with tein-linked supa shoota', 20,
                             gear=[Gear('Twin-linked supa shoota')])
                self.variant('Red paint job', 5)

        def __init__(self, parent):
            super(WazbomBlastajets.SingleWazbom, self).__init__(parent, points=140, gear=[Gear('Smasha gun')])
            self.Weapon(self)
            self.eq = self.Equipment(self)
            self.Options(self)

    class Wazbom(SingleWazbom, ListSubUnit):
        pass

    unit_min = 1
    unit_max = 4

    def __init__(self, parent):
        super(WazbomBlastajets, self).__init__(parent)
        self.vehicles = UnitList(self, self.Wazbom, self.unit_min, self.unit_max)

    def get_count(self):
        return self.vehicles.count
