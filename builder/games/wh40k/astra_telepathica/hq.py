from builder.core2 import *
from builder.games.wh40k.roster import Unit


class Astropath(Unit):
    type_id = 'astra_telepathica_astropath_v1'
    type_name = 'Astropath'

    class Level(OneOf):
        def __init__(self, parent):
            super(Astropath.Level, self).__init__(parent=parent, name='Psyker')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Astropath.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Close combat weapon', 0)
            self.variant('Laspistol', 0)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Astropath.Options, self).__init__(parent=parent, name='Options')
            self.variant('Digital weapons', 10)
            self.variant('Refractor field', 10)

    def __init__(self, parent, points=25):
        super(Astropath, self).__init__(parent, points=points, gear=[Gear('Frag grenades')])
        self.Weapon(self)
        self.psy = self.Level(self)
        self.Options(self)

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Astropath, self).build_statistics())


class PrimarisPsyker(Unit):
    type_name = 'Primaris Psyker'
    type_id = 'astra_telepathica_primaris_psyker_v1'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Primaris-Psyker')

    def __init__(self, parent):
        super(PrimarisPsyker, self).__init__(parent=parent, points=50, gear=[
            Gear('Laspistol'),
            Gear('Force weapon'),
            Gear('Frag grenades'),
            Gear('Refractor field')
        ])
        self.psy = self.Level(self)
        self.Options(self)

    class Level(OneOf):
        def __init__(self, parent):
            super(PrimarisPsyker.Level, self).__init__(parent=parent, name='Psyker')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    class Options(OptionsList):
        def __init__(self, parent):
            super(PrimarisPsyker.Options, self).__init__(parent=parent, name='Options')
            self.variant('Digital weapons', 10)

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(PrimarisPsyker, self).build_statistics())
