__author__ = 'Ivan Truskov'
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.chaos_marines.armory import vehicle_upgrades, tank_upgrades
from builder.games.wh40k.obsolete.chaos_marines.troops import Rhino, IconBearer, AspiringChampion
from functools import reduce


class Havoks(Unit):
    name = 'Havocs'

    class Havok(IconBearer):
        name = 'Havoc'
        base_points = 13
        heavy_list = ['flame', 'hbgun', 'acan', 'mgun', 'pgun', 'mlaunch', 'lcannon']

        def __init__(self):
            IconBearer.__init__(self, wrath=20, flame=15, despair=10, excess=30, vengeance=25)
            self.count = self.opt_count(self.name, 1, 19, self.base_points)
            self.wep = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bgun'],
                ['Flamer', 5, 'flame'],
                ['Heavy bolter', 10, 'hbgun'],
                ['Autocannon', 10, 'acan'],
                ['Meltagun', 10, 'mgun'],
                ['Plasma gun', 15, 'pgun'],
                ['Missile launcher', 15, 'mlaunch'],
                ['Lascannon', 20, 'lcannon']
            ])
            self.opt = self.opt_options_list('', [
                ['Close combat weapon', 2, 'ccw'],
                ['Flakk missiles', 10, 'flakk']
            ])
            self.gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Bolt pistol']

        def has_heavy(self):
            return self.get_count() if self.wep.get_cur() != 'bgun' else 0

        def check_rules(self):
            self.opt.set_visible_options(['flakk'], self.wep.get_cur() == 'mlaunch')
            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id])

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.ldr = self.opt_sub_unit(AspiringChampion())
        self.warriors = self.opt_units_list(self.Havok.name, self.Havok, 4, 19)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 3, 'ng'],
            ['Mark of Slaanesh', 2, 'sl']
        ], limit=1)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [['Veterans of the Long War', 1, 'lwvet']])
        self.transport = self.opt_optional_sub_unit('Transport', Rhino())

    def check_rules(self):
        self.warriors.update_range()

        icon = sum([u.set_icon(self.marks) for u in [self.ldr.get_unit()] + self.warriors.get_units()])
        if icon > 1:
            self.error("Havoks can't take more then 1 icon (taken: {0}).".format(icon))

        heavy = reduce(lambda val, it: val + it.has_heavy(), self.warriors.get_units(), 0)
        if heavy > 4:
            self.error("Havoks can't have more then 4 heavy weapon (taken: {0}).".format(heavy))

        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors, self.transport]) +
                        ((self.opt.points() if not self.black_legion else 1) + self.marks.points()) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_count(self):
        return self.warriors.get_count() + 1


class Obliterators(Unit):
    name = "Obliterators"
    base_points = 70
    base_gear = ['Fleshmetal', 'Power fist', 'Obliterator weapons']

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.count = self.opt_count('Obliterator', 1, 3, self.base_points)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 4, 'kh'],
            ['Mark of Tzeentch', 8, 'tz'],
            ['Mark of Nurgle', 6, 'ng'],
            ['Mark of Slaanesh', 1, 'sl']
        ], limit=1)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 3, 'lwvet']
            ])

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]) + (3 * self.get_count() if self.black_legion else 0))
        self.build_description(exclude=[self.count.id], count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')
        self.description.add(ModelDescriptor('Obliterator', self.count.get(), self.base_points, self.base_gear).build())


class Defiler(Unit):
    name = "Defiler"
    base_points = 195
    gear = ['Battle cannon', 'Power fist', 'Power fist', 'Searchlight', 'Daemonic posession', 'Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Twin-linked heavy flamer', 0, 'tlhflame'],
            ['Havoc launcher', 5, 'hlaunch'],
            ['Power scourge', 25, 'psc']
        ])
        self.wep2 = self.opt_one_of('', [
            ['Reaper autocannon', 0, 'racan'],
            ['Power fist', 0, 'pfist'],
            ['Twin-linked heavy bolter', 0, 'tlhbgun'],
            ['Twin-linked lascannon', 20, 'tllcan']
        ])
        self.opt = self.opt_options_list('Options', vehicle_upgrades[:-1])  # Daemonic posession included

    def check_rules(self):
        self.opt.set_active_options(['cflame'], not self.opt.get('cmelta') and not self.opt.get('cplasma'))
        self.opt.set_active_options(['cplasma'], not self.opt.get('cmelta') and not self.opt.get('cflame'))
        self.opt.set_active_options(['cmelta'], not self.opt.get('cplasma') and not self.opt.get('cflame'))
        Unit.check_rules(self)


class Forgefiend(Unit):
    name = "Forgefiend"
    base_points = 175
    gear = ['Daemonic posession']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapons', [
            ['Hades autocannon', 0, 'hacan'],
            ['Ectoplasma cannon', 0, 'ecan']
        ])
        self.opt = self.opt_options_list('', [['Ectoplasma cannon', 25, 'ecan']])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[self.wep, self.wep, self.opt])  # 2x cannons


class Maulerfiend(Unit):
    name = "Maulerfiend"
    base_points = 125
    gear = ['Daemonic posession', 'Power fists', 'Power fists']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Magma cutter', 0, 'mcut'],
            ['Lasher tendrils', 10, 'lash']
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[self.wep, self.wep])  # 2x cannons


class LandRaider(Unit):
    name = "Chaos Land Raider"
    base_points = 230
    gear = ['Twin-linked heavy bolter', 'Twin-linked lascannon', 'Twin-linked lascannon', 'Smoke launchers',
            'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', tank_upgrades)

    def check_rules(self):
        self.opt.set_active_options(['cflame'], not self.opt.get('cmelta') and not self.opt.get('cplasma'))
        self.opt.set_active_options(['cplasma'], not self.opt.get('cmelta') and not self.opt.get('cflame'))
        self.opt.set_active_options(['cmelta'], not self.opt.get('cplasma') and not self.opt.get('cflame'))
        Unit.check_rules(self)


class Vindicator(Unit):
    name = "Chaos Vindicator"
    base_points = 120
    gear = ['Demolisher cannon', 'Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', tank_upgrades + [['Siege shield', 10, 'ss']])

    def check_rules(self):
        self.opt.set_active_options(['cflame'], not self.opt.get('cmelta') and not self.opt.get('cplasma'))
        self.opt.set_active_options(['cplasma'], not self.opt.get('cmelta') and not self.opt.get('cflame'))
        self.opt.set_active_options(['cmelta'], not self.opt.get('cplasma') and not self.opt.get('cflame'))
        Unit.check_rules(self)


class Predator(Unit):
    name = "Chaos Predator"
    base_points = 75
    gear = ['Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Autocannon', 0, 'acan'],
            ['Twin-linked lascannon', 25, 'tllcan']
        ])
        self.sp = self.opt_options_list("Side Sponsons", [
            ["Heavy Bolters", 20, "bolt"],
            ["Lascannons", 40, "las"],
        ], limit=1)

        self.opt = self.opt_options_list('Options', tank_upgrades)

    def check_rules(self):
        self.opt.set_active_options(['cflame'], not self.opt.get('cmelta') and not self.opt.get('cplasma'))
        self.opt.set_active_options(['cplasma'], not self.opt.get('cmelta') and not self.opt.get('cflame'))
        self.opt.set_active_options(['cmelta'], not self.opt.get('cplasma') and not self.opt.get('cflame'))
        Unit.check_rules(self)
