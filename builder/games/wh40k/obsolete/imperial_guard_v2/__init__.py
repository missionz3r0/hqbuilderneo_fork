from builder.games.wh40k.obsolete.imperial_guard import troops, heavy, fast, elites, hq

__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.old_roster import Wh40k
from builder.games.wh40k.roster import HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, Fort
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.escalation.imperial_guard import LordsOfWar as IGLordsOfWar


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.commandsquad = UnitType(self, Unit.norm_core1_unit(hq.CommandSquad))
        self.lordcommissar = UnitType(self, Unit.norm_core1_unit(hq.LordCommissar))
        self.yarrick = UnitType(self, Unit.norm_core1_unit(hq.Yarrick))
        self.primalispsyker = UnitType(self, Unit.norm_core1_unit(hq.PrimalisPsyker))
        self.priest = UnitType(self, Unit.norm_core1_unit(hq.Priest))
        self.techpriest = UnitType(self, Unit.norm_core1_unit(hq.Techpriest))


class HQ(BaseHQ):
    pass


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.ogrynsquad = UnitType(self, Unit.norm_core1_unit(elites.OgrynSquad))
        self.ratlingsquad = UnitType(self, Unit.norm_core1_unit(elites.RatlingSquad))
        self.psykersquad = UnitType(self, Unit.norm_core1_unit(elites.PsykerSquad))
        self.stormtroopers = UnitType(self, Unit.norm_core1_unit(elites.StormTroopers))
        self.guardsmanmarbo = UnitType(self, Unit.norm_core1_unit(elites.GuardsmanMarbo))


class Elites(BaseElites):
    pass


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.infantryplatoon = UnitType(self, Unit.norm_core1_unit(troops.InfantryPlatoon))
        self.veteransquad = UnitType(self, Unit.norm_core1_unit(troops.VeteranSquad))
        self.penallegion = UnitType(self, Unit.norm_core1_unit(troops.PenalLegion))


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.scoutsentinelsquad = UnitType(self, Unit.norm_core1_unit(fast.ScoutSentinelSquad))
        self.armouredsentinelsquad = UnitType(self, Unit.norm_core1_unit(fast.ArmouredSentinelSquad))
        self.riders = UnitType(self, Unit.norm_core1_unit(fast.Riders))
        self.hellhounds = UnitType(self, Unit.norm_core1_unit(fast.Hellhounds))
        self.valkyries = UnitType(self, Unit.norm_core1_unit(fast.Valkyries))
        self.vendetta = UnitType(self, Unit.norm_core1_unit(fast.Vendetta))


class FastAttack(BaseFastAttack):
    pass


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.lemans = UnitType(self, Unit.norm_core1_unit(heavy.Lemans))
        self.hydra = UnitType(self, Unit.norm_core1_unit(heavy.Hydra))
        self.ordnance = UnitType(self, Unit.norm_core1_unit(heavy.Ordnance))
        self.manticore = UnitType(self, Unit.norm_core1_unit(heavy.Manticore))
        self.deathstrike = UnitType(self, Unit.norm_core1_unit(heavy.Deathstrike))


class HeavySupport(BaseHeavySupport):
    pass


class LordsOfWar(Titans, IGLordsOfWar):
    pass


class ImperialGuardV2(Wh40k):
    army_id = 'imperial_guard_v2'
    army_name = 'Imperial Guard'
    obsolete = True

    def __init__(self, secondary=False):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)

        super(ImperialGuardV2, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self),
            ally=Ally(parent=self, ally_type=Ally.IG),
            secondary=secondary
        )

    def check_rules(self):
        super(ImperialGuardV2, self).check_rules()
        psy = self.hq.primalispsyker.count
        if psy > 5:
            self.error("You can't take more 5 Primalis Psykers (taken: {0}).".format(psy))

        eng = self.hq.techpriest.count
        if eng > 2:
            self.error("You can't take more 2 Techpriest Enginseers (taken: {0}).".format(eng))
