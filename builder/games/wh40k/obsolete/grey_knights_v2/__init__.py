__author__ = 'Denis Romanow'

from builder.games.wh40k.old_roster import Wh40k
from builder.games.wh40k.roster import HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, Fort
from builder.games.wh40k.escalation.space_marines import GKLordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.draigo = UnitType(self, Draigo)
        UnitType(self, Mordrak)
        UnitType(self, Stern)
        self.crowe = UnitType(self, Crowe)
        UnitType(self, GrandMaster)
        UnitType(self, Captain)
        UnitType(self, Champion)
        UnitType(self, Librarian)
        self.karamazov = UnitType(self, Karamazov)
        self.coteaz = UnitType(self, Coteaz)
        self.valeria = UnitType(self, Valeria)
        self.malleus = UnitType(self, Malleus)
        self.hereticus = UnitType(self, Hereticus)
        self.xenos = UnitType(self, Xenos)

        self.inquisitors = [self.karamazov, self.valeria, self.malleus, self.hereticus, self.xenos]


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Techmarine)
        self.purifiers = UnitType(self, Purifiers)
        UnitType(self, VenDred)
        self.paladins = UnitType(self, Paladins)
        UnitType(self, Callidus)
        UnitType(self, Eversor)
        UnitType(self, Culexus)
        UnitType(self, Vindicare)
        self.warband = UnitType(self, Warband)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, Terminators)
        UnitType(self, Strikes)
        self.paladins = UnitType(self, Paladins)
        self.purifiers = UnitType(self, Purifiers)
        self.warband = UnitType(self, Warband)


class FastAttack(FastSection):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, StormravenGunship)
        UnitType(self, Interceptors)


class HeavySupport(HeavySection):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        UnitType(self, Purgations)
        UnitType(self, Dred)
        UnitType(self, Dreadknight)
        UnitType(self, LandRaider)
        UnitType(self, LandRaiderCrusader)
        UnitType(self, LandRaiderRedeemer)


class LordsOfWar(Titans, GKLordsOfWar):
    pass


class GreyKnightsV2(Wh40k):
    army_id = 'grey_knights_v2'
    army_name = 'Grey Knights'
    obsolete = True

    def __init__(self, secondary=False):
        from builder.games.wh40k.old_roster import Ally
        super(GreyKnightsV2, self).__init__(
            hq=HQ(parent=self),
            elites=Elites(parent=self),
            troops=Troops(parent=self),
            fast=FastAttack(parent=self),
            heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self),
            ally=Ally(parent=self, ally_type=Ally.GK),
            secondary=secondary
        )

    def check_rules(self):
        super(GreyKnightsV2, self).check_rules()
        self.move_units(self.hq.draigo.count > 0, self.elites.paladins, self.troops.paladins)
        self.move_units(self.hq.crowe.count > 0, self.elites.purifiers, self.troops.purifiers)
        self.move_units(self.hq.coteaz.count > 0, self.elites.warband, self.troops.warband)
        if self.hq.coteaz.count == 0:
            inq_cnt = sum(u.count for u in self.hq.inquisitors)
            if self.elites.warband.count > inq_cnt:
                self.error("Only one Inquisitorial warband can be taken per inquisitor.")
