from builder.games.wh40k.obsolete.grey_knights import heavy

__author__ = 'Denis Romanov'

from builder.core2 import *

Purgations = Unit.norm_core1_unit(heavy.Purgations)
Dred = Unit.norm_core1_unit(heavy.Dred)
Dreadknight = Unit.norm_core1_unit(heavy.Dreadknight)
LandRaider = Unit.norm_core1_unit(heavy.LandRaider)
LandRaiderCrusader = Unit.norm_core1_unit(heavy.LRCrusader)
LandRaiderRedeemer = Unit.norm_core1_unit(heavy.LRRedeemer)
