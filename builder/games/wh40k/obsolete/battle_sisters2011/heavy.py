__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.battle_sisters2011.troops import Rhino,Immolator
from functools import reduce

class Retributors(Unit):
    name = 'Retributor Squad'
    class Retributor(ListSubUnit):
        max = 9
        name = 'Retributor'
        base_points = 12
        gear = ['Power armour','Frag grenades','Krak grenades','Bolt pistol']
        def __init__(self):
            ListSubUnit.__init__(self)
            self.hlist = [
                ['Heavy bolter',5,'hbgun'],
                ['Multi-melta', 10, 'mmgun' ],
                ['Heavy flamer', 20, 'hflame' ]
            ]

            self.wep = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + self.hlist)
            self.flag = self.opt_options_list('Options',[['Simulacrum imperialis',20,'simp']])
            self.have_flag = False
            self.have_hvy = False
        def has_hvy(self):
            return self.wep.get_cur() != 'bgun'
        def check_rules(self):
            if self.wep.get_cur() == 'bgun':
                self.wep.set_active_options([w[2] for w in self.hlist], not self.have_hvy)
            if not self.flag.get('simp'):
                self.flag.set_active_options(self.flag.get_all_ids(), not self.have_flag)
            ListSubUnit.check_rules(self)
    class Superior(Unit):
        name = 'Retributor Superior'
        gear = ['Power armour','Frag grenades','Krak grenades']
        base_points = 65 - 12 * 4
        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Chainsword',0,'chsw'],
                ['Storm Bolter',3,'sbgun'],
                ['Power sword',10,'psw'],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Condemnor boltgun',15,'cbgun'],
                ['Plasma pistol',15,'ppist']
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + weaponlist)
            self.wep2 = self.opt_one_of('Weapon',[['Bolt pistol',0,'bpist']] + weaponlist)
            self.opt = self.opt_options_list('Options',[
                ['Melta bombs',5,'mbomb']
            ])
    def __init__(self):
        Unit.__init__(self)
        self.squad = self.opt_units_list(self.Retributor.name,self.Retributor,4,9)
        self.sup = self.opt_sub_unit(self.Superior())
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')
    def check_rules(self):
        self.squad.update_range()
        flag = reduce(lambda val, c: val + (1 if c.flag.get('simp') else 0), self.squad.get_units(), 0)
        if flag > 1:
            self.error('Only one Simulacrum imperialis can be carried by squad of Dominions.')
        hvy = reduce(lambda val, c: val + (1 if c.has_hvy() else 0), self.squad.get_units(), 0)
        if hvy > 4:
            self.error('No more then 4 Retributors can carry heavy weapons.')
        for c in self.squad.get_units():
            c.have_flag = (flag > 0)
            self.have_hvy = hvy >= 4
            c.check_rules()
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)
    def get_count(self):
        return self.squad.get_count() + 1

class Exorcist(Unit):
    name = "Exorcist"
    base_points = 135
    gear = ['Exorcist launcher','Smoke launchers']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
                ['Searchlight',1,'light'],
                ['Dozer blade', 5, 'dblade'],
                ['Storm bolter', 10, 'sbgun'],
                ['Hunter-killer missile', 10, 'hkm'],
                ['Extra armour', 15, 'exarm']
        ])

class PEngine(ListSubUnit):
    max = 3
    name = 'Penitent Engine'
    base_points = 85
    gear = ['Two Dreadnought close combat weapons (with built in heavy flamers)']
