__author__ = 'Denis Romanov'

from builder.core2 import *


class SpecialList(OptionsList):
    def __init__(self, parent):
        super(SpecialList, self).__init__(parent=parent, name='Special weapon', limit=1)
        self.flame = self.variant('Flamer', 5)
        self.mgun = self.variant('Meltagun', 10)
        self.pgun = self.variant('Plasmagun', 15)


class HeavyList(OptionsList):
    def __init__(self, parent):
        super(HeavyList, self).__init__(parent=parent, name='Heavy weapon')
        self.heavy = []
        self.hbgun = self.variant('Heavy bolter', 10)
        self.heavyflamer = self.variant('Heavy flamer', 10)
        self.mulmgun = self.variant('Multi-melta', 10)
        self.mlaunch = self.variant('Missile launcher', 15)
        self.flakkmissiles = self.variant('Flakk missiles', 10, visible=False)
        self.pcannon = self.variant('Plasma cannon', 15)
        self.lcannon = self.variant('Lascannon', 20)
        self.heavy += [self.hbgun, self.mulmgun, self.mlaunch, self.pcannon, self.lcannon, self.heavyflamer]

    def check_rules(self):
        super(HeavyList, self).check_rules()
        self.flakkmissiles.visible = self.flakkmissiles.used = self.mlaunch.value
        self.process_limit(self.heavy, 1)


class BoltPistol(OneOf):
    def __init__(self, *args, **kwargs):
        super(BoltPistol, self).__init__(*args, **kwargs)
        self.boltpistol = self.variant('Bolt pistol', 0)


class Boltgun(OneOf):
    def __init__(self, *args, **kwargs):
        super(Boltgun, self).__init__(*args, **kwargs)
        self.boltgun = self.variant('Boltgun', 0)


class Melee(OneOf):
    def __init__(self, *args, **kwargs):
        super(Melee, self).__init__(*args, **kwargs)
        self.chainsword = self.variant('Chainsword', 0)
        self.pwr = self.variant('Power weapon', 15)
        self.fist = self.variant('Power fist', 25)


class Ranged(OneOf):
    def __init__(self, *args, **kwargs):
        super(Ranged, self).__init__(*args, **kwargs)

        self.stormbolter = self.variant('Storm bolter', 5)
        self.combimelta = self.variant('Combi-melta', 10)
        self.combiflamer = self.variant('Combi-flamer', 10)
        self.combigrav = self.variant('Combi-grav', 10)
        self.combiplasma = self.variant('Combi-plasma', 10)
        self.gravpistol = self.variant('Grav-pistol', 15)
        self.plasmapistol = self.variant('Plasma pistol', 15)


power_armour_set = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]


class TheDamned(Unit):
    type_name = 'Legion of the Damned'
    type_id = 'legionofthedamned_v1'

    model_points = 25

    class DamnedSergeant(Unit):

        class Weapon1(Ranged, BoltPistol):
            pass

        class Weapon2(Ranged, Melee, Boltgun):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(TheDamned.DamnedSergeant.Options, self).__init__(parent=parent, name='Options')
                self.variant('Animus Malorum', 35)

        def __init__(self, parent):
            super(TheDamned.DamnedSergeant, self).__init__(
                parent=parent, points=125 - 4 * TheDamned.model_points, name='Legionnaire Sergeant',
                gear=power_armour_set
            )

            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self)

    def __init__(self, parent):
        super(TheDamned, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.DamnedSergeant(None))
        self.legionnaire = Count(self, 'Legionnaire', min_limit=4, max_limit=9, points=self.model_points)
        self.special = SpecialList(self)
        self.heavy = HeavyList(self)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, options=self.sergeant.description, count=self.get_count())
        leg = UnitDescription('Legionnaire', self.model_points, options=power_armour_set + [Gear('Bolt pistol')])

        for o in [self.heavy, self.special]:
            if o.used and o.any:
                desc.add(leg.clone().add_points(o.points).add(o.description))

        desc.add(leg.add(Gear('Boltgun')).set_count(self.legionnaire.cur - self.special.count - self.heavy.count))
        return desc

    def get_count(self):
        return self.legionnaire.cur + 1

    def get_unique_gear(self):
        return self.sergeant.unit.opt.description
