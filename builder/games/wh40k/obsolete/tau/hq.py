__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.tau.armory import *
from builder.core.options import norm_counts


class Bodyguards_v2(Unit):
    name = 'Crisis bodyguard team'

    class CrisisSuit(ListSubUnit):
        base_points = 32
        name = 'Crisis Bodyguard'
        gear = ['Crisis battlesuit', 'Multi-tracker', 'Blacksun filter']

        def __init__(self, enclaves=False):
            ListSubUnit.__init__(self)
            self.base_wep = [self.opt_count(w[0], 0, 3, w[1]) for w in base_weapon]
            self.tl_wep = self.opt_options_list('', tl_weapon)
            self.unique_wep = self.opt_options_list('', unique_weapon)

            self.support = self.opt_options_list('Support system', crisis_support)
            self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]
            self.signature = self.opt_options_list('Signature system', signature)
            self.enclaves = enclaves
            if self.enclaves:
                self.enclaves_signature = self.opt_options_list('Signature system of the Farsight Enclaves',
                                                                enclaves_signature)

        def check_rules(self):
            norm_counts(0, 2, self.drones)
            if self.enclaves:
                self.enclaves_signature.set_visible(True)
                self.signature.set_visible(False)
            nodes = len(self.unique_wep.get_all()) + len(self.tl_wep.get_all()) * 2 + len(self.support.get_all()) + \
                sum((c.get() for c in self.base_wep))
            if nodes > 3:
                self.error(self.name + ' may take up to 3 items from Ranged weapon and Support systems lists. '
                                       '(taken {0})'.format(nodes))
            ListSubUnit.check_rules(self)

        def get_unique_gear(self):
            return (self.unique_wep.get_selected() +
                    (self.signature.get_selected() if self.signature.is_used() else []) +
                    (self.enclaves_signature.get_selected() if self.enclaves and self.enclaves_signature.is_used()
                     else [])) * self.get_count()

    class EnclaveCrisisSuit(CrisisSuit):
        def __init__(self):
            Bodyguards_v2.CrisisSuit.__init__(self, enclaves=True)

    def __init__(self, max_models=2, enclaves=False):
        Unit.__init__(self)
        self.enclaves = enclaves
        if self.enclaves:
            self.team = self.opt_units_list('Shas\'ui', self.EnclaveCrisisSuit, 1, max_models)
        else:
            self.team = self.opt_units_list('Shas\'ui', self.CrisisSuit, 1, max_models)
            self.ritual = self.opt_options_list('Options', [
                ['Bonding knife ritual', 2, 'up']
            ])

    def check_rules(self):
        self.team.update_range()
        if self.enclaves:
            self.set_points(self.build_points(count=1) + 2 * self.get_count())
        else:
            self.set_points(self.build_points(count=1, exclude=[self.ritual.id]) +
                            self.ritual.points() * self.get_count())
        self.build_description(count=1)
        if self.enclaves:
            self.description.add('Bonding knife ritual')

    def get_count(self):
        return self.team.get_count()

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.team.get_units()), [])


class EnclavesBodyguards(Bodyguards_v2):
    def __init__(self):
        Bodyguards_v2.__init__(self, enclaves=True)


class FarsightBodyguards(Bodyguards_v2):
    name = 'Farsight\'s bodyguard team'

    def __init__(self):
        Bodyguards_v2.__init__(self, 7)


class EnclavesFarsightBodyguards(Bodyguards_v2):
    name = 'Farsight\'s bodyguard team'

    def __init__(self):
        Bodyguards_v2.__init__(self, max_models=7, enclaves=True)


class FarsightCommanders(Unit):
    name = 'Farsight\'s commander team'
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.team = self.opt_options_list('Team', [
            ['O\'Vesa', 305, 'vesa'],
            ['Commander Bravestorm', 199, 'bst'],
            ['Commander Brightsword', 200, 'bsw'],
            ['Shas\'O Sha\'vastos', 174, 'shav'],
            ['Shas\'O Arra\'kon', 169, 'arr'],
            ['Broadside Shas\'vre Ob\'lotai 9-o', 129, 'broad'],
            ['Sub-commander Torchstar', 154, 'torch'],
        ])
        self.desc = {
            'vesa': ModelDescriptor('O\'Vesa', points=305,
                                    gear=['Riptide battlesuit', 'Ion accelerator', 'Twin-linked fusion blaster',
                                          'Riptide shield generator', 'Early warning override', 'Stimulant injector',
                                          'Shielded missile drone', 'Shielded missile drone',
                                          'Earth Caste Pilot Array']).build(1),
            'bst': ModelDescriptor('Commander Bravestorm', points=199,
                                   gear=['XV8-02 Crisis \'Iridium\' Battlesuit', 'Plasma rifle', 'Flamer',
                                         'Stimulant injector', 'Shield generator', 'Gun drone', 'Gun drone',
                                         'Onager Gauntlet']).build(1),
            'bsw': ModelDescriptor('Commander Brightsword', points=200,
                                   gear=['Crisis battlesuit', 'Twin-linked fusion blaster', 'Advanced targeting system',
                                         'Stimulant injector', 'Shield drone', 'Fusion Blades',
                                         'Warscaper Drone']).build(1),
            'shav': ModelDescriptor('Shas\'O Sha\'vastos', points=174,
                                    gear=['Crisis battlesuit', 'Plasma rifle', 'Flamer', 'Shield generator',
                                          'Vectored retro-thrusters', 'Gun drone', 'Gun drone',
                                          'Puretide Engram Neurochip']).build(1),
            'arr': ModelDescriptor('Shas\'O Arra\'kon', points=169,
                                   gear=['Crisis battlesuit', 'Plasma rifle', 'Cyclic ion blaster',
                                         'Airbursting fragmentation projector', 'Counterfire defence system',
                                         'Gun drone', 'Gun drone', 'Repulsor Impact Field']).build(1),
            'broad': ModelDescriptor('Broadside Shas\'vre Ob\'lotai 9-o', points=129,
                                     gear=['Broadside battlesuit', 'Twinlinked high-yield missile pod',
                                           'Twin-linked smart missile system', 'Velocity tracker', 'Missile drone',
                                           'Missile drone', 'Seeker missile']).build(1),
            'torch': ModelDescriptor('Sub-commander Torchstar', points=154,
                                     gear=['Crisis battlesuit', 'Flamer', 'Flamer', 'Target lock', 'Drone controller',
                                           'Marker drone', 'Marker drone', 'Multi-Spectrum Sensor Suite',
                                           'Neuroweb System Jammer']).build(1),
        }

        self.unique_gear = {
            'vesa': ['Earth Caste Pilot Array'],
            'bst': ['XV8-02 Crisis \'Iridium\' Battlesuit', 'Onager Gauntlet'],
            'bsw': ['Fusion Blades', 'Warscaper Drone'],
            'shav': ['Puretide Engram Neurochip'],
            'arr': ['Cyclic ion blaster', 'Airbursting fragmentation projector', 'Repulsor Impact Field'],
            'broad': [],
            'torch': ['Multi-Spectrum Sensor Suite', 'Neuroweb System Jammer'],
        }

    def check_rules(self):
        if self.get_count() == 0:
            self.error('Farsight\'s commander team must include at least one member')
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        for commander_id in self.team.get_all():
            self.description.add(self.desc[commander_id])

    def get_count(self):
        return len(self.team.get_all())

    def get_unique_gear(self):
        return sum([self.unique_gear[i] for i in self.team.get_all()], [])


class Commander_v2(Unit):
    base_points = 85
    name = 'Commander'
    gear = ['Crisis battlesuit', 'Multi-tracker', 'Blacksun filter']

    def __init__(self, enclaves=False):
        Unit.__init__(self)
        self.base_wep = [self.opt_count(w[0], 0, 4, w[1]) for w in base_weapon]
        self.tl_wep = [self.opt_count(w[0], 0, 2, w[1]) for w in tl_weapon]
        self.unique_wep = self.opt_options_list('', unique_weapon)
        self.support = self.opt_options_list('Support system', crisis_support)
        self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]
        self.signature = self.opt_options_list('Signature system', signature)
        self.enclaves = enclaves
        if self.enclaves:
            self.enclaves_signature = self.opt_options_list('Signature system of the Farsight Enclaves',
                                                            commander_signature)

    def check_rules(self):
        norm_counts(0, 2, self.drones)
        if self.enclaves:
            self.enclaves_signature.set_visible(True)
            self.signature.set_visible(False)
            if self.enclaves_signature.get('fblades') and self.tl_wep[2].get() == 0:
                self.error('Fusion blades available for Commander with a twin-linked fusion blaster only')
        nodes = len(self.unique_wep.get_all()) + len(self.support.get_all()) + \
            sum((c.get() for c in self.base_wep)) + sum((c.get() for c in self.tl_wep)) * 2
        if nodes > 4:
            self.error(self.name + ' may take up to 4 items from Ranged weapon and Support systems lists. '
                                   '(taken {0})'.format(nodes))
        Unit.check_rules(self)

    def get_unique_gear(self):
            return self.unique_wep.get_selected() + \
                (self.signature.get_selected() if self.signature.is_used() else []) + \
                (self.enclaves_signature.get_selected() if self.enclaves and self.enclaves_signature.is_used() else [])


class EnclavesCommander(Commander_v2):
    def __init__(self):
        Commander_v2.__init__(self, enclaves=True)


class Farsight_v2(Unit):
    unique = True
    static = True
    name = 'Commander Farsight'
    base_points = 165
    gear = ['Crisis battlesuit', 'Multi-tracker', 'Blacksun filter', 'Shield generator', 'Plasma rifle', 'Dawn blade']


class Shadowsun_v2(Unit):
    unique = True
    name = 'Commander Shadowsun'
    base_points = 135
    gear = ['XV22 Stealth battlesuit',
            'Advanced targeting system',
            'Fusion blaster',
            'Fusion blaster']

    def __init__(self):
        Unit.__init__(self)
        self.opt_count('Command link drone', 0, 1, 20)
        self.opt_count('MV52 Shield drone', 0, 2, 20)


class Ethereal(Unit):
    name = 'Ethereal'
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Honour blade', 5],
            ['Two equalisers', 10, 'eq']
        ], limit=1)

        self.opt_options_list('Options', [
            ['Blacksun filter', 5],
            ['Homing beacon', 5]
        ])
        self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]

    def check_rules(self):
        norm_counts(0, 2, self.drones)
        exclude = []
        gear = []
        if self.wep.get('eq'):
            exclude = [self.wep.id]
            gear = ['Equaliser', 'Equaliser']
        self.set_points(self.build_points())
        self.build_description(gear=gear, exclude=exclude)


class Aunva(Unit):
    name = 'Aun\'va'
    base_points = 100
    gear = ['The Paradox of Duality', 'Recon armour']
    static = True
    unique = True
    hg = ModelDescriptor(name='Honour Guard', gear=['Honour blade', 'Recon armour', 'Photon grenades'])

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description()
        self.description.add(self.hg.build(2))

    def get_unique(self):
        return self.name


class Aunshi(StaticUnit):
    name = 'Aun\'shi'
    base_points = 110
    gear = ['Honour blade', 'EMP Grenades', 'Photon grenades', 'Shield generator']


class Darkstider(StaticUnit):
    name = 'Darkstider'
    base_points = 100
    gear = ['Recon armour', 'Pulse carabine', 'Photon greandes', 'Blacksun filter', 'Markerlight']


class Cadre(Unit):
    name = 'Cadre Fireblade'
    gear = ['Combat armour', 'Pulse rifle', 'Photon grenades', 'Markerlight']
    base_points = 60

    def __init__(self):
        Unit.__init__(self)
        self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]

    def check_rules(self):
        norm_counts(0, 2, self.drones)
        Unit.check_rules(self)
