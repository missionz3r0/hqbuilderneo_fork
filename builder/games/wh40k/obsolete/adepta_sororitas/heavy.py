__author__ = 'Ivan Truskov'

from builder.core.options import norm_counts
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.adepta_sororitas.troops import Rhino, Immolator
from builder.games.wh40k.obsolete.adepta_sororitas.armory import melee, ranged, vehicles


class Retributors(Unit):
    name = 'Retributor Squad'
    common_gear = ['Power armour', 'Frag grenades', 'Krak grenades']
    model_points = 12

    class Superior(Unit):
        name = 'Retributor Superior'

        def __init__(self):
            self.gear = Retributors.common_gear
            self.base_points = Retributors.model_points
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun', 0, 'bgun']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
            ])
            self.vet = self.opt_options_list('', [
                ['Veteran', 10, 'vet']
            ])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Retributor Superior', exclude=[self.vet.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.sup = self.opt_sub_unit(self.Superior())
        self.squad = self.opt_count('Retributor', 4, 9, self.model_points)
        self.opt = self.opt_options_list('Options', [
            ['Simulacrum imperialis', 10, 'simp'],
        ], limit=1)
        self.hb = self.opt_count('Heavy bolter', 0, 4, 10)
        self.fl = self.opt_count('Heavy flamer', 0, 4, 10)
        self.mm = self.opt_count('Multi-melta', 0, 4, 10)
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')

    def check_rules(self):
        free = 3 if self.squad.get() == 4 and self.opt.is_any_selected() else 4
        norm_counts(0, free, [self.fl, self.hb, self.mm])
        self.points.set(self.build_points(count=1))
        self.build_description(options=[self.sup])
        desc = ModelDescriptor('Retributor', points=self.model_points, gear=self.common_gear + ['Bolt Pistol'])
        count = self.squad.get()
        if self.opt.is_any_selected():
            self.description.add(desc.clone().add_gear('Boltgun').add_gear_opt(self.opt).build(1))
            count -= 1
        self.description.add(desc.clone().add_gear('Heavy bolter', points=10).build(self.hb.get()))
        self.description.add(desc.clone().add_gear('Heavy flamer', points=10).build(self.fl.get()))
        self.description.add(desc.clone().add_gear('Multi-melta', points=10).build(self.mm.get()))
        self.description.add(desc.clone().add_gear('Boltgun').build(count - self.fl.get() - self.hb.get() -
                                                                    self.mm.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.squad.get() + 1


class Exorcist(Unit):
    name = "Exorcist"
    base_points = 125
    gear = ['Exorcist missile launcher', 'Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicles)


class PenitentEngine(Unit):
    name = 'Penitent Engine'
    model_points = 80

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.name, 1, 3, self.model_points)

    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(exclude=[self.count.id])
        self.description.add(ModelDescriptor(self.name, points=self.model_points)
            .add_gear('Dreadnought close combat weapon', count=2)
            .add_gear('Heavy flamer', count=2).build(self.get_count()))
