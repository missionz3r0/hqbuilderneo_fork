__author__ = 'Ivan Truskov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.adepta_sororitas.troops import Rhino, Immolator
from builder.games.wh40k.obsolete.adepta_sororitas.armory import melee, ranged, special, heavy


class Celestians(Unit):
    name = 'Celestian Squad'
    common_gear = ['Power armour', 'Frag grenades', 'Krak grenades']
    model_points = 14

    class Superior(Unit):
        name = 'Celestian Superior'

        def __init__(self):
            self.gear = Celestians.common_gear
            self.base_points = Celestians.model_points
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun', 0, 'bgun']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
            ])

    def __init__(self):
        Unit.__init__(self)
        self.sup = self.opt_sub_unit(self.Superior())
        self.squad = self.opt_count('Battle Sister', 4, 9, self.model_points)
        self.opt = self.opt_options_list('Options', [
            ['Simulacrum imperialis', 10, 'simp'],
        ], limit=1)
        self.wep1 = self.opt_options_list('Weapon', special)
        self.wep2 = self.opt_options_list('', special + heavy)
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')

    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(options=[self.sup])
        des = ModelDescriptor('Celestian', points=self.model_points, gear=self.common_gear + ['Bolt Pistol'])
        count = self.squad.get()
        if self.opt.is_any_selected():
            self.description.add(des.clone().add_gear('Boltgun').add_gear_opt(self.opt).build(1))
            count -= 1
        for wep in [self.wep1, self.wep2]:
            if wep.is_any_selected():
                self.description.add(des.clone().add_gear_opt(wep).build(1))
                count -= 1
        self.description.add(des.clone().add_gear('Boltgun').build(count))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.squad.get() + 1


class Repentia(Unit):
    name = 'Repentia squad'
    common_gear = ['Evisecrator']
    model_points = 14

    class Mistress(Unit):
        name = 'Mistress of Repentance'

        def __init__(self):
            self.gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Neural whip', 'Neural whip']
            self.base_points = 85 - 4 * Repentia.model_points
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Mistress())
        self.warriors = self.opt_count('Sister Repentia', 4, 9, self.model_points)
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')

    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(options=[self.leader])
        self.description.add(ModelDescriptor('Sister Repentia', points=self.model_points,
                                             gear=self.common_gear).build(self.warriors.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.warriors.get() + 1
