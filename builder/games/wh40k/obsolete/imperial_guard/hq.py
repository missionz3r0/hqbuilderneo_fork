__author__ = 'Denis Romanow'

from builder.core.unit import Unit, OptionsList
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.imperial_guard.troops import Chimera


class CommandSquad(Unit):
    name = 'Company Command Squad'
    base_points = 50

    class GearUser(Unit):
        def __init__(self, gear_opt):
            Unit.__init__(self)
            self.gear_opt = gear_opt

        def check_rules(self):
            self.set_points(self.build_points())
            self.build_description(add=[self.gear_opt.get_selected()],
                                   gear=self.gear + (['Flak armour'] if not self.gear_opt.get('ca') else []))

    class HeavyWeaponTeam(GearUser):
        name = 'Heavy Weapon Team'
        gear = ['Frag grenades', 'Close combat weapon', 'Lasgun']

        def __init__(self, gear_opt):
            CommandSquad.GearUser.__init__(self, gear_opt)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Mortar', 5],
                ['Autocannon', 10],
                ['Heavy bolter', 10],
                ['Missile launcher', 15],
                ['Lascannon', 20],
            ])

    class Commander(GearUser):
        name = 'Company Commander'
        gear = ['Frag grenades', 'Refractor field']

        def __init__(self, gear_opt):
            CommandSquad.GearUser.__init__(self, gear_opt)
            gear = [
                ['Bolt pistol', 2],
                ['Boltgun', 2],
                ['Power weapon', 10],
                ['Plasma pistol', 10],
                ['Power fist', 15],
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Laspistol', 0]] + gear)
            self.wep2 = self.opt_one_of('', [['Close combat weapon', 0]] + gear)
            self.opt = self.opt_options_list('', [['Melta bombs', 5]])

    class Creed(Unit):
        name = 'Lord Castellan Creed'
        static = True
        unique = True
        base_points = 90
        gear = ['Carapace armour', 'Hot-shot las pistol', 'Hot-shot las pistol', 'Frag grenades', 'Refractor field']

    class Straken(Unit):
        name = 'Colonel \'Iron Hand\' Straken'
        static = True
        unique = True
        base_points = 95
        gear = ['Flak armour', 'Plasma pistol', 'Shotgun', 'Close combat weapon', 'Frag grenades', 'Refractor field']

    class Kell(Unit):
        name = 'Colour Sergeant Kell'
        static = True
        unique = True
        base_points = 85
        gear = ['Carapace armour', 'Las pistol', 'Power weapon', 'Power fist',  'Frag grenades', 'Regimental standard']

    class Nork(Unit):
        name = 'Nork Deddog'
        static = True
        unique = True
        base_points = 110
        gear = ['Carapace armour', 'Ripper gun', 'Frag grenades']

    class Guardsman(GearUser):
        name = 'Guardsman'
        gear = ['Frag grenades', 'Close combat weapon']

        def __init__(self, gear_opt):
            self.spec_ids = ['fl', 'gl', 'sr', 'mg', 'pg', 'hflame']
            CommandSquad.GearUser.__init__(self, gear_opt)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Lasgun', 0],
                ['Laspistol', 0],
                ['Flamer', 5, 'fl'],
                ['Grenade launcher', 5, 'gl'],
                ['Sniper rifle', 5, 'sr'],
                ['Meltagun', 10, 'mg'],
                ['Plasma gun', 15, 'pg'],
                ['Heavy flamer', 20, 'hflame'],
            ])
            self.opt = self.opt_options_list('Options', [
                ['Medi-pack', 30, 'medic'],
                ['Regimental standard', 15, 'ps'],
                ['Vox-caster', 5, 'vox'],
            ], limit=1)

        def has_spec(self, opt_id):
            if opt_id == 'hflame':
                return self.wep1.get_cur() == 'hflame'
            return self.opt.get(opt_id)

        def check_rules(self):
            self.opt.set_visible(self.wep1.get_cur() not in self.spec_ids)
            self.wep1.set_active_options(self.spec_ids, len(self.opt.get_all()) == 0)
            CommandSquad.GearUser.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.gear_opt = OptionsList('krak', '', [
            ['Krak grenades', 5, 'kg'],
            ['Carapace armour', 20, 'ca'],
            ['Camo cloaks', 20, 'cc'],
        ])
        self.commander = self.opt_sub_unit(CommandSquad.Commander(self.gear_opt))
        self.unique_commanders = self.opt_optional_sub_unit('', [self.Creed(), self.Straken()])
        self.kell = self.opt_optional_sub_unit('', self.Kell())
        self.guard1 = self.opt_sub_unit(self.Guardsman(self.gear_opt))
        self.guard2 = self.opt_sub_unit(self.Guardsman(self.gear_opt))
        self.guard3 = self.opt_sub_unit(self.Guardsman(self.gear_opt))
        self.guard4 = self.opt_sub_unit(self.Guardsman(self.gear_opt))
        self.heavy = self.opt_optional_sub_unit(CommandSquad.HeavyWeaponTeam.name,
                                                CommandSquad.HeavyWeaponTeam(self.gear_opt))
        self.advisors = self.opt_options_list('Regimental advisors', [
            ['Astropath', 30, 'ap'],
            ['Master of ordnance', 30, 'moo'],
            ['Officer of the Fleer', 30, 'of'],
        ])
        self.nork = self.opt_optional_sub_unit('', self.Nork())
        self.bodyguards = self.opt_count('Bodyguards', 0, 2, 15)
        self.append_opt(self.gear_opt)
        self.transport = self.opt_optional_sub_unit('Transport', Chimera())
        self.guards = [self.guard1, self.guard2, self.guard3, self.guard4]

    def check_rules(self):
        self.commander.set_active(self.unique_commanders.get_count() == 0)
        self.bodyguards.set_active(self.nork.get_count() == 0)
        self.guard1.set_active(self.kell.get_count() == 0)
        self.guard3.set_active(self.heavy.get_count() == 0)
        self.guard4.set_active(self.heavy.get_count() == 0)
        options = {
            'hflame': 'Heavy flamer',
            'medic': 'Medi-pack',
            'ps': 'Regimental standard',
            'vox': 'Vox-caster',
        }
        for key, value in options.items():
            count = sum((1 if g.is_used() and g.get_unit().has_spec(key) else 0 for g in self.guards))
            if count > 1 or (self.kell.get_count() > 0 and key == 'ps' and count > 0):
                self.error('{0} can include only one {1}'.format(self.name, value))
        self.points.set(self.build_points())
        self.build_description(exclude=[self.gear_opt.id, self.advisors.id, self.bodyguards.id, self.transport.id])
        adv_gear = ['Frag grenades', 'Close combat weapon',
                    'Laspistol'] + (['Flak armour'] if not self.gear_opt.get('ca') else ['Carapace armour'])
        if self.gear_opt.get('kg'):
            adv_gear += ['Krak grenades']
        if self.gear_opt.get('cc'):
            adv_gear += ['Camo cloaks']
        if self.advisors.get('ap'):
            self.description.add(ModelDescriptor('Astropath', points=30, gear=adv_gear).build(1))
        if self.advisors.get('moo'):
            self.description.add(ModelDescriptor('Master of ordnance', points=30, gear=adv_gear).build(1))
        if self.advisors.get('of'):
            self.description.add(ModelDescriptor('Officer of the Fleer', points=30, gear=adv_gear).build(1))
        if self.bodyguards.get() > 0:
            self.description.add(ModelDescriptor('Bodyguard', points=15, gear=adv_gear).build(self.bodyguards.get()))
        self.description.add(self.transport.get_selected())

    def get_unique(self):
        unique = []
        if self.unique_commanders.get(self.Creed.name):
            unique.append(self.Creed.name)
        if self.unique_commanders.get(self.Straken.name):
            unique.append(self.Straken.name)
        if self.kell.get_count():
            unique.append(self.Kell.name)
        if self.nork.get_count():
            unique.append(self.Nork.name)
        return unique


class LordCommissar(Unit):
    name = 'Lord Commissar'
    base_points = 70
    gear = ['Frag grenades', 'Krak grenades', 'Refractor field']

    def __init__(self):
        Unit.__init__(self)
        gear = [
            ['Boltgun', 0],
            ['Power weapon', 10],
            ['Plasma pistol', 10],
            ['Power fist', 15],
        ]
        self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0]] + gear)
        self.wep2 = self.opt_one_of('', [['Close combat weapon', 0]] + gear)
        self.arm = self.opt_one_of('Armour', [
            ['Flak armour', 0],
            ['Carapace armour', 10],
        ])
        self.gear_opt = self.opt_options_list('Options', [
            ['Melta bombs', 5, 'kg'],
            ['Camo cloaks', 10, 'cc'],
        ])

        self.transport = self.opt_optional_sub_unit('Transport', Chimera())


class Priest(Unit):
    name = 'Ministorum Priest'
    base_points = 45
    gear = ['Flak armour', 'Frag grenades', 'Rosarius']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Close combat weapon', 0],
            ['Eviscerator', 15],
        ])
        self.wep2 = self.opt_one_of('', [
            ['Laspistol', 0],
            ['Shootgun', 0]
        ])


class Techpriest(Unit):
    name = 'Techpriest Enginseer'
    base_points = 45
    gear = ['Power armour', 'Laspistol', 'Power weapon', 'Frag grenades', 'Krak grenades', 'Servo-arm']

    def __init__(self):
        Unit.__init__(self)
        self.gear_opt = self.opt_options_list('Options', [
            ['Melta bombs', 5, 'kg'],
        ])

        self.serv = self.opt_count('Servitors', 0, 5, 15)
        self.hw = [
            self.opt_count('Heavy bolter', 0, 2, 20),
            self.opt_count('Multi-melta', 0, 2, 30),
            self.opt_count('Plasma cannon', 0, 2, 30)
        ]

    def check_rules(self):
        norm_counts(0, min(2, self.serv.get()), self.hw)
        self.set_points(self.build_points(count=1))
        self.build_description(count=1, exclude=[self.serv.id] + [u.id for u in self.hw])
        desc = ModelDescriptor(name='Servitor', gear=['Carapace armour'], points=15)
        count = self.serv.get()
        for gear in self.hw:
            self.description.add(desc.clone().add_gear(name=gear.name, points=gear.unit_points).build(gear.get()))
            count -= gear.get()
        self.description.add(desc.clone().add_gear(name='Servo-arm').build(count))

    def get_count(self):
        return 1 + self.serv.get()


class Yarrick(Unit):
    unique = True
    name = 'Commissar Yarrick'
    base_points = 185
    gear = ['Carapace armour', 'Storm bolter', 'Battle claw', 'Close combat weapon', 'Bolt pistol', 'Frag grenades',
            'Krak grenades', 'Force field', 'Bale eye']

    def __init__(self):
        Unit.__init__(self)
        self.transport = self.opt_optional_sub_unit('Transport', Chimera())


class PrimalisPsyker(Unit):
    name = 'Primalis Psyker'
    static = True
    base_points = 70
    gear = ['Flak armour', 'Laspistol', 'Force weapon', 'Frag grenades', 'Refractor field']
