__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.imperial_guard.hq import *
from builder.games.wh40k.obsolete.imperial_guard.elites import *
from builder.games.wh40k.obsolete.imperial_guard.troops import *
from builder.games.wh40k.obsolete.imperial_guard.fast import *
from builder.games.wh40k.obsolete.imperial_guard.heavy import *
from builder.games.wh40k.obsolete.imperial_guard.imperial_guard_ex import ia_hq, ia_super_heavy


class ImperialGuard(LegacyWh40k):
    army_id = 'a66dbd8b7d8b408e98664cd481dbf877'
    army_name = 'Imperial Guard'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[CommandSquad, LordCommissar, Yarrick, PrimalisPsyker, Priest, Techpriest] + ia_hq,
            elites=[OgrynSquad, RatlingSquad, PsykerSquad, StormTroopers, GuardsmanMarbo],
            troops=[InfantryPlatoon, VeteranSquad, PenalLegion],
            fast=[ScoutSentinelSquad, ArmouredSentinelSquad, Riders, Hellhounds, Valkyries, Vendetta],
            heavy=[Lemans, Hydra, Ordnance, Manticore, Deathstrike],
            super_heavy=ia_super_heavy,
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.IG)
        )

        self.opt.visible = True

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_units([Priest, Techpriest])
            return self.hq.min <= count <= self.hq.max

        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        self.hq.set_active_types(ia_hq, self.opt.ia.value)

        psy = self.hq.count_unit(PrimalisPsyker)
        if psy > 5:
            self.error("You can't take more 5 Primalis Psykers (taken: {0}).".format(psy))
        eng = self.hq.count_unit(Techpriest)
        if eng > 2:
            self.error("You can't take more 2 Techpriest Enginseers (taken: {0}).".format(eng))
