__author__ = 'Denis Romanow'

from builder.core.unit import Unit, OptionsList, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.core.options import norm_counts


class Chimera(Unit):
    name = 'Chimera armored transport'
    base_points = 55
    gear = ['Search light', 'Smoker launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Multi-laser', 0],
            ['Heavy flamer', 0],
            ['Heavy bolter', 0],
        ])
        self.wep2 = self.opt_one_of('', [
            ['Heavy flamer', 0],
            ['Heavy bolter', 0],
        ])
        self.wep3 = self.opt_options_list('', [
            ['Pintle-mounted storm bolter', 10],
            ['Pintle-mounted heavy stubber', 10],
        ], limit=1)
        self.opt = self.opt_options_list('Options', [
            ['Dozen blade', 10],
            ['Extra armor', 15],
            ['Camo netting', 20],
            ['Hunter-killer missile', 10],
        ])


class KrakUser(Unit):
    def __init__(self, krak):
        Unit.__init__(self)
        self.krak = krak

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(add=[self.krak.get_selected()])


class HeavyWeaponTeam(KrakUser):
    name = 'Heavy Weapon Team'
    gear = ['Flak armour', 'Frag grenades', 'Close combat weapon', 'Lasgun']

    def __init__(self, krak):
        KrakUser.__init__(self, krak)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Mortar', 5],
            ['Autocannon', 10],
            ['Heavy bolter', 10],
            ['Missile launcher', 15],
            ['Lascannon', 20],
        ])


class Commander(KrakUser):
    gear = ['Flak armour', 'Frag grenades']

    def __init__(self, name, gear, krak):
        self.name = name
        KrakUser.__init__(self, krak)
        self.wep1 = self.opt_one_of('Weapon', [['Laspistol', 0], ['Bolt pistol', 2]] + gear)
        self.wep2 = self.opt_one_of('', [['Close combat weapon', 0]] + gear)
        self.opt = self.opt_options_list('', [['Melta bombs', 5]])


class Commissar(KrakUser):
    name = 'Commissar'
    gear = ['Flak armour', 'Frag grenades']
    base_points = 35

    def __init__(self, gear, krak):
        KrakUser.__init__(self, krak)
        self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0]] + gear)
        self.wep2 = self.opt_one_of('', [['Close combat weapon', 0]] + gear)


class InfantryPlatoon(Unit):
    name = 'Infantry Platoon'

    class CommandSquad(Unit):
        name = 'Platoon Command Squad'
        base_points = 30

        class Chenkov(Unit):
            name = 'Commander Chenkov'
            static = True
            unique = True
            base_points = 50
            gear = ['Carapace armour', 'Power weapon', 'Frag grenades', 'Bolt pistol']

        class AlRahem(Unit):
            name = 'Commander Al\'Rahem'
            static = True
            unique = True
            base_points = 70
            gear = ['Flak armour', 'Claw of Desert Tiger', 'Frag grenades', 'Plasma pistol']

        class Guardsman(KrakUser):
            name = 'Guardsman'
            gear = ['Flak armour', 'Frag grenades', 'Close combat weapon']

            def __init__(self, krak):
                self.spec_ids = ['fl', 'gl', 'sr', 'mg', 'pg', 'hflame']
                KrakUser.__init__(self, krak)
                self.wep1 = self.opt_one_of('Weapon', [
                    ['Lasgun', 0],
                    ['Laspistol', 0],
                    ['Flamer', 5, 'fl'],
                    ['Grenade launcher', 5, 'gl'],
                    ['Sniper rifle', 5, 'sr'],
                    ['Meltagun', 10, 'mg'],
                    ['Plasma gun', 15, 'pg'],
                    ['Heavy flamer', 20, 'hflame'],
                ])
                self.opt = self.opt_options_list('Options', [
                    ['Medi-pack', 30, 'medic'],
                    ['Platoon standard', 15, 'ps'],
                    ['Vox-caster', 5, 'vox'],
                ], limit=1)

            def has_spec(self, opt_id):
                if opt_id == 'hflame':
                    return self.wep1.get_cur() == 'hflame'
                return self.opt.get(opt_id)

            def check_rules(self):
                self.opt.set_visible(self.wep1.get_cur() not in self.spec_ids)
                self.wep1.set_active_options(self.spec_ids, len(self.opt.get_all()) == 0)
                KrakUser.check_rules(self)

        def __init__(self):
            gear = [
                ['Boltgun', 2],
                ['Power weapon', 10],
                ['Plasma pistol', 10],
                ['Power fist', 15],
            ]
            Unit.__init__(self)
            self.grenades = OptionsList('krak', '', [['Krak grenades', 5]])
            self.commander = self.opt_sub_unit(Commander('Platoon Commander', gear, self.grenades))
            self.unique_commanders = self.opt_optional_sub_unit('', [self.Chenkov(), self.AlRahem()])
            self.commissar = self.opt_optional_sub_unit(Commissar.name, Commissar(gear, self.grenades))
            self.guard1 = self.opt_sub_unit(self.Guardsman(self.grenades))
            self.guard2 = self.opt_sub_unit(self.Guardsman(self.grenades))
            self.guard3 = self.opt_sub_unit(self.Guardsman(self.grenades))
            self.guard4 = self.opt_sub_unit(self.Guardsman(self.grenades))
            self.heavy = self.opt_optional_sub_unit(HeavyWeaponTeam.name, HeavyWeaponTeam(self.grenades))
            self.append_opt(self.grenades)
            self.transport = self.opt_optional_sub_unit('Transport', Chimera())
            self.guards = [self.guard1, self.guard2, self.guard3, self.guard4]

        def check_rules(self):
            self.commander.set_active(self.unique_commanders.get_count() == 0)
            self.guard3.set_active(self.heavy.get_count() == 0)
            self.guard4.set_active(self.heavy.get_count() == 0)
            options = {
                'hflame': 'Heavy flamer',
                'medic': 'Medi-pack',
                'ps': 'Platoon standard',
                'vox': 'Vox-caster',
            }
            for key, value in options.items():
                count = sum((1 if g.is_used() and g.get_unit().has_spec(key) else 0 for g in self.guards))
                if count > 1:
                    self.error('{0} can include only one {1}'.format(self.name, value))
            self.points.set(self.build_points())
            self.build_description(exclude=[self.grenades.id])

        def has_chenkov(self):
            return self.unique_commanders.get(self.Chenkov.name)

        def has_alrahem(self):
            return self.unique_commanders.get(self.AlRahem.name)

    class InfantrySquad(Unit):
        name = 'Infantry Squad'
        base_points = 50

        def __init__(self):
            gear = [
                ['Power weapon', 10],
                ['Plasma pistol', 10],
            ]
            Unit.__init__(self)
            self.count = self.opt_count(self.name, 1, 5, self.base_points)
            self.grenades = OptionsList('krak', '', [['Krak grenades', 10, 'krak']])
            self.commander = self.opt_sub_unit(Commander('Sergeant', gear, self.grenades))
            self.commissar = self.opt_optional_sub_unit(Commissar.name, Commissar(gear, self.grenades))
            self.wep1 = self.opt_options_list('Special weapon', [
                ['Flamer', 5],
                ['Grenade launcher', 5],
                ['Sniper rifle', 5],
                ['Meltagun', 10],
                ['Plasma gun', 15],
            ], limit=1)
            self.opt = self.opt_options_list('Options', [
                ['Vox-caster', 5, 'vox'],
            ])
            self.heavy = self.opt_optional_sub_unit(HeavyWeaponTeam.name, HeavyWeaponTeam(self.grenades))
            self.append_opt(self.grenades)
            self.transport = self.opt_optional_sub_unit('Transport', Chimera())

        def check_rules(self):
            self.points.set(self.build_points(exclude=[self.count.id]))
            self.build_description(exclude=[self.grenades.id, self.count.id, self.wep1.id, self.opt.id])
            trooper = ModelDescriptor(name='Guardsman', gear=['Flak armour', 'Frag grenades', 'Close combat weapon',
                                                              'Lasgun'])
            count = 9
            if self.grenades.get('krak'):
                trooper.add_gear('Krak grenades')
            if self.opt.get('vox'):
                self.description.add(trooper.clone().add_gear_opt(self.opt).build(1))
                count -= 1
            if len(self.wep1.get_all()) > 0:
                self.description.add(trooper.clone().add_gear_opt(self.wep1).build(1))
                count -= 1
            if self.heavy.get_count() > 0:
                count -= 1
            self.description.add(trooper.build(count))

    class HeavyWeaponSquad(Unit):
        name = 'Heavy Weapon Squad'
        base_points = 60

        def __init__(self):
            Unit.__init__(self)
            self.count = self.opt_count(self.name, 1, 5, self.base_points)
            self.hb = self.opt_count('Heavy bolter', 0, 3, 5)
            self.ac = self.opt_count('Autocannon', 0, 3, 5)
            self.ml = self.opt_count('Missile launcher', 0, 3, 10)
            self.lc = self.opt_count('Lascannon', 0, 3, 15)
            self.krak = self.opt_options_list('', [['Krak grenades', 5, 'krak']])

        def check_rules(self):
            norm_counts(0, 3, [self.hb, self.ac, self.ml, self.lc])
            self.set_points(self.build_points(exclude=[self.count.id]))
            self.build_description(options=[])
            team = ModelDescriptor(name=HeavyWeaponTeam.name, gear=HeavyWeaponTeam.gear)
            count = 3
            if self.krak.get('krak'):
                team.add_gear('Krak grenades')
            for opt, pt in [(self.hb, 5), (self.ac, 5), (self.ml, 10), (self.lc, 15)]:
                self.description.add(team.clone().add_gear(opt.name, pt).build(opt.get()))
                count -= opt.get()
            self.description.add(team.clone().add_gear('Mortar').build(count))

    class SpecialWeaponSquad(Unit):
        name = 'Special Weapon Squad'
        base_points = 35

        def __init__(self):
            Unit.__init__(self)
            self.count = self.opt_count(self.name, 1, 2, self.base_points)
            gear = [
                ['Flamer', 5],
                ['Grenade launcher', 5],
                ['Sniper rifle', 5],
                ['Meltagun', 10],
                ['Plasma gun', 15],
                ['Demolition charge', 20, 'dc'],
            ]
            self.wep = [self.opt_one_of('Weapon' if not i else '', gear) for i in range(3)]

        def check_rules(self):
            self.set_points(self.build_points(exclude=[self.count.id]))
            self.build_description(options=[])
            guard = ModelDescriptor(name='Guardsman', gear=['Flak armour', 'Close combat weapon'])
            for opt in self.wep:
                spec = guard.clone().add_gear(name=opt.get_selected(), points=opt.points())
                if opt.get_cur() == 'dc':
                    spec.add_gear('Lasgun')
                self.description.add(spec.build(1))
            self.description.add(guard.add_gear('Lasgun').build(3))

    class ConscriptSquad(Unit):
        name = 'Conscript Squad'
        base_points = 4

        def __init__(self, command):
            Unit.__init__(self)
            self.command = command
            self.count = self.opt_count('Conscript', 20, 50, self.base_points)
            self.next_wave = self.opt_options_list('', [['Send in the Next Wave', 75]])

        def check_rules(self):
            self.next_wave.set_active(self.command.has_chenkov())
            self.set_points(self.count.points() + self.next_wave.points())
            self.build_description(count=1, exclude=[self.count.id])
            self.description.add(ModelDescriptor(
                name='Conscript',
                gear=['Flak armour', 'Close combat weapon', 'Lasgun'],
                points=self.base_points,
                count=self.count.get()
            ).build())

    def __init__(self):
        Unit.__init__(self)
        self.command = self.CommandSquad()
        self.opt_sub_unit(self.command)
        self.squads = self.opt_units_list(self.InfantrySquad.name, self.InfantrySquad, 2, 5)
        self.hvt_enable = self.opt_options_list('', [[self.HeavyWeaponSquad.name, self.HeavyWeaponSquad.base_points,
                                                      'hvt']])
        self.hvt = self.opt_units_list(self.HeavyWeaponSquad.name, self.HeavyWeaponSquad, 1, 5)
        self.spec_enable = self.opt_options_list('', [[self.SpecialWeaponSquad.name,
                                                       self.SpecialWeaponSquad.base_points, 'spec']])
        self.spec = self.opt_units_list(self.SpecialWeaponSquad.name, self.SpecialWeaponSquad, 1, 2)
        self.cons_enable = self.opt_options_list('', [[self.ConscriptSquad.name, 80, 'cons']])
        self.cons = self.opt_sub_unit(self.ConscriptSquad(self.command))

    def check_rules(self):
        self.squads.update_range()
        self.hvt.update_range()
        self.spec.update_range()
        self.hvt.set_active(self.hvt_enable.get('hvt'))
        self.spec.set_active(self.spec_enable.get('spec'))
        self.cons.set_active(self.cons_enable.get('cons'))
        self.points.set(self.build_points(exclude=[self.hvt_enable.id, self.spec_enable.id, self.cons_enable.id]))
        self.build_description(exclude=[self.hvt_enable.id, self.spec_enable.id, self.cons_enable.id])

    def get_unique(self):
        if self.command.has_alrahem():
            return self.CommandSquad.AlRahem.name
        elif self.command.has_chenkov():
            return self.CommandSquad.Chenkov.name


class VeteranSquad(Unit):
    name = 'Veteran Squad'
    base_points = 70

    class DoctrineUser(Unit):
        def __init__(self, base_gear):
            Unit.__init__(self)
            self._base_gear = base_gear[:]
            self.demo_charge = True

        def update_doctrine(self, ids):
            self.gear = self._base_gear[:]
            if 'gr' in ids:
                self.gear += ['Carapace armour']
            else:
                self.gear += ['Flak armour']
            if 'fs' in ids:
                self.gear += ['Camo-cloaks', 'Snare mines']
            if 'dem' in ids:
                self.gear += ['Melta bombs']
                self.demo_charge = True
            else:
                self.demo_charge = False
            Unit.check_rules(self)

    class Harker(Unit):
        name = 'Gunnery Sergeant \'Stonetooth\' Harker'
        static = True
        base_points = 55
        gear = ['Frag grenades', 'Krak grenades', 'Close combat weapon', 'Flak armour', 'Payback']

    class Bastonne(Unit):
        name = 'Sergeant Bastonne'
        static = True
        base_points = 60
        gear = ['Frag grenades', 'Krak grenades', 'Power sword', 'Carapace armour', 'Hot-shot laspistol']

    class Commander(DoctrineUser):
        name = 'Veteran Sergeant'

        def __init__(self):
            VeteranSquad.DoctrineUser.__init__(self, ['Frag grenades'])
            wep = [
                ['Shotgun', 0],
                ['Bolt pistol', 2],
                ['Power weapon', 10],
                ['Plasma pistol', 10],
                ['Power fist', 15],
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Laspistol', 0]] + wep)
            self.wep2 = self.opt_one_of('', [['Close combat weapon', 0]] + wep)

    class Veteran(ListSubUnit, DoctrineUser):
        name = 'Veteran'

        def __init__(self):
            VeteranSquad.DoctrineUser.__init__(self, ['Frag grenades', 'Close combat weapon'])
            ListSubUnit.__init__(self, max_models=9)
            spec = [
                ['Flamer', 5, 'fl'],
                ['Grenade launcher', 5, 'gl'],
                ['Sniper rifle', 5, 'sr'],
                ['Meltagun', 10, 'mg'],
                ['Plasma gun', 15, 'pg'],
                ['Heavy flamer', 20, 'hflame'],
            ]
            self.spec_id = [v[2] for v in spec]
            self.wep1 = self.opt_one_of('Weapon', [['Lasgun', 0], ['Shotgun', 0]] + spec)
            self.opt = self.opt_options_list('Options', [
                ['Vox-caster', 5, 'vox'],
                ['Demolition charger', 0, 'dem'],
            ])

        def check_rules(self):
            self.opt.set_active_options(['dem'], self.demo_charge)
            self.opt.set_active_options(['vox'], self.wep1.get_cur() not in self.spec_id)
            self.wep1.set_active_options(self.spec_id, not self.opt.get('vox'))
            ListSubUnit.check_rules(self)

        def get_vox(self):
            return self.count.get() if self.opt.get('vox') else 0

        def get_demo(self):
            return self.count.get() if self.opt.get('dem') else 0

        def get_spec(self):
            return self.count.get() if self.wep1.get_cur() in self.spec_id else 0

        def get_heavy(self):
            return self.count.get() if self.wep1.get_cur() == 'hflame' else 0

    class HeavyWeaponTeam(DoctrineUser):
        name = 'Heavy Weapon Team'

        def __init__(self):
            VeteranSquad.DoctrineUser.__init__(self, ['Frag grenades', 'Close combat weapon', 'Lasgun'])
            self.wep1 = self.opt_one_of('Weapon', [
                ['Mortar', 5],
                ['Autocannon', 10],
                ['Heavy bolter', 10],
                ['Missile launcher', 15],
                ['Lascannon', 20],
            ])

    def __init__(self):
        Unit.__init__(self)
        self.commander = self.opt_sub_unit(self.Commander())
        self.unique_commanders = self.opt_optional_sub_unit('', [self.Harker(), self.Bastonne()])
        self.veterans = self.opt_units_list('', self.Veteran, 1, 9)
        self.veterans.get_units()[0].count.set(9)
        self.heavy = self.opt_optional_sub_unit('', self.HeavyWeaponTeam())
        self.opt = self.opt_options_list('Options', [
            ['Grenadiers', 30, 'gr'],
            ['Froward Sentries', 30, 'fs'],
            ['Demolitions', 30, 'dem'],
        ])
        self.transport = self.opt_optional_sub_unit('Transport', Chimera())

    def check_rules(self):
        self.commander.set_active(self.unique_commanders.get_count() == 0)
        self.veterans.update_range()

        units = [self.commander.get_unit()] + self.veterans.get_units() + ([self.heavy.get_unit().get_unit()]
                                                                           if self.heavy.get_unit() else [])
        for unit in units:
            unit.update_doctrine(self.opt.get_all())
        veterans = self.veterans.get_count()
        spec = sum((u.get_spec() for u in self.veterans.get_units()))
        if spec > 3:
            self.error('Veteran Squad may take not more 3 special weapon (taken: {0})'.format(spec))
        hflame = sum((u.get_heavy() for u in self.veterans.get_units()))
        if hflame > 1:
            self.error('Veteran Squad may take only one heavy flamer (taken: {0})'.format(hflame))
        vox = sum((u.get_vox() for u in self.veterans.get_units()))
        if vox > 1:
            self.error('Veteran Squad may take only one vox-caster (taken: {0})'.format(vox))
        if self.opt.get('dem'):
            dem = sum((u.get_demo() for u in self.veterans.get_units()))
            if dem != 1:
                self.error('Veteran in Veteran Squad with demolitions doctrine must take one demolition charge '
                           '(taken: {0})'.format(dem))
        if self.heavy.get_count() and veterans != 7:
            self.error('Veteran Squad must take 7 veterans and 1 heavy weapon team (taken: {0})'.format(veterans))
        elif self.heavy.get_count() == 0 and veterans != 9:
            self.error('Veteran Squad must take 9 veterans (taken: {0})'.format(veterans))
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return self.veterans.get_count() + self.heavy.get_count() * 2 + 1

    def get_unique(self):
        if self.unique_commanders.get(self.Harker.name):
            return self.Harker.name
        if self.unique_commanders.get(self.Bastonne.name):
            return self.Bastonne.name


class PenalLegion(Unit):
    name = 'Penal Legion'
    base_points = 80
    base_gear = ['Flak armour', 'Close combat weapon']
    static = True

    def get_count(self):
        return 10

    def check_rules(self):
        self.set_points(self.base_points)
        self.build_description()
        self.description.add(
            ModelDescriptor(name='Penal custodian', count=1, gear=self.base_gear).add_gear('Laspistol').build()
        )
        self.description.add(
            ModelDescriptor(name='Penal legionnaire', count=9, gear=self.base_gear).add_gear('Lasgun').build()
        )
