__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.space_marines2008.troops import Rhino,Razorback,DropPod
from functools import reduce


class Calgar(Unit):
    name = 'Marneus Calgar, Lorg McGragge'
    base_points = 250
    gear = ['Power sword', 'Iron Halo', 'Gauntlets of Ultramar']
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour',
        [
            ['Power Armour',0,'pwr'],
            ['Armour of Antilochus',15,'ant']
            ])

class Sicarius(StaticUnit):
    name = 'Captain Cato Sicarius'
    base_points = 200
    gear = ['Mantle of the Suzerain','Talassarian Tempest Blade','Plasma pistol','Frag grenades','Krak grenades','Iron Halo']

class Tigurius(StaticUnit):
    name = 'Chief Librarian Tigurius'
    base_points = 230
    gear = ['Power armour','Bolt pistol','Hood of Hellfire','Frag grenades','Krak grenades','Rod of Tigurius']

class Cassius(StaticUnit):
    name = 'Chaplain Cassius'
    base_points = 125
    gear = ['Power armour','Bolt pistol','Crozius Arcanum','Frag grenades','Krak grenades','Rosarius','Infernus']

class Kantor(StaticUnit):
    name = 'Chapter Master Pedro Cantor'
    base_points = 175
    gear = ['Power armour','Dorn\'s arrow','Power fist','Frag grenades','Krak grenades','Iron Halo']

class Lysander(StaticUnit):
    name = 'Captain Darnath Lysander'
    base_points = 200
    gear = ['Terminator armour','The Fist of Dorn','Storm shield']

class Shrike(StaticUnit):
    name = 'Shadow Captain Kayvaan Shrike'
    base_points = 195
    gear = ['Power armour','Bolt Pistol','The Raven\'s Talons','Frag grenades','Krak grenades','Jump pack','Iron Halo']

class Vulkan(StaticUnit):
    name = 'Forgefather Vulkan He\'Stan'
    base_points = 190
    gear = ['Artificer armour','Bolt Pistol','Frag grenades','Krak grenades','Kesare\'s Mantle','The Spear of Vulkan','Digital weapons','The Gauntlet of the Forge']

class Khan(Unit):
    name = 'Kor\'sarro Khan'
    base_points = 160
    gear = ['Power armour','Bolt Pistol','Moonfang','Frag grenades','Krak grenades','Iron Halo']
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Bike', [['Moondrakkan',45,'pimp_bike']])

    def has_bike(self):
        return self.ride.get('pimp_bike')

class ChapterMaster(Unit):
    name = 'Space Marine Chapter Master'
    base_points = 125

    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Boltgun',0,'bgun'],
            ['Storm Bolter',3,'sbgun'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Storm shield', 15, 'ss'],
            ['Power sword',15,'psw'],
            ['Lightning claw',15,'lclaw'],
            ['Plasma pistol',15,'ppist'],
            ['Power fist',25,'pfist'],
            ['Relic blade',30,'rsw'],
            ['Thunder hammer',30,'ham']
                ]
        self.armr = self.opt_one_of('Armour',[
            ['Power armour',0,'pwr'],
            ['Artificer armour',15,'art'],
            ['Terminator armour',40,'tda']
                ])
        self.wep1 = self.opt_one_of('Weapon',
            [['Chainsword',0,'csw']] + weaponlist,id='pow_w1')
        self.wep2 = self.opt_one_of('',
            [['Bolt pistol',0,'bpist']] + weaponlist,id='pow_w2')
        self.twep1 = self.opt_one_of('Weapon',[
            ['Storm bolter',0,'sbgun'],
            ['Combi-melta', 5, 'cmelta' ],
            ['Combi-flamer', 5, 'cflame' ],
            ['Combi-plasma', 5, 'cplasma' ],
            ['Lightning claw',10,'lclaw'],
            ['Thunder hammer',20,'ham']
                ],id='tda_w1')
        self.twep2 = self.opt_one_of('',[
            ['Power sword',0,'psw'],
            ['Lightning claw',5,'lclaw'],
            ['Storm shield', 10, 'ss'],
            ['Power fist',10,'pfist'],
            ['Thunder hammer',15,'ham'],
            ['Chainfist',15,'chfist']
                ],id='tda_w2')
        self.opt = self.opt_options_list('Options',[
            ['Melta bombs',5,'mbomb'],
            ['Digital weapons',10,'digw'],
            ['Hellfire rounds',10,'hfre'],
            ['Auxiliary grenade launcher',15,'auxgl']
                ])
        self.ride = self.opt_options_list('',[
            ['Jump pack',25,'jpack'],
            ['Space Marine Bike',35,'bike']],1)

    def check_rules(self):
        tda = self.armr.get_cur() == 'tda'
        self.ride.set_visible(not tda)
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.twep1.set_visible(tda)
        self.twep2.set_visible(tda)
        self.gear = ['Iron halo']
        if not tda:
            self.gear += ['Frag grenades','Krak grenades']
        Unit.check_rules(self)

class HonourGuardSquad(Unit):
    name = 'Honour Guard Squad'

    class HonourGuard(ListSubUnit):
        name = 'Honour Guard'
        min = 1
        max = 9
        base_points = 35
        gear = ['Artificer armour','Power weapon','Frag grenades','Krak grenades','Bolt pistol','Boltgun' ]
        def __init__(self):
            ListSubUnit.__init__(self)
            self.opt = self.opt_options_list('Options',[
                ['Relic blade',15,'rsw'],
                ['Auxiliary grenade launcher',15,'auxgl']
                    ])
            self.banner = self.opt_options_list('Banner',[
                ['Chapter Banner',25,'chban']])

        def have_banner(self):
            return self.banner.get('chban')

        def set_enable_banner(self, val):
            self.banner.set_active_options(['chban'], (val or self.have_banner()) and self.count.get() == 1)

    class ChapterChampion(Unit):
        name = 'Chapter Champion'
        gear = ['Artificer armour','Frag grenades','Krak grenades','Bolt pistol' ]
        base_points = 115 - 35 - 35
        def __init__(self):
            Unit.__init__(self)
            self.rng = self.opt_one_of('Weapon',[
                ['Boltgun',0,'bgun'],
                ['Combat blade',0,'csw']
                ])
            self.ccw = self.opt_one_of('',[
                ['Power sword',0,'psw'],
                ['Thunder hammer',15,'ham']
                ])

            self.opt = self.opt_options_list('Options',[
                ['Relic blade',15,'rsw'],
                ['Auxiliary grenade launcher',15,'auxgl'],
                ['Digital weapons',10,'dgwep']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.guards = self.opt_units_list(self.HonourGuard.name,self.HonourGuard,2,9)
        self.champ = self.opt_sub_unit(self.ChapterChampion())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        self.guards.update_range()
        have_banner = reduce(lambda val, u: u.have_banner() or val, self.guards.get_units(), False)
        for u in self.guards.get_units():
            u.set_enable_banner(not have_banner)
        self.transport.set_active_options([Razorback.name], self.get_count() <= 6)
        self.points.set(self.build_points(count=1))
        self.build_description()
    def get_count(self):
        return self.champ.get_count() + self.guards.get_count()

class Captain(Unit):
    name = 'Space Marine Captain'
    base_points = 100
    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Boltgun',0,'bgun'],
            ['Storm Bolter',3,'sbgun'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Storm shield', 15, 'ss'],
            ['Power sword',15,'psw'],
            ['Lightning claw',15,'lclaw'],
            ['Plasma pistol',15,'ppist'],
            ['Power fist',25,'pfist'],
            ['Relic blade',30,'rsw'],
            ['Thunder hammer',30,'ham']
                ]
        self.armr = self.opt_one_of('Armour',[
            ['Power armour',0,'pwr'],
            ['Artificer armour',15,'art'],
            ['Terminator armour',40,'tda']
                ])
        self.wep1 = self.opt_one_of('Weapon',
            [['Chainsword',0,'csw']] + weaponlist,id='pow_w1')
        self.wep2 = self.opt_one_of('',
            [['Bolt pistol',0,'bpist']] + weaponlist,id='pow_w2')
        self.twep1 = self.opt_one_of('Weapon',[
            ['Storm bolter',0,'sbgun'],
            ['Combi-melta', 5, 'cmelta' ],
            ['Combi-flamer', 5, 'cflame' ],
            ['Combi-plasma', 5, 'cplasma' ],
            ['Lightning claw',10,'lclaw'],
            ['Thunder hammer',20,'ham']
                ],id='tda_w1')
        self.twep2 = self.opt_one_of('',[
            ['Power sword',0,'psw'],
            ['Lightning claw',5,'lclaw'],
            ['Storm shield', 10, 'ss'],
            ['Power fist',10,'pfist'],
            ['Thunder hammer',15,'ham'],
            ['Chainfist',15,'chfist']
                ],id='tda_w2')
        self.opt = self.opt_options_list('Options',[
            ['Melta bombs',5,'mbomb'],
            ['Digital weapons',10,'digw'],
            ['Hellfire rounds',10,'hfre'],
            ['Auxiliary grenade launcher',15,'auxgl']
                ])
        self.ride = self.opt_options_list('',[
            ['Jump pack',25,'jpack'],
            ['Space Marine Bike',35,'bike']],1)

    def has_bike(self):
        return self.ride.get('bike')

    def check_rules(self):
        tda = self.armr.get_cur() == 'tda'
        self.ride.set_visible(not tda)
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.twep1.set_visible(tda)
        self.twep2.set_visible(tda)
        self.gear = ['Iron halo']
        if not tda:
            self.gear += ['Frag grenades','Krak grenades']
        Unit.check_rules(self)

class CommandSquad(Unit):
    name = 'Command Squad'

    class Apothecary(Unit):
        base_points = 23
        name = 'Apothecary'
        gear = ['Chainsword','Power armour', 'Frag grenades', 'Krak grenades', 'Narthecium']
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon',[
                ['Bolt pistol',0,'bpist'],
                ['Boltgun',0,'bgun']
                ])

    class Veteran(Unit):
        name = 'Veteran'
        gear = ['Power armour', 'Frag grenades', 'Krak grenades']
        base_points = 23
        def __init__(self):
            Unit.__init__(self)
            self.champ = self.opt_options_list('', [ ["Company champion", 15, 'up'],])
            weaponlist = [
            ['Storm Bolter',3,'sbgun'],
            ['Flamer', 5, 'flame' ],
            ['Meltagun', 10, 'mgun' ],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Plasmagun',15,'pgun'],
            ['Power sword',15,'psw'],
            ['Lightning claw',15,'lclaw'],
            ['Power fist',25,'pfist'],
            ['Thunder hammer',30,'ham']
                ]
            self.wep1 = self.opt_one_of('Weapon', [['Chainsword',0,'csw']] + weaponlist)
            self.wep2 = self.opt_one_of('Weapon',
                [['Bolt pistol',0,'bpist'], ['Boltgun',0,'bgun'], ['Plasma pistol',15,'ppist']] + weaponlist)
            self.opt = self.opt_options_list('Options',[
                ['Melta bombs',5,'mbomb'],
                ['Storm shield', 15, 'ss']
            ])
            self.banner = self.opt_options_list('Standard',[
                ['Company Standard',15,'comstd']])

            self.have_flag = False
            self.have_champ = False
            self.have_bikes = False

        def check_rules(self):
            ch = self.champ.get('up')
            self.wep1.set_visible(not ch)
            self.wep2.set_visible(not ch)
            self.opt.set_visible(not ch)
            self.banner.set_visible(not ch)

            self.champ.set_active_options(['up'], not self.have_champ or ch)
            if not self.banner.get_all():
                self.banner.set_active_options(self.banner.get_all_ids(), not self.have_flag)
            self.gear = ["Power Armour", 'Frag grenades', 'Krak grenades',]
            if self.have_bikes:
                self.gear += ['Space Marine Bike']
            self.set_points(self.build_points())
            if ch:
                self.gear += ['Power Weapon', 'Combat Shield']
                self.build_description(name='Company champion', exclude=[self.champ.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.apoth = self.opt_sub_unit(self.Apothecary())
        self.hg = [self.opt_sub_unit(self.Veteran()) for i in range(0,4)]
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()], id='trans')
        self.ride = self.opt_options_list('',[['Space Marine Bikes',90,'bike']])

    def check_rules(self):
        ch = reduce(lambda val, c: val + (1 if c.get_unit().champ.get('up') else 0), self.hg, 0)
        flag = reduce(lambda val, c: val + (1 if c.get_unit().banner.get_all() else 0), self.hg, 0)
        bikes = self.ride.get('bike')
        for hg in self.hg:
            hg.get_unit().have_bch = (ch > 0)
            hg.get_unit().have_flag = (flag > 0)
            hg.get_unit().have_bikes = bikes
            hg.get_unit().check_rules()
        self.transport.get_options().set_active_all(not bikes)
        self.ride.set_active_all(self.transport.get_count() == 0)
        self.points.set(self.build_points())
        self.build_description(exclude=[self.ride.id])

class Librarian(Unit):
    name = 'Space Marine Librarian'
    base_points = 100
    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour',[
            ['Power armour',0,'pwr'],
            ['Terminator armour',25,'tda']
            ])
        self.wep = self.opt_one_of('Weapon',[
            ['Bolt pistol',0,'bpist'],
            ['Boltgun',0,'bgun'],
            ['Storm Bolter',3,'sbgun'],
            ['Combi-melta', 15, 'cmelta' ],
            ['Combi-flamer', 15, 'cflame' ],
            ['Combi-plasma', 15, 'cplasma' ],
            ['Plasma pistol',15,'ppist'],
                ])
        self.twep = self.opt_one_of('Weapon',[
            ['No additional weapon',0,'nada'],
            ['Storm Bolter',5,'sbgun'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Storm shield', 15, 'ss']
            ])

        self.upgr = self.opt_options_list('Options',[
            ['Epistolary',50,'epst'],
            ])
        self.ride = self.opt_options_list('',[
            ['Jump pack',25,'jpack'],
            ['Space Marine Bike',35,'bike']
            ], limit=1)

    def check_rules(self):
        tda = (self.armr.get_cur() == 'tda')
        self.ride.set_visible(not tda)
        self.wep.set_visible(not tda)
        self.twep.set_visible(tda)
        self.gear = ['Force weapon', 'Psychic hood']
        if not tda:
            self.gear += ['Frag grenades','Krak grenades']
        Unit.check_rules(self)

class Chaplain(Unit):
    name = 'Space Marine Chaplain'
    base_points = 100
    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour',[
            ['Power armour',0,'pwr'],
            ['Terminator armour',30,'tda']
                ])
        self.wep = self.opt_one_of('Weapon',[
            ['Bolt pistol',0,'bpist'],
            ['Boltgun',0,'bgun'],
            ['Storm Bolter',3,'sbgun'],
            ['Combi-melta', 15, 'cmelta' ],
            ['Combi-flamer', 15, 'cflame' ],
            ['Combi-plasma', 15, 'cplasma' ],
            ['Plasma pistol',15,'ppist'],
            ['Power fist',15,'pfist']
                ], id = 'pwr_w')
        self.twep = self.opt_one_of('Weapon',[
            ['Storm Bolter',0,'sbgun'],
            ['Combi-melta', 5, 'cmelta' ],
            ['Combi-flamer', 5, 'cflame' ],
            ['Combi-plasma', 5, 'cplasma' ],
                ],id='tda_w')

        self.opt = self.opt_options_list('Options',[
            ['Melta bombs',5,'mbomb'],
            ['Digital weapons', 10, 'ss']
            ])
        self.ride = self.opt_options_list('',[
            ['Jump pack',25,'jpack'],
            ['Space Marine Bike',35,'bike']], limit=1)

    def check_rules(self):
        tda = (self.armr.get_cur() == 'tda')
        self.ride.set_visible(not tda)
        self.wep.set_visible(not tda)
        self.twep.set_visible(tda)
        self.gear = ['Rozarius', 'Crozius Arcanum']
        if not tda:
            self.gear += ['Frag grenades','Krak grenades']
        Unit.check_rules(self)

class ForgeMaster(Unit):
    name = 'Master Of the Forge'
    base_points = 100
    gear = ['Frag grenades','Krak grenades','Artificar armour']
    def __init__(self):
        Unit.__init__(self)
        self.harness = self.opt_one_of('Special',[
            ['Servo-harness',0,'pwr'],
            ['Conversion beamer',20,'cbeam']
                ])
        self.rng = self.opt_one_of('Ranged weapon',[
            ['Bolt pistol',0,'bpist'],
            ['Boltgun',0,'bgun'],
            ['Storm Bolter',5,'sbgun'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Plasma pistol',15,'ppist']
                ])
        self.ccw = self.opt_options_list('Close combat weapon',[
            ['Power sword',15,'psw'],
            ['Thunder hammer', 30, 'ham' ]
                ],1)
        self.ride = self.opt_options_list('Options',[
            ['Space Marine Bike',35,'bike']])
        self.opt = self.opt_options_list('',[
            ['Digital weapons', 10, 'ss']
                ])
    def check_rules(self):
        self.rng.set_visible(not self.harness.get_cur() == 'cbeam')
        Unit.check_rules(self)
