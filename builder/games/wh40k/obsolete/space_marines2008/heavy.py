__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit
from builder.games.wh40k.obsolete.space_marines2008.troops import DropPod, Rhino, Razorback

class LandRaider(Unit):
    name = "Land Raider"
    base_points = 250
    gear = ['Twin-linked heavy bolter','Twin-linked lascannon','Twin-linked lascannon','Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Storm bolter", 10, 'sbgun'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Multi melta", 10, 'mmelta'],
            ["Extra armour", 15, 'exarm']
            ])

class LRCrusader(Unit):
    name = "Land Raider Crusader"
    base_points = 250
    gear = ['Twin-linked assault cannon','Hurricane bolter','Hurricane bolter','Smoke launchers','Searchlight','Frag Assault Launcher']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Storm bolter", 10, 'sbgun'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Multi melta", 10, 'mmelta'],
            ["Extra armour", 15, 'exarm']
            ])

class LRRedeemer(Unit):
    name = "Land Raider Redeemer"
    base_points = 240
    gear = ['Twin-linked assault cannon','Flamestorm cannon','Flamestorm cannon','Smoke launchers','Searchlight','Frag Assault Launcher']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
            ["Storm bolter", 10, 'sbgun'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Multi melta", 10, 'mmelta'],
            ["Extra armour", 15, 'exarm']
            ])

class Predator(Unit):
    name = "Predator"
    base_points = 60
    gear = ['Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Turret', [
            ['Autocannon',0,'acan'],
            ['Twin-linked lascannon',45,'tllcan']
            ])
        self.side = self.opt_options_list('Side sponsons',[
            ['Sponsons with heavy bolters',25,'hbgun'],
            ['Sponsons with lascannons',60,'lcan']
            ],1)
        self.opt = self.opt_options_list('Options',[
            ["Storm bolter", 10, 'sbgun'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Dozer blade", 5, 'dbld'],
            ["Extra armour", 15, 'exarm']
            ])

class Whirlwind(Unit):
    name = "Whirlwind"
    base_points = 85
    gear = ['Whirlwind multiple missile launcher','Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
            ["Storm bolter", 10, 'sbgun'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Dozer blade", 5, 'dbld'],
            ["Extra armour", 15, 'exarm']
                ])

class Vindicator(Unit):
    name = "Vindicator"
    base_points = 115
    gear = ['Demolisher cannon','Storm bolter','Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
            ["Storm bolter", 10, 'sbgun'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Dozer blade", 5, 'dbld'],
            ["Siege shield", 10, 'sshld'],
            ["Extra armour", 15, 'exarm']
            ])

class Thunderfire(Unit):
    name = "Thunderfire cannon"
    base_points = 100

    class Gunner(StaticUnit):
        name = "Techmarine gunner"
        gear = ['Artificer armour','Bolt pistol','Frag grenades','Krak grenades','Servo-harness']

    def __init__(self):
        Unit.__init__(self)
        self.gunner = self.opt_sub_unit(self.Gunner())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

class StormravenGunship(Unit):
    name = 'Stormraven Gunship'
    base_points = 200
    gear = ['4 stormstrike missiles','Ceramite plating']
    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon',[
            ['Twin-linked heavy bolter',0],
            ['Twin-linked Multi-melta',0],
            ['Typhoon missile launcher',25],
            ])
        self.wep2 = self.opt_one_of('',[
            ['Twin-linked Plasma cannon',0,'pcan'],
            ['Twin-linked Assault cannon',0,'asscan'],
            ['Twin-linked Lascannon',0,'tllcan']
        ])
        self.side = self.opt_options_list('Side sponsons',[
            ['Hurricane bolters',30],
            ],1)

        self.opt = self.opt_options_list('Options',[
            ["Searchlight", 1],
            ["Locator beacon", 15],
            ["Extra armour", 5],
            ])

class Devastators(Unit):
    name = 'Devastator'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

    class Sergeant(Unit):
        base_points = 90 - 16 * 4
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Signum']

        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Chainsword', 0, 'chain' ],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Stormbolter', 10, 'sbgun' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun', 0, 'bgun' ]] + weaponlist)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist' ]] + weaponlist)
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine',4,9,16)
        weaponlist = [
            ['Boltgun', 0, 'bgun' ],
            ['Heavy bolter', 15, 'hbgun' ],
            ['Multi-melta',15,'mmgun'],
            ['Missile launcher', 15, 'mlaunch' ],
            ['Plasma cannon', 25, 'pcannon' ],
            ['Lascannon', 35, 'lcannon']
        ]
        self.hvy1 = self.opt_one_of('Heavy weapon',weaponlist)
        self.hvy2 = self.opt_one_of('',weaponlist)
        self.hvy3 = self.opt_one_of('',weaponlist)
        self.hvy4 = self.opt_one_of('',weaponlist)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(count=self.marines.get(), exclude=[self.marines.id, self.hvy1.id, self.hvy2.id,
                                                                  self.hvy3.id, self.hvy4.id, ])
        self.description.add('Boltgun', self.marines.get() - 4)
        self.description.add(self.hvy1.get_selected())
        self.description.add(self.hvy2.get_selected())
        self.description.add(self.hvy3.get_selected())
        self.description.add(self.hvy4.get_selected())

    def get_count(self):
        return self.marines.get() + 1

class Chronus(StaticUnit):
    name = 'Brother-Sergeant Antharo Chronus'
    base_points = 70
    gear = ['Power armour','Bolt pistol','Frag grenades','Krak grenades','Servo-arm']
