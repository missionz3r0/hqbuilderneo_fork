__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.blood_angels.transport import Rhino, DropPod, Razorback, LandRaider, LandRaiderCrusader, LandRaiderRedeemer
from builder.core.options import norm_counts
from functools import reduce

class Chaplain(Unit):
    name = 'Chaplain'
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Terminator Armour", 30, "tda"],
            ])

        wep = [
            ["Boltgun", 0, "gun"],
            ["Storm Bolter", 3, "storm"],
            ["Hand flamer", 10, "hflame"],
            ["Combi-flamer", 10, "cflame"],
            ["Combi-melta", 10, "cmelta"],
            ["Combi-plasma", 10, "cplasma"],
            ["Plasma Pistol", 15, "plasma"],
            ["Infernus Pistol", 15, "infernus"],
            ["Power Fist", 15, "fist"],
            ]

        self.wep1 = self.opt_one_of('Weapon', [["Bolt Pistol", 0, "pistol"]] + wep)

        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ])

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ], limit = 1)

        self.bombs = self.opt_options_list("Options", [
            ["Melta bombs", 5, "bomb"],
        ])

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.mount.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.gear = ["Rosarius", 'Crozius Arcanum']
        if not tda:
            self.gear += ['Frag and Krak Grenades']
        Unit.check_rules(self)

class SanguinaryGuards(Unit):
    name = 'Sanguinary Guard'

    class SanguinaryGuard(Unit):
        base_points = 40
        name = 'Sanguinary Guard'

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ["Angelus Boltgun", 0, "gun"],
                ["Plasma Pistol", 10, "plasma"],
                ["Infernus Pistol", 10, "infernus"],
                ])
            self.wep2 = self.opt_one_of('', [
                ["Glaive Encarmine", 0, "ccw"],
                ["Power Fist", 10, "fist"],
                ])
            self.flag = self.opt_options_list('Standard', [
                ["Chapter Banner", 30, "cb"],
                ], limit=1)
            self.have_flag = False
            self.have_dm = False

        def check_rules(self):
            if not self.flag.get_all():
                self.flag.set_active_options(self.flag.get_all_ids(), not self.have_flag)
            self.gear = ["Artificer Armour", 'Frag and Krak Grenades', 'Jump Pack']
            if self.have_dm:
                self.gear += ['Death Mask']
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.sg = [self.opt_sub_unit(self.SanguinaryGuard()) for i in range(0,5)]
        self.dm = self.opt_options_list('', [['Death Masks', 25, 'dm']])

    def check_rules(self):
        dm = self.dm.get('dm')
        flag = reduce(lambda val, c: val + (1 if c.get_unit().flag.get_all() else 0), self.sg, 0)
        for sg in self.sg:
            sg.get_unit().have_flag = (flag > 0)
            sg.get_unit().have_dm = dm
            sg.get_unit().check_rules()
        self.points.set(self.build_points())
        self.build_description(exclude=[self.dm.id])

    def get_unique(self):
        if reduce(lambda val, c: val + (1 if c.get_unit().flag.get('cb') else 0), self.sg, 0):
            return "Chapter Banner"


class FuriosoDreadnought(Unit):
    name = 'Furioso Dreadnought'
    base_points = 125
    gear = ['Smoke launchers']

    def __init__(self):
        Unit.__init__(self)

        self.wep1 = self.opt_one_of('Weapon',[
            ['Blood Fist',0,'dccw'],
            ['Frag Cannon',0,'can'],
        ])

        self.bin1 = self.opt_one_of('',[
            ['Built-in Storm bolter',0,'sbgun'],
            ['Built-in Heavy flamer',10,'hflame']
        ])

        self.wep2 = self.opt_one_of('',[
            ['Blood Fist',0,'dccw'],
            ['Frag Cannon',0,'can'],
            ])

        self.talons = self.opt_options_list('',[
            ["Blood talons", 0, 'bt'],
        ])

        self.opt = self.opt_options_list('Options',[
            ["Extra armour", 15, 'earmr'],
            ["Magna-grapple", 15, 'mg'],
            ["Searchlight", 1, 'sl'],
        ])
        self.lib = self.opt_options_list('',[
            ["Librarian", 50, 'up'],
            ])

        self.psy = self.opt_options_list("Psychic powers", [
            ["Blood Boil", 0, "boil"],
            ["Fear of the Darkness", 0, "fear"],
            ["Might of Heroes", 0, "might"],
            ["Shackle Soul", 0, "shackle"],
            ["Shield of Sanguinius", 0, "sangshield"],
            ["Smite", 0, "smite"],
            ["The Blood Lance", 0, "lance"],
            ["The Sanguine Sword", 0, "sword"],
            ["Unleash Rage", 0, "rage"],
            ["Wings of Sanguinius", 0, "wings"],
            ], limit=2)

        self.gear_opt = [self.wep1, self.opt, self.talons, self.bin1, self.wep2]
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        lib = self.lib.get('up')
        self.psy.set_visible(lib)
        for o in self.gear_opt:
            o.set_visible(not lib)
        self.talons.set_active_options(['bt'], self.wep1.get_cur() == 'dccw' and self.wep2.get_cur() == 'dccw' )
        self.bin1.set_visible(self.wep1.get_cur() == 'dccw' and not lib)
        self.wep1.set_visible(not self.talons.get('bt') and not lib)
        self.wep2.set_visible(not self.talons.get('bt') and not lib)

        self.points.set(self.build_points())
        if lib:
            self.build_description(name='Furioso Librarian', add=['Blood Fist', 'Built-in Storm Bolter', 'Psychic Hood', 'Forced Weapon'], exclude=[self.lib.id])
        else:
            self.build_description(exclude=[self.talons.id])
            if self.wep2.get_cur() == 'dccw' or self.talons.get('bt'):
                self.description.add('Built-in Meltagun')
            if self.talons.get('bt'):
                self.description.add('Blood Talon', 2)

class TerminatorAssaultSquad(Unit):
    name = 'Terminator Assault Squad'

    class Sergeant(Unit):
        name = 'Terminator Sergeant'
        base_points = 40
        gear = ['Terminator armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [['Lightning Claws', 0], [' Thunder Hammer and Storm Shield', 0] ])

    class Terminator(ListSubUnit):
        name = 'Terminator'
        base_points = 40
        gear = ['Terminator armour']
        min = 1
        max = 9

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [['Lightning Claws', 0], [' Thunder Hammer and Storm Shield', 5] ])

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Sergeant())
        self.terms = self.opt_units_list(self.Terminator.name, self.Terminator, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(), LandRaiderCrusader(), LandRaiderRedeemer()])

    def check_rules(self):
        self.terms.update_range()
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.terms.get_count() + 1

class TerminatorSquad(Unit):
    name = 'Terminator Squad'

    class Terminator(ListSubUnit):
        name = 'Terminator'
        base_points = 40
        min = 1
        max = 9
        gear = ['Terminator armour']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.ccw = self.opt_one_of('Close combat weapon',[
                ['Power fist',0,'pfist'],
                ['Chainfist',5,'chfist']
            ])
            self.rng = self.opt_one_of('Ranged weapon',[
                ['Storm bolter',0,'sbgun'],
                ['Heavy flamer',5,'hflame'],
                ['Assault cannon',30,'asscan']
            ])
            self.cym = self.opt_options_list('Take missile launcher',[["Cyclone missile launcher", 30, 'cmlnch']])

        def has_special(self):
            return (1 if self.cym.get('cmlnch') or self.rng.get_cur() != 'sbgun' else 0) * self.count.get()

        def check_rules(self):
            self.cym.set_active(self.rng.get_cur() =='sbgun')
            self.rng.set_active_options(['hflame','asscan'], not self.cym.get('cmlnch'))
            ListSubUnit.check_rules(self)

    class TerminatorSergeant(StaticUnit):
        name = "Terminator Sergeant"
        base_points = 40
        gear = ['Terminator armour','Storm bolter','Power sword']
        static = True

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.TerminatorSergeant()
        self.terms = self.opt_units_list(self.Terminator.name, self.Terminator, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(), LandRaiderCrusader(), LandRaiderRedeemer()])

    def check_rules(self):
        self.terms.update_range()
        spec_limit = 1 if self.terms.get_count() < 9 else 2
        spec_total = reduce(lambda val, u: u.has_special() + val, self.terms.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon (Cyclone missile launcher, Heavy flamer, Assault cannon) is over limit ({0})'.format(spec_limit))
        self.set_points(self.build_points(count=1, base_points=self.TerminatorSergeant.base_points))
        self.build_description(add=[self.leader.get_description()])

    def get_count(self):
        return self.terms.get_count() + self.leader.get_count()


class SternguardVeteranSquad(Unit):
    name = 'Sternguard Veteran Squad'

    class SpaceMarineSergeant(Unit):
        name = 'Space Marine Sergeant'
        base_points = 25
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Chainsword',0,'chsw'],
                ['Plasma pistol',15,'ppist'],
                ['Power weapon',15,'psw'],
                ['Lightning claw',15,'lclaw'],
                ['Power fist',25,'pfist']
            ]
            combilist = [
                ['Storm bolter', 5, 'sbgun'],
                ['Combi-melta', 5, 'cmelta' ],
                ['Combi-flamer', 5, 'cflame' ],
                ['Combi-plasma', 5, 'cplasma' ]
            ]
            self.wep1 = self.opt_one_of('Weapon',
                [['Bolt pistol', 0, 'bpist' ]] + weaponlist)
            self.wep2 = self.opt_one_of('Weapon',
                [['Boltgun', 0, 'bgun' ]] + weaponlist + combilist)
            self.opt = self.opt_options_list('Options',[["Melta bombs", 5, 'mbomb']])

    class Veteran(ListSubUnit):
        name = "Veteran"
        base_points = 25
        gear = ['Power armor', 'Frag grenades', 'Krak grenades','Bolt pistol']
        min = 1
        max = 9
        specweaponlist = ['flame','mgun','hbgun','mulmgun','mlaunch','pgun','pcannon','hflame','lcannon']
        def __init__(self):
            ListSubUnit.__init__(self)
            combilist = [
                ['Storm bolter', 5, 'sbgun'],
                ['Combi-melta', 5, 'cmelta' ],
                ['Combi-flamer', 5, 'cflame' ],
                ['Combi-plasma', 5, 'cplasma' ]
            ]
            speclist = [
                ['Flamer', 5, 'flame' ],
                ['Meltagun', 5, 'mgun' ],
                ['Heavy bolter', 5, 'hbgun' ],
                ['Multi-melta', 5, 'mulmgun' ],
                ['Missile launcher', 5, 'mlaunch' ],
                ['Plasmagun', 10, 'pgun' ],
                ['Plasma cannon', 10, 'pcannon' ],
                ['Heavy flamer', 10, 'hflame' ],
                ['Lascannon', 15, 'lcannon']
            ]
            self.wep = self.opt_one_of('Weapon',
                [['Boltgun', 0, 'bgun' ]] + combilist + speclist)

        def has_special(self):
            return self.get_count() if self.wep.get_cur() in self.specweaponlist else 0

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.SpaceMarineSergeant())
        self.vets = self.opt_units_list(self.Veteran.name, self.Veteran, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback(), LandRaider(),
                                                                  LandRaiderCrusader(), LandRaiderRedeemer()])

    def check_rules(self):
        self.vets.update_range()
        spec_limit = 2
        spec_total = reduce(lambda val, u: u.has_special() + val, self.vets.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon is over limit ({0})'.format(spec_limit))
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.vets.get_count() + self.sergeant.get_count()

class Techmarine(Unit):
    name = 'Techmarine'
    base_points = 50
    gear = ['Frag grenades','Krak grenades','Artificar armour']

    class Servitors(Unit):
        name = "Servitors"
        base_points = 10

        def __init__(self):
            Unit.__init__(self)
            self.serv = self.opt_count(self.name, 1, 5, self.base_points)
            self.hw = [
                self.opt_count('Heavy bolter', 0, 2, 20),
                self.opt_count('Multi-melta', 0, 2, 30),
                self.opt_count('Plasma cannon', 0, 2, 30)
                ]

        def check_rules(self):
            norm_counts(0, min(2, self.serv.get()), self.hw)
            self.set_points(self.build_points(count=1, base_points=0))
            self.build_description(exclude=[self.serv.id])
            self.description.add('Servo-Arm', self.serv.get() - reduce(lambda val, h: val + h.get(), self.hw, 0))

    def __init__(self):
        Unit.__init__(self)
        self.harness = self.opt_one_of('Special',[
            ['Servo-arm',0,'arm'],
            ['Jump Pack',0,'jp'],
            ['Servo-harness',25,'sharn']
        ])
        self.rng = self.opt_one_of('Ranged weapon',[
            ['Bolt pistol',0,'bpist'],
            ['Boltgun',0,'bgun'],
            ['Storm Bolter',5,'sbgun'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Plasma pistol',15,'ppist']
        ])
        self.ccw = self.opt_options_list('Close combat weapon',[
            ['Power weapon',15,'psw'],
            ['Thunder hammer', 30, 'ham' ]
        ],1)
        self.serv = self.opt_optional_sub_unit('', self.Servitors())

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return 1 if not self.serv.get_unit() else 1 + self.serv.get_unit().get_unit().serv.get()

class SanguinaryPriest(Unit):
    name = "Sanguinary Priest"
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Terminator Armour", 35, "tda"],
            ])

        self.wep1 = self.opt_one_of('Weapon', [
            ["Bolt Pistol", 0, "pistol"],
            ["Storm Bolter", 3, "storm"],
            ["Hand flamer", 10, "hflame"],
            ["Combi-flamer", 10, "cflame"],
            ["Combi-melta", 10, "cmelta"],
            ["Combi-plasma", 10, "cplasma"],
            ["Plasma Pistol", 15, "plasma"],
            ["Infernus Pistol", 15, "infernus"],
            ])

        self.wep2 = self.opt_one_of('', [
            ["Chainsword", 0, "sword"],
            ["Power Weapon", 15, "power"],
            ["Lightning Claw", 15, "claw"],
            ["Power Fist", 25, "fist"],
            ])

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ], limit = 1)

        self.bombs = self.opt_options_list("Options", [
            ["Melta bombs", 5, "bomb"],
        ])

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.mount.set_visible(not tda)
        if not tda:
            self.gear = ['Blood Chalice', 'Frag and Krak Grenades']
        else:
            self.gear = ['Chalice of Blood', 'Power Sword']
        Unit.check_rules(self)

class Corbulo(StaticUnit):
    name = 'Brother Corbulo'
    base_points = 105
    gear = ['Power Armour', 'Bolt pistol', 'Frag and Krak Grenades', "Heaven's Teeth", 'The Red Grail']


