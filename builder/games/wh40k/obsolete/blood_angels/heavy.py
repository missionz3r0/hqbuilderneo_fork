__author__ = 'Denis Romanov'

from builder.games.wh40k.obsolete.blood_angels.transport import *

class Dreadnought(Unit):
    name = 'Dreadnought'
    base_points = 105
    gear = ['Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon',[
            ['Multi-melta',0,'mmelta'],
            ['Twin-linked heavy flamer',0,'tlhflame'],
            ['Twin-linked heavy bolter',5,'tlhbgun'],
            ['Twin-linked autocannon',10,'tlacan'],
            ['Plasma cannon',10,'pcan'],
            ['Assault cannon',10,'asscan'],
            ['Twin-linked lascannon',30,'tllcan']
        ])
        self.wep2 = self.opt_one_of('',[
            ['Blood fist',0,'dccw'],
            ['Twin-linked autocannon',10,'tlacan'],
            ['Missile launcher',10,'mlnch']
        ])
        self.bin = self.opt_one_of('',[
            ['Built-in Storm bolter',0,'sbgun'],
            ['Built-in Heavy flamer',10,'hflame']
        ])
        self.opt = self.opt_options_list('Options',[
            ["Extra armour", 15, 'earmr'],
            ["Searchlight", 1],
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.bin.set_visible(self.wep2.get_cur() == 'dccw')
        Unit.check_rules(self)


class StormravenGunship(Unit):
    name = 'Stormraven Gunship'
    base_points = 200
    gear = ['Bloodstrike missile' for _ in range(4)] + ['Ceramite plating']
    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon',[
            ['Twin-linked heavy bolter',0],
            ['Twin-linked Multi-melta',0],
            ['Typhoon missile launcher',25],
        ])
        self.wep2 = self.opt_one_of('',[
            ['Twin-linked Plasma cannon',0,'pcan'],
            ['Twin-linked Assault cannon',0,'asscan'],
            ['Twin-linked Lascannon',0,'tllcan']
        ])
        self.side = self.opt_options_list('Side sponsons',[
            ['Hurricane bolters',30],
            ],1)

        self.opt = self.opt_options_list('Options',[
            ["Searchlight", 1],
            ["Locator beacon", 15],
            ["Extra armour", 15],
        ])

class Predator(Unit):
    name = 'Predator'
    base_points = 70
    gear = ["Smoke Launchers"]

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of("Weapon", [
            ["Autocannon", 0, "auto"],
            ["Twin-linked Lascannon", 45, "las"],
            ])
        self.sp = self.opt_options_list("Side Sponsons", [
            ["Heavy Bolters", 30, "bolt"],
            ["Lascannons", 65, "las"],
            ], limit = 1)
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Extra Armour", 15, "rh_armor"],
            ["Searchlight", 1],
            ])

class Devastators(Unit):
    name = 'Devastator'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

    class Sergeant(Unit):
        base_points = 90 - 16 * 4
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Signum']

        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Chainsword', 0, 'chain' ],
                ['Stormbolter', 3, 'sbgun' ],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun', 0, 'bgun' ]] + weaponlist)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist' ]] + weaponlist)
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine',4,9,16)
        weaponlist = [
            ['Boltgun', 0, 'bgun' ],
            ['Heavy bolter', 10, 'hbgun' ],
            ['Multi-melta',10,'mmgun'],
            ['Missile launcher', 10, 'mlaunch' ],
            ['Plasma cannon', 15, 'pcannon' ],
            ['Lascannon', 25, 'lcannon']
        ]
        self.hvy1 = self.opt_one_of('Heavy weapon',weaponlist)
        self.hvy2 = self.opt_one_of('',weaponlist)
        self.hvy3 = self.opt_one_of('',weaponlist)
        self.hvy4 = self.opt_one_of('',weaponlist)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback(), LandRaider(),
                                                                  LandRaiderCrusader(), LandRaiderRedeemer()])

    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(count=self.marines.get(), exclude=[self.marines.id, self.hvy1.id, self.hvy2.id,
                                                                  self.hvy3.id, self.hvy4.id, ])
        self.description.add('Boltgun', self.marines.get() - 4)
        self.description.add(self.hvy1.get_selected())
        self.description.add(self.hvy2.get_selected())
        self.description.add(self.hvy3.get_selected())
        self.description.add(self.hvy4.get_selected())

    def get_count(self):
        return self.marines.get() + 1

class Whirlwind(Unit):
    name = "Whirlwind"
    base_points = 90
    gear = ["Whirlwind Multiple Missile Launcher", "Smoke Launchers"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Extra Armour", 15, "rh_armor"],
            ["Searchlight", 1],
            ])

class Vindicator(Unit):
    name = "Vindicator"
    base_points = 145
    gear = ["Demolisher Cannon", "Storm Bolter", "Smoke Launchers"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Siege Shield", 10, "shield"],
            ["Extra Armour", 15, "rh_armor"],
            ["Searchlight", 1],
            ])

