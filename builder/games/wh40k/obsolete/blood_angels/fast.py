__author__ = 'Denis Romanov'

from builder.core.options import norm_counts
from builder.core.unit import ListSubUnit, ListUnit
from builder.games.wh40k.obsolete.blood_angels.transport import *
from functools import reduce


class VanguardVeteranSquad(Unit):
    name = 'Vanguard Veteran Squad'

    class Veteran(ListSubUnit):
        name = 'Veteran'
        start_points = 20
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=9)

            weaponlist = [
                ['Hand flamer', 10, 'hf'],
                ['Plasma pistol', 15, 'ppist'],
                ['Infernus pistol', 15, 'ip'],
                ['Power weapon', 15, 'psw'],
                ['Lightning claw', 15, 'lclaw'],
                ['Storm shield', 20, 'ssh'],
                ['Power fist', 25, 'pfist'],
                ['Thunder hammer', 30, 'ham']
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Chainsword', 0, 'csw']] + weaponlist)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + weaponlist)
            self.opt = self.opt_options_list('Options', [['Melta bombs', 5, 'mbomb']])

        def set_jp(self, jp):
            self.base_points = self.start_points + jp.points()
            ListSubUnit.check_rules(self)
            self.description.add(jp.get_selected())

        def check_rules(self):
            pass

    class Sergeant(Unit):
        name = 'Space Marine Sergeant'
        start_points = 115 - 4 * 20
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Bolt pistol']

        def __init__(self):
            Unit.__init__(self)
            self.wep2 = self.opt_one_of('', [
                ['Power sword', 0, 'psw'],
                ['Lightning claw', 0, 'lclaw'],
                ['Glaive Encarmine', 0, 'ge'],
                ['Power fist', 10, 'pfist'],
                ['Thunder hammer', 15, 'ham'],
            ])
            self.opt = self.opt_options_list('Options', [['Melta bombs', 5, 'mbomb']])

        def set_jp(self, jp):
            self.base_points = self.start_points + jp.points()
            Unit.check_rules(self)
            self.description.add(jp.get_selected())

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.vets = self.opt_units_list(self.Veteran.name, self.Veteran, 4, 9)
        self.leader = self.opt_sub_unit(self.Sergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback(),
                                                                  LandRaider(), LandRaiderCrusader(),
                                                                  LandRaiderRedeemer()])
        self.ride = self.opt_options_list('Options', [['Jump pack', 10, 'jp']])

    def get_count(self):
        return self.vets.get_count() + 1

    def check_rules(self):
        self.vets.update_range()
        self.transport.set_active(not self.ride.get('jp'))
        for unit in self.vets.get_units() + [self.leader.get_unit()]:
            unit.set_jp(self.ride)
        self.set_points(self.build_points(count=1, exclude=[self.ride.id]))
        self.build_description(exclude=[self.ride.id])


class LandSpeederSquadron(ListUnit):
    name = "Land Speeder Squadron"

    class LandSpeeder(ListSubUnit):
        name = "Land Speeder"
        base_points = 50
        min = 1
        max = 3

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Heavy Bolter", 0, "bolt"],
                ["Heavy Flamer", 0, "flame"],
                ["Multi-melta", 10, "melta"],
                ])
            self.up = self.opt_options_list('Upgrade', [
                ["Heavy Bolter", 10, "bolt"],
                ["Heavy Flamer", 10, "flame"],
                ["Multi-melta", 20, "melta"],
                ["Assault Cannon", 40, "assault"],
                ["Typhoon Missile Launcher", 40, "tml"],
                ], limit = 1)

        def check_rules(self):
            self.name = "Land Speeder"
            if self.up.get('tml'):
                self.name += ' Typhoon'
            elif self.up.get("bolt") or self.up.get("flame") or self.up.get("melta") or self.up.get("assault"):
                self.name += ' Tornado'
            ListSubUnit.check_rules(self)

    unit_class = LandSpeeder

class BaalPredator(Unit):
    name = "Baal Predator"
    base_points = 115
    gear = ['Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Turret', [
            ['Twin-linked assault cannon', 0],
            ['Flamestorm lascannon', 0]
        ])
        self.side = self.opt_options_list('Side sponsons', [
            ['Heavy flamers', 25, 'hfl'],
            ['Heavy bolters', 30, 'hbgun'],
        ], 1)
        self.opt = self.opt_options_list('Options', [
            ["Storm bolter", 10, 'sbgun'],
            ["Hunter-killer missile", 10, 'hkm'],
            ["Dozer blade", 5, 'dbld'],
            ["Extra armour", 15, 'exarm'],
            ["Searchlight", 1]
        ])


class AttackBikeSquad(ListUnit):
    name = "Attack Bike Squad"

    class AttackBike(ListSubUnit):
        name = "Attack bike"
        base_points = 40
        gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Space Marine bike']
        min = 1
        max = 3

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0, 'hbgun'],
                ['Multi-melta', 10, 'mmelta']
            ])

    unit_class = AttackBike


class SpaceMarineBikers(Unit):
    name = 'Space Marine Bike Squad'
    gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Space Marine bike']

    class BikerSergeant(Unit):
        name = 'Biker Sergeant'
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Space Marine bike']
        base_points = 90 - 25 * 2
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Bolt pistol', 0, 'bpist' ],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
            ])
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])

    class AttackBike(Unit):
        name = "Attack bike"
        base_points = 40
        gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Space Marine bike']
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0, 'hbgun'],
                ['Multi-melta', 10, 'mmelta']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Space Marine Biker', 2, 7, 25)
        self.flame = self.opt_count('Flamer', 0, 2, 5)
        self.melta = self.opt_count('Meltagun', 0, 2, 10)
        self.plasma = self.opt_count('Plasma gun', 0, 2, 15)

        self.sergeant = self.opt_sub_unit(self.BikerSergeant())
        self.abike = self.opt_optional_sub_unit('', [self.AttackBike()])
        self.spec = [self.flame, self.plasma, self.melta]

    def get_count(self):
        return self.bikers.get() + self.abike.get_count() + self.sergeant.get_count()

    def check_rules(self):
        norm_counts(0, 2, self.spec)
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.bikers.id], count=self.bikers.get())
        self.description.add('Bolt pistol', self.bikers.get() - reduce(lambda val, c: val + c.get(), self.spec, 0))

class ScoutBikers(Unit):
    name = 'Scout Bike Squad'
    gear = ['Scout armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Space Marine bike']

    class BikerSergeant(Unit):
        name = 'Scout Biker Sergeant'
        gear = ['Scout armour', 'Frag grenades', 'Krak grenades', 'Space Marine bike']
        base_points = 70 - 2 * 20

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Bolt pistol', 0, 'bpist' ],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
            ])
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ['Locator beacon', 25, 'lbcn']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Scout Biker', 2, 9, 20)
        self.grenades = self.opt_count('Astartes grenade launcher', 0, 3, 10)
        self.opt = self.opt_options_list('Options', [['Cluster mines', 10, 'cmines']])
        self.sergeant = self.opt_sub_unit(self.BikerSergeant())

    def check_rules(self):
        norm_counts(0, min(self.bikers.get(), 3), [self.grenades])
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.bikers.id, self.opt.id], count=self.bikers.get())
        self.description.add(self.opt.get_selected())
