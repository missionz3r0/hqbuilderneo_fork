__author__ = 'Denis Romanov'


from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed,\
    CombinedArmsDetachment, AlliedDetachment, HQSection, ElitesSection,\
    TroopsSection, FastSection, HeavySection, Fort
from builder.games.wh40k.escalation.space_marines\
    import LordsOfWar as MarinesLordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume2
from builder.games.wh40k.dataslates.cypher import Cypher
from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *

import json

class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.calgar = UnitType(self, Calgar)
        self.sicarius = UnitType(self, Sicarius)
        self.tigurius = UnitType(self, Tigurius)
        self.cassius = UnitType(self, Cassius)
        self.khan = UnitType(self, Khan)
        self.vulkan = UnitType(self, Vulkan)
        self.shrike = UnitType(self, Shrike)
        self.lysander = UnitType(self, Lysander)
        self.cantor = UnitType(self, Cantor)
        self.helbrecht = UnitType(self, Helbrecht)
        self.grimaldus = UnitType(self, Grimaldus)
        self.champion = UnitType(self, Champion)
        self.chapter_master = UnitType(self, ChapterMaster)
        self.honour_guard_squad = UnitType(self, HonourGuardSquad, active=False, slot=0)
        self.captain = UnitType(self, Captain)
        self.terminator_captain = UnitType(self, TerminatorCaptain)
        self.command_squad = UnitType(self, CommandSquad, active=False, slot=0)
        self.librarian = UnitType(self, Librarian)
        self.chaplain = UnitType(self, Chaplain)
        self.forge_master = UnitType(self, ForgeMaster)
        self.techmarine = UnitType(self, Techmarine, active=False, slot=0)
        self.servitors = UnitType(self, Servitors, active=False, slot=0)
        self.tactical_squad = UnitType(self, TacticalSquad, visible=False)
        UnitType(self, Cypher, slot=0)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.vanguard_veteran_squad = UnitType(self, VanguardVeteranSquad)
        self.sternguard_veteran_squad = UnitType(self, SternguardVeteranSquad)
        self.dreadnought = UnitType(self, Dreadnought)
        self.iron_dred = UnitType(self, IronDred)
        self.damned = UnitType(self, TheDamned)
        self.terminator_assault_squad = UnitType(self, TerminatorAssaultSquad)
        self.terminator_squad = UnitType(self, TerminatorSquad)
        self.centurion_assault = UnitType(self, CenturionAssault)
        self.centurion_devastators = UnitType(self, CenturionDevastators)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.tactical_squad = UnitType(self, TacticalSquad)
        self.scout_squad = UnitType(self, ScoutSquad)
        self.crusader_squad = UnitType(self, CrusaderSquad)
        self.bike_squad = UnitType(self, TroopsBikeSquad, active=False)

    def check_limits(self):
        min_count = len(self.units)
        max_count = sum([2 if tr.get_count() == 10 else 1 for tr in self.units])
        return self.min <= max_count and min_count <= self.max


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.assault_squad = UnitType(self, AssaultSquad)
        self.land_speeder = UnitType(self, LandSpeederSquadron)
        self.stormtalon = UnitType(self, Stormtalon)
        self.bike_squad = UnitType(self, BikeSquad)
        self.attack_bike_squad = UnitType(self, AttackBikeSquad)
        self.scout_bike_squad = UnitType(self, ScoutBikers)
        self.centurion_assault = UnitType(self, CenturionAssault)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.devastators = UnitType(self, Devastators)
        self.centurion_devastators = UnitType(self, CenturionDevastators)
        self.thunderfire = UnitType(self, Thunderfire)
        self.predator = UnitType(self, Predator)
        self.whirlwind = UnitType(self, Whirlwind)
        self.vindicator = UnitType(self, Vindicator)
        self.hunter = UnitType(self, Hunter)
        self.stalker = UnitType(self, Stalker)
        self.land_raider = UnitType(self, LandRaider)
        self.land_raider_crusader = UnitType(self, LandRaiderCrusader)
        self.land_raider_redeemer = UnitType(self, LandRaiderRedeemer)
        self.stormraven_gunship = UnitType(self, StormravenGunship)
        self.chronus = UnitType(self, Chronus, slot=0)
        self.dreadnought = UnitType(self, Dreadnought, active=False)
        self.iron_dred = UnitType(self, IronDred, active=False)


class HQ(volume2.SpaceMarineHQ, BaseHQ):
    pass


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    pass


class Elites(volume2.SpaceMarineElites, BaseElites):
    pass


class HeavySupport(volume2.SpaceMarineHeavySupport, BaseHeavySupport):
    pass


class Lords(volume2.SpaceMarineLordsOfWar, Titans, MarinesLordsOfWar):
    pass


#class SpaceMarinesV2(Wh40k):
#    army_id = 'space_marines_v2'
#    army_name = 'Space Marines'
#
#    class SupplementOptions(OneOf):
#        def __init__(self, parent):
#            super(SpaceMarinesV2.SupplementOptions, self).__init__(parent=parent, name='Codex')
#            self.base = self.variant(name=SpaceMarinesV2.army_name)
#            self.sentinels = self.variant(name='Sentinels of Terra')
#            self.raukaan = self.variant(name='Clan Raukaan')
#
#    def __init__(self, secondary=False):
#        from builder.games.wh40k.roster import Ally
#        self.hq = HQ(parent=self)
#        self.elites = Elites(parent=self)
#        self.troops = Troops(parent=self)
#        self.fast = FastAttack(parent=self)
#        self.heavy = HeavySupport(parent=self)
#        super(SpaceMarinesV2, self).__init__(
#            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
#            lords=Lords(self), fort=Fort(parent=self),
#            ally=Ally(parent=self, ally_type=Ally.SM),
#            secondary=secondary
#        )
#
#        self.codex = self.SupplementOptions(self)
#
#    def is_base_codex(self):
#        return self.codex.cur == self.codex.base
#
#    def is_sentinels(self):
#        return self.codex.cur == self.codex.sentinels
#
#    def is_raukaan(self):
#        return self.codex.cur == self.codex.raukaan
#
#    def check_rules(self):
#        super(SpaceMarinesV2, self).check_rules()
#
#        chapters_units = [self.hq.calgar, self.hq.sicarius, self.hq.tigurius, self.hq.cassius, self.hq.khan,
#                          self.hq.vulkan, self.hq.shrike, self.hq.helbrecht, self.hq.grimaldus, self.hq.champion,
#                          self.troops.crusader_squad, self.heavy.chronus]
#
#        if self.codex.cur == self.codex.base:
#            for u in chapters_units:
#                u.visible = u.active = True
#        elif self.codex.cur == self.codex.raukaan:
#            for u in chapters_units + [self.hq.lysander, self.hq.cantor]:
#                u.visible = u.active = False
#        elif self.codex.cur == self.codex.sentinels:
#            for u in chapters_units:
#                u.visible = u.active = False
#
#        # Sentinels of Terra centurions
#        for u in [self.elites.centurion_devastators, self.fast.centurion_assault]:
#            u.visible = u.active = self.codex.cur == self.codex.sentinels
#        if self.codex.cur != self.codex.sentinels:
#            if self.fast.centurion_assault.count > 0:
#                self.error("You can have Centurion Assault Squad in fast only in a Sentinels of Terra detachment.")
#            if self.elites.centurion_devastators.count > 0:
#                self.error("You can have Centurion Devastator Squad in elites only in a Sentinels of Terra detachment.")
#
#        # Sentinels of Terra sergeant Garadon
#        if self.codex.cur == self.codex.sentinels:
#            self.hq.move_units(filter(lambda u: u.has_garadon(), self.troops.tactical_squad.units))
#            self.troops.move_units(filter(lambda u: not u.has_garadon(), self.hq.tactical_squad.units))
#        else:
#            self.troops.move_units(self.hq.tactical_squad.units)
#            if any(u.has_garadon() for u in self.troops.tactical_squad.units):
#                self.error("You can have Sergeant Garadon only in a Sentinels of Terra detachment.")
#
#        # Honor Guards
#        honor_guards = self.hq.chapter_master.count + self.hq.cantor.count + self.hq.helbrecht.count + \
#            3 * self.hq.calgar.count
#        self.hq.honour_guard_squad.active = honor_guards > 0
#        if honor_guards < self.hq.honour_guard_squad.count:
#            self.error("You can't have more Honour Guard squads then Chapter Masters "
#                       "(except for Calgar, he allows 3 of them).")
#
#        # Command Squad
#        command_squads = sum(o.count for o in [
#            self.hq.sicarius, self.hq.tigurius, self.hq.cassius, self.hq.vulkan, self.hq.lysander, self.hq.shrike,
#            self.hq.khan, self.hq.grimaldus, self.hq.captain, self.hq.terminator_captain, self.hq.librarian,
#            self.hq.chaplain
#        ])
#        self.hq.command_squad.active = command_squads > 0
#        if command_squads < self.hq.command_squad.count:
#            self.error("You can't have more Command squads then Captains, Librarians and Chaplains.")
#
#        # Techmarines
#        if self.codex.cur != self.codex.raukaan:
#            techmarines = len(self.hq.units) - self.hq.honour_guard_squad.count - self.hq.command_squad.count - \
#                self.hq.techmarine.count - self.hq.servitors.count
#            self.hq.techmarine.active = techmarines > 0
#            if techmarines < self.hq.techmarine.count:
#                self.error("For each HQ choice you may include one Techmarine.")
#        else:
#            techmarines = (
#                self.hq.captain.count + self.hq.librarian.count + self.hq.chaplain.count +
#                self.hq.chapter_master.count + self.hq.terminator_captain.count) * 2 + self.hq.forge_master.count * 3
#            self.hq.techmarine.active = techmarines > 0
#            if techmarines < self.hq.techmarine.count:
#                self.error("For each HQ choice in a Clan Raukaan detachment you may include up to two Techmarines. "
#                           "For each Master of the Forge in your army, you may include up to three Techmarines.")
#
#        # Servitors
#        serv_units = self.hq.techmarine.count + self.hq.forge_master.count
#        self.hq.servitors.active = serv_units > 0
#        if serv_units < self.hq.servitors.count:
#            self.error("You can only have one unit of Servitors per Techmarine or Master of the Forge.")
#
#        # Dreadnoughts in heavy
#        if self.codex.cur != self.codex.raukaan:
#            self.heavy.dreadnought.active = self.heavy.iron_dred.active = self.hq.forge_master.count > 0
#            if self.hq.forge_master.count == 0 and self.heavy.dreadnought.count + self.heavy.iron_dred.count > 0:
#                self.error("You can only take dreadnoughts in heavy support if your army includes Master of the Forge.")
#        else:
#            self.heavy.dreadnought.active = self.heavy.iron_dred.active = True
#
#        # Sergeant Chronus
#        tanks = sum(o.count for o in [
#            self.heavy.land_raider, self.heavy.land_raider_crusader, self.heavy.land_raider_redeemer,
#            self.heavy.predator, self.heavy.whirlwind, self.heavy.vindicator, self.heavy.stalker, self.heavy.hunter
#        ])
#        self.heavy.chronus.active = tanks > 0
#        if self.heavy.chronus.count > 0 and tanks == 0:
#            self.error("There must be a tank for Chronus to command.")
#
#        # Bikers in troops
#        biker_troops = any(u.has_bike() for u in
#                           self.hq.captain.units + self.hq.chapter_master.units + self.hq.khan.units)
#        self.troops.bike_squad.active = biker_troops
#        if not biker_troops and self.troops.bike_squad.count > 0:
#            self.error("Space Maine bikers can only be taken as troops if Chapter Master, Captain or Kor\'sarro Khan "
#                       "on the bike is in HQ.")
#
#        #Chapter tactics
#        def ultramarines():
#            units_names = []
#            for u in [self.hq.calgar, self.hq.sicarius, self.hq.tigurius, self.hq.cassius, self.heavy.chronus]:
#                if u.count:
#                    units_names.append(u.name)
#            for u in self.troops.scout_squad.units:
#                if u.has_telion():
#                    units_names.append('Sergeant Telion')
#                    break
#            if units_names:
#                return 'Ultramarines', units_names
#
#        def whitescars():
#            if self.hq.khan.count:
#                return 'White Scars', [self.hq.khan.name]
#
#        def salamanders():
#            if self.hq.vulkan.count:
#                return 'Salamanders', [self.hq.vulkan.name]
#
#        def ravenguard():
#            if self.hq.shrike.count:
#                return 'Raven Guard', [self.hq.shrike.name]
#
#        def imperialfists():
#            units_names = []
#            for u in [self.hq.lysander, self.hq.cantor]:
#                if u.count:
#                    units_names.append(u.name)
#            if units_names:
#                return 'Imperial Fists', units_names
#
#        def blacktemplars():
#            units_names = []
#            for u in [self.hq.helbrecht, self.hq.grimaldus, self.hq.champion, self.troops.crusader_squad]:
#                if u.count:
#                    units_names.append(u.name)
#            if units_names:
#                return 'Black Templars', units_names
#
#        chapters_check = [
#            ultramarines,
#            whitescars,
#            salamanders,
#            ravenguard,
#            imperialfists,
#            blacktemplars
#        ]
#
#        chapters = dict(filter(None, map(lambda f: f(), chapters_check)))
#
#        units = '; '.join([ch + ': ' + ', '.join(units) for ch, units in chapters.iteritems()])
#        if self.codex.cur == self.codex.base:
#            if len(chapters) > 1:
#                self.error("You can't mix different chapter tactics in one detachment. ({0})".format(units))
#
#            if 'Black Templars' in chapters and self.hq.librarian.count:
#                self.error('Abhor the Witch! Librarians may not be included in detachments of Black Templars.')
#        elif self.codex.cur == self.codex.raukaan:
#            if len(chapters):
#                self.error("When choosing a Clan Raukaan detachment, you may only use the Iron Hands Chapter Tactics. "
#                           "({0})".format(units))
#        elif self.codex.cur == self.codex.sentinels:
#            if filter(lambda chapter: chapter != 'Imperial Fists', chapters.keys()):
#                self.error("When choosing a Sentinels of Terra detachment, you may only use the Imperial Fists Chapter "
#                           "Tactics. ({0})".format(units))


class SpaceMarinesV2Base(Wh40kBase):
    army_id = 'space_marines_v2_base'
    army_name = 'Space Marines'
    ia_enabled = True

    class SupplementOptions(OneOf):
        def __init__(self, parent):
            super(SpaceMarinesV2Base.SupplementOptions, self).\
                __init__(parent=parent, name='Codex')
            self.base = self.variant(name=SpaceMarinesV2Base.army_name)
            self.sentinels = self.variant(name='Sentinels of Terra')
            self.raukaan = self.variant(name='Clan Raukaan')

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(SpaceMarinesV2Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            lords=Lords(self), fort=Fort(parent=self)
        )

        self.codex = self.SupplementOptions(self)
        self.chapters = dict()

    def is_base_codex(self):
        return self.codex.cur == self.codex.base

    def is_sentinels(self):
        return self.codex.cur == self.codex.sentinels

    def is_raukaan(self):
        return self.codex.cur == self.codex.raukaan

    def check_rules(self):
        super(SpaceMarinesV2Base, self).check_rules()

        chapters_units = [self.hq.calgar, self.hq.sicarius, self.hq.tigurius, self.hq.cassius, self.hq.khan,
                          self.hq.vulkan, self.hq.shrike, self.hq.helbrecht, self.hq.grimaldus, self.hq.champion,
                          self.troops.crusader_squad, self.heavy.chronus]

        if self.codex.cur == self.codex.base:
            for u in chapters_units:
                u.visible = u.active = True
        elif self.codex.cur == self.codex.raukaan:
            for u in chapters_units + [self.hq.lysander, self.hq.cantor]:
                u.visible = u.active = False
        elif self.codex.cur == self.codex.sentinels:
            for u in chapters_units:
                u.visible = u.active = False

        # Sentinels of Terra centurions
        for u in [self.elites.centurion_devastators, self.fast.centurion_assault]:
            u.visible = u.active = self.codex.cur == self.codex.sentinels
        if self.codex.cur != self.codex.sentinels:
            if self.fast.centurion_assault.count > 0:
                self.error("You can have Centurion Assault Squad in fast only in a Sentinels of Terra detachment.")
            if self.elites.centurion_devastators.count > 0:
                self.error("You can have Centurion Devastator Squad in elites only in a Sentinels of Terra detachment.")

        # Sentinels of Terra sergeant Garadon
        if self.codex.cur == self.codex.sentinels:
            self.hq.move_units([u for u in self.troops.tactical_squad.units if u.has_garadon()])
            self.troops.move_units([u for u in self.hq.tactical_squad.units if not u.has_garadon()])
        else:
            self.troops.move_units(self.hq.tactical_squad.units)
            if any(u.has_garadon() for u in self.troops.tactical_squad.units):
                self.error("You can have Sergeant Garadon only in a Sentinels of Terra detachment.")

        # Honor Guards
        honor_guards = self.hq.chapter_master.count + self.hq.cantor.count + self.hq.helbrecht.count + \
            3 * self.hq.calgar.count
        self.hq.honour_guard_squad.active = honor_guards > 0
        if honor_guards < self.hq.honour_guard_squad.count:
            self.error("You can't have more Honour Guard squads then Chapter Masters "
                       "(except for Calgar, he allows 3 of them).")

        # Command Squad
        command_squads = sum(o.count for o in [
            self.hq.sicarius, self.hq.tigurius, self.hq.cassius, self.hq.vulkan, self.hq.lysander, self.hq.shrike,
            self.hq.khan, self.hq.grimaldus, self.hq.captain, self.hq.terminator_captain, self.hq.librarian,
            self.hq.chaplain
        ])
        self.hq.command_squad.active = command_squads > 0
        if command_squads < self.hq.command_squad.count:
            self.error("You can't have more Command squads then Captains, Librarians and Chaplains.")

        # Techmarines
        if self.codex.cur != self.codex.raukaan:
            techmarines = len(self.hq.units) - self.hq.honour_guard_squad.count - self.hq.command_squad.count - \
                self.hq.techmarine.count - self.hq.servitors.count
            self.hq.techmarine.active = techmarines > 0
            if techmarines < self.hq.techmarine.count:
                self.error("For each HQ choice you may include one Techmarine.")
        else:
            techmarines = (
                self.hq.captain.count + self.hq.librarian.count + self.hq.chaplain.count +
                self.hq.chapter_master.count + self.hq.terminator_captain.count) * 2 + self.hq.forge_master.count * 3
            self.hq.techmarine.active = techmarines > 0
            if techmarines < self.hq.techmarine.count:
                self.error("For each HQ choice in a Clan Raukaan detachment you may include up to two Techmarines. "
                           "For each Master of the Forge in your army, you may include up to three Techmarines.")

        # Servitors
        serv_units = self.hq.techmarine.count + self.hq.forge_master.count
        self.hq.servitors.active = serv_units > 0
        if serv_units < self.hq.servitors.count:
            self.error("You can only have one unit of Servitors per Techmarine or Master of the Forge.")

        # Dreadnoughts in heavy
        if self.codex.cur != self.codex.raukaan:
            self.heavy.dreadnought.active = self.heavy.iron_dred.active = self.hq.forge_master.count > 0
            if self.hq.forge_master.count == 0 and self.heavy.dreadnought.count + self.heavy.iron_dred.count > 0:
                self.error("You can only take dreadnoughts in heavy support if your army includes Master of the Forge.")
        else:
            self.heavy.dreadnought.active = self.heavy.iron_dred.active = True

        # Sergeant Chronus
        tanks = sum(o.count for o in [
            self.heavy.land_raider, self.heavy.land_raider_crusader, self.heavy.land_raider_redeemer,
            self.heavy.predator, self.heavy.whirlwind, self.heavy.vindicator, self.heavy.stalker, self.heavy.hunter
        ])
        self.heavy.chronus.active = tanks > 0
        if self.heavy.chronus.count > 0 and tanks == 0:
            self.error("There must be a tank for Chronus to command.")

        # Bikers in troops
        biker_troops = any(u.has_bike() for u in
                           self.hq.captain.units + self.hq.chapter_master.units + self.hq.khan.units)
        self.troops.bike_squad.active = biker_troops
        if not biker_troops and self.troops.bike_squad.count > 0:
            self.error("Space Maine bikers can only be taken as troops if Chapter Master, Captain or Kor\'sarro Khan "
                       "on the bike is in HQ.")

        #Chapter tactics
        def ultramarines():
            units_names = []
            for u in [self.hq.calgar, self.hq.sicarius, self.hq.tigurius, self.hq.cassius, self.heavy.chronus]:
                if u.count:
                    units_names.append(u.name)
            for u in self.troops.scout_squad.units:
                if u.has_telion():
                    units_names.append('Sergeant Telion')
                    break
            if units_names:
                return 'Ultramarines', units_names

        def whitescars():
            if self.hq.khan.count:
                return 'White Scars', [self.hq.khan.name]

        def salamanders():
            if self.hq.vulkan.count:
                return 'Salamanders', [self.hq.vulkan.name]

        def ravenguard():
            if self.hq.shrike.count:
                return 'Raven Guard', [self.hq.shrike.name]

        def imperialfists():
            units_names = []
            for u in [self.hq.lysander, self.hq.cantor]:
                if u.count:
                    units_names.append(u.name)
            if units_names:
                return 'Imperial Fists', units_names

        def blacktemplars():
            units_names = []
            for u in [self.hq.helbrecht, self.hq.grimaldus, self.hq.champion, self.troops.crusader_squad]:
                if u.count:
                    units_names.append(u.name)
            if units_names:
                return 'Black Templars', units_names

        chapters_check = [
            ultramarines,
            whitescars,
            salamanders,
            ravenguard,
            imperialfists,
            blacktemplars
        ]

        self.chapters = dict([_f for _f in [f() for f in chapters_check] if _f])

        units = '; '.join([ch + ': ' + ', '.join(units) for ch, units in self.chapters.items()])
        if self.codex.cur == self.codex.base:
            if len(self.chapters) > 1:
                self.error("You can't mix different chapter tactics in one detachment. ({0})".format(units))

            if 'Black Templars' in self.chapters and self.hq.librarian.count:
                self.error('Abhor the Witch! Librarians may not be included in detachments of Black Templars.')
        elif self.codex.cur == self.codex.raukaan:
            if len(self.chapters):
                self.error("When choosing a Clan Raukaan detachment, you may only use the Iron Hands Chapter Tactics. "
                           "({0})".format(units))
        elif self.codex.cur == self.codex.sentinels:
            if [chapter for chapter in list(self.chapters.keys()) if chapter != 'Imperial Fists']:
                self.error("When choosing a Sentinels of Terra detachment, you may only use the Imperial Fists Chapter "
                           "Tactics. ({0})".format(units))


class SpaceMarinesV2CAD(SpaceMarinesV2Base, CombinedArmsDetachment):
    army_id = 'space_marines_v2_cad'
    army_name = 'Space Marines (Combined arms detachment)'


class SpaceMarinesV2AD(SpaceMarinesV2Base, AlliedDetachment):
    army_id = 'space_marines_v2_ad'
    army_name = 'Space Marines (Allied detachment)'

faction = 'Space_marines'


class SpaceMarinesV2(Wh40k7ed):
    army_id = 'space_marines_v2'
    army_name = 'Space Marines'
    faction = faction
    obsolete = True

    def __init__(self):
        super(SpaceMarinesV2, self).__init__([SpaceMarinesV2CAD],
                                             [SpaceMarinesV2AD])

    def check_rules(self):
        super(SpaceMarinesV2, self).check_rules()
        # ensure there is no same-shapter allying going on
        # find space marine allied detachments
        if any([isinstance(m.sub_roster.roster, SpaceMarinesV2CAD)
               for m in self.primary.units]):
            ally_marines = [m.sub_roster.roster
                            for m in self.secondary.units
                            if isinstance(m.sub_roster.roster, SpaceMarinesV2AD)]
            ally_chapters = set(dict(sum([list(ally.chapters.items())
                                          for ally in ally_marines], [])).keys())
            own_chapter = set(self.primary.units[0].sub_roster.roster.chapters.keys())
            if len(own_chapter & ally_chapters):
                self.error("Roster cannot contain allied Space Marine detachments with chapter tactics of {}".format((own_chapter & ally_chapters).pop()))

detachments = [SpaceMarinesV2CAD, SpaceMarinesV2AD]
