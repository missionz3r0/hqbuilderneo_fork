from builder.core2 import OptionsList, OneOf, Gear

__author__ = 'Denis Romanov'


class Vehicle(OptionsList):
    def __init__(self, parent, blade=True, melta=False, shield=False):
        super(Vehicle, self).__init__(parent, 'Options')
        self.sbgun = self.Variant(self, 'Storm bolter', 5)
        if blade:
            self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
        self.exarm = self.Variant(self, 'Extra armour', 10)
        if melta:
            self.melta = self.Variant(self, 'Multi melta', 10)
        if shield:
            self.shield = self.Variant(self, 'Siege shield', 10)


class BaseWeapon(OneOf):
    def __init__(self, parent, name, **kwargs):
        self.armour = kwargs.pop('armour', None)
        self.pwr_weapon = []
        self.tda_weapon = []
        self.relic_options = []

        super(BaseWeapon, self).__init__(parent, name, **kwargs)

    def check_rules(self):
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.used and self.cur in self.relic_options:
            return self.description
        return []


class Heavy(BaseWeapon):

    class Flakk(OptionsList):
        def __init__(self, parent):
            super(Heavy.Flakk, self).__init__(parent=parent, name='', limit=None)
            self.flakkmissiles = self.variant('Flakk missiles', 10)

    def __init__(self, parent, *args, **kwargs):
        heavyflamer = kwargs.pop('heavy_flamer', False)
        super(Heavy, self).__init__(parent, *args, **kwargs)

        self.heavybolter = self.variant('Heavy bolter', 10)
        self.heavyflamer = heavyflamer and self.variant('Heavy flamer', 10)
        self.multimelta = self.variant('Multi-melta', 10)
        self.missilelauncher = self.variant('Missile launcher', 15)
        self.plasmacannon = self.variant('Plasma cannon', 15)
        self.lascannon = self.variant('Lascannon', 20)

        self.heavy = [self.heavybolter, self.heavyflamer, self.missilelauncher, self.multimelta, self.plasmacannon,
                      self.lascannon]
        self.flakk = self.Flakk(parent=parent)

    def is_heavy(self):
        return self.cur in self.heavy

    def check_rules(self):
        if self.flakk:
            self.flakk.visible = self.flakk.used = self.cur == self.missilelauncher


class Comby(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Comby, self).__init__(*args, **kwargs)

        self.stormbolter = self.variant('Storm bolter', 5)
        self.combimelta = self.variant('Combi-melta', 10)
        self.combiflamer = self.variant('Combi-flamer', 10)
        self.combigrav = self.variant('Combi-grav', 10)
        self.combiplasma = self.variant('Combi-plasma', 10)
        self.pwr_weapon += [self.stormbolter, self.combiflamer, self.combigrav, self.combimelta, self.combiplasma]


class Special(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Special, self).__init__(*args, **kwargs)

        self.flamer = self.variant('Flamer', 5)
        self.meltagun = self.variant('Meltagun', 10)
        self.gravgun = self.variant('Grav-gun', 15)
        self.plasmagun = self.variant('Plasma gun', 15)
        self.spec = [self.flamer, self.meltagun, self.gravgun, self.plasmagun]

    def is_spec(self):
        return self.cur in self.spec


class SpecialList(OptionsList):
    def __init__(self, parent, limit=1, grav=True):
        super(SpecialList, self).__init__(parent=parent, name='Special weapon', limit=limit)
        self.flame = self.variant('Flamer', 5)
        self.mgun = self.variant('Meltagun', 10)
        self.gravgun = grav and self.variant('Grav-gun', 15)
        self.pgun = self.variant('Plasmagun', 15)


class HeavyList(OptionsList):
    def __init__(self, parent, limit=1, heavy_flamer=False):
        super(HeavyList, self).__init__(parent=parent, name='Heavy weapon')
        self.weapon_limit = limit
        self.heavy = []
        self.hbgun = self.variant('Heavy bolter', 10)
        if heavy_flamer:
            self.heavyflamer = self.variant('Heavy flamer', 10)
            self.heavy += [self.heavyflamer]
        self.mulmgun = self.variant('Multi-melta', 10)
        self.mlaunch = self.variant('Missile launcher', 15)
        self.flakkmissiles = self.variant('Flakk missiles', 10, visible=False)
        self.pcannon = self.variant('Plasma cannon', 15)
        self.lcannon = self.variant('Lascannon', 20)
        self.heavy += [self.hbgun, self.mulmgun, self.mlaunch, self.pcannon, self.lcannon]

    def check_rules(self):
        super(HeavyList, self).check_rules()
        self.flakkmissiles.visible = self.flakkmissiles.used = self.mlaunch.value
        self.process_limit(self.heavy, self.weapon_limit)


class BoltPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(BoltPistol, self).__init__(*args, **kwargs)
        self.boltpistol = self.variant('Bolt pistol', 0)
        self.pwr_weapon += [self.boltpistol]


class Boltgun(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Boltgun, self).__init__(*args, **kwargs)
        self.boltgun = self.variant('Boltgun', 0)
        self.pwr_weapon += [self.boltgun]


class Chainsword(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Chainsword, self).__init__(*args, **kwargs)
        self.chainsword = self.variant('Chainsword', 0)
        self.pwr_weapon += [self.chainsword]


class RelicBlade(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(RelicBlade, self).__init__(*args, **kwargs)
        self.relic_blade = self.variant('Relic blade', 25)
        self.pwr_weapon += [self.relic_blade]


class PowerFist(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerFist, self).__init__(*args, **kwargs)
        self.fist = self.variant('Power fist', 25)
        self.pwr_weapon += [self.fist]


class PowerAxe(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerAxe, self).__init__(*args, **kwargs)
        self.power_axe = self.variant('Power axe', 15)


class PowerWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerWeapon, self).__init__(*args, **kwargs)
        self.pwr = self.variant('Power weapon', 15)
        self.pwr_weapon += [self.pwr]


class ForceWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ForceWeapon, self).__init__(*args, **kwargs)
        self.force = self.variant('Force weapon', 0)


class Crozius(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Crozius, self).__init__(*args, **kwargs)
        self.roz = self.variant('Crozius Arcanum', 0)


class LightningClaw(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(LightningClaw, self).__init__(*args, **kwargs)
        self.claw = self.variant('Lightning claw', 15)
        self.pwr_weapon += [self.claw]


class ThunderHammer(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ThunderHammer, self).__init__(*args, **kwargs)
        self.hummer = self.variant('Thunder hammer', 30)
        self.pwr_weapon += [self.hummer]


class Melee(ThunderHammer, PowerFist, PowerWeapon, LightningClaw):
    pass


class PowerPistols(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerPistols, self).__init__(*args, **kwargs)
        self.gravpistol = self.variant('Grav-pistol', 15)
        self.plasmapistol = self.variant('Plasma pistol', 15)
        self.pwr_weapon += [self.gravpistol, self.plasmapistol]


class Ranged(PowerPistols, Comby):
    pass


class TerminatorComby(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorComby, self).__init__(*args, **kwargs)
        self.tda_stormbolter = self.variant('Storm Bolter', 0)
        self.tda_combimelta = self.variant('Combi-melta', 6)
        self.tda_combiflamer = self.variant('Combi-flamer', 6)
        self.tda_combiplasma = self.variant('Combi-plasma', 6)
        self.tda_weapon += [self.tda_stormbolter, self.tda_combiflamer, self.tda_combimelta, self.tda_combiplasma]


class TerminatorRanged(TerminatorComby):
    def __init__(self, *args, **kwargs):
        super(TerminatorRanged, self).__init__(*args, **kwargs)
        self.tda_lightningclaw = self.variant('Lightning claw', 10)
        self.tda_thunderhammer = self.variant('Thunder hammer', 25)
        self.tda_weapon += [self.tda_lightningclaw, self.tda_thunderhammer]


class TerminatorMelee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorMelee, self).__init__(*args, **kwargs)
        self.tda2_powerweapon = self.variant('Power weapon', 0)
        self.tda2_lightningclaw = self.variant('Lightning claw', 5)
        self.tda2_stormshield = self.variant('Storm shield', 5)
        self.tda2_powerfist = self.variant('Power fist', 10)
        self.tda2_chainfist = self.variant('Chainfist', 15)
        self.tda2_thunderhammer = self.variant('Thunder hammer', 15)
        self.tda_weapon += [self.tda2_powerweapon, self.tda2_lightningclaw, self.tda2_stormshield,
                            self.tda2_powerfist, self.tda2_chainfist, self.tda2_thunderhammer]


class Armour(OneOf):
    power_armour_set = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    artificer_armour_set = [Gear('Artificer armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    scout_armour_set = [Gear('Scout armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent, art=0, tda=0):
        super(Armour, self).__init__(parent, 'Armour')
        self.pwr = self.variant('Power armour', 0, gear=self.power_armour_set)
        self.art = art and self.variant('Artificer armour', art, gear=self.artificer_armour_set)
        self.tda = tda and self.variant('Terminator armour', tda)

    def is_tda(self):
        return self.cur == self.tda


class Options(OptionsList):
    def __init__(self, parent, armour=None, bike=False, jump=False, storm_shield=False):
        super(Options, self).__init__(parent, 'Options')
        self.armour = armour
        self.bike_option = bike
        self.ride = []

        self.auspex = self.variant('Auspex', 5)
        self.meltabombs = self.variant('Melta bombs', 5)
        self.digitalweapons = self.variant('Digital weapons', 10)
        self.teleporthomer = self.variant('Teleport homer', 10)

        if jump:
            self.jumppack = self.variant('Jump pack', 15)
            self.ride += [self.jumppack]
        if bike:
            self.spacemarinebike = self.variant('Space Marine Bike', 20)
            self.ride += [self.spacemarinebike]
        if storm_shield:
            self.ss = self.variant('Storm Shield', 15)

    def check_rules(self):
        if self.armour:
            for opt in self.ride:
                opt.visible = opt.used = not self.armour.is_tda()
        self.process_limit(self.ride, 1)

    def has_bike(self):
        return self.bike_option and self.spacemarinebike.value


class Relic(OptionsList):
    def __init__(self, parent, armour=None, librarian=False):
        super(Relic, self).__init__(parent, 'Relic')
        self.armour = armour
        self.the_armour = self.variant('The Armour Indomitus', 60)

        #Raukaan
        self.axe = self.variant('The Axe of Medusa', 25)
        self.ironstone = self.variant('The Ironstone', 30)
        self.bane = self.variant('Betrayer\'s Bane', 25)
        self.chain = self.variant('The Gorgon\'s Chain', 45)
        self.helm = self.variant('The Tempered Helm', 35)
        self.raukaan = [self.axe, self.ironstone, self.bane, self.chain, self.helm]

        #Sentinels
        self.spartean = self.variant('The Spartean', 5)
        self.theeye = self.variant('The Eye of Hypnoth', 15)
        self.sentinels = [self.spartean, self.theeye]
        if librarian:
            self.bones = self.variant('The Bones of Osrak', 25)  # Librarian only
            self.sentinels.append(self.bones)

    def check_rules(self):
        super(Relic, self).check_rules()
        if self.armour and self.parent.roster.is_base_codex():
            self.visible = self.used = not self.armour.is_tda()
        self.the_armour.visible = self.the_armour.used = self.parent.roster.is_base_codex()
        for o in self.raukaan:
            o.visible = o.used = self.parent.roster.is_raukaan()
        for o in self.sentinels:
            o.visible = o.used = self.parent.roster.is_sentinels()

    def get_unique(self):
        if self.used:
            return self.description
        return []


class RelicWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        self.librarian = kwargs.pop('librarian', False)
        self.chaplain = kwargs.pop('chaplain', False)
        super(RelicWeapon, self).__init__(*args, **kwargs)
        self.theprimarchswrath = self.variant('The Primarch\'s Wrath', 20)
        self.teethofterra = self.variant('Teeth of Terra', 35)
        self.theshieldeternal = self.variant('The Shield Eternal', 50)
        self.theburningblade = self.variant('The Burning Blade', 55)
        self.relic_weapon = [self.theprimarchswrath, self.teethofterra, self.theshieldeternal, self.theburningblade]
        self.relic_options = self.relic_weapon[:]

        #Raukaan
        if self.librarian:
            self.stave = self.variant('The Mindforge Stave', 15)  # Librarian only
            self.relic_options.append(self.stave)

        #Sentinels
        if self.chaplain:
            self.angel = self.variant('The Angel of Sacrifice', 10)  # Chaplain only
            self.relic_options.append(self.angel)

    def check_rules(self):
        super(RelicWeapon, self).check_rules()
        if self.librarian:
            self.stave.visible = self.stave.used = self.parent.roster.is_raukaan()
        if self.chaplain:
            self.angel.visible = self.angel.used = self.parent.roster.is_sentinels()
        for o in self.relic_weapon:
            o.visible = o.used = self.parent.roster.is_base_codex()
