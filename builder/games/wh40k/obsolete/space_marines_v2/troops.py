from builder.core2 import *
from .transport import Transport
from .armory import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle

__author__ = 'Denis Romanov'


class TacticalSquad(Unit):
    type_name = 'Tactical Squad'
    type_id = 'tactical_squad_v1'
    model_points = 14
    model_gear = Armour.power_armour_set

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent, weapon1, weapon2):
                self.weapon1 = weapon1
                self.weapon2 = weapon2
                super(TacticalSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.garadon = self.variant('Sergeant Garadon', 75)
                self.meltabombs = self.variant('Melta bombs', 5)
                self.teleport = self.variant('Teleport homer', 10)
                self.veteran = self.variant('Veteran', 10, gear=[])

            def check_rules(self):
                super(TacticalSquad.Sergeant.Options, self).check_rules()
                for o in [self.veteran, self.meltabombs, self.teleport, self.weapon1, self.weapon2]:
                    o.visible = o.used = not self.garadon.value

        class Weapon1(Ranged, Melee, Chainsword, Boltgun):
            pass

        class Weapon2(Ranged, Melee, Chainsword, BoltPistol):
            pass

        def __init__(self, parent):
            super(TacticalSquad.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=70 - 4 * TacticalSquad.model_points,
                gear=TacticalSquad.model_gear
            )
            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self, self.wep1, self.wep2)

        def build_description(self):
            if self.opt.garadon.value and self.opt.garadon.visible:
                return UnitDescription(self.opt.garadon.title, self.points, options=Armour.power_armour_set + [
                    Gear('Power fist'),
                    Gear('The Spartean'),
                ])
            desc = super(TacticalSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            return desc

    def __init__(self, parent):
        super(TacticalSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.special = SpecialList(parent=self)
        self.heavy = HeavyList(parent=self)
        self.transport = Transport(parent=self)

    def check_rules(self):
        self.sergeant.unit.opt.garadon.visible = self.roster.is_sentinels() or self.has_garadon()
        self.special.visible = self.special.used = (not self.heavy.any or self.get_count() == 10)
        self.heavy.visible = self.heavy.used = (not self.special.any or self.get_count() == 10)

    def has_garadon(self):
        return self.sergeant.unit.opt.garadon.value

    def get_unique(self):
        if self.has_garadon():
            return self.sergeant.unit.opt.garadon.title

    def get_unique_gear(self):
        return self.transport.get_unique_gear() + (['The Spartean'] if self.has_garadon() else [])

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=TacticalSquad.model_gear + [Gear('Bolt pistol')],
            points=TacticalSquad.model_points
        )
        count = self.marines.cur
        for o in [self.heavy, self.special]:
            if o.used and o.any:
                desc.add(marine.clone().add_points(o.points).add(o.description))
                count -= 1
        desc.add(marine.add(Gear('Boltgun')).set_count(count))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1


class ScoutSquad(Unit):
    type_name = 'Scout Squad'
    type_id = 'scout_squad_v1'

    model_points = 11
    model_gear = Armour.scout_armour_set

    class LandSpeederStorm(SpaceMarinesBaseVehicle):
        type_name = 'Land Speeder Storm'
        model_points = 45

        def __init__(self, parent):
            super(ScoutSquad.LandSpeederStorm, self).__init__(
                parent=parent, points=self.model_points, transport=True,
                gear=[Gear('Cerberus launcher'), Gear('Jamming beacon')]
            )
            self.weapon = self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(ScoutSquad.LandSpeederStorm.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.heavyflamer = self.variant('Heavy flamer', 0)
                self.multimelta = self.variant('Multi-melta', 10)
                self.assaultcannon = self.variant('Assault cannon', 20)

    class Sergeant(Unit):
        class Weapon1(Boltgun):
            def __init__(self, parent):
                super(ScoutSquad.Sergeant.Weapon1, self).__init__(parent=parent, name='Weapon')
                self.sniperrifle = self.variant('Sniper rifle', 1)
                self.shotgun = self.variant('Space Marines shotgun', 0)
                self.combatknife = self.variant('Close combat weapon', 0)

        class Weapon2(Ranged, Melee, Chainsword, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent, weapon1, weapon2):
                super(ScoutSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.weapon1 = weapon1
                self.weapon2 = weapon2
                self.telion = self.variant('Sergeant Telion', 50, gear=[])
                self.meltabombs = self.variant('Melta bombs', 5)
                self.teleport = self.variant('Teleport homer', 10)
                self.veteran = self.variant('Veteran', 10, gear=[])

            def check_rules(self):
                super(ScoutSquad.Sergeant.Options, self).check_rules()
                for o in [self.veteran, self.meltabombs, self.teleport, self.weapon1, self.weapon2]:
                    o.visible = o.used = not self.telion.value

        def __init__(self, parent):
            super(ScoutSquad.Sergeant, self).__init__(
                name='Scout Sergeant',
                parent=parent, points=55 - 4 * ScoutSquad.model_points,
                gear=TacticalSquad.model_gear
            )
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self, self.wep1, self.wep2)

        def build_description(self):
            if self.opt.telion.value and self.opt.telion.visible:
                return UnitDescription('Sergeant Telion', self.points, options=[
                    Gear('Scout armour'),
                    Gear('Bolt pistol'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Camo cloak'),
                    Gear('Quietus'),
                ])
            desc = super(ScoutSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Scout Sergeant'
            return desc

    class Heavy(OptionsList):
        def __init__(self, parent):
            super(ScoutSquad.Heavy, self).__init__(parent=parent, name='Heavy weapon')

            self.hbgun = self.variant('Heavy bolter', 8)
            self.hf = self.variant('Hellfire shells', 5)
            self.mlaunch = self.variant('Missile launcher', 15)
            self.flakkmissiles = self.variant('Flakk missiles', 10, visible=False)
            self.heavy = [self.hbgun, self.mlaunch]

        def check_rules(self):
            super(ScoutSquad.Heavy, self).check_rules()
            self.flakkmissiles.visible = self.flakkmissiles.used = self.mlaunch.value
            self.hf.visible = self.hf.used = self.hbgun.value
            self.process_limit(self.heavy, 1)

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutSquad.Options, self).__init__(parent=parent, name='Options')
            self.cloak = self.variant('Camo cloaks', 2, per_model=True)
            self.speeder = self.variant(ScoutSquad.LandSpeederStorm.type_name, ScoutSquad.LandSpeederStorm.model_points)
            self.speeder.used = False
            self.speeder_unit = SubUnit(parent, ScoutSquad.LandSpeederStorm(None))

        def check_rules(self):
            super(ScoutSquad.Options, self).check_rules()
            self.speeder_unit.used = self.speeder_unit.visible = self.speeder.value

    def __init__(self, parent):
        super(ScoutSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Scout', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.sniper = Count(parent=self, name='Sniper rifle', min_limit=0, max_limit=4, points=1, per_model=True)
        self.shotgun = Count(parent=self, name='Space Marines shotgun', min_limit=0, max_limit=4, points=0)
        self.knife = Count(parent=self, name='Close combat weapon', min_limit=0, max_limit=4, points=0)

        self.heavy = self.Heavy(parent=self)
        self.opt = self.Options(parent=self)

    def get_count(self):
        return self.marines.cur + 1

    def check_rules(self):
        super(ScoutSquad, self).check_rules()
        self.sergeant.unit.opt.telion.visible = self.roster.is_base_codex() or self.has_telion()
        max_wep = self.marines.cur
        if self.heavy.any:
            max_wep -= 1
        Count.norm_counts(0, max_wep, [self.sniper, self.shotgun, self.knife])

    def build_points(self):
        return super(ScoutSquad, self).build_points() + (self.get_count() - 1) * self.opt.points

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.get_count(),
                               self.opt.description + self.sergeant.description)
        scout = UnitDescription('Scout', self.model_points, options=self.model_gear + [Gear('Bolt pistol')])
        count = self.marines.cur
        if self.heavy.any:
            desc.add(scout.clone().add(self.heavy.description).add_points(self.heavy.points))
            count -= 1
        for o in [self.knife, self.shotgun, self.sniper]:
            if o.cur:
                desc.add(scout.clone().add(Gear(o.name)).add_points(o.option_points).set_count(o.cur))
                count -= o.cur
        if count:
            desc.add(scout.add(Gear('Boltgun')).set_count(count))
        if self.opt.speeder_unit.used:
            desc.add(self.opt.speeder_unit.description)
        return desc

    def get_unique_gear(self):
        return self.opt.speeder_unit.unit.get_unique_gear() if self.opt.speeder_unit.used else []

    def get_unique(self):
        if self.has_telion():
            return 'Sergeant Telion'

    def has_telion(self):
        return self.sergeant.unit.opt.telion.value


class CrusaderSquad(Unit):
    type_name = 'Crusader Squad'
    type_id = 'crusader_squad_v1'

    initiate_points = 14
    neophyte_points = 10

    class Initiate(ListSubUnit):

        class CrusaderWeapon(Chainsword, Boltgun):
            pass

        class InitiateHeavy(HeavyList):
            def __init__(self, parent, weapon):
                self.weapon = weapon
                super(CrusaderSquad.Initiate.InitiateHeavy, self).__init__(parent)
                self.fist = self.variant('Power fist', 25)
                self.pwr = self.variant('Power weapon', 15)
                self.pure_heavy = self.heavy[:]
                self.heavy += [self.fist, self.pwr]

        def __init__(self, parent):
            super(CrusaderSquad.Initiate, self).__init__(
                parent, name='Initiate', points=CrusaderSquad.initiate_points,
                gear=Armour.power_armour_set + [Gear('Bolt pistol')])
            self.weapon = self.CrusaderWeapon(self, 'Weapon')
            self.heavy = self.InitiateHeavy(self, self.weapon)
            self.special = SpecialList(self)

        def check_rules(self):
            super(CrusaderSquad.Initiate, self).check_rules()
            self.heavy.visible = self.heavy.used = not self.special.any
            self.special.visible = self.special.used = not self.heavy.any
            hide_weapon = self.special.used and self.special.any or \
                self.heavy.used and any(o.value for o in self.heavy.pure_heavy)
            self.weapon.visible = self.weapon.used = not hide_weapon

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.heavy.any

        @ListSubUnit.count_gear
        def count_special(self):
            return self.special.any

    class Neophyte(ListSubUnit):

        class NeophyteWeapon(Boltgun):
            def __init__(self, parent):
                super(CrusaderSquad.Neophyte.NeophyteWeapon, self).__init__(parent=parent, name='Weapon')
                self.shotgun = self.variant('Space Marines shotgun', 0)
                self.combatknife = self.variant('Close combat weapon', 0)

        def __init__(self, parent):
            super(CrusaderSquad.Neophyte, self).__init__(
                parent, name='Neophyte', points=CrusaderSquad.neophyte_points,
                gear=Armour.scout_armour_set + [Gear('Bolt pistol')])
            self.weapon = self.NeophyteWeapon(self)

    class Sergeant(Unit):
        type_name = 'Sword Brother'

        class SergeantWeapon1(Ranged, Melee, Chainsword, Boltgun):
            pass

        class SergeantWeapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(CrusaderSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(CrusaderSquad.Sergeant, self).__init__(
                parent, points=70 - CrusaderSquad.initiate_points * 4 + 10,
                gear=Armour.power_armour_set
            )
            #self.weapon1 = CrusaderSquad.Initiate.CrusaderWeapon(self, 'Weapon')
            self.weapon1 = self.SergeantWeapon1(self, 'Weapon')
            self.weapon2 = self.SergeantWeapon2(self, '')
            self.opt = self.Options(self)

    class Options(OptionsList):

        def __init__(self, parent):
            super(CrusaderSquad.Options, self).__init__(parent=parent, name='Options')
            self.sergeant = self.variant(CrusaderSquad.Sergeant.type_name, gear=[])
            self.neophytes = self.variant('Neophytes', gear=[])
            self.sergeant_unit = SubUnit(parent, CrusaderSquad.Sergeant(None))
            self.neophytes_unit = UnitList(parent, CrusaderSquad.Neophyte, 1, 10)

        def check_rules(self):
            super(CrusaderSquad.Options, self).check_rules()
            self.sergeant_unit.visible = self.sergeant_unit.used = self.sergeant.value
            self.neophytes_unit.visible = self.neophytes_unit.used = self.neophytes.value

    def __init__(self, parent):
        super(CrusaderSquad, self).__init__(parent)
        self.marines = UnitList(self, self.Initiate, 5, 10)
        self.opt = self.Options(self)
        self.transport = Transport(self, crusader=True)

    def check_rules(self):
        super(CrusaderSquad, self).check_rules()
        self.marines.update_range(5 - self.opt.sergeant.value, 10 - self.opt.sergeant.value)
        if self.marines.count < self.opt.neophytes_unit.count * self.opt.neophytes.value:
            self.error('You may not purchase more Neophytes than you have Initiates in the squad.')
        spec = sum(u.count_special() for u in self.marines.units)
        if spec > 1:
            self.error('Initiates can take only 1 special weapon (taken: {0})'.format(spec))
        heavy = sum(u.count_heavy() for u in self.marines.units)
        if heavy > 1:
            self.error('Initiates can take only 1 heavy or power weapon (taken: {0})'.format(heavy))

    def get_count(self):
        return self.marines.count + self.opt.sergeant_unit.used + \
            self.opt.neophytes_unit.used * self.opt.neophytes_unit.count

    def get_unique_gear(self):
        return self.transport.get_unique_gear()
