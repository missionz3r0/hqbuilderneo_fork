__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, StaticUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.space_marines.armory import *
from builder.games.wh40k.obsolete.space_marines.heavy import LandRaiderCrusader
from builder.games.wh40k.obsolete.space_marines.transport import DropPod, Rhino, Razorback
from functools import reduce


class LandSpeederStorm(Unit):
    name = "Land Speeder Storm"
    base_points = 45
    gear = ['Cerberus launcher', 'Jamming beacon']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ["Heavy bolter", 0, 'hbgun'],
            ["Heavy flamer", 0, 'hflame'],
            ["Multi-melta", 10, 'mm'],
            ["Assault cannon", 20, 'asscan'],
        ])


class TacticalSquad(Unit):
    name = 'Tactical Squad'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

    class Sergeant(Unit):
        base_points = 70 - 14 * 4
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bolt'],
                ['Chainsword', 0, 'csw'],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bolt'],
                ['Chainsword', 0, 'csw'],
            ] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ["Teleport homer", 10, 'th'],
            ])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine', 4, 9, 14)
        self.wep = self.opt_one_of('Weapon', [['Boltgun', 0, 'bgun']] + special + heavy)
        self.spec = self.opt_one_of('Weapon', [['Boltgun', 0, 'bgun']] + special)
        self.hvy = self.opt_one_of('', [['Boltgun', 0, 'bgun']] + heavy)
        self.flakk = self.opt_options_list('', flakk)

        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        full = (self.get_count() == 10)
        self.wep.set_visible(not full)
        self.spec.set_visible(full)
        self.hvy.set_visible(full)
        self.flakk.set_visible(self.hvy.get_cur() == 'mlaunch' or self.wep.get_cur() == 'mlaunch')

        self.set_points(self.build_points(count=1))
        self.build_description(count=1, options=[self.sergeant], gear=[])
        desc = ModelDescriptor('Space Marine', points=14, gear=self.gear)
        base_count = self.marines.get()
        if full:
            if self.hvy.get_cur() != 'bgun':
                self.description.add(desc.clone().add_gear_opt(self.hvy).add_gear_opt(self.flakk).build(1))
                base_count -= 1
            if self.spec.get_cur() != 'bgun':
                self.description.add(desc.clone().add_gear_opt(self.spec).build(1))
                base_count -= 1
        else:
            if self.wep.get_cur() != 'bgun':
                self.description.add(desc.clone().add_gear_opt(self.wep).add_gear_opt(self.flakk).build(1))
                base_count -= 1
        self.description.add(desc.add_gear('Boltgun').build(base_count))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.marines.get() + 1


class ScoutSquad(Unit):
    name = 'Scout Squad'

    class Sergeant(Unit):
        base_points = 55 - 11 * 4
        name = 'Scout Sergeant'
        gear = ['Scout armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bolt'],
                ['Sniper rifle', 1],
                ['Shotgun', 0],
                ['Close combat weapon', 0],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bolt'],
                ['Chainsword', 0, 'csw'],
            ] + melee + ranged)

            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ["Teleport homer", 10, 'th'],
            ])

            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    class Telion(StaticUnit):
        name = 'Sergeant Telion'
        base_points = 50 + 11
        gear = ['Scout armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Camo cloak', 'Quietus']

    class Scout(ListSubUnit):
        base_points = 11
        name = 'Scout'
        gear = ['Scout armour', 'Frag grenades', 'Krak grenades', 'Bolt pistol']

        def __init__(self):
            ListSubUnit.__init__(self)

            self.wep = self.opt_one_of('Weapon', [
                ['Boltgun', 0],
                ['Sniper rifle', 1],
                ['Shotgun', 0],
                ['Close combat weapon', 0],
                ['Heavy Bolter', 8, 'hb'],
                ['Missile Launcher', 15, 'ml'],
            ])
            self.hf = self.opt_options_list('', [['Hellfire shells', 5]])
            self.flakk = self.opt_options_list('', [['Flakk missiles', 10]])

        def has_special(self):
            return (1 if self.wep.get_cur() in ['hb', 'ml'] else 0) * self.count.get()

        def check_rules(self):
            self.flakk.set_visible(self.wep.get_cur() == 'ml')
            self.hf.set_visible(self.wep.get_cur() == 'hb')
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.scouts = self.opt_units_list(self.Scout.name, self.Scout, 4, 9)
        self.telion = self.opt_optional_sub_unit('', [self.Telion()])
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.opt = self.opt_options_list('Options', [
            ["Camo cloaks", 2, 'cmcl']
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [LandSpeederStorm()])

    def get_count(self):
        return self.scouts.get_count() + 1

    def check_rules(self):
        self.scouts.update_range()
        self.sergeant.set_active(not self.telion.get(self.Telion.name))
        self.points.set(self.build_points(exclude=[self.opt.id], count=1) +
                        (self.scouts.get_count() + self.sergeant.get_count()) * self.opt.points())
        spec_limit = 1
        spec_total = reduce(lambda val, u: u.has_special() + val, self.scouts.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Only one Scout can take a heavy weapon (taken: {})'.format(spec_total))
        self.build_description(count=1)

    def has_telion(self):
        return self.telion.get_count() > 0


class CrusaderSquad(Unit):
    name = 'Crusader Squad'

    class Initiate(ListSubUnit):
        name = 'Initiate'
        base_points = 14
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Bolt pistol']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.wep = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bg'],
                ['Chainsword', 0, 'ccw'],
            ] + heavy)
            self.flakk = self.opt_options_list('', flakk)
            self.pwr = self.opt_options_list('', [
                ['Power weapon', 15, 'pw'],
                ['Power fist', 25, 'pf'],
            ], limit=1)
            self.spec = self.opt_options_list('', special, limit=1)

        def check_rules(self):
            self.flakk.set_visible(self.wep.get_cur() == 'mlaunch')
            self.spec.set_visible(not self.pwr.is_any_selected() and self.wep.get_cur() not in heavy_id)
            self.pwr.set_visible(not self.spec.is_any_selected() and self.wep.get_cur() not in heavy_id)
            self.wep.set_active_options(heavy_id, not self.spec.is_any_selected() and not self.pwr.is_any_selected())
            ListSubUnit.check_rules(self)

        def has_heavy(self):
            return self.get_count() if self.pwr.is_any_selected() or self.wep.get_cur() in heavy_id else 0

        def has_spec(self):
            return self.get_count() if self.spec.is_any_selected() else 0

    class Sergeant(Unit):
        base_points = 70 - 14 * 4 + 10
        name = 'Sword Brother'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bolt'],
                ['Chainsword', 0, 'csw'],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bolt'],
            ] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
            ])

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_units_list('', CrusaderSquad.Initiate, 5, 10)
        self.brother = self.opt_optional_sub_unit('', [CrusaderSquad.Sergeant()])
        self.neo = self.opt_count('Neophyte', 0, 5, 10)
        self.ccw = self.opt_count('Close combat weapon', 0, 0, 0)
        self.shotgun = self.opt_count('Shotguns', 0, 0, 0)
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback(),
                                                                  LandRaiderCrusader()])

    def check_rules(self):
        spec = sum((u.has_spec() for u in self.marines.get_units()))
        if spec > 1:
            self.error('Initiates can take only 1 special weapon (taken: {0})'.format(spec))
        heavy = sum((u.has_heavy() for u in self.marines.get_units()))
        if heavy > 1:
            self.error('Initiates can take only 1 heavy or power weapon (taken: {0})'.format(heavy))

        leader = self.brother.get_count()
        self.marines.update_range(5 - leader, 10 - leader)
        self.neo.update_range(0, self.marines.get_count())
        norm_counts(0, self.neo.get(), [self.ccw, self.shotgun])

        self.set_points(self.build_points(count=1, exclude=[self.transport.id]))
        self.build_description(options=[self.brother, self.marines])

        desc = ModelDescriptor('Neophyte', gear=['Scout armor', 'Frag grenades', 'Krak grenades', 'Bolt pistol'],
                               points=10)
        self.description.add(desc.clone().add_gear('Shotgun').build(self.shotgun.get()))
        self.description.add(desc.clone().add_gear('Close combat weapon').build(self.ccw.get()))
        self.description.add(desc.add_gear('Boltgun').build(self.neo.get() - self.shotgun.get() - self.ccw.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.marines.get_count() + self.neo.get() + self.brother.get_count()
