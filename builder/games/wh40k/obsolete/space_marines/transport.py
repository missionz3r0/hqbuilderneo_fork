__author__ = 'dromanow'

from builder.core.unit import Unit
from builder.games.wh40k.obsolete.space_marines.armory import vehicle


class DropPod(Unit):
    name = "Drop pod"
    base_points = 35

    def __init__(self, base_points=35):
        Unit.__init__(self)
        self.base_points = base_points
        self.wep = self.opt_one_of('Weapon', [
            ["Storm bolter", 0, 'sbgun'],
            ["Deathwind missile launcher", 15, 'dwind']
        ])
        self.opt = self.opt_options_list('Options', [
            ["Locator beacon", 10, 'lbcon']
        ])


class Rhino(Unit):
    name = "Rhino"
    base_points = 35
    gear = ['Storm bolter', 'Smoke launchers', 'Searchlight']

    def __init__(self, base_points=35):
        Unit.__init__(self)
        self.base_points = base_points
        self.opt = self.opt_options_list('Options', vehicle)


class Razorback(Unit):
    name = "Razorback"
    base_points = 55
    gear = ['Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ["Twin-linked heavy bolter", 0, 'tlhbgun'],
            ["Twin-linked heavy flamer", 0, 'tlhflame'],
            ["Twin-linked assault cannon", 20, 'tlasscan'],
            ["Twin-linked lascannon", 20, 'tllcannon'],
            ["Lascannon and twin-linked plasma gun", 20, 'lascplasgun']
        ])
        self.opt = self.opt_options_list('Options', vehicle)
