__author__ = 'Ivan Truskov'

from builder.core.options import norm_counts
from builder.core.unit import StaticUnit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.space_marines.armory import *
from builder.games.wh40k.obsolete.space_marines.transport import *


class Chronus(StaticUnit):
    name = 'Sergeant Chronus'
    base_points = 50
    gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Servo-arm']


class LandRaider(Unit):
    name = "Land Raider"
    base_points = 250
    gear = ['Twin-linked heavy bolter', 'Twin-linked lascannon', 'Twin-linked lascannon', 'Smoke launchers',
            'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Multi-melta", 10, 'mmelta'],
        ] + lr_vehicle)


class LandRaiderCrusader(Unit):
    name = "Land Raider Crusader"
    base_points = 250
    gear = ['Twin-linked assault cannon', 'Hurricane bolter', 'Hurricane bolter', 'Smoke launchers', 'Searchlight',
            'Frag Assault Launcher']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Multi-melta", 10, 'mmelta'],
        ] + lr_vehicle)


class LandRaiderRedeemer(Unit):
    name = "Land Raider Redeemer"
    base_points = 240
    gear = ['Twin-linked assault cannon', 'Flamestorm cannon', 'Flamestorm cannon', 'Smoke launchers', 'Searchlight',
            'Frag Assault Launcher']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Multi-melta", 10, 'mmelta'],
        ] + lr_vehicle)


class Predator(Unit):
    name = "Predator"
    base_points = 75
    gear = ['Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Turret', [
            ['Autocannon', 0, 'acan'],
            ['Twin-linked lascannon', 25, 'tllcan']
        ])
        self.side = self.opt_options_list('Side sponsons', [
            ['Sponsons with heavy bolters', 25, 'hbgun'],
            ['Sponsons with lascannons', 40, 'lcan']
        ], limit=1)
        self.opt = self.opt_options_list('Options', vehicle)


class Whirlwind(Unit):
    name = "Whirlwind"
    base_points = 65
    gear = ['Whirlwind multiple missile launcher', 'Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicle)


class Vindicator(Unit):
    name = "Vindicator"
    base_points = 125
    gear = ['Demolisher cannon', 'Storm bolter', 'Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Siege shield", 10, 'sshld'],
        ] + vehicle)


class Hunter(Unit):
    name = "Hunter"
    base_points = 70
    gear = ['Skyspear missile launcher', 'Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicle)


class Stalker(Unit):
    name = "Stalker"
    base_points = 75
    gear = ['Icarus stormcannon array', 'Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicle)


class Thunderfire(Unit):
    name = "Thunderfire cannon"
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Techmarine gunner',
                                             gear=['Artificer armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades',
                                                   'Servo-harness']).build(1))
        self.description.add(self.transport.get_selected())


class StormravenGunship(Unit):
    name = 'Stormraven Gunship'
    base_points = 200
    gear = ['Stormstrike missile' for _ in range(4)] + ['Ceramite plating']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Twin-linked heavy bolter', 0],
            ['Twin-linked Multi-melta', 0],
            ['Typhoon missile launcher', 25],
        ])
        self.wep2 = self.opt_one_of('', [
            ['Twin-linked Assault cannon', 0, 'asscan'],
            ['Twin-linked Plasma cannon', 0, 'pcan'],
            ['Twin-linked Lascannon', 0, 'tllcan']
        ])
        self.side = self.opt_options_list('Side sponsons', [
            ['Hurricane bolters', 30],
        ])

        self.opt = self.opt_options_list('Options', [
            ["Searchlight", 1],
            ["Extra armour", 5],
            ["Locator beacon", 15],
        ])


class Devastators(Unit):
    name = 'Devastator Squad'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

    class Sergeant(Unit):
        base_points = 70 - 14 * 4
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Signum']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bolt'],
                ['Chainsword', 0, 'csw'],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bolt'],
            ] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
            ])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.marines = self.opt_count('Space Marine', 4, 9, 14)
        self.hb = self.opt_count('Heavy bolter', 0, 4, 10)
        self.mm = self.opt_count('Multi-melta', 0, 4, 10)
        self.ml = self.opt_count('Missile launcher', 0, 4, 15)
        self.flakk = self.opt_count('Flakk missiles', 0, 0, 10)
        self.pc = self.opt_count('Plasma cannon', 0, 4, 15)
        self.lc = self.opt_count('Lascannon', 0, 4, 20)
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        norm_counts(0, 4, [self.hb, self.mm, self.ml, self.pc, self.lc])
        self.flakk.update_range(0, self.ml.get())
        self.points.set(self.build_points(count=1))
        self.build_description(options=[self.sergeant], gear=[], count=1)
        desc = ModelDescriptor('Space marine', points=14, gear=self.gear)
        self.description.add(desc.clone().add_gear('Heavy bolter', points=10).build(self.hb.get()))
        self.description.add(desc.clone().add_gear('Multi-melta', points=10).build(self.mm.get()))
        if self.ml.get() > 0:
            ml_full = self.ml.get()
            ml_desc = desc.clone().add_gear('Missile launcher', points=15)
            self.description.add(ml_desc.clone().add_gear('Flakk missiles', points=10).build(self.flakk.get()))
            ml_full -= self.flakk.get()
            self.description.add(ml_desc.build(ml_full))
        self.description.add(desc.clone().add_gear('Plasma cannon', points=15).build(self.pc.get()))
        self.description.add(desc.clone().add_gear('Lascannon', points=10).build(self.lc.get()))
        self.description.add(desc.clone().add_gear('Boltgun').build(self.marines.get() - self.lc.get() - self.hb.get() -
                                                                    self.mm.get() - self.ml.get() - self.pc.get()))

    def get_count(self):
        return self.marines.get() + 1


class CenturionDevastators(Unit):
    name = 'Centurion Devastator Squad'

    class Centurion(ListSubUnit):
        name = 'Centurion'
        base_points = 60

        def __init__(self):
            ListSubUnit.__init__(self, max_models=5)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Hurricane bolter', 0, 'bolt'],
                ['Missile launcher', 10, 'ml'],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Twin-linked heavy bolter', 0, 'bolt'],
                ['Twin-linked lascannon', 20, 'las'],
                ['Grav-cannon and grav-amp', 20, 'grav'],
            ])

    class Sergeant(Unit):
        base_points = 70
        name = 'Centurion Sergeant'

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Hurricane bolter', 0, 'bolt'],
                ['Missile launcher', 10, 'ml'],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Twin-linked heavy bolter', 0, 'bolt'],
                ['Twin-linked lascannon', 20, 'las'],
                ['Grav-cannon and grav-amp', 20, 'grav'],
            ])
            self.opt = self.opt_options_list('Options', [
                ['Omniscope', 10, 'scope'],
            ])

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.marines = self.opt_units_list(self.Centurion.name, self.Centurion, 2, 5)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(), LandRaiderCrusader(),
                                                                  LandRaiderRedeemer()])

    def check_rules(self):
        self.marines.update_range()
        self.points.set(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.marines.get_count() + 1
