__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.corsairs.hq import Venom, Falcon
from builder.core.model_descriptor import ModelDescriptor


class CorsairsUnit(Unit):
    name = 'Eldar Corsair Squad'

    class Corsair(ListSubUnit):
        name = 'Corsair'
        base_points = 9
        start_gear = ['Plasma grenades', 'Close Combat Weapon']

        def set_pack(self, val):
            self.gear = self.start_gear + (['Corsair Jet Pack'] if val else [])
            ListSubUnit.check_rules(self)

        def has_spec1(self):
            return self.wep.get_cur() in ['flame', 'fgun']

        def has_spec2(self):
            return self.wep.get_cur() in ['scan', 'eml']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.wep = self.opt_one_of('Weapon', [
                ['Lasblaster', 0, 'lbl'],
                ['Shuriken catapult', 0, 'scat'],
                ['Shuriken pistol', 0, 'spist'],
                ['Flamer', 6, 'flame'],
                ['Fusion Gun', 15, 'fgun'],
                ['Shuriken Cannon', 10, 'scan'],
                ['Eldar Missile Launcher', 15, 'eml']
            ])

        def check_rules(self):
            pass

    class Felarch(Unit):
        name = 'Felarch'
        base_points = 19
        start_gear = ['Plasma grenades']
        has_jp = False

        def set_pack(self, val):
            self.gear = self.start_gear + (['Corsair Jet Pack'] if val else [])
            Unit.check_rules(self)

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Ranged Weapon', [
                ['Lasblaster', 0, 'lbl'],
                ['Shuriken catapult', 0, 'scat'],
                ['Shuriken pistol', 0, 'spist'],
                ['Fusion Pistol', 10, 'fpist']
            ])
            self.wep2 = self.opt_one_of('Close Combat Weapon', [
                ['Close Combat Weapon', 0, 'ccw'],
                ['Power Sword', 10, 'psw']
            ])
            self.opt = self.opt_options_list('Options', [
                ['Haywire grenades', 5, 'hwg']
            ])

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.wars = self.opt_units_list(self.Corsair.name, self.Corsair, 5, 10)
        self.ldr = self.opt_optional_sub_unit(self.Felarch.name, self.Felarch())
        self.opt = self.opt_options_list('Options', [
            ['Corsair Jet Packs', 25, 'cjp']
        ])
        self.boat = self.opt_optional_sub_unit('Transport', [Venom(), Falcon()])

    def get_count(self):
        return self.wars.get_count() + self.ldr.get_count()

    def check_rules(self):
        self.wars.update_range(5 - self.ldr.get_count(), 10 - self.ldr.get_count())
        self.boat.set_active_options([Venom.name], self.get_count() <= 5)
        self.boat.set_active_options([Falcon.name], self.get_count() <= 6)
        self.boat.set_active(not self.opt.get('cjp'))
        for war in self.wars.get_units():
            war.set_pack(self.opt.get('cjp'))
        if self.ldr.get_unit():
            self.ldr.get_unit().get_unit().set_pack(self.opt.get('cjp'))
        specmax = int(self.get_count() / 2)
        spec1 = sum([1 if war.has_spec1 else 0 for war in self.wars.get_units()])
        spec2 = sum([1 if war.has_spec2 else 0 for war in self.wars.get_units()])
        if spec1 > specmax:
            self.error('No more then 1 Corsair per 5 may carry flamer or fusion gun.')
        if spec2 > specmax:
            self.error("No more then 1 Corsair per 5 may carry shuriken cannon or missile launcher")
        self.set_points(self.build_points(count=1, base_points=0))
        self.build_description(count=1, exclude=[self.opt.id])


class Wasps(Unit):
    name = 'Corsair Wasp Assault Walker Squadron'

    class Wasp(ListSubUnit):
        name = 'Wasp'
        start_points = 55
        weplist = [
            ['Shuriken cannon', 5, 'scan'],
            ['Scatter Laser', 15, 'scat'],
            ['Eldar Missile Launcher', 20, 'eml'],
            ['Starcannon', 25, 'scan'],
            ['Bright Lance', 30, 'blnc']
        ]
        has_stones = False

        def set_stones(self, val):
            self.has_stones = val

        def __init__(self):
            ListSubUnit.__init__(self, max_models=3)
            self.wep1 = self.opt_one_of('Weapon', self.weplist)
            self.wep2 = self.opt_one_of('', self.weplist)

        def check_rules(self):
            self.gear = ['Spirit Stones'] if self.has_stones else []
            self.base_points = self.start_points + (5 if self.has_stones else 0)
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.waw = self.opt_units_list(self.Wasp.name, self.Wasp, 1, 3)
        self.opt = self.opt_options_list('Options', [['Spirit Stones', 5, 'ss']])

    def get_count(self):
        return self.waw.get_count()

    def check_rules(self):
        self.waw.update_range()
        for wasp in self.waw.get_units():
            wasp.set_stones(self.opt.get('ss'))
        self.set_points(self.build_points(count=1, base_points=0, options=[self.waw]))
        self.build_description(count=1, options=[self.waw])


class Jetbikers(Unit):
    name = 'Corsair Jetbike Squadron'
    base_points = 75

    class Felarch(Unit):
        name = 'Felarch Jetbiker'
        base_points = 35
        start_gear = ['Eldar Jetbike', 'Twin-linked Shuriken Catapults']
        weplist = [
            ['Power Sword', 10, 'psw'],
            ['Fusion Pistol', 10, 'fpist']
        ]

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Shuriken Pistol', 0, 'spist']] + self.weplist)
            self.wep2 = self.opt_one_of('', [['Close Combat Weapon', 0, 'ccw']] + self.weplist)

    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Corsair Jetbiker', 3, 10, 25)
        self.cannons = self.opt_count('Shuriken Cannons', 0, 1, 10)
        self.ldr = self.opt_optional_sub_unit(self.Felarch.name, self.Felarch())

    def get_count(self):
        return self.bikers.get() + self.ldr.get_count()

    def check_rules(self):
        self.bikers.update_range(3 - self.ldr.get_count(), 10 - self.ldr.get_count())
        self.cannons.update_range(0, int(self.get_count() / 3))
        self.set_points(self.build_points(base_points=0, count=1))
        self.build_description(options=[self.ldr])
        d = ModelDescriptor('Corsair Jetbiker', gear=['Eldar Jetbike', 'Close Combat Weapon', 'Shuriken Pistol'],
                            points=25)
        self.description.add(d.clone().add_gear('Shuriken Cannon', 10).build(self.cannons.get()))
        self.description.add(d.add_gear('Twin-linked Shuriken Catapults').build(self.bikers.get() - self.cannons.get()))
