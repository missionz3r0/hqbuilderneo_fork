__author__ = 'Ivan Truskov'

from builder.games.wh40k.legacy_roster import *
from builder.games.wh40k.obsolete.corsairs.elites import *
from builder.games.wh40k.obsolete.corsairs.fast import *
from builder.games.wh40k.obsolete.corsairs.heavy import *
from builder.games.wh40k.obsolete.corsairs.hq import *
from builder.games.wh40k.obsolete.corsairs.troops import *
from builder.games.wh40k.obsolete.dark_eldar_v1 import *
from builder.games.wh40k.obsolete.eldar_v2 import *

class Corsairs(LegacyWh40k):
    army_id = 'f81a669e1c824e8db8ab56b0da5f4724'
    army_name = 'Eldar Corsairs'
    imperial_armour = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[Prince, Dreamer, dict(unit=Retinue, active=False)],
            elites=[Scorpions, Dragons, Wraithguard, Banshees, Rangers, Spectres, Hawks, Spears,
                    Spiders, VyperSquadron, KabaliteWarriors, VoidStorm, Harlequins],
            troops=[CorsairsUnit, Wasps, Jetbikers],
            fast=[Hornets, Nightwing, NightSpinner],
            heavy=[Firestorm, Phoenix, WHunter],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.ELD)

        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_units([Retinue])
            return self.hq.min <= count <= self.hq.max

        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        command = self.hq.count_units([Prince])
        self.hq.set_active_types([Retinue], command > 0)
        if command < self.hq.count_units([Retinue]):
            self.error("You can't have more {0} then {1}".format(Retinue.name, Prince.name))

        def count_jp(units):
            return sum((1 for u in units if u.get_jp()))
        if count_jp(self.hq.get_units(Retinue)) != count_jp(self.hq.get_units(Prince)):
            self.error("Each {0} must have Jet Packs only if {1} have it.".format(Retinue.name, Prince.name))

        tcnt = self.troops.count_unit(CorsairsUnit)
        if tcnt < 1:
            self.error("Corsair army must include at least one squad of Corsairs")
        if self.troops.count_unit(Wasps) > tcnt:
            self.error("Corsair army cannot have more Wasp squadrons then Corsair squads")
        ecnt = self.elites.count_units([Scorpions, Dragons, Wraithguard, Banshees, Rangers, Spectres, Hawks, Spears,
                                        Spiders, VyperSquadron])
        if ecnt > 1:
            self.error("Corsair army cannot include more then 1 unit from Codex: Eldar")
        kknt = self.elites.count_unit(KabaliteWarriors)
        if kknt > 1:
            self.error("Corsair army cannot include more then 1 unit of Kabalite Warriors")
        if ecnt + kknt > tcnt:
            self.error("Corsair army must include more squads of Corsairs then units from Codex: Eldar and "
                       "Codex: Dark Eldar")
