__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, ListUnit


class Hornets(ListUnit):
    name = 'Corsair Hornet Squadron'

    class Hornet(ListSubUnit):
        name = 'Hornet'
        gear = ['Star Engines']
        base_points = 65
        weplist = [
            ['Shuriken cannon', 0, 'scan'],
            ['Scatter Laser', 10, 'scat'],
            ['Eldar Missile Launcher', 15, 'eml'],
            ['Starcannon', 20, 'scan'],
            ['Bright Lance', 25, 'blnc'],
            ['Pulse laser', 30, 'plas']
        ]

        def __init__(self):
            ListSubUnit.__init__(self, max_models=3)
            self.wep1 = self.opt_one_of('Weapon', self.weplist)
            self.wep2 = self.opt_one_of('', self.weplist)
            self.opt = self.opt_options_list('Options', [
                ['Holo-field', 35, 'hf'],
                ['Vector Engines', 20, 'veng'],
                ['Spirit Stones', 10, 'ss']
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Hornet, 1, 3)


class Nightwing(Unit):
    name = 'Nightwing Interceptor'
    base_points = 145
    static = True
    gear = ['Shuriken cannon' for _ in range(2)] + ['Bright lance' for _ in range(2)]


class NightSpinner(Unit):
    name = 'Corsair Night Spinner'
    base_points = 130
    gear = ['Twin-linked Doomweaver']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Twin-linked Shuriken Catapult', 0, 'tlshc'],
            ['Shuriken Cannon', 10, 'shcan']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Vectored Engines', 20, 'veng'],
            ['Star Engines', 15, 'seng'],
            ['Holo-fields', 35, 'hf'],
            ['Spirit Stones', 10, 'ss']
        ])
