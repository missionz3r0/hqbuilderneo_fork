__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit, ListUnit, ListSubUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor


class MegaDread(Unit):
    name = 'Mega Dread'
    base_points = 175
    gear = ["Armour Plates"]

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Big shoota', 10], 
            ["Grot riggers", 5], 
            ["Mega-charga", 15]
        ])
        self.wep1 = self.opt_one_of('Weapon', [
            ['Killkannon', 0, 'kk'],
            ['Supa-scorcha', 0], 
            ['Rippa klaw', 0]
        ])
        self.wep2 = self.opt_one_of('', [
            ['Rippa klaw', 0], 
            ['Killkannon', 35, 'kk']
        ])
        self.wep3 = self.opt_one_of('', [
            ["Big Shoota", 0], 
            ["Scorcha", 0], 
            ["Rokkit Launcha", 5], 
            ["Kustom mega-blasta", 10]
        ])
        self.wep4 = self.opt_one_of('', [
            ["Big Shoota", 0], 
            ["Scorcha", 0], 
            ["Rokkit Launcha", 5], 
            ["Kustom mega-blasta", 10]
        ])

    def check_rules(self):
        self.points.set(self.build_points())
        if self.wep1.get_cur() == 'kk' and self.wep2.get_cur() == 'kk':
            self.build_description(exclude=[self.wep1.id, self.wep2.id], add=['Twin-linked killkannon'])
        else:
            self.build_description()


class Lootas(Unit):
    name = 'Loota Mob'

    class Mek(ListSubUnit):
        name = 'Mek'
        base_points = 15
        gear = ["Mek's Tools"]

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Kustom mega-blasta", 0, 'kmb'], 
                ["Slugga and Choppa", 0, 'sc'], 
                ["Big Shoota", 0, 'bs'], 
                ["Rokkit launcha", 5, 'rl'], 
            ])
            self.opt = self.opt_options_list('', [['Grot Oiler', 5]])

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Lootas", 5, 15, 15)
        self.meks = self.opt_options_list("", [['Meks', 15, 'add']])
        self.meks_unit = self.opt_units_list('Meks Unit', self.Mek, 0, 3)
        self.ride = self.opt_optional_sub_unit('Transport', [LootedWagon()])

    def check_rules(self):
        self.ride.set_active(self.get_count() <= 12)
        have_meks = self.meks.get('add')
        self.meks_unit.set_visible(have_meks)
        self.meks_unit.set_active(have_meks)
        meks = 0
        if have_meks:
            self.meks_unit.update_range(1, 3)
            meks = self.meks_unit.get_count()

        norm_counts(5 - meks, 15 - meks, [self.count])
        self.set_points(self.build_points(count=1, exclude=[self.meks.id]))
        self.build_description(options=[self.meks_unit], count=1)
        self.description.add(ModelDescriptor('Lootas', points=15, count=self.count.get(), gear=['Deffgun'])
            .build())

    def get_count(self):
        return self.count.get() + self.meks_unit.get_count()


class LootedWagon(Unit):
    name = "Looted Wagon"
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.big = self.opt_options_list('Weapon', [
            ["Boomgun", 70], 
            ["Skorcha", 15], 
        ], limit=1)

        self.bs = self.opt_count("Big shoota", 0, 2, 5)
        self.rl = self.opt_count("Rokkit launcha", 0, 2, 10)

        self.opt = self.opt_options_list('Options', [

            ["'ard case", 10], 
            ["Red paint job", 5], 
            ["Grot riggers", 5], 
            ["Stikkbomb chukka", 5], 
            ["Armor plates", 10], 
            ["Boarding plank", 5], 
            ["Wreckin' ball", 10], 
            ["Reinforces ram", 5, 'ram'], 
            ["Grabbin' klaw", 5], 
        ])

    def check_rules(self):
        norm_counts(0, 2, [self.bs, self.rl])

        Unit.check_rules(self)


class Lifta(Unit):
    name = "Lifta Wagon"
    base_points = 225
    gear = ['Lifta-Droppa']

    def __init__(self):
        Unit.__init__(self)

        self.bs = self.opt_count("Big shoota", 0, 2, 5)
        self.rl = self.opt_count("Rokkit launcha", 0, 2, 10)

        self.opt = self.opt_options_list('Options', [
            ["Deff rolla", 20, 'rolla'], 
            ["Reinforced ram", 5, 'ram'], 
            ["Red paint job", 5], 
            ["Grot riggers", 5], 
            ["Stikkbomb chukka", 5], 
            ["Armor plates", 10], 
            ["Boarding plank", 5], 
            ["Wreckin' ball", 10], 
            ["Grabbin' klaw", 5], 
        ])

    def check_rules(self):
        self.opt.set_active_options(['ram'], not self.opt.get('rolla'))
        self.opt.set_active_options(['rolla'], not self.opt.get('ram'))
        norm_counts(0, 2, [self.bs, self.rl])
        Unit.check_rules(self)


class BigTrakk(Unit):
    name = "Big Trakk"
    base_points = 50
    gear = ['Armour Plates']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ["Big Shoota", 0], 
            ["Scorcha", 5], 
            ["Rokkit Launcha", 10]
        ])
        self.wep2 = self.opt_one_of('', [
            ["Big Shoota", 0], 
            ["Scorcha", 5], 
            ["Rokkit Launcha", 10]
        ])
        self.big = self.opt_options_list('Weapon', [
            ["Kannon", 10, 'kan'], 
            ["Lobba", 10, 'lb'], 
            ["Zzap gun", 10, 'zz'], 
            ['Supa-scorcha', 10], 
            ['Big lobba', 20], 
            ['Killkannon', 45], 
            ['Flakka-gunz', 40], 
            ['Big-zzappa', 30], 
            ['Supa-kannon', 70]
        ], limit=1)
        self.bs = self.opt_count("Pintle-mounted Big shoota", 0, 2, 5)
        self.sc = self.opt_count("Pintle-mounted Scorcha", 0, 2, 10)
        self.rl = self.opt_count("Pintle-mounted Rokkit launcha", 0, 2, 15)

        self.opt1 = self.opt_options_list('Options', [
            ["Boarding plank", 5], 
            ['\'Ard case', 10], 
            ["Stikkbomb chukka", 5], 
            ["Red paint job", 5], 
            ["Grot riggers", 5]
        ])
        self.opt2 = self.opt_options_list('', [
            ["Deff rolla", 10, 'rolla'], 
            ["Reinforced ram", 5, 'ram'], 
            ["Wreckin' ball", 10], 
            ["Grabbin' klaw", 10]
        ], limit=1)

    def check_rules(self):

        norm_counts(0, 2, [self.bs, self.rl, self.sc])

        Unit.check_rules(self)
