__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, StaticUnit
from builder.games.wh40k.obsolete.grey_knights.elites import Rhino, Razorback


class Terminators(Unit):
    name = 'Grey Knight Terminator squad'

    class Justicar(Unit):
        name = 'Terminator Justicar'
        base_points = 40
        gear = ['Terminator armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            Unit.__init__(self)
            self.rngup = self.opt_options_list('Weapon', [['Master-crafted storm bolter', 5, 'wup']])
            self.wep = self.opt_one_of('', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 0, 'nfhlb'],
                ['Nemesis Daemon Hammer', 0, 'nham'],
                ['Pair of Nemesis falcions', 5, 'nfalc'],
                ['Nemesis warding stave', 20, 'stave']
            ])
            self.mlwup = self.opt_options_list('', [['Make master-crafted', 5, 'wup']])
            self.ban = self.opt_options_list('Options', [['Brotherhood banner', 25, 'ban']])

        def has_stave(self):
            return 1 if self.wep.get_cur() == 'stave' else 0

        def has_banner(self):
            return 1 if self.ban.get('ban') else 0

        def check_rules(self):
            self.wep.set_active(not self.has_banner())
            self.mlwup.set_active(not self.has_banner())
            self.set_points(self.build_points())
            self.build_description(exclude=[self.wep.id, self.mlwup.id, self.rngup.id])
            if self.rngup.get('wup'):
                self.description.add('Master-crafted storm bolter')
            else:
                self.description.add('Storm bolter')
            if not self.has_banner():
                if self.mlwup.get('wup'):
                    self.description.add('Master-crafted ' + self.wep.get_selected())
                else:
                    self.description.add(self.wep.get_selected())

    class Thawn(StaticUnit):
        name = 'Justicar Thawn'
        gear = ['Terminator armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']
        base_points = 75

    class Terminator(ListSubUnit):
        name = 'Terminator'
        base_points = 40
        gear = ['Terminator armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.rng = self.opt_one_of('Weapon', [
                ['Storm bolter', 0, 'sbgun'],
                ['Incinerator', 5, 'inc'],
                ['Psilencer', 15, 'psil'],
                ['Psycannon', 25, 'pcan']
            ])
            self.wep = self.opt_one_of('', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 0, 'nfhlb'],
                ['Nemesis Daemon Hammer', 0, 'nham'],
                ['Pair of Nemesis falcions', 5, 'nfalc'],
                ['Nemesis warding stave', 20, 'stave']
            ])
            self.ban = self.opt_options_list('Options', [['Brotherhood banner', 25, 'ban']])

        def has_stave(self):
            return self.get_count() if self.wep.get_cur() == 'stave' else 0

        def has_hvy(self):
            return self.get_count() if self.rng.is_active() and self.rng.get_cur() != 'sbgun' else 0

        def has_banner(self):
            return self.get_count() if self.ban.get('ban') else 0

        def check_rules(self):
            self.wep.set_active(not self.has_banner())
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Justicar())
        self.jThawn = self.opt_optional_sub_unit(self.Thawn.name, [self.Thawn()])
        self.squad = self.opt_units_list(self.Terminator.name, self.Terminator, 4, 9)
        self.opt = self.opt_options_list('Options', [['Psybolt ammunition', 20, 'pba']])

    def check_rules(self):
        self.squad.update_range()
        self.leader.set_active(not self.jThawn.get(self.Thawn.name))
        stave_cnt = sum((cur.has_stave() for cur in self.squad.get_units())) + self.leader.get_unit().has_stave()
        if stave_cnt > 1:
            self.error('Only one Warding Stave may be present in unit. Taken: {0}'.format(stave_cnt))

        hvy_lim = int(self.get_count() / 5)
        hvy_cnt = sum((cur.has_hvy() for cur in self.squad.get_units()))
        if hvy_cnt > hvy_lim:
            self.error("In squad of {0} models only {1} special weapons are allowed. "
                       "Taken: {2}".format(self.get_count(), hvy_lim, hvy_cnt))

        ban_cnt = sum((cur.has_banner() for cur in self.squad.get_units())) + self.leader.get_unit().has_banner()
        if ban_cnt > 1:
            self.error('Only one Brotherhood Banner may be present in unit. Taken: {0}'.format(ban_cnt))

        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.squad.get_count() + 1

    def get_unique(self):
        if self.jThawn.get_count():
            return self.Thawn.name


class Strikes(Unit):
    name = 'Grey Knight Strike squad'

    class Justicar(Unit):
        name = "Justicar"
        base_points = 20
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            Unit.__init__(self)
            self.urwep = self.opt_options_list('Weapon', [
                ['Master-crafted Storm bolter', 5, 'uwep']
            ])
            self.wep = self.opt_one_of('', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 5, 'nfhlb'],
                ['Nemesis Daemon Hammer', 10, 'nham'],
                ['Pair of Nemesis falcions', 10, 'nfalc'],
                ['Nemesis warding stave', 25, 'stave']
            ])
            self.uwep = self.opt_options_list('', [
                ['Make master-crafted', 5, 'uwep']
            ])

        def has_stave(self):
            return 1 if self.wep.get_cur() == 'stave' else 0

        def check_rules(self):
            self.set_points(self.build_points())
            self.build_description(exclude=[self.urwep.id, self.uwep.id, self.wep.id])
            if self.urwep.get('uwep'):
                self.description.add('Master-crafted Storm bolter')
            else:
                self.description.add('Storm bolter')
            if self.uwep.get('uwep'):
                self.description.add('Master-crafted ' + self.wep.get_selected())
            else:
                self.description.add(self.wep.get_selected())

    class Knight(ListSubUnit):
        name = 'Grey Knight'
        base_points = 20
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.rng = self.opt_one_of('Ranged weapon', [
                ['Storm bolter', 0, 'sbgun'],
                ['Psilencer', 0, 'psil'],
                ['Psycannon', 10, 'pcan'],
                ['Incinerator', 20, 'inc']
            ])
            self.wep = self.opt_one_of('Close combar weapon', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 5, 'nfhlb'],
                ['Nemesis Daemon Hammer', 10, 'nham'],
                ['Pair of Nemesis falcions', 10, 'nfalc'],
                ['Nemesis warding stave', 25, 'stave']
            ])

        def has_stave(self):
            return self.wep.get_cur() == 'stave'

        def has_hvy(self):
            return self.get_count() if self.rng.get_cur() != 'sbgun' else 0

        def check_rules(self):
            self.wep.set_active(not self.has_hvy())
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Justicar())
        self.squad = self.opt_units_list(self.Knight.name, self.Knight, 4, 9)
        self.opt = self.opt_options_list('Options', [['Psybolt ammunition', 20, 'pba']])
        self.ride = self.opt_optional_sub_unit('Transport', [Rhino(), Razorback()])

    def check_rules(self):
        self.squad.update_range()
        fullunit = self.squad.get_units() + [self.leader.get_unit()]
        stave_cnt = sum((cur.has_stave() for cur in fullunit))
        if stave_cnt > 1:
            self.error('Only one Warding Stave may be present in unit. Taken: {0}'.format(stave_cnt))
        hvy_lim = int(self.get_count() / 5)
        hvy_cnt = sum((cur.has_hvy() for cur in self.squad.get_units()))
        if hvy_cnt > hvy_lim:
            self.error("In squad of {0} models only {1} special weapons are allowed. "
                       "Taken: {2}".format(self.get_count(), hvy_lim, hvy_cnt))
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return 1 + self.squad.get_count()
