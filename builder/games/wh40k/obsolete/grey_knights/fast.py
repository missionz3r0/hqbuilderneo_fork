__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit


class StormravenGunship(Unit):
    name = 'Stormraven Gunship'
    base_points = 205
    gear = ['Mindstrike missiles' for _ in range(4)] + ['Ceramite plating']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Twin-linked heavy bolter', 0],
            ['Twin-linked Multi-melta', 0],
            ['Typhoon missile launcher', 25],
        ])
        self.wep2 = self.opt_one_of('', [
            ['Twin-linked Plasma cannon', 0, 'pcan'],
            ['Twin-linked Assault cannon', 0, 'asscan'],
            ['Twin-linked Lascannon', 0, 'tllcan']
        ])
        self.side = self.opt_options_list('Side sponsons', [
            ['Hurricane bolters', 30],
        ], 1)

        self.opt = self.opt_options_list('Options', [
            ["Searchlight", 1],
            ["Locator beacon", 10],
            ["Extra armour", 5],
            ['Psybolt ammunition', 20, 'pba'],
            ['Warp stabilisation field', 5, 'wsf'],
            ['Truesilver armour', 10, 'tsa']
        ])


class Interceptors(Unit):
    name = 'Grey Knight Interceptor squad'

    class Justicar(Unit):
        name = "Justicar"
        base_points = 26
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades', 'Personal teleporter']

        def __init__(self):
            Unit.__init__(self)
            self.urwep = self.opt_options_list('Weapon', [
                ['Master-crafted Storm bolter', 5, 'uwep']
            ])
            self.wep = self.opt_one_of('', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 5, 'nfhlb'],
                ['Nemesis Daemon Hammer', 10, 'nham'],
                ['Pair of Nemesis falcions', 10, 'nfalc'],
                ['Nemesis warding stave', 25, 'stave']
            ])
            self.uwep = self.opt_options_list('', [
                ['Make master-crafted', 5, 'uwep']
            ])

        def has_stave(self):
            return 1 if self.wep.get_cur() == 'stave' else 0

        def check_rules(self):
            self.set_points(self.build_points())
            self.build_description(exclude=[self.urwep.id, self.uwep.id, self.wep.id])
            if self.urwep.get('uwep'):
                self.description.add('Master-crafted Storm bolter')
            else:
                self.description.add('Storm bolter')
            if self.uwep.get('uwep'):
                self.description.add('Master-crafted ' + self.wep.get_selected())
            else:
                self.description.add(self.wep.get_selected())

    class Knight(ListSubUnit):
        name = 'Grey Knight'
        base_points = 26
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades', 'Personal teleporter']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.rng = self.opt_one_of('Ranged weapon', [
                ['Storm bolter', 0, 'sbgun'],
                ['Psilencer', 0, 'psil'],
                ['Psycannon', 10, 'pcan'],
                ['Incinerator', 20, 'inc']
            ])
            self.wep = self.opt_one_of('Close combar weapon', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 5, 'nfhlb'],
                ['Nemesis Daemon Hammer', 10, 'nham'],
                ['Pair of Nemesis falcions', 10, 'nfalc'],
                ['Nemesis warding stave', 25, 'stave']
            ])

        def has_stave(self):
            return self.wep.get_cur() == 'stave'

        def has_hvy(self):
            return self.get_count() if self.rng.get_cur() != 'sbgun' else 0

        def check_rules(self):
            self.wep.set_active(not self.has_hvy())
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Justicar())
        self.squad = self.opt_units_list(self.Knight.name, self.Knight, 4, 9)
        self.opt = self.opt_options_list('Options', [['Psybolt ammunition', 20, 'pba']])

    def check_rules(self):
        self.squad.update_range()
        fullunit = self.squad.get_units() + [self.leader.get_unit()]
        stave_cnt = sum((cur.has_stave() for cur in fullunit))
        if stave_cnt > 1:
            self.error('Only one Warding Stave may be present in unit. Taken: {0}'.format(stave_cnt))
        hvy_lim = int(self.get_count() / 5)
        hvy_cnt = sum((cur.has_hvy() for cur in self.squad.get_units()))
        if hvy_cnt > hvy_lim:
            self.error("In squad of {0} models only {1} special weapons are allowed. "
                       "Taken: {2}".format(self.get_count(), hvy_lim, hvy_cnt))
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return 1 + self.squad.get_count()
