__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, ListSubUnit, ListUnit
from builder.core.options import norm_points


class SoulGrinder(Unit):
    name = 'Soul Grinder'
    base_points = 135
    gear = ['Iron claw', 'Harvester cannon']

    def __init__(self):
        Unit.__init__(self)
        self.god = self.opt_one_of('Allegiance', [
            ['Daemon of Khorne', 0, 'kh'],
            ['Daemon of Tzeentch', 5, 'tz'],
            ['Daemon of Nurgle', 15, 'ng'],
            ['Daemon of Slaanesh', 15, 'sl']
        ])

        self.opt = self.opt_options_list('Options', [
            ['Baleful torrent', 20, 'btor'],
            ['Warp gaze', 25, 'wg'],
            ['Phlegm bombardment', 30, 'ph']
        ], 1)
        self.wep = self.opt_options_list('Weapon', [['Warpsword', 25, 'wsw']])


class SkullCannon(Unit):
    name = "Skull Cannon of Khorne"
    base_points = 125
    static = True


class BurningChariot(Unit):
    name = "Burning Chariot of Tzeentch"
    base_points = 100

    class Exalted(Unit):
        name = "Exalted Flamer"
        gear = ['Blue Fire of Tzeentch', 'Pink Fire of Tzeentch']

        def __init__(self):
            Unit.__init__(self)
            self.les = self.opt_count("Lesser Rewards", 0, 2, 10)
            self.grt = self.opt_count("Greater Rewards", 0, 1, 20)

        def check_rules(self):
            norm_points(20, [self.les, self.grt])
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.rider = self.opt_sub_unit(self.Exalted())
        self.opt = self.opt_options_list('Options', [['Blue Horror Crew', 10, 'crew']])


class SeekerCavalcade(ListUnit):
    name = 'Seeker Cavalcade'

    class Chariot(ListSubUnit):
        max = 3
        name = 'Seeker Chariot of Slaanesh'
        base_points = 40

        class Exalted(Unit):
            name = "Exalted Alluress"

            def __init__(self):
                Unit.__init__(self)
                self.les = self.opt_count("Lesser Rewards", 0, 2, 10)
                self.grt = self.opt_count("Greater Rewards", 0, 1, 20)

            def check_rules(self):
                norm_points(20, [self.les, self.grt])
                Unit.check_rules(self)

        def __init__(self):
            ListSubUnit.__init__(self)
            self.rider = self.opt_sub_unit(self.Exalted())
            self.opt = self.opt_options_list('Upgrade', [['to Exalted Chariot', 35, 'up']])

        def check_rules(self):
            fullname = "Exalted Seeker Chariot of Slaanesh" if self.opt.get('up') else 'Seeker Chariot of Slaanesh'
            self.set_points(self.build_points(exclude=[self.count.id]))
            self.build_description(name=fullname, exclude=[self.opt.id, self.count.id])

    unit_class = Chariot
