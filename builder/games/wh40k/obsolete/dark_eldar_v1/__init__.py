__author__ = 'Ivan Truskov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *
from functools import reduce


class DarkEldarV1(LegacyWh40k):
    army_id = '9e637aa4c8864d588ded549b5aaa2cb2'
    army_name = 'Dark Eldar'
    obsolete = True
    arcane = [['Archangel of Pain','apain'],["Animus vitae",'avit'],['Casket of Flensing','coflns'],['Crucible of malediction','crucm'],['Dark gate','dgate'],['Orb of despair','dorb'],['Shattershard','sshard']]
    def __init__(self, secondary=False):
        LegacyWh40k.__init__(self,
            hq = [Vect, Malys, Drazhar, Lelith, Rakarth, Sliscus, Decapitator, Sathonyx, Archon,{'unit': Court, 'active': False},Succubus,Haemunculus],
            elites = [Incubi, Grotesques, Wracks, Mandrakes, Harlequins, Trueborns, HekatrixBloodbrides],
            troops = [KabaliteWarriors, Wyches, {'unit': Wracks, 'active': False}, {'unit': Hellions, 'active': False}],
            fast = [Hellions, Scourges, Reavers, Beastmasters],
            heavy = [Ravager, Talos, Chronos, Razorwing, Voidraven],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.DE)
            )
        def check_hq_limit():
            #every Ancient Haemunculus is in separate slot
            ah_count = reduce(lambda val,h: val + (1 if h.upgrade.get('up') else 0),self.hq.get_units(Haemunculus),0)
            #but one slot can be occupied by 3 Haemunculi
            h_count = int((2 + self.hq.count_unit(Haemunculus))/3)
            count = len(self.hq.get_units()) - self.hq.count_unit(Court) - self.hq.count_unit(Haemunculus) + max(h_count,ah_count)
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        courts = self.hq.count_units([Archon,Vect, Malys])
        self.hq.set_active_types([Court],courts > 0)
        if courts < self.hq.count_units([Court]):
            self.error("You can't have more Courts of Archon then Archons (including named ones).")

        troop_hellions = self.hq.count_unit(Sathonyx) > 0
        self.troops.set_active_types([Hellions], troop_hellions)
        if not troop_hellions and self.troops.count_unit(Hellions) > 0:
            self.error("Hellions can be taken as troops only if the army includes Baron Sathonyx")

        troop_wracks = self.hq.count_units([Rakarth, Haemunculus]) > 0
        self.troops.set_active_types([Wracks],troop_wracks)
        if not troop_wracks and self.troops.count_units([Wracks]) > 0:
            self.error("Wracks can be taken as troops only if the army includes Haemunculus (including Rakarth)")
        for u in self.elites.get_units(Grotesques):
            u.set_hulk(self.hq.count_units([Rakarth]) > 0)
        blist = self.hq.get_units(Haemunculus) + self.hq.get_units(Rakarth)
        for pair in self.arcane:
            acount = reduce(lambda val, u: val + u.check_arcane(pair[1]),blist,0)
            if acount > 1:
                self.error(pair[0] + " can be only taken once per army")
