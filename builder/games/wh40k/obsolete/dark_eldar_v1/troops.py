__author__ = 'Ivan Truskov'
from builder.core.unit import Unit
from builder.core.options import norm_counts
from functools import reduce

class Raider(Unit):
    name = 'Raider'
    base_points = 60
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon',[
            ['Dark lance', 0, 'dlance'],
            ['Disintegrator cannon', 0, 'dcan']
        ])
        self.opt = self.opt_options_list('Options',[
            ['Shock prow', 5, 'sprow'],
            ['Torment grenade launchers', 5, 'tgl'],
            ['Enchanted aethersails', 5, 'eaes'],
            ['Retrofire jets', 5, 'rfjet'],
            ['Chain-snares', 5, 'chsn'],
            ['Grisly trophies', 5, 'gristr'],
            ['Envenomed blades', 5, 'envblld'],
            ['Splinter racks', 10, 'spr'],
            ['Night shields', 10, 'nsh'],
            ['Flickerfield', 10, 'flkf']
        ])

class Venom(Unit):
    name = 'Venom'
    base_points = 55
    gear = ['Splinter cannon','Flickerfield']
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon',[
            ['Twin-linked splinter rifle', 0, 'tlsplr'],
            ['Splinter cannon', 10, 'splcan']
        ])
        self.opt = self.opt_options_list('Options',[
            ['Retrofire jets', 5, 'rfjet'],
            ['Chain-snares', 5, 'chsn'],
            ['Grisly trophies', 5, 'gristr'],
            ['Envenomed blades', 5, 'envblld'],
            ['Night shields', 10, 'nsh']
        ])


class KabaliteWarriors(Unit):
    name = 'Kabalite Warriors'
    gear = ['Kabalite armour']

    class Sybarite(Unit):
        name = 'Sybarite'
        base_points = 9 + 10

        def __init__(self):
            Unit.__init__(self)
            self.armr = self.opt_one_of('Armour',[
                ['Kabalite armour',0,'kabarmr'],
                ['Ghostplate armour',10,'ghostarmr']
            ])
            self.opt = self.opt_options_list('Weapon',[
                ["Splinter rifle", 0, 'splgun']
            ])
            self.opt.set('splgun', True)
            self.wep1 = self.opt_one_of('',[
                ['Splinter pistol', 0, 'splpist' ],
                ['Blast pistol', 15, 'blpist' ]
            ])
            self.wep2 = self.opt_one_of('', [
                ['Close combat weapon', 0, 'ccw' ],
                ['Venom blade', 5, 'vblade' ],
                ['Power weapon', 10, 'pwep' ],
                ['Agoniser', 20, 'agon' ]
            ])
            self.gl = self.opt_options_list('',[
                ["Phantasm grenade launcher", 25, 'phgrlnch']
            ])

        def check_rules(self):
            self.wep1.set_visible(not self.opt.get('splgun'))
            self.wep2.set_visible(not self.opt.get('splgun'))
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Kabalite Warrior', 5, 20, 9)
        h1 = [
            ['Splinter cannon', 10, 'splcan' ],
            ['Dark lance', 25, 'dlance' ]
        ]
        self.hvy1 = [self.opt_count(o[0], 0, 1, o[1]) for o in h1]

        h2 = [
            ['Shredder', 5, 'shred' ],
            ['Blaster', 15, 'blast' ]
        ]
        self.hvy2 = [self.opt_count(o[0], 0, 1, o[1]) for o in h2]
        self.leader = self.opt_optional_sub_unit(self.Sybarite.name, self.Sybarite())
        self.transport = self.opt_optional_sub_unit('Transport', [Raider(), Venom()], id='trans')

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(5 - ldr, 20 - ldr)
        norm_counts(0, int(self.get_count() / 10), self.hvy1)
        norm_counts(0, 1, self.hvy2)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id] + [o.id for o in self.hvy1 + self.hvy2])
        self.description.add(['Splinter rifle'], self.warriors.get() - reduce(lambda val, i: val + i.get(), self.hvy1 + self.hvy2, 0))
        self.description.add([o.get_selected() for o in self.hvy1 + self.hvy2])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Wyches(Unit):
    name = 'Wyches'
    gear = ['Wychsuit', 'Combat drugs','Plasma grenades']

    class Hekatrix(Unit):
        name = 'Hekatrix'
        gear = ['Wychsuit', 'Combat drugs','Plasma grenades']
        base_points = 10 + 10

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon',[
                ['Splinter pistol', 0, 'splpist' ],
                ['Blast pistol', 15, 'blpist' ]
            ])
            self.wep2 = self.opt_one_of('', [
                ['Close combat weapon', 0, 'ccw' ],
                ['Venom blade', 5, 'vblade' ],
                ['Power weapon', 10, 'pwep' ],
                ['Agoniser', 20, 'agon' ]
            ])
            self.opt = self.opt_options_list('',[
                ['Phantasm grenade launcher', 10, 'phgrl']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Wych', 5, 15, 10)
        ccwlist = [
            ['Razorflails', 10, 'rzf' ],
            ['Hydra gauntlets', 10, 'hydra' ],
            ['Shardnet and impaler', 10, 'net' ]
        ]
        self.wep = [self.opt_count(o[0], 0, 1, o[1]) for o in ccwlist]
        self.leader = self.opt_optional_sub_unit(self.Hekatrix.name, self.Hekatrix())
        self.spec = self.opt_options_list('Option',[
            ['Haywire grenades', 2, 'hwrgrnd' ]
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [Raider(), Venom()], id='trans')

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(5 - ldr, 15 - ldr)
        norm_counts(0, int(self.get_count() / 5), self.wep)
        self.set_points(self.build_points(count=1, exclude=[self.spec.id]) + self.spec.points() * self.get_count())
        self.build_description(count=self.warriors.get(), exclude=[self.spec.id, self.warriors.id] + [o.id for o in self.wep])
        self.description.add(['Splinter pistol','Close combat weapon'], self.warriors.get() - reduce(lambda val, i: val + i.get(), self.wep, 0))
        self.description.add([o.get_selected() for o in self.wep])
        self.description.add(self.spec.get_selected(), self.get_count())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
