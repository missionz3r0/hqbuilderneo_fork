__author__ = 'dante'

from builder.core.unit import Unit, StaticUnit

class GuardBrood(Unit):
    name = 'Tyrant Guard Brood'
    base_points = 60

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Guards', 1, 3, self.base_points)
        self.weapon = self.opt_one_of('Weapon', [
            ['Scything talons', 0],
            ['Bone sword', 15],
            ['Lash whip', 5],
        ])

    def check_rules(self):
        self.points.set(self.count.points() + self.weapon.points() * self.count.get())
        self.description.reset()
        self.description.set_header(self.name, self.base_points)
        self.description.add('Guard')
        self.description.add(self.weapon.get_selected())
        self.set_description(self.description)


class HiveTyrant(Unit):
    name = 'Hive Tyrant'
    base_points = 170

    def __init__(self):
        Unit.__init__(self)

        self.wep1 = self.opt_one_of('Weapon',
            [
                ['Lash whip and bone sword', 0],
                ['Scything talons', 0],
                ['Twin-linked deathspitter', 15],
                ['Twin-linked devourers with brainleech worms', 15],
                ['Stranglethorn cannon', 20, 'sc'],
                ['Heavy venom cannon', 25, 'hv'],
                ], 'wep1')
        self.wep2 = self.opt_one_of('Weapon',
            [
                ['Scything talons', 0, 'st'],
                ['Twin-linked deathspitter', 15],
                ['Twin-linked devourers with brainleech worms', 15],
                ['Stranglethorn cannon', 20, 'sc'],
                ['Heavy venom cannon', 25, 'hv'],
                ], 'wep2')
        self.opt1 = self.opt_options_list('Options', [
                ["Hive Commander",25],
                ["Indescribable Horror", 25],
                ["Old Adversary", 25],
                ["Adrenal glands", 10],
                ["Toxin sacs", 10],
                ["Acid blood", 15],
                ["Implant Attack", 15],
                ["Toxic Miasma", 15],
                ["Regeneration", 20],
                ], id='opt1')
        self.opt2 = self.opt_options_list('Options', [
                ['Thorax swarm with either electroshock grubs', 25],
                ['Desiccator larvae', 25],
                ['Shredder shard beetles', 25],
                ['Wings', 60],
                ['Armoured shell', 40],
            ], limit=1, id='opt2')
        self.psychic = self.opt_options_list('Psychic Powers', [
            ['The Horror', 0],
            ['Leech Essence', 0],
            ['Paroxysm', 0],
            ['Psychic Scream', 0],
            ], limit=2)
        self.guards = self.opt_options_list('Guards',
            [['Tyrant Guard Brood', 60, 'grd']])
        self.guards_unit = self.opt_sub_unit(GuardBrood())

    def check_rules(self):
        self.guards_unit.set_active(self.guards.get('grd'))
        self.wep2.set_active_options(['hv', 'sc'], not self.wep1.get_cur() == 'hv' and not self.wep1.get_cur() == 'sc')
        self.wep1.set_active_options(['hv', 'sc'], not self.wep2.get_cur() == 'hv' and not self.wep2.get_cur() == 'sc')

        self.points.set(self.base_points + self.wep1.points() + self.wep2.points() + self.opt1.points() + self.opt2.points() +
            (self.guards_unit.points() if self.guards.get('grd') else 0))
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.wep1.get_selected())
        self.description.add(self.wep2.get_selected())
        self.description.add(self.opt1.get_selected())
        self.description.add(self.opt2.get_selected())
        self.description.add(self.psychic.get_selected())
        self.description.add(self.guards_unit.get_selected(), self.guards_unit.get_count())
        self.set_description(self.description)


class TheSwarmlord(Unit):
    name = 'The Swarmlord'
    base_points = 280
    unique = True

    def __init__(self):
        Unit.__init__(self)

        self.guards = self.opt_options_list('Guards',
            [['Tyrant Guard Brood', 60, 'grd']])
        self.guards_unit = self.opt_sub_unit(GuardBrood())

    def check_rules(self):
        self.guards_unit.set_active(self.guards.get('grd'))
        self.points.set(self.base_points + (self.guards_unit.points() if self.guards.get('grd') else 0))
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(['Bonded Exoskeleton', 'Bonesabres'])
        self.description.add(self.guards_unit.get_selected(), self.guards_unit.get_count())
        self.set_description(self.description)


class Tervigon(Unit):
    name = 'Tervigon'
    base_points = 160
    gear = ['Bonded Exoskeleton','Claws and teeth']
    def __init__(self):
        Unit.__init__(self)

        self.wep1 = self.opt_options_list('Weapon',
            [
                ['Scything talons', 5],
                ['Crushing Claws', 25],
                ], limit = 1, id = 'wep1')
        self.wep2 = self.opt_one_of('Weapon',
            [
                ["Stinger salvo", 0],
                ["Cluster spines", 0],
                ], 'wep2')

        self.opt1 = self.opt_options_list('Options', [
            ["Adrenal glands", 10],
            ["Toxin sacs", 10],
            ["Acid blood", 15],
            ["Implant Attack", 15],
            ["Toxic Miasma", 15],
            ["Regeneration", 30],
            ], id='opt1')

        self.psychic = self.opt_options_list('Psychic Powers', [
            ['Catalyst', 15],
            ['Onslaught', 15],
            ])

class TyranidPrime(Unit):
    name = 'Tyranid Prime'
    base_points = 80
    gear = ['Bonded Exoskeleton']
    def __init__(self):
        Unit.__init__(self)

        self.wep1 = self.opt_one_of('Weapon',
            [
                ['Scything talons', 0],
                ['Rending claws', 5],
                ['A pair of boneswords', 10],
                ['Lash whip and bone sword', 15],
                ], 'wep1')

        self.wep2 = self.opt_one_of('Weapon',
            [
                ['Devourer', 0],
                ['Scything talons', 0],
                ['Rending claws', 0],
                ['Spinefists', 0],
                ['Deathspitter', 5],
                ], 'wep2')
        self.opt1 = self.opt_options_list('Options', [
            ["Adrenal glands", 10],
            ["Toxin sacs", 10],
            ["Regeneration", 10],
            ], id='opt1')

class TheParasite(StaticUnit):
    name = "The Parasite of Mortex"
    base_points = 160
    gear = ['Implant Attack', 'Rending claws', 'Bonded Exoskeleton', 'Wings']
