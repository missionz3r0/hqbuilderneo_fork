__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit

class Shrike(Unit):
    name = 'Tyranid Shrike Brood'
    base_points = 35
    gear = ['Reinforced chitin', 'Wings']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Shrike', 3, 9, self.base_points)
        self.wep1 = self.opt_one_of('Weapon',
            [
                ['Scything talons', 0],
                ['Rending claws', 5],
                ], 'wep1')

        self.wep2 = self.opt_one_of('Weapon',
            [
                ['Devourer', 0],
                ['Spinefists', 0],
                ['Deathspitter', 5],
                ['A pair of boneswords', 10],
                ['Lash whip and bone sword', 15],
                ['Scything talons', 0],
                ], 'wep2')

        self.heavy = self.opt_options_list('Heavy Weapon', [
            ['Barbed strangler', 10, 'bs'],
            ['Venom cannon', 15, 'vc'],
            ], limit=1)
        self.opt1 = self.opt_options_list('Options', [
            ["Adrenal glands", 5],
            ["Toxin sacs", 5],
            ], id='opt1')

    def check_rules(self):
        heavy = self.heavy.get('bs') or self.heavy.get('vc')
        self.wep2.set_visible(not heavy)
        self.points.set((self.base_points + self.wep1.points() + self.wep2.points() + self.opt1.points()) * self.count.get()
                        + self.heavy.points())

        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.wep1.get_selected(), self.count.get())
        self.description.add(self.wep2.get_selected(), self.count.get())
        self.description.add(self.heavy.get_selected())
        if heavy:
            self.description.add('Devourer', self.count.get() - 1)
        self.description.add(self.opt1.get_selected(), self.count.get())
        self.set_description(self.description)


class Ravener(Unit):
    name = 'Ravener Brood'
    base_points = 30
    gear = ['Scything talons', 'Reinforced chitin']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Ravener ', 3, 9, self.base_points)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Scything talons', 0],
            ['Rending claws', 5],
        ], 'wep1')

        self.wep2 = self.opt_options_list('Weapon',[
            ['Devourer', 5],
            ['Spinefist', 5],
            ['Deathspitter', 10],
        ], id='wep2', limit=1)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])


class SkySlasher(Unit):
    name = 'Sky-Slasher Swarm Brood'
    base_points = 15
    gear = ['Chitin', 'Claws and Teeth', 'Wings']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Sky-Slasher Swarm', 3, 9, self.base_points)
        self.opt = self.opt_options_list('Options', [
            ["Spinefists", 5],
            ["Adrenal glands", 4],
            ["Toxin sacs", 4],
        ])

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])


class Gargoyle(Unit):
    name = 'Gargoyle Brood'
    base_points = 6
    gear = ['Blinding venom', 'Fleshborer', 'Chitin', 'Claws and Teeth', 'Wings']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Gargoyle', 10, 30, self.base_points)
        self.opt = self.opt_options_list('Options', [
            ["Adrenal glands", 1],
            ["Toxin sacs", 1],
        ])

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])


class Harpy(Unit):
    name = 'Harpy'
    base_points = 160
    gear = ['Hardened carapace', 'Scything talons', 'Spore Mine cysts', 'Wings']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Twin-linked Stranglethorn cannon', 0],
            ['Twin-linked Heavy venom cannon', 10],
        ], 'wep1')

        self.wep2 = self.opt_one_of('Weapon', [
            ['Stinger salvo', 0],
            ['Cluster spines', 0],
        ], 'wep2')

        self.opt = self.opt_options_list('Options', [
            ["Adrenal glands", 10],
            ["Toxin sacs", 10],
            ["Regeneration", 15],
        ])


class SporeMine(Unit):
    name = 'Spore Mine Cluster'
    base_points = 10
    gear = ['Spore Mine']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Spore Mine', 3, 6, self.base_points)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])
