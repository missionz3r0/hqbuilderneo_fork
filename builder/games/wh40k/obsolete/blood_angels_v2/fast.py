__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from .transport import Transport


class VanguardVeteranSquad(IATransportedUnit):
    type_name = 'Vanguard Veteran Squad'
    type_id = 'vanguard_veteran_squad_v1'

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    class Options(OptionsList):
        def __init__(self, parent):
            super(VanguardVeteranSquad.Options, self).__init__(parent=parent, name='Options')
            self.meltabombs = self.variant('Melta bombs', 5)

    class JumpPack(OptionsList):
        def __init__(self, parent):
            super(VanguardVeteranSquad.JumpPack, self).__init__(parent=parent, name='Options')
            self.variant('Jump packs', 10, per_model=True)

        @property
        def points(self):
            return super(VanguardVeteranSquad.JumpPack, self).points * self.parent.get_count()

    class CCW(OneOf):
        def __init__(self, parent, name):
            super(VanguardVeteranSquad.CCW, self).__init__(parent=parent, name=name)
            self.variant('Chainsword', 0)

    class Pistol(OneOf):
        def __init__(self, parent, name):
            super(VanguardVeteranSquad.Pistol, self).__init__(parent=parent, name=name)
            self.boltpistol = self.variant('Bolt pistol', 0)

    class Weapon(OneOf):
        def __init__(self, parent, name):
            super(VanguardVeteranSquad.Weapon, self).__init__(parent=parent, name=name)
            self.handflamer = self.variant('Hand flamer', 10)
            self.plasmapistol = self.variant('Plasma pistol', 15)
            self.infernuspistol = self.variant('Infernus pistol', 15)
            self.powerweapon = self.variant('Power weapon', 15)
            self.lightningclaw = self.variant('Lightning claw', 15)
            self.stormshield = self.variant('Storm shield', 20)
            self.powerfist = self.variant('Power fist', 25)
            self.thunderhammer = self.variant('Thunder hammer', 30)

    class SergeantWeapon(OneOf):
        def __init__(self, parent):
            super(VanguardVeteranSquad.SergeantWeapon, self).__init__(parent=parent, name='')
            self.powersword = self.variant('Power sword', 0)
            self.lightningclaw = self.variant('Lightning claw', 0)
            self.glaiveencarmine = self.variant('Glaive Encarmine', 0)
            self.powerfist = self.variant('Power fist', 10)
            self.thunderhammer = self.variant('Thunder hammer', 15)

    class VeteranWeapon1(Weapon, Pistol):
        pass

    class VeteranWeapon2(Weapon, CCW):
        pass

    class Veteran(ListSubUnit):
        type_name = 'Veteran'

        def __init__(self, parent):
            super(VanguardVeteranSquad.Veteran, self).__init__(parent=parent, points=20,
                                                               gear=VanguardVeteranSquad.model_gear)
            VanguardVeteranSquad.VeteranWeapon1(self, name='Weapon')
            VanguardVeteranSquad.VeteranWeapon2(self, name='')
            VanguardVeteranSquad.Options(self)

    class Sergeant(Unit):
        type_name = 'Space Marine Sergeant'

        def __init__(self, parent):
            super(VanguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, points=115-20*4, gear=VanguardVeteranSquad.model_gear
            )
            VanguardVeteranSquad.VeteranWeapon1(self, name='Weapon')
            VanguardVeteranSquad.SergeantWeapon(self)
            VanguardVeteranSquad.Options(self)

    def __init__(self, parent):
        super(VanguardVeteranSquad, self).__init__(parent)
        self.sergeant = SubUnit(self, self.Sergeant(None))
        self.marines = UnitList(self, self.Veteran, 4, 9)
        self.jump = self.JumpPack(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.marines.count + 1

    def check_rules(self):
        super(VanguardVeteranSquad, self).check_rules()
        self.transport.visible = self.transport.used = not self.jump.any
        self.jump.visible = self.jump.used = not self.transport.any


class SpaceMarineBikers(Unit):
    type_name = 'Space Marine Bike Squad'
    type_id = 'spacemarinebikesquad_v1'

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Space Marine bike'),
                  Gear('Twin-linked boltgun')]
    model_points = 25
    model = UnitDescription('Space Marine Biker', points=model_points, options=model_gear)

    class MarineCount(Count):
        normalizer = None

        @property
        def description(self):
            return [SpaceMarineBikers.model.clone().add(Gear('Bolt pistol')).set_count(self.cur - self.normalizer())]

    def __init__(self, parent):
        super(SpaceMarineBikers, self).__init__(parent=parent)

        self.sergeant = SubUnit(self, self.BikerSergeant(None))
        self.spacemarine = self.MarineCount(self, 'Space Marine Biker', min_limit=2, max_limit=7,
                                            points=self.model_points)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=2, points=g['points'],
                  gear=SpaceMarineBikers.model.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15),
            ]
        ]
        self.spacemarine.normalizer = lambda: sum(c.cur for c in self.spec)
        self.attack = self.AttackBike(self)

    class BikerSergeant(Unit):
        type_name = 'Biker Sergeant'
        type_id = 'bikersergeant_v1'

        def __init__(self, parent):
            super(SpaceMarineBikers.BikerSergeant, self).__init__(
                parent=parent, name='Biker Sergeant', points=90 - 2 * SpaceMarineBikers.model_points,
                gear=SpaceMarineBikers.model_gear
            )
            self.Weapon(self)
            self.Options(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(SpaceMarineBikers.BikerSergeant.Weapon, self).__init__(parent=parent, name='Weapon')
                self.boltpistol = self.variant('Bolt pistol', 0)
                self.combimelta = self.variant('Combi-melta', 10)
                self.combiflamer = self.variant('Combi-flamer', 10)
                self.combiplasma = self.variant('Combi-plasma', 10)
                self.plasmapistol = self.variant('Plasma pistol', 15)
                self.powerweapon = self.variant('Power weapon', 15)
                self.powerfist = self.variant('Power fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(SpaceMarineBikers.BikerSergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)

    class AttackBike(OptionsList):
        def __init__(self, parent):
            super(SpaceMarineBikers.AttackBike, self).__init__(parent=parent, name='Options')
            self.bike = self.variant('Attack bike', 40)
            self.weapon = self.Weapon(parent)

        def check_rules(self):
            self.weapon.visible = self.bike.value

        class Weapon(OneOf):
            def __init__(self, parent):
                super(SpaceMarineBikers.AttackBike.Weapon, self).__init__(parent=parent, name='', used=False)
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        @property
        def description(self):
            if self.bike.value:
                return UnitDescription(
                    'Attack Bike', points=40,
                    options=SpaceMarineBikers.model_gear + [Gear('Bolt pistol')])\
                    .add(self.weapon.description).add_points(self.weapon.points)
            return []

        @property
        def points(self):
            return super(SpaceMarineBikers.AttackBike, self).points + self.weapon.points

    def check_rules(self):
        super(SpaceMarineBikers, self).check_rules()
        Count.norm_counts(0, 2, self.spec)

    def get_count(self):
        return self.spacemarine.cur + 1 + self.attack.bike.value


class ScoutBikers(Unit):
    type_name = 'Scout Bike Squad'
    type_id = 'scoutbikesquad_v1'

    model_gear = [Gear('Scout armor'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Space Marine bike'),
                  Gear('Shotgun')]
    model_points = 20
    model = UnitDescription('Scout Biker', points=model_points, options=model_gear)

    class MarineCount(Count):
        normalizer = None

        @property
        def description(self):
            return [ScoutBikers.model.clone().add([Gear('Bolt pistol'), Gear('Twin-linked boltgun')]).
                    set_count(self.cur - self.normalizer())]

    def __init__(self, parent):
        super(ScoutBikers, self).__init__(parent=parent)

        self.sergeant = SubUnit(self, self.BikerSergeant(None))
        self.spacemarine = self.MarineCount(self, 'Scout Biker', min_limit=2, max_limit=9,
                                            points=self.model_points)
        grenade_name = 'Astartes grenade launcher'
        grenade_points = 10
        self.grenade = Count(
            self, name=grenade_name, min_limit=0, max_limit=3, points=grenade_points,
            gear=ScoutBikers.model.clone().add(Gear('Bolt pistol')).
            add(Gear(grenade_name)).add_points(grenade_points)
        )

        self.spacemarine.normalizer = lambda: self.grenade.cur
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutBikers.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.clustermines = self.variant('Cluster mines', 10)

    class BikerSergeant(Unit):
        def __init__(self, parent):
            super(ScoutBikers.BikerSergeant, self).__init__(name='Scout Biker Sergeant', parent=parent,
                                                            points=70 - 2 * ScoutBikers.model_points,
                                                            gear=ScoutBikers.model_gear + [Gear('Twin-linked boltgun')])
            self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(ScoutBikers.BikerSergeant.Weapon, self).__init__(parent=parent, name='Weapon')

                self.boltpistol = self.variant('Bolt pistol', 0)
                self.combimelta = self.variant('Combi-melta', 10)
                self.combiflamer = self.variant('Combi-flamer', 10)
                self.combiplasma = self.variant('Combi-plasma', 10)
                self.plasmapistol = self.variant('Plasma pistol', 15)
                self.powerweapon = self.variant('Power weapon', 15)
                self.powerfist = self.variant('Power fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(ScoutBikers.BikerSergeant.Options, self).__init__(parent=parent, name='Options')

                self.meltabombs = self.variant('Melta bombs', 5)
                self.locatorbeacon = self.variant('Locator beacon', 25)

    def check_rules(self):
        super(ScoutBikers, self).check_rules()
        self.grenade.max = min(3, self.spacemarine.cur)

    def get_count(self):
        return self.spacemarine.cur + 1


class LandSpeederSquadron(Unit):
    type_name = "Land Speeder Squadron"
    type_id = "land_speeder_squadron_v1"

    class LandSpeeder(ListSubUnit, SpaceMarinesBaseVehicle):
        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(LandSpeederSquadron.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy Bolter', 0)
                self.heavyflamer = self.variant('Heavy Flamer', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(LandSpeederSquadron.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade', limit=1)
                self.heavybolter = self.variant('Heavy Bolter', 10)
                self.heavyflamer = self.variant('Heavy Flamer', 10)
                self.multimelta = self.variant('Multi-melta', 20)
                self.assaultcannon = self.variant('Assault Cannon', 40)
                self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 40)

        def __init__(self, parent):
            super(LandSpeederSquadron.LandSpeeder, self).__init__(parent=parent, points=50, name="Land Speeder")
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

        @ListSubUnit.count_unique
        def get_unique_gear(self):
            return SpaceMarinesBaseVehicle.get_unique_gear(self)

    def __init__(self, parent):
        super(LandSpeederSquadron, self).__init__(parent=parent)
        self.speeders = UnitList(parent=self, unit_class=self.LandSpeeder, min_limit=1, max_limit=3)

    def get_count(self):
        return self.speeders.count

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.speeders.units), [])


class BaalPredator(SpaceMarinesBaseVehicle):
    type_name = 'Baal Predator'
    type_id = 'baalpredator_v1'

    def __init__(self, parent):
        super(BaalPredator, self).__init__(parent=parent, points=115, gear=[Gear('Smoke launchers')], tank=True)
        self.Turret(self)
        self.Sponsons(self)
        self.Options(self)

    class Turret(OneOf):
        def __init__(self, parent):
            super(BaalPredator.Turret, self).__init__(parent=parent, name='Weapon')
            self.twinlinkedassaultcannon = self.variant('Twin-linked assault cannon', 0)
            self.flamestormlascannon = self.variant('Flamestorm lascannon', 0)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(BaalPredator.Sponsons, self).__init__(parent=parent, name='Side sponsons', limit=1)
            self.heavyflamers = self.variant('Heavy flamers', 25, gear=[Gear('Heavy flamer', count=2)])
            self.heavybolters = self.variant('Heavy bolters', 30, gear=[Gear('Heavy bolter', count=2)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(BaalPredator.Options, self).__init__(parent=parent, name='Options')
            self.stormbolter = self.variant('Storm bolter', 10)
            self.hunterkillermissile = self.variant('Hunter-killer missile', 10)
            self.dozerblade = self.variant('Dozer blade', 5)
            self.extraarmour = self.variant('Extra armour', 15)
            self.searchlight = self.variant('Searchlight', 1)


class AttackBikeSquad(Unit):
    type_name = 'Attack Bike Squad'
    type_id = 'attackbike_v1'

    model_points = 40
    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Bolt pistol'),
                  Gear('Space Marine bike'), Gear('Twin-linked boltgun')]
    model_name = 'Attack Bike'
    model = UnitDescription(name=model_name, options=model_gear, points=model_points)

    class Count(Count):
        @property
        def description(self):
            return AttackBikeSquad.model.clone().add(Gear('Heavy bolter')).set_count(self.cur - self.parent.melta.cur)

    def __init__(self, parent):
        super(AttackBikeSquad, self).__init__(parent)
        self.bikers = self.Count(self, self.model_name, 1, 3, self.model_points, per_model=True)
        self.melta = Count(
            self, 'Multi-melta', 0, 1, 10, per_model=True,
            gear=self.model.clone().add(Gear('Multi-melta')).add_points(10)
        )

    def check_rules(self):
        super(AttackBikeSquad, self).check_rules()
        self.melta.max = self.bikers.cur

    def get_count(self):
        return self.bikers.cur
