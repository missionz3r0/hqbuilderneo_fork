__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.obsolete.blood_angels import troops
from .transport import Transport, DreadnoughtTransport
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedDreadnought, IATransportedUnit


ScoutSquad = Unit.norm_core1_unit(troops.ScoutSquad)


class DeathCompanyDreadnought(IATransportedDreadnought):
    type_name = 'Death Company Dreadnought'
    type_id = 'deathcompanydreadnought_v1'

    def __init__(self, parent):
        super(DeathCompanyDreadnought, self).__init__(parent=parent, points=125, gear=[Gear('Smoke launchers')],
                                                      walker=True)
        self.Ccw(self)
        self.BuildIn(self)
        self.Options(self)
        self.transport = DreadnoughtTransport(self)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.BuildIn, self).__init__(parent=parent, name='')

            self.builtinstormbolter = self.variant('Built-in Storm bolter', 0,
                                                   gear=[Gear('Storm bolter'), Gear('Meltagun')])
            self.builtinheavyflamer = self.variant('Built-in Heavy flamer', 10,
                                                   gear=[Gear('Heavy flamer'), Gear('Meltagun')])

    class Ccw(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.Ccw, self).__init__(parent=parent, name='Weapon')
            self.bloodtalons = self.variant('Blood fists', 0, gear=[Gear('Blood fist', count=2)])
            self.bloodtalons = self.variant('Blood talons', 0, gear=[Gear('Blood talon', count=2)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.magnagrapple = self.variant('Magna-grapple', 15)
            self.searchlight = self.variant('Searchlight', 1)


class DeathCompany(IATransportedUnit):
    type_name = 'Death Company'
    type_id = 'death_company_v1'

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    class Options(OptionsList):
        def __init__(self, parent):
            super(DeathCompany.Options, self).__init__(parent=parent, name='')
            self.variant('Lemartes, Guardian of the Lost', 150, gear=[
                UnitDescription(
                    name='Lemartes, Guardian of the Lost',
                    points=150,
                    options=[Gear('Power armour'), Gear('Bolt pistol'), Gear('The Blood Crozius'),
                             Gear('Frag grenades'), Gear('Krak grenades'), Gear("Jump Pack"), Gear('Rosarius')]
                )
            ])

    class JumpPack(OptionsList):
        def __init__(self, parent):
            super(DeathCompany.JumpPack, self).__init__(parent=parent, name='Options')
            self.variant('Jump packs', 15, per_model=True)

        @property
        def points(self):
            return super(DeathCompany.JumpPack, self).points * self.parent.marines.count

    class CCW(OneOf):
        def __init__(self, parent, name):
            super(DeathCompany.CCW, self).__init__(parent=parent, name=name)
            self.variant('Chainsword', 0)

    class Pistol(OneOf):
        def __init__(self, parent, name):
            super(DeathCompany.Pistol, self).__init__(parent=parent, name=name)
            self.variant('Bolt pistol', 0)
            self.variant('Boltgun', 0)

    class Weapon(OneOf):
        def __init__(self, parent, name):
            super(DeathCompany.Weapon, self).__init__(parent=parent, name=name)
            self.variant('Power weapon', 15)
            self.variant('Power fist', 25)
            self.variant('Thunder hammer', 30)

    class SpecWeapon(OneOf):
        def __init__(self, parent, name):
            super(DeathCompany.SpecWeapon, self).__init__(parent=parent, name=name)
            self.spec = [
                self.variant('Hand flamer', 10),
                self.variant('Plasma pistol', 15),
                self.variant('Infernus pistol', 15),
            ]

    class Weapon1(SpecWeapon, Weapon, Pistol):
        pass

    class Weapon2(Weapon, CCW):
        pass

    class Marine(ListSubUnit):
        type_name = 'Death Company'

        def __init__(self, parent):
            super(DeathCompany.Marine, self).__init__(parent=parent, points=20, gear=DeathCompany.model_gear)
            self.wep1 = DeathCompany.Weapon1(self, name='Weapon')
            DeathCompany.Weapon2(self, name='')

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.wep1.cur in self.wep1.spec

    def __init__(self, parent):
        super(DeathCompany, self).__init__(parent)
        self.marines = UnitList(self, self.Marine, 3, 30)
        self.jump = self.JumpPack(self)
        self.opt = self.Options(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.marines.count + int(self.opt.any)

    def check_rules(self):
        super(DeathCompany, self).check_rules()
        self.transport.visible = self.transport.used = not self.jump.any
        self.jump.visible = self.jump.used = not self.transport.any
        spec_lim = int(self.get_count() / 5)
        spec = sum(u.count_spec() for u in self.marines.units)
        if spec > spec_lim:
            self.error('Death Company can take ony {} special pistols (taken: {})'.format(spec_lim, spec))

    def get_unique_gear(self):
        return super(DeathCompany, self).get_unique_gear() + self.opt.description


class TacticalSquad(IATransportedUnit):
    type_name = 'Tactical Squad'
    type_id = 'tacticalsquad_v1'

    model_points = 16
    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]
    model = UnitDescription('Space Marine', points=model_points, options=model_gear + [Gear('Bolt pistol')])

    class SpaceMarineSergeant(Unit):
        def __init__(self, parent):
            super(TacticalSquad.SpaceMarineSergeant, self).__init__(
                parent=parent, points=90-4*TacticalSquad.model_points,
                gear=TacticalSquad.model_gear, name='Space Marine Sergeant'
            )
            self.Weapon(self, 'Weapon', boltpistol=True)
            self.Weapon(self, '', boltgun=True)
            self.Options(self)

        class Weapon(OneOf):
            def __init__(self, parent, name, boltpistol=False, boltgun=False):
                super(TacticalSquad.SpaceMarineSergeant.Weapon, self).__init__(parent=parent, name=name)

                if boltgun:
                    self.boltgun = self.variant('Boltgun', 0)
                if boltpistol:
                    self.boltpistol = self.variant('Bolt pistol', 0)
                self.chainsword = self.variant('Chainsword', 0)
                self.combimelta = self.variant('Combi-melta', 10)
                self.combiflamer = self.variant('Combi-flamer', 10)
                self.combiplasma = self.variant('Combi-plasma', 10)
                self.stormbolter = self.variant('Stormbolter', 10)
                self.plasmapistol = self.variant('Plasma pistol', 15)
                self.powerweapon = self.variant('Power weapon', 15)
                self.powerfist = self.variant('Power fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(TacticalSquad.SpaceMarineSergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.teleporthomer = self.variant('Teleport homer', 15)

    def __init__(self, parent):
        super(TacticalSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.SpaceMarineSergeant(None))
        self.marines = Count(self, 'Space Marine', min_limit=4, max_limit=9, points=TacticalSquad.model_points)
        self.spec = self.Special(self)
        self.transport = Transport(self)

    class Special(OptionsList):
        def build_opts(self, opts):
            return [
                self.variant(g['name'], points=g['points'],
                             gear=TacticalSquad.model.clone().add(Gear(g['name'])).add_points(g['points']))
                for g in opts
            ]

        def __init__(self, parent):
            super(TacticalSquad.Special, self).__init__(parent=parent, name='Weapon')
            self.spec = self.build_opts([
                dict(name='Flamer', points=0),
                dict(name='Meltagun', points=5),
                dict(name='Plasma gun', points=10),
            ])
            self.heavy = self.build_opts([
                dict(name='Multi-melta', points=0),
                dict(name='Missile launcher', points=0),
                dict(name='Plasma cannon', points=5),
                dict(name='Lascannon', points=10),
            ])

        def check_rules(self):
            super(TacticalSquad.Special, self)
            self.visible = self.used = self.parent.get_count() == 10
            self.process_limit(self.spec, 1)
            self.process_limit(self.heavy, 1)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.get_count(), self.sergeant.description)
        count = self.marines.cur
        if self.spec.used:
            desc.add(self.spec.description)
            count -= self.spec.count
        desc.add(self.model.clone().add(Gear('Boltgun')).set_count(count))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1


class AssaultSquad(IATransportedUnit):
    type_name = 'Assault squad'
    type_id = 'assaultsquad_v1'

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 18
    model = UnitDescription('Space Marine', points=model_points, options=model_gear + [Gear('Chainsword')])

    class MarineCount(Count):
        normalizer = None

        @property
        def description(self):
            return [AssaultSquad.model.clone().add(Gear('Bolt pistol')).set_count(self.cur - self.normalizer())]

    def __init__(self, parent):
        super(AssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.SpaceMarineSergeant(None))
        self.spacemarine = self.MarineCount(self, 'Space Marine', min_limit=4, max_limit=9, points=self.model_points)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=1, points=g['points'],
                  gear=AssaultSquad.model.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Hand flamer', points=10),
                dict(name='Meltagun', points=10),
                dict(name='Plasma pistol', points=15),
                dict(name='Plasma gun', points=15),
                dict(name='Infernus pistol', points=15),
            ]
        ]
        self.spacemarine.normalizer = lambda: sum(c.cur for c in self.spec)
        self.opt = self.Options(self)
        self.transport = Transport(self, off=35)

    class SpaceMarineSergeant(Unit):
        type_name = 'Space Marine Sergeant'
        type_id = 'spacemarinesergeant_v1'

        def __init__(self, parent):
            super(AssaultSquad.SpaceMarineSergeant, self).__init__(
                parent=parent, name='Space Marine Sergeant', points=100 - 4 * AssaultSquad.model_points,
                gear=AssaultSquad.model_gear
            )
            self.Weapon(self, 'Weapon', ccw=True)
            self.Weapon(self, '', boltpistol=True)
            self.Options(self)

        class Weapon(OneOf):
            def __init__(self, parent, name, boltpistol=False, ccw=False):
                super(AssaultSquad.SpaceMarineSergeant.Weapon, self).__init__(parent=parent, name=name)

                if boltpistol:
                    self.boltpistol = self.variant('Bolt pistol', 0)
                if ccw:
                    self.chainsword = self.variant('Chainsword', 0)
                self.handflamer = self.variant('Hand flamer', 10)
                self.plasmapistol = self.variant('Plasma pistol', 15)
                self.infernuspistol = self.variant('Infernus pistol', 15)
                self.powerweapon = self.variant('Power weapon', 15)
                self.lightningclaw = self.variant('Lightning claw', 15)
                self.stormshield = self.variant('Storm shield', 20)
                self.powerfist = self.variant('Power fist', 25)
                self.thunderhammer = self.variant('Thunder hammer', 30)

        class Options(OptionsList):
            def __init__(self, parent):
                super(AssaultSquad.SpaceMarineSergeant.Options, self).__init__(parent=parent, name='Options')

                self.meltabombs = self.variant('Melta bombs', 5)
                self.combatshield = self.variant('Combat shield', 5)

    class Options(OptionsList):
        def __init__(self, parent):
            super(AssaultSquad.Options, self).__init__(parent=parent, name='Options')
            self.jumppack = self.variant('Jump Pack')
            self.jumppack.value = True

        def check_rules(self):
            super(AssaultSquad.Options, self).check_rules()
            self.visible = self.used = not self.parent.transport.any
            self.parent.transport.visible = self.parent.transport.used = not self.jumppack.value

    def check_rules(self):
        super(AssaultSquad, self).check_rules()
        Count.norm_counts(0, int(self.get_count() / 5), self.spec)

    def get_count(self):
        return self.spacemarine.cur + 1
