__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.obsolete.blood_angels import elites
from .transport import Transport, TerminatorTransport, DreadnoughtTransport
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit, IATransportedDreadnought

Chaplain = Unit.norm_core1_unit(elites.Chaplain)
SanguinaryPriest = Unit.norm_core1_unit(elites.SanguinaryPriest)
Corbulo = Unit.norm_core1_unit(elites.Corbulo)


class TerminatorAssaultSquad(IATransportedUnit):
    type_name = 'Terminator Assault Squad'
    type_id = 'terminatorassaultsquad_v1'

    model_points = 40

    class Sergeant(Unit):

        def __init__(self, parent):
            super(TerminatorAssaultSquad.Sergeant, self).__init__(
                name='Terminator Sergeant', parent=parent, points=200 - 4 * TerminatorAssaultSquad.model_points,
                gear=[Gear('Terminator armour')]
            )
            self.weapon = self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(TerminatorAssaultSquad.Sergeant.Weapon, self).__init__(parent=parent, name='Weapon')
                self.lightningclaws = self.variant('Lightning claws', 0, gear=[Gear('Lightning claw', count=2)])
                self.thunderhammerandstormshield = self.variant(
                    'Thunder hammer and storm shield', 5, gear=[Gear('Thunder hammer'), Gear('Storm shield')])

    def __init__(self, parent):
        super(TerminatorAssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(None))
        self.terminator = Count(self, 'Terminator', min_limit=4, max_limit=9, points=40, gear=[])
        self.thunderhammerandstormshield = Count(self, 'Thunder Hammer and Storm Shield',
                                                 min_limit=0, max_limit=4, points=5, gear=[])
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terminator.cur + 1

    def check_rules(self):
        super(TerminatorAssaultSquad, self).check_rules()
        self.thunderhammerandstormshield.max = self.terminator.cur

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        term = UnitDescription('Terminator', self.model_points, options=[Gear('Terminator armour')])
        desc.add(term.clone().add([Gear('Thunder hammer'), Gear('Storm shield')]).add_points(5)
            .set_count(self.thunderhammerandstormshield.cur))
        desc.add(term.add([Gear('Lightning claw', count=2)])
            .set_count(self.terminator.cur - self.thunderhammerandstormshield.cur))
        desc.add(self.transport.description)
        return desc


class TerminatorSquad(IATransportedUnit):
    type_name = 'Terminator Squad'
    type_id = 'terminator_squad_v1'

    model_points = 40

    class Veteran(ListSubUnit):
        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant('Power fist', 0)
                self.chain_fist = self.variant('Chainfist', 5)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant('Storm bolter', 0)
                self.hf = self.variant('Heavy flamer', 5)
                self.ac = self.variant('Assault Cannon', 30)

            def enable_spec(self, value):
                self.hf.active = self.ac.active = value

            def has_spec(self):
                return self.cur != self.bolt

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(TerminatorSquad.Veteran.Weapon, self).__init__(parent=parent, name='')
                self.cyc = self.variant('Cyclone missile launcher', 30)

        def __init__(self, parent):
            super(TerminatorSquad.Veteran, self).__init__(
                parent=parent, name='Terminator', points=TerminatorSquad.model_points,
                gear=[Gear('Terminator Armour')]
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.weapon = self.Weapon(self)

        def check_rules(self):
            super(TerminatorSquad.Veteran, self).check_rules()
            self.right_weapon.enable_spec(not self.weapon.cyc.value)
            self.weapon.cyc.active = not self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.weapon.cyc.value or self.right_weapon.has_spec()

    def __init__(self, parent):
        super(TerminatorSquad, self).__init__(parent=parent, points=self.model_points, gear=[UnitDescription(
            'Terminator Sergeant', 200 - 4 * self.model_points,
            options=[Gear('Terminator armour'), Gear('Storm bolter'), Gear('Power sword')]
        )])
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))


class FuriosoDreadnought(IATransportedDreadnought):
    type_name = 'Furioso Dreadnought'
    type_id = 'furioso_v1'

    def __init__(self, parent):
        super(FuriosoDreadnought, self).__init__(parent=parent, points=125, walker=True, gear=[Gear('Smoke launchers')])
        self.claws = self.Claws(self)
        self.wep1 = self.Weapon(self)
        self.build_in = self.BuildIn(self)
        self.wep2 = self.Weapon(self, melta=True)
        self.opt = self.Options(self)
        self.transport = DreadnoughtTransport(self)

    def check_rules(self):
        self.wep1.cannon.active = not self.wep2.cannon == self.wep2.cur
        self.wep2.cannon.active = not self.wep1.cannon == self.wep1.cur

    class Weapon(OneOf):
        def __init__(self, parent, melta=False):
            super(FuriosoDreadnought.Weapon, self).__init__(parent=parent, name='')
            self.fist = self.variant('Blood fist', 0, gear=[Gear('Blood fist')] + [Gear('Meltagun') if melta else []])
            self.cannon = self.variant('Frag cannon', 0)

        def check_rules(self):
            super(FuriosoDreadnought.Weapon, self).check_rules()
            self.visible = self.used = not self.parent.is_librarian and not self.parent.claws.any

        @property
        def is_dccw(self):
            return self.cur == self.fist

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(FuriosoDreadnought.BuildIn, self).__init__(parent, name='')
            self.variant('Built-in Storm bolter', 0, gear=[Gear('Storm bolter')])
            self.variant('Built-in Heavy flamer', 10, gear=[Gear('Heavy flamer')])

        def check_rules(self):
            super(FuriosoDreadnought.BuildIn, self).check_rules()
            self.visible = self.used = not self.parent.is_librarian

    class Claws(OptionsList):
        def __init__(self, parent):
            super(FuriosoDreadnought.Claws, self).__init__(parent=parent, name='Weapon')
            self.variant('Blood talons', 0, gear=[Gear('Blood talon', count=2), Gear('Meltagun')])

        def check_rules(self):
            super(FuriosoDreadnought.Claws, self).check_rules()
            self.visible = self.used = not self.parent.is_librarian

    class Options(OptionsList):
        def __init__(self, parent):
            super(FuriosoDreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.extraarmour = self.variant('Extra armour', 15)
            self.magna_grapple = self.variant('Magna-grapple', 15)
            self.searchlight = self.variant('Searchlight', 1)
            self.librarian = self.variant('Furioso Librarian', 50, gear=[Gear('Blood fist'), Gear('Storm bolter'),
                                                                         Gear('Psychic hood'), Gear('Force weapon')])

    @property
    def is_librarian(self):
        return self.opt.librarian.value


class SanguinaryGuards(Unit):
    type_name = 'Sanguinary Guard'
    type_id = 'sanguinaryguard_v1'

    class SanguinaryGuard(ListSubUnit):
        def __init__(self, parent):
            super(SanguinaryGuards.SanguinaryGuard, self).__init__(
                parent=parent, name='Sanguinary Guard', points=40,
                gear=[Gear("Artificer Armour"), Gear('Frag and Krak Grenades'), Gear('Jump Pack')]
            )
            self.Weapon1(self)
            self.Weapon2(self)
            self.banner = self.Standard(self)

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(SanguinaryGuards.SanguinaryGuard.Weapon1, self).__init__(parent=parent, name='Weapon')
                self.angelusboltgun = self.variant('Angelus Boltgun', 0)
                self.plasmapistol = self.variant('Plasma Pistol', 10)
                self.infernuspistol = self.variant('Infernus Pistol', 10)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(SanguinaryGuards.SanguinaryGuard.Weapon2, self).__init__(parent=parent, name='')
                self.glaiveencarmine = self.variant('Glaive Encarmine', 0)
                self.powerfist = self.variant('Power Fist', 10)

        class Standard(OptionsList):
            def __init__(self, parent):
                super(SanguinaryGuards.SanguinaryGuard.Standard, self).__init__(parent=parent, name='Banner', limit=1)
                self.chapterbanner = self.variant('Chapter Banner', 30)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banner.any

    def __init__(self, parent):
        super(SanguinaryGuards, self).__init__(parent=parent, points=0, gear=[])
        self.guards = UnitList(self, self.SanguinaryGuard, 1, 5, start_value=5)
        self.opt = self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(SanguinaryGuards.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.deathmasks = self.variant('Death Masks', 25)

    def get_count(self):
        return self.guards.count

    def check_rules(self):
        super(SanguinaryGuards, self).check_rules()
        flag = sum(c.count_banners() for c in self.guards.units)
        if flag > 1:
            self.error('Only one Sanguinary Guard can take a banner (taken: {})'.format(flag))
        if self.get_count() != 5:
            self.error('Sanguinary Guard must include 5 Sanguinary Guards (taken: {})'.format(self.get_count()))

    def get_unique_gear(self):
        return sum((unit.banner.description for unit in self.guards.units), [])


class Techmarine(Unit):
    type_name = 'Techmarine'
    type_id = 'techmarine_v1'

    def __init__(self, parent):
        super(Techmarine, self).__init__(parent=parent, points=50,
                                         gear=[Gear('Frag grenades'), Gear('Krak grenades'), Gear('Artificar armour')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Special(self)

        self.base_servitor = 15
        self.servitor_desc = UnitDescription('Servitor', points=self.base_servitor,
                                             options=[Gear('Close combat weapon')])

        self.servitors = Count(self, 'Servitors', min_limit=0, max_limit=5, points=self.base_servitor, gear=[])
        self.spec = [
            Count(self, 'Heavy bolter', min_limit=0, max_limit=2, points=20,
                  gear=self.servitor_desc.clone().add(Gear('Heavy bolter')).add_points(20)),
            Count(self, 'Multi-melta', min_limit=0, max_limit=2, points=30,
                  gear=self.servitor_desc.clone().add(Gear('Multi-melta')).add_points(30)),
            Count(self, 'Plasma cannon', min_limit=0, max_limit=2, points=30,
                  gear=self.servitor_desc.clone().add(Gear('Plasma cannon')).add_points(30))
        ]

    def check_rules(self):
        Count.norm_counts(0, min(self.servitors.cur, 2), self.spec)
        for c in self.spec:
            c.visible = c.used = self.servitors.cur != 0

    def build_description(self):
        desc = super(Techmarine, self).build_description()
        desc.add(self.servitor_desc.clone().add(Gear('Servo-arm')).set_count(self.servitors.cur -
                                                                             sum(o.cur for o in self.spec)))
        return desc

    class Special(OneOf):
        def __init__(self, parent):
            super(Techmarine.Special, self).__init__(parent=parent, name='Options')
            self.servoarm = self.variant('Servo-arm', 0)
            self.jumppack = self.variant('Jump Pack', 0)
            self.servoharness = self.variant('Servo-harness', 25)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Techmarine.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.boltpistol = self.variant('Bolt pistol', 0)
            self.boltgun = self.variant('Boltgun', 0)
            self.stormbolter = self.variant('Storm Bolter', 5)
            self.combimelta = self.variant('Combi-melta', 10)
            self.combiflamer = self.variant('Combi-flamer', 10)
            self.combiplasma = self.variant('Combi-plasma', 10)
            self.plasmapistol = self.variant('Plasma pistol', 15)

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(Techmarine.Weapon2, self).__init__(parent=parent, name='', limit=1)
            self.powerweapon = self.variant('Power weapon', 15)
            self.thunderhammer = self.variant('Thunder hammer', 30)


class SternguardVeteranSquad(IATransportedUnit):
    type_name = 'Sternguard Veteran Squad'
    type_id = 'sternguard_veteran_squad_v1'

    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Special issue ammunition')]
    model_points = 25

    class Options(OptionsList):
        def __init__(self, parent):
            super(SternguardVeteranSquad.Options, self).__init__(parent=parent, name='Options')
            self.meltabombs = self.variant('Melta bombs', 5)

    class Pistol(OneOf):
        def __init__(self, parent, name):
            super(SternguardVeteranSquad.Pistol, self).__init__(parent=parent, name=name)
            self.boltpistol = self.variant('Bolt pistol', 0)

    class Boltgun(OneOf):
        def __init__(self, parent, name):
            super(SternguardVeteranSquad.Boltgun, self).__init__(parent=parent, name=name)
            self.boltgun = self.variant('Boltgun', 0)

    class CombyWeapon(OneOf):
        def __init__(self, parent, name):
            super(SternguardVeteranSquad.CombyWeapon, self).__init__(parent=parent, name=name)
            self.stormbolter = self.variant('Storm bolter', 5)
            self.combimelta = self.variant('Combi-melta', 5)
            self.combiflamer = self.variant('Combi-flamer', 5)
            self.combiplasma = self.variant('Combi-plasma', 5)

    class SpecialWeapon(OneOf):
        def __init__(self, parent, name):
            super(SternguardVeteranSquad.SpecialWeapon, self).__init__(parent=parent, name=name)
            self.spec = [
                self.variant('Flamer', 5),
                self.variant('Meltagun', 5),
                self.variant('Heavy bolter', 5),
                self.variant('Multi-melta', 5),
                self.variant('Missile launcher', 5),
                self.variant('Plasmagun', 10),
                self.variant('Plasma cannon', 10),
                self.variant('Heavy flamer', 10),
                self.variant('Lascannon', 15),
            ]

    class SergeantWeapon(OneOf):
        def __init__(self, parent, name):
            super(SternguardVeteranSquad.SergeantWeapon, self).__init__(parent=parent, name=name)
            self.variant('Chainsword', 0)
            self.powersword = self.variant('Power weapon', 15)
            self.plasmapistol = self.variant('Plasma pistol', 15)
            self.lightningclaw = self.variant('Lightning claw', 15)
            self.powerfist = self.variant('Power fist', 25)

    class SergeantWeapon1(SergeantWeapon, Pistol):
        pass

    class SergeantWeapon2(CombyWeapon, SergeantWeapon, Boltgun):
        pass

    class VeteranWeapon(SpecialWeapon, CombyWeapon, Boltgun):
        pass

    class Veteran(ListSubUnit):
        type_name = 'Veteran'

        def __init__(self, parent):
            super(SternguardVeteranSquad.Veteran, self).__init__(
                parent=parent, points=SternguardVeteranSquad.model_points,
                gear=SternguardVeteranSquad.model_gear + [Gear('Bolt pistol')]
            )
            self.wep = SternguardVeteranSquad.VeteranWeapon(self, name='Weapon')

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.wep.cur in self.wep.spec

    class Sergeant(Unit):
        type_name = 'Space Marine Sergeant'

        def __init__(self, parent):
            super(SternguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, points=125 - SternguardVeteranSquad.model_points * 4,
                gear=SternguardVeteranSquad.model_gear
            )
            SternguardVeteranSquad.SergeantWeapon1(self, name='Weapon')
            SternguardVeteranSquad.SergeantWeapon2(self, name='')
            SternguardVeteranSquad.Options(self)

    def __init__(self, parent):
        super(SternguardVeteranSquad, self).__init__(parent)
        self.sergeant = SubUnit(self, self.Sergeant(None))
        self.marines = UnitList(self, self.Veteran, 4, 9)
        self.transport = Transport(self)

    def get_count(self):
        return self.marines.count + 1

    def check_rules(self):
        super(SternguardVeteranSquad, self).check_rules()
        spec = sum(c.count_spec() for c in self.marines.units)
        if spec > 2:
            self.error('Veterans may take only {0} special weapon (taken {1})'.format(2, spec))

