__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OptionsList, SubUnit, Count, UnitDescription
from .troops import Transport
from .armory import Boltgun, BoltPistol, Melee, HeavyWeapon, SpecialWeapon,\
    Ranged
from builder.games.wh40k.roster import Unit


class Celestians(Unit):
    type_name = 'Celestian Squad'
    type_id = 'celestians_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Celestian-Squad')

    common_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 14

    class Superior(Unit):
        name = 'Celestian Superior'

        class Weapon1(Ranged, Melee, Boltgun):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Celestians.Superior.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(Celestians.Superior, self).__init__(parent, self.name, Celestians.model_points,
                                                      gear=Celestians.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self)

        def check_rules(self):
            super(Celestians.Superior, self).check_rules()
            self.wep1.allow_melee(not self.wep2.is_melee())
            self.wep1.allow_ranged(not self.wep2.is_ranged())
            self.wep2.allow_melee(not self.wep1.is_melee())
            self.wep2.allow_ranged(not self.wep1.is_ranged())

    class MixWeapon(HeavyWeapon, SpecialWeapon):
        pass

    class Simulacrum(OptionsList):
        def __init__(self, parent):
            super(Celestians.Simulacrum, self).__init__(parent, 'Icon of Faith')
            self.variant('Simulacrum imperialis', 10)

    def __init__(self, parent):
        super(Celestians, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=None))
        self.squad = Count(self, 'Battle Sister', 4, 9, self.model_points, True)
        self.opt = self.Simulacrum(self)
        self.wep1 = SpecialWeapon(self)
        self.wep2 = self.MixWeapon(self, 'Special or Heavy weapon')
        self.transport = Transport(self)

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        desc = UnitDescription('Celestian', points=self.model_points,
                               options=self.common_gear + [Gear('Bolt Pistol')])
        res.add(self.sup.description)
        count = self.squad.cur
        if self.opt.any:
            res.add(desc.clone().add(Gear('Boltgun'))\
                                 .add(self.opt.description).add_points(self.opt.points))
            count -= 1
        for wep in [self.wep1, self.wep2]:
            if wep.any:
                res.add_dup(desc.clone().add(wep.description).add_points(wep.points))
                count -= 1
        if count:
            res.add(desc.clone().add(Gear('Boltgun')).set_count(count))
        res.add(self.transport.description)
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_statistics(self):
        return self.note_transport(super(Celestians, self).build_statistics())


class Repentia(Unit):
    type_name = 'Repentia squad'
    type_id = 'repentia_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Repentia-Squad')

    common_gear = [Gear('Evisecrator')]
    model_points = 14

    class Mistress(Unit):
        def __init__(self, parent):
            super(Repentia.Mistress, self).__init__(parent, 'Mistress of Repentance', 85 - 4 * Repentia.model_points,
                                                    gear=[Gear('Power armour'), Gear('Frag grenades'),
                                                          Gear('Krak grenades'), Gear('Neural whip', count=2)])
            # melta bombs for 5 points
            self.opt = Celestians.Superior.Options(self)

    def __init__(self, parent):
        super(Repentia, self).__init__(parent)
        self.leader = SubUnit(self, self.Mistress(parent=None))
        self.warriors = Count(self, 'Sister Repentia', 4, 9, self.model_points, True,
                              gear=UnitDescription('Sister Repentia', self.model_points, options=self.common_gear))
        self.transport = Transport(self)

    def get_count(self):
        return self.warriors.cur + 1

    def build_statistics(self):
        return self.note_transport(super(Repentia, self).build_statistics())
