__author__ = 'Denis Romanov'

from builder.core.unit import Unit, ListUnit, ListSubUnit


class Ark(Unit):
    name = "Doomsday Ark"
    base_points = 175
    gear = ['Doomsday cannon', 'Quantum shielding', 'Gauss Flayer Array', 'Gauss Flayer Array']
    static = True


class Barge(Unit):
    name = "Annihilation Barge"
    base_points = 90
    gear = ['Quantum shielding', 'Twin-linked Tesla Destructor']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Tesla Cannon', 0],
            ['Gauss Cannon', 0]
        ])


class Monolith(Unit):
    name = 'Monolith'
    base_points = 200
    gear = ['Eternity Gate', 'Gauss Flux Arcs', 'Gauss Flux Arcs', 'Gauss Flux Arcs', 'Gauss Flux Arcs','Particle Whip']
    static = True


class Scythe(Unit):
    name = "Doom Scythe"
    base_points = 175
    gear = ['Death Ray', 'Twin-linked Tesla Destructor']
    static = True


class Spyders(ListUnit):
    name = "Canoptek Spyder"

    class Spyder(ListSubUnit):
        name = "Canoptek Spyder"
        base_points = 50
        gear = ['Scarab Hive']

        def __init__(self):
            ListSubUnit.__init__(self)

            self.opt = self.opt_options_list('Options', [
                ["Fabricator Claw Array", 10],
                ["Gloom prism", 15],
                ["Twin-linked Particle Beamer", 25],
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Spyder, 1, 3)
