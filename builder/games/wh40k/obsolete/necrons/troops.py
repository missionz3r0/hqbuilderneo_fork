__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor


class GhostArk(Unit):
    name = 'Ghost Ark'
    base_points = 115
    gear = ['Quantum shielding', 'Gauss Flayer Array']
    static = True


class NightScythe(Unit):
    name = 'Night Scythe'
    base_points = 100
    gear = ['Twin-linked tesla destructor']
    static = True


class Warriors(Unit):
    name = "Necron Warriors"
    base_points = 13
    base_gear = ['Gauss flayer']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Warriors', 5, 20, self.base_points)
        self.trans = self.opt_options_list('Transport', [
            [GhostArk.name, GhostArk.base_points, 'ga'],
            [NightScythe.name, NightScythe.base_points, 'ns']
        ], limit=1)
        self.ga_unit = self.opt_sub_unit(GhostArk())
        self.ns_unit = self.opt_sub_unit(NightScythe())

    def check_rules(self):
        self.ga_unit.set_active(self.trans.get('ga'))
        self.ns_unit.set_active(self.trans.get('ns'))

        self.set_points(self.build_points(exclude=[self.trans.id], count=1, base_points=0))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Necron Warrior', points=self.base_points,
                                             gear=self.base_gear).build(self.get_count()))
        self.description.add(self.ga_unit.get_selected())
        self.description.add(self.ns_unit.get_selected())


class Immortals(Unit):
    name = "Necron Immortals"
    base_points = 17

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Immortals', 5, 10, self.base_points)
        self.wep = self.opt_one_of('Weapon', [
            ['Gauss blaster', 0],
            ['Tesla carbine', 0],
        ])
        self.trans = self.opt_options_list('Transport', [
            [NightScythe.name, NightScythe.base_points, 'ns']
        ], limit=1)
        self.ns_unit = self.opt_sub_unit(NightScythe())

    def check_rules(self):
        self.ns_unit.set_active(self.trans.get('ns'))

        self.set_points(self.build_points(exclude=[self.trans.id], count=1, base_points=0))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Necron Immortal',
                                             points=self.base_points).add_gear_opt(self.wep).build(self.get_count()))
        self.description.add(self.ns_unit.get_selected())
