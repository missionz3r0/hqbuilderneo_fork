__author__ = 'Ivan Truskov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.obsolete.eldar.elites import *
from builder.games.wh40k.obsolete.eldar.troops import *
from builder.games.wh40k.obsolete.eldar.fast import *
from builder.games.wh40k.obsolete.eldar.heavy import *
from builder.games.wh40k.obsolete.eldar.eldar_ex import *


class Eldar2006(LegacyWh40k):
    army_id = '37accc67457846b8ada0b8f63572273a'
    army_name = 'Eldar (2006)'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[Asurmen, JainZar, Baharroth, Karandras, Fuegan, Maugan, Avatar, Eldrad, Uriel, Autarch, Farseer,
                {'unit': Warlocks, 'active': False}] + ia_hq,
            elites=[Scorpions, Dragons, Wraithguard, Banshees, Harlequins],
            troops=[Avengers, Rangers, Guardians, BikerGuardians, TWraithguard],
            fast=[Spears, Spiders, Hawks, VyperSquadron] + ia_fast,
            heavy=[Battery, Reapers, Wraithlord, WalkerSquadron, Falkon, FirePrism] + ia_heavy,
            super_heavy=ia_super_heavy,
            secondary=secondary
        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_unit(Warlocks)
            return self.hq.min <= count <= self.hq.max

        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        warlocks = self.hq.count_units([Eldrad, Farseer])
        self.hq.set_active_types([Warlocks], warlocks > 0)
        if warlocks < self.hq.count_units([Warlocks]):
            self.error("You can't have more squads of Warlocks then Farseers (including Eldrad).")

        self.hq.set_active_types(ia_hq, self.opt.ia.value)
        self.fast.set_active_types(ia_fast, self.opt.ia.value)
        self.heavy.set_active_types(ia_heavy, self.opt.ia.value)
        if self.opt.ia.value:
            #IA vol. 11
            if self.hq.count_unit(BelAnnath) > 0:
                if self.hq.count_units([Farseer, Eldrad]) > 0:
                    self.error("If Farseer Bel-Annath is part of the army, no other Farseers may be included.")

            if self.hq.count_unit(Wraithseer) > 0:
                if self.troops.count_unit(TWraithguard) + self.elites.count_unit(Wraithguard) < 1:
                    self.error("If Wraithseer is part of the army, it must also incluide at least one unit of wraithguard.")
                if len(self.hq.get_units()) - self.hq.count_units([Warlocks, Wraithseer]) <= 0:
                        self.error("You must always include another non-Wraithseer unit in HQ")
