
__author__ = 'Ivan Truskov'

from .transport import WaveSerpent
from builder.core.unit import Unit, StaticUnit,ListSubUnit


class Asurmen(StaticUnit):
    name = "Asurmen, The Hand of Asuryan"
    base_points = 230
    gear = ['Wrist-mounted Avenger shuriken catapults','The Sword of Asur']


class JainZar(StaticUnit):
    name = "Jain Zar, The Storm of Silence"
    base_points = 190
    gear = ['Banshee mask','Executioner','The Silent Death']


class Baharroth(StaticUnit):
    name = 'Baharroth, the Cry of the wind'
    base_points = 200
    gear = ['Swooping Hawk wings','Swoopig Hawk grenade pack','Plasma grenades','Haywire grenades','Power weapon','Hawk\'s talon']
    

class Karandras(StaticUnit):
    name = 'Karandras, The Shadow Hunter'
    base_points = 215
    gear = ['The Scorpion\'s Bite','Scorpion chainsword','Scorpion\'s claw','Plasma grenades']
    

class Fuegan(StaticUnit):
    name = 'Fuegan, The Burning Lance'
    base_points = 205
    gear = ['Firepike','The Fireman Axe','Melta bombs']
    

class Maugan(StaticUnit):
    name = 'Maugan Ra, The harvester of Souls'
    base_points = 195
    gear = ['The Maugetar']
    

class Avatar(StaticUnit):
    name = 'The Avatar of Khaine'
    base_points = 155
    gear = ['The Wailing Doom']
    

class Eldrad(StaticUnit):
    name = 'Eldrad Ulthran'
    base_points = 210
    gear = ['Shuriken pistol','Staff of Ulthamar','Runes of warding','Runes of witnessing','Ghosthelm','Spirit stones','Witchblade','Rune armour']
    

class Uriel(StaticUnit):
    name = 'Prince Yriel'
    base_points = 155
    gear = ['Forcefield','The Eye of Wrath','The Spear of Twilight','Plasma grenades']
    

class Autarch(Unit):
    name = 'Autarch'
    base_points = 70
    gear = ['Shuriken pistol','Plasma grenades','Haywire grenades','Forcefield']
    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_options_list('One-handed weapon',[
            ['Power weapon',10,'pwep'],
            ['Scorpion chainsword',5,'sch'],
            ['Laser lance',20,'lance']
        ],1)
        self.wep2 = self.opt_options_list('Two-handed weapon',[
            ['Avenger shuriken catapult',2,'asc'],
            ['Death spinner',5,'dsp'],
            ['Fusion gun',10,'fgun'],
            ['Lasblaster',1,'lblas'],
            ['Reaper launcher',25,'rlnch'],
            ],1)
        self.ride = self.opt_options_list('Options',[
            ['Swooping Hawk wings',20,'wing'],
            ['Warp jump generator',25,'jump'],
            ['Eldar jetbike',30,'bike']
        ],1)
        self.face = self.opt_options_list('',[
            ['Banshee mask',3,'mask'],
            ['Mandiblasters',10,'blast']
        ],1)

    def check_rules(self):
        self.wep1.set_active_options(['lance'],self.ride.get('bike'))
        Unit.check_rules(self)


class Farseer(Unit):
    name = 'Farseer'
    base_points = 55
    gear = ['Ghosthelm','Shuriken pistol','Rune armour']
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon',[
                        ['Witchblade',0,'wsw'],
                        ['Singing spear',3,'ssp']
                            ])
        self.opt = self.opt_options_list('Options',[
                        ['Rues of warding',15,'rwr'],
                        ['Runes of witnessing',10,'rwt'],
                        ['Spirit stones',20,'spst'],
                        ['Eldar jetbike',30,'bike']
                            ])
        self.powers = self.opt_options_list('Powers',[
                        ['Doom',25,'d'],
                        ['Eldritch storm',20,'est'],
                        ['Fortune',30,'f'],
                        ['Guide',20,'gd'],
                        ['Mind war',20,'mw']
                            ],4)
    def check_rules(self):
        if len(self.powers.get_all()) < 1:
            self.error('You must take at least one psychic power.')
        Unit.check_rules(self)


class Warlocks(Unit):
    name = 'Warlocks'

    class Warlock(ListSubUnit):
        base_points = 25
        max = 10
        gear = ['Rune armour','Shuriken pistol']
        name = 'Warlock'
        def __init__(self):
            ListSubUnit.__init__(self)
            self.biker = False
            self.wep = self.opt_one_of('Weapon',[
                ['Witchblade',0,'wsw'],
                ['Singing spear',3,'ssp']
            ])
            self.opt = self.opt_options_list('Options',[
                ['Spiritseer',6,'seer']
            ])
            self.powers = self.opt_options_list('Powers',[
                ['Conceal',15,'con'],
                ['Destructor',10,'dtr'],
                ['Embolden',5,'emb'],
                ['Enhance',15,'enh']
            ],1)

        def check_rules(self):
            self.name = "Spiritseer" if self.opt.get('seer') else "Warlock"
            single_points = self.build_points(exclude=[self.count.id], count=1) + (20 if self.biker else 0)
            self.set_points(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id])
            if self.biker:
                self.description.add('Eldar Jetbike')

    def __init__(self):
        Unit.__init__(self)
        self.warlocks = self.opt_units_list(self.Warlock.name,self.Warlock,3,10)
        self.transport = self.opt_options_list('Transport',[
            ['Eldar Jetbikes',20,'bike'],
            [WaveSerpent.name, WaveSerpent.base_points, 'ws']
        ], limit=1)
        self.ws = self.opt_sub_unit(WaveSerpent())

    def check_rules(self):
        self.ws.set_active(self.transport.get('ws'))
        for w in self.warlocks.get_units():
            w.biker = self.transport.get('bike')
            w.check_rules()
        self.set_points(self.build_points(exclude=[self.transport.id]))
        self.build_description(exclude=[self.transport.id])

class Warlock(Unit):
    name = 'Warlock'
    gear = ['Rune armour','Shuriken pistol']
    base_points = 25
    def __init__(self, biker = False):
        Unit.__init__(self)
        self.biker = biker
        self.wep = self.opt_one_of('Weapon',[
            ['Witchblade',0,'wsw'],
            ['Singing spear',3,'ssp']
        ])
        self.opt = self.opt_options_list('Options',[
            ['Spiritseer',6,'seer']
        ])
        self.powers = self.opt_options_list('Powers',[
            ['Conceal',15,'con'],
            ['Destructor',10,'dtr'],
            ['Embolden',5,'emb'],
            ['Enhance',15,'enh']
        ],1)

    def check_rules(self):
        self.name = "Spiritseer" if self.opt.get('seer') else "Warlock"
        self.set_points(self.build_points() + (20 if self.biker else 0))
        self.build_description()
        if self.biker:
            self.description.add('Eldar Jetbike')

