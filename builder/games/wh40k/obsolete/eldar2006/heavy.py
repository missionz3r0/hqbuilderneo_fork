
__author__ = 'Ivan Truskov'

from .transport import WaveSerpent
from .hq import Warlock
from builder.core.unit import Unit, ListSubUnit, ListUnit

class Battery(Unit):
    name = 'Support Weapon Battery'
    base_points = 20
    min = 1
    max = 3

    def __init__(self):
        Unit.__init__(self)
        self.guns = self.opt_count('Support weapon', self.min, self.max,20)
        self.type = self.opt_one_of('Cannon type',[
                            ['Shadow weaver',10,'shw'],
                            ['D-cannon',30,'dcan'],
                            ['Vibro-cannon',30,'vcan']
                                ])
        self.leader = self.opt_optional_sub_unit(Warlock.name, Warlock())
    def check_rules(self):
        self.set_points(self.build_points(count = self.guns.get(), exclude = [self.leader.id,self.guns.id]) + self.leader.points())
        self.build_description(exclude=[self.guns.id], count = self.guns.get())
        self.description.add('Shuriken catapult', self.guns.get() * 2)

    def get_count(self):
        return self.guns.get() * 3 + self.leader.get_count()

class Reapers(Unit):
    name = 'Dark Reapers'
    gear = ['Reaper launcher']
    min = 3
    max = 5

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 35 + 12
        def __init__(self):
            Unit.__init__(self)

            self.wep = self.opt_one_of('Weapon',[
                ['Reaper launcher',0,'rlnch'],
                ['Shuriken cannon',0,'shcan'],
                ['Eldar missile launcher',10,'eml'],
                ['Tempest launcher',20,'tlnch']
            ])

            self.opt = self.opt_options_list('Warrior powers',[
                ['Crack shot', 10, 'cocs'],
                ['Fast shot', 20, 'speedz']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Dark Reaper', self.min, self.max,35)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()

class Wraithlord(Unit):
    name = 'Wraithlord'
    base_points = 90
    def __init__(self):
        Unit.__init__(self)
        free = [
            ['Shuriken catapult', 0, 'sc'],
            ['Flamer',0,'flame'],
        ]
        heavy = [
            ['Shuriken cannon',10,'shcan'],
            ['Scatter laser',20,'scat'],
            ['Eldar missile launcher',25,'eml'],
            ['Starcannon',30,'stcan'],
            ['Bright lance',40,'blance']
        ]
        self.free1 = self.opt_one_of('Weapon',free)
        self.free2 = self.opt_one_of('',free)
        self.costy = self.opt_one_of('',[['Wraithsword',10,'wsw']] + heavy)
        self.sec = self.opt_options_list('', heavy, limit=1)

    def check_rules(self):
        if len(self.sec.get_all()) > 0 and self.costy.get_cur() in self.sec.get_all():
            self.set_points(self.build_points(count=1))
            self.build_description(count=1, exclude=[self.costy.id, self.sec.id])
            self.description.add('Twin-linked ' + self.costy.get_selected())
        else:
            Unit.check_rules(self)
            
class WalkerSquadron(ListUnit):
    name = 'War Walker Squadron'
    class WarWalker(ListSubUnit):
        min = 1
        max = 3
        name = 'War Walker'
        base_points = 30
        def stone(self,val):
            if val:
                self.gear = ['Spirit stones']
                self.base_points = 35
            else:
                self.gear = []
                self.base_points = 30
        def __init__(self):
            ListSubUnit.__init__(self)
            self.left = self.opt_one_of('Weapon',[
                ['Shuriken cannon',5,'shcan'],
                ['Scatter laser',15,'sclas'],
                ['Eldar missile launcher',20,'eml'],
                ['Starcannon',25,'scan'],
                ['Bright lances',30,'blance']
            ])
            self.right = self.opt_one_of('',[
                ['Shuriken cannon',5,'shcan'],
                ['Scatter laser',15,'sclas'],
                ['Eldar missile launcher',20,'eml'],
                ['Starcannon',25,'scan'],
                ['Bright lances',30,'blance']
            ])
    unit_class = WarWalker
    def __init__(self):
        ListUnit.__init__(self)
        self.opt = self.opt_options_list('Upgrade entire squadron with',[['Spirit stones',5,'stone']])

    def check_rules(self):
        self.units.update_range()
        for w in self.units.get_units():
            w.stone(self.opt.get('stone'))
        self.set_points(self.build_points(count=1, exclude=[self.opt.id]))
        self.build_description(count=1, exclude=[self.opt.id])

class Falkon(Unit):
    name = 'Falcon'
    base_points = 115
    gear = ['Pulse laser']
    def __init__(self):
        Unit.__init__(self)
        self.costy = self.opt_one_of('Weapon',[
                        ['Shuriken cannon',5,'shcan'],
                        ['Scatter laser',15,'sclas'],
                        ['Eldar missile launcher',20,'eml'],
                        ['Starcannon',25,'scan'],
                        ['Bright lances',30,'blance']
                            ])
        self.free = self.opt_one_of('',[
                        ['Twin-linked shuriken catapults',0,'tlshc'],
                        ['Shuriken cannon',10,'shcan']
                            ])
        self.opt = self.opt_options_list('Options',[
                        ['Vectored engines',20,'veng'],
                        ['Star engines',15,'seng'],
                        ['Spirit stones',10,'spst'],
                        ['Holo-fields',35,'holo']
                            ])
                            
class FirePrism(Unit):
    name = 'Fire prism'
    base_points = 115
    gear = ['Prism cannon']
    def __init__(self):
        Unit.__init__(self)
        self.free = self.opt_one_of('Weapon',[
                        ['Twin-linked shuriken catapults',0,'tlshc'],
                        ['Shuriken cannon',10,'shcan']
                            ])
        self.opt = self.opt_options_list('Options',[
                        ['Vectored engines',20,'veng'],
                        ['Star engines',15,'seng'],
                        ['Spirit stones',10,'spst'],
                        ['Holo-fields',35,'holo']
                            ])