from builder.core.unit import Unit

__author__ = 'Denis Romanov'

class WaveSerpent(Unit):
    name = 'Wave Serpent'
    base_points = 90
    def __init__(self):
        Unit.__init__(self)
        self.costy = self.opt_one_of('Primary weapon',[
                        ['Twin-linked shuriken cannons',10,'tlshcan'],
                        ['Twin-linked scatter lasers',25,'tlsclas'],
                        ['Twin-linked Eldar missile launchers',30,'tleml'],
                        ['Twin-linked starcannons',35,'tlscan'],
                        ['Twin-linked bright lances',45,'tlblance']
                            ])
        self.free = self.opt_one_of('Secondary weapon',[
                        ['Twin-linked shuriken catapults',0,'tlshc'],
                        ['Shuriken cannon',10,'shcan']
                            ])
        self.opt = self.opt_options_list('Options',[
                        ['Vectored engines',20,'veng'],
                        ['Star engines',15,'seng'],
                        ['Spirit stones',10,'spst']
                            ])