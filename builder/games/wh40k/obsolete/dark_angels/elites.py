__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.dark_angels.transport import Rhino, Razorback, DropPod
from builder.games.wh40k.obsolete.dark_angels.heavy import LandRaider, LRCrusader, LRRedeemer
from functools import reduce


class DeathwingTerminatorSquad(Unit):
    name = 'Deathwing Terminator Squad'

    class Terminator(ListSubUnit):
        name = 'Deathwing Terminator'
        base_points = 44
        gear = ['Terminator armour']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=9)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Power fist', 0],
                ['Chainfist', 5]
            ])
            self.wep2 = self.opt_one_of('', [
                ['Storm bolter', 0, 'bolt'],
                ['Heavy flamer', 10, 'hf'],
                ['Plasma Cannon', 15, 'pc'],
                ['Assault Cannon', 20, 'ac'],
            ])
            self.spec = ['hf', 'pc', 'ac']
            self.both = self.opt_options_list('', [
                ['Pair of lightning claws', 0, 'lc'],
                ['Thunder hammer and storm shield', 5, 'thss'],
            ], limit=1)
            self.cyc = self.opt_options_list('', [
                ['Cyclone missile launcher', 25, 'cyc']
            ])

        def has_spec_wep(self):
            return self.count.get() if self.wep2.get_cur() in self.spec or self.cyc.get('cyc') else 0

        def check_rules(self):
            self.wep1.set_visible(self.both.get_all() == [])
            self.wep2.set_visible(self.both.get_all() == [])
            self.cyc.set_active_options(['cyc'], self.wep2.get_cur() not in self.spec)
            self.wep2.set_active_options(self.spec, not self.cyc.get('cyc'))
            ListSubUnit.check_rules(self)

    class TerminatorSergeant(Unit):
        name = "Deathwing Terminator Sergeant"
        base_points = 220 - 44 * 4

        def __init__(self):
            Unit.__init__(self)
            self.both = self.opt_options_list('', [
                ['Pair of lightning claws', 0, 'lc'],
                ['Thunder hammer and storm shield', 5, 'thss'],
            ], limit=1)

        def check_rules(self):
            self.gear = ["Terminator Armour"]
            if not self.both.get_all():
                self.gear += ['Storm bolter', 'Power sword']
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.TerminatorSergeant())
        self.terms = self.opt_units_list(self.Terminator.name, self.Terminator, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(deathwing=True), LRCrusader(deathwing=True),
                                                                  LRRedeemer(deathwing=True)])

    def check_rules(self):
        self.terms.update_range()
        spec_limit = 1 if self.terms.get_count() < 9 else 2
        spec_total = reduce(lambda val, u: u.has_spec_wep() + val, self.terms.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Deathwing Terminators may take only {0} special weapon (taken {1})'.format(spec_limit,
                                                                                                   spec_total))
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.terms.get_count() + 1


class DeathwingKnights(Unit):
    name = 'Deathwing Knights'
    gear = ['Terminator armour','Mace of absolution','Storm shield']

    class TerminatorSergeant(StaticUnit):
        name = "Knight master"
        base_points = 235 - 4 * 46
        gear = ['Terminator armour','Flail of the Unforgiven','Storm shield']
        static = True

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.TerminatorSergeant()
        self.terms = self.opt_count('Deathwing Knight', 4, 9, 46)
        self.relic = self.opt_options_list('Relic', [['Perfidious Relic of the Unforgiven', 10, 'pru']])
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(deathwing=True), LRCrusader(deathwing=True),
                                                                  LRRedeemer(deathwing=True)])

    def check_rules(self):
        self.set_points(self.build_points(base_points=self.TerminatorSergeant.base_points, count=1))
        self.build_description(exclude=[self.relic.id, self.terms.id], count=self.terms.get())
        self.description.add(self.relic.get_selected())
        self.description.add(self.leader.get_description())

    def get_count(self):
        return self.terms.get() + 1



class CompanyVeteransSquad(Unit):
    name = 'Company Veterans Squad'
    weapon = [
        ['Boltgun', 0, 'bgun' ],
        ['Chainsword',0,'chsw'],
        ['Storm bolter', 5, 'sbgun'],
        ['Combi-melta', 10, 'cmelta' ],
        ['Combi-flamer', 10, 'cflame' ],
        ['Combi-plasma', 10, 'cplasma' ],
        ['Plasma pistol',15,'ppist'],
        ['Power weapon',15,'psw'],
        ['Lightning claw',15,'lclaw'],
        ['Power fist',25,'pfist'],
        ['Pair of Lightning claws',30,'plclaw'],
        ]
    options = [
        ["Combat shield", 5, 'csh'],
        ["Melta bombs", 5, 'mbomb'],
        ["Storm shield", 10, 'ssh'],
        ]
    special = [
        ['Flamer', 5, 'flame' ],
        ['Meltagun', 10, 'mgun' ],
        ['Plasmagun', 15, 'pgun' ],
        ]
    spec_id = [w[2] for w in special]
    heavy = [
        ['Heavy bolter', 10, 'hbgun' ],
        ['Multi-melta', 10, 'mulmgun' ],
        ['Missile launcher', 15, 'mlaunch' ],
        ['Plasma cannon', 15, 'pcannon' ],
        ['Lascannon', 20, 'lcannon']
    ]
    heavy_id = [w[2] for w in heavy]

    class Sergeant(Unit):
        name = 'Veteran Sergeant'
        base_points = 90 - 4 * 18
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Bolt pistol']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', CompanyVeteransSquad.weapon)
            self.opt = self.opt_options_list('Options', CompanyVeteransSquad.options)

    class Veteran(ListSubUnit):
        name = "Veteran"
        base_points = 18
        gear = ['Power armor', 'Frag grenades', 'Krak grenades','Bolt pistol']
        max = 9
        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', CompanyVeteransSquad.weapon + CompanyVeteransSquad.special +
                                                  CompanyVeteransSquad.heavy)
            self.flakk = self.opt_options_list('', [['Flakk missiles', 10]])
            self.opt = self.opt_options_list('Options', CompanyVeteransSquad.options)

        def has_special(self):
            return self.get_count() if self.wep.get_cur() in CompanyVeteransSquad.spec_id else 0

        def has_heavy(self):
            return self.get_count() if self.wep.get_cur() in CompanyVeteransSquad.heavy_id else 0

        def check_rules(self):
            self.flakk.set_visible(self.wep.get_cur() == 'mlaunch')
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.vets = self.opt_units_list(self.Veteran.name, self.Veteran, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        self.vets.update_range()
        spec_limit = int(self.get_count() / 5)
        spec_total = reduce(lambda val, u: u.has_special() + val, self.vets.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon is over limit ({0})'.format(spec_limit))
        heavy_total = reduce(lambda val, u: u.has_heavy() + val, self.vets.get_units(), 0)
        if heavy_total > 1:
            self.error('Heavy weapon is over limit (1)')

        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.vets.get_count() + self.sergeant.get_count()


class Dred(Unit):
    name = 'Dreadnought'
    base_points = 100
    gear = ['Smoke launchers','Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon',[
            ['Multi-melta',0,'mmelta'],
            ['Twin-linked heavy flamer',5,'tlhflame'],
            ['Twin-linked heavy bolter',5,'tlhbgun'],
            ['Twin-linked autocannon',5,'tlacan'],
            ['Plasma cannon',10,'pcan'],
            ['Assault cannon',20,'asscan'],
            ['Twin-linked lascannon',25,'tllcan']
        ])
        self.wep2 = self.opt_one_of('',[
            ['Power fist',0,'dccw'],
            ['Missile launcher',10,'mlnch'],
            ['Twin-linked autocannon',15,'tlacan'],
        ])
        self.bin = self.opt_one_of('',[
            ['Built-in Storm bolter',0,'sbgun'],
            ['Built-in Heavy flamer',10,'hflame']
        ])
        self.opt = self.opt_options_list('Options',[
            ["Extra armour", 15, 'earmr']
        ])
        self.ven = self.opt_options_list('',[
            ["Venerable Dreadnought", 25, 'ven']
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.bin.set_visible(self.wep2.get_cur() == 'dccw')
        self.set_points(self.build_points())
        if self.ven.get('ven'):
            self.build_description(name='Venerable Dreadnought', add=['Deathwing vehicle'], exclude=[self.ven.id])
        else:
            self.build_description()
