__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.dark_angels.hq import *
from builder.games.wh40k.obsolete.dark_angels.elites import *
from builder.games.wh40k.obsolete.dark_angels.troops import *
from builder.games.wh40k.obsolete.dark_angels.fast import *
from builder.games.wh40k.obsolete.dark_angels.heavy import *
from functools import reduce


class DarkAngels(LegacyWh40k):
    army_id = '91f97db800b34fae968e46d165c5428e'
    army_name = 'Dark Angels'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[
                Azrael, Ezekiel, Asmodai, Belial, Sammael, CompanyMaster, InterrogatorChaplain, Chaplain, Librarian,
                {'unit': Techmarine, 'active': False},
                {'unit': Servitors, 'active': False},
                {'unit': CommandSquad_v2, 'active': False, 'type_id': 'command_squad_v2'},
                {'unit': DeathwingCommandSquad_v2, 'active': False, 'type_id': 'dw_squad_v2'},
                {'unit': RavenwingCommandSquad_v2, 'active': False, 'type_id': 'rw_squad_v2'},
                {'unit': CommandSquad, 'active': False, 'deprecated': True},
                {'unit': DeathwingCommandSquad, 'active': False, 'deprecated': True},
                {'unit': RavenwingCommandSquad, 'active': False, 'deprecated': True}
            ],
            elites=[CompanyVeteransSquad, DeathwingTerminatorSquad, DeathwingKnights, Dred],
            troops=[
                TacticalSquad, ScoutSquad,
                {'unit': DeathwingTerminatorSquad, 'active': False},
                {'unit': RavenwingAttackSquadron, 'active': False}
            ],
            fast=[RavenwingAttackSquadron, RavenwingSupportSquadron, RavenwingBlackKnights, RavenwingDarkShroud,
                  AssaultSquad, NephilimJetfighter, RavenwingDarkTalon],
            heavy=[Devastators, Predator, Whirlwind, Vindicator, LandRaider, LRCrusader, LRRedeemer,
                   LandSpeederVengeance],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.DA)
        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_units([Techmarine, Servitors, CommandSquad_v2,
                                                                   DeathwingCommandSquad_v2, RavenwingCommandSquad_v2])
            return self.hq.min <= count <= self.hq.max

        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        term = self.hq.count_units([Azrael, Belial])
        self.troops.set_active_types([DeathwingTerminatorSquad], term > 0)
        if term == 0 and self.troops.count_units([DeathwingTerminatorSquad]) > 0:
            self.error("You can't have Deathwing Terminator Squad in troops without Asrael or Belial in HQ.")

        raven = self.hq.count_units([Azrael, Sammael])
        self.troops.set_active_types([RavenwingAttackSquadron], raven > 0)
        if raven == 0 and self.troops.count_units([RavenwingAttackSquadron]) > 0:
            self.error("You can't have Ravenwing Attack Squadron in troops without Asrael or Sammael in HQ.")

        serv_units = self.hq.count_unit(Techmarine)
        self.hq.set_active_types([Servitors], serv_units > 0)
        if serv_units < self.hq.count_unit(Techmarine):
            self.error("You can only have one unit of Servitors per Techmarine.")

        command = len(self.hq.get_units()) - self.hq.count_units([Techmarine, Servitors, CommandSquad_v2,
                                                                  DeathwingCommandSquad_v2, RavenwingCommandSquad_v2])
        self.hq.set_active_types([CommandSquad_v2], command > 0)
        if command < self.hq.count_units([CommandSquad_v2]):
            self.error("You can't have more Command Squads in HQ then Commanders")
        self.hq.set_active_types([Techmarine], command > 0)
        if command < self.hq.count_units([Techmarine]):
            self.error("You can't have more Techmarine in HQ then Commanders")

        term_command = self.hq.count_unit(Belial) + reduce(lambda val, u: (val + 1) if u.has_tda() else val,
            self.hq.get_units([CompanyMaster, InterrogatorChaplain, Librarian]), 0)
        self.hq.set_active_types([DeathwingCommandSquad_v2], term_command > 0)
        if term_command < self.hq.count_units([DeathwingCommandSquad_v2]):
            self.error("You can't have more Deathwing Command Squads in HQ then Commanders in Terminator armour")

        bike_command = self.hq.count_unit(Sammael) + reduce(lambda val, u: (val + 1) if u.has_bike() else val,
            self.hq.get_units([InterrogatorChaplain, Librarian, Chaplain]), 0)
        self.hq.set_active_types([RavenwingCommandSquad_v2], bike_command > 0)
        if bike_command < self.hq.count_units([RavenwingCommandSquad_v2]):
            self.error("You can't have more Ravenwing Command Squads in HQ then Commanders on a Bike")
