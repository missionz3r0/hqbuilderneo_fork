__author__ = 'Denis Romanov'

from builder.core.unit import ListSubUnit, StaticUnit
from builder.games.wh40k.obsolete.dark_angels.transport import *
from builder.games.wh40k.obsolete.dark_angels.armory import *
from builder.games.wh40k.obsolete.dark_angels.elites import CompanyVeteransSquad
from functools import reduce


class TacticalSquad(Unit):
    name = 'Tactical Squad'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

    class Sergeant(Unit):
        base_points = 70 - 14 * 4
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun', 0, 'bolt']] + melee)
            self.wep2 = self.opt_one_of('', ranged_pistol + melee)
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine', 4, 9, 14)
        self.spec = self.opt_one_of('Special weapon',
                                    [['Boltgun', 0, 'bgun']] + CompanyVeteransSquad.special +
                                    CompanyVeteransSquad.heavy)
        self.hvy = self.opt_one_of('Heavy weapon',
                                   [['Boltgun', 0, 'bgun']] + CompanyVeteransSquad.heavy)
        self.flakk = self.opt_options_list('', [['Flakk missiles', 10]])

        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        heavy = self.get_count() == 10
        self.hvy.set_visible(heavy)
        self.spec.set_active_options([v[2] for v in CompanyVeteransSquad.heavy], not heavy)
        self.flakk.set_visible(self.hvy.get_cur() == 'mlaunch' or self.spec.get_cur() == 'mlaunch')
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.marines.get(), exclude=[self.marines.id, self.spec.id, self.hvy.id,
                                                                  self.flakk.id])
        self.description.add('Boltgun', self.marines.get() - (2 if heavy else 1))
        self.description.add(self.spec.get_selected())
        self.description.add(self.hvy.get_selected())
        self.description.add(self.flakk.get_selected())

    def get_count(self):
        return self.marines.get() + 1


class ScoutSquad(Unit):
    name = 'Scout Squad'

    class Telion(StaticUnit):
        name = 'Sergeant Telion'
        base_points = 50 + 75 - 13 * 4
        gear = ['Scout armour','Bolt pistol','Stalker Pattern Boltgun','Frag grenades','Krak grenades','Camo cloak']


    class Sergeant(Unit):
        base_points = 60 - 12 * 4
        name = 'Scout Sergeant'
        gear = ['Scout armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bolt'],
                ['Sniper rifle', 0],
                ['Shotgun', 0],
                ['Combat knife', 0],
            ] + melee)
            self.wep2 = self.opt_one_of('', ranged_pistol + melee)
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    class Scout(ListSubUnit):
        base_points = 12
        name = 'Scout'
        max = 9
        gear = ['Scout armour','Frag grenades','Krak grenades', 'Bolt pistol']

        def __init__(self):
            ListSubUnit.__init__(self)

            self.wep = self.opt_one_of('Weapon', [
                ['Boltgun', 0],
                ['Sniper rifle', 0],
                ['Shotgun', 0],
                ['Combat knife', 0],
                ['Heavy Bolter', 8, 'hb'],
                ['Missile Launcher', 15, 'ml'],
            ])
            self.flakk = self.opt_options_list('', [['Flakk missiles', 10]])

        def has_special(self):
            return (1 if self.wep.get_cur() in ['hb', 'ml'] else 0) * self.count.get()

        def check_rules(self):
            self.flakk.set_visible(self.wep.get_cur() == 'ml')
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.scouts = self.opt_units_list(self.Scout.name, self.Scout, 4, 9)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.opt = self.opt_options_list('Options',[["Camo cloaks", 2, 'cmcl']])

    def get_count(self):
        return self.scouts.get_count() + 1

    def check_rules(self):
        self.scouts.update_range()
        self.points.set(self.build_points(exclude=[self.opt.id], count=1) +
                        (self.scouts.get_count() + self.sergeant.get_count()) * self.opt.points())
        spec_limit = 1
        spec_total = reduce(lambda val, u: u.has_special() + val, self.scouts.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Heavy weapon is over limit ({0})'.format(spec_limit))
        self.build_description(exclude=[self.opt.id])
        self.description.add(self.opt.get_selected(), self.scouts.get_count() + self.sergeant.get_count())
