__author__ = 'Denis Romanov'

#replace bolt pislol or ccw
ranged = [
    ['Boltgun', 0, 'bg'],
    ['Storm Bolter', 5,'sb'],
    ['Combi-melta', 10, 'cmelta' ],
    ['Combi-flamer', 10, 'cflame' ],
    ['Combi-plasma', 10, 'cplasma' ],
    ['Plasma pistol',15,'pp'],
]

ranged_pistol = [['Bolt pistol', 0, 'bp']] + ranged
ranged_ccw = [['Close combat weapon', 0, 'ccw']] + ranged

#replace any
melee = [
    ['Chainsword', 0, 'csw'],
    ['Lightning claw',15,'claw'],
    ['Power weapon',15,'pw'],
    ['Power fist',25,'pfist'],
    ['Thunder hammer',30,'ham']
]

tda_weapon = [
    ['Storm Bolter', 0,'sb'],
    ['Combi-melta', 6, 'cmelta' ],
    ['Combi-flamer', 6, 'cflame' ],
    ['Combi-plasma', 6, 'cplasma' ],
]

tda_power = [
    ['Power sword', 0, 'psw'],
    ]

spec = [
    ['Auspex',5,'ax'],
    ['Combat shield',5,'csh'],
    ['Infravisor',5,'iv'],
    ['Melta bombs',5,'mbomb'],
    ['Digital weapons',10,'digw'],
    ['Porta-rack', 10,'pr'],
    ['Conversion field', 15, 'cf'],
    ['Displacer field', 25, 'df'],
    ['Power field generator', 30, 'pfg'],
]

spec_bike = [['Space Marine Bike', 20, 'bike']]
spec_jump = [['Jump pack', 15, 'jpack']]

relic_weapon = [
    ['Foe-smiter', 20, 'fs'],
    ['Lion\'s Roar', 20, 'lr'],
    ['Mace or Redemption', 30, 'mor'],
    ['Monster Slayer of Caliban', 45, 'msoc'],
]

relic_gear = [
    ['Shroud of heroes', 50, 'shroud'],
]

standard = [
    ['Standard of Retribution', 45, 'sor'],
    ['Standard of Devastation', 65, 'sod'],
    ['Standard of Fortitude', 85, 'sof'],
]

vehicle = [
    ["Storm bolter", 5, 'sbgun'],
    ["Dozer blade", 5, 'dblade'],
    ["Hunter-killer missile", 10, 'hkm'],
    ["Extra armour", 10, 'exarm']
]
