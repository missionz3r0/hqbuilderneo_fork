__author__ = 'maria'

from builder.core2 import *
from .armory import *
from .troops import Transport


class Pathfinders(Unit):
    type_name = 'Pathfinder Team'
    type_id = 'pathfinderteam_v1'

    model_points = 11
    model_gear = [Gear('Recon armour'), Gear('Photon grenades')]
    model_min = 4
    model_max = 10
    model_name = 'Pathfinder'
    model = UnitDescription(model_name, points=model_points, options=model_gear)

    class ModelCount(Count):
        @property
        def description(self):
            return [Pathfinders.model.clone().add([Gear('Pulse carbine'), Gear('Markerlight')]).
                    set_count(self.cur - self.parent.ionrifle.cur - self.parent.railrifle.cur)]

    def __init__(self, parent):
        super(Pathfinders, self).__init__(parent=parent, points=0, gear=[])
        self.leader = self.Leader(self)
        self.pathfinder = self.ModelCount(self, self.model_name, min_limit=self.model_min, max_limit=self.model_max,
                                          points=self.model_points)
        self.ionrifle = Count(self, 'Ion rifle', min_limit=0, max_limit=3, points=10,
                              gear=self.model.clone().add(Gear('Ion rifle')).add_points(10))
        self.railrifle = Count(self, 'Rail rifle', min_limit=0, max_limit=3, points=15,
                               gear=self.model.clone().add(Gear('Rail rifle')).add_points(15))
        self.Options(self)
        self.drones = self.Drones(self)
        Transport(self)

    class Options(Ritual):
        def __init__(self, parent):
            super(Pathfinders.Options, self).__init__(parent=parent)
            self.empgrenades = self.variant('EMP grenades', 2)

    class Drones(OptionsList):
        def __init__(self, parent):
            super(Pathfinders.Drones, self).__init__(parent=parent, name='Drones', limit=None)
            self.recondrone = self.variant('Recon Drone', 28)
            self.gravinhibitordrone = self.variant('Grav-inhibitor Drone', 15)
            self.pulseacceleratordrone = self.variant('Pulse accelerator Drone', 15)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Pathfinders.Leader, self).__init__(parent=parent, name='')
            self.shasui = SubUnit(self, Pathfinders.Shasui())

    class Shasui(Unit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent=None):
            super(Pathfinders.Shasui, self).__init__(parent=parent, points=Pathfinders.model_points + 10,
                                                     name=self.type_name, gear=Pathfinders.model_gear)
            self.weapon = self.Weapon(self)
            self.opt = self.Options(self)
            self.drones = Drones(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Pathfinders.Shasui.Weapon, self).__init__(parent=parent, name='Weapon')
                self.pulsecarbine = self.variant('Pulse carbine', 0, gear=[Gear('Pulse carbine'), Gear('Markerlight')])
                self.ion = self.variant('Ion rifle', 10)
                self.rail = self.variant('Rail rifle', 15)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Pathfinders.Shasui.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.markerlightandtargetlock = self.variant('Blacksun filter', 1)

        def check_rules(self):
            self.drones.check_rules()

        def has_rifles(self):
            return self.weapon.cur != self.weapon.pulsecarbine

    def check_rules(self):
        super(Pathfinders, self).check_rules()
        self.pathfinder.min = self.model_min - self.leader.count
        self.pathfinder.max = self.model_max - self.leader.count
        rifle_limit = 3 - (1 if self.leader.shasui.unit.has_rifles() and self.leader.count else 0)
        Count.norm_counts(0, rifle_limit, [self.ionrifle, self.railrifle])

    def get_count(self):
        return self.pathfinder.cur + self.leader.count


class GunDrones(Unit):
    type_name = 'Drone squadron'
    type_id = 'dronesquadron_v1'

    def __init__(self, parent):
        super(GunDrones, self).__init__(parent=parent)
        self.drones = [
            Count(self, 'Gun Drone', min_limit=4, max_limit=12, points=14),
            Count(self, 'Marker Drone', min_limit=0, max_limit=12, points=14),
            Count(self, 'Shield Drone', min_limit=0, max_limit=12, points=14)
        ]

    def check_rules(self):
        Count.norm_counts(4, 12, self.drones)

    def get_count(self):
        return sum(o.cur for o in self.drones)


class PiranhaTeam(Unit):
    type_name = 'Piranha Team'
    type_id = 'piranha_team_v1'

    class Piranha(ListSubUnit):

        def __init__(self, parent):
            super(PiranhaTeam.Piranha, self).__init__(parent, name='Piranha', points=40,
                                                      gear=[Gear('Gun Drone', count=2)])
            self.Weapon(self)
            Count(self, 'Seeker missile', min_limit=0, max_limit=2, points=8)
            Vehicle(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(PiranhaTeam.Piranha.Weapon, self).__init__(parent=parent, name='Weapon')
                self.variant('Burst cannon', 0)
                self.variant('Fusion blaster', 10)

    def __init__(self, parent):
        super(PiranhaTeam, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Piranha, min_limit=1, max_limit=5)

    def get_count(self):
        return self.models.count


class Vespids(Unit):
    type_name = 'Vespid Stingwings'
    type_id = 'vespidstingwings_v1'
    model_points = 18
    model_gear = [Gear('Neutron blaster'), Gear('Combat armour')]

    def __init__(self, parent):
        super(Vespids, self).__init__(parent=parent)
        self.leader = self.Leader(self)
        self.vespidstingwing = Count(
            self, 'Vespid Stingwing', min_limit=4, max_limit=12, points=self.model_points,
            gear=UnitDescription(name='Vespid Stingwing', points=Vespids.model_points, options=Vespids.model_gear)
        )

    class Leader(OptionsList):
        def __init__(self, parent):
            super(Vespids.Leader, self).__init__(parent=parent, name='')
            self.vespidstainleader = self.variant(
                'Vespid Stain Leader', Vespids.model_points + 10,
                gear=UnitDescription(name='Vespid Stain Leader', points=Vespids.model_points+10,
                                     options=Vespids.model_gear)
            )
    def get_count(self):
        return self.vespidstingwing.cur + self.leader.vespidstainleader.value


class SunShark(Unit):
    type_name = 'Sun Shark Bomber'
    type_id = 'sunsharkbomber_v1'

    def __init__(self, parent):
        super(SunShark, self).__init__(
            parent=parent, points=160,
            gear=[Gear('Pulse bomb generator'), Gear('Networked markerlight'), Gear('Seeker missile', count=2),
                  UnitDescription(name='Interceptor Drone', options=[Gear('Twin-linked ion rifle')], count=2)]
        )
        self.Weapon(self)
        Vehicle(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SunShark.Weapon, self).__init__(parent=parent, name='Weapon')
            self.missilepod = self.variant('Missile pod', 0)
            self.twinlinkedmissilepod = self.variant('Twin-linked missile pod', 5)


class RazorShark(Unit):
    type_name = 'Razorshark Strike Fighter'
    type_id = 'razorsharkstrikefighter_v1'

    def __init__(self, parent):
        super(RazorShark, self).__init__(
            parent=parent, points=145,
            gear=[Gear('Quad ion turret'), Gear('Seeker missile'), Gear('Seeker missile')]
        )
        self.Weapon(self)
        Vehicle(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RazorShark.Weapon, self).__init__(parent=parent, name='Weapon')
            self.burstcannon = self.variant('Burst cannon', 0)
            self.missilepod = self.variant('Missile pod', 5)
