__author__ = 'Denis Romanov'

from builder.core2 import *
from .armory import *


class Devilfish(Unit):
    type_name = 'Devilfish'
    type_id = 'devilfish_v1'

    def __init__(self, parent=None):
        super(Devilfish, self).__init__(parent=parent, points=80, gear=[Gear('Burst cannon'), ])
        self.Weapon(self)
        Vehicle(self)
        Count(self, 'Seeker missile', min_limit=0, max_limit=2, points=8)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Devilfish.Weapon, self).__init__(parent=parent, name='Weapon')
            self.twogundrones = self.variant('Two Gun Drones', 0, gear=[Gear('Gun Drone', count=2)])
            self.twinlinkedsmartmissilesystem = self.variant('Twin-linked smart missile system', 10)


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(parent=parent, name='Transport', limit=1)
        SubUnit(self, Devilfish())


class FireWarriors(Unit):
    type_name = 'Fire Warrior Team'
    type_id = 'firewarriorteam_v1'

    model_points = 9
    model_gear = [Gear('Combat armour'), Gear('Photon grenades')]
    model_min = 6
    model_max = 12
    model_name = 'Fire Warrior'
    model = UnitDescription(model_name, points=model_points, options=model_gear)

    class ModelCount(Count):
        @property
        def description(self):
            return [FireWarriors.model.clone().add(Gear('Pulse rifle')).set_count(self.cur - self.parent.carbine.cur)]

    def __init__(self, parent):
        super(FireWarriors, self).__init__(parent=parent)
        self.leader = self.Leader(self)
        self.warrior = self.ModelCount(self, self.model_name, min_limit=self.model_min, max_limit=self.model_max,
                                       points=FireWarriors.model_points)
        self.carbine = Count(self, 'Pulse carbine', min_limit=0, max_limit=self.model_min, points=0,
                             gear=self.model.clone().add(Gear('Pulse carbine')))
        self.Options(self)
        Transport(self)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(FireWarriors.Leader, self).__init__(parent=parent, name='')
            SubUnit(self, FireWarriors.Shasui())

    class Shasui(Unit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent=None):
            super(FireWarriors.Shasui, self).__init__(parent=parent, points=FireWarriors.model_points + 10,
                                                      gear=FireWarriors.model_gear)
            self.Weapon(self)
            self.Options(self)
            self.drones = Drones(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(FireWarriors.Shasui.Weapon, self).__init__(parent=parent, name='Weapon')
                self.pulserifle = self.variant('Pulse rifle', 0)
                self.pulsecarbine = self.variant('Pulse carbine', 0)

        class Options(OptionsList):
            def __init__(self, parent):
                super(FireWarriors.Shasui.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.variant('Markerlight and target lock', 15, gear=[Gear('Markerlight'), Gear('Target lock')])

        def check_rules(self):
            self.drones.check_rules()

    class Options(Ritual):
        def __init__(self, parent):
            super(FireWarriors.Options, self).__init__(parent=parent)
            self.variant('EMP grenades', 2, per_model=True)

    def check_rules(self):
        super(FireWarriors, self).check_rules()
        self.warrior.min = self.model_min - self.leader.count
        self.warrior.max = self.model_max - self.leader.count
        self.carbine.max = self.warrior.cur

    def get_count(self):
        return self.warrior.cur + self.leader.count


class KrootSquad(Unit):
    type_name = 'Kroot Carnivore Squad'
    type_id = 'krootcarnivoresquad_v1'

    model_points = 6
    model_gear = [Gear('Kroot armour')]

    def __init__(self, parent):
        super(KrootSquad, self).__init__(parent=parent)

        self.leader = self.Leader(self)
        self.kroot = Count(
            self, 'Kroot', min_limit=10, max_limit=20, points=self.model_points,
            gear=UnitDescription('Kroot', self.model_points, options=self.model_gear + [Gear('Kroot rifle')])
        )
        self.krootoxrider = Count(
            self, 'Krootox Rider', min_limit=0, max_limit=3, points=25,
            gear=UnitDescription('Krootox Rider', 25, options=self.model_gear + [Gear('Kroot gun')])
        )
        self.kroothound = Count(
            self, 'Kroot Hound', min_limit=0, max_limit=10, points=5, gear=UnitDescription('Kroot Hound', 5)
        )
        self.Options(self)

    @property
    def rifles(self):
        return self.kroot.cur + self.leader.count_rifle()

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(KrootSquad.Leader, self).__init__(parent=parent, name='')
            self.leader = SubUnit(self, KrootSquad.Shaper())

        def count_rifle(self):
            return self.leader.unit.weapon.count_rifle() and self.leader.used

    class Shaper(Unit):
        type_name = 'Shaper'
        type_id = 'shaper_v1'

        def __init__(self, parent=None):
            super(KrootSquad.Shaper, self).__init__(parent=parent, points=KrootSquad.model_points+15,
                                                    gear=KrootSquad.model_gear)
            self.weapon = self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(KrootSquad.Shaper.Weapon, self).__init__(parent=parent, name='Weapon')

                self.krootrifle = self.variant('Kroot rifle', 0)
                self.pulserifle = self.variant('Pulse rifle', 4)
                self.pulsecarbine = self.variant('Pulse carbine', 4)

            def count_rifle(self):
                return self.cur == self.krootrifle

    class Options(OptionsList):
        def __init__(self, parent):
            super(KrootSquad.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.sniperrounds = self.variant('Sniper rounds', 1, per_model=True)

        @property
        def points(self):
            return super(KrootSquad.Options, self).points * self.parent.rifles
