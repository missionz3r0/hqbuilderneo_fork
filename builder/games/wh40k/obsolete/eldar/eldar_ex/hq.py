__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit


class Irillyth(StaticUnit):
    name = "Irillyth, Shade of Twilight"
    base_points = 220
    gear = ['Spear of Starlight', 'Shadow Spectre jet pack', 'Spectre Holo-fields', 'Haywire grenade']


class BelAnnath(StaticUnit):
    name = "Bel-Annath"
    base_points = 175
    gear = ['Ghosthelm', 'Shuriken Pistol', 'Rune Armour', 'Runes of Witnessing', 'Spirit Stones', 'The Sundered Spear']


class Wraithseer(Unit):
    name = "Wraithseer"
    base_points = 185
    gear = ['Wraithspear', 'Wraithshield']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Bright Lance', 30, 'bl'],
            ['Scatter Laser', 10, 'scat'],
            ['Eldar Missile Launcher', 15, 'eml'],
            ['Star Cannon', 20, 'scan'],
            ['D-Cannon', 40, 'dcan']
        ], 1)
