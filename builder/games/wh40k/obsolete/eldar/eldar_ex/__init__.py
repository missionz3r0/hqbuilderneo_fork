__author__ = 'Ivan Truskov'

from builder.games.wh40k.obsolete.eldar.eldar_ex.hq import *
from builder.games.wh40k.obsolete.eldar.eldar_ex.fast import *
from builder.games.wh40k.obsolete.eldar.eldar_ex.heavy import *
from builder.games.wh40k.obsolete.eldar.eldar_ex.superheavy import *

ia_hq = [Irillyth, BelAnnath, Wraithseer]
ia_fast = [Spectres, Wasps, Hornets]
ia_heavy = [WHunter]
ia_super_heavy = [Lynx, Scorpion, Cobra, VampireRaider, VampireHunter, Revenant, Phantom]
