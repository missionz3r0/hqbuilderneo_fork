from builder.core.unit import Unit

__author__ = 'Denis Romanov'


vehicle_options = [
    ['Ghostwalk Matrix', 10, 'gwm'], 
    ['Spirit Stones', 10, 'spst'], 
    ['Holo-fields', 15, 'hf'],
    ['Star Engines', 15, 'seng'], 
    ['Vectored Engines', 15, 'veng'], 
    ['Crystal Targeting Matrix', 25, 'ctm'],
]


class WaveSerpent(Unit):
    name = 'Wave Serpent'
    base_points = 115
    gear = ['Serpent shield']

    def __init__(self):
        Unit.__init__(self)
        self.costy = self.opt_one_of('Weapon', [
            ['Twin-linked shuriken cannon', 0, 'tlshcan'], 
            ['Twin-linked bright lance', 5, 'tlblance'], 
            ['Twin-linked scatter lasers', 5, 'tlsclas'], 
            ['Twin-linked starcannon', 5, 'tlscan'], 
            ['Twin-linked Eldar missile launchers', 15, 'tleml'], 
        ])
        self.free = self.opt_one_of('', [
            ['Twin-linked shuriken catapult', 0, 'tlshc'], 
            ['Shuriken cannon', 10, 'shcan']
        ])
        self.opt = self.opt_options_list('Options', vehicle_options)