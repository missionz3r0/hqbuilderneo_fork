#
# __author__ = 'Ivan Truskov'
#
from builder.games.wh40k.obsolete.eldar.transport import WaveSerpent
from builder.core.options import norm_counts
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.eldar.elites import Dragons


class Avengers(Dragons):
    name = 'Dire Avengers'
    base_gear = ['Avenger shuriken catapult', 'Plasma grenades', 'Aspect armour']
    min_models = 5
    max_models = 10
    model_points = 13
    model_name = 'Dire Avenger'

    class Exarch(Unit):
        name = 'Dire Avenger Exarch'
        base_points = 13 + 10
        base_gear = ['Plasma grenades', 'Heavy Aspect armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Avenger shuriken catapult', 0, 'asc'],
                ['Twin-linked Avenger shuriken catapult', 5, 'tlasc'],
                ['Power weapon and shuriken pistol', 15, 'pair1'],
                ['Diresword and shuriken pistol', 20, 'pair2'],
                ['Power weapon and shimmershield', 20, 'pair3'],
            ])
            self.opt = self.opt_options_list('Exarch powers', [
                ['Disarm Strike', 10, 'cb'],
                ['Shield of Grace', 10, 'stlk'],
                ['Battle fortune', 15, 'bf'],
            ], limit=2)


class Rangers(Unit):
    name = 'Rangers'
    base_gear = ['Ranger long rifle', 'Shuriken pistol', 'Mesh armour']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Ranger', 5, 10, 12)
        self.path = self.opt_options_list('Illic\'s upgrade', [['Alaitoc Pathfinders', 13, 'path']])

    def check_rules(self):
        self.path.set_active_options(['path'], self.get_roster().has_illic())
        self.set_points(self.build_points(count=1, exclude=[self.path.id]) + self.path.points() * self.get_count())
        self.build_description(options=[self.path], count=1)
        self.description.add(ModelDescriptor('Ranger', points=12 + self.path.points(),
                                             gear=self.base_gear).build(self.get_count()))


class Guardians(Unit):
    name = 'Guardian Defenders'
    min_models = 10
    max_models = 20
    base_gear = ['Shuriken catapult', 'Mesh armour', 'Plasma grenades']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Guardian', self.min_models, self.max_models, 9)
        heavy = [
            ['Shuriken cannon', 15, 'shcan'],
            ['Scatter laser', 20, 'scat'],
            ['Starcannon', 20, 'scan'],
            ['Bright lance', 20, 'blance'],
            ['Eldar missile launcher', 30, 'eml'],
        ]
        self.heavy1 = self.opt_options_list('Heavy weapon platform', heavy, limit=1)
        self.heavy2 = self.opt_options_list('', heavy, limit=1)
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        self.heavy2.set_visible(self.get_count() == 20)
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.count.id, self.transport.id], count=1)
        self.description.add(ModelDescriptor('Guardian', points=9, gear=self.base_gear, count=self.get_count()).build())
        self.description.add(self.transport.get_selected())


class StormGuardians(Unit):
    name = 'Storm Guardians'
    min_models = 10
    max_models = 20
    base_gear = ['Mesh armour', 'Plasma grenades']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Guardian', self.min_models, self.max_models, 9)
        self.flame = self.opt_count('Flamer', 0, 2, 5)
        self.fg = self.opt_count('Fusion gun', 0, 2, 10)
        self.pwr = self.opt_count('Power sword', 0, 2, 15)
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        norm_counts(0, 2, [self.flame, self.fg])
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        d = ModelDescriptor('Guardian', points=9, gear=self.base_gear)
        self.description.add(d.clone().add_gear('Flamer', points=5).build(self.flame.get()))
        self.description.add(d.clone().add_gear('Fusion gun', points=10).build(self.fg.get()))
        d.add_gear('Shuriken pistol')
        self.description.add(d.clone().add_gear('Power sword', points=15).build(self.pwr.get()))
        self.description.add(d.add_gear('Chainsword').build(self.get_count() -
                                                            self.pwr.get() - self.flame.get() - self.fg.get()))
        self.description.add(self.transport.get_selected())


class BikerGuardians(Unit):
    name = 'Windrider Jetbike Squad'
    base_gear = ['Eldar jetbike', 'Mesh armour']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Windrider Guardian', 3, 10, 17)
        self.hvy = self.opt_count('Shuriken cannon', 0, 1, 10)

    def check_rules(self):
        self.hvy.update_range(0, int(self.count.get() / 3))
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        d = ModelDescriptor('Windrider Guardian', points=17, gear=self.base_gear)
        self.description.add(d.clone().add_gear('Shuriken cannon', points=10).build(self.hvy.get()))
        self.description.add(d.add_gear('Twin-linked shuriken catapult').build(self.get_count() - self.hvy.get()))
