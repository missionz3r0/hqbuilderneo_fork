__author__ = 'Ivan Truskov'

import math

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.eldar.hq import *
from builder.games.wh40k.obsolete.eldar.elites import *
from builder.games.wh40k.obsolete.eldar.troops import *
from builder.games.wh40k.obsolete.eldar.fast import *
from builder.games.wh40k.obsolete.eldar.heavy import *
from builder.games.wh40k.obsolete.eldar.eldar_ex import ia_fast, ia_hq, ia_super_heavy, ia_heavy, BelAnnath, Wraithseer


class Eldar(LegacyWh40k):
    army_id = '14611eaecc554dd1a0798a51aa9528e5'
    army_name = 'Eldar'
    obsolete = True

    def __init__(self, secondary=False, iyanden=False):
        if iyanden:
            spec_hq = [IyandenAutarch, IyandenFarseer, IyandenSpiritseer]
        else:
            spec_hq = [Autarch, Farseer, Spiritseer]
        LegacyWh40k.__init__(
            self,
            hq=[Eldrad, Uriel, Illic, Asurmen, JainZar, Karandras, Fuegan, Baharroth, Maugan,
                Avatar] + spec_hq + [Warlocks] + ia_hq,
            elites=[Dragons, Scorpions, Banshees, Wraithguard, Wraithblades, Harlequins],
            troops=[Guardians, StormGuardians, Avengers, BikerGuardians, Rangers,
                    dict(unit=Wraithguard, active=False), dict(unit=Wraithblades, active=False)],
            fast=[{'unit': Hunters, 'active': False, 'deprecated': True}, CrimsonHunter, VyperSquadron, Hemlock,
                  Spiders, Spears, Hawks] + ia_fast,
            heavy=[Falcon, WalkerSquadron, Battery, FirePrism, NightSpinner, Wraithlord, Wraithknight,
                   DarkReapers] + ia_heavy,
            super_heavy=ia_super_heavy,
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.ELD)
        )
        self.opt.visible = True

        if not iyanden:
            def check_hq_limit():
                count = len(self.hq.get_units()) - self.hq.count_unit(Warlocks)
                return self.hq.min <= count <= self.hq.max

            self.hq.check_limits = check_hq_limit

    def has_illic(self):
        return self.hq.count_unit(Illic) > 0

    def has_shadow(self):
        return self.opt.ia.value and self.fast.count_units([ia_fast[0]]) > 0

    def check_rules(self):
        spirit_seer = self.hq.count_units([Spiritseer, IyandenSpiritseer]) > 0
        self.troops.set_active_types([Wraithblades, Wraithguard], spirit_seer)
        self.elites.set_active_types([Wraithblades, Wraithguard], not spirit_seer)
        if not spirit_seer and self.troops.count_units([Wraithblades, Wraithguard]) > 0:
            self.error("You can't have Wraithblades and Wraithguard in Troops without Spiritseer in HQ")

        self.hq.set_active_types(ia_hq, self.opt.ia.value)
        self.fast.set_active_types(ia_fast, self.opt.ia.value)
        self.heavy.set_active_types(ia_heavy, self.opt.ia.value)
        if self.opt.ia.value:
            #IA vol. 11
            if self.hq.count_unit(BelAnnath) > 0:
                if self.hq.count_units([Farseer, Eldrad]) > 0:
                    self.error("If Farseer Bel-Annath is part of the army, no other Farseers may be included.")

            if self.hq.count_unit(Wraithseer) > 0:
                if self.elites.count_unit(Wraithguard) + self.troops.count_unit(Wraithguard) < 1:
                    self.error("If Wraithseer is part of the army, it must also incluide at least one unit of "
                               "wraithguard.")
                if len(self.hq.get_units()) - self.hq.count_units([Warlocks, Wraithseer]) <= 0:
                        self.error("You must always include another non-Wraithseer unit in HQ")


class Iyanden(Eldar):
    army_id = '4beeae1ac5ff442f8ac720f9a879966c'
    army_name = 'Eldar: Iyanden'

    def __init__(self, secondary=False):
        Eldar.__init__(self, secondary=secondary, iyanden=True)

        def check_hq_limit():
            seers = self.hq.count_unit(IyandenSpiritseer)
            seer_slots = math.ceil(float(seers) / 5.0)

            min_count = len(self.hq.get_units()) - self.hq.count_unit(Warlocks)
            max_count = min_count - seers + seer_slots
            return self.hq.min <= min_count and max_count <= self.hq.max

        self.hq.check_limits = check_hq_limit
