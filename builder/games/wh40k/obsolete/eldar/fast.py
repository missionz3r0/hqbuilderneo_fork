__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, ListUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.eldar.transport import vehicle_options


class CrimsonHunter(Unit):
    name = 'Crimson Hunter'
    base_points = 160
    gear = ['Pulse laser']

    def __init__(self):
        Unit.__init__(self)
        weapon = [
            ['Bright lance', 0, 'bl'],
            ['Starcannon', 0, 'sc']
        ]
        self.ex = self.opt_options_list('Exarch', [
            ['Crimson Hunter Exarch', 20, 'ex'],
        ])
        self.wep1 = self.opt_one_of('Weapon', weapon)
        self.wep2 = self.opt_one_of('', weapon)
        self.opt = self.opt_options_list('Exarch powers', [
            ['Night Vision', 5, 'nv'],
            ['Marksman\'s Eye', 10, 'me'],
        ])

    def check_rules(self):
        self.wep1.set_visible(self.ex.get('ex'))
        self.wep2.set_visible(self.ex.get('ex'))
        self.opt.set_visible(self.ex.get('ex'))
        self.set_points(self.build_points())
        if self.ex.get('ex'):
            self.build_description(name='Crimson Hunter Exarch', exclude=[self.ex.id])
        else:
            self.build_description(gear=self.gear + ['Bright lance' for _ in range(2)])

    def get_unique(self):
        if self.ex.get('ex'):
            return 'Crimson Hunter Exarch'


# Deprecated
class Hunters(Unit):
    name = 'Crimson Hunters'
    base_gear = ['Pulse laser'] + ['Bright lance' for _ in range(2)]
    min_models = 1
    max_models = 3
    model_points = 160
    model_name = 'Crimson Hunter'

    class Exarch(Unit):
        name = 'Crimson Hunter Exarch'
        base_points = 160 + 20
        gear = ['Pulse laser']

        def __init__(self):
            Unit.__init__(self)
            weapon = [
                ['Bright lance', 0, 'bl'], 
                ['Starcannon', 0, 'sc']
            ]
            self.wep1 = self.opt_one_of('Weapon', weapon)
            self.wep2 = self.opt_one_of('', weapon)
            self.opt = self.opt_options_list('Exarch powers', [
                ['Night Vision', 5, 'nv'], 
                ['Marksman\'s Eye', 10, 'me'], 
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count(self.model_name, self.min_models, self.max_models, self.model_points)
        self.leader = self.opt_optional_sub_unit('Exarch', self.Exarch())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min_models - ldr, self.max_models - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(options=[self.leader])
        self.description.add(ModelDescriptor(name=self.model_name, gear=self.base_gear, 
                                             points=self.model_points).build(self.warriors.get()))

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Spears(Hunters):
    name = 'Shining Spears'
    base_gear = ['Laser lance', 'Eldar Jetbike', 'Heavy Aspect armour']
    min_models = 3
    max_models = 9
    model_points = 25
    model_name = 'Shining Spear'

    class Exarch(Unit):
        name = 'Shining Spear Exarch'
        base_points = 25 + 10
        gear = ['Eldar Jetbike', 'Heavy Aspect armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('', [
                ['Laser lance', 0, 'll'],
                ['Power weapon', 0, 'pwr'],
                ['Star lance', 10, 'sl'],
            ])

            self.opt = self.opt_options_list('Exarch powers', [
                ['Monster hunter', 5, 'mh'],
                ['Disarm strike', 10, 'ds'],
                ['Hit & Run', 15, 'hr'],
            ], limit=2)


class Spiders(Hunters):
    name = 'Warp Spiders'
    base_gear = ['Warp jump generator', 'Death spinner', 'Heavy Aspect armour']
    min_models = 5
    max_models = 10
    model_points = 19
    model_name = 'Warp Spider'

    class Exarch(Unit):
        name = 'Warp Spider Exarch'
        base_points = 19 + 10
        gear = ['Warp jump generator', 'Heavy Aspect armour']

        def __init__(self):
            Unit.__init__(self)
            self.bikew = self.opt_options_list('Weapon', [
                ['Powerblades', 20, 'pbld']
            ])
            self.wep = self.opt_one_of('', [
                ['Death spinner', 0, 'dsp'],
                ['Twin-linked death spinners', 5, '2dsp'],
                ['Spinneret rifle', 15, 'sprifl']
            ])

            self.opt = self.opt_options_list('Exarch powers', [
                ['Fast shot', 10, 'fs'],
                ['Marksman\'s Eye', 10, 'me'],
                ['Stalker', 10, 'st'],
            ], limit=2)


class Hawks(Hunters):
    name = 'Swooping Hawks'
    base_gear = ['Plasma grenades', 'Haywire grenades', 'Grenade pack', 'Swooping Hawk grenade pack', 'Lasblaster',
                 'Aspect armour']
    min_models = 5
    max_models = 10
    model_points = 16
    model_name = 'Swooping Hawk'

    class Exarch(Unit):
        name = 'Swooping Hawk Exarch'
        base_points = 16 + 10
        gear = ['Plasma grenades', 'Haywire grenades', 'Grenade pack', 'Swooping Hawk grenade pack',
                'Heavy Aspect armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_options_list('Weapon', [
                ['Power weapon', 10, 'pbld']
            ])
            self.rng = self.opt_one_of('', [
                ['Lasblaster', 0, 'lblast'],
                ['Hawk\'s talon', 10, 'talon'],
                ['Sunrifle', 15, 'srifl']
            ])

            self.opt = self.opt_options_list('Exarch powers', [
                ['Night Vision', 5, 'nv'],
                ['Marksman\'s Eye', 10, 'me'],
                ['Hit & Run', 15, 'hr'],
            ], limit=2)


# class Hawks(Unit):
#     name = 'Swooping Hawks'
#     gear = ['Plasma grenades', 'Haywire grenades', 'Swooping Hawk grenade pack', 'Lasblaster']
#     min = 5
#     max = 10
#
#     class Exarch(Unit):
#         name = 'Exarch'
#         base_points = 21 + 12
#         gear = ['Plasma grenades', 'Haywire grenades', 'Swooping Hawk grenade pack']
#         def __init__(self):
#             Unit.__init__(self)
#             self.wep = self.opt_options_list('Melee weapon', [
#                 ['Power weapon', 10, 'pwep']
#             ])
#             self.rng = self.opt_one_of('Ranged weapon', [
#                 ['Lasblaster', 0, 'lblast'],
#                 ['Hawk\'s talon', 10, 'talon'],
#                 ['Sunrifle', 5, 'srifl']
#             ])
#
#             self.opt = self.opt_options_list('Warrior powers', [
#                 ['Intercept', 5, 'int'],
#                 ['Skyleap', 15, 'leap']
#             ])
#         def check_rules(self):
#             self.rng.set_active_options(['talon', 'srifl'], not self.wep.get('pwep'))
#             self.wep.set_active(self.rng.get_cur() == 'lblast')
#             Unit.check_rules(self)
#
#     def __init__(self):
#         Unit.__init__(self)
#         self.warriors = self.opt_count('Swooping Hawk', self.min, self.max, 21)
#         self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())
#
#     def check_rules(self):
#         ldr = self.leader.get_count()
#         self.warriors.update_range(self.min - ldr, self.max - ldr)
#         self.set_points(self.build_points(count=1))
#         self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])
#
#     def get_count(self):
#         return self.warriors.get() + self.leader.get_count()
#

class VyperSquadron(ListUnit):
    name = 'Vyper Squadron'

    class Vyper(ListSubUnit):
        base_points = 50
        name = 'Vyper'

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep1 = self.opt_one_of('Primary weapon', [
                ['Shuriken cannon', 0, 'shcan'], 
                ['Starcannon', 5, 'scan'],
                ['Bright lance', 10, 'blance'], 
                ['Scatter laser', 10, 'sclas'], 
                ['Eldar missile launcher', 15, 'eml'], 
            ])
            self.wep2 = self.opt_one_of('Secondary weapon', [
                ['Twin-linked shuriken catapults', 0, 'tlshc'], 
                ['Shuriken cannon', 10, 'shcan']
            ])
            self.opt = self.opt_options_list('Options', vehicle_options)

    def __init__(self):
        ListUnit.__init__(self, unit_class=self.Vyper, min_num=1, max_num=3)

    def check_rules(self):
        mtx = sum((1 for u in self.units.get_units() if u.opt.get('gwm')))
        if mtx and mtx != len(self.units.get_units()):
            self.error('Ghostwalk Matrix must be taken for all vehicles in squadron')
        ListUnit.check_rules(self)


class Hemlock(Unit):
    name = 'Hemlock Wraithfighter'
    base_points = 185
    gear = ['Heavy D-scythe' for _ in range(2)] + ['Mindshock pod', 'Spirit stones']
    static = True
