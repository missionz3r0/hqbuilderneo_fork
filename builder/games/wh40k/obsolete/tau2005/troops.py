__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit
from builder.games.wh40k.obsolete.tau2005.armory import Vehicle,Infantry,Drones
from builder.core.options import norm_counts
from functools import reduce


class Devilfish(Unit):
    name = 'Devilfish'
    base_points = 80
    gear = ['Burst cannon']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon system', [
            ['Gun drones', 0, 'gd'],
            ['Smart missile system', 20, 'sms']
        ])
        self.opt = self.opt_options_list('Options', Vehicle)
        self.mis = self.opt_count('Seeker missile', 0, 2, 10)


class FireWarriors(Unit):
    name = 'Fire Warrior team'

    class Shasui(Unit):
        name = 'Shas\'ui'
        gear = ['Pulse rifle']

        def __init__(self, base_points):
            self.base_points = base_points + 10
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options',Infantry + [['Markerlight',10,'mark']])
            self.drones = [self.opt_count(tp[0],0,2,tp[1]) for tp in Drones]

        def check_rules(self):
            has_dc = self.opt.get('dc')
            dr_cnt = 0
            for ctr in self.drones:
                ctr.set_active(has_dc)
                dr_cnt += ctr.get() if has_dc else 0
            if has_dc:
                norm_counts(0, 2, self.drones)
                if dr_cnt != 2:
                    self.error('2 drones must be taken along with controller')
            self.points.set(self.build_points(count=1))
            self.build_description(count=1)

        def full_count(self):
            return 3 if self.opt.get('dc') else 1

        def get_count(self):
            return 1 + (reduce(lambda val, n: val + n.get(), self.drones, 0) if self.opt.get('dc') else 0)

    def __init__(self, name=None, base_points=10):
        if name:
            self.name = name
        self.transport_enable = True
        Unit.__init__(self)
        self.team = self.opt_count('Shas\'la', 6, 12, base_points)
        self.carb = self.opt_count('Pulse carbines', 0, 6, 0)
        self.gren = self.opt_options_list('Grenades',[
            ['Photon grenades',1,'ph'],
            ['EMP grenades',3,'rmp']
        ])
        self.leader = self.opt_optional_sub_unit('Team leader',[self.Shasui(base_points)])
        self.transport = self.opt_optional_sub_unit('Transport',[Devilfish()])

    def check_rules(self):
        leader = 1 if self.leader.get_count() else 0
        self.team.update_range(6 - leader, 12 - leader)
        self.carb.update_range(0, self.team.get())
        lcnt = 0 if not self.leader.get(self.Shasui.name) else self.leader.get_unit().get_unit().full_count()
        self.transport.set_active(self.team.get() + lcnt <= 12 and self.transport_enable)
        self.set_points(self.leader.points()+self.transport.points() + self.team.get()*(10 + self.gren.points()) + self.leader.get_count()*self.gren.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        if self.team.get() - self.carb.get() > 0:
            self.description.add('Pulse rifle',self.team.get() - self.carb.get())
        if self.carb.get() > 0:
            self.description.add('Pulse carbine',self.carb.get())
        if self.gren.get('ph'):
            self.description.add('Photon grenades',self.leader.get_count() + self.team.get())
        if self.gren.get('emp'):
            self.description.add('EMP grenades',self.leader.get_count() + self.team.get())
        self.description.add(self.leader.get_selected())

        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.team.get() + self.leader.get_count()

    def enable_transport(self, value):
        self.transport_enable = value
        self.transport.set_active(value)


class KrootSquad(Unit):
    name = "Kroot Carnivore squad"

    class Shaper(Unit):
        name = 'Shaper'
        base_points = 28

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Kroot rifle', 0, 'kr'],
                ['Pulse rifle', 5, 'pr'],
                ['Pulse carbine', 5, 'pc']
            ])

    class Kroot(StaticUnit):
        name = 'Kroot'
        base_points = 7
        gear = ['Kroot rifle']

    class Krootox(StaticUnit):
        name = 'Krootox rider'
        base_points = 35
        gear = ['Kroot gun']

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_optional_sub_unit('Leader', [self.Shaper()])
        self.team = self.opt_count('Kroot', 10, 20, 7)
        self.dog = self.opt_count('Kroot Hound', 0, 12, 6)
        self.cav = self.opt_count('Krootox rider', 0, 3, 35)
        self.opt = self.opt_options_list('Options', [['Take armor save on models', 1, 'sv']])

    def check_rules(self):
        norm_counts(10 - self.leader.get_count(), 20 - self.leader.get_count(), [self.team])
        self.opt.set_active(self.leader.get_count() > 0)
        self.set_points(self.build_points(count=1, exclude=[self.opt.id]) +
                        self.opt.points() * (self.team.get() + self.dog.get() + self.cav.get()))
        self.build_description(count=1, options=[self.leader, self.dog])
        if self.opt.get('sv'):
            self.description.add('Armor save', self.get_count() - 1)
        self.description.add(self.Kroot(count=self.team.get()).get_description())
        self.description.add(self.Krootox(count=self.cav.get()).get_description())

    def get_count(self):
        return self.team.get() + self.dog.get() + self.cav.get() + self.leader.get_count()
