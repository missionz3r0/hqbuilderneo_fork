__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.games.wh40k.obsolete.space_wolves.troops import DropPod, Rhino, Razorback


class LongFangs(Unit):
    name = "Long Fangs Pack"
    base_points = 15

    class LongFang(Unit):
        name = 'Long Fang'
        base_points = 15
        gear = ["Power Armour", "Frag and Krak grenades", "Close Combat Weapon"]

        def __init__(self):
            Unit.__init__(self)
            self.count = self.opt_count(self.name, 1, 5, self.base_points)
            self.wep = self.opt_one_of("Weapon", [
                ["Heavy Bolter", 5, "bolt"],
                ["Missile Launcher", 10, "missile"],
                ["Multi-melta", 10, "melta"],
                ["Plasma Cannon", 20, "plasma"],
                ["Lascannon", 25, "las"],
            ])

        def check_rules(self):
            self.points.set((self.base_points + self.wep.points()) * self.count.get() )
            self.description.reset()
            self.description.set_header(self.name, self.base_points + self.wep.points())
            self.description.add(self.gear)
            self.description.add(self.wep.get_selected())
            self.set_description(self.description)

    class LongFangLeader(Unit):
        name = 'Squad Leader'
        base_points = 15
        gear = ["Power Armour", "Frag and Krak grenades"]

        def __init__(self):
            Unit.__init__(self)
            wep = [
                ["Flamer", 5, "flame"],
                ["Meltagun", 10, "melta"],
                ["Plasma Gun", 15, "plasmagun"],
                ["Plasma Pistol", 15, "plasma"],
                ["Power Weapon", 15, "pw"],
                ["Power Fist", 25, "fist"],
                ]
            self.wep1 = self.opt_one_of("Weapon", [["Bolt Pistol", 0, "bolt"]] + wep, id='w1')
            self.wep2 = self.opt_one_of("Weapon", [["Close Combat Weapon", 0, "bolt"]] + wep, id='w2')
            self.opt = self.opt_options_list("Options", [["Melta Bombs", 5, "power"]])

    def __init__(self):
        Unit.__init__(self)
        self.lead = self.opt_sub_unit(self.LongFangLeader())
        self.fangs = self.opt_units_list(self.LongFang.name, self.LongFang, 1, 5)
        self.trans = self.opt_options_list('Transport', [
            [DropPod.name, DropPod.base_points, 'dp'],
            [Rhino.name, Rhino.base_points, 'rh'],
            [Razorback.name, Razorback.base_points, 'rz']
        ], limit = 1)
        self.dp_unit = self.opt_sub_unit(DropPod())
        self.rh_unit = self.opt_sub_unit(Rhino())
        self.rz_unit = self.opt_sub_unit(Razorback())

    def check_rules(self):
        self.fangs.update_range(1, 5)
        self.dp_unit.set_active(self.trans.get('dp'))
        self.rh_unit.set_active(self.trans.get('rh'))
        self.rz_unit.set_active(self.trans.get('rz'))

        self.points.set(self.lead.points() + self.fangs.points() +
            self.dp_unit.points() + self.rh_unit.points() + self.rz_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.lead.get_selected())
        self.description.add(self.fangs.get_selected())
        self.description.add(self.dp_unit.get_selected())
        self.description.add(self.rh_unit.get_selected())
        self.description.add(self.rz_unit.get_selected())
        self.set_description(self.description)

    def get_count(self):
        return self.fangs.get_count() + 1


class Predator(Unit):
    name = 'Predator'
    base_points = 60
    gear = ["Smoke Launchers", "Searchlight"]

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of("Weapon", [
            ["Autocannon", 0, "auto"],
            ["Twin-linked Lascannon", 45, "las"],
            ])
        self.sp = self.opt_options_list("Side Sponsons", [
            ["Heavy Bolters", 25, "bolt"],
            ["Lascannons", 60, "las"],
            ], limit = 1)
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Extra Armour", 15, "rh_armor"],
            ])


class Whirlwind(Unit):
    name = "Whirlwind"
    base_points = 85
    gear = ["Whirlwind Multiple Missile Launcher", "Smoke Launchers", "Searchlight"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Extra Armour", 15, "rh_armor"],
            ])


class Vindicator(Unit):
    name = "Vindicator"
    base_points = 115
    gear = ["Demolisher Cannon", "Storm Bolter", "Smoke Launchers", "Searchlight"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Siege Shield", 10, "shield"],
            ["Extra Armour", 15, "rh_armor"],
            ])


class LandRaider(Unit):
    name = 'Land Raider'
    base_points = 250
    gear = ["Twin-linked Heavy Bolter", "Twin-linked Lascannon", "Twin-linked Lascannon", "Smoke Launchers", "Searchlight"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Multi-melta", 10, "melta"],
            ["Extra Armour", 15, "rh_armor"],
            ])


class LandRaiderCrusader(Unit):
    name = 'Land Raider Crusader'
    base_points = 250
    gear = ["Twin-linked Assault Cannon", "Hurricane Bolter", "Hurricane Bolter", "Smoke Launchers", "Frag Assault Launcher", "Searchlight"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Multi-melta", 10, "melta"],
            ["Extra Armour", 15, "rh_armor"],
            ])


class LandRaiderRedeemer(Unit):
    name = 'Land Raider Redeemer'
    base_points = 240
    gear = ["Twin-linked Assault Cannon", "Flamestorm Cannon", "Flamestorm Cannon", "Frag Assault Launcher", "Smoke Launchers", "Searchlight"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Multi-melta", 10, "melta"],
            ["Extra Armour", 15, "rh_armor"],
            ])
