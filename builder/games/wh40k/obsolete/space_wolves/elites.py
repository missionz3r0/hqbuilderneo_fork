__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.space_wolves.troops import DropPod, Rhino, Razorback
from builder.games.wh40k.obsolete.space_wolves.heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer
from builder.core.options import norm_counts
from functools import reduce

class WolfGuards(Unit):
    name = "Wolf Guard Pack"
    min = 3
    max = 10

    class Arjac(StaticUnit):
        name = "Arjac Rockfist, The Anvil of Fernis"
        base_points = 170 + 18
        gear = ['Terminator Armour', 'Foehammer', 'Anvil Shield', 'Wolftooth Necklace', 'Saga of Bear']

    class WolfGuard(ListSubUnit):
        name = 'Wolf Guard'
        base_points = 18
        min = 1
        max = 10

        def __init__(self):
            ListSubUnit.__init__(self)

            self.armor = self.opt_one_of('Armour', [
                ["Power Armour", 0, "power"],
                ["Terminator Armour", 15, "tda"],
                ])

            wep = [
                ["Boltgun", 0, "gun"],
                ["Storm Bolter", 3, "storm"],
                ["Combi-flamer", 5, "cflame"],
                ["Combi-melta", 5, "cmelta"],
                ["Combi-plasma", 5, "cplasma"],
                ["Power Weapon", 10, "pw"],
                ["Plasma Pistol", 10, "plasma"],
                ["Wolf Claw", 15, "claw"],
                ["Power Fist", 20, "fist"],
                ["Frost Blade", 20, "blade"],
                ["Frost Axe", 20, "axe"],
                ["Thunder Hummer", 25, "hummer"],
                ["Storm Shield", 25, "shield"],
            ]
            self.wep1 = self.opt_one_of('Weapon', [["Bolt Pistol", 0, "pistol"]] + wep, id='w1')

            self.wep2 = self.opt_one_of('', [["Close Combat Weapon", 0, "ccw"]] + wep, id='w2')

            self.tda_wep2 = self.opt_one_of('Weapon', [
                ["Power Weapon", 0, "power"],
                ["Wolf Claw", 5, "claw"],
                ["Power Fist", 10, "fist"],
                ["Frost Blade", 10, "blade"],
                ["Frost Axe", 10, "axe"],
                ["Thunder Hummer", 15, "hummer"],
                ["Storm Shield", 15, "shield"],
                ["Chain Fist", 15, "chain"],
                ], id='tda_w2')

            self.tda_wep1 = self.opt_one_of('', [
                ["Storm Bolter", 0, "storm"],
                ["Combi-flamer", 5, "cflame"],
                ["Combi-melta", 5, "cmelta"],
                ["Combi-plasma", 5, "cplasma"],
                ["Wolf Claw", 10, "claw"],
                ["Power Fist", 10, "fist"],
                ["Thunder Hummer", 15, "hummer"],
                ["Storm Shield", 15, "shield"],
                ["Chain Fist", 15, "chain"],
                ["Heavy Flamer", 5, "flamer"],
                ["Assault Cannon", 30, "cannon"],
                ], id='tda_w1')

            self.heavy_opt = ["flamer", "cannon"]

            self.cyc = self.opt_options_list('', [
                ["Cyclone Missile Launcher", 30, "cyc"],
                ], id='cyc')

            self.mount = self.opt_options_list("Mount", [
                ["Jump Pack", 25, "jump"],
                ["Space Marine Bike", 35, "bike"],
                ], limit = 1)

            self.opt = self.opt_options_list("Options", [
                ["Melta Bombs", 5, "power"],
                ["Mark of the Wulfen", 15, "mark"],
                ])

        def check_rules(self):
            tda = self.armor.get_cur() == 'tda'
            self.wep1.set_visible(not tda)
            self.wep2.set_visible(not tda)
            self.mount.set_visible(not tda)
            self.tda_wep1.set_visible(tda)
            self.tda_wep2.set_visible(tda)
            self.cyc.set_visible(tda)
            self.gear = ['Frag and Krak Grenades'] if not tda else []
            ListSubUnit.check_rules(self)

        def have_heavy(self):
            return ((1 if self.tda_wep1.get_cur() in self.heavy_opt else 0) + (1 if self.cyc.get('cyc') else 0))* self.get_count()

        def drop_heavy(self):
            if self.tda_wep1.get_cur() in self.heavy_opt:
                self.tda_wep1.set('storm')
            self.cyc.set('cyc', False)

        def set_enable_heavy(self, val):
            if self.cyc.get('cyc'):
                tda_val = False
                cyc_val = val
            elif self.tda_wep1.get_cur() in self.heavy_opt:
                tda_val = val
                cyc_val = False
            else:
                tda_val = cyc_val = val
            self.tda_wep1.set_active_options(self.heavy_opt, tda_val)
            self.cyc.set_active_options(['cyc'], cyc_val)

        def have_mark(self):
            return self.opt.get('mark')

        def disable_mark(self, val):
            if self.count.get() != 1:
                self.opt.set_active_options(['mark'], False)
            elif not self.have_mark():
                self.opt.set_active_options(['mark'], val)

    def __init__(self):
        Unit.__init__(self)
        self.units = self.opt_units_list(self.WolfGuard.name, self.WolfGuard, self.min, self.max)
        self.arj = self.opt_optional_sub_unit('', self.Arjac())
        self.trans = self.opt_optional_sub_unit('Transport', [DropPod(), Razorback(), Rhino(), LandRaider(),
                                                                LandRaiderCrusader(), LandRaiderRedeemer()])

    def have_land_raider(self):
        return self.trans.get(LandRaider.name) or self.trans.get(LandRaiderCrusader.name) or self.trans.get(LandRaiderRedeemer.name)

    def check_rules(self):
        have_mark = reduce(lambda val, u: u.have_mark() or val, self.units.get_units(), False)
        for u in self.units.get_units():
            u.disable_mark(not have_mark)

        self.arj.set_active(self.units.get_count() < 10)
        arj = self.arj.get_count()
        self.units.update_range(self.min - arj, self.max - arj)

        heavy_limit = int((self.units.get_count() + arj) / 5)
        heavy_total = reduce(lambda val, u: u.have_heavy() + val, self.units.get_units(), 0)

        if heavy_total > heavy_limit:
            for u in self.units.get_units():
                u.drop_heavy()
            heavy_total = 0
            for u in self.units.get_units():
                u.set_enable_heavy(u.count.get() <= heavy_limit)
        if heavy_total == heavy_limit:
            for u in self.units.get_units():
                u.set_enable_heavy(u.have_heavy() != 0)
        elif heavy_total < heavy_limit:
            for u in self.units.get_units():
                u.set_enable_heavy(u.count.get() <= heavy_limit - heavy_total)

        self.points.set(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return self.units.get_count() + self.arj.get_count()

    def get_unique(self):
        if self.arj.get_count():
            return self.Arjac.name


class Dreadnought(Unit):
    name = "Dreadnought"
    base_points = 105
    gear = ["Smoke Launchers", "Searchlight"]

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of("Weapon", [
            ["Dreadnought CCW (with build-in Storm Bolter)", 0, "ccw_bolter"],
            ["Dreadnought CCW (with build-in Heavy Flamer)", 10, "ccw_flamer"],
            ["Twin-linked Autocannon", 10, "ac"],
            ["Missile Launcher ", 10, "missile"],
            ])

        self.wep2 = self.opt_one_of("", [
            ["Assault Cannon", 0, "assault"],
            ["Twin-linked Heavy Flamer", 0, "flamer"],
            ["Multi-melta", 0, "melta"],
            ["Twin-linked Heavy Bolter", 5, "bolter"],
            ["Twin-linked Autocannon", 10, "ac"],
            ["Plasma Cannon", 10, "plasma"],
            ["Twin-linked Lascannon", 30, "lascannon"],
            ])

        self.opt = self.opt_options_list("Options", [
            ["Extra Armour", 15, "armor"],
            ["Wolftooth Necklace", 10, "neck"],
            ["Wolf Tail Talisman", 5, "tail"],
            ])

        self.trans = self.opt_optional_sub_unit('Transport', DropPod())


class Venerable(Unit):
    name = "Venerable Dreadnought"
    base_points = 165
    gear = ["Smoke Launchers", "Searchlight"]

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of("Weapon", [
            ["Dreadnought CCW (with build-in Storm Bolter)", 0, "ccw_bolter"],
            ["Dreadnought CCW (with build-in Heavy Flamer)", 10, "ccw_flamer"],
            ["Twin-linked Autocannon", 10, "ac"],
            ["Missile Launcher ", 10, "missile"],
            ])

        self.wep2 = self.opt_one_of("", [
            ["Assault Cannon", 0, "assault"],
            ["Twin-linked Heavy Flamer", 0, "flamer"],
            ["Multi-melta", 0, "melta"],
            ["Twin-linked Heavy Bolter", 5, "bolter"],
            ["Twin-linked Autocannon", 10, "ac"],
            ["Plasma Cannon", 10, "plasma"],
            ["Twin-linked Lascannon", 30, "lascannon"],
            ])

        self.opt = self.opt_options_list("Options", [
            ["Extra Armour", 15, "armor"],
            ["Wolftooth Necklace", 10, "neck"],
            ["Wolf Tail Talisman", 5, "tail"],
            ["Saga of Majesty", 15, "saga"],
        ])

        self.trans = self.opt_optional_sub_unit('Transport', DropPod())


class IronPriest(Unit):
    name = 'Iron Priest'
    base_points = 50

    gear = ['Runic Armour', 'Frag and Krak Grenades', 'Servo-arm', 'Thunder Hammer']
    class Servitor(Unit):
        name = 'Servitor'
        base_points = 10
        def __init__(self):
            Unit.__init__(self)
            self.count = self.opt_count('Thrall-servitor', 1, 3, 10)
            self.hb = self.opt_count('Heavy Bolter', 0, 1, 5)
            self.mm = self.opt_count('Multi-melta', 0, 1, 10)
            self.pc = self.opt_count('Plasma Cannon', 0, 1, 20)
            self.heavy = [self.hb, self.mm, self.pc]
        def check_rules(self):
            norm_counts(0, min(self.count.get(), 2), self.heavy)
            heavy = reduce(lambda val, o: val + o.get(), self.heavy, 0)
            self.gear = ['Servo-Arm' for i in range(self.count.get() - heavy)]
            self.points.set(self.build_points(count=1))
            self.build_description(count=1, name='Thrall-servitors', exclude=[self.count.id])


    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of("Weapon", [
            ["Bolt Pistol", 0, "pistol"],
            ["Boltgun", 0, "gun"],
            ])

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ], limit = 1)

        self.opt = self.opt_options_list("Options", [
            ["Wolftooth Necklace", 10, "neck"],
            ["Wolf Tail Talisman", 5, "tail"],
            ["Saga of the Iron Wolf", 15, "saga"],
            ])

        self.wolves = self.opt_count('Cyberwolf', 0, 4, 15)
        self.serv = self.opt_optional_sub_unit('Servitors', self.Servitor())
    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return 1 + self.serv.get_count()


class WolfScouts(Unit):
    name = "Wolf Scouts Pack"
    min = 5
    max = 10

    class WolfScout(ListSubUnit):
        name = 'Wolf Scout'
        base_points = 15
        min = 1
        max = 10
        gear = ['Scout Armour', 'Frag and Krak Grenades']
        def __init__(self):
            ListSubUnit.__init__(self)

            self.wep1 = self.opt_one_of("Weapon", [
                ["Bolt Pistol", 0, "pistol"],
                ["Boltgun", 0, "gun"],
                ["Sniper Rifle", 3, "sniper"],
                ["Flamer", 5, "flame"],
                ["Heavy Bolter", 5, "hbolter"],
                ["Meltagun", 10, "melta"],
                ["Missile Launcher", 10, "missile"],
                ["Plasma Gun", 15, "plasmagun"],
                ["Power Weapon", 15, "pw"],
                ["Plasma Pistol", 15, "plasma"],
                ])
            self.heavy = ["flame", "hbolter", "melta", "missile", "plasmagun"]
            self.power = ['pw', 'plasma']

            self.wep2 = self.opt_one_of("", [
                ["Close Combat Weapon", 0, "ccw"],
                ["Boltgun", 0, "gun"],
                ["Sniper Rifle", 3, "sniper"],
                ])

            self.opt = self.opt_options_list("Options", [
                ["Mark of the Wulfen", 15, "mark"],
                ])

        def have_mark(self):
            return self.opt.get('mark')

        def disable_mark(self, val):
            if self.count.get() != 1:
                self.opt.set_active_options(['mark'], False)
            elif not self.have_mark():
                self.opt.set_active_options(['mark'], val)

        def have_heavy(self):
            return self.wep1.get_cur() in self.heavy

        def disable_heavy(self, val):
            if self.count.get() != 1:
                self.wep1.set_active_options(self.heavy, False)
            elif not self.have_heavy():
                self.wep1.set_active_options(self.heavy, val)

        def have_power(self):
            return self.get_count() if self.wep1.get_cur() in self.power else 0

        def drop_power(self):
            if self.wep1.get_cur() in self.power:
                self.wep1.set('pistol')

        def set_enable_power(self, val):
            self.wep1.set_active_options(self.power, val)

    def __init__(self):
        Unit.__init__(self)
        self.units = self.opt_units_list(self.WolfScout.name, self.WolfScout, self.min, self.max)

    def check_rules(self):
        have_mark = reduce(lambda val, u: u.have_mark() or val, self.units.get_units(), False)
        have_heavy = reduce(lambda val, u: u.have_heavy() or val, self.units.get_units(), False)
        for u in self.units.get_units():
            u.disable_mark(not have_mark)
            u.disable_heavy(not have_heavy)

        power_limit = 2
        power_total = reduce(lambda val, u: u.have_power() + val, self.units.get_units(), 0)

        if power_total > power_limit:
            for u in self.units.get_units():
                u.drop_power()
            power_total = 0
            for u in self.units.get_units():
                u.set_enable_power(u.count.get() <= power_limit)
        if power_total == power_limit:
            for u in self.units.get_units():
                if not u.have_power():
                    u.set_enable_power(False)
        elif power_total < power_limit:
            for u in self.units.get_units():
                u.set_enable_power(u.count.get() <= power_limit - power_total)

        self.units.update_range()

        self.points.set(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.units.get_count()


class LoneWolf(Unit):
    name = "Lone Wolf"
    base_points = 20
    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Terminator Armour", 25, "tda"],
            ])

        wep = [
            ["Power Weapon", 15, "pw"],
            ["Plasma Pistol", 15, "plasma"],
            ["Wolf Claw", 20, "claw"],
            ["Power Fist", 25, "fist"],
            ["Frost Blade", 25, "blade"],
            ["Frost Axe", 25, "axe"],
            ["Thunder Hummer", 30, "hummer"],
            ["Storm Shield", 30, "shield"],
            ]

        self.wep1 = self.opt_one_of('Weapon', [["Bolt Pistol", 0, "pistol"]] + wep, id='w1')

        self.wep2 = self.opt_one_of('Weapon', [["Close Combat Weapon", 0, "ccw"]] + wep, id='w2')

        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ["Wolf Claw", 15, "claw"],
            ["Thunder Hummer", 25, "hummer"],
            ["Storm Shield", 25, "shield"],
            ["Chain Fist", 25, "chain"],
            ], id='tda_w1')

        self.tda_wep2 = self.opt_one_of('Weapon', [
            ["Power Weapon", 0, "power"],
            ["Wolf Claw", 5, "claw"],
            ["Power Fist", 10, "fist"],
            ["Frost Blade", 10, "blade"],
            ["Frost Axe", 10, "axe"],
            ["Thunder Hummer", 15, "hummer"],
            ["Storm Shield", 15, "shield"],
            ["Chain Fist", 15, "chain"],
            ], id='tda_w2')

        self.opt = self.opt_options_list("Options", [
            ["Melta Bombs", 5, "power"],
            ["Fenrisian Wolf", 10, "fwolf1"],
            ["Fenrisian Wolf", 10, "fwolf2"],
            ["Mark of the Wulfen", 15, "mark"],
            ])

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.tda_wep2.set_visible(tda)
        self.gear = ['Frag and Krak Grenades'] if not tda else []
        Unit.check_rules(self)

