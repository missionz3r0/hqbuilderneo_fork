__author__ = 'Denis Romanow'

from builder.core.unit import Unit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.black_templars2005.armory import vehicle
from builder.games.wh40k.obsolete.black_templars2005.heavy import LandRaiderCrusader


class Rhino(Unit):
    name = "Rhino"
    base_points = 50
    gear = ['Storm bolter']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicle)


class Razorback(Unit):
    name = "Razorback"
    base_points = 70

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ["Twin-linked heavy bolter", 0, 'tlhbgun'],
            ["Twin-linked lascannon", 20, 'tllcannon'],
        ])
        self.opt = self.opt_options_list('Options', vehicle)


class DropPod(Unit):
    name = "Drop pod"
    base_points = 30
    gear = ['Power of the Machine Spirit']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ["Storm bolter", 0, 'sbgun'],
        ])
        self.opt = self.opt_options_list('Options', [
            ["Deathwind missile launcher", 20, 'dwind']
        ])


class TacticalSquad(Unit):
    name = 'Crusader Squad'

    class Initiate(ListSubUnit):
        name = 'Initiate'
        base_points = 16
        gear = ['Power armor']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bg'],
                ['Bolt pistol and close combat weapon', 0, 'ccw'],
            ])
            self.wep2 = self.opt_options_list('', [
                ['Power weapon', 10, 'pw'],
                ['Power fist', 15, 'pf'],
                ['Heavy bolter', 5, 'hb'],
                ['Missile launcher', 10, 'ml'],
                ['Multi-melta', 10, 'mm'],
                ['Lascannon', 15, 'lc'],
                ['Plasma cannon', 20, 'pc'],
                ['Flamer', 6, 'fl'],
                ['Meltagun', 10, 'mg'],
                ['Plasma gun', 6, 'pg'],
            ], limit=1)
            self.heavy = ['pw', 'pf', 'hb', 'ml', 'mm', 'lc', 'pc']
            self.spec = ['fl', 'mg', 'pg']

        def set_opt(self, bomb, cs):
            single_points = self.build_points(exclude=[self.count.id], count=1) + bomb.points() + cs.points()
            self.set_points(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id, self.wep1.id])
            if self.wep1.get_cur() == 'bg':
                self.description.add(self.wep1.get_selected())
                if 'pw' in self.wep2.get_all() or 'pf' in self.wep2.get_all():
                    self.description.add(['Bolt pistol'])
            else:
                self.description.add(['Bolt pistol', 'Close combat weapon'])
            self.description.add(bomb.get_selected())
            self.description.add(cs.get_selected())

        def check_rules(self):
            pass

        def has_heavy(self):
            for ids in self.wep2.get_all():
                if ids in self.heavy:
                    return self.get_count()
            return 0

        def has_spec(self):
            for ids in self.wep2.get_all():
                if ids in self.spec:
                    return self.get_count()
            return 0

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_units_list('', TacticalSquad.Initiate, 5, 10)
        self.neo = self.opt_count('Neophyte', 0, 5, 10)
        self.shotgun = self.opt_count('Shotguns', 0, 0, 0)
        self.opt = self.opt_options_list('Options', [
            ['Frag grenades', 1, 'frag'],
            ['Krak grenades', 2, 'krak'],
        ])
        self.cs = self.opt_options_list('', [
            ['Crusader seals', 2, 'cs'],
        ])
        self.transport = self.opt_options_list('Transport', [
            [Rhino.name, Rhino.base_points, 'rh'],
            [DropPod.name, DropPod.base_points, 'dp'],
            [Razorback.name, Razorback.base_points, 'rz'],
            [LandRaiderCrusader.name, LandRaiderCrusader.base_points, 'lr']
        ], limit=1)
        self.rh = self.opt_sub_unit(Rhino())
        self.rz = self.opt_sub_unit(Razorback())
        self.dp = self.opt_sub_unit(DropPod())
        self.lr = self.opt_sub_unit(LandRaiderCrusader())

    def check_rules(self):
        spec = sum((u.has_spec() for u in self.marines.get_units()))
        if spec > 1:
            self.error('Initiates can take only 1 special weapon (taken: {0})'.format(spec))
        heavy = sum((u.has_heavy() for u in self.marines.get_units()))
        if heavy > 1:
            self.error('Initiates can take only 1 heavy or power weapon (taken: {0})'.format(heavy))

        self.marines.update_range()
        for unit in self.marines.get_units():
            unit.set_opt(self.opt, self.cs)
        self.dp.set_active(self.get_count() <= 10 and self.transport.get('dp'))
        self.rh.set_active(self.get_count() <= 10 and self.transport.get('rh'))
        self.rz.set_active(self.get_count() <= 6 and self.transport.get('rz'))
        self.lr.set_active(self.get_count() <= 15 and self.transport.get('lr'))
        self.neo.update_range(0, self.marines.get_count())
        self.shotgun.update_range(0, self.neo.get())

        self.set_points(self.build_points(count=1, exclude=[self.opt.id, self.cs.id, self.transport.id]))
        self.build_description(options=[self.marines])

        desc = ModelDescriptor('Neophyte', gear=['Scout armor'], points=10).add_gear_opt(self.opt)
        self.description.add(desc.clone().add_gear('Shotgun').build(self.shotgun.get()))
        self.description.add(desc.add_gear('Bolt pistol').add_gear('Close combat weapon')
            .build(self.neo.get() - self.shotgun.get()))
        self.description.add(self.dp.get_selected())
        self.description.add(self.lr.get_selected())
        self.description.add(self.rz.get_selected())
        self.description.add(self.rh.get_selected())

    def get_count(self):
        return self.marines.get_count() + self.neo.get()
