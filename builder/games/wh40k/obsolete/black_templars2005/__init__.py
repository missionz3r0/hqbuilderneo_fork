__author__ = 'maria'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.obsolete.black_templars2005.hq import *
from builder.games.wh40k.obsolete.black_templars2005.elites import *
from builder.games.wh40k.obsolete.black_templars2005.troops import *
from builder.games.wh40k.obsolete.black_templars2005.fast import *
from builder.games.wh40k.obsolete.black_templars2005.heavy import *


class BlackTemplars2005(LegacyWh40k):
    army_id = '42a664e205134495bee36a13890c99cb'
    army_name = 'Black Templars (2005)'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[HighMarshall, Grimaldus, Marshall, Castellan, Champion, Master, Reclusiarch],
            elites=[TerminatorSquad, TerminatorAssaultSquad, SwordBrethrenSquad, Dreadnought, Techmarine],
            troops=[TacticalSquad],
            fast=[AssaultSquad, LandSpeederSquadron, SpaceMarineBikers, AttackBikeSquad],
            heavy=[Vindicator, Predator, Destructor, LandRaider, LandRaiderCrusader],
            secondary=secondary
        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_unit(Champion)
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        if self.limit < 1500:
            if self.hq.count_unit(HighMarshall) > 0:
                self.error(HighMarshall.name + " may be used in armies of at least 1500 points")
            if self.hq.count_unit(Grimaldus) > 0:
                self.error(Grimaldus.name + " may be used in armies of at least 1500 points")
        if self.limit > 750 and not self.hq.count_unit(Champion):
            self.error(Champion.name + " must be taken for any armies of at least 750 points")
