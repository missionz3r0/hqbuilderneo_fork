__author__ = 'Ivan Truskov'
from builder.core.unit import Unit
from builder.games.wh40k.obsolete.chaos_daemons2007.elites import OptUnit


class Letters(OptUnit):
    name = "Bloodletters of Khorne"
    gear = ['Hellblade']
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Bloodletter',5,20,16)
        self.opt = self.opt_options_list('Options',[
            ['Fury of Khorne',10,'fkh'],
            ['Chaos Icon',25,'chic'],
            ['Instrument of Chaos',5,'ich']
        ])


class Daemonettes(OptUnit):
    name = "Daemonettes of Slaanesh"
    gear = ['Rending Claws','Aura of Acquiescence']
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Daemonette',5,20,14)
        self.opt = self.opt_options_list('Options',[
            ['Transfixing Gaze',5,'tgaze'],
            ['Chaos Icon',25,'chic'],
            ['Instrument of Chaos',5,'ich']
        ])


class Bearers(OptUnit):
    name = "Plaguebearers of Nurgle"
    gear = ['Plaguesword']
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Plaguebearer',5,20,16)
        self.opt = self.opt_options_list('Options',[
            ['Noxious Touch',10,'ntch'],
            ['Chaos Icon',25,'chic'],
            ['Instrument of Chaos',5,'ich']
        ])

class Horrors(Unit):
    name = "Pink Horrors of Tzeentch"
    gear = ['Warpfire']

    class TheChangeling(Unit):
        name = 'The Changeling'
        base_points = 17 + 5
        gear = ['Warpfire']
        static = True

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Pink Horror',5,20,17)
        self.opt = self.opt_options_list('Options',[
            ['Bolt of Tzeentch',10,'bot'],
            ['Chaos Icon',25,'chic'],
            ['Instrument of Chaos',5,'ich']
        ])
        self.up = self.opt_optional_sub_unit('', self.TheChangeling())

    def check_rules(self):
        cheng = self.up.get_count()
        self.warriors.update_range(5 - cheng, 20 - cheng)
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.warriors.id, self.opt.id], count=self.warriors.get())
        self.description.add(self.opt.get_selected())

    def get_count(self):
        return self.warriors.get() + self.up.get_count()

    def get_unique(self):
        if self.up.get_count():
            return 'The Changeling'

class Nurglings(Unit):
    name = "Nurglings"
    base_points = 13
    min = 3
    max = 9
    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.name, self.min, self.max, self.base_points)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])
