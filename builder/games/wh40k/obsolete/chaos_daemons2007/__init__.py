__author__ = 'Ivan Truskov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.obsolete.chaos_daemons2007.hq import *
from builder.games.wh40k.obsolete.chaos_daemons2007.elites import *
from builder.games.wh40k.obsolete.chaos_daemons2007.troops import *
from builder.games.wh40k.obsolete.chaos_daemons2007.fast import *
from builder.games.wh40k.obsolete.chaos_daemons2007.heavy import *


class ChaosDaemons(LegacyWh40k):
    army_id = 'f0ae14057787434ab2ee1ea63af939cd'
    army_name = 'Chaos Daemons (2007)'
    obsolete = True

    def __init__(self):
        LegacyWh40k.__init__(self,
            hq = [Kugath, Fateweaver, Scarbrand, Keeper, Unclean, Bloodthirster, LordOfChange, Masque, Epidemius, BlueScribes,Skulltaker,HKhorne,HTzeentch,HNurgle,HSlaanesh],
            elites = [Fiends, Flamers, Crushers, Beasts],
            troops = [Letters, Daemonettes, Bearers, Horrors, Nurglings],
            fast = [Hound, Seekers, Screamers, Fury],
            heavy = [SoulGrinder, DaemonPrince]
        )
        def check_hq_limit():
            count = 2 * self.hq.count_units([Kugath, Fateweaver, Scarbrand, Keeper, Unclean, Bloodthirster, LordOfChange]) + \
                    self.hq.count_units([Masque, Epidemius, BlueScribes, Skulltaker, HKhorne, HTzeentch, HNurgle, HSlaanesh])
            return self.hq.min <= count <= self.hq.max * 2
        self.hq.check_limits = check_hq_limit
