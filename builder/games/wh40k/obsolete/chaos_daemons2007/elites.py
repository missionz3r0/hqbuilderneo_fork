__author__ = 'Ivan Truskov'
from builder.core.unit import Unit

class OptUnit(Unit):
    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.warriors.id, self.opt.id])
        self.description.add(self.opt.get_selected())

    def get_count(self):
        return self.warriors.get()

class Fiends(OptUnit):
    name = "Fiends of Slaanesh"
    gear = ['Soporific Musk','Rending claws']

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Fiend',1,6,30)
        self.opt = self.opt_options_list('Options',[['Unholy Might',10,'umght']])


class Flamers(OptUnit):
    name = "Flamers of Tzeentch"
    gear = ['Warpfire','Breath of Chaos']

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Flamer',3,12,35)
        self.opt = self.opt_options_list('Options',[['Bolt of Tzeentch',30,'bot']])


class Crushers(OptUnit):
    name = "Bloodcrushers of Khorne"
    gear = ['Hellblade','Iron Hide']
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Bloodcrusher',1,8,40)
        self.opt = self.opt_options_list('Options',[
            ['Fury of Khorne',10,'fkh'],
            ['Chaos Icon',25,'chic'],
            ['Instrument of Chaos',5,'ich']
        ])

    def check_rules(self):
        self.opt.set_limit(min(3, self.warriors.get()))
        OptUnit.check_rules(self)

class Beasts(OptUnit):
    name = "Beasts of Nurgle"
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Beast',1,7,35)
        self.opt = self.opt_options_list('Options',[['Noxious Touch',10,'ntch']])
