__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit
from builder.games.wh40k.obsolete.chaos_daemons2007.elites import OptUnit

class Hound(Unit):
    name = "Flesh Hounds of Khorne"
    gear = ['Blessing of the Blood God']

    class Karanak(StaticUnit):
        name = "Karanak, Hound of Vengeance"
        gear = ['Fury of Khorne','Blessing of the Blood God','Instrument of Chaos']
        base_points = 35 + 15

    def __init__(self):
        Unit.__init__(self)
        self.hounds = self.opt_count('Flesh Hound',5,20,15)
        self.opt = self.opt_options_list('Options',[
            ['Fury of Khorne',10,'fok']
        ])
        self.kar = self.opt_optional_sub_unit('', [self.Karanak()])

    def check_rules(self):
        knk = self.kar.get_count()
        self.hounds.update_range(5-knk, 20-knk)
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.hounds.id, self.opt.id], count=self.hounds.get())
        self.description.add(self.opt.get_selected())

    def get_unique(self):
        if self.kar.get(self.Karanak.name):
            return self.Karanak.name


class Seekers(OptUnit):
    name = "Seekers of Slaanesh"
    gear = ['Rending Claws','Aura of Acquiescence']
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Seeker',5,20,17)
        self.opt = self.opt_options_list('Options',[
            ['Transfixing Gaze',5,'tgaze'],
            ['Chaos Icon',25,'chic'],
            ['Instrument of Chaos',5,'ich']
        ])


class Screamers(OptUnit):
    name = "Screamers of Tzeentch"
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Screamer',3,12,16)
        self.opt = self.opt_options_list('Options',[['Unholy Might',5,'umght']])


class Fury(Unit):
    name = "Fury"
    base_points = 15
    min = 5
    max = 20
    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.name, self.min, self.max, self.base_points)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])
