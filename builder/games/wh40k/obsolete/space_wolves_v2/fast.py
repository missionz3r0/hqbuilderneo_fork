__author__ = 'Denis Romanov'

from builder.games.wh40k.obsolete.space_wolves import fast
from builder.core2 import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle

Cavalry = Unit.norm_core1_unit(fast.Cavalry)


class LandSpeederSquadron(Unit):
    type_name = "Land Speeder Squadron"
    type_id = "land_speeder_squadron_v1"

    class LandSpeeder(ListSubUnit, SpaceMarinesBaseVehicle):
        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(LandSpeederSquadron.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy Bolter', 0)
                self.heavyflamer = self.variant('Heavy Flamer', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(LandSpeederSquadron.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade', limit=1)
                self.heavybolter = self.variant('Heavy Bolter', 10)
                self.heavyflamer = self.variant('Heavy Flamer', 10)
                self.multimelta = self.variant('Multi-melta', 20)
                self.assaultcannon = self.variant('Assault Cannon', 40)
                self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 40)

        def __init__(self, parent):
            super(LandSpeederSquadron.LandSpeeder, self).__init__(parent=parent, points=50, name="Land Speeder")
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

        @ListSubUnit.count_unique
        def get_unique_gear(self):
            return SpaceMarinesBaseVehicle.get_unique_gear(self)

    def __init__(self, parent):
        super(LandSpeederSquadron, self).__init__(parent=parent)
        self.speeders = UnitList(parent=self, unit_class=self.LandSpeeder, min_limit=1, max_limit=3)

    def get_count(self):
        return self.speeders.count

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.speeders.units), [])


class Biker(Unit):
    type_name = "Swiftclaw Biker Pack"
    type_id = "swiftclaw_biker_pack_v1"
    model_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Space marine bike')]

    class Marine(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Biker.Marine.Weapon1, self).__init__(parent=parent, name='Weapon')
                self.bolter = self.variant('Bolt pistol', 0)
                self.flamer = self.variant('Flamer', 5)
                self.meltagun = self.variant('Meltagun', 10)
                self.plasmagun = self.variant('Plasma gun', 15)
                self.plasmapistol = self.variant('Plasma pistol', 15)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Biker.Marine.Weapon2, self).__init__(parent=parent, name='')
                self.closecombatweapon = self.variant('Close combat weapon', 0)
                self.powerweapon = self.variant('Power weapon', 15)
                self.powerfist = self.variant('Power fist', 25)

        def __init__(self, parent):
            super(Biker.Marine, self).__init__(parent=parent, name='Swiftclaw Biker', points=25, gear=Biker.model_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep1.cur != self.wep1.bolter

        @ListSubUnit.count_gear
        def has_ccw(self):
            return self.wep2.cur != self.wep2.closecombatweapon

    class AttackBike(Unit):
        model_name = 'Attack Bike'
        model_points = 30

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Biker.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        def __init__(self, parent):
            super(Biker.AttackBike, self).__init__(
                parent=parent, name=self.model_name, points=self.model_points,
                gear=Biker.model_gear + [Gear('Bolt pistol'), Gear('Close combat weapon')])
            self.wep = self.Weapon(self)

    class OptUnits(OptionalSubUnit):
        def __init__(self, parent):
            super(Biker.OptUnits, self).__init__(parent=parent, name='')
            self.attackbike = SubUnit(self, Biker.AttackBike(parent=None))

    class Options(OptionsList):
        def __init__(self, parent):
            super(Biker.Options, self).__init__(parent=parent, name='Options')
            self.variant('Melta bombs', 5, per_model=True)

        @property
        def points(self):
            return super(Biker.Options, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(Biker, self).__init__(parent)
        self.marines = UnitList(self, self.Marine, 3, 10)
        self.attack = self.OptUnits(self)
        self.opt = self.Options(self)

    def check_rules(self):
        super(Biker, self).check_rules()
        if sum(u.has_spec() for u in self.marines.units) > 1:
            self.error('Only one Swiftclaw Biker may upgrade his bolt pistol')
        if sum(u.has_ccw() for u in self.marines.units) > 1:
            self.error('Only one Swiftclaw Biker may upgrade his close combat weapon')

    def get_count(self):
        return self.marines.count + self.attack.count


class Assault(Unit):
    type_name = "Skyclaw Assault Pack"
    type_id = "skyclaw_pack_v1"
    model_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Jump pack')]

    class Marine(ListSubUnit):

        def __init__(self, parent):
            super(Assault.Marine, self).__init__(parent=parent, name='Skyclaw', points=18, gear=Assault.model_gear)
            self.wep1 = Biker.Marine.Weapon1(self)
            self.wep2 = Biker.Marine.Weapon2(self)
            self.opt = self.Options(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep1.cur != self.wep1.bolter

        @ListSubUnit.count_gear
        def has_ccw(self):
            return self.wep2.cur != self.wep2.closecombatweapon

        @ListSubUnit.count_gear
        def has_mark(self):
            return self.opt.markofthewulfen.value

        class Options(OptionsList):
            def __init__(self, parent):
                super(Assault.Marine.Options, self).__init__(parent=parent, name='Options')
                self.markofthewulfen = self.variant('Mark of the Wulfen', 15)

    def __init__(self, parent):
        super(Assault, self).__init__(parent)
        self.marines = UnitList(self, self.Marine, 5, 10)

    def check_rules(self):
        super(Assault, self).check_rules()
        if sum(u.has_mark() for u in self.marines.units) > 1:
            self.error('Only one Skyclaw may have Mark of the Wulfen')
        if sum(u.has_spec() for u in self.marines.units) > 1:
            self.error('Only one Skyclaw may upgrade his bolt pistol')
        if sum(u.has_ccw() for u in self.marines.units) > 1:
            self.error('Only one Skyclaw may upgrade his close combat weapon')

    def get_count(self):
        return self.marines.count


class WolfPack(Unit):
    type_name = "Fenrisian Wolf Pack"
    type_id = "fenrisian_wolf_pack_v1"

    def __init__(self, parent):
        super(WolfPack, self).__init__(parent)
        self.wolves = Count(
            self, 'Fenrisian Wolf', 5, 15, 8,
            gear=UnitDescription('Fenrisian Wolf', options=Gear('Claws and fangs'), points=8)
        )
        self.opt = self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(WolfPack.Options, self).__init__(parent=parent, name='Options')
            self.variant('Cyberwolf', 16, gear=UnitDescription('Cyberwolf', options=Gear('Claws and fangs'), points=16))

    def get_count(self):
        return self.wolves.cur + self.opt.count
