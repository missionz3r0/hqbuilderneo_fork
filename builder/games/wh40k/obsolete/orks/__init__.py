__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.orks.hq import *
from builder.games.wh40k.obsolete.orks.elites import *
from builder.games.wh40k.obsolete.orks.troops import *
from builder.games.wh40k.obsolete.orks.fast import *
from builder.games.wh40k.obsolete.orks.heavy import *


class Orks(LegacyWh40k):
    army_id = 'a7badbbe318f4fa7bf6b134900126a39'
    army_name = 'Orks'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[GhazghkullThraka, MadDokGrotsnik, WazdakkaGutsmek, OldZogwort, Warboss, BigMek, Weirdboy],
            elites=[Meganobz, Nobz, BurnaBoyz, Tankbustas, Lootas, Kommandos],
            troops=[
                OrkBoyz,
                Gretchins,
                {'unit': Meganobz, 'active': False},
                {'unit': Nobz, 'active': False},
                {'unit': DeffDread, 'active': False},
                {'unit': Warbikers, 'active': False},
            ],
            fast=[Stormboyz, Warbikers, Warbuggies, Deffkoptas, Dakkajet, BurnaBommer, BlitzaBommer],
            heavy=[Battlewagon, DeffDread, LootedWagon, Kans, Gunz, FlashGitz],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.ORCS)

        )

    def check_rules(self):
        big_bosses = self.hq.count_units([Warboss, GhazghkullThraka])
        self.troops.set_active_types([Nobz, Meganobz], big_bosses > 0)
        if big_bosses < self.troops.count_units([Nobz, Meganobz]):
            self.error("You can't have more Nobz or Meganobz in Troops then Warbosses (include Ghazghkull Thraka) "
                       "in HQ.")

        bike_boss = self.hq.count_unit(WazdakkaGutsmek) > 0
        self.troops.set_active_types([Warbikers], bike_boss)
        if not bike_boss and self.troops.count_unit(Warbikers) > 0:
            self.error("You can't have Warbikers in Troops without Wazdakka Gutsmek in HQ.")

        big_meks = self.hq.count_unit(BigMek)
        self.troops.set_active_types([DeffDread], big_meks > 0)
        if big_meks < self.troops.count_unit(DeffDread):
            self.error("You can't have more Deff Dreads in Troops then Big Meks in HQ.")

    def has_grotsnik(self):
        return self.hq.count_unit(MadDokGrotsnik) > 0
