__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.core.options import norm_counts


class Trukk(Unit):
    name = "Trukk"
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.rng = self.opt_one_of('Weapon', [
            ["Big shoota", 0],
            ["Rokkit launcha", 5],
        ])
        self.opt = self.opt_options_list('Options', [
            ["Red paint job", 5],
            ["Grot riggers", 5],
            ["Stikkbomb chukka", 5],
            ["Armor plates", 10],
            ["Boarding plank", 5],
            ["Wreckin' ball", 10],
            ["Reinforced ram", 5],
        ])


class OrkBoyz(Unit):
    name = 'Ork Boyz'

    class Nob(Unit):
        name = 'Nob'
        base_points = 16

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon', [
                ['Choppa', 0, 'choppa'],
                ['Big choppa', 5, 'bc'],
                ['Power klaw', 25, 'klaw'],
            ])
            self.rng = self.opt_one_of('', [
                ["Slugga", 0, 'slugga'],
                ["Shoota", 0, 'shoota'],
                ["Big shoota", 5, 'bs'],
                ["Rokkit launcha", 10, 'rl'],
            ])
            self.opt = self.opt_options_list('Options', [
                ["Bosspole", 5, 'pole'],
                ["'eavy armour", 5, 'armour'],
            ])

        def have_hw(self):
            return self.rng.get_cur() == 'bs' or self.rng.get_cur() == 'rl'

        def set_options(self, shoota_boys, hard, bombs, cyborg):
            self.gear = []
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            self.base_points += (4 if hard else 0) + (1 if bombs else 0)
            self.gear += (['Stikkbombs'] if bombs else []) + (['\'eavy armour'] if hard else [])
            self.rng.set_active_options(['shoota'], shoota_boys)
            self.opt.set_active_options(['armour'], not hard)
            Unit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.boyz = self.opt_count("Boyz", 10, 30, 6)

        self.weapon = self.opt_one_of('Weapon', [
            ['Choppa and Slugga', 0, 'cs'],
            ["Shoota", 0, 'shoota'],
        ])

        self.bs = self.opt_count("Big shoota", 0, 3, 5)
        self.rl = self.opt_count("Rokkit launcha", 0, 3, 10)

        self.opt = self.opt_options_list('Options', [
            ["Stikkbombs", 1, 'bombs'],
            ["'eavy armour", 4, 'hard'],
        ])

        self.nob = self.opt_options_list('Boss', [
            ["Nob", self.Nob.base_points, 'up'],
        ])
        self.nob_unit = self.opt_sub_unit(self.Nob())
        self.trukk = self.opt_options_list('Transport', [
            ["Trukk", 35, 'add'],
        ])
        self.trukk_unit = self.opt_sub_unit(Trukk())
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.nob_unit.set_active(self.nob.get('up'))
        self.trukk_unit.set_active(self.trukk.get('add'))
        if self.trukk.get('add') and self.get_count() > 12:
            self.error('Trukk overloaded! It can transport only 12 orks. (taken: {0})'.format(self.get_count()))
        nob = 1 if self.nob.get('up') else 0
        heavy_limit = int(self.get_count() / 10)
        if nob:
            self.nob_unit.get_unit().set_options(self.weapon.get_cur() == 'shoota', self.opt.get('hard'),
                                                 self.opt.get('bombs'), self.cyborg.get('cyborg'))
            heavy_limit -= (1 if self.nob_unit.get_unit().have_hw() else 0)
        norm_counts(10 - nob, 30 - nob, [self.boyz])
        norm_counts(0, heavy_limit, [self.bs, self.rl])

        self.set_points(self.build_points(exclude=[self.opt.id, self.nob.id, self.trukk.id, self.cyborg.id],
                                          count=1) + self.boyz.get() * (self.opt.points() + self.cyborg.points()))
        self.build_description(options=[self.nob_unit])
        desc = ModelDescriptor('Ork Boy', points=6).add_gear_opt(self.opt).add_gear_opt(self.cyborg)
        minor = desc.clone()
        if self.weapon.get_cur() == 'cs':
            minor.add_gear('Choppa').add_gear('Slugga')
        else:
            minor.add_gear('Shoota')
        self.description.add(minor.build(self.boyz.get() - self.bs.get() - self.rl.get()))
        self.description.add(desc.clone().add_gear('Big shoota', points=5).build(self.bs.get()))
        self.description.add(desc.clone().add_gear('Rokkit launcha', points=10).build(self.rl.get()))
        self.description.add(self.trukk_unit.get_selected())

    def get_count(self):
        return self.boyz.get() + self.nob_unit.get_count()


class Gretchins(Unit):
    name = 'Gretchins'

    def __init__(self):
        Unit.__init__(self)

        self.count = self.opt_count('Gretchin', 10, 30, 3)
        self.runth = self.opt_count('Runtheard', 1, 1, 10)
        self.prod = self.opt_count('Grot-prod', 0, 1, 5)
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        runth = int(self.count.get() / 10)
        norm_counts(runth, runth, [self.runth])
        norm_counts(0, runth, [self.prod])
        self.set_points(self.build_points(count=1, exclude=[self.cyborg.id]) + self.cyborg.points() * self.get_count())
        self.build_description(options=[])
        ork = ModelDescriptor('Runtheard', points=10, gear=['Slugga', 'Squig hound']).add_gear_opt(self.cyborg)
        self.description.add(ork.clone().add_gear('Grabba stikk').build(self.runth.get() - self.prod.get()))
        self.description.add(ork.add_gear('Grot-prod', 5).build(self.prod.get()))
        self.description.add(ModelDescriptor('Gretchins', points=3, gear=['Blasta'])
            .add_gear_opt(self.cyborg).build(self.count.get()))

    def get_count(self):
        return self.count.get() + self.runth.get()
