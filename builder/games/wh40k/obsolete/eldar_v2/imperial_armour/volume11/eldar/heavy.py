__author__ = 'Denis Romanov'

from builder.core2 import *

ia_id = ' (IA vol.11)'


class WarpHunter(Unit):
    clear_name = 'Warp Hunter'
    type_name = clear_name + ia_id
    type_id = 'warphunter_v1'

    def __init__(self, parent):
        super(WarpHunter, self).__init__(parent=parent, points=125, gear=[Gear('D-cannon')], name=self.clear_name)
        self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(WarpHunter.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Twin-linked Shuriken Catapult', 0)
            self.variant('Shuriken Cannon', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(WarpHunter.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.variant('Vectored Engines', 20)
            self.variant('Star Engines', 15)
            self.variant('Holo-fields', 35)
            self.variant('Spirit Stones', 10)
