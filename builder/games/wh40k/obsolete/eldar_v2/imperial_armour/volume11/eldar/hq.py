__author__ = 'Denis Romanov'

from builder.core2 import *

ia_id = ' (IA vol.11)'


class Irillyth(Unit):
    clear_name = 'Irillyth, Shade of Twilight'
    type_name = clear_name + ia_id
    type_id = 'irillythshadeoftwilight_v1'

    def __init__(self, parent):
        super(Irillyth, self).__init__(parent=parent, points=220, unique=True, name=self.clear_name, gear=[
            Gear('Spear of Starlight'),
            Gear('Shadow Spectre jet pack'),
            Gear('Spectre Holo-fields'),
            Gear('Haywire grenade')
        ])


class BelAnnath(Unit):
    clear_name = 'Bel-Annath'
    type_name = clear_name + ia_id
    type_id = 'belannath_v1'

    def __init__(self, parent):
        super(BelAnnath, self).__init__(parent=parent, points=175, unique=True, name=self.clear_name, gear=[
            Gear('Ghosthelm'), Gear('Shuriken Pistol'), Gear('Rune Armour'),
            Gear('Runes of Witnessing'), Gear('Spirit Stones'), Gear('The Sundered Spear')
        ])


class Wraithseer(Unit):
    clear_name = 'Wraithseer'
    type_name = clear_name + ia_id
    type_id = 'wraithseer_v1'

    def __init__(self, parent):
        super(Wraithseer, self).__init__(parent=parent, name=self.clear_name, points=185,
                                         gear=[Gear('Wraithspear'), Gear('Wraithshield')])
        self.Weapon(self)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Wraithseer.Weapon, self).__init__(parent=parent, name='Weapon', limit=1)
            self.variant('Bright Lance', 30)
            self.variant('Scatter Laser', 10)
            self.variant('Eldar Missile Launcher', 15)
            self.variant('Star Cannon', 20)
            self.variant('D-Cannon', 40)
