__author__ = 'Ivan Truskov'

from builder.core2 import ListSubUnit, Gear, OptionsList,\
    UnitList, OptionalSubUnit, SubUnit
from builder.games.wh40k.roster import Unit
from .armory import Melee, Ranged, Special, Heavy, SpecialWargear,\
    BaseWeapon, Boltgun, PlainMelee
from .fast import Rhino, Razorback, DropPod, Corvus


class Veterans(Unit):
    type_name = 'Veterans'
    type_id = 'veterans_v1'

    model_points = 22

    class Veteran(ListSubUnit):

        class Shield(BaseWeapon):
            def __init__(self, *args, **kwargs):
                super(Veterans.Veteran.Shield, self).__init__(*args, **kwargs)
                self.variant('Storm shield', 10)

        class RangedWeapon(Shield, Melee, Heavy, Special, Ranged, Boltgun):
            pass

        class MeleeWeapon(Shield, Ranged, Boltgun, Melee, PlainMelee):
            def __init__(self, *args, **kwargs):
                super(Veterans.Veteran.MeleeWeapon, self).__init__(*args, **kwargs)
                self.xeno = self.variant('Xenophase blade', 25)

        class HeavyHammer(OptionsList):
            def __init__(self, parent):
                super(Veterans.Veteran.HeavyHammer, self).__init__(parent, '')
                self.variant('Heavy thunder hammer', 30)

        class Upgrades(OptionsList):
            def __init__(self, parent):
                super(Veterans.Veteran.Upgrades, self).__init__(parent, 'Upgrade to', limit=1)
                self.black = self.variant('Black Shield', 15, gear=[])
                self.sarge = self.variant('Watch Sergeant', gear=[])

        def __init__(self, parent):
            super(Veterans.Veteran, self).__init__(
                parent=parent, points=Veterans.model_points, name='Veteran',
                gear=[Gear('Frag grenades'), Gear('Krak renades'), Gear('Special issue ammunition')])
            self.rng = self.RangedWeapon(self, 'Ranged weapon')
            self.mle = self.MeleeWeapon(self, 'Melee weapon')
            self.ham = self.HeavyHammer(self)
            self.up = self.Upgrades(self)
            self.opt = SpecialWargear(self)

        def check_rules(self):
            super(Veterans.Veteran, self).check_rules()
            # only Veterans can bear heavy hammer
            self.ham.used = self.ham.visible = not self.up.any
            # if hammer is taken, hide the rest of the weapons
            self.rng.used = self.rng.visible =\
                            self.mle.used = self.mle.visible =\
                                            not (self.ham.used and self.ham.any)
            # only sergeant uses special wargear or xenoblade
            self.opt.used = self.opt.visible = self.up.sarge.value
            self.mle.xeno.used = self.mle.xeno.visible = self.up.sarge.value
            # only Veterans can carry heavy weapons
            self.rng.set_visible(not self.up.any)

        def build_description(self):
            res = super(Veterans.Veteran, self).build_description()
            for vrt in [self.up.black, self.up.sarge]:
                if vrt.value:
                    res.name = vrt.title
            return res

        @ListSubUnit.count_gear
        def count_weapon(self):
            return self.rng.is_heavy()

        @ListSubUnit.count_gear
        def is_black(self):
            return self.up.black.value

        @ListSubUnit.count_gear
        def is_sarge(self):
            return self.up.sarge.value

        @ListSubUnit.count_gear
        def has_furor_weapon(self):
            return self.rng.has_furor()

        @ListSubUnit.count_gear
        def has_hammer(self):
            return self.mle. is_hammer() or\
                (self.ham.used and self.ham.any)

        @ListSubUnit.count_gear
        def has_stalker(self):
            return self.rng.is_stalker()

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Veterans.Transport, self).__init__(parent, 'Transport', limit=1)
            SubUnit(self, Rhino(parent=None))
            SubUnit(self, Razorback(parent=None))
            self.pod = SubUnit(self, DropPod(parent=None))
            self.corvus = SubUnit(self, Corvus(parent=None))

    def __init__(self, parent):
        super(Veterans, self).__init__(parent)
        self.vets = UnitList(self, self.Veteran, 5, 10)
        self.transport = self.Transport(self)

    def check_rules(self):
        super(Veterans, self).check_rules()
        heavy_total = sum(u.count_weapon() for u in self.vets.units)
        if 4 < heavy_total:
            self.error('Veterans can take no more then 4 heavy weapons; taken {}'.format(heavy_total))
        if 1 < sum(u.is_black() for u in self.vets.units):
            self.error('Only one Veteran may be a Black Shield')
        if 1 < sum(u.is_sarge() for u in self.vets.units):
            self.error('Only one Veteran may be a Watch Sergeant')

    def get_count(self):
        return self.vets.count

    def build_statistics(self):
        return self.note_transport(super(Veterans, self).build_statistics())

    def furor_weapons(self):
        return sum(u.has_furor_weapon() for u in self.vets.units)

    def hammers(self):
        return sum(u.has_hammer() for u in self.vets.units)

    def stalkers(self):
        return sum(u.has_stalker() for u in self.vets.units)
