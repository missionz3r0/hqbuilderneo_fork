__author__ = 'Ivan Trusov'

from builder.core2 import OneOf, Gear, UnitList,\
    ListSubUnit
from .armory import MechanicusOption
from builder.games.wh40k.roster import Unit


class Breachers(Unit):
    type_id = 'breachers_v1'
    type_name = 'Kataphron Breachers'

    class Breacher(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Breachers.Breacher.Weapon1, self).__init__(parent, 'Ranged weapon')
                self.variant('Heavy arc rifle', 0)
                self.variant('Torsion cannon', 0)

        class Weapon2(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(Breachers.Breacher.Weapon2, self).__init__(parent, 'Melee weapon')
                self.variant('Arc claw', 0)
                self.variant('Hydraulic claw', 10 * self.freeflag)

        def __init__(self, parent):
            super(Breachers.Breacher, self).__init__(parent, name='Kataphron Breacher',
                                                     points=50, gear=[Gear('Kataphron breacherplate')])
            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(Breachers, self).__init__(parent)
        self.models = UnitList(self, self.Breacher, 3, 12)

    def get_count(self):
        return self.models.count


class Destroyers(Unit):
    type_id = 'destroyers_v1'
    type_name = 'Kataphron Destroyers'

    class Destroyer(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Destroyers.Destroyer.Weapon1, self).__init__(parent, 'Ranged weapon')
                self.variant('Plasma culverin', 0)
                self.variant('Heavy grav-cannon', 0)

        class Weapon2(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(Destroyers.Destroyer.Weapon2, self).__init__(parent, '')
                self.variant('Phosphor blaster', 0)
                self.variant('Cognis flamer', 5 * self.freeflag)

        def __init__(self, parent):
            super(Destroyers.Destroyer, self).__init__(parent, name='Kataphron Destroyer',
                                                       points=55, gear=[Gear('Kataphron demiplate')])
            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(Destroyers, self).__init__(parent)
        self.models = UnitList(self, self.Destroyer, 3, 12)

    def get_count(self):
        return self.models.count
