__author__ = 'Ivan Truskov'

from builder.core2 import UnitList, ListSubUnit, OneOf, Gear
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianWeaponArcana
from .armory import SpecialIssue, Field, CadiaArcana, MechanicusOption
from itertools import chain
from builder.games.wh40k.roster import Unit


class KastelanManiple(Unit):
    type_id = 'kastellan_v1'
    type_name = 'Kastellan Robot Maniple'

    class Datasmith(ListSubUnit):
        def __init__(self, parent):
            class Pistol(OneOf):
                def __init__(self, parent):
                    super(Pistol, self).__init__(parent, 'Pistol')
                    self.variant('Gamma pistol')

            class ArcanaPistol(CadianWeaponArcana, Pistol):
                def check_rules(self):
                    super(ArcanaPistol, self).check_rules()
                    self.visible = self.qann.visible

                def get_unique(self):
                    if self.cur == self.qann:
                        return self.description
                    return []

            super(KastelanManiple.Datasmith, self).__init__(
                parent, 'Cybernetica Datasmith', 50, [
                    Gear('Artificer armour'),
                    Gear('Dataspike')
                ])
            self.pistol = ArcanaPistol(self)
            SpecialIssue(self)
            Field(self)
            self.arcana = CadiaArcana(self)

        def get_unique_gear(self):
            return self.arcana.description + self.pistol.get_unique()

        def check_rules(self):
            super(KastelanManiple.Datasmith, self).check_rules()

            if len(self.get_unique_gear()) > 1:
                self.error('Model cannot carry more then one relic')

    class Robot(ListSubUnit):

        class Weapon1(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(KastelanManiple.Robot.Weapon1, self).__init__(parent, 'Hand weapons')
                self.variant('Two power fists', 0, gear=[Gear('Power fist', count=2)])
                self.variant('Twin-linked heavy phosphor blaster', 10 * self.freeflag)

        class Weapon2(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(KastelanManiple.Robot.Weapon2, self).__init__(parent, 'Caparace weapon')
                self.variant('Incedine combustor', 0)
                self.variant('Heavy phosphor blaster', 5 * self.freeflag)

        def __init__(self, parent):
            super(KastelanManiple.Robot, self).__init__(parent, name='Kastelan Robot',
                                                       points=120, gear=[Gear('Kastelan battleplate')])
            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(KastelanManiple, self).__init__(parent)
        self.smiths = UnitList(self, self.Datasmith, 1, 3)
        self.robots = UnitList(self, self.Robot, 2, 6)

    def get_unique_gear(self):
        res = list(chain(*[u.arcana.description * u.count for u in self.smiths.units]))
        return res

    def build_statistics(self):
        return {'Models': self.smiths.count + self.robots.count, 'Units': 1}
