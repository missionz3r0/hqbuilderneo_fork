__author__ = 'Ivan Truskov'
__summary__ = 'Codex Cult Mechanicus 7th edition'

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kKillTeam, PrimaryDetachment, Wh40kImperial,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    HeavySection, UnitType, Detachment, Formation, CompositeFormation
from builder.games.wh40k.fortifications import Fort
from .hq import Dominus, Techpriest, Cawl
from .troops import Breachers, Destroyers
from .elites import Corpuscarii, Fulgurites, Servitors
from .heavy import KastelanManiple

from builder.games.wh40k.imperial_knight_v2 import Oathsworn, BaronialCourt
from builder.games.wh40k.skitarii import Maniple, Cohort
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as BaseLords
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        UnitType(self, Dominus)
        UnitType(self, Techpriest)
        UnitType(self, Cawl)


class HQ(BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, Breachers)
        UnitType(self, Destroyers)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Fulgurites)
        UnitType(self, Corpuscarii)
        UnitType(self, Servitors)


class HeavySupport(HeavySection):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        UnitType(self, KastelanManiple)


class LordsOfWar(CerastusSection, BaseLords):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class CohortCybernetica(CompositeFormation):
    army_id = 'cult_mechanicus_v1_cybernetica'
    army_name = 'Cohort Cybernetica'

    def __init__(self):
        super(CohortCybernetica, self).__init__()
        priests = [UnitType(self, Dominus),
                   UnitType(self, Cawl)]
        self.add_type_restriction(priests, 1, 1)
        UnitType(self, KastelanManiple, min_limit=2, max_limit=2)


class EliminationManiple(Formation):
    army_id = 'cult_mechanicus_v1_elimination'
    army_name = 'Elimination Maniple'

    def __init__(self):
        super(EliminationManiple, self).__init__()
        UnitType(self, Destroyers, min_limit=2, max_limit=3)
        UnitType(self, KastelanManiple, min_limit=1, max_limit=3)


class NuminousConclave(Formation):
    army_id = 'cult_mechanicus_v1_conclave'
    army_name = 'Numinous Conclave'

    def __init__(self):
        super(NuminousConclave, self).__init__()
        UnitType(self, Fulgurites, min_limit=2, max_limit=3)
        UnitType(self, Corpuscarii, min_limit=2, max_limit=3)


class Requisitioner(Formation):
    army_id = 'cult_mechanicus_v1_requisitioner'
    army_name = 'Holy Requisitioner'

    def __init__(self):
        super(Requisitioner, self).__init__()
        priests = [UnitType(self, Dominus),
                   UnitType(self, Cawl)]
        self.add_type_restriction(priests, 1, 1)
        UnitType(self, Breachers, min_limit=2, max_limit=3)


class EnginseerCongregation(Formation):
    army_id = 'cult_mechanicus_v1_enginseer_congregation'
    army_name = 'Enginseer Congregation'

    def __init__(self):
        super(EnginseerCongregation, self).__init__()
        UnitType(self, Techpriest, min_limit=1, max_limit=1)
        UnitType(self, Servitors, min_limit=0, max_limit=1)


class CultMechanicusBase(Wh40kBase):
    army_id = 'cult_mechanicus_v1_base'
    army_name = 'Cult Mechanicus'

    def __init__(self):

        super(CultMechanicusBase, self).__init__(
            hq=HQ(self), elites=Elites(self), troops=Troops(self),
            heavy=HeavySupport(self), fort=Fort(parent=self),
            lords=LordsOfWar(self)
        )


class CultMechanicusCAD(CultMechanicusBase, CombinedArmsDetachment):
    army_id = 'cult_mechanicus_v1_cad'
    army_name = 'Cult Mechanicus (Combined arms detachment)'


class CultMechanicusPA(CultMechanicusBase, PlanetstrikeAttacker):
    army_id = 'cult_mechanicus_v1_pa'
    army_name = 'Cult Mechanicus (Planetstrike attacker detachment)'


class CultMechanicusPD(CultMechanicusBase, PlanetstrikeDefender):
    army_id = 'cult_mechanicus_v1_pd'
    army_name = 'Cult Mechanicus (Planetstrike defender detachment)'


class CultMechanicusSA(CultMechanicusBase, SiegeAttacker):
    army_id = 'cult_mechanicus_v1_sa'
    army_name = 'Cult Mechanicus (Siege War attacker detachment)'


class CultMechanicusSD(CultMechanicusBase, SiegeDefender):
    army_id = 'cult_mechanicus_v1_sd'
    army_name = 'Cult Mechanicus (Siege War defender detachment)'


class BattleCongregation(CultMechanicusCAD):
    army_id = 'cult_mechanicus_v1_congregation'
    army_name = 'Cult Mechanicus Battle Congregation'

    def __init__(self):
        super(BattleCongregation, self).__init__()
        self.troops.max = 8
        self.elites.max = 4
        self.heavy.max = 2


class CultMechanicusKillTeam(Wh40kKillTeam):
    army_id = 'cult_mechanicus_v1_kt'
    army_name = 'Cult Mechanicus'

    def __init__(self):
        super(CultMechanicusKillTeam, self).__init__(
            Troops(self), Elites(self), None
        )


class Convocation(CompositeFormation):
    army_id = 'cult_mechanicus_v1_convocation'
    army_name = 'Adeptus Mechanicus War Convocation'

    class Maniple(Maniple):
        convocation = True

    class Oathsworn(Oathsworn):
        convocation = True

    class BattleCongregation(BattleCongregation):
        convocation = True

    def __init__(self):
        super(Convocation, self).__init__()
        Detachment.build_detach(self, self.BattleCongregation, min_limit=1, max_limit=1)
        Detachment.build_detach(self, self.Maniple, min_limit=1, max_limit=1)
        Detachment.build_detach(self, self.Oathsworn, min_limit=1, max_limit=1)


class CohortMechanicus(Formation):
    army_name = 'Cohort Mechanicus'
    army_id = 'cult_mechanicus_v1_cohort_mechanicus'

    def __init__(self):
        super(CohortMechanicus, self).__init__()

        from builder.games.wh40k.skitarii import Vanguard, Rangers, Ruststalkers,\
            Dunecrawlers, Ironstriders, Dragoons, Infiltrators
        priests = [UnitType(self, Dominus),
                   UnitType(self, Cawl)]
        self.add_type_restriction(priests, 1, 1)
        UnitType(self, Fulgurites, min_limit=1, max_limit=1)
        UnitType(self, Corpuscarii, min_limit=1, max_limit=1)
        UnitType(self, Breachers, min_limit=1, max_limit=1)
        UnitType(self, Destroyers, min_limit=1, max_limit=1)
        UnitType(self, KastelanManiple, min_limit=2, max_limit=2)
        UnitType(self, Vanguard, min_limit=1, max_limit=1)
        UnitType(self, Rangers, min_limit=1, max_limit=1)
        UnitType(self, Ruststalkers, min_limit=1, max_limit=1)
        UnitType(self, Infiltrators, min_limit=1, max_limit=1)
        UnitType(self, Dunecrawlers, min_limit=2, max_limit=2)
        self.striders = [
            UnitType(self, Ironstriders),
            UnitType(self, Dragoons)
        ]
        self.add_type_restriction(self.striders, 1, 1)

    def check_rules(self):
        super(CohortMechanicus, self).check_rules()
        for unittype in self.striders:
            for unit in unittype.units:
                if unit.count != 3:
                    self.error('A Cohort Mechanicus must include either 3 Sydonian Dragoons or 3 Ironstrider Ballistarii')


class DominusManiple(Formation):
    army_name = 'Dominus Maniple'
    army_id = 'cult_mechanicus_v1_dominus_maniple'

    def __init__(self):
        super(DominusManiple, self).__init__()

        from builder.games.wh40k.skitarii import Vanguard, Dunecrawlers, Rangers
        priests = [UnitType(self, Dominus),
                   UnitType(self, Cawl)]
        self.add_type_restriction(priests, 1, 1)
        infantry = [UnitType(self, Vanguard),
                    UnitType(self, Rangers)]
        self.add_type_restriction(infantry, 1, 1)
        self.dunecrawler = UnitType(self, Dunecrawlers, min_limit=1, max_limit=1)

    def check_rules(self):
        super(DominusManiple, self).check_rules()
        for unit in self.dunecrawler.units:
            if unit.count != 1:
                self.error('A Dominus Maniple must include 1 Onager Dunecrawlers')


class EradicationCohort(Formation):
    army_name = 'Eradication Cohort'
    army_id = 'cult_mechanicus_v1_eradication_cohort'

    def __init__(self):
        super(EradicationCohort, self).__init__()

        from builder.games.wh40k.skitarii import Dragoons, Infiltrators
        self.dragoons = UnitType(self, Dragoons, min_limit=1, max_limit=1)
        UnitType(self, KastelanManiple, min_limit=1, max_limit=1)
        UnitType(self, Destroyers, min_limit=1, max_limit=1)
        UnitType(self, Fulgurites, min_limit=1, max_limit=1)
        UnitType(self, Infiltrators, min_limit=1, max_limit=1)

    def check_rules(self):
        super(EradicationCohort, self).check_rules()
        for unit in self.dragoons.units:
            if unit.count != 1:
                self.error('A Eradication Cohort must include 1 Sydonian Dragoon')


class ConclaveAcquisitorius(CompositeFormation):
    army_name = 'Conclave Acquisitorius'
    army_id = 'cult_mechanicus_v1_conclave_acquisitorius'
    allow_cadia_arcana = True

    def allow_relics(self):
        return False

    def __init__(self):
        from builder.games.wh40k.imperial_knight_v2.lords import Paladin, Errant, Warden, Gallant, Crusader
        super(ConclaveAcquisitorius, self).__init__()
        self.cawl = UnitType(self, Cawl, min_limit=1, max_limit=1)
        skitarii = [
            Detachment.build_detach(self, Maniple),
            Detachment.build_detach(self, Cohort)
        ]
        self.add_type_restriction(skitarii, 1, 2)
        Detachment.build_detach(self, Requisitioner, min_limit=1, max_limit=1)
        Detachment.build_detach(self, CohortCybernetica, min_limit=0, max_limit=1)
        Detachment.build_detach(self, NuminousConclave, min_limit=0, max_limit=1)

        self.knights = [
            UnitType(self, Paladin),
            UnitType(self, Errant),
            UnitType(self, Warden),
            UnitType(self, Gallant),
            UnitType(self, Crusader),
        ]
        self.court = Detachment.build_detach(self, BaronialCourt, min_limit=0, max_limit=1)
        self.add_type_restriction(self.knights, 0, 2)

    def check_rules(self):
        super(ConclaveAcquisitorius, self).check_rules()
        sum_knights = sum([u.count for k in self.knights for u in k.units])
        sum_court = sum([u.count for u in self.court.units])
        if sum_knights + sum_court == 0:
            self.error('A Conclave Acquisitorius must include 1-2 Imperial Knights of any type or 1 Baronial Court')


class CultMechanicusAD(CultMechanicusBase, AlliedDetachment):
    army_id = 'cult_mechanicus_v1_ad'
    army_name = 'Cult Mechanicus (Allied detachment)'


faction = 'Cult Mechanicus'


class CultMechanicus(Wh40k7ed, Wh40kImperial):
    army_id = 'cult_mechanicus_v1'
    army_name = 'Cult Mechanicus'
    faction = faction
    # development = True

    def __init__(self):
        super(CultMechanicus, self).__init__([
            CultMechanicusCAD, BattleCongregation, EnginseerCongregation, CohortCybernetica,
            EliminationManiple, NuminousConclave, Requisitioner,
            Convocation, CohortMechanicus, DominusManiple, EradicationCohort,
            ConclaveAcquisitorius
        ], [CultMechanicusAD])


class CultMechanicusMissions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'cult_mechanicus_v1_mis'
    army_name = 'Cult Mechanicus'
    faction = faction
    # development = True

    def __init__(self):
        super(CultMechanicusMissions, self).__init__([
            CultMechanicusCAD, CultMechanicusPA, CultMechanicusPD, CultMechanicusSA, CultMechanicusSD,
            BattleCongregation, EnginseerCongregation, CohortCybernetica,
            EliminationManiple, NuminousConclave, Requisitioner,
            Convocation, CohortMechanicus, DominusManiple, EradicationCohort,
            ConclaveAcquisitorius
        ], [CultMechanicusAD])


detachments = [
    CultMechanicusCAD,
    CultMechanicusAD,
    CultMechanicusPA,
    CultMechanicusPD,
    CultMechanicusSA,
    CultMechanicusSD,
    BattleCongregation,
    EnginseerCongregation,
    CohortCybernetica,
    EliminationManiple,
    NuminousConclave,
    Requisitioner,
    Convocation,
    CohortMechanicus,
    DominusManiple,
    EradicationCohort,
    ConclaveAcquisitorius
]


unit_types = [
    Dominus,
    Techpriest,
    Cawl,
    Breachers,
    Destroyers,
    Fulgurites,
    Corpuscarii,
    Servitors,
    KastelanManiple
]
