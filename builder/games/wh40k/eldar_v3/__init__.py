from builder.core2 import OneOf, OptionsList
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, LordsOfWarSection, UnitType, Detachment, Formation,\
    FlyerSection
from builder.games.wh40k.fortifications import Fort

from builder.games.wh40k.escalation.eldar import LordsOfWar as EscLordsOfWar
from builder.games.wh40k.imperial_armour.volume11.eldar import IA11HQSection,\
    IA11HeavySection, IA11FastSection, IA11ElitesSection, IA11LordsOfWar,\
    UndyingHostLord, SpectresShrine, HawksSquadron, PhoenixSquadron,\
    WarpHunterSquadron, HornetSquadron, WaspPhalanx, HammerOfVaul, Skyhunters,\
    Skyreavers, WraithTitans, PaleCourtsBattlehost
from .hq import Eldrad, Yriel, Illic, JainZar, Asurmen, Karandras, Fuegan, \
    Baharroth, Maugan, Autarch, Farseer, Spiritseer, Warlocks
from .troops import GuardianDefenders, Windriders, Rangers, Avengers, \
    StormGuardians
from .elites import Banshees, Scorpions, Dragons, Wraithguards, Wraithblades,\
    BlackGuardians, BlackWindriders, BlackVypers, BlackWarWalkers
from .fast import WaveSerpent, Hawks, Spiders, Spears, CrimsonHunter, \
    VyperSquadron, HemlockWraithfighter, CrimsonHunters, Hemlocks
from .heavy import DarkReapers, Battery, Falcons, FirePrisms, NightSpinners, \
    WarWalkers, Wraithlord
from .lords import Wraithknight, Avatar
from builder.games.wh40k.ynnari import Yvraine, Visarch, Yncarne


__author__ = 'Denis Romanov'


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.eldrad = UnitType(self, Eldrad)
        self.yriel = UnitType(self, Yriel)
        self.illic = UnitType(self, Illic)
        self.asurmen = UnitType(self, Asurmen)
        self.jainzar = UnitType(self, JainZar)
        self.karandras = UnitType(self, Karandras)
        self.fuegan = UnitType(self, Fuegan)
        self.baharroth = UnitType(self, Baharroth)
        self.maugan = UnitType(self, Maugan)
        self.autarch = UnitType(self, Autarch)
        self.farseer = UnitType(self, Farseer)
        self.spiritseer = UnitType(self, Spiritseer)
        self.warlocks = UnitType(self, Warlocks)

    def check_limits(self):
        self.spiritseer.slot = 0.2 if self.roster.is_iyanden else 1.0
        return super(BaseHQ, self).check_limits()


class HQ(IA11HQSection, BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        UnitType(self, Yvraine)
        UnitType(self, Visarch)

    def check_rules(self):
        super(HQ, self).check_rules()
        for unit in self.autarch.units:
            unit.allow_spectre_weapons(self.roster.fast.spectres.count > 0)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.dragons = UnitType(self, Dragons)
        self.scorpions = UnitType(self, Scorpions)
        self.banshees = UnitType(self, Banshees)
        self.wraithguards = UnitType(self, Wraithguards)
        self.wraithblades = UnitType(self, Wraithblades)


class Elites(IA11ElitesSection, BaseElites):
    pass


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.guardians = UnitType(self, GuardianDefenders)
        self.stormguardians = UnitType(self, StormGuardians)
        self.avengers = UnitType(self, Avengers)
        self.windriders = UnitType(self, Windriders)
        self.rangers = UnitType(self, Rangers)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, WaveSerpent)
        hunter = UnitType(self, CrimsonHunter)
        hunter.visible = False
        UnitType(self, CrimsonHunters)
        self.vypersquadron = UnitType(self, VyperSquadron)
        hemlocks = UnitType(self, HemlockWraithfighter)
        hemlocks.visible = False
        UnitType(self, Hemlocks)
        self.spiders = UnitType(self, Spiders)
        self.spears = UnitType(self, Spears)
        self.hawks = UnitType(self, Hawks)


class FastAttack(IA11FastSection, BaseFastAttack):
    pass


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.falcons = UnitType(self, Falcons)
        self.WarWalkers = UnitType(self, WarWalkers)
        self.battery = UnitType(self, Battery)
        self.fireprisms = UnitType(self, FirePrisms)
        self.nightspinners = UnitType(self, NightSpinners)
        self.wraithlord = UnitType(self, Wraithlord)
        self.darkreapers = UnitType(self, DarkReapers)


class HeavySupport(IA11HeavySection, BaseHeavySupport):
    pass


class BaseLordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLordsOfWar, self).__init__(parent)
        UnitType(self, Wraithknight)
        UnitType(self, Avatar)


class LordsOfWar(IA11LordsOfWar, EscLordsOfWar, BaseLordsOfWar):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        UnitType(self, Yncarne)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [CrimsonHunters, Hemlocks])


class UlthweElites(ElitesSection):
    def __init__(self, parent):
        super(UlthweElites, self).__init__(parent, 1, 4)
        UnitType(self, BlackGuardians)
        UnitType(self, BlackWindriders)
        UnitType(self, BlackVypers)
        UnitType(self, BlackWarWalkers)


class IyandenFormation(Formation):

    class SupplementOptions(OneOf):
        def __init__(self, parent):
            super(IyandenFormation.SupplementOptions, self).__init__(parent=parent, name='Codex')
            self.base = self.variant(name=EldarV3Base.army_name)
            self.iyanden = self.variant(name='Iyanden')

    def __init__(self):
        super(IyandenFormation, self).__init__()
        self.codex = self.SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_iyanden(self):
        return self.codex.cur == self.codex.iyanden


class GuardianBattlehost(IyandenFormation):
    army_id = 'guardian_battlehost'
    army_name = 'Guardian Battlehost'

    class GuardianDefenders(GuardianDefenders):
        discount = True

    def __init__(self):
        super(GuardianBattlehost, self).__init__()
        UnitType(self, Farseer, min_limit=1, max_limit=1)
        UnitType(self, self.GuardianDefenders, min_limit=3, max_limit=3)
        UnitType(self, VyperSquadron, min_limit=1, max_limit=1)
        UnitType(self, WarWalkers, min_limit=1, max_limit=1)
        UnitType(self, Battery, min_limit=1, max_limit=1)
        UnitType(self, Warlocks, min_limit=0, max_limit=1)


class WindriderHost(IyandenFormation):
    army_id = 'windrider_host'
    army_name = 'Windrider Host'

    def __init__(self):
        super(WindriderHost, self).__init__()
        self.farseer = UnitType(self, Farseer, min_limit=1, max_limit=1)
        self.warlocks = UnitType(self, Warlocks, min_limit=1, max_limit=1)
        UnitType(self, Windriders, min_limit=3, max_limit=3)
        UnitType(self, VyperSquadron, min_limit=1, max_limit=1)

    def check_rules(self):
        super(WindriderHost, self).check_rules()
        for unit in self.farseer.units:
            if not unit.mobility.is_skyrunner():
                self.error('Farseer must be upgraded to a Farseer Skyrunner')
        for unit in self.warlocks.units:
            if not unit.mobility.is_skyrunner():
                self.error('All models in the Warlock Conclave must be upgraded to a Warlock Skyrunner')


class GuardianStormhost(IyandenFormation):
    army_id = 'guardian_stormhost'
    army_name = 'Guardian Stormhost'

    class StormGuardians(StormGuardians):
        discount = True

    def __init__(self):
        super(GuardianStormhost, self).__init__()
        UnitType(self, Farseer, min_limit=1, max_limit=1)
        UnitType(self, self.StormGuardians, min_limit=3, max_limit=3)
        UnitType(self, VyperSquadron, min_limit=1, max_limit=1)
        UnitType(self, WarWalkers, min_limit=1, max_limit=1)
        UnitType(self, Battery, min_limit=1, max_limit=1)
        UnitType(self, Warlocks, min_limit=0, max_limit=1)


class SeerCouncil(IyandenFormation):
    army_id = 'seer_council'
    army_name = 'Seer Council'

    class Warlocks(Warlocks):
        def check_rules(self):
            super(SeerCouncil.Warlocks, self).check_rules()
            if self.models.cur < 5:
                self.error("Must include at least 5 models")

    def __init__(self):
        super(SeerCouncil, self).__init__()
        self.farseers = [
            UnitType(self, Farseer),
            UnitType(self, Eldrad)
        ]
        self.add_type_restriction(self.farseers, 2, 2)
        self.warlocks = UnitType(self, self.Warlocks, min_limit=1, max_limit=1)

    def check_rules(self):
        super(SeerCouncil, self).check_rules()
        farseer_skyrunners = [u.is_skyrunner() for f in self.farseers for u in f.units]
        if any(farseer_skyrunners) != all(farseer_skyrunners):
            self.error("Both Farseer must be either Farseer or Farseer Skyrunner")
        if all(farseer_skyrunners) != all((u.is_skyrunner() for u in self.warlocks.units)):
            if all(farseer_skyrunners) is True:
                self.error("Warlocks mast also be upgrade to Warlocks Skyrunner")
            else:
                self.error("Warlocks Skyrunner mast also be downgrade to Warlocks")


class AspectHost(IyandenFormation):
    army_id = 'aspect_host'
    army_name = 'Aspect Host'

    def __init__(self):
        super(AspectHost, self).__init__()
        self.aspects = [
            UnitType(self, Avengers),
            UnitType(self, Banshees),
            UnitType(self, Scorpions),
            UnitType(self, Dragons),
            UnitType(self, Hawks),
            UnitType(self, Spiders),
            UnitType(self, Spears),
            UnitType(self, DarkReapers),
        ]
        self.add_type_restriction(self.aspects, 3, 3)

    def check_rules(self):
        super(AspectHost, self).check_rules()
        exarch_count = [u.exarch.count for a in self.aspects for u in a.units]
        if sum(exarch_count) != 3:
            self.error("Each unit in this Formation must include an Exarch")


class DireAvengerShrine(IyandenFormation):
    army_id = 'dire_avenger_shrine'
    army_name = 'Dire Avenger Shrine'

    def __init__(self):
        super(DireAvengerShrine, self).__init__()
        self.avengers = UnitType(self, Avengers, min_limit=3, max_limit=3)

    def check_rules(self):
        super(DireAvengerShrine, self).check_rules()
        exarch_count = [u.exarch.count for u in self.avengers.units]
        if sum(exarch_count) > 1:
            self.error("Only one unit in this Formation may include an Exarch")


class CrimsonDeatch(IyandenFormation):
    army_id = 'crimson_deatch'
    army_name = 'Crimson Death'

    def __init__(self):
        super(CrimsonDeatch, self).__init__()
        self.hunters = UnitType(self, CrimsonHunter, min_limit=3, max_limit=3)

    def check_rules(self):
        super(CrimsonDeatch, self).check_rules()
        exarch_count = [u.exarch.count for u in self.hunters.units]
        if sum(exarch_count) < 1:
            self.error("One Crimson Hunter must be upgraded to an Exarch")
        elif sum(exarch_count) > 1:
            self.error("Only one Crimson Hunter may be upgraded to an Exarch")


class Shroud(IyandenFormation):
    army_id = 'kurnous_shroud'
    army_name = 'Shroud of Kurnous'

    def __init__(self):
        super(Shroud, self).__init__()

        class Hunters(CrimsonHunters):
            unit_min = 2
            unit_max = 3

        UnitType(self, Hunters, min_limit=1, max_limit=1)
        UnitType(self, HemlockWraithfighter, min_limit=1, max_limit=1)


class WraithHost(IyandenFormation):
    army_id = 'wraith_host'
    army_name = 'Wraith Host'

    def __init__(self):
        super(WraithHost, self).__init__()
        UnitType(self, Spiritseer, min_limit=1, max_limit=1)
        self.wraith = [
            UnitType(self, Wraithguards),
            UnitType(self, Wraithblades)
        ]
        self.add_type_restriction(self.wraith, 3, 3)
        UnitType(self, Wraithlord, min_limit=1, max_limit=1)
        UnitType(self, Wraithknight, min_limit=1, max_limit=1)


class EldarV3Base(Wh40kBase):
    army_id = 'eldar_v3_base'
    army_name = 'Eldar'

    class SupplementOptions(OneOf):
        def __init__(self, parent):
            super(EldarV3Base.SupplementOptions, self).__init__(parent=parent, name='Codex')
            self.base = self.variant(name=EldarV3Base.army_name)
            self.iyanden = self.variant(name='Iyanden')

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)

        super(EldarV3Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

        self.codex = self.SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_iyanden(self):
        return self.codex.cur == self.codex.iyanden

    def has_illic(self):
        return self.hq.illic.count > 0


class CraftworldWarhost(Wh40k7ed):
    army_id = 'eldar_v3_warhost'
    army_name = 'Craftworld Warhost'

    class WarhostSubformation(IyandenFormation):
        def __init__(self):
            super(CraftworldWarhost.WarhostSubformation, self).__init__()
            self.codex.used = self.codex.visible = False

        @property
        def is_base(self):
            return self.parent.roster.is_base

        @property
        def is_iyanden(self):
            return self.parent.roster.is_iyanden

    @staticmethod
    def make_warhost(formation):
        class BaseSubformation(CraftworldWarhost.WarhostSubformation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            super(CraftworldWarhost.Core, self).__init__(parent, 'core', 'Core',
                                               [CraftworldWarhost.make_warhost(f) for f in
                                                [GuardianBattlehost, WindriderHost, GuardianStormhost]], 1, 3)
            self.dctype = self.types[0]

    class HeroesOfTheCraftworlds(WarhostSubformation):
        army_id = 'heroes_of_craftworlds'
        army_name = 'Heroes of the Craftworlds'

        def __init__(self):
            super(CraftworldWarhost.HeroesOfTheCraftworlds, self).__init__()
            self.heroes = [
                UnitType(self, Autarch),
                UnitType(self, Yriel),
                UnitType(self, Eldrad),
                UnitType(self, Illic),
            ]
            self.add_type_restriction(self.heroes, 1, 1)

    class LivingLegends(Formation):
        army_id = 'living_legends'
        army_name = 'Living Legends'

        def __init__(self):
            super(CraftworldWarhost.LivingLegends, self).__init__()
            self.legends = [
                UnitType(self, Avatar),
                UnitType(self, Asurmen),
                UnitType(self, JainZar),
                UnitType(self, Karandras),
                UnitType(self, Fuegan),
                UnitType(self, Baharroth),
                UnitType(self, Maugan)
            ]
            self.add_type_restriction(self.legends, 1, 1)

    class CommandSection(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(CraftworldWarhost.CommandSection, self).__init__(
                parent, 'command', 'Command',
                [CraftworldWarhost.HeroesOfTheCraftworlds,
                 CraftworldWarhost.make_warhost(SeerCouncil),
                 CraftworldWarhost.LivingLegends,
                 CraftworldWarhost.make_warhost(UndyingHostLord)], 0, 3)

        def check_limits(self):
            self.min = 0
            self.max = max([1, len(self.core.units)]) * 3
            return super(CraftworldWarhost.CommandSection, self).check_limits()

    class Outcasts(Formation):
        army_id = 'outcasts'
        army_name = 'Outcasts'

        def __init__(self):
            super(CraftworldWarhost.Outcasts, self).__init__()
            UnitType(self, Rangers, 1, 1)

    class EnginesOfVaul(Formation):
        army_id = 'engines_of_vaul'
        army_name = 'Engines of Vaul'

        def __init__(self):
            super(CraftworldWarhost.EnginesOfVaul, self).__init__()
            self.engines = [
                UnitType(self, NightSpinners),
                UnitType(self, FirePrisms),
                UnitType(self, Falcons)
            ]
            self.add_type_restriction(self.engines, 1, 1)

    class WraithConstructs(Formation):
        army_id = 'wraith_constructs'
        army_name = 'Wraith - Constructs'

        def __init__(self):
            super(CraftworldWarhost.WraithConstructs, self).__init__()
            self.constructs = [
                UnitType(self, HemlockWraithfighter),
                UnitType(self, Wraithlord),
                UnitType(self, Wraithknight)
            ]
            self.add_type_restriction(self.constructs, 1, 1)

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(CraftworldWarhost.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    CraftworldWarhost.make_warhost(f) for f in
                    [CraftworldWarhost.Outcasts,
                     AspectHost,
                     CrimsonDeatch,
                     DireAvengerShrine,
                     CraftworldWarhost.EnginesOfVaul,
                     WraithHost,
                     CraftworldWarhost.WraithConstructs,
                     SpectresShrine,
                     HawksSquadron,
                     PhoenixSquadron,
                     WarpHunterSquadron,
                     HornetSquadron,
                     WaspPhalanx]
                ], 1, 12)

        def check_limits(self):
            self.min = max([1, len(self.core.units)])
            self.max = max([1, len(self.core.units)]) * 12
            return super(CraftworldWarhost.Auxilary, self).check_limits()

    class Support(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(CraftworldWarhost.Support, self).__init__(
                parent, 'sup', 'Support', [
                    HammerOfVaul, Skyhunters,
                    Skyreavers, WraithTitans
                ], 0, 1)

        def check_limits(self):
            self.max = max([1, len(self.core.units)])
            return super(CraftworldWarhost.Support, self).check_limits()

    def __init__(self):
        self.core = self.Core(self)
        command = self.CommandSection(self, self.core)
        aux = self.Auxilary(self, self.core)
        sup = self.Support(self, self.core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, self.core, aux, sup], None)
        self.codex = EldarV3Base.SupplementOptions(self)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_iyanden(self):
        return self.codex.cur == self.codex.iyanden


class PaleCourtsWarhost(CraftworldWarhost):
    army_id = 'eldar_v3_pale_warhost'
    army_name = 'Warhost of the Pale Courts'
    imperial_armour = True

    class PaleCore(Detachment):
        def __init__(self, parent):
            super(PaleCourtsWarhost.PaleCore, self).__init__(parent, 'core', 'Core',
                                               [PaleCourtsBattlehost], 1, 3)
            self.palecourts = self.types[0]

        def allow_avengers(self):
            if self.palecourts.count > 0:
                court = self.palecourts.units[0].sub_roster.roster
                if court.traits.aspect.value:
                    return court.dire.count > 0
            return True

        def check_rules(self):
            if self.palecourts.count > 0:
                court = self.palecourts.units[0].sub_roster.roster
                for court2unit in self.palecourts.units[1:]:
                    court2 = court2unit.sub_roster.roster
                    if not OptionsList.same_options([court.traits, court2.traits]):
                        court2unit.error("Battlehost has Craftworld traits different from other in the same warhost")

        def is_strong(self):
            if self.palecourts.count > 0:
                court = self.palecourts.units[0].sub_roster.roster
                return court.traits.strong.value
            return False

        def get_traits(self):
            if self.palecourts.count > 0:
                court = self.palecourts.units[0].sub_roster.roster
                return court.traits
            return None

    class PaleAuxilary(CraftworldWarhost.Auxilary):
        def check_rules(self):
            super(PaleCourtsWarhost.PaleAuxilary, self).check_rules()
            if not self.core.allow_avengers():
                dire_avengers = [u for u in self.units if u.type_id == 'dire_avenger_shrine']
                if len(dire_avengers) > 0:
                    for u in dire_avengers:
                        u.error('Dire Avenger Shrines cannot be part of this detachment')

    def __init__(self):
        self.core = self.PaleCore(self)
        command = self.CommandSection(self, self.core)
        aux = self.PaleAuxilary(self, self.core)
        sup = self.Support(self, self.core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, self.core, aux, sup], None)
        self.codex = EldarV3Base.SupplementOptions(self)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        from builder.games.wh40k.dark_eldar_v2 import faction as de_faction
        from builder.games.wh40k.harlequins import faction as har_faction
        from builder.games.wh40k.corsairs_v2 import faction as cor_faction

        def strong_check(roster):
            own_traits = self.core.get_traits()
            for sec in roster.sections:
                for unit in sec.units:
                    if unit.faction in [de_faction, har_faction, cor_faction]:
                        roster.error("The Strong stand alone! Alliance including {} is not possible".format(unit.faction))
                        return
                    if unit.faction == 'Eldar':
                        if isinstance(unit.sub_roster.roster, PaleCourtsWarhost):
                            if not OptionsList.same_options([trait for trait in
                                                            [own_traits,
                                                             unit.sub_roster.roster.core.get_traits()]
                                                             if trait is not None]):
                                roster.error("The Strong Stand Alone! Alliance is only permitted with other pale courts warhosts detachments with similar craftworld traits")
                        else:
                            roster.error("The Strong Stand Alone! Alliance is only permitted with other pale courts warhosts detachments")
                            return

        if self.core.is_strong():
            self.root.extra_checks['strong_pale_court'] = strong_check

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_iyanden(self):
        return self.codex.cur == self.codex.iyanden


class RuneweaverHost(IyandenFormation):
    army_id = 'runeweaver_host'
    army_name = 'Runeweaver Host'

    def __init__(self):
        super(RuneweaverHost, self).__init__()
        self.farseer = UnitType(self, Farseer, min_limit=1, max_limit=1)
        UnitType(self, Windriders, min_limit=1, max_limit=1)

        class FirePrism(FirePrisms):
            def __init__(self, parent):
                super(FirePrism, self).__init__(parent)
                self.models.update_range(1, 1)

        UnitType(self, FirePrism, min_limit=1, max_limit=1)

    def check_rules(self):
        super(RuneweaverHost, self).check_rules()
        for unit in self.farseer.units:
            if not unit.mobility.is_skyrunner():
                self.error('Farseer must be upgraded to a Farseer Skyrunner')


class EldarV3CAD(EldarV3Base, CombinedArmsDetachment):
    army_id = 'eldar_v3_cad'
    army_name = 'Eldar (Combined arms detachment)'


class EldarV3AD(EldarV3Base, AlliedDetachment):
    army_id = 'eldar_v3_ad'
    army_name = 'Eldar (Allied detachment)'


class EldarV3PA(EldarV3Base, PlanetstrikeAttacker):
    army_id = 'eldar_v3_pa'
    army_name = 'Eldar (Planetstrike attacker detachment)'


class EldarV3PD(EldarV3Base, PlanetstrikeDefender):
    army_id = 'eldar_v3_pd'
    army_name = 'Eldar (Planetstrike defender detachment)'


class EldarV3SA(EldarV3Base, SiegeAttacker):
    army_id = 'eldar_v3_sa'
    army_name = 'Eldar (Siege War attacker detachment)'


class EldarV3SD(EldarV3Base, SiegeDefender):
    army_id = 'eldar_v3_sd'
    army_name = 'Eldar (Siege War defender detachment)'


class UlthweStrikeForce(Wh40kBase):
    army_id = 'eldar_v3_ulthwe'
    army_name = 'Ulthwe Strike Force'

    def __init__(self):
        self.elites = UlthweElites(parent=self)

        super(UlthweStrikeForce, self).__init__(elites=self.elites)


class FlierDetachment(Wh40kBase):
    army_id = 'eldar_v3_asd'
    army_name = 'Eldar (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class EldarV3KillTeam(Wh40kKillTeam):
    army_id = 'eldar_v3_kt'
    army_name = 'Eldar'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(EldarV3KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                GuardianDefenders, StormGuardians, Avengers
            ], lambda u: [u.transport.serpent])
            self.windriders = UnitType(self, Windriders)
            self.rangers = UnitType(self, Rangers)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(EldarV3KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                Dragons, Scorpions, Banshees, Wraithguards, Wraithblades
            ], lambda u: [u.transport.serpent])

    class KTFast(FastSection):
        def __init__(self, parent):
            super(EldarV3KillTeam.KTFast, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [VyperSquadron])
            self.spiders = UnitType(self, Spiders)
            self.spears = UnitType(self, Spears)
            self.hawks = UnitType(self, Hawks)

    def __init__(self):
        super(EldarV3KillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self),
            self.KTFast(self)
        )


faction = 'Eldar Craftworlds'


class EldarV3(Wh40kImperial, Wh40k7ed):
    army_id = 'eldar_v3'
    army_name = 'Eldar'
    faction = faction
    # development = True

    def __init__(self):
        super(EldarV3, self).__init__([
            EldarV3CAD,
            UlthweStrikeForce,
            FlierDetachment,
            CraftworldWarhost,
            PaleCourtsWarhost,
            GuardianBattlehost,
            WindriderHost,
            GuardianStormhost,
            SeerCouncil,
            AspectHost,
            DireAvengerShrine,
            CrimsonDeatch,
            WraithHost,
            Shroud,
            RuneweaverHost
        ])


class EldarV3Missions(Wh40kImperial, Wh40k7edMissions):
    army_id = 'eldar_v3_mis'
    army_name = 'Eldar'
    faction = faction
    # development = True

    def __init__(self):
        super(EldarV3Missions, self).__init__([
            EldarV3CAD,
            EldarV3PA,
            EldarV3PD,
            EldarV3SA,
            EldarV3SD,
            UlthweStrikeForce,
            FlierDetachment,
            CraftworldWarhost,
            PaleCourtsWarhost,
            GuardianBattlehost,
            WindriderHost,
            GuardianStormhost,
            SeerCouncil,
            AspectHost,
            DireAvengerShrine,
            CrimsonDeatch,
            WraithHost,
            Shroud,
            RuneweaverHost
        ])


detachments = [
    EldarV3CAD,
    EldarV3AD,
    EldarV3PA,
    EldarV3PD,
    EldarV3SA,
    EldarV3SD,
    UlthweStrikeForce,
    FlierDetachment,
    CraftworldWarhost,
    PaleCourtsWarhost,
    GuardianBattlehost,
    WindriderHost,
    GuardianStormhost,
    SeerCouncil,
    AspectHost,
    DireAvengerShrine,
    CrimsonDeatch,
    WraithHost,
    Shroud,
    RuneweaverHost
]


unit_types = [
    Eldrad, Yriel, Illic, JainZar, Asurmen, Karandras, Fuegan,
    Baharroth, Maugan, Autarch, Farseer, Spiritseer, Warlocks,
    GuardianDefenders, Windriders, Rangers, Avengers,
    StormGuardians,
    Banshees, Scorpions, Dragons, Wraithguards, Wraithblades,
    WaveSerpent, Hawks, Spiders, Spears, CrimsonHunter,
    VyperSquadron, HemlockWraithfighter, CrimsonHunters, Hemlocks,
    DarkReapers, Battery, Falcons, FirePrisms, NightSpinners,
    WarWalkers, Wraithlord,
    Wraithknight, Avatar, BlackGuardians, BlackWindriders,
    BlackVypers, BlackWarWalkers
]
