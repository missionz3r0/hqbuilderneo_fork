__author__ = 'dvarh'
from builder.core2 import Gear, OptionsList, UnitList,\
    ListSubUnit, Count, UnitDescription, OneOf, SubUnit, OptionalSubUnit
from builder.games.wh40k.unit import Unit, Squadron
from .armory import VehicleEquipment, Ghostwalk


class WaveSerpent(Unit):
    type_name = 'Wave Serpent'
    type_id = 'wave_serpent_v3'

    class TopWeapon(OneOf):
        def __init__(self, parent):
            super(WaveSerpent.TopWeapon, self).__init__(parent, 'Top Weapon')
            self.variant('Twin-linked shuriken cannon', 0)
            self.variant('Twin-linked bright lance', 5)
            self.variant('Twin-linked scatter laser', 5)
            self.variant('Twin-linked starcannon', 5)
            self.variant('Twin-linked Eldar missile launcher', 15)

    class BottomWeapon(OneOf):
        def __init__(self, parent):
            super(WaveSerpent.BottomWeapon, self).__init__(parent, 'Bottom Weapon')
            self.variant('Twin-linked shuriken catapult', 0)
            self.variant('Shuriken cannon', 10)

    def __init__(self, parent):
        super(WaveSerpent, self).__init__(parent, self.type_name, 110, [Gear('Serpent Shield')])
        self.topwep = self.TopWeapon(self)
        self.bottomwep = self.BottomWeapon(self)
        self.opt = VehicleEquipment(self, squadron=False)


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(
            parent, 'Transport', 1)
        self.serpent = SubUnit(self, WaveSerpent(parent=None))


class Hawks(Unit):
    type_name = 'Swooping Hawks'
    type_id = 'hawks_v3'
    default_min = 5
    default_max = 10
    model_points = 16
    model_gear = [Gear('Aspect armor'), Gear('Lasblaster'), Gear('Haywire grenades'), Gear('Plasma grenades'), Gear('Grenade pack'), Gear('Swooping Hawk wings')]

    class Exarch(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(Hawks.Exarch.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Lasblaster', 0)
                self.variant('Hawk\'s talon', 10)
                self.variant('Sunrifle', 15)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Hawks.Exarch.Options, self).__init__(parent, 'Options')
                self.variant('Power weapon', 10)

        def __init__(self, parent):
            super(Hawks.Exarch, self).__init__(parent, name='Swooping Hawk Exarch', points=Hawks.model_points + 10,
                                               gear=[Gear('Aspect Armor'), Gear('Haywire grenades'), Gear('Plasma grenades'), Gear('Grenade pack'), Gear('Swooping Hawk wings')])
            self.Weapons(self)
            self.Options(self)

    class HawksExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(Hawks.HawksExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, Hawks.Exarch(parent=None))

    def __init__(self, parent):
        super(Hawks, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Hawk', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Hawk', self.model_points, options=self.model_gear), per_model=True)
        self.exarch = self.HawksExarch(self)

    def get_count(self):
        return self.models.cur + self.exarch.count

    def check_rules(self):
        self.models.min = self.default_min - self.exarch.count
        self.models.max = self.default_max - self.exarch.count


class Spiders(Unit):
    type_name = 'Warp Spiders'
    type_id = 'spiders_v3'
    default_min = 5
    default_max = 10
    model_points = 19
    model_gear = [Gear('Heavy Aspect armor'), Gear('Death spinner'), Gear('Warp jump generator')]

    class Exarch(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(Spiders.Exarch.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Death spinner', 0)
                self.variant('Twin-linked death spinner', 5)
                self.variant('Spinneret rifle', 15)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Spiders.Exarch.Options, self).__init__(parent, 'Options')
                self.variant('Pair of powerblades', 20)

        def __init__(self, parent):
            super(Spiders.Exarch, self).__init__(parent, name='Warp Spider Exarch', points=Spiders.model_points + 10,
                                               gear=[Gear('Heavy Aspect armor'), Gear('Warp jump generator')])
            self.Weapons(self)
            self.Options(self)

    class SpidersExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(Spiders.SpidersExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, Spiders.Exarch(parent=None))

    def __init__(self, parent):
        super(Spiders, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Spider', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Spider', self.model_points, options=self.model_gear), per_model=True)
        self.exarch = self.SpidersExarch(self)

    def get_count(self):
        return self.models.cur + self.exarch.count

    def check_rules(self):
        self.models.min = self.default_min - self.exarch.count
        self.models.max = self.default_max - self.exarch.count


class Spears(Unit):
    type_name = 'Shining Spears'
    type_id = 'spears_v3'
    default_min = 3
    default_max = 9
    model_points = 25
    model_gear = [Gear('Heavy Aspect armor'), Gear('Laser lance'), Gear('Eldar jetbike')]

    class Exarch(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(Spears.Exarch.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Laser lance', 0)
                self.variant('Power weapon', 0)
                self.variant('Star lance', 10)

        def __init__(self, parent):
            super(Spears.Exarch, self).__init__(parent, name='Shining Spear Exarch', points=Spears.model_points + 10,
                                               gear=[Gear('Heavy Aspect armor'), Gear('Eldar jetbike')])
            self.Weapons(self)

    class SpearsExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(Spears.SpearsExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, Spears.Exarch(parent=None))

    def __init__(self, parent):
        super(Spears, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Spear', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Spear', self.model_points, options=self.model_gear), per_model=True)
        self.exarch = self.SpearsExarch(self)

    def get_count(self):
        return self.models.cur + self.exarch.count

    def check_rules(self):
        self.models.min = self.default_min - self.exarch.count
        self.models.max = self.default_max - self.exarch.count


class CrimsonHunter(Unit):
    type_name = 'Crimson Hunter'
    type_id = 'hunter_v3'
    cost = 140

    class Exarch(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(CrimsonHunter.Exarch.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Bright lances (x2)', 0)
                self.variant('Starcannon', 0)

        def __init__(self, parent):
            super(CrimsonHunter.Exarch, self).__init__(parent, name='Crimson Hunter Exarch', points=CrimsonHunter.cost + 20,
                                               gear=[Gear('Pulse laser')])
            self.Weapons(self)

    class CrimsonHunterExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(CrimsonHunter.CrimsonHunterExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, CrimsonHunter.Exarch(parent=None))

    def __init__(self, parent):
        super(CrimsonHunter, self).__init__(parent, self.type_name, 140, gear=[Gear('Bright lances', count=2), Gear('Pulse laser')])
        self.exarch = self.CrimsonHunterExarch(self)

    def build_points(self):
        pts = self.cost
        if self.is_exarch:
            pts = self.exarch.points
        return pts

    def build_description(self):
        desc = super(CrimsonHunter, self).build_description()
        if self.is_exarch:
            desc = self.exarch.description[0]
        return desc

    @property
    def is_exarch(self):
        return self.exarch.any


class CrimsonHunters(Unit):
    type_name = 'Crimson Hunters'
    type_id = 'hunters_v3'

    class Hunter(CrimsonHunter, ListSubUnit):
        @ListSubUnit.count_gear
        def exarch_flag(self):
            return self.is_exarch

    unit_min = 1
    unit_max = 4

    def __init__(self, parent):
        super(CrimsonHunters, self).__init__(parent)
        self.vehicles = UnitList(self, self.Hunter, self.unit_min, self.unit_max)

    def get_count(self):
        return self.vehicles.count

    def check_rules(self):
        super(CrimsonHunters, self).check_rules()
        exarch_count = sum(u.exarch_flag() for u in self.vehicles.units)
        if exarch_count > 1:
            self.error("No more then one Crimson Hunter may be upgraded to exarch")


class VyperSquadron(Unit):
    type_id = 'vypers_v3'
    type_name = 'Vypers squadron'

    class Vyper(ListSubUnit):
        name = 'Vyper'
        base_points = 40

        class TopWeapon(OneOf):
            def __init__(self, parent):
                super(VyperSquadron.Vyper.TopWeapon, self).__init__(parent, 'Top Weapon')
                self.variant('Shuriken cannon', 0)
                self.variant('Starcannon', 5)
                self.variant('Bright lance', 10)
                self.variant('Scatter laser', 10)
                self.variant('Eldar missile launcher', 15)

        class BottomWeapon(OneOf):
            def __init__(self, parent):
                super(VyperSquadron.Vyper.BottomWeapon, self).__init__(parent, 'Bottom Weapon')
                self.variant('Twin-linked shuriken catapult', 0)
                self.variant('Shuriken cannon', 10)

        def __init__(self, parent):
            super(VyperSquadron.Vyper, self).__init__(parent, self.name, self.base_points)
            self.topwep = self.TopWeapon(self)
            self.bottomwep = self.BottomWeapon(self)
            self.opt = VehicleEquipment(self)

        def build_description(self):
            desc = super(VyperSquadron.Vyper, self).build_description()
            if self.root_unit.matrix.used and self.root_unit.matrix.any:
                desc.add(Gear('Ghostwalk matrix'))
                desc.add_points(self.root_unit.matrix.points)
            return desc

    def __init__(self, parent):
        super(VyperSquadron, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Vyper, 1, 5)
        self.matrix = Ghostwalk(parent=self)

    def build_points(self):
        return super(VyperSquadron, self).build_points() + self.matrix.points * (self.models.count - 1) * self.matrix.used


class HemlockWraithfighter(Unit):
    type_name = 'Hemlock Wraithfighter'
    type_id = 'hemlock_wraithfighter_v3'

    def __init__(self, parent):
        super(HemlockWraithfighter, self).__init__(
            parent,
            self.type_name,
            185,
            [Gear('Heavy D-scythes', count=2), Gear('Mindshock pod'), Gear('Spirit stones')]
        )

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(HemlockWraithfighter, self).build_statistics())


class Hemlocks(Squadron):
    type_name = 'Hemlock Wraithfighters'
    type_id = 'hemlocks_v3'
    unit_class = HemlockWraithfighter
    unit_max = 4

    def count_charges(self):
        return 2 * self.get_count()

    def build_statistics(self):
        return self.note_charges(super(Hemlocks, self).build_statistics())
