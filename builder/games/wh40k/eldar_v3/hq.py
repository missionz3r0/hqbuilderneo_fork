__author__ = 'dvarh'

from builder.core2 import *
from builder.games.wh40k.roster import Unit
from .armory import YnnariRelics, RelicBlade, RelicPistol,\
    Pistol


class Eldrad(Unit):
    type_name = 'Eldrad Ulthran, High Farseer of Ulthwe'
    type_id = 'eldradultran_v3'

    def __init__(self, parent):
        super(Eldrad, self).__init__(parent=parent, name='Eldrad Ulthran',
                                     points=195, unique=True, gear=[
                                         Gear('Armour of the Last Runes'),
                                         Gear('Staff of Ulthamar'),
                                         Gear('Shuriken pistol'),
                                         Gear('Witchblade'),
                                         Gear('Ghosthelm'),
                                     ], static=True)

    def is_skyrunner(self):
        return False

    def count_charges(self):
        return 4

    def build_statistics(self):
        return self.note_charges(super(Eldrad, self).build_statistics())


class Yriel(Unit):
    type_name = 'Prince Yriel, Autarch of Iyanden'
    type_id = 'princeyriel_v3'

    def __init__(self, parent):
        super(Yriel, self).__init__(parent=parent, name='Prince Yriel',
                                      points=140, unique=True, gear=[
                                          Gear('The Eye of Wrath'),
                                          Gear('The Spear of Twillight'),
                                          Gear('Heave Aspect armor'),
                                          Gear('Forceshield'),
                                          Gear('Plasma grenades'),
                                      ], static=True)


class Illic(Unit):
    type_name = 'Illic Nightspear, The Walker of the Hidden Path'
    type_id = 'illicnightspear_v3'

    def __init__(self, parent):
        super(Illic, self).__init__(parent=parent, name='Illic Nightspear',
                                    points=140, unique=True, gear=[
                                        Gear('Voidbringer'),
                                        Gear('Mesh armor'),
                                        Gear('Shuriken pistol'),
                                        Gear('Power sword'),
                                    ], static=True)


class Asurmen(Unit):
    type_name = 'Asurmen, The Hand of Asuryan'
    type_id = 'asurmen_v3'

    def __init__(self, parent):
        super(Asurmen, self).__init__(parent=parent, name='Asurmen',
                                      points=220, unique=True, gear=[
                                          Gear('The Sword of Asur'),
                                          Gear('Phoenix armor'),
                                          Gear('Twin-linked avenger shuriken catapult'),
                                          Gear('Plasma grenades'),
                                      ], static=True)


class JainZar(Unit):
    type_name = 'Jain Zar, The Storm of Silence'
    type_id = 'jainzar_v3'

    def __init__(self, parent):
        super(JainZar, self).__init__(parent=parent, name='Jain Zar',
                                      points=200, unique=True, gear=[
                                          Gear('Death and Destruction'),
                                          Gear('Mask of Jain Zar'),
                                          Gear('Phoenix armor'),
                                      ], static=True)


class Karandras(Unit):
    type_name = 'Kaarandras, The Shadow Hunter'
    type_id = 'karandras_v3'

    def __init__(self, parent):
        super(Karandras, self).__init__(parent=parent, name='Karandras',
                                        points=200, unique=True, gear=[
                                            Gear('The Scorpion\'s Bite'),
                                            Gear('Phoenix armor'),
                                            Gear('Scorpion chainsword'),
                                            Gear('Scorpion\'s claw'),
                                            Gear('Plasma grenades'),
                                        ], static=True)


class Fuegan(Unit):
    type_name = 'Fuegan, The Burning Lance'
    type_id = 'fuegan_v3'

    def __init__(self, parent):
        super(Fuegan, self).__init__(parent=parent, name='Fuegan',
                                      points=220, unique=True, gear=[
                                          Gear('Fire Axe'),
                                          Gear('Phoenix armor'),
                                          Gear('Firepike'),
                                          Gear('Melta bombs'),
                                      ], static=True)


class Baharroth(Unit):
    type_name = 'Baharroth, The Cry of the Wind'
    type_id = 'baharroth_v3'

    def __init__(self, parent):
        super(Baharroth, self).__init__(parent=parent, name='Baharroth',
                                        points=170, unique=True, gear=[
                                            Gear('The Shining Blade'),
                                            Gear('Phoenix armor'),
                                            Gear('Hawk\'s talon'),
                                            Gear('Haywire grenades'),
                                            Gear('Plasma grenades'),
                                            Gear('Grenade pack'),
                                            Gear('Swooping Hawk wings'),
                                        ], static=True)


class Maugan(Unit):
    type_name = 'Maugan Ra, The Harvester of Souls'
    type_id = 'mauganra_v3'

    def __init__(self, parent):
        super(Maugan, self).__init__(parent=parent, name='Maugan Ra',
                                      points=195, unique=True, gear=[
                                          Gear('The Maugetar'),
                                          Gear('Phoenix armor'),
                                      ], static=True)


class Gifted(Unit):
    def __init__(self, *args, **kwargs):
        super(Gifted, self).__init__(*args, **kwargs)
        self.relics = []

    @property
    def has_gifts(self):
        return any(relic.has_gifts for relic in self.relics)

    @property
    def has_remnants(self):
        return any(relic.has_remnants for relic in self.relics)

    def check_rules(self):
        super(Gifted, self).check_rules()
        for o in self.relics:
            o.show_gifts(self.roster.is_iyanden)
            o.activate_remnants(not self.has_gifts or not self.roster.is_iyanden)
            o.activate_gifts(not self.has_remnants)


class Autarch(Gifted):
    type_name = 'Autarch'
    type_id = 'autarch_v3'

    class PistolWeapon(Pistol):
        def __init__(self, parent):
            super(Autarch.PistolWeapon, self).__init__(parent)
            self.variant('Fusion pistol', 10)

    class ArtefactPistol(RelicPistol, PistolWeapon):
        pass

    class Helmet(OptionsList):
        def __init__(self, parent):
            super(Autarch.Helmet, self).__init__(parent, name='Helmet')
            self.variant('Banshee mask', 5)
            self.variant('Mandiblasters', 10)

    class Mobility(OptionsList):
        def __init__(self, parent):
            super(Autarch.Mobility, self).__init__(parent, name='Mobility', limit=1)
            self.variant('Warp jump generator', 10)
            self.variant('Swooping Hawk wings', 15)
            self.skyrunner = self.variant('Autarch Skyrunner', 15)
            self.spectre = self.variant('Shadow Spectre jet pack and Spectre holo-field', 20,
                                        gear=[Gear('Shadow Spectre jet pack'), Gear('Spectre holo-field')])

        def is_skyrunner(self):
            return self.skyrunner.value

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Autarch.Weapon, self).__init__(parent, name='Weapons', limit=2)
            self.variant('Scorpion chainsword', 3)
            self.variant('Avenger shuriken catapult', 5)
            self.variant('Lasblaster', 5)
            self.variant('Deathspinner', 10)
            self.variant('Fusion gun', 10)
            self.lance = self.variant('Laser lance', 10)
            self.variant('Power weapon', 15)
            self.variant('Reaper launcher with starswarm misslies', 25)
            self.prism = self.variant('Prism rifle', 10)

    class ArtefactWeapon(RelicBlade, Weapon):
        pass

    def __init__(self, parent):
        gear = [
            Gear('Heavy Aspect armour'),
            Gear('Haywire grenades'),
            Gear('Plasma grenades'),
            Gear('Forceshield'),
        ]
        super(Autarch, self).__init__(parent=parent, points=70, gear=gear)
        self.mobility = self.Mobility(self)
        self.art = YnnariRelics(self, seer=False)
        self.wep = self.ArtefactWeapon(self)

        self.Helmet(self)
        self.pist = self.ArtefactPistol(self)
        if self.mobility.is_skyrunner():
            self.gear.append(Gear('Eldar jetbike'))

        self.relics = [self.wep, self.pist, self.art]

    def check_rules(self):
        super(Autarch, self).check_rules()
        self.wep.lance.visible = self.mobility.is_skyrunner()

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.art.get_unique() + self.wep.get_unique() +\
            self.pist.get_unique()

    def allow_spectre_weapons(self, flag):
        self.wep.prism.used = self.wep.prism.visible = flag
        self.mobility.spectre.used = self.mobility.spectre.visible = flag


class Farseer(Gifted):
    type_name = 'Farseer'
    type_id = 'farseer_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Farseer.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Witchblade', 0)
            self.variant('Sinhing spear', 5)

    class ArtefactWeapon(RelicBlade, Weapon):
        pass

    class ArtefactPistol(RelicPistol, Pistol):
        pass

    class Mobility(OptionsList):
        def __init__(self, parent):
            super(Farseer.Mobility, self).__init__(parent, name='Mobility', limit=1)
            self.skyrunner = self.variant('Farseer Skyrunner', 15)

        def is_skyrunner(self):
            return self.skyrunner.value

    def __init__(self, parent):

        gear = [
            Gear('Rune armour'),
            Gear('Ghosthelm'),
        ]
        super(Farseer, self).__init__(parent=parent, points=100, gear=gear)
        self.mobility = self.Mobility(self)
        self.art = YnnariRelics(self, seer=True)
        self.wep = self.ArtefactWeapon(self, psyker=True)
        self.pist = self.ArtefactPistol(self, psyker=True)
        if self.mobility.is_skyrunner():
            self.gear.append(Gear('Eldar jetbike'))

        self.relics = [self.wep, self.pist, self.art]

    def check_rules(self):
        super(Farseer, self).check_rules()

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.art.get_unique() + self.wep.get_unique() +\
            self.pist.get_unique()

    def is_skyrunner(self):
        return self.mobility.is_skyrunner()

    def count_charges(self):
        return 3

    def build_statistics(self):
        return self.note_charges(super(Farseer, self).build_statistics())


class Warlocks(Unit):
    type_name = 'Warlock Conclave'
    type_id = 'warlocks_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Warlocks.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Whitchblade', 0)
            self.variant('Singing spear', 5)

    class Mobility(OptionsList):
        def __init__(self, parent):
            super(Warlocks.Mobility, self).__init__(parent, name='Mobility', limit=1)
            self.skyrunner = self.variant('Warlock Skyrunner', 15)

        def is_skyrunner(self):
            return self.skyrunner.value

    def __init__(self, parent):
        super(Warlocks, self).__init__(parent)
        self.mobility = self.Mobility(self)
        self.models = Count(self, 'Warlock Conclave', 1, 10, 35, gear=[])
        self.spear = Count(self, 'Sinhing spear',
                           min_limit=0, max_limit=1, points=5, gear=[])

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return self.models.points + self.models.cur * self.mobility.points\
            + self.spear.points

    def check_rules(self):
        super(Warlocks, self).check_rules()
        self.spear.max = self.models.cur

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, count=self.get_count())
        gear = [
            Gear('Rune armour'),
            Gear('Shuriken pistol'),
        ]
        if self.mobility.is_skyrunner():
            gear.append(Gear('Eldar jetbike'))
        model = UnitDescription('Warlocks', 35 + self.mobility.points,
                                options=gear)
        desc.add(model.clone().add([Gear('Singing spear')]).add_points(5)
                 .set_count(self.spear.cur))
        desc.add(model.add([Gear('Witchblade')])
                 .set_count(self.models.cur - self.spear.cur))
        return desc

    def is_skyrunner(self):
        return self.mobility.is_skyrunner()

    def count_charges(self):
        return self.models.cur

    def build_statistics(self):
        return self.note_charges(super(Warlocks, self).build_statistics())


class Spiritseer(Gifted):
    type_name = 'Spiritseer'
    type_id = 'spiritseer_v3'

    class Staff(OneOf):
        def __init__(self, parent):
            super(Spiritseer.Staff, self).__init__(parent, 'Melee weapon')
            self.variant('Witch staff')

    class ArtefactWeapon(RelicBlade, Staff):
        pass

    class ArtefactPistol(RelicPistol, Pistol):
        pass

    def __init__(self, parent):
        gear = [
            Gear('Rune armour'),
        ]
        super(Spiritseer, self).__init__(parent=parent, points=70, gear=gear)
        self.wep = self.ArtefactWeapon(self, psyker=True)
        self.pist = self.ArtefactPistol(self, psyker=True)
        self.art = YnnariRelics(self, seer=True)

        self.relics = [self.wep, self.pist, self.art]

    def check_rules(self):
        super(Spiritseer, self).check_rules()

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.art.get_unique() + self.wep.get_unique() +\
            self.pist.get_unique()

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Spiritseer, self).build_statistics())
