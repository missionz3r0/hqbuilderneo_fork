__author__ = 'Ivan Truskov'

from builder.core2 import OneOf, Gear, UnitList, OptionsList,\
    Count, ListSubUnit, OptionalSubUnit, SubUnit, UnitDescription
from .armory import VehicleEquipment, Melee, Carbine, Special, Heavy,\
    Pistol, NoWeapon
from builder.games.wh40k.unit import Unit, Squadron


class Raider(Unit):
    type_name = 'Raider'
    type_id = 'raider'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Raider.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Disintegrator cannon', 0)
            self.variant('Dark lance', 5)

    def __init__(self, parent):
        super(Raider, self).__init__(parent, self.type_name, 55)
        self.Weapon(self)
        VehicleEquipment(self)


class Venom(Unit):
    type_name = 'Venom'
    type_id = 'venom'
    base_points = 55

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Venom.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked splinter rifle', 0)
            self.variant('Splinter cannon', 10)

    def __init__(self, parent):
        super(Venom, self).__init__(parent, self.type_name, self.base_points, [Gear('Splinter cannon'), Gear('Flickerfield')])
        self.Weapon(self)
        VehicleEquipment(self, is_venom=True)


class Beastmasters(Unit):
    type_name = 'Beastmasters'
    type_id = 'beasts'

    class Beastmaster(ListSubUnit):

        def __init__(self, parent):
            super(Beastmasters.Beastmaster, self)\
                .__init__(parent, 'Beastmaster', 10, gear=[Gear('Wychsuit'), Gear('Skyboard')])
            self.wep = Melee(self)

    class Characters(OptionsList):
        def __init__(self, parent):
            super(Beastmasters.Characters, self).__init__(parent, '')
            self.bmaster = self.variant('Beastmasters')
            self.bmasters = UnitList(parent, Beastmasters.Beastmaster, 1, 12)
            self.bmasters.used = False

        def get_count(self):
            return self.bmasters.count if self.bmaster.value else 0

        def check_rules(self):
            super(Beastmasters.Characters, self).check_rules()
            self.bmasters.used = self.bmasters.visible = self.bmaster.value

    def __init__(self, parent):
        super(Beastmasters, self).__init__(parent, 'Beastmasters')
        self.chars = self.Characters(self)
        self.models = [
            Count(self, name=m['name'], min_limit=0,
                  max_limit=12, per_model=True, points=m['points'],
                  gear=UnitDescription(name=m['name'], points=m['points']))
            for m in
            (
                dict(name='Khymera', points=10),
                dict(name='Razorwing Flock', points=20),
                dict(name='Clawed Fiend', points=30)
            )]

    def get_count(self):
        return self.chars.get_count() + sum(c.cur for c in self.models)

    def check_rules(self):
        cm = self.chars.get_count()
        Count.norm_counts(0, 12 - cm, self.models)
        if self.get_count() < 1 or self.get_count() > 12:
            self.error('Beastmaster unit must consist from 1-12 models; taken: {}'.
                       format(self.get_count()))


class Reavers(Unit):
    type_name = 'Reavers'
    type_id = 'reavers'
    common_gear = [Gear('Wychsuit'), Gear('Splinter pistol'), Gear('Reaver jetbike'), Gear('Bladevanes')]

    class Options(OptionsList):
        def __init__(self, parent):
            super(Reavers.Options, self).__init__(parent, 'Options', limit=1)
            self.variant('Grav-talon', 5)
            self.variant('Cluster caltrops', 15)

    class Champ(Unit):
        def __init__(self, parent):
            super(Reavers.Champ, self).__init__(parent, 'Arena Champion', 26, Reavers.common_gear + [Gear('Splinter rifle')])
            Melee(self)
            self.opt = Reavers.Options(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Reavers.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            self.achamp = SubUnit(self, Reavers.Champ(parent=None))

    class Reaver(ListSubUnit):

        class Ranged(OneOf):
            def __init__(self, parent):
                super(Reavers.Reaver.Ranged, self).__init__(parent, 'Jetbike weapon')
                self.rifle = self.variant('Splinter rifle', 0)
                self.variant('Heat lance', 10)
                self.variant('Blaster', 10)

        def __init__(self, parent):
            super(Reavers.Reaver, self).__init__(parent, 'Reaver', 16, Reavers.common_gear + [Gear('Close combat weapon')])
            self.bw = self.Ranged(self)
            self.opt = Reavers.Options(self)

        @ListSubUnit.count_gear
        def count_bw(self):
            return self.bw.cur != self.bw.rifle

        @ListSubUnit.count_gear
        def count_opt(self):
            return self.opt.any

    def __init__(self, parent):
        super(Reavers, self).__init__(parent, 'Reavers')
        self.warriors = UnitList(self, self.Reaver, 3, 12, order=5)
        self.ldr = self.Boss(self)

    def get_count(self):
        return self.warriors.count + self.ldr.count

    def check_rules(self):
        lcnt = self.ldr.count
        self.warriors.min = 3 - lcnt
        self.warriors.max = 12 - lcnt

        bweps = sum(u.count_bw() for u in self.warriors.units)
        if bweps > self.get_count() / 3:
            self.error('Only 1 special bike weapon for 3 models allowed; taken: {}'.format(bweps))

        opts = sum(u.count_opt() for u in self.warriors.units)\
            + (1 if self.ldr.achamp.unit.opt.any else 0)
        if opts > self.get_count() / 3:
            self.error('Only 1 piece of equipment per 3 models allowed; taken: {}'.format(opts))


class Hellions(Unit):
    type_name = 'Hellions'
    type_id = 'kellions'

    class Helliarch(Unit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(Hellions.Helliarch.Options, self).__init__(parent, 'Options')
                self.variant('Phantasm grenade launcher', 15)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Hellions.Helliarch.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Hellglaive', 0)
                self.variant('Splinter pistol and stunclaw', 10,
                             gear=[Gear('Splinter pistol'), Gear('Stunclaw')])
                self.variant('Splinter pistol and power sword', 15,
                             gear=[Gear('Splinter pistol'), Gear('Power sword')])
                self.variant('Splinter pistol and agonizer', 20,
                             gear=[Gear('Splinter pistol'), Gear('Agonizer')])

        def __init__(self, parent):
            super(Hellions.Helliarch, self).__init__(parent, 'Helliarch', 23, gear=[Gear('Hellion skyboard')])
            self.Weapon(self)
            self.Options(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Hellions.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            SubUnit(self, Hellions.Helliarch(parent=None))

    def __init__(self, parent):
        super(Hellions, self).__init__(parent, 'Hellions')
        self.warriors = Count(self, 'Hellions', 5, 15, 13, True, order=10,
                              gear=UnitDescription('Hellion', 13, options=[Gear('Close combat weapon'), Gear('Hellglaive'), Gear('Hellion skyboard')]))
        self.leader = self.Boss(self)

    def check_rules(self):
        lcnt = self.leader.count
        Count.norm_counts(5 - lcnt, 15 - lcnt, [self.warriors])

    def get_count(self):
        return self.warriors.cur + self.leader.count


class Razorwing(Unit):
    type_name = 'Razorwing jetfighter'
    type_id = 'razorwing'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Razorwing.Options, self).__init__(parent, 'Options')
            self.variant('Night shields', 15)

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(Razorwing.MainWeapon, self).__init__(parent, 'Main weapom')
            self.variant('Two disintegrator cannons', 0, gear=[Gear('Disintegrator cannon', count=2)])
            self.variant('Two dark lances', 10, gear=[Gear('Dark lance', count=2)])

    class SecondaryWeapon(OneOf):
        def __init__(self, parent):
            super(Razorwing.SecondaryWeapon, self).__init__(parent, 'Secondary weapon')
            self.variant('Twin-linked splinter rifle', 0)
            self.variant('Splinter cannon', 10)

    class Missile(OneOf):
        def __init__(self, parent):
            super(Razorwing.Missile, self).__init__(parent, 'Missile')
            self.variant('Monoscythe missile', 0)
            self.variant('Necrotoxin missile', 0)
            self.variant('Shatterfield missile', 5)

    def __init__(self, parent):
        super(Razorwing, self).__init__(parent, self.type_name, 130)
        self.MainWeapon(self)
        self.SecondaryWeapon(self)
        [self.Missile(self) for i in range(0, 4)]
        self.Options(self)


class Razorwings(Squadron):
    type_name = 'Razorwing Jetfighters'
    type_id = 'razorwings_v2'
    unit_class = Razorwing
    unit_max = 4


class Scourges(Unit):
    type_name = 'Scourges'
    type_id = 'scourges'
    common_gear = [Gear('Ghostplate armour'), Gear('Plasma grenades'), Gear('Close combat weapon')]
    member_name = 'Scourge'
    member_points = 16

    class Solarite(Unit):
        class Ranged(Pistol, Carbine):
            def __init__(self, parent):
                super(Scourges.Solarite.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant('Blast pistol', 15)

        class Melee(NoWeapon):
            def __init__(self, parent):
                super(Scourges.Solarite.Melee, self).__init__(parent, 'Melee weapon')
                self.variant('Venom blade', 5)
                self.variant('Power lance', 10)
                self.variant('Agonizer', 15)

        def __init__(self, parent):
            super(Scourges.Solarite, self).__init__(parent, 'Solarite', 26, Scourges.common_gear)
            self.rng = self.Ranged(self)
            self.mle = self.Melee(self)

        def check_rules(self):
            super(Scourges.Solarite, self).check_rules()
            self.mle.visible = self.mle.used = not self.rng.cur == self.rng.rifle

    class Weapon(Heavy, Special, Carbine):
        def __init__(self, parent):
            super(Scourges.Weapon, self).__init__(parent, 'Ranged weapon', is_scourge=True)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Scourges.Boss, self).__init__(parent=parent, name='Leader', limit=1)
            SubUnit(self, Scourges.Solarite(parent=None))

    def __init__(self, parent):
        super(Scourges, self).__init__(parent, 'Scourges')
        self.wars = Count(self, self.member_name, 5, 10, self.member_points, True)
        self.hweps = [self.Weapon(self) for i in range(0, 4)]
        self.leader = self.Boss(self)

    def check_rules(self):
        ldr = self.leader.count
        Count.norm_counts(5 - ldr, 10 - ldr, [self.wars])

    def get_count(self):
        return self.wars.cur + self.leader.count

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.leader.description)
        model = UnitDescription(name=self.member_name, points=self.member_points, options=self.common_gear)
        spec_count = sum([(not o.cur == o.rifle) for o in self.hweps])
        desc.add(model.clone().add([Gear('Shardcarbine')]).set_count(self.wars.cur - spec_count))
        for g in self.hweps:
            if not g.cur == g.rifle:
                desc.add_dup(model.clone().add(g.description).add_points(g.points))
        return desc
