from builder.core2 import OneOf, OptionsList, Gear
from builder.games.wh40k.ynnari.armory import YnnariArtefacts, YnnariBlade,\
    YnnariPistol

__author__ = 'Ivan Truskov'


class NoWeapon(OneOf):
    def __init__(self, parent, name):
        super(NoWeapon, self).__init__(parent, name)
        self.nothing = self.variant('No weapon', 0)

    @property
    def description(self):
        if self.cur == self.nothing:
            return []
        else:
            return super(NoWeapon, self).description

    def allow_empty(self, flag):
        self.nothing.visible = flag


class Pistol(OneOf):
    def __init__(self, parent, name):
        super(Pistol, self).__init__(parent, name)
        self.variant('Splinter pistol', 0)


class WrackTool(OneOf):
    def __init__(self, parent, name):
        super(WrackTool, self).__init__(parent, name)
        self.tool = self.variant('Wrack tool', 0)


class Rifle(OneOf):
    def __init__(self, parent, name):
        super(Rifle, self).__init__(parent, name)
        self.rifle = self.variant('Splinter rifle', 0)


class Carbine(OneOf):
    def __init__(self, parent, name):
        super(Carbine, self).__init__(parent, name)
        self.rifle = self.variant('Shardcarbine', 0)


class Melee(OneOf):
    def __init__(self, parent, name="Melee weapon", extra=None):
        super(Melee, self).__init__(parent, name=name)
        self.variant('Close combat weapon', 0)
        if extra is not None:
            self.variant(*extra)
        self.variant('Power sword', 15)
        self.variant('Agonizer', 25)


class Special(OneOf):
    def __init__(self, parent, name="Special weapon"):
        super(Special, self).__init__(parent, name=name)
        self.variant('Shredder', 5)
        self.variant('Blaster', 15)


class Heavy(OneOf):
    def __init__(self, parent, name="Heavy weapon", is_scourge=False):
        super(Heavy, self).__init__(parent, name=name)
        if is_scourge:
            self.variant('Haywire blaster', 10)
            self.variant('Heat lance', 10)
        self.variant('Splinter cannon', 15)
        self.variant('Dark lance', 20)


class Arcane(OptionsList):
    def __init__(self, parent):
        super(Arcane, self).__init__(parent, name="Arcane Wargear")
        self.variant('Haywire grenades', 5)
        self.variant('Soul-trap', 10)
        self.variant('Phantasm grenade launcher', 15)
        self.clone = self.variant('Clone field', 20)
        self.variant('Webway portal', 35)
        self.shadow = self.variant('Shadow field', 40)

    def check_rules(self):
        super(Arcane, self).check_rules()
        self.clone.visible = self.clone.used = not self.shadow.value
        self.shadow.visible = self.shadow.used = not self.clone.value


class Torture(OneOf):
    def __init__(self, parent, name="Weapon of Torture", is_acothyst=False):
        super(Torture, self).__init__(parent, name=name)
        if is_acothyst:
            self.variant('Wrack tool')
        else:
            self.variant('Close combat weapon', 0)
        self.variant('Mindphase gauntlet', 5)
        self.variant('Flesh gauntlet', 10)
        self.variant('Scissorhand', 10)
        if is_acothyst:
            self.variant('Venom blade', 10)
            self.variant('Electrocorrosive whip', 20)
        self.variant('Agonizer', 25)


class Torment(OneOf):
    def __init__(self, parent, name="Tool of Torment"):
        super(Torment, self).__init__(parent, name=name)
        self.variant('Stinger pistol', 5)
        self.variant('Hexrifle', 10)
        self.variant('Liquifier gun', 15)


class WychWeapon(OneOf):
    def __init__(self, parent, name="Wych Cult weapon", has_default=False):
        super(WychWeapon, self).__init__(parent, name=name)
        if has_default:
            self.default = self.variant('Close combat weapon and splinter pistol', 0, gear=[])
        self.hyd = self.variant('Two hydra gauntlers', 5,
                     gear=[Gear('Hydra gauntlet', count=2)])
        self.flail = self.variant('Two razorflails', 5,
                     gear=[Gear('Razorflail', count=2)])
        self.net = self.variant('Shardnet & impaler', 5,
                     gear=[Gear('Shardnet'), Gear('Impaler')])

    def is_wych(self):
        return self.cur in [self.hyd, self.flail, self.net]


class VehicleEquipment(OptionsList):
    def __init__(self, parent, is_venom=False):
        super(VehicleEquipment, self).__init__(parent,
                                               name='Dark Eldar Vehicle Equipment')
        self.variant('Chain-snares', 5)
        self.variant('Grisly trophies', 10)
        if not is_venom:
            self.variant('Enchanted aethersails', 5)
            self.variant('Shock prow', 10)
            self.variant('Night shields', 15)
            self.variant('Torment grenade launchers', 15)
            self.variant('Splinter racks', 15)


class ArtefactBlade(OneOf):
    def __init__(self, *args, **kwargs):
        super(ArtefactBlade, self).__init__(*args, **kwargs)
        self.djin = self.variant('The Djin Blade', 30)
        self.relic_weapons = [self.djin]

    def get_unique(self):
        if self.cur in self.relic_weapons and self.used:
            return self.description
        return []


class YnnariArtefactBlade(YnnariBlade, ArtefactBlade):
    def __init__(self, *args, **kwargs):
        super(YnnariArtefactBlade, self).__init__(*args, **kwargs)
        self.relic_weapons.append(self.hblade)


class ArtefactPistol(OneOf):
    def __init__(self, *args, **kwargs):
        super(ArtefactPistol, self).__init__(*args, **kwargs)
        self.par = self.variant('The Parasite\'s Kiss', 5)
        self.relic_weapons = [self.par]

    def get_unique(self):
        if self.cur in self.relic_weapons and self.used:
            return self.description
        return []


class YnnariArtefactPistol(YnnariPistol, ArtefactPistol):
    def __init__(self, *args, **kwargs):
        super(YnnariArtefactPistol, self).__init__(*args, **kwargs)
        self.relic_weapons.append(self.ysong)


class Artefacts(OptionsList):
    def __init__(self, parent):
        self.flag = parent.roster.is_coven()
        if self.flag:
            name = 'Diabolical playthings'
        else:
            name = "Artefacts of cruelty"
        super(Artefacts, self).__init__(parent, name=name, limit=1)

        self.base = [
            self.variant('The Armour of Misery', 15),
            self.variant('The Animus Vitae', 20),
            self.variant('The Archangel of Pain', 25),
            self.variant('The Helm of Spite', 25)
        ]

        self.supp = [
            self.variant('Syndriq\'s sump', 10),
            self.variant('The Vexator Mask', 10),
            self.variant('The Orbs of Despair', 25),
            self.variant('The Khaidesi Haemovores', 10),
            self.variant('The Panacea perverted', 20),
            self.variant('The Nightmare Doll', 35)
        ]

        if self.flag:
            unused = self.base
        else:
            unused = self.supp

        for variant in unused:
            variant.used = variant.visible = False


class YnnariCruelArtefacts(YnnariArtefacts, Artefacts):
    pass
