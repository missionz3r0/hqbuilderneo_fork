from builder.core2 import Gear, OptionsList, SubUnit, Count, UnitDescription, ListSubUnit, UnitList
from builder.games.wh40k.adepta_sororitas_v4.fast import Transport
from .armory import Boltgun, BoltPistol, Melee, HeavyWeapon, SpecialWeapon,\
    Ranged
from .hq import ASRelic
from builder.games.wh40k.roster import Unit


class Celestians(Unit):
    type_name = 'Celestian Squad'
    type_id = 'celestians_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Celestian-Squad')

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 14

    class Superior(Unit):
        name = 'Celestian Superior'

        class Weapon1(Ranged, Melee, Boltgun):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Celestians.Superior.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(Celestians.Superior, self).__init__(parent, self.name, Celestians.model_points,
                                                      gear=Celestians.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self)

        def check_rules(self):
            super(Celestians.Superior, self).check_rules()
            self.wep1.allow_melee(not self.wep2.is_melee())
            self.wep1.allow_ranged(not self.wep2.is_ranged())
            self.wep2.allow_melee(not self.wep1.is_melee())
            self.wep2.allow_ranged(not self.wep1.is_ranged())

    class MixWeapon(HeavyWeapon, SpecialWeapon):
        pass

    class Simulacrum(OptionsList):
        def __init__(self, parent):
            super(Celestians.Simulacrum, self).__init__(parent, 'Icon of Faith')
            self.variant('Simulacrum imperialis', 10)

    def __init__(self, parent):
        super(Celestians, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=None))
        self.squad = Count(self, 'Battle Sister', 4, 9, self.model_points, True)
        self.opt = self.Simulacrum(self)
        self.wep1 = SpecialWeapon(self)
        self.wep2 = self.MixWeapon(self, 'Special or Heavy weapon')
        self.transport = Transport(self)

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        desc = UnitDescription('Celestian', points=self.model_points,
                               options=self.common_gear + [Gear('Bolt Pistol')])
        res.add(self.sup.description)
        count = self.squad.cur
        if self.opt.any:
            res.add(desc.clone().add(Gear('Boltgun')).add(self.opt.description).add_points(self.opt.points))
            count -= 1
        for wep in [self.wep1, self.wep2]:
            if wep.any:
                res.add_dup(desc.clone().add(wep.description).add_points(wep.points))
                count -= 1
        if count:
            res.add(desc.clone().add(Gear('Boltgun')).set_count(count))
        res.add(self.transport.description)
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_statistics(self):
        return self.note_transport(super(Celestians, self).build_statistics())


class Repentia(Unit):
    type_name = 'Repentia squad'
    type_id = 'repentia_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Repentia-Squad')

    common_gear = [Gear('Evisecrator')]
    model_points = 14

    class Mistress(Unit):
        def __init__(self, parent):
            super(Repentia.Mistress, self).__init__(parent, 'Mistress of Repentance', 85 - 4 * Repentia.model_points,
                                                    gear=[Gear('Frag grenades'),
                                                          Gear('Krak grenades'), Gear('Neural whip', count=2)])
            self.opt = Celestians.Superior.Options(self)

    def __init__(self, parent):
        super(Repentia, self).__init__(parent)
        self.leader = SubUnit(self, self.Mistress(parent=None))
        self.warriors = Count(self, 'Sister Repentia', 4, 9, self.model_points, True,
                              gear=UnitDescription('Sister Repentia', self.model_points, options=self.common_gear))
        self.transport = Transport(self)

    def get_count(self):
        return self.warriors.cur + 1

    def build_statistics(self):
        return self.note_transport(super(Repentia, self).build_statistics())


class CommandSquad(Unit):
    type_name = 'Sororitas Command Squad'
    type_id = 'sor_command_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Sororitas-Command-Squad')

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 65 / 5

    class Celestian(ListSubUnit):
        name = 'Celestian'

        class Upgrades(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Celestian.Upgrades, self).__init__(parent, 'Upgrades', limit=1)
                self.diag = self.variant('Dialogus', gear=[Gear('Bolt pistol'), Gear('Loud hailer')])
                self.hosp = self.variant('Hospitaler', gear=[Gear('Bolt pistol'), Gear('Chirurgeon\'s tools')])

        class Icons(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Celestian.Icons, self).__init__(parent, 'Icons of faith', limit=1)
                default_weapons = [Gear('Bolt pistol'), Gear('Boltgun')]
                self.simp = self.variant('Simulacrum imperialis', 10,
                                         gear=default_weapons + [Gear('Simulacrum imperialis')])
                self.flags = [self.variant('Blessed banner', 15, gear=default_weapons + [Gear('Blessed banner')]),
                              self.variant('Sacred Banner of the Order Militant', 40,
                                           gear=default_weapons + [Gear('Sacred Banner of the Order Militant')])]

        class Weapon1(Melee, Ranged, BoltPistol):
            pass

        class Weapon2(Melee, Ranged, Boltgun):
            def __init__(self, parent):
                super(CommandSquad.Celestian.Weapon2, self).__init__(parent, name='')
                self.variant('Storm bolter', 5)
                self.variant('Flamer', 5)
                self.variant('Meltagun', 10)
                self.variant('Heavy bolter', 10)
                self.variant('Multi-melta', 10)
                self.variant('Heavy flamer', 10)

        def __init__(self, parent):
            super(CommandSquad.Celestian, self).__init__(parent, 'Celestian',
                                                         CommandSquad.model_points,
                                                         CommandSquad.common_gear)
            self.up = self.Upgrades(self)
            self.icon = self.Icons(self)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.relic = ASRelic(self)

        def check_rules(self):
            super(CommandSquad.Celestian, self).check_rules()
            self.icon.used = self.icon.visible = not self.up.any
            spec_gear = self.icon.any and self.icon.used
            self.wep1.used = self.wep1.visible = not(spec_gear or self.up.any)
            self.wep2.used = self.wep2.visible = not(spec_gear or self.up.any)
            self.relic.used = self.relic.visible = self.up.diag.value
            self.wep1.allow_melee(not self.wep2.is_melee())
            self.wep1.allow_ranged(not self.wep2.is_ranged())
            self.wep2.allow_melee(not self.wep1.is_melee())
            self.wep2.allow_ranged(not self.wep1.is_ranged())

        def build_description(self):
            res = super(CommandSquad.Celestian, self).build_description()
            if self.up.diag.value:
                res.name = 'Dialogus'
            if self.up.hosp.value:
                res.name = 'Hospitaler'
            return res

        @ListSubUnit.count_unique
        def get_relic(self):
            return self.relic.description if self.relic.used else []

        @ListSubUnit.count_gear
        def has_flag(self):
            return any(opt.value for opt in self.icon.flags) if self.icon.used else 0

        @ListSubUnit.count_gear
        def has_sim(self):
            return self.icon.simp.value if self.icon.used else 0

        @ListSubUnit.count_gear
        def is_dialogus(self):
            return self.up.diag.value

        @ListSubUnit.count_gear
        def is_hosp(self):
            return self.up.hosp.value

    class Options(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.Options, self).__init__(parent, 'Options')
            self.variant('Melta bombs', 25)

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent)
        self.models = UnitList(self, self.Celestian, 5, 5)
        self.opt = self.Options(self)
        self.transport = Transport(self)

    def check_rules(self):
        super(CommandSquad, self).check_rules()
        flag = sum((c.has_flag() for c in self.models.units))
        if flag > 1:
            self.error('Only one Celestian can take a banner (taken: {})'.format(flag))

        sim = sum((c.has_sim() for c in self.models.units))
        if sim > 1:
            self.error('Only one Celestian can take a Simulacrum imperialis (taken: {})'.format(sim))

        diag = sum(c.is_dialogus() for c in self.models.units)
        if diag > 1:
            self.error('Only one Celestian may be upgraded to Dialogus')

        hosp = sum(c.is_hosp() for c in self.models.units)
        if hosp > 1:
            self.error('Only one Celestian may be upgraded to Hospitaler')

    def get_count(self):
        return 5

    def get_unique_gear(self):
        return sum((u.get_relic() for u in self.models.units), [])

    def build_statistics(self):
        return self.note_transport(super(CommandSquad, self).build_statistics())


class ArcoFlagellants(Unit):
    type_name = 'Arco-Flagellants'
    type_id = 'arco_flagellants_v1'

    def __init__(self, parent):
        super(ArcoFlagellants, self).__init__(parent)
        self.models = Count(self, 'Arco-flagellant', 3, 10, 10, True,
                            gear=UnitDescription('Arco-flagellant', 10,
                                                 options=[Gear('Arco-flails', count=2)]))
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur


class Crusaders(Unit):
    type_name = 'Crusaders'
    type_id = 'crusaders_v1'

    def __init__(self, parent):
        super(Crusaders, self).__init__(parent)
        self.models = Count(self, 'Crusader', 2, 10, 15, True,
                            gear=UnitDescription('Crusader', 15,
                                                 options=[Gear('Power sword'), Gear('Storm shield')]))
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur


class DeathCultAssasins(Unit):
    type_name = 'Death Cult Assasins'
    type_id = 'death_cult_assasins_v1'

    def __init__(self, parent):
        super(DeathCultAssasins, self).__init__(parent)
        self.models = Count(self, 'Death Cult Assasins', 2, 10, 15, True,
                            gear=UnitDescription('Death Cult Assasins', 15,
                                                 options=[Gear('Power sword', count=2)]))
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur
