from .armory import Boltgun, BoltPistol,\
    Melee, Ranged, SpecialWeapon, HeavyWeapon
from builder.core2 import Gear, UnitDescription, OptionsList, SubUnit, Count
from builder.games.wh40k.adepta_sororitas_v4.fast import Transport
from builder.games.wh40k.roster import Unit


class Sisters(Unit):
    type_name = 'Battle Sister Squad'
    type_id = 'sisters_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Battle-Sister-Squad')

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 12

    class Superior(Unit):
        name = 'Sister Superior'

        class Weapon1(Ranged, Melee, Boltgun):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Sisters.Superior.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.vet = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(Sisters.Superior, self).__init__(parent, 'Sister Superior',
                                                   Sisters.model_points, Sisters.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(Sisters.Superior, self).check_rules()
            self.wep1.allow_melee(not self.wep2.is_melee())
            self.wep1.allow_ranged(not self.wep2.is_ranged())
            self.wep2.allow_melee(not self.wep1.is_melee())
            self.wep2.allow_ranged(not self.wep1.is_ranged())

        def build_description(self):
            res = super(Sisters.Superior, self).build_description()
            if self.opt.vet.value:
                res.name = 'Veteran Sister Superior'
            return res

    class MixWeapon(HeavyWeapon, SpecialWeapon):
        pass

    class Simulacrum(OptionsList):
        def __init__(self, parent):
            super(Sisters.Simulacrum, self).__init__(parent, 'Icon of Faith')
            self.variant('Simulacrum imperialis', 10)

    def __init__(self, parent):
        super(Sisters, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=None))
        self.squad = Count(self, 'Battle Sister', 4, 19, self.model_points)
        self.opt = self.Simulacrum(self)
        self.wep1 = SpecialWeapon(self)
        self.wep2 = self.MixWeapon(self, name='Special or Heavy weapon')
        self.transport = Transport(self)

    def get_count(self):
        return 1 + self.squad.cur

    def build_statistics(self):
        return self.note_transport(super(Sisters, self).build_statistics())

    def build_description(self):
        res = UnitDescription(self.name, self.build_points(), self.get_count())
        res.add(self.sup.description)
        sister = UnitDescription('Battle Sister', self.model_points, options=self.common_gear + [Gear('Bolt Pistol')])
        count = self.squad.cur
        if self.opt.any:
            res.add(sister.clone().add(Gear('Boltgun')).add(self.opt.description).add_points(self.opt.points))
            count -= 1
        for wep in [self.wep1, self.wep2]:
            if wep.any:
                res.add_dup(sister.clone().add(wep.description).add_points(wep.points))
                count -= 1
        if count:
            res.add(sister.clone().add(Gear('Boltgun')).set_count(count))
        res.add(self.transport.description)
        return res
