__author__ = 'Ivan Truskov'

from builder.core2 import OneOf, OptionsList
from builder.games.wh40k.imperial_knight_v2.lords import MechanicusOption


class CCW(OneOf):
    def __init__(self, parent, name='Melee weapon'):
        super(CCW, self).__init__(parent, name)
        self.variant('Close combat weapon', 0)


class BaseWeapon(OneOf):
    def __init__(self, parent, name):
        super(BaseWeapon, self).__init__(parent, name)
        self.melee = []
        self.ranged = []

    def is_melee(self):
        return self.cur in self.melee

    def is_ranged(self):
        return self.cur in self.ranged

    def allow_melee(self, flag):
        for vnt in self.melee:
            vnt.active = flag

    def allow_ranged(self, flag):
        for vnt in self.ranged:
            vnt.active = flag


class Melee(BaseWeapon):
    def __init__(self, parent, name='Melee weapon'):
        super(Melee, self).__init__(parent, name)
        self.melee += [self.variant('Chainsword', 0),
                       self.variant('Power weapon', 15)]


class Eviscerator(BaseWeapon):
    def __init__(self, parent, name='Melee weapon'):
        super(Eviscerator, self).__init__(parent, name)
        self.melee += [self.variant('Evisecrator', 30)]


class Boltgun(OneOf):
    def __init__(self, parent, name='Ranged weapon'):
        super(Boltgun, self).__init__(parent, name)
        self.variant('Boltgun')


class Laspistol(OneOf):
    def __init__(self, parent, name='Ranged weapon'):
        super(Laspistol, self).__init__(parent, name)
        self.variant('Laspistol')


class BoltPistol(OneOf):
    def __init__(self, parent, name='Ranged weapon'):
        super(BoltPistol, self).__init__(parent, name)
        self.variant('Bolt pistol')


class Ranged(BaseWeapon):
    def __init__(self, parent, name='Ranged weapon'):
        super(Ranged, self).__init__(parent, name)
        self.ranged += [self.variant('Storm Bolter', 5),
                        self.variant('Combi-melta', 10),
                        self.variant('Combi-flamer', 10),
                        self.variant('Combi-plasma', 10),
                        self.variant('Condemnor boltgun', 15),
                        self.variant('Plasma pistol', 15)]


class Shotgun(BaseWeapon):
    def __init__(self, parent, name='Ranged weapon'):
        super(Shotgun, self).__init__(parent, name)
        self.ranged += [self.variant('Shotgun', 1)]


class InfernoPistol(BaseWeapon):
    def __init__(self, parent, name='Ranged weapon'):
        super(InfernoPistol, self).__init__(parent, name)
        self.ranged += [self.variant('Inferno pistol', 15)]


class BaseRelic(OptionsList):
    def __init__(self, parent, priest=False):
        super(BaseRelic, self).__init__(parent, 'Ecclesiarchy Relics', limit=1)
        self.variant('The Book of St. Lucius', 5)
        self.variant('The Litanies of Faith', 15)
        self.variant('The Cloak of St. Aspira', 20)
        self.variant('The Mantle of Ophelia', 25)
        if priest:
            self.variant('The Mace of Valaan', 25)


class WeaponRelic(OneOf):
    def __init__(self, parent, name='Weapon'):
        super(WeaponRelic, self).__init__(parent, name)
        self.relics = [self.variant('The Blade of Admonition', 30)]


class SpecialWeapon(OptionsList):
    def __init__(self, parent, name='Special Weapon', limit=1):
        super(SpecialWeapon, self).__init__(parent, name, limit=1)
        self.variant('Storm bolter', 5)
        self.variant('Flamer', 5)
        self.variant('Meltagun', 10)


class HeavyWeapon(OptionsList):
    def __init__(self, parent, name='Heavy Weapon', limit=1):
        super(HeavyWeapon, self).__init__(parent, name, limit=1)
        self.variant('Heavy bolter', 10)
        self.variant('Multi-melta', 10)
        self.variant('Heavy flamer', 10)


class VehicleOptions(OptionsList):
    def __init__(self, parent):
        super(VehicleOptions, self).__init__(parent, 'Options')
        self.variant('Storm bolter', 5)
        self.variant('Dozer blade', 5)
        self.variant('Extra armour', 10)
        self.variant('Hunter-killer missile', 10)
        self.variant('Laud hailer', 10)


class CadianRelicsHoly(OptionsList, MechanicusOption):
    def __init__(self, parent, *args, **kwargs):
        super(CadianRelicsHoly, self).__init__(parent, *args, **kwargs)
        self.cadia_holy = [
            self.variant('Scull of Petronella the Pious', 30 * self.freeflag),
            self.variant('Castigator', 15 * self.freeflag),
            self.variant('The Font of Fury', 10 * self.freeflag),
            self.variant('Shroud of the Anti-Martyr', 35 * self.freeflag),
            self.variant("Desvalle's Holy Circle", 35 * self.freeflag)
        ]
        self.cadia_arcana = [
            self.variant("Numinasta's Casket of Electromancy", 30 * self.freeflag),
            self.variant('Sacrifactorum Autorepulsor', 15 * self.freeflag),
            self.variant('Memento-Mortispex', 30 * self.freeflag),
            self.variant("Omnissiah's grace", 10 * self.freeflag),
            self.variant("Saint Curia's Autopurger", 25 * self.freeflag)
        ]

    def check_rules(self):
        super(CadianRelicsHoly, self).check_rules()
        flag = getattr(self.parent.roster, 'allow_cadia_holy', False)
        flag = flag or ('Celestine' in
                        self.parent.root.get_unique_map('unit_cahce',
                                                        lambda unit: unit.get_unique()))
        for opt in self.cadia_holy:
            opt.used = opt.visible = flag
        flag = getattr(self.parent.roster, 'allow_cadia_arcana', False)
        flag = flag or ('Belisarius Cawl' in
                        self.parent.root.get_unique_map('unit_cahce',
                                                        lambda unit: unit.get_unique()))
        for opt in self.cadia_arcana:
            opt.used = opt.visible = flag


class CadianWeaponHoly(MechanicusOption):
    def __init__(self, parent, *args, **kwargs):
        super(CadianWeaponHoly, self).__init__(parent, *args, **kwargs)
        self.worthy_blade = self.variant('Blade of the Worthy', 30 * self.freeflag)

    def check_rules(self):
        super(CadianWeaponHoly, self).check_rules()
        flag = getattr(self.parent.roster, 'allow_cadia_holy', False)
        flag = flag or ('Celestine' in
                        self.parent.root.get_unique_map('unit_cahce',
                                                        lambda unit: unit.get_unique()))
        self.worthy_blade.used = self.worthy_blade.visible = flag


class CadianWeaponArcana(MechanicusOption):
    def __init__(self, parent, *args, **kwargs):
        super(CadianWeaponArcana, self).__init__(parent, *args, **kwargs)
        self.qann = self.variant('Quantum Annihilator', 30 * self.freeflag)

    def check_rules(self):
        super(CadianWeaponArcana, self).check_rules()
        flag = getattr(self.parent.roster, 'allow_cadia_arcana', False)
        flag = flag or ('Belisarius Cawl' in
                        self.parent.root.get_unique_map('unit_cahce',
                                                        lambda unit: unit.get_unique()))
        self.qann.used = self.qann.visible = flag
