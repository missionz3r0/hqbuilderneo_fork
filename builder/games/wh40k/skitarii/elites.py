__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Count,\
    SubUnit, UnitDescription, Gear, OneOf
from .armory import SpecialIssue, CadiaRelics, MechanicusOption,\
    HolyWeaponRelic, ArcanaWeaponRelic
from builder.games.wh40k.roster import Unit


class Ruststalkers(Unit):
    type_name = 'Sicarian Ruststalkers'
    type_id = 'ruststalkers_v1'

    class Princeps(Unit):
        class Weapon1(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(Ruststalkers.Princeps.Weapon1, self).__init__(parent, 'Melee weapon')
                self.razor = self.variant('Transonic razor', 0)
                self.blade = self.variant('Transonic blade', 0)

        class Weapon2(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(Ruststalkers.Princeps.Weapon2, self).__init__(parent, '')
                self.claw = self.variant('Chordclaw', 0)
                self.blade = self.variant('Transonic blade', 0)

        class RelicWeapon1(HolyWeaponRelic, Weapon1):
            pass

        class RelicWeapon2(HolyWeaponRelic, Weapon2):
            pass

        class Options(OptionsList, MechanicusOption):
            def __init__(self, parent):
                super(Ruststalkers.Princeps.Options, self).__init__(parent, 'Options')
                self.claw = self.variant('Chordclaw', 5 * self.freeflag)
                self.variant('Prehensile dataspike', 10 * self.freeflag)

        def __init__(self, parent):
            super(Ruststalkers.Princeps, self).__init__(
                parent, 'Ruststalkers princeps', 40, gear=[
                    Gear('Sicarian battle armour')])
            self.wep1 = self.RelicWeapon1(self)
            self.wep2 = self.RelicWeapon2(self)
            self.opt = self.Options(self)
            SpecialIssue(self)
            self.relics = CadiaRelics(self)

        def get_unique_gear(self):
            return self.relics.description +\
                self.wep1.get_unique() + self.wep2.get_unique()

        def check_rules(self):
            super(Ruststalkers.Princeps, self).check_rules()
            blades = self.parent.parent.wep.cur == self.parent.parent.wep.blades
            # so sorry for this
            # order must be certain, or else there will be phase tasers everywhere
            if blades:
                self.wep1.blade.visible = self.wep1.blade.used = blades
                self.wep2.blade.visible = self.wep2.blade.used = blades
                self.wep1.razor.visible = self.wep1.razor.used = not blades
                self.wep2.claw.visible = self.wep2.claw.used = not blades
            else:
                self.wep1.razor.visible = self.wep1.razor.used = not blades
                self.wep2.claw.visible = self.wep2.claw.used = not blades
                self.wep1.blade.visible = self.wep1.blade.used = blades
                self.wep2.blade.visible = self.wep2.blade.used = blades
            self.opt.claw.used = self.opt.claw.visible = blades

            self.gear = self.gear[0:1] + ([Gear('Mindscrambler grenades')] if not blades else [])
            if len(self.get_unique_gear()) > 1:
                self.error('One model can carry no more than one Relic of Mars')

    class SquadWeapon(OneOf):
        def __init__(self, parent):
            super(Ruststalkers.SquadWeapon, self).__init__(parent, 'Squad weapon')
            self.variant('Transonic razor and chordclaw')
            self.blades = self.variant('Two transonic blades')

    def __init__(self, parent):
        super(Ruststalkers, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Princeps(parent=self))
        self.wars = Count(self, self.type_name, 4, 9, 30, True)
        self.wep = self.SquadWeapon(self)

    def get_unique_gear(self):
        return self.ldr.unit.get_unique_gear()

    def get_count(self):
        return self.wars.cur + 1  # leader always present

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        model = UnitDescription(name='Sicarian Ruststalker', points=30, options=[Gear('Sicarian battle armour')])
        if self.wep.blades == self.wep.cur:
            model.add(Gear('Transonic blade', count=2))
        else:
            model.add([
                Gear('Transonic razor'),
                Gear('Chordclaw'),
                Gear('Mindscrambler grenades')])
        desc.add(model.set_count(self.wars.cur))
        return desc


class Infiltrators(Unit):
    type_name = 'Sicarian Infiltrators'
    type_id = 'infiltrators_v1'

    class Princeps(Unit):
        class Melee(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(Infiltrators.Princeps.Melee, self).__init__(parent, 'Melee weapon')
                self.sword = self.variant('Power sword', 0)
                self.goad = self.variant('Taser goad', 0)

        class Ranged(OneOf, MechanicusOption):
            def __init__(self, parent):
                super(Infiltrators.Princeps.Ranged, self).__init__(parent, 'Ranged weapon')
                self.carb = self.variant('Stubcarbine', 0)
                self.blaster = self.variant('Flechette blaster', 0)

        class RelicMelee(HolyWeaponRelic, Melee):
            pass

        class RelicRanged(ArcanaWeaponRelic, Ranged):
            pass

        class Options(OptionsList, MechanicusOption):
            def __init__(self, parent):
                super(Infiltrators.Princeps.Options, self).__init__(parent, 'Options')
                self.variant('Infoslave skull', 10 * self.freeflag)

        def __init__(self, parent):
            super(Infiltrators.Princeps, self).__init__(
                parent, 'Infiltrators princeps', 45, gear=[
                    Gear('Sicarian battle armour')])
            self.wep1 = self.RelicMelee(self)
            self.wep2 = self.RelicRanged(self)
            self.opt = self.Options(self)
            SpecialIssue(self)
            self.relics = CadiaRelics(self)

        def get_unique_gear(self):
            return self.relics.description +\
                self.wep1.get_unique() + self.wep2.get_unique()

        def check_rules(self):
            super(Infiltrators.Princeps, self).check_rules()
            goads = self.parent.parent.wep.cur == self.parent.parent.wep.goads
            if goads:
                self.wep1.goad.visible = self.wep1.goad.used = goads
                self.wep2.blaster.visible = self.wep2.blaster.used = goads
                self.wep1.sword.visible = self.wep1.sword.used = not goads
                self.wep2.carb.visible = self.wep2.carb.used = not goads
            else:
                self.wep1.sword.visible = self.wep1.sword.used = not goads
                self.wep2.carb.visible = self.wep2.carb.used = not goads
                self.wep1.goad.visible = self.wep1.goad.used = goads
                self.wep2.blaster.visible = self.wep2.blaster.used = goads

            if len(self.get_unique_gear()) > 1:
                self.error('One model can carry no more than one Relic of Mars')

    class SquadWeapon(OneOf):
        def __init__(self, parent):
            super(Infiltrators.SquadWeapon, self).__init__(parent, 'Squad weapon')
            self.variant('Stubcarbine and power sword')
            self.goads = self.variant('Flechette blaster and taser goad')

    def __init__(self, parent):
        super(Infiltrators, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Princeps(parent=self))
        self.wars = Count(self, self.type_name, 4, 9, 35, True)
        self.wep = self.SquadWeapon(self)

    def get_unique_gear(self):
        return self.ldr.unit.get_unique_gear()

    def get_count(self):
        return self.wars.cur + 1  # leader always present

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        model = UnitDescription(name='Sicarian Infiltrator', points=35, options=[Gear('Sicarian battle armour')])
        if self.wep.goads == self.wep.cur:
            model.add([Gear('Flechette blaster'), Gear('Taser goad')])
        else:
            model.add([
                Gear('Stubcarbine'),
                Gear('Power sword')])
        desc.add(model.set_count(self.wars.cur))
        return desc
