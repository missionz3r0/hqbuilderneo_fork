__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList,\
    Gear, UnitList, ListSubUnit,\
    OneOf
from .armory import MechanicusOption
from builder.games.wh40k.roster import Unit


class Dragoons(Unit):
    type_name = 'Sydonian Dragoons'
    type_id = 'dragoons_v1'

    class Dragoon(Unit):
        class MainWeapon(OneOf):
            def __init__(self, parent):
                super(Dragoons.Dragoon.MainWeapon, self).__init__(
                    parent, 'Main weapon')
                self.variant('Tazer lance', 0)
                self.variant('Radium jezzail', 0)

        class OptionalWeapon(OptionsList, MechanicusOption):
            def __init__(self, parent):
                super(Dragoons.Dragoon.OptionalWeapon, self).__init__(
                    parent, 'Optional weapon')
                self.variant('Phosphor serpenta', 10 * self.freeflag)

        def __init__(self, parent):
            super(Dragoons.Dragoon, self).__init__(parent, 'Sydonian Dragoon',
                                                   45, gear=[
                                                       Gear('Broad spectrum data-tether'),
                                                       Gear('Searchlight')
                                                   ])
            self.MainWeapon(self)
            self.OptionalWeapon(self)

    class MultyDragoons(Dragoon, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Dragoons, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.MultyDragoons, 1, 6)

    def get_count(self):
        return self.models.count
