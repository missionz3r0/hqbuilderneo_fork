
__author__ = 'Ivan Truskov'
__summary__ = 'Codex Skitarii 7th edition'

from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as BaseLords
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, ElitesSection,\
    FastSection, HeavySection, TroopsSection, Fort, Formation, Detachment,\
    Wh40kImperial, Wh40kKillTeam, CompositeFormation, CombinedArmsDetachment,\
    AlliedDetachment, PrimaryDetachment
from builder.core2 import UnitType
from builder.games.wh40k.section import HQSection
from .troops import Vanguard, Rangers
from .elites import Ruststalkers, Infiltrators
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from builder.games.wh40k.imperial_armour.dataslates.secutarii import Hoplites,\
    Peltasts
from .fast import Dragoons
from .heavy import Ironstriders, Dunecrawlers


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent, 2, 8)
        UnitType(self, Vanguard)
        UnitType(self, Rangers)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent, 0, 4)
        UnitType(self, Ruststalkers)
        UnitType(self, Infiltrators)


class Elites(BaseElites):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Hoplites)
        UnitType(self, Peltasts)


class Fast(FastSection):
    def __init__(self, parent):
        super(Fast, self).__init__(parent, 0, 2)
        UnitType(self, Dragoons)


class Heavy(HeavySection):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent, 0, 4)
        UnitType(self, Ironstriders)
        UnitType(self, Dunecrawlers)


class LordsOfWar(CerastusSection, BaseLords):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class Maniple(Formation):
    army_id = 'skitarii_maniple'
    army_name = 'Battle maniple'

    def __init__(self):
        super(Maniple, self).__init__()
        UnitType(self, Vanguard, min_limit=1, max_limit=1)
        UnitType(self, Rangers, min_limit=1, max_limit=1)
        UnitType(self, Ruststalkers, min_limit=1, max_limit=1)
        UnitType(self, Infiltrators, min_limit=1, max_limit=1)
        dragoons = UnitType(self, Dragoons)
        striders = UnitType(self, Ironstriders)
        self.add_type_restriction([dragoons, striders], 1, 1)
        UnitType(self, Dunecrawlers, min_limit=1, max_limit=1)


class Cohort(CompositeFormation):
    army_id = 'skitarii_cohort'
    army_name = 'War Cohort'

    def __init__(self):
        super(Cohort, self).__init__()
        Detachment.build_detach(self, Maniple, min_limit=3, max_limit=3)


class Killclade(Formation):
    army_id = 'skitarii_killclade'
    army_name = 'Sicarian killclade'

    def __init__(self):
        super(Killclade, self).__init__()
        UnitType(self, Ruststalkers, min_limit=3, max_limit=3)
        UnitType(self, Infiltrators, min_limit=1, max_limit=1)


class Cavaliers(Formation):
    army_id = 'skitarii_cavaliers'
    army_name = 'Ironstrider cavaliers'

    def __init__(self):
        super(Cavaliers, self).__init__()
        UnitType(self, Dragoons, min_limit=2, max_limit=2)
        UnitType(self, Ironstriders, min_limit=1, max_limit=1)


class SkitariiDetachment(Wh40kBase):
    army_id = 'skitarii_det'
    army_name = 'Skitarii maniple'

    def __init__(self):
        super(SkitariiDetachment, self).__init__(
            troops=Troops(parent=self),
            elites=Elites(parent=self),
            fast=Fast(parent=self),
            heavy=Heavy(parent=self),
            fort=Fort(parent=self)
        )


class SkitariiV1Base(Wh40kBase):

    def __init__(self):
        super(SkitariiV1Base, self).__init__(
            hq=HQ(parent=self),
            elites=Elites(parent=self), troops=Troops(parent=self),
            fast=Fast(parent=self), heavy=Heavy(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(self)
        )


class SkitariiV1CAD(SkitariiV1Base, CombinedArmsDetachment):
    army_id = 'skitarii_v1_cad'
    army_name = 'Skitarii (Combined arms detachment)'


class SkitariiV1AD(SkitariiV1Base, AlliedDetachment):
    army_id = 'skitarii_v1_ad'
    army_name = 'Skitarii (Allied detachment)'


class SkitariiKillTeam(Wh40kKillTeam):
    army_id = 'skitarii_kt'
    army_name = 'Skitarii'

    class KTFast(FastSection):
        def __init__(self, parent):
            super(SkitariiKillTeam.KTFast, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [Dragoons])

    def __init__(self):
        super(SkitariiKillTeam, self).__init__(Troops(self),
                                               BaseElites(self),
                                               self.KTFast(self))


faction = 'Skitarii'


class Skitarii(Wh40k7ed, Wh40kImperial):
    army_id = 'skitarii'
    army_name = 'Skitarii'
    # development = True
    faction = faction

    def __init__(self):
        super(Skitarii, self).__init__([
            SkitariiDetachment,
            SkitariiV1CAD,
            Maniple,
            Cohort,
            Killclade,
            Cavaliers
        ])


detachments = [
    SkitariiDetachment,
    SkitariiV1CAD,
    SkitariiV1AD,
    Maniple,
    Cohort,
    Killclade,
    Cavaliers
]


unit_types = [
    Vanguard, Rangers,
    Ruststalkers, Infiltrators,
    Dragoons,
    Ironstriders, Dunecrawlers
]
