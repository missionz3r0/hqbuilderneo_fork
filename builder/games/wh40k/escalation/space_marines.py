__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import LordsOfWarSection
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle


class ThunderhawkGunship(SpaceMarinesBaseVehicle):
    type_id = 'thunderhawk_gunship_v1'
    type_name = 'Thunderhawk Gunship'

    def __init__(self, parent):
        super(ThunderhawkGunship, self) .__init__(parent=parent, points=685, gear=[
            Gear('Twin-linked heavy bolter', count=4),
            Gear('Lascannon', count=2),
            Gear('Armoured Ceramite'),
        ])
        self.Weapon(self)
        self.Bombs(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ThunderhawkGunship.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Thunderhawk cannon', 0)
            self.variant('Turbo-laser destructor', 90)

    class Bombs(OneOf):
        def __init__(self, parent):
            super(ThunderhawkGunship.Bombs, self).__init__(parent=parent, name='')
            self.variant('Hellstrike missiles', 0, gear=Gear('Hellstrike missile', count=6))
            self.variant('Thunderhawk cluster bombs', 60, gear=Gear('Thunderhawk cluster bomb', count=6))


class GKThunderhawkGunship(Unit):
    type_id = 'gk_thunderhawk_gunship_v1'
    type_name = 'Thunderhawk Gunship'

    def __init__(self, parent):
        super(GKThunderhawkGunship, self) .__init__(parent=parent, points=685, gear=[
            Gear('Twin-linked heavy bolter', count=4),
            Gear('Lascannon', count=2),
            Gear('Armoured Ceramite'),
        ])
        ThunderhawkGunship.Weapon(self)
        ThunderhawkGunship.Bombs(self)


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, ThunderhawkGunship)


class GKLordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(GKLordsOfWar, self).__init__(parent=parent)
        UnitType(self, GKThunderhawkGunship)
