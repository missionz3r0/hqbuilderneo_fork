__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import LordsOfWarSection


class Revenant(Unit):
    type_name = 'Revenant Titan'
    type_id = 'revenanttitan_v1'

    def __init__(self, parent):
        super(Revenant, self).__init__(parent=parent, points=900, gear=[Gear('Revenant Missile Launcher')])
        self.Weapons(self)

    class Weapons(OneOf):
        def __init__(self, parent):
            super(Revenant.Weapons, self).__init__(parent=parent, name='Weapons')
            self.variant('Pulsars', 0, gear=[Gear('Pulsar', count=2)])
            self.variant('Sonic Lances', 0, gear=[Gear('Sonic Lance', count=2)])


class Phantom(Unit):
    type_name = 'Phantom Titan'
    type_id = 'phantomtitan_v1'

    def __init__(self, parent):
        super(Phantom, self).__init__(parent=parent, points=2500,
                                      gear=[Gear('Phantom cloudburst missile launcher'),
                                            Gear('Phantom missile launcher')])
        self.Weapon1(self)
        self.Weapon2(self, name='Arm mounted')
        self.Weapon3(self, name='')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Phantom.Weapon1, self).__init__(parent=parent, name='Carapace-mounted')
            self.variant('Phantom Starcannon', 0)
            self.variant('Pulse Laser', 0)

    class Weapon2(OneOf):
        def __init__(self, parent, name):
            super(Phantom.Weapon2, self).__init__(parent=parent, name=name)
            self.variant('Phantom Pulsar', 0)
            self.variant('Phantom D-Cannon', 0)

    class Weapon3(Weapon2):
        def __init__(self, parent, name):
            super(Phantom.Weapon3, self).__init__(parent=parent, name=name)
            self.variant('Phantom Close Combat Weapon with Twin-linked Phantom Starcannon', 0, gear=[
                Gear('Phantom  Close Combat Weapon'),
                Gear('Twin-linked Phantom Starcannon'),
            ])


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, Revenant)
        UnitType(self, Phantom)
