__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import LordsOfWarSection


class Squiggoth(Unit):
    type_id = 'gargantuan_squiggoth_v1'
    type_name = 'Gargantuan Squiggoth'

    def __init__(self, parent):
        super(Squiggoth, self) .__init__(parent=parent, points=525, gear=[
            Gear('Supa-lobba', count=2),
            Gear('Twin-linked big shoota', count=2),
        ])
        Count(self, 'Big shoota', 0, 4, 5)


class Stompa(Unit):
    type_id = 'stompa_v2'
    type_name = 'Stompa'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Stompa.Options, self).__init__(parent, name='Options')
            self.variant("Grot riggers", 30)

    def __init__(self, parent):
        super(Stompa, self) .__init__(parent=parent, points=770, gear=[
            Gear('Big shoota', count=3),
            Gear('Deff kannon'),
            Gear('Supa-gatler'),
            Gear('Supa-rokkit', count=3),
            Gear('Skorcha'),
            Gear('Twin-linked big shoota'),
            Gear('Mega-choppa'),
        ])
        Count(self, 'Supa-rokkit', 0, 2, 20)
        self.opts = self.Options(self)


class BigMekStompa(Unit):
    type_id = 'mek_stompa_v1'
    type_name = 'Big Mek Stompa'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BigMekStompa.Weapon1, self).__init__(parent, name='Weapon')
            self.variant('Mega-klaw', 0)
            self.variant('Deff kannon', 50)

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(BigMekStompa.Weapon2, self).__init__(parent, name='')
            self.variant('Deff kannon', 100)

    def __init__(self, parent):
        super(BigMekStompa, self) .__init__(parent=parent, points=830, gear=[
            Gear('Big shoota', count=3),
            Gear('Gaze of Mork'),
            Gear('Lifta-droppa'),
        ])
        self.Weapon1(self)
        self.Weapon2(self)
        Count(self, 'Supa-rokkit', 0, 3, 20)


class Skullhamma(Unit):
    type_id = 'skullhamma_v1'
    type_name = 'Skullhamma Battle Fortress'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Skullhamma.Weapon1, self).__init__(parent, name='Weapon')
            self.variant('Kannon', 0)
            self.variant('Lobba', 0)

    def __init__(self, parent):
        super(Skullhamma, self).__init__(parent=parent, points=410, gear=[
            Gear('Skullhamma kannon'),
        ])
        self.Weapon1(self)
        Count(self, 'Supa-rokkit', 0, 3, 20)
        self.bs = Count(self, 'Twin-linked big shoota', 0, 4, 10)
        self.rl = Count(self, 'Twin-linked rokkit launcha', 0, 4, 10)

    def check_rules(self):
        super(Skullhamma, self).check_rules()
        Count.norm_counts(2, 4, [self.bs, self.rl])


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, Squiggoth)
        UnitType(self, Stompa)
        UnitType(self, BigMekStompa)
        UnitType(self, Skullhamma)
