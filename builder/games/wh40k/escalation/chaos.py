__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import LordsOfWarSection


class LordOfSkulls(Unit):
    type_id = 'lord_of_skulls_v1'
    type_name = 'Khorne Lord of Skulls'

    def __init__(self, parent):
        super(LordOfSkulls, self) .__init__(parent=parent, points=888, gear=[
            Gear('Daemonic Possession'),
            Gear('Great cleaver of Khorne'),
        ])
        self.Weapon1(self)
        self.Weapon2(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(LordOfSkulls.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Hades gatling cannon', 0)
            self.variant('Skullhurler', 60)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(LordOfSkulls.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Gorestorm cannon', 0)
            self.variant('Ichor cannon', 10)
            self.variant('Daemongore cannon', 65)


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, LordOfSkulls)
