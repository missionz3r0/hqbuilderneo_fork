__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.section import LordsOfWarSection


class ReaverTitan(Unit):
    type_name = 'Reaver Battle Titan'
    type_id = 'reaver_titan_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ReaverTitan.Weapon1, self).__init__(parent, name='Carapace weapon')
            self.variant('Apocalypse missile-launcher')
            self.variant('Double-barrelled turbo laser destructor')
            self.variant('Inferno gun')
            self.variant('Plasma blastgun')
            self.variant('Vortex missile')
            self.variant('Vulcan mega-bolter')

    class Weapon2(OneOf):
        def __init__(self, parent, name):
            super(ReaverTitan.Weapon2, self).__init__(parent, name=name)
            self.variant('Gatling blaster')
            self.variant('Laser blaster')
            self.variant('Melta cannon')
            self.variant('Titan power first')
            self.variant('Vulcano cannon')

    def __init__(self, parent):
        super(ReaverTitan, self).__init__(parent, points=1450)
        self.Weapon1(self)
        self.Weapon2(self, name='Arm weapon')
        self.Weapon2(self, name='')


class WarhoundTitan(Unit):
    type_name = 'Warhound Scout Titan'
    type_id = 'warhound_titan_v1'

    class Weapon1(OneOf):
        def __init__(self, parent, name):
            super(WarhoundTitan.Weapon1, self).__init__(parent, name=name)
            self.variant('Inferno gun')
            self.variant('Plasma blastgun')
            self.variant('Double-barrelled turbo laser destructor')
            self.variant('Vulcan mega-bolter')

    def __init__(self, parent):
        super(WarhoundTitan, self).__init__(parent, points=720)
        self.Weapon1(self, name='Arm weapon')
        self.Weapon1(self, name='')


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, ReaverTitan)
        UnitType(self, WarhoundTitan)
