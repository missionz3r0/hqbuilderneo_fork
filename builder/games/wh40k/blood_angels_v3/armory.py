from builder.core2 import OptionsList, OneOf, Gear
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianWeaponHoly, CadianRelicsHoly,\
    CadianWeaponArcana

__author__ = 'Ivan Truskov'


class BaseWeapon(OneOf):
    def __init__(self, parent, name, **kwargs):
        self.armour = kwargs.pop('armour', None)
        self.pwr_weapon = []
        self.tda_weapon = []
        self.relic_options = []

        super(BaseWeapon, self).__init__(parent, name, **kwargs)

    def check_rules(self):
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.used and self.cur in self.relic_options:
            return self.description
        return []


class BoltPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(BoltPistol, self).__init__(*args, **kwargs)
        self.boltpistol = self.variant('Bolt pistol', 0)
        self.pwr_weapon += [self.boltpistol]


class Boltgun(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Boltgun, self).__init__(*args, **kwargs)
        self.boltgun = self.variant('Boltgun', 0)
        self.pwr_weapon += [self.boltgun]


class Combi(BaseWeapon):
    def __init__(self, *args, **kwargs):
        """ pass scout=True fro scout squad characters """
        super(Combi, self).__init__(*args, **kwargs)
        self.stormbolter = self.variant('Storm bolter', 5)
        self.combimelta = self.variant('Combi-melta', 10)
        self.combiflamer = self.variant('Combi-flamer', 10)
        self.combigrav = self.variant('Combi-grav', 10)
        self.combiplasma = self.variant('Combi-plasma', 10)
        self.comby_weapons = [self.combiflamer, self.combigrav,
                             self.combimelta, self.combiplasma]
        self.pwr_weapon += self.comby_weapons + [self.stormbolter]


class CommonRanged(Combi):
    def __init__(self, *args, **kwargs):
        """ pass scout=True fro scout squad characters """
        scout = kwargs.pop('scout', False)
        super(CommonRanged, self).__init__(*args, **kwargs)
        self.gravpistol = self.variant('Grav pistol', 15)
        self.plaspistol = self.variant('Plasma pistol', 15)
        if not scout:
            self.handflamer = self.variant('Hand flamer', 10)
            self.infpistol = self.variant('Inferno pistol', 15)
        self.pwr_weapon += [self.gravpistol,
                            self.plaspistol]
        if not scout:
            self.pwr_weapon += [self.handflamer, self.infpistol]


class Chainsword(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Chainsword, self).__init__(*args, **kwargs)
        self.chainsword = self.variant('Chainsword', 0)
        self.pwr_weapon += [self.chainsword]


class PowerWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerWeapon, self).__init__(*args, **kwargs)
        self.pwr = self.variant('Power weapon', 15)
        self.pwr_weapon += [self.pwr]


class ForceWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ForceWeapon, self).__init__(*args, **kwargs)
        self.force = self.variant('Force weapon', 15)


class LightningClaw(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(LightningClaw, self).__init__(*args, **kwargs)
        self.claw = self.variant('Lightning claw', 15)
        self.pwr_weapon += [self.claw]


class PowerFist(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerFist, self).__init__(*args, **kwargs)
        self.fist = self.variant('Power fist', 25)
        self.pwr_weapon += [self.fist]


class RelicBlade(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(RelicBlade, self).__init__(*args, **kwargs)
        self.rblade = self.variant('Relic blade', 25)
        self.pwr_weapon += [self.rblade]


class ThunderHammer(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ThunderHammer, self).__init__(*args, **kwargs)
        self.hummer = self.variant('Thunder hammer', 30)
        self.pwr_weapon += [self.hummer]


class PowerAxe(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerAxe, self).__init__(*args, **kwargs)
        self.pwr_axe = self.variant('Power axe', 15)
        self.pwr_weapon += [self.pwr_axe]


class Melee(ThunderHammer, PowerFist, LightningClaw, PowerWeapon, Chainsword):
    pass


class TerminatorComby(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(TerminatorComby, self).__init__(*args, **kwargs)
        self.tda_stormbolter = self.variant('Storm Bolter', 0)
        self.tda_combimelta = self.variant('Combi-melta', 5)
        self.tda_combiflamer = self.variant('Combi-flamer', 5)
        self.tda_combiplasma = self.variant('Combi-plasma', 5)
        self.tda_weapon += [self.tda_stormbolter, self.tda_combiflamer,
                            self.tda_combimelta]


class TerminatorRanged(TerminatorComby):
    def __init__(self, *args, **kwargs):
        super(TerminatorRanged, self).__init__(*args, **kwargs)
        self.tda_lightningclaw = self.variant('Lightning claw', 10)
        self.tda_thunderhammer = self.variant('Thunder hammer', 25)
        self.tda_weapon += [self.tda_combiplasma, self.tda_lightningclaw,
                            self.tda_thunderhammer]


class TerminatorMelee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        free_hammer = kwargs.pop('free_hammer', False)
        super(TerminatorMelee, self).__init__(*args, **kwargs)
        if free_hammer:
            self.tda2_thunderhammer = self.variant('Thunder hammer', 0)
        self.tda2_powerweapon = self.variant('Power weapon', 0)
        self.tda2_lightningclaw = self.variant('Lightning claw', 5)
        self.tda2_stormshield = self.variant('Storm shield', 5)
        self.tda2_powerfist = self.variant('Power fist', 10)
        self.tda2_chainfist = self.variant('Chainfist', 15)
        if not free_hammer:
            self.tda2_thunderhammer = self.variant('Thunder hammer', 15)
        self.tda_weapon += [self.tda2_powerweapon, self.tda2_lightningclaw, self.tda2_stormshield,
                            self.tda2_powerfist, self.tda2_chainfist, self.tda2_thunderhammer]


class Heavy(BaseWeapon):

    class Flakk(OptionsList):
        def __init__(self, parent):
            super(Heavy.Flakk, self).__init__(parent=parent, name='', limit=None)
            self.flakkmissiles = self.variant('Flakk missiles', 10)

    def __init__(self, parent, *args, **kwargs):
        """pass heavy_flamer=True for Strnguard Veterans and Tactical squads"""
        heavyflamer = kwargs.pop('heavy_flamer', False)
        super(Heavy, self).__init__(parent, *args, **kwargs)

        self.heavybolter = self.variant('Heavy bolter', 10)
        self.heavyflamer = heavyflamer and self.variant('Heavy flamer', 10)
        self.multimelta = self.variant('Multi-melta', 10)
        self.missilelauncher = self.variant('Missile launcher', 15)
        self.plasmacannon = self.variant('Plasma cannon', 15)
        self.lascannon = self.variant('Lascannon', 20)

        self.heavy = [self.heavybolter, self.heavyflamer, self.missilelauncher, self.multimelta, self.plasmacannon,
                      self.lascannon]
        self.flakk = self.Flakk(parent=parent)

    def is_heavy(self):
        return self.cur in self.heavy

    def check_rules(self):
        if self.flakk:
            self.flakk.visible = self.flakk.used = self.cur == self.missilelauncher


class Special(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Special, self).__init__(*args, **kwargs)

        self.flamer = self.variant('Flamer', 5)
        self.meltagun = self.variant('Meltagun', 10)
        self.gravgun = self.variant('Grav-gun', 15)
        self.plasmagun = self.variant('Plasma gun', 15)
        self.spec = [self.flamer, self.meltagun, self.gravgun, self.plasmagun]

    def is_spec(self):
        return self.cur in self.spec


class Armour(OneOf):
    power_armour_set = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    artificer_armour_set = [Gear('Artificer armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    scout_armour_set = [Gear('Scout armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent, art=0, tda=0):
        super(Armour, self).__init__(parent, 'Armour')
        self.pwr = self.variant('Power armour', 0, gear=self.power_armour_set)
        self.art = art and self.variant('Artificer armour', art, gear=self.artificer_armour_set)
        self.tda = tda and self.variant('Terminator armour', tda)

    def is_tda(self):
        return self.cur == self.tda

    def get_unique(self):
        return []


class TearerArmour(Armour):
    def __init__(self, parent, art=0, tda=0):
        super(TearerArmour, self).__init__(parent, art, tda)
        self.shield = self.variant('Shield of Cretacia', 5,
                                   gear=[Gear('Shield of Cretacia')] + self.power_armour_set[1:])

    def get_unique(self):
        if self.cur == self.shield:
            return [Gear('Shield of Cretacia')]
        return []


class SpecialOptions(OptionsList):
    def __init__(self, parent, armour=None, relic=None, bike=False, jump=False):
        super(SpecialOptions, self).__init__(parent, 'Special Issue Wargear')
        self.armour = armour
        self.relic = relic
        # self.bike_option = bike
        self.ride = []

        self.auspex = self.variant('Auspex', 5)
        self.meltabombs = self.variant('Melta bombs', 5)
        self.digitalweapons = self.variant('Digital weapons', 10)
        self.teleporthomer = self.variant('Teleport homer', 10)

        if jump:
            self.jumppack = self.variant('Jump pack', 15)
            self.ride += [self.jumppack]
        if bike:
            self.spacemarinebike = self.variant('Space Marine Bike', 20)
            self.ride += [self.spacemarinebike]

    def check_rules(self):
        ride_flag = True
        if self.armour:
            ride_flag = ride_flag and not self.armour.is_tda()
        if self.relic:
            ride_flag = ride_flag and not self.relic.is_wing()
        for opt in self.ride:
            opt.visible = opt.used = ride_flag
        self.process_limit(self.ride, 1)


class Relic(OptionsList):
    def __init__(self, parent, armour=None, lost_relics=False):
        super(Relic, self).__init__(parent, 'Relic', limit=1)
        self.armour = armour
        self.crown = self.variant('The Crown Angelic', 10)
        self.vitae = self.variant('The Veritas Vitae', 15)
        self.wing = self.variant('The Angel\'s Wing', 25)
        if lost_relics:
            self.variant('The Reliquary Armour', 30)
            self.variant('The Blood Shard', 30)

    def check_rules(self):
        if self.armour:
            self.wing.visible = self.wing.used = not self.armour.is_tda()

    def is_wing(self):
        return self.wing.value

    def get_unique(self):
        if self.used and self.any:
            return self.description
        return []


class HolyRelic(CadianRelicsHoly, Relic):
    pass


class RelicWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        lost_relics = kwargs.pop('lost', False)
        super(RelicWeapon, self).__init__(*args, **kwargs)
        self.edge = self.variant('Valour\'s Edge', 20)
        self.fury = self.variant('Fury of Baal', 25)
        self.relic_options += [self.edge, self.fury]
        if lost_relics:
            self.relic_options += [
                self.variant("The Guardian's Blade", 30),
                self.variant("Baal's Vengeance", 20),
                self.variant('Fyrestorm', 25)]


class HolyRelicWeapon(CadianWeaponHoly, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(HolyRelicWeapon, self).__init__(*args, **kwargs)
        self.relic_options += [self.worthy_blade]


class ArcanaRelicWeapon(CadianWeaponArcana, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(ArcanaRelicWeapon, self).__init__(*args, **kwargs)
        self.relic_options += [self.qann]


class Wrath(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Wrath, self).__init__(*args, **kwargs)
        self.wrath = self.variant('Slayer\'s Wrath', 10)
        self.relic_options += [self.wrath]
        self.wrath.active = self.parent.roster.supplement == 'tearer'


class VehicleEquipment(OptionsList):
    def __init__(self, parent, blade=True):
        super(VehicleEquipment, self).__init__(parent, 'Options')
        self.sbgun = self.Variant(self, 'Storm bolter', 5)
        if blade:
            self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
        self.exarm = self.Variant(self, 'Extra armour', 10)


class Crozius(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Crozius, self).__init__(*args, **kwargs)
        self.roz = self.variant('Crozius Arcanum', 0)
