__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import OptionsList, SubUnit, Gear, UnitDescription,\
    Count
from .armory import Boltgun, BoltPistol, CommonRanged, Heavy, Special,\
    Melee, Armour
from .transport import Transport
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from builder.games.wh40k.roster import Unit


class TacticalSquad(IATransportedUnit):
    type_name = 'Tactical Squad'
    type_id = 'tacticalsquad_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Tactical-Squad')

    model_points = 14
    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]
    model = UnitDescription('Space Marine', points=model_points, options=model_gear + [Gear('Bolt pistol')])

    class SpaceMarineSergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(TacticalSquad.SpaceMarineSergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.teleporthomer = self.variant('Teleport homer', 15)

        class Upgrade(OptionsList):
            def __init__(self, parent):
                super(TacticalSquad.SpaceMarineSergeant.Upgrade, self).__init__(parent, 'Upgrade')
                self.variant('Upgrade to Veteran Sergeant', 10, gear=[])

        class Weapon1(Melee, CommonRanged, BoltPistol):
            pass

        class Weapon2(Melee, Boltgun):
            pass

        def __init__(self, parent):
            super(TacticalSquad.SpaceMarineSergeant, self).__init__(
                parent=parent, points=TacticalSquad.model_points,
                gear=TacticalSquad.model_gear, name='Space Marine Sergeant'
            )
            self.Weapon1(self, 'Weapon')
            self.Weapon2(self, '')
            self.opt = self.Options(self)
            self.up = self.Upgrade(self)

        def check_rules(self):
            super(TacticalSquad.SpaceMarineSergeant, self).check_rules()
            self.name = 'Veteran Sergeant' if self.up.any else 'Space Marine Sergeant'

    class TacHeavy(Heavy, Boltgun):
        pass

    class TacSpecial(Special, Boltgun):
        pass

    def __init__(self, parent):
        super(TacticalSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.SpaceMarineSergeant(None))
        self.marines = Count(self, 'Space Marine', min_limit=4, max_limit=9, points=TacticalSquad.model_points)
        self.spec = self.TacSpecial(self, 'Special weapon')
        self.heavy = self.TacHeavy(self, 'Heavy weapon', heavy_flamer=True)
        self.transport = Transport(self)

    def check_rules(self):
        super(TacticalSquad, self).check_rules()
        self.spec.used = self.spec.visible = self.marines.cur == 9 or self.heavy.cur == self.heavy.boltgun
        self.heavy.used = self.heavy.visible = self.marines.cur == 9 or self.spec.cur == self.spec.boltgun

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.get_count(), self.sergeant.description)
        count = self.marines.cur
        if self.spec.used and self.spec.cur != self.spec.boltgun:
            desc.add(self.model.clone().add(self.spec.description).add_points(self.spec.points))
            count -= 1
        if self.heavy.used and self.heavy.cur != self.heavy.boltgun:
            desc.add(self.model.clone().add(self.heavy.description).add_points(self.heavy.points))
            count -= 1
        desc.add(self.model.clone().add(Gear('Boltgun')).set_count(count))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1


class ScoutSquad(Unit):
    type_name = 'Scout Squad'
    type_id = 'scout_squad_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Scout-Squad')

    model_points = 11
    model_gear = Armour.scout_armour_set

    class ScoutWeapon(Boltgun):
        def __init__(self, parent, name='Weapon'):
            super(ScoutSquad.ScoutWeapon, self).__init__(parent=parent, name=name)
            self.sniperrifle = self.variant('Sniper rifle', 1)
            self.shotgun = self.variant('Space Marines shotgun', 0)
            self.combatknife = self.variant('Close combat weapon', 0)

    class SergeantWeapon1(Melee, ScoutWeapon):
        pass

    class Sergeant(Unit):

        class Weapon2(Melee, CommonRanged, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(ScoutSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.teleport = self.variant('Teleport homer', 10)
                self.veteran = self.variant('Upgrade to Veteran Scout Sergeant', 10, gear=[])

        def check_rules(self):
            super(ScoutSquad.Sergeant, self).check_rules()
            self.name = 'Veteran Scout Sergeant' if self.opt.veteran.value else 'Scout Sergeant'

        def __init__(self, parent):
            super(ScoutSquad.Sergeant, self).__init__(
                name='Scout Sergeant',
                parent=parent, points=ScoutSquad.model_points,
                gear=ScoutSquad.model_gear
            )
            self.wep1 = ScoutSquad.SergeantWeapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '', scout=True)
            self.opt = self.Options(self)

    class Heavy(Boltgun):
        class Flakk(OptionsList):
            def __init__(self, parent):
                super(ScoutSquad.Heavy.Flakk, self).__init__(parent, '')
                self.flakkmissiles = self.variant('Flakk missiles', 10)

        class Hellfire(OptionsList):
            def __init__(self, parent):
                super(ScoutSquad.Heavy.Hellfire, self).__init__(parent, '')
                self.flakkmissiles = self.variant('Hellfire shells', 5)

        def __init__(self, parent, **kwargs):
            super(ScoutSquad.Heavy, self).__init__(parent=parent, **kwargs)
            self.hbgun = self.variant('Heavy bolter', 8)
            self.hf = self.Hellfire(parent=parent)
            self.mlaunch = self.variant('Missile launcher', 15)
            self.flakkmissiles = self.Flakk(parent=parent)
            self.heavy = [self.hbgun, self.mlaunch]

        def check_rules(self):
            super(ScoutSquad.Heavy, self).check_rules()
            self.flakkmissiles.visible = self.flakkmissiles.used = self.cur == self.mlaunch
            self.hf.visible = self.hf.used = self.hbgun == self.cur

        def is_heavy(self):
            return self.cur in self.heavy

        def get_all_points(self):
            return self.points + (self.hf.points if self.hf.used else 0)\
                + (self.flakkmissiles.points if self.flakkmissiles.used else 0)

        @property
        def description(self):
            return super(ScoutSquad.Heavy, self).description\
                + (self.hf.description if self.hf.used else [])\
                + (self.flakkmissiles.description if self.flakkmissiles.used else [])

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutSquad.Options, self).__init__(parent=parent, name='Options')
            self.cloak = self.variant('Camo cloaks', 2, per_model=True, gear=[Gear('Camo cloak')])

    def __init__(self, parent):
        super(ScoutSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Scout', min_limit=4,
                             max_limit=9, points=self.model_points,
                             per_model=True)
        self.sniper = Count(parent=self, name='Sniper rifle', min_limit=0, max_limit=4, points=1, per_model=True)
        self.shotgun = Count(parent=self, name='Space Marines shotgun', min_limit=0, max_limit=4, points=0)
        self.knife = Count(parent=self, name='Close combat weapon', min_limit=0, max_limit=4, points=0)

        self.heavy = self.Heavy(parent=self, name='Squad heavy weapon')
        self.opt = self.Options(parent=self)

    def get_count(self):
        return self.marines.cur + 1

    def check_rules(self):
        super(ScoutSquad, self).check_rules()
        max_wep = self.marines.cur
        if self.heavy.is_heavy():
            max_wep -= 1
        Count.norm_counts(0, max_wep, [self.sniper, self.shotgun, self.knife])
        self.heavy.used = self.heavy.visible = (self.sniper.cur + self.shotgun.cur + self.knife.cur) < self.marines.cur

    def build_points(self):
        return super(ScoutSquad, self).build_points() + (self.get_count() - 1) * self.opt.points

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.get_count())
        sarge = UnitDescription(self.sergeant.unit.name, self.sergeant.points + self.opt.points)
        sarge.add(self.model_gear)
        sarge.add(self.sergeant.unit.wep1.description).add(self.sergeant.unit.wep2.description).add(self.sergeant.unit.opt.description)
        if self.opt.any:
            sarge.add(self.opt.description)
        scout = UnitDescription('Scout', self.model_points, options=self.model_gear + [Gear('Bolt pistol')])
        if self.opt.any:
            scout.add(self.opt.description).add_points(self.opt.points)
        desc.add(sarge)
        count = self.marines.cur
        if self.heavy.used and self.heavy.is_heavy():
            desc.add(scout.clone().add(self.heavy.description).add_points(self.heavy.get_all_points()))
            count -= 1
        for o in [self.knife, self.shotgun, self.sniper]:
            if o.cur:
                desc.add(scout.clone().add(Gear(o.name)).add_points(o.option_points).set_count(o.cur))
                count -= o.cur
        if count:
            desc.add(scout.add(Gear('Boltgun')).set_count(count))

        return desc


class RaphenDeathCompany(Unit):
    type_name = 'Raphen\'s Death Company'
    type_id = 'rafen_v3'
    common_gear = [Gear('Power armour'), Gear('Jump pack'), Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent):
        super(RaphenDeathCompany, self).__init__(parent, points=210, static=True,
                                            unique=True, gear=[
                                                UnitDescription(name='Raphen', options=RaphenDeathCompany.common_gear + [Gear('Bolt pistol'), Gear('Thunder hammer')]),
                                                UnitDescription(name='Death Company Marine', options=RaphenDeathCompany.common_gear + [Gear('Bolt pistol'), Gear('Power fist')]),
                                                UnitDescription(name='Death Company Marine', options=RaphenDeathCompany.common_gear + [Gear('Bolt pistol'), Gear('Power sword')]),
                                                UnitDescription(name='Death Company Marine', options=RaphenDeathCompany.common_gear + [Gear('Inferno pistol'), Gear('Chainsword')]),
                                                UnitDescription(name='Death Company Marine', options=RaphenDeathCompany.common_gear + [Gear('Bolt pistol'), Gear('Chainsword')])])

    def build_statistics(self):
        return {'Models': 5, 'Units': 1}


class Cassor(Unit):
    type_name = 'Cassor the Damned'
    type_id = 'cassor_v3'

    def __init__(self, parent):
        super(Cassor, self).__init__(parent, points=140, static=True, unique=True, gear=[
            Gear('Blood talon', count=2), Gear('Meltagun'), Gear('Storm bolter'),
            Gear('Magna grapple'), Gear('Searchlight')])
