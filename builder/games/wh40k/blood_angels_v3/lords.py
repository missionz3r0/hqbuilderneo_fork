__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class Dante(Unit):
    type_name = 'Commander Dante, Chapter Master of the Blood Angels'
    type_id = 'dante_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Commander-Dante')

    def __init__(self, parent):
        super(Dante, self).__init__(parent, 'Commander Dante', 220, [
            Gear('Artificer armour'),
            Gear('Inferno pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron halo'),
            Gear('Jump pack'),
            Gear('The Axe Mortalis'),
            Gear('The Death Mask of Sanguinius')], True, True)


class Seth(Unit):
    type_name = 'Gabriel Seth, Chapter Master of the Flesh Tearers'
    type_id = 'seth_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Gabriel-Seth')

    def __init__(self, parent):
        super(Seth, self).__init__(parent, 'Gabriel Seth', 155, [
            Gear('Power armour'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Iron halo'),
            Gear('Blood Reaver')], True, True)
