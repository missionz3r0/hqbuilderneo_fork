__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OptionalSubUnit, SubUnit, UnitDescription,\
    Count, UnitList, OptionsList, ListSubUnit, OneOf
from builder.games.wh40k.unit import Unit, Squadron
from .armory import Vehicle, Melee, Ranged, Armour, BoltPistol, Ccw, IconBearerGroup,\
    IconicUnit, IconBearer, BaseWeapon, Special, CommonOptions, PlasmaPistol


class Spawn(Unit):
    type_name = 'Chaos Spawns'
    type_id = 'chaosspawns_v1'

    def __init__(self, parent):
        super(Spawn, self).__init__(parent=parent, gear=[Gear('Mark of Khorne')])
        self.models = Count(self, 'Chaos Spawn', min_limit=1, max_limit=5, points=32,
                            gear=UnitDescription('Chaos Spawn', points=32))

    def get_count(self):
        return self.models.cur


class Rhino(Unit):
    type_id = 'rhino_v1'
    type_name = 'Chaos Rhino'

    class Vehicle(Vehicle):
        def __init__(self, parent):
            super(Rhino.Vehicle, self).__init__(parent, tank=True)
            self.variant('Dozer blade', 5)

    def __init__(self, parent):
        super(Rhino, self).__init__(parent, name='Chaos Rhino', points=35,
                                    gear=[Gear('Combi-bolter'), Gear('Smoke launchers'), Gear('Searchlight')])
        self.Vehicle(self)


class RhinoTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(RhinoTransport, self).__init__(parent, 'Transport', order=1001)
        SubUnit(self, Rhino(parent=None))


class Bikers(IconicUnit):
    type_name = 'Chaos Bikers'
    type_id = 'chaos_bikers_v1'

    model_points = 22
    model_gear = Armour.power_armour_set + [Gear('Chaos bike')]

    class Champion(IconBearer):

        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(Bikers.Champion, self).__init__(
                parent, points=76 - Bikers.model_points * 2, name='Chaos Biker Champion',
                gear=Bikers.model_gear + [Gear('Twin-linked boltgun')],
                wrath=20)

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            CommonOptions(self)

    class Biker(IconBearerGroup):
        class TlBoltgun(BaseWeapon):
            def __init__(self, *args, **kwargs):
                super(Bikers.Biker.TlBoltgun, self).__init__(*args, **kwargs)
                self.tl_boltgun = self.variant('Twin-linked boltgun', 0)

        class Weapon1(Special, Ccw):
            pass

        class Weapon2(Special, TlBoltgun):
            pass

        def __init__(self, parent):
            super(Bikers.Biker, self).__init__(
                parent, name='Chaos Biker', points=Bikers.model_points,
                gear=Bikers.model_gear + [Gear('Bolt pistol')],
                wrath=20)
            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')

        def check_rules(self):
            super(Bikers.Biker, self).check_rules()
            self.wep1.activate_spec(not self.wep2.has_special())
            self.wep2.activate_spec(not self.wep1.has_special())

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.wep1.has_special() or self.wep2.has_special()

    def __init__(self, parent):
        super(Bikers, self).__init__(parent, gear=[Gear('Mark of Khorne')])
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Biker, min_limit=2, max_limit=9)

    def check_rules(self):
        super(Bikers, self).check_rules()
        spec = sum(o.count_spec() for o in self.models.units)
        if spec > 2:
            self.error("Only 2 chaos bikers can take special weapons (taken: {0}).".format(spec))

    def get_count(self):
        return self.models.count + 1


class Hounds(Unit):
    type_id = 'hounds_v1'
    type_name = 'Flesh Hounds'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/khorne-daemonkin/profiles#TOC-Flesh-Hounds'

    model_points = 16
    model = UnitDescription('Flesh Hound', model_points,
                            options=[Gear('Collar of Khorne')])

    def __init__(self, parent):
        super(Hounds, self).__init__(parent)
        self.models = Count(self, 'Flesh Hounds', 5, 20, self.model_points, per_model=True,
                            gear=Hounds.model)

    def get_count(self):
        return self.models.cur


class Raptors(IconicUnit):
    type_name = 'Raptors'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/khorne-daemonkin/profiles#TOC-Raptors'
    type_id = 'raptors_v1'

    model_points = 19
    model_gear = Armour.power_armour_set + [Gear('Jump pack')]

    class Champion(IconBearer):

        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(Raptors.Champion, self).__init__(
                parent, points=105 - Raptors.model_points * 4, name='Raptor Champion',
                gear=Bikers.model_gear,
                wrath=20)

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            CommonOptions(self)

    class Raptor(IconBearerGroup):
        class Pistol(PlasmaPistol, BoltPistol):
            pass

        class WeaponSpecial(OptionsList):
            def __init__(self, parent):
                super(Raptors.Raptor.WeaponSpecial, self).__init__(parent, 'Special weapon', limit=1)
                self.variant('Flamer', 5)
                self.variant('Meltagun', 10)
                self.variant('Plasma gun', 15)

        def __init__(self, parent):
            super(Raptors.Raptor, self).__init__(
                parent, name='Raptor', points=Raptors.model_points,
                gear=Bikers.model_gear + [Gear('Close combat weapon')],
                wrath=20)
            self.wep1 = self.Pistol(self, 'Pistol')
            self.wep2 = self.WeaponSpecial(self)

        def check_rules(self):
            super(Raptors.Raptor, self).check_rules()
            self.wep2.used = self.wep2.visible = not self.wep1.cur == self.wep1.plaspistol

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.wep1.cur == self.wep1.plaspistol or self.wep2.any

    def __init__(self, parent):
        super(Raptors, self).__init__(parent, gear=[Gear('Mark of Khorne')])
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Raptor, min_limit=4, max_limit=14)

    def check_rules(self):
        super(Raptors, self).check_rules()
        spec = sum(o.count_spec() for o in self.models.units)
        if spec > 2:
            self.error("Only 2 Raptors can take special weapons (taken: {0}).".format(spec))

    def get_count(self):
        return self.models.count + 1


class Talons(Unit):
    type_id = 'talons_v1'
    type_name = 'Warp Talons'
    model_points = 34
    model = UnitDescription('Warp Talon', points=model_points,
                            options=[
                                Gear('Power armour'),
                                Gear('Lightning claw', count=2),
                                Gear('Jump pack')
                            ])

    def __init__(self, parent):
        super(Talons, self).__init__(parent, points=180 - 4 * self.model_points)
        self.models = Count(self, 'Warp Talons', 4, 9, self.model_points, per_model=True,
                            gear=Talons.model)

    def get_count(self):
        return self.models.cur + 1

    def build_description(self):
        desc = super(Talons, self).build_description()
        desc.add(UnitDescription('Warp Talon Champion', points=Talons.model_points + 10,
                            options=[
                                Gear('Power armour'),
                                Gear('Lightning claw', count=2),
                                Gear('Jump pack')
                            ]))
        return desc


class Heldrake(Unit):
    type_name = 'Heldrake'

    type_id = 'heldrake_v1'

    def __init__(self, parent):
        super(Heldrake, self).__init__(parent=parent, points=170, gear=[Gear('Daemonic posession')])
        self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Heldrake.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Hades autocannon', 0)
            self.variant('Baleflamer', 0)


class Heldrakes(Squadron):
    type_name = 'Heldrakes'
    type_id = 'heldrakes_v1'
    unit_class = Heldrake
    unit_max = 4
