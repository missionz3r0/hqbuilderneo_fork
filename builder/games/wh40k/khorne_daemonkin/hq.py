__author__ = 'Ivan Truekov'

from builder.core2 import Gear, OptionsList, UnitDescription,\
    SubUnit
from builder.games.wh40k.roster import Unit
from .armory import ArtefactWeapon, BaseWeapon, Ranged, Melee, BoltPistol,\
    Ccw, Armour, Gifts, SpecialIssue, ArtefactGear, Hellblade, Loci


class Lord(Unit):
    type_name = 'Chaos Lord'
    type_id = 'chaos_lord_v1'

    class TerminatorRanged(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(Lord.TerminatorRanged, self).__init__(*args, **kwargs)
            self.tda_weapon += [
                self.variant('Combi-bolter', 0),
                self.variant('Combi-melta', 7),
                self.variant('Combi-flamer', 7),
                self.variant('Combi-plasma', 7),
                self.variant('Power weapon', 12),
                self.variant('Lightning claw', 17),
                self.variant('Power fist', 22),
                self.variant('Chainfist', 27)
            ]

    class TerminatorMelee(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(Lord.TerminatorMelee, self).__init__(*args, **kwargs)
            self.tda_weapon += [
                self.variant('Power weapon', 0),
                self.variant('Lightning claw', 5),
                self.variant('Power fist', 10),
                self.variant('Chainfist', 15),
                self.variant('Axe of Khorne', 20)
            ]

    class Weapon1(ArtefactWeapon, TerminatorRanged, Ranged, Melee, BoltPistol):
        pass

    class Weapon2(ArtefactWeapon, TerminatorMelee, Ranged, Melee, Ccw):
        pass

    def __init__(self, parent):
        super(Lord, self).__init__(parent, points=75)

        self.armour = Armour(self, tda=40)
        self.weapon1 = self.Weapon1(self, name='Weapon', armour=self.armour, lord=True)
        self.weapon2 = self.Weapon2(self, name='', armour=self.armour, lord=True)
        self.artefacts = ArtefactGear(self, armour=self.armour, lord=True)
        self.gifts = Gifts(self, armour=self.armour, lord=True)
        self.spec = SpecialIssue(self, armour=self.armour, gift=self.gifts)

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique()\
            + self.artefacts.get_unique()

    def check_rules(self):
        super(Lord, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Model can carry no more than one Artefact of Slaughter')


class DaemonPrince(Unit):
    type_name = "Daemon Prince"
    type_id = "daemon_prince_v1"

    class Options(OptionsList):
        def __init__(self, parent):
            super(DaemonPrince.Options, self).__init__(parent, name='Options')
            self.variant('Warp-forged armour', 20)
            self.variant('Daemonic flight', 40)

    class Weapon(ArtefactWeapon, Ccw):
        pass

    def __init__(self, parent):
        super(DaemonPrince, self).__init__(parent, points=160)
        self.weapon = self.Weapon(self, name='Weapon', prince=True)
        self.artefacts = ArtefactGear(self, prince=True)
        self.gifts = Gifts(self)
        self.opt = self.Options(self)

    def check_rules(self):
        super(DaemonPrince, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Model can carry no more than one Artefact of Slaughter')

    def get_unique_gear(self):
        return self.weapon.get_unique() + self.artefacts.get_unique()


class Herald(Unit):
    type_name = 'Herald'
    type_id = 'herald_v1'

    class Weapon(ArtefactWeapon, Hellblade):
        pass

    def __init__(self, parent):
        super(Herald, self).__init__(parent, points=55)
        self.wep = self.Weapon(self, 'Weapon')
        self.artefacts = ArtefactGear(self)
        self.gifts = Gifts(self, herald=True)
        Loci(self)

    def check_rules(self):
        super(Herald, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Model can carry no more than one Artefact of Slaughter')

    def get_unique_gear(self):
        return self.wep.get_unique() + self.artefacts.get_unique()


class BloodThrone(Unit):
    type_name = 'Blood Throne'
    type_id = 'blood_throne_v1'

    def __init__(self, parent):
        super(BloodThrone, self).__init__(parent, points=130 - 55,
                                          gear=[UnitDescription(name='Blood Throne')])
        self.herald = SubUnit(self, Herald(parent=None))

    def get_unique_gear(self):
        return self.herald.unit.get_unique_gear()


class Skulltaker(Unit):
    type_name = 'Skulltaker, The Champion of Khorne'
    type_id = 'skulltaker_v1'

    def __init__(self, parent):
        super(Skulltaker, self).__init__(parent, 'Skulltaker', 100, gear=[
            Gear('Cloak of Skulls'),
            Gear('The Slayer Sword'),
            Gear('Lesser Locus of Abjuration')
        ], unique=True, static=True)


class FuryThirster(Unit):
    type_name = 'Bloodthirster of Unfettered Fury'
    type_id = 'bloodthirster_fury_v1'

    def __init__(self, parent):
        super(FuryThirster, self).__init__(parent, self.type_name, 250, gear=[
            Gear('Warp-forged armour'),
            Gear('Lash of Khorne'),
            Gear('Axe of Khorne')
        ], static=True)


class RageThirster(Unit):
    type_name = 'Bloodthirster of Insensate Rage'
    type_id = 'bloodthirster_rage_v1'

    def __init__(self, parent):
        super(RageThirster, self).__init__(parent, self.type_name, 275, gear=[
            Gear('Warp-forged armour'),
            Gear('Great axe of Khorne')
        ], static=True)


class WrathThirster(Unit):
    type_name = 'Wrath of Khorne Bloodthirster'
    type_id = 'bloodthirster_wrath_v1'

    def __init__(self, parent):
        super(WrathThirster, self).__init__(parent, self.type_name, 300, gear=[
            Gear('Warp-forged armour'),
            Gear('Hellfire'),
            Gear('Bloodlflail'),
            Gear('Axe of Khorne')
        ], static=True)
