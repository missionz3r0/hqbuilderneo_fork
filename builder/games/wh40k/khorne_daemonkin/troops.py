__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UnitDescription, Count, OptionsList, \
    ListSubUnit, SubUnit, UnitList
from .armory import IconBearer, IconBearerGroup, IconicUnit, Heavy, Special, \
    Boltgun, BoltPistol, Ccw, PlasmaPistol, Armour, Ranged, Melee, CommonOptions
from builder.games.wh40k.roster import Unit
from .fast import RhinoTransport


class ChaosCultists(Unit):
    type_name = "Chaos Cultists"
    type_id = "chaos_cultists_v1"

    model_gear = [Gear('Improvised armour'), Gear('Close combat weapon')]
    model_points = 6
    model_desc = UnitDescription('Chaos Cultist', points=model_points, options=model_gear)

    class Champion(OptionsList):
        def __init__(self, parent):
            super(ChaosCultists.Champion, self).__init__(parent=parent, name='Cultist Champion', limit=None)
            self.variant('Shotgun', 2)

        @property
        def description(self):
            return [
                UnitDescription(
                    'Cultist Champion',
                    points=58 - 7 * ChaosCultists.model_points + super(ChaosCultists.Champion, self).points,
                    options=ChaosCultists.model_gear + [Gear('Autopistol')] + super(ChaosCultists.Champion, self).description
                )
            ]

    class Count(Count):
        @property
        def description(self):
            return [ChaosCultists.model_desc.clone().add(Gear('Autopistol')).set_count(
                self.cur - sum(o.cur for o in [self.parent.gun, self.parent.stubb, self.parent.flame])
            )]

    def __init__(self, parent):
        super(ChaosCultists, self).__init__(parent, gear=[Gear('Mark of Khorne')],
                                            points=58 - 7 * self.model_points)
        self.champion = self.Champion(self)
        self.models = self.Count(self, 'Chaos Cultist', 7, 34, self.model_points)
        self.gun = Count(
            self, 'Autogun', 0, 34, 1,
            gear=ChaosCultists.model_desc.clone().add_points(1).add(Gear('Autogun'))
        )
        self.stubb = Count(
            self, 'Heavy stubber', 0, 1, 5,
            gear=ChaosCultists.model_desc.clone().add_points(5).add(Gear('Heavy stubber')),
        )
        self.flame = Count(
            self, 'Flamer', 0, 1, 5,
            gear=ChaosCultists.model_desc.clone().add_points(5).add(Gear('Flamer')),
        )

    def check_rules(self):
        super(ChaosCultists, self).check_rules()
        Count.norm_counts(0, self.get_count() / 10, [self.stubb, self.flame])
        self.gun.max = self.models.cur - sum(o.cur for o in [self.stubb, self.flame])

    def get_count(self):
        return self.models.cur + 1


class ChaosSpaceMarines(IconicUnit):
    type_name = 'Chaos Space Marines'
    type_id = 'chaos_space_marines_v1'

    model_points = 15

    class Marine(IconBearerGroup):

        class Weapon1(Heavy, Special, Ccw, Boltgun):
            pass

        class Weapon2(PlasmaPistol, BoltPistol):
            def __init__(self, parent, boltgun):
                self.boltgun = boltgun
                super(ChaosSpaceMarines.Marine.Weapon2, self).__init__(parent=parent, name='')

            def check_rules(self):
                super(ChaosSpaceMarines.Marine.Weapon2, self).check_rules()
                self.plaspistol.active = not (self.boltgun.has_heavy() or self.boltgun.has_special())

        class Weapon3(OptionsList):
            def __init__(self, parent, boltgun):
                super(ChaosSpaceMarines.Marine.Weapon3, self).__init__(parent=parent, name='')
                self.boltgun = boltgun
                self.closecombatweapon = self.variant('Close combat weapon', 2)

            def check_rules(self):
                super(ChaosSpaceMarines.Marine.Weapon3, self).check_rules()
                self.used = self.visible = not self.boltgun.cur == self.boltgun.chainsword

        def __init__(self, parent):
            super(ChaosSpaceMarines.Marine, self).__init__(
                parent, name='Chaos Space Marines', points=ChaosSpaceMarines.model_points, gear=Armour.power_armour_set,
                wrath=20)
            self.weapon1 = self.Weapon1(self, 'Weapon')
            self.weapon2 = self.Weapon2(self, self.weapon1)
            self.weapon3 = self.Weapon3(self, self.weapon1)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.weapon1.has_heavy()

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon1.has_special()

        @ListSubUnit.count_gear
        def count_pistol(self):
            return self.weapon2.cur == self.weapon2.plaspistol

        def activate_heavy(self, val):
            for o in self.weapon1.heavy:
                o.active = val

    class Champion(IconBearer):

        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Weapon3(Ccw, Boltgun):
            pass

        class Weapon4(OptionsList):
            def __init__(self, parent, boltgun):
                super(ChaosSpaceMarines.Champion.Weapon4, self).__init__(parent=parent, name='')
                self.boltgun = boltgun
                self.closecombatweapon = self.variant('Close combat weapon', 2)
                self.used = False

            def check_rules(self):
                super(ChaosSpaceMarines.Champion.Weapon4, self).check_rules()
                self.used = self.visible = not self.boltgun.cur == self.boltgun.chainsword

        def __init__(self, parent):
            super(ChaosSpaceMarines.Champion, self).__init__(
                parent, points=130 - ChaosSpaceMarines.model_points * 7, name='Aspiring Champion',
                gear=Armour.power_armour_set,
                wrath=20)

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.wep3 = self.Weapon3(self, name='')
            self.wep4 = self.Weapon4(self, self.wep3)
            self.wep5 = self.Weapon1(self, name='')
            CommonOptions(self)

        def check_rules(self):
            super(ChaosSpaceMarines.Champion, self).check_rules()
            self.wep5.used = self.wep5.visible = self.wep4.any

    def __init__(self, parent):
        super(ChaosSpaceMarines, self).__init__(parent, gear=[Gear('Mark of Khorne')])
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Marine, min_limit=7, max_limit=19)

        self.transport = RhinoTransport(self)

    def check_rules(self):
        super(ChaosSpaceMarines, self).check_rules()
        for o in self.models.units:
            o.activate_heavy(self.get_count() >= 10)
        heavy = sum(o.count_heavy() for o in self.models.units)
        spec = sum(o.count_spec() for o in self.models.units)
        pistol = sum(o.count_pistol() for o in self.models.units)

        if self.get_count() >= 10:
            if heavy > 1:
                self.error("Only one marine can exchange his bolter for heavy weapon. (taken: {})".format(heavy))
            if spec + pistol + heavy > 2:
                self.error("Only 2 marines can take special weapons (taken: {0}).".format(spec + pistol + heavy))
        else:
            if heavy > 0:
                self.error("Only if the squad numbers ten or more models one marine can exchange his bolter for "
                           "heavy weapon. (taken: {})".format(heavy))
            if spec + pistol > 1:
                self.error("Only 1 marine can take special weapons (taken: {0}).".format(spec + pistol))

    def build_statistics(self):
        return self.note_transport(super(ChaosSpaceMarines, self).build_statistics())


class Berzerks(IconicUnit):
    type_name = 'Khorne Berzerkers'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/khorne-daemonkin/profiles#TOC-Berzerkers'
    type_id = 'khorne_berzerkers_v1'

    model_points = 19
    model_gear = Armour.power_armour_set

    class Options(OptionsList):
        def __init__(self, parent):
            super(Berzerks.Options, self).__init__(parent=parent, name='Options')
            self.chainaxe = self.variant('Chainaxe', 3)

    class ChampionOptions(Options, CommonOptions):
        pass

    class Berserk(IconBearerGroup):
        class Weapon(PlasmaPistol, BoltPistol):
            pass

        def __init__(self, parent):
            super(Berzerks.Berserk, self).__init__(
                parent, name='Khorne Berzerker', points=Berzerks.model_points,
                gear=Berzerks.model_gear + [Gear('Close combat weapon')]
            )

            self.weapon = self.Weapon(self, 'Weapon')
            self.opt = Berzerks.Options(self)

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon.cur != self.weapon.boltpistol

    class Champion(IconBearer):
        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(Berzerks.Champion, self).__init__(
                parent, name='Berzerker Champion', points=162 - Berzerks.model_points * 7,
                gear=Berzerks.model_gear, wrath=15
            )
            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.opt = Berzerks.ChampionOptions(self)

        def check_rules(self):
            super(Berzerks.Champion, self).check_rules()
            self.wep1.set_unique_ranged(not self.wep2.has_unique_ranged())
            self.wep2.set_unique_ranged(not self.wep1.has_unique_ranged())

    def __init__(self, parent):
        super(Berzerks, self).__init__(parent, gear=[Gear('Mark of Khorne')])
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Berserk, min_limit=7, max_limit=19)

        self.transport = RhinoTransport(self)

    def check_rules(self):
        super(Berzerks, self).check_rules()
        taken = sum((it.count_spec() for it in self.models.units))
        if taken > 2:
            self.error("Khorne Berzerkers can't have more then 2 plasma pistols in unit (taken: {0}).".format(taken))

    def get_count(self):
        return self.models.count + 1

    def build_statistics(self):
        return self.note_transport(super(Berzerks, self).build_statistics())


class Bloodletters(Unit):
    type_id = 'bloodletters_v1'
    type_name = 'Bloodletters'
    model_points = 10
    model = UnitDescription('Bloodletter', points=model_points,
                            options=[Gear('Hellblade')])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Bloodletters.Options, self).__init__(parent, 'Options')
            self.reaper = self.variant('Bloodreaper', 5)
            self.instrument = self.variant('Instrument of Chaos', 10)
            self.banner = self.variant('Banner of Blood', 20)

    def __init__(self, parent):
        super(Bloodletters, self).__init__(parent)
        self.models = Count(self, 'Bloodletters', 8, 20, self.model_points, per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur

    def build_description(self):
        desc = UnitDescription(self.name, self.points)
        diff = sum([o.value for o in self.opt.options])
        desc.add(self.model.clone().set_count(self.models.cur - diff))
        if self.opt.reaper.value:
            desc.add(UnitDescription(name='Bloodreaper', points=20, options=[Gear('Hellblade')]))
        if self.opt.banner.value:
            desc.add(self.model.clone().add(Gear('Banner of Blood')).add_points(self.opt.banner.points))
        if self.opt.instrument.value:
            desc.add(self.model.clone().add(Gear('Instrument of Chaos')).add_points(self.opt.instrument.points))
        return desc
