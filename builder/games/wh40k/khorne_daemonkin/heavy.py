__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OptionalSubUnit, SubUnit,\
    OneOf, OptionsList
from builder.games.wh40k.roster import Unit
from .armory import Vehicle


class LandRaider(Unit):
    type_name = "Chaos Land Raider"
    type_id = "chaos_land_raider_v1"

    def __init__(self, parent):
        super(LandRaider, self).__init__(
            parent, points=230,
            gear=[Gear('Twin-linked heavy bolter'), Gear('Twin-linked lascannon', count=2), Gear('Smoke launchers'),
                  Gear('Searchlight')]
        )
        Vehicle(self, tank=True)


class LandRaiderTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(LandRaiderTransport, self).__init__(parent, 'Transport', order=1001)
        SubUnit(self, LandRaider(parent=None))


class Forgefiend(Unit):
    type_name = 'Forgefiend'
    type_id = 'forgefiend_v1'

    def __init__(self, parent):
        super(Forgefiend, self).__init__(parent=parent, points=180, gear=[Gear('Daemonic posession'), ])
        self.Weapon1(self)
        self.Weapon2(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Forgefiend.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Hades autocannons', 0, gear=Gear('Hades autocannon', count=2))
            self.variant('Ectoplasma cannons', 0, gear=Gear('Ectoplasma cannon', count=2))

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(Forgefiend.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Ectoplasma cannon', 25)


class Maulerfiend(Unit):
    type_name = 'Maulerfiend'
    type_id = 'maulerfiend_v1'

    def __init__(self, parent):
        super(Maulerfiend, self).__init__(parent=parent, points=130, gear=[Gear('Daemonic posession'),
                                                                           Gear('Power fists', count=2)])
        self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Maulerfiend.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Magma cutters', 0, gear=Gear('Magma cutter', count=2))
            self.variant('Lasher tendrils', 10, gear=Gear('Lasher tendrils', count=2))


class Defiler(Unit):
    type_name = 'Defiler'
    type_id = 'defiler_v1'

    def __init__(self, parent):
        super(Defiler, self).__init__(
            parent=parent, points=200,
            gear=[Gear('Battle cannon'), Gear('Power fist', count=2), Gear('Searchlight'),
                  Gear('Daemonic posession'), Gear('Smoke launchers')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Defiler.Options, self).__init__(parent, 'Options')
            self.variant('Extra armour', 10)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Defiler.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Twin-linked heavy flamer', 0)
            self.variant('Havoc launcher', 5)
            self.variant('Power scourge', 25)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Defiler.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Reaper autocannon', 0)
            self.variant('Power fist', 0)
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Twin-linked lascannon', 20)


class Grinder(Unit):
    type_name = 'Soul Grinder'
    type_id = 'grinder_v1'

    def __init__(self, parent):
        super(Grinder, self).__init__(
            parent=parent, points=135,
            gear=[Gear('Harvester cannon'), Gear('Iron claw')])
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Grinder.Options, self).__init__(parent, 'Extra weapons')
            self.variant('Warpsword', 25)
            self.torrent = self.variant('Baleful torrent', 20)
            self.gaze = self.variant('Warp gaze', 25)
            self.phlegm = self.variant('Phlegm bombardment', 30)

        def check_rules(self):
            super(Grinder.Options, self).check_rules()
            OptionsList.process_limit([self.torrent, self.gaze, self.phlegm], 1)


class Helbrute(Unit):
    type_name = 'Helbrute'
    type_id = 'helbrute_v1'

    class Melta(OneOf):
        def __init__(self, parent, name):
            super(Helbrute.Melta, self).__init__(parent=parent, name=name)
            self.variant('Multi-melta', 0)
            self.powerfist = self.variant('Power fist', 0)
            self.variant('Power fist with built-in combi-bolter', 5, gear=[
                Gear('Power fist'), Gear('Combi-bolter')])
            self.variant('Reaper autocannon', 5)
            self.thunderhammer = self.variant('Thunder hammer', 5)
            self.variant('Twin-linked heavy bolter', 5)
            self.variant('Plasma cannon', 10)
            self.variant('Power scourge', 10)
            self.missilelauncher = self.variant('Missile launcher', 10)
            self.variant('Power fist with built-in heavy flamer', 15, gear=[
                Gear('Power fist'), Gear('Heavy flamer')])
            self.variant('Twin-linked lascannon')

        def has_missile(self):
            return self.cur == self.missilelauncher

        def activate_missile(self, val):
            self.missilelauncher.active = val

    class Fist(OneOf):
        def __init__(self, parent, name):
            super(Helbrute.Fist, self).__init__(parent=parent, name=name)
            self.powerfist = self.variant('Power fist', 0)
            self.variant('Power fist with built-in combi-bolter', 5, gear=[
                Gear('Power fist'), Gear('Combi-bolter')])
            self.thunderhammer = self.variant('Thunder hammer', 5)
            self.variant('Power scourge', 10)
            self.missilelauncher = self.variant('Missile launcher', 10)
            self.variant('Power fist with built-in heavy flamer', 15, gear=[
                Gear('Power fist'), Gear('Heavy flamer')])

        def has_missile(self):
            return self.cur == self.missilelauncher

        def activate_missile(self, val):
            self.missilelauncher.active = val

    def __init__(self, parent):
        super(Helbrute, self).__init__(parent=parent, points=100, gear=[])
        self.wep1 = self.Melta(self, 'Weapon')
        self.wep2 = self.Fist(self, '')

    def check_rules(self):
        super(Helbrute, self).check_rules()
        self.wep1.activate_missile(not self.wep2.has_missile())
        self.wep2.activate_missile(not self.wep1.has_missile())


class ScullCannon(Unit):
    type_id = 'scull_cannon_v1'
    type_name = 'Scull Cannon'

    def __init__(self, parent):
        super(ScullCannon, self).__init__(parent, points=125,
                                          gear=[Gear('Scull cannon')],
                                          static=True)
