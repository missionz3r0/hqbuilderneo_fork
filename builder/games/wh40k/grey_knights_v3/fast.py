__author__ = 'Ivan Truskov'

from builder.core2 import *
from .armory import VehicleEquipment, MasterWeapon, NFSword, Special, SpecialIssue, SpecialIssueWeapon, Melee
from builder.games.wh40k.unit import Unit, Squadron


class Rhino(Unit):
    type_id = 'gk_rhino_v3'
    type_name = "Rhino"
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Rhino')

    def __init__(self, parent, points=35):
        super(Rhino, self).__init__(parent=parent, points=points, gear=[
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.opt = VehicleEquipment(self)


class Razorback(Unit):
    type_id = 'gk_razorback_v3'
    type_name = 'Razorback'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Razorback')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent, 'Weapon')
            self.tlhbgun = self.variant('Twin-linked heavy bolter', 0)
            self.tlhflame = self.variant('Twin-linked heavy flamer', 0)
            self.tlasscan = self.variant('Twin-linked assault cannon', 20)
            self.tllcannon = self.variant('Twin-linked lascannon', 20)
            self.lascplasgun = self.variant('Lascannon and twin-linked plasma gun', 20, gear=[
                Gear('Lascannon'), Gear('Twin-linked plasma gun')
            ])

    def __init__(self, parent):
        super(Razorback, self).__init__(parent=parent, points=55, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.weapon = Razorback.Weapon(self)
        self.opt = VehicleEquipment(self)


class InterceptorSquad(Unit):
    type_name = 'Interceptor squad'
    type_id = 'interceptor_squad_v3'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Interceptor-Squad')

    model_points = 24
    model_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades'), Gear('Personal teleporter')]

    class Justicar(Unit):

        class MeleeWeapon(MasterWeapon, Melee, NFSword):
            pass

        class RangedOption(OptionsList):
            def __init__(self, parent):
                super(InterceptorSquad.Justicar.RangedOption, self).__init__(parent, name='')
                self.variant('Upgrade Storm Bolter to Master-crafted', 10)

            @property
            def description(self):
                if self.any:
                    return [Gear('Master-crafted Storm bolter', 10)]
                else:
                    return [Gear('Storm bolter')]

        def __init__(self, parent):
            super(InterceptorSquad.Justicar, self).__init__(
                name='Interceptor Justicar',
                parent=parent, points=InterceptorSquad.model_points + 10,
                gear=InterceptorSquad.model_gear
            )
            self.wep1 = SpecialIssueWeapon(self, self.MeleeWeapon, 'Melee weapon')
            self.wep2 = self.RangedOption(self)
            self.opt = SpecialIssue(self)

    class Interceptor(ListSubUnit):

        class MeleeWeapon(Melee, NFSword):
            pass

        class RangedWeapon(Special):
            pass

        def __init__(self, parent):
            super(InterceptorSquad.Interceptor, self).__init__(
                parent=parent, points=InterceptorSquad.model_points, name='Grey Knight',
                gear=InterceptorSquad.model_gear
            )
            self.mle = self.MeleeWeapon(self, 'Melee weapon')
            self.rng = self.RangedWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.rng.cur != self.rng.sb

    def __init__(self, parent):
        super(InterceptorSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Justicar(None))
        self.marines = UnitList(self, self.Interceptor, 4, 9)

    def check_rules(self):
        super(InterceptorSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.marines.units)
        if 5 * spec_total > self.get_count():
            self.error('In Intercaptor squad may be only 1 special weapon per 5 models (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.marines.count + 1

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(InterceptorSquad, self).build_statistics())


class StormravenGunship(Unit):
    type_name = 'Stormraven Gunship'
    type_id = 'gk_stormraven_gunship_v3'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Stormraven-Gunship')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon1, self).__init__(parent, 'Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedmultimelta = self.variant('Twin-linked Multi-melta', 0)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 25)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon2, self).__init__(parent, '')
            self.twinlinkedassaultcannon = self.variant('Twin-linked Assault cannon', 0)
            self.twinlinkedplasmacannon = self.variant('Twin-linked Plasma cannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked Lascannon', 0)

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormravenGunship.Options, self).__init__(parent, 'Options')
            self.hurricanebolters = self.variant('Side sponsons with hurricane bolters', 30, gear=[
                Gear('Hurricane bolter', count=2)
            ])
            self.searchlight = self.variant('Searchlight', 1)
            self.extraarmour = self.variant('Extra armour', 5)
            self.locatorbeacon = self.variant('Locator beacon', 10)

    def __init__(self, parent):
        super(StormravenGunship, self).__init__(parent=parent, points=200, gear=[
            Gear('Ceramite plating'),
            Gear('Stormstrike missile', count=4),
        ])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.opt = self.Options(self)


class Stormravens(Squadron):
    type_name = 'Stormraven Gunships'
    type_id = 'gk_stormraven_wing_v3'
    unit_class = StormravenGunship
    unit_max = 4
