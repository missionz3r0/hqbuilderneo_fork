from builder.core2 import *
from .armory import *
from .fast import Rhino, Razorback
from .heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer
from builder.games.wh40k.grey_knights_v3.armory import Melee
from builder.games.wh40k.roster import Unit

__author__ = 'Ivan Truskov'


class PurifierSquad(Unit):
    type_name = 'Purifier squad'
    type_id = 'purifier_squad_v3'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Purifier-Squad')

    model_points = 25
    model_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')]

    class KnightOfFlame(Unit):

        class MeleeWeapon(MasterWeapon, Melee, NFSword):
            pass

        class RangedOption(OptionsList):
            def __init__(self, parent):
                super(PurifierSquad.KnightOfFlame.RangedOption, self).__init__(parent, name='')
                self.variant('Upgrade Storm Bolter to Master-crafted', 10)

            @property
            def description(self):
                if self.any:
                    return [Gear('Master-crafted Storm bolter', 10)]
                else:
                    return [Gear('Storm bolter')]

        def __init__(self, parent):
            super(PurifierSquad.KnightOfFlame, self).__init__(
                name='Knight of the Flame',
                parent=parent, points=PurifierSquad.model_points,
                gear=PurifierSquad.model_gear
            )
            self.wep1 = SpecialIssueWeapon(self, self.MeleeWeapon, 'Melee weapon')
            self.wep2 = self.RangedOption(self)
            self.opt = SpecialIssue(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(PurifierSquad.Transport, self).__init__(parent, 'Transport', limit=1)
            self.models = [
                SubUnit(self, Rhino(parent=None)),
                SubUnit(self, Razorback(parent=None))
            ]

    class Purifier(ListSubUnit):

        class MeleeWeapon(Melee, NFSword):
            pass

        class RangedWeapon(Special):
            pass

        def __init__(self, parent):
            super(PurifierSquad.Purifier, self).__init__(
                parent=parent, points=PurifierSquad.model_points, name='Purifier',
                gear=PurifierSquad.model_gear
            )
            self.mle = self.MeleeWeapon(self, 'Melee weapon')
            self.rng = self.RangedWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.rng.cur != self.rng.sb

    def __init__(self, parent):
        super(PurifierSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.KnightOfFlame(None))
        self.marines = UnitList(self, self.Purifier, 4, 9)
        self.transport = self.Transport(parent=self)

    def check_rules(self):
        super(PurifierSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.marines.units)
        if 5 * spec_total > 2 * self.get_count():
            self.error('In Purifier squad may be only 2 special weapons per 5 models (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.marines.count + 1

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(PurifierSquad, self).build_statistics()))


class PaladinSquad(Unit):
    type_name = 'Paladin squad'
    type_id = 'paladin_squad_v3'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Paladin-Squad')

    model_points = 55
    model_gear = [Gear('Terminator armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')]

    class Paladin(ListSubUnit):

        class MeleeWeapon(MasterWeapon, Melee, NFSword):
            pass

        class RangedWeapon(MasterWeapon, TerminatorSpecial):
            pass

        class Upgrade(OptionsList):
            def __init__(self, parent):
                super(PaladinSquad.Paladin.Upgrade, self).__init__(parent, "Upgrade")
                self.variant("Upgrade to apothecary", 20, gear=[Gear("Narthecium")])

        class Banners(OptionsList):
            def __init__(self, parent):
                super(PaladinSquad.Paladin.Banners, self).__init__(parent, "Banner", limit=1)
                self.variant('Brotherhood banner', 25)
                self.uniq = self.variant('The Nemesis Banner', 35)

            def get_unique(self):
                if self.uniq.value:
                    return self.description
                return []

        def __init__(self, parent):
            super(PaladinSquad.Paladin, self).__init__(
                name='Paladin',
                parent=parent, points=PaladinSquad.model_points,
                gear=PaladinSquad.model_gear
            )
            self.mle = SpecialIssueWeapon(self, self.MeleeWeapon, 'Melee weapon')
            self.rng = SpecialIssueWeapon(self, self.RangedWeapon, 'Ranged weapon')
            self.opt = SpecialIssue(self)
            self.ban = self.Banners(self)
            self.prom = self.Upgrade(self)

        def check_rules(self):
            if self.prom.any:
                self.name = 'Apothecary'
            else:
                self.name = 'Paladin'
            self.rng.wep.visible = self.rng.wep.used = self.rng.flag.visible = self.rng.flag.used = self.ban.visible = self.ban.used = not self.prom.any

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.ban.any

        @ListSubUnit.count_gear
        def count_medics(self):
            return self.prom.any

        @ListSubUnit.count_gear
        def count_special(self):
            return self.rng.wep.cur != self.rng.wep.sb

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(PaladinSquad.Transport, self).__init__(parent, 'Transport', limit=1)
            self.models = [
                SubUnit(self, LandRaider(parent=None)),
                SubUnit(self, LandRaiderCrusader(parent=None)),
                SubUnit(self, LandRaiderRedeemer(parent=None))
            ]

    def __init__(self, parent):
        super(PaladinSquad, self).__init__(parent=parent)
        self.pals = UnitList(self, self.Paladin, 3, 10)
        self.transport = self.Transport(parent=self)

    def get_unique_gear(self):
        return sum((pal.ban.get_unique() for pal in self.pals.units), [])

    def check_rules(self):
        super(PaladinSquad, self).check_rules()
        spec_total = sum(pal.count_special() for pal in self.pals.units)
        if 5 * spec_total > 2 * self.get_count():
            self.error('In Paladin squad may be only 2 special weapon per 5 models (taken: {0})'.format(spec_total))
        medics = sum(pal.count_medics() for pal in self.pals.units)
        if medics > 1:
            self.error("Paladin squad may include only one Apothecary")
        flags = sum(pal.count_banners() for pal in self.pals.units)
        if flags > 1:
            self.error('Only one banner per unit may be taken; taken: {}'.format(flags))

    def get_count(self):
        return self.pals.count

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(PaladinSquad, self).build_statistics()))


class Dreadnought(Unit):
    type_name = 'Dreadnought'
    type_id = 'gk_dread_v3'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Dreadnought')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon, self).__init__(parent, name='')
            self.variant('Power fist with built-in storm bolter', 0, gear=[Gear('Power fist'), Gear('Storm bolter')])
            self.variant('Power fist with built-in heavy flamer', 10, gear=[Gear('Power fist'), Gear('Heavy flamer')])
            self.variant('Missile launcher', 10)
            self.variant('Twin-linked autocannon', 15)

    class Upgrade(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Upgrade, self).__init__(parent, 'Upgrade')
            self.variant('Upgrade to Venerable Dreadnought', 25, gear=[])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Options, self).__init__(parent, 'Options')
            self.variant('Extra armour', 10)

    def __init__(self, parent):
        super(Dreadnought, self).__init__(parent, 'Dreadnought', 125, [Gear('Searchlights'), Gear('Smoke launchers')])
        self.lwep = DreadWeapon(self)
        self.rwep = self.Weapon(self)
        self.opt = self.Options(self)
        self.prom = self.Upgrade(self)

    def check_rules(self):
        if self.prom.any:
            self.name = 'Venerable Dreadnought'
        else:
            self.name = 'Dreadnought'

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Dreadnought, self).build_statistics())
