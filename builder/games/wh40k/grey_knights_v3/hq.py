from builder.games.wh40k.grey_knights_v3.armory import Melee, TerminatorSpecial,\
    SpecialIssue, SpecialIssueWeapon, NFSword, MasterWeapon, ArcanaRelicWeapon, HolyRelicWeapon,\
    HolyRelics, PowerAxe
from builder.core2 import *
from builder.games.wh40k.roster import Unit

__author__ = 'Ivan Truskov'


class BrotherCaptain(Unit):
    type_id = 'gk_captain_v3'
    type_name = 'Brother-Captain'

    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Brother-Captain')

    class MeleeWeapon(HolyRelicWeapon, Melee, NFSword):
        pass

    class RangedWeapon(ArcanaRelicWeapon, TerminatorSpecial):
        pass

    class Upgrade(OptionsList):
        def __init__(self, parent):
            super(BrotherCaptain.Upgrade, self).__init__(parent, 'Upgrade')
            self.prom = self.variant('Upgrade to Grand-Master', 35, gear=[])

    def __init__(self, parent):
        super(BrotherCaptain, self).__init__(parent=parent, points=150, gear=[Gear('Iron halo'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')])
        self.mle = SpecialIssueWeapon(self, self.MeleeWeapon, 'Melee weapon')
        self.rng = SpecialIssueWeapon(self, self.RangedWeapon, 'Ranged weapon', melee=False)
        self.spec = SpecialIssue(self)
        self.rlc = HolyRelics(self)
        self.prom = self.Upgrade(self)

    def check_rules(self):
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

        if self.prom.any:
            self.name = 'Grand-Master'
        else:
            self.name = self.type_name

    def get_unique_gear(self):
        return self.rlc.description + self.mle.get_unique() + self.rng.get_unique()

    def build_description(self):
        desc = super(BrotherCaptain, self).build_description()
        if not self.rlc.armour.value:
            desc.add(Gear('Terminator armour'))
        return desc

    def count_charges(self):
        if self.prom.any:
            return 2
        else:
            return 1

    def build_statistics(self):
        return self.note_charges(super(BrotherCaptain, self).build_statistics())


class GrandMaster(BrotherCaptain):
    type_id = 'gk_grandmaster_v3'
    type_name = 'Grand Master'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Brother-Captain')

    def __init__(self, parent):
        super(GrandMaster, self).__init__(parent)
        self.prom.visible = False
        self.prom.prom.value = True


class Stern(Unit):
    type_id = 'captain_stern_v2'
    type_name = 'Brother-Captain Stern'

    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Brother-Captain-Stern')

    def __init__(self, parent):
        super(Stern, self).__init__(parent=parent, points=185, unique=True, static=True, gear=[
            Gear('Terminator armour'),
            Gear('Storm bolter'),
            Gear('Nemesis force sword'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
            Gear('Iron Halo')
        ])

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Stern, self).build_statistics())


class Champion(Unit):
    type_id = 'champion_v2'
    type_name = 'Brotherhood champion'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Brotherhood-Champion')

    def __init__(self, parent):
        super(Champion, self).__init__(parent=parent, points=150, static=True, gear=[
            Gear('Artificer armour'),
            Gear('Storm bolter'),
            Gear('Master-crafted Nemesis force sword'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
            Gear('Iron Halo')
        ])

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Champion, self).build_statistics())


class Crowe(Unit):
    type_id = 'crowe_v2'
    type_name = 'Castellan Crowe'
    blade = Gear('The Blade of Antwyr')
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Castellan-Crowe')

    def __init__(self, parent):
        super(Crowe, self).__init__(parent=parent, points=175, unique=True, static=True, gear=[
            Gear('Artificer armour'),
            Gear('Storm bolter'),
            self.blade,
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
            Gear('Iron Halo')
        ])

    def get_unique_gear(self):
        return [self.blade]

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Crowe, self).build_statistics())


class Librarian(Unit):
    type_id = 'gk_librarian_v1'
    type_name = 'Librarian'

    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Librarian')

    class MeleeWeapon(MasterWeapon):
        def __init__(self, parent, name, buddy):
            super(Librarian.MeleeWeapon, self).__init__(parent, name, buddy)
            self.variant('Nemesis warding stave', 0)
            self.variant('Nemesis force sword', 0)
            self.variant('Nemesis force halberd', 0)
            self.variant('Two Nemesis falcions', 0, gear=[Gear('Nemesis falcion', count=2)])
            self.variant('Nemesis Daemon hammer', 5)

    class RelicMelee(HolyRelicWeapon, MeleeWeapon):
        pass

    class RangedWeapon(MasterWeapon):
        def __init__(self, parent, name, buddy):
            super(Librarian.RangedWeapon, self).__init__(parent, name, buddy)
            self.dfl = self.variant('No weapon', 0)
            self.sb = self.variant('Storm bolter', 5)
            self.variant('Combi-melta', 10)
            self.variant('Combi-flamer', 10)
            self.variant('Combi-plasma', 10)

        def check_rules(self):
            super(Librarian.RangedWeapon, self).check_rules()
            self.buddy.used = self.buddy.visible = self.buddy.used and not self.cur == self.dfl

        @property
        def description(self):
            if self.cur == self.dfl:
                return []
            else:
                return super(Librarian.RangedWeapon, self).description

    class RelicRanged(ArcanaRelicWeapon, RangedWeapon):
        pass

    class Level(OneOf):
        def __init__(self, parent):
            super(Librarian.Level, self).__init__(parent, 'Mastery level')
            self.level2 = self.variant('Mastery level 2', 0)
            self.level3 = self.variant('Mastery level 3', 25)

        def count_charges(self):
            if self.cur == self.level2:
                return 2
            else:
                return 3

    def __init__(self, parent):
        super(Librarian, self).__init__(parent=parent, points=110, gear=[Gear('Psychic hood'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')])
        self.mle = SpecialIssueWeapon(self, self.RelicMelee, 'Melee weapon')
        self.rng = SpecialIssueWeapon(self, self.RelicRanged, 'Ranged weapon', melee=False)
        self.spec = SpecialIssue(self)
        self.rlc = HolyRelics(self)
        self.prom = self.Level(self)

    def check_rules(self):
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def count_charges(self):
        return self.prom.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Librarian, self).build_statistics())

    def get_unique_gear(self):
        return self.rlc.description + self.mle.get_unique() + self.rng.get_unique()

    def build_description(self):
        desc = super(Librarian, self).build_description()
        if not self.rlc.armour.value:
            desc.add(Gear('Terminator armour'))
        return desc


class Techmarine(Unit):
    type_id = 'gk_techmarine_v2'
    type_name = 'Techmarine'

    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Techmarine')

    class MeleeWeapon(HolyRelicWeapon, Melee, PowerAxe):
        pass

    class RangedWeapon(MasterWeapon):
        def __init__(self, parent, name, buddy):
            super(Techmarine.RangedWeapon, self).__init__(parent, name, buddy)
            self.variant('Boltgun', 0)
            self.variant('Storm bolter', 3)
            self.cb = self.variant('Conversion beamer', 20)

    class RelicRanged(ArcanaRelicWeapon, RangedWeapon):
        pass

    def __init__(self, parent):
        super(Techmarine, self).__init__(parent=parent, points=90, gear=[Gear('Artificer armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')])
        self.mle = SpecialIssueWeapon(self, self.MeleeWeapon, 'Melee weapon')
        self.rng = SpecialIssueWeapon(self, self.RelicRanged, 'Ranged weapon', melee=False)
        self.spec = SpecialIssue(self)
        self.rlc = HolyRelics(self, armour_allowed=False)

    def check_rules(self):
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.rlc.description + self.mle.get_unique() + self.rng.get_unique()

    def build_description(self):
        desc = super(Techmarine, self).build_description()
        if self.rng.wep.cb != self.rng.wep.cur:
            desc.add(Gear('Servo-harness'))
        return desc

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Techmarine, self).build_statistics())


class Voldus(Unit):
    type_id = 'gk_voldus_v3'
    type_name = 'Grand Master Voldus, Warden of the Librarius'

    def __init__(self, parent):
        super(Voldus, self).__init__(parent, 'Grand Master Voldus', points=240,
                                     static=True, unique=True, gear=[
                                         Gear('Terminator armour'),
                                         Gear('Storm bolter'),
                                         Gear('Frag grenades'),
                                         Gear('Krak grenades'),
                                         Gear('Psyk-out grenades'),
                                         Gear('Iron halo'),
                                         Gear('Malleus Argyrum')
                                     ])
