__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Gear, OneOf,\
    UnitDescription, SubUnit, Count
from builder.games.wh40k.unit import Unit
from .transport import SiegeTransport, StormTransport
from .armory import Weapon, Bombs


class Commander(Unit):
    type_name = 'Company Commander'

    def __init__(self, parent):
        super(Commander, self).__init__(parent, gear=[
            Gear('Caparace armour'), Gear('Refractor field'),
            Gear('Frag grenades'), Gear('Krak grenades')])
        self.melee = Weapon(self, 'Weapon', 'Close combat weapon')
        self.ranged = Weapon(self, '', 'Laspistol')
        Bombs(self)


class SiegeAdvisors(OptionsList):
    def __init__(self, parent):
        super(SiegeAdvisors, self).__init__(parent, 'Advisors')
        self.variant('Master of Ordnance', 20, gear=[UnitDescription(
            'Master of Ordnance', 20, options=[Gear('Flak armour'),
                                               Gear('Laspistol'),
                                               Gear('Close combat weapon'),
                                               Gear('Frag grenades')]
        )])
        self.variant('Officer of the fleet', 20, gear=UnitDescription(
            'Officer of the Fleet', 20, options=[Gear('Flak armour'),
                                               Gear('Laspistol'),
                                               Gear('Close combat weapon'),
                                               Gear('Frag grenades')]
        ))

    def get_count(self):
        return sum(opt.value for opt in self.options)


class CommandSquad(Unit):
    type_name = 'Death Korps Company Command Squad'
    type_id = 'company_command_squad_krieg_v1'
    imperial_armour = True

    veteran = UnitDescription('Veteran', points=12, options=[
        Gear('Close combat weapon'),
        Gear('Frag grenades'),
        Gear('Krak grenades')
    ])

    class VeteranWeaponTeam(OptionsList):

        def __init__(self, parent):
            super(CommandSquad.VeteranWeaponTeam, self).__init__(
                parent, 'Veteran Weapon Team', limit=1)
            self.variant('Heavy bolter', 10)
            self.variant('Autocannon', 10)
            self.variant('Twin-linked heavy stubbers', 15)

    def get_bearer_desc(self):
        return self.veteran.clone().add([Gear('Laspistol'),
                                         Gear('Regimental standard')]). \
                                         add(Gear('Caparace armour')
                                             if self.vet_opt.caparace.value
                                             else Gear('Flak armour'))

    def get_advisors(self):
        return SiegeAdvisors

    def get_transport(self):
        return SiegeTransport

    def get_commander(self):
        return Commander

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent, points=100)
        self.commander = SubUnit(self, self.get_commander()(self))
        self.vet_opt = self.VeteranOptions(self)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=2, points=g['points'])
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Greanade launcher', points=5),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15)
            ]
        ]
        self.hwt = self.VeteranWeaponTeam(self)
        self.advisors = self.get_advisors()(self)
        self.transport = self.get_transport()(self)
        self.transport.order = 100

    class VeteranOptions(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.VeteranOptions, self).__init__(parent, name='Veteran options', used=False)
            self.caparace = self.variant('Carapace armour', 10)
            self.vox = self.variant('Vox-caster', 10)

    def check_rules(self):
        super(CommandSquad, self).check_rules()
        other = 3 - self.vet_opt.vox.value - 2 * self.hwt.any
        Count.norm_counts(0, min(2, other), self.spec)

    def get_count(self):
        return 5 + self.advisors.get_count()

    def build_statistics(self):
        return self.note_transport(super(CommandSquad, self).build_statistics())

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        res.add(self.commander.description)
        common_desc = self.veteran.clone().add(Gear('Caparace armour') if self.vet_opt.caparace.value else Gear('Flak armour'))
        res.add(self.get_bearer_desc())
        gcnt = 3
        if self.vet_opt.vox.value:
            res.add(common_desc.clone().add([Gear('Lasgun'), Gear('Vox-caster')]))
            gcnt -= 1
        for c in self.spec:
            if c.cur > 0:
                res.add(common_desc.clone().add(c.gear).set_count(c.cur))
                gcnt -= c.cur

        if self.hwt.any:
            res.add(UnitDescription('Veteran Weapons Team', options=[
                Gear('Close combat weapon'),
                Gear('Frag grenades'),
                Gear('Krak grenades')
            ] + [Gear('Caparace armour') if self.vet_opt.caparace.value
                 else Gear('Flak armour')] + self.hwt.description))
            gcnt -= 2
        if gcnt > 0:
            res.add(common_desc.add(Gear('Lasgun')).set_count(gcnt))
        res.add(self.advisors.description)

        res.add(self.transport.description)

        return res


class AssaultCommander(Unit):
    type_name = 'Company Commander'

    class Bombs(OptionsList):
        def __init__(self, parent):
            super(AssaultCommander.Bombs, self).__init__(parent, 'Options')
            self.variant('Melta-bombs', 5)
            self.variant('Memento Mori', 10)

    def __init__(self, parent):
        super(AssaultCommander, self).__init__(parent, gear=[
            Gear('Caparace armour'), Gear('Refractor field'),
            Gear('Frag grenades'), Gear('Krak grenades')])
        self.melee = Weapon(self, 'Weapon', 'Close combat weapon')
        self.ranged = Weapon(self, '', 'Laspistol')
        self.Bombs(self)


class AssaultAdvisors(OptionsList):
    def __init__(self, parent):
        super(AssaultAdvisors, self).__init__(parent, 'Advisors')
        self.q = self.variant('Quartermaster', 30, gear=[UnitDescription(
            'Quartermaster', 30, options=[Gear('Caparace armour'),
                                          Gear('Laspistol'),
                                          Gear('Close combat weapon'),
                                          Gear('Frag grenades'),
                                          Gear('Medi-pack')]
        )])
        self.a = self.variant('Artillerist', 30, gear=UnitDescription(
            'Artilerist', 30, options=[Gear('Flak armour'),
                                       Gear('Laspistol'),
                                       Gear('Close combat weapon'),
                                       Gear('Frag grenades'),
                                       Gear('Krak grenades'),
                                       Gear('Battle scope')]
        ))
        self.priest = self.variant('Tech-Priest Militant', 30, gear=[])
        self.bombs = self.variant('Melta-bombs', 5, gear=[])

    def get_count(self):
        return sum(opt.value for opt in [self.q, self.q, self.priest])

    @property
    def description(self):
        res = super(AssaultAdvisors, self).description
        if self.priest.value:
            priest = UnitDescription('Tech-Priest Militant', 30, options=[
                Gear('Power armour'), Gear('Laspistol'), Gear('Power axe'),
                Gear('Frag grenades'), Gear('Servo arm')
            ])
            if self.bombs.value:
                priest.add(Gear('Melta-bombs')).add_points(5)
            res.append(priest)
        return res

    def check_rules(self):
        super(AssaultAdvisors, self).check_rules()
        self.bombs.used = self.bombs.visible = self.priest.value


class AssaultCommandSquad(CommandSquad):

    def get_advisors(self):
        return AssaultAdvisors

    def get_transport(self):
        return StormTransport

    def get_commander(self):
        return AssaultCommander

    class RelicStandard(OptionsList):
        def __init__(self, parent):
            super(AssaultCommandSquad.RelicStandard, self).__init__(
                parent, 'Relic Standard', limit=1, used=False)
            self.variant('Icon of Righteous Spite', 20)
            self.variant('Banner of Martyrdom', 15)
            self.variant('Ossuary of the Blessed Dead', 25)

    def __init__(self, parent):
        super(AssaultCommandSquad, self).__init__(parent)
        self.relic = self.RelicStandard(self)

    def get_bearer_desc(self):
        return self.veteran.clone().add(Gear('Laspistol')).\
            add(self.relic.description if self.relic.any
                else [Gear('Regimental standard')]).\
            add(Gear('Caparace armour') if self.vet_opt.caparace.value
                else Gear('Flak armour')).add_points(self.relic.points)

    def get_unique_gear(self):
        if self.relic.any:
            return [Gear('Death Korps relic standard')]
        else:
            return []


class Venner(Unit):
    type_name = 'Death Korps Marshal Karis Venner'

    def __init__(self, parent):
        super(Venner, self).__init__(parent, 'Marshal Karis Venner', 165 - 100,
                                     [Gear('Caparace armour'),
                                      Gear('Hot-shot Laspistol'),
                                      Gear('Power sword'),
                                      Gear('Frag grenades'),
                                      Gear('Krak grenades'),
                                      Gear('Refractor field'),
                                      Gear('Memento mori')], True, True)


class VennerCommandSquad(AssaultCommandSquad):
    type_name = 'Death Korps Marshal Karis Venner'
    type_id = 'venner_v1'

    def get_commander(self):
        return Venner

    def get_unique(self):
        return 'Karis Venner'


class CommissarGeneral(Unit):
    type_name = 'Commissar-general'
    type_id = 'commissar_krieg_v1'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent, name, default):
            super(CommissarGeneral.Weapon, self).__init__(parent, name=name)
            self.variant(default, 0)
            self.variant('Boltgun')
            self.variant('Power weapon', 10)
            self.variant('Plasma pistol', 15)
            self.variant('Power fist', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(CommissarGeneral.Options, self).__init__(parent, 'Options')
            self.cap = self.variant('Caparace armour', 10)
            self.variant('Camo cloak', 10)
            self.variant('Melta-bombs', 5)

    def __init__(self, parent):
        super(CommissarGeneral, self).__init__(parent=parent, points=70, gear=[
            Gear('Refractor field'),
            Gear('Frag grenades'),
            Gear('Krak grenades')
        ])

        self.Weapon(self, 'Weapon', 'Bolt pistol')
        self.Weapon(self, '', 'Close combat weapon')
        self.opt = self.Options(self)
        self.transport = SiegeTransport(self)

    def build_description(self):
        res = super(CommissarGeneral, self).build_description()
        if not self.opt.cap.value:
            res.add(Gear('Flak armour'))
        return res

    def build_statistics(self):
        return self.note_transport(super(CommissarGeneral, self).build_statistics())


class QuartermasterCadre(Unit):
    type_name = 'Death Korps Quartermaster Cadre'
    type_id = 'quartermaster_krieg_v1'
    imperial_armour = True

    class Quartermaster(Unit):
        type_name = 'Quartermaster Revenant'

        class Weapon(OneOf):
            def __init__(self, parent, name, default):
                super(QuartermasterCadre.Quartermaster.Weapon, self).__init__(parent, name=name)
                self.variant(default, 0)
                if self.parent.roster.is_siege():
                    self.variant('Bolt pistol', 1)
                    self.variant('Power weapon', 10)
                else:
                    self.variant('Bolt pistol', 2)
                    self.variant('Power sword', 10)

        def __init__(self, parent):
            super(QuartermasterCadre.Quartermaster, self).__init__(parent, gear=[
                Gear('Caparace armour'), Gear('Medi-pack')])
            self.melee = self.Weapon(self, 'Weapon', 'Close combat weapon')
            self.ranged = self.Weapon(self, '', 'Laspistol')
            Bombs(self)

    def __init__(self, parent):
        super(QuartermasterCadre, self).__init__(parent, points=75 - 2 * 10)
        self.rev = SubUnit(self, self.Quartermaster(self))
        self.serv = Count(self, 'Medicae-servitor', 2, 4, 10, True,
                          gear=UnitDescription('Medicae-servitor', 10, options=[
                              Gear('Close combat weapon'), Gear('Caparace armour')
                          ]))

        self.transport = SiegeTransport(self)

    def get_count(self):
        return 1 + self.serv.cur

    def build_statistics(self):
        return self.note_transport(super(QuartermasterCadre, self).build_statistics())
