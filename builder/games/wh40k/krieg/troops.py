__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Gear, OneOf,\
    UnitDescription, SubUnit, ListSubUnit, UnitList,\
    OptionalSubUnit, Count
from builder.games.wh40k.unit import Unit
from .transport import SiegeTransport
from .armory import Weapon, Bombs


class Commander(Unit):
    type_name = 'Platoon Commander'

    class Bombs(OptionsList):
        def __init__(self, parent):
            super(Commander.Bombs, self).__init__(parent, 'Options')
            self.variant('Melta-bombs', 5)
            self.cap = self.variant('Caparace armour', 3)

    def __init__(self, parent, points=0):
        super(Commander, self).__init__(parent, points=points, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')])
        self.melee = Weapon(self, 'Weapon', 'Close combat weapon')
        self.ranged = Weapon(self, '', 'Laspistol')
        self.opt = self.Bombs(self)

    def build_description(self):
        res = super(Commander, self).build_description()
        if not self.opt.cap.value:
            res.add(Gear('Flak armour'))
        return res


class Commissar(Commander):
    type_name = 'Commissar'

    def __init__(self, parent):
        super(Commissar, self).__init__(parent, points=30)


class CommandSquad(Unit):
    type_name = 'Death Korps Platoon Command Squad'

    imperial_armour = True

    guardsman = UnitDescription('Guardsman', options=[
        Gear('Close combat weapon'),
        Gear('Frag grenades'),
        Gear('Krak grenades'),
        Gear('Flak armour')
    ])

    class Options(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.Options, self).__init__(parent, name='Options')
            spec = CommandSquad.guardsman.clone().add(Gear('Lasgun'))
            self.caster = self.variant('Vox-caster', 5, gear=[spec.clone().add(Gear('Vox-caster'))])
            self.standard = self.variant('Platoon standard', 10, gear=[spec.clone().add(Gear('Platoon standard'))])

    class Overseer(OptionalSubUnit):
        def __init__(self, parent):
            super(CommandSquad.Overseer, self).__init__(parent, 'Commissar')
            SubUnit(self, Commissar(parent=self))

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent, points=50)
        SubUnit(self, Commander(parent=self))
        self.com = self.Overseer(self)
        self.opt = self.Options(self)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=2, points=g['points'],
                  gear=CommandSquad.guardsman.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Greanade launcher', points=5),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15)
            ]
        ]

    def check_rules(self):
        super(CommandSquad, self).check_rules()
        Count.norm_counts(0, 2, self.spec)

    def count_models(self):
        return 5 + self.com.count

    def build_description(self):
        res = super(CommandSquad, self).build_description()
        gc = 4 - sum(o.value for o in self.opt.options) - sum(c.cur for c in self.spec)
        if gc > 0:
            res.add(CommandSquad.guardsman.clone().add(Gear('Lasgun')).set_count(gc))
        return res


class InfantrySquad(ListSubUnit):
    type_name = 'Death Korps Infantry Squad'
    imperial_armour = True

    guardsman = UnitDescription('Guardsman', options=[
        Gear('Close combat weapon'),
        Gear('Frag grenades'),
        Gear('Krak grenades'),
        Gear('Flak armour')
    ])

    class Watchmaster(Unit):
        type_name = 'Watchmaster'

        def __init__(self, parent):
            super(InfantrySquad.Watchmaster, self).__init__(parent, gear=[
                Gear('Flak armour'),
                Gear('Frag grenades'), Gear('Krak grenades')])
            self.melee = Weapon(self, 'Weapon', ['Close combat weapon', 'Lasgun'], fist=False)
            self.ranged = Weapon(self, '', ['Laspistol', 'Lasgun'], fist=False)

    class Special(OptionsList):
        def __init__(self, parent):
            super(InfantrySquad.Special, self).__init__(parent, 'Special weapon', limit=1)
            self.spec = [
                self.variant(g['name'], g['points'],
                      gear=InfantrySquad.guardsman.clone().add(Gear(g['name'])))
                for g in [
                        dict(name='Flamer', points=5),
                        dict(name='Greanade launcher', points=5),
                        dict(name='Meltagun', points=10),
                        dict(name='Plasma gun', points=15)
                ]
            ]

    def __init__(self, parent):
        super(InfantrySquad, self).__init__(parent, points=70)
        SubUnit(self, self.Watchmaster(parent=self))
        self.opt = CommandSquad.Options(self)
        self.spec = self.Special(self)

    @ListSubUnit.count_gear
    def count_models(self):
        return 10

    def build_description(self):
        res = super(InfantrySquad, self).build_description()
        gcnt = 9 - sum(o.value for o in self.opt.options) - self.spec.any
        res.add(self.guardsman.clone().add(Gear('Lasgun')).set_count(gcnt))
        return res


class HeavyWeaponSquad(ListSubUnit):
    type_name = 'Death Korps Heavy Weapons Squad'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(HeavyWeaponSquad.Weapon, self).__init__(parent, 'Heavy weapon')
            self.variant('Mortar')
            self.variant('Heavy bolter', 5)
            self.variant('Autocannon', 5)
            self.variant('Twin-linked heavy stubber', 5)
            self.variant('Lascannon', 15)

    def __init__(self, parent):
        super(HeavyWeaponSquad, self).__init__(parent, points=60)
        self.wep = [self.Weapon(self) for q in range(0, 3)]

    @ListSubUnit.count_gear
    def count_models(self):
        return 3

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        team = UnitDescription('Heavy Weapons Team', points=20, options=[
            Gear('Lasgun'), Gear('Close combat weapon'), Gear('Frag grenades'),
            Gear('Krak grenades'), Gear('Flak armour')
        ])
        for wep in self.wep:
            res.add_dup(team.clone().add_points(wep.points).add(wep.description))
        return res


class InfantryPlatoon(Unit):
    type_name = 'Death Korps Infantry Platoon'
    type_id = 'infantry_platoon_krieg_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(InfantryPlatoon.Options, self).__init__(parent, name='Squads', used=False)
            self.heavy = self.variant('Heavy Weapon Team')

    def __init__(self, parent):
        super(InfantryPlatoon, self).__init__(parent)
        self.command = SubUnit(self, CommandSquad(self))
        self.infantry = UnitList(self, InfantrySquad, 2, 6)
        self.opt = self.Options(self)
        self.hwt = UnitList(self, HeavyWeaponSquad, 1, 3)

    def check_rules(self):
        super(InfantryPlatoon, self).check_rules()
        self.opt.visible = self.parent.roster.is_siege()
        self.hwt.used = self.hwt.visible = self.opt.heavy.value

    def build_statistics(self):
        uc = 1 + self.infantry.count + self.opt.heavy.value * self.hwt.count
        mc = self.command.unit.count_models() + 10 * self.infantry.count +\
            3 * self.opt.heavy.value * self.hwt.count
        return {'Models': mc, 'Units': uc}


class Engineers(Unit):
    type_name = 'Death Korps Combat Engineer Squad'
    type_id = 'engineer_krieg_v1'

    imperial_armour = True

    engineer = UnitDescription('Engineer', options=[
        Gear('Close combat weapon'),
        Gear('Frag grenades'),
        Gear('Krak grenades'),
        Gear('Acid gas bombs'),
        Gear('Caparace armour')
    ], points=8)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Engineers.Options, self).__init__(parent, name='Options')
            spec = Engineers.engineer.clone().add(Gear('Shotgun'))
            self.caster = self.variant('Vox-caster', 5, gear=[spec.clone().add(Gear('Vox-caster')).add_points(5)])

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Engineers.Weapon, self).__init__(parent, name='Special weapon', limit=1)

            self.spec = [
                self.variant(g['name'], g['points'],
                             gear=Engineers.engineer.clone().add(Gear(g['name'])).add_points(g['points']))
                for g in [
                    dict(name='Flamer', points=5),
                    dict(name='Greanade launcher', points=5),
                    dict(name='Meltagun', points=10),
                    dict(name='Plasma gun', points=15),
                    dict(name='Demolition charge', points=20)
                ]]

    class HeavyWeapon(OptionsList):
        def __init__(self, parent):
            super(Engineers.HeavyWeapon, self).__init__(parent, name='Heavy weapon', limit=1)
            hwt = UnitDescription('Engineer Heavy Weapons Team', 16, options=[
                Gear('Close combat weapon'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
                Gear('Acid gas bombs'),
                Gear('Caparace armour')]
            )
            self.spec = [
                self.variant(g['name'], g['points'],
                             gear=hwt.clone().add(Gear(g['name'])).add_points(g['points']))
                for g in [
                        dict(name='Heavy flamer', points=10),
                        dict(name='Mole launcher', points=15)]
            ]

    class Engineer(Count):
        @property
        def description(self):
            return Engineers.engineer.clone().add(Gear('Shotgun')).set_count(
                self.cur - self.parent.opt.any - self.parent.spec.any - 2 * self.parent.heavy.any)

    class Watchmaster(Unit):
        type_name = 'Watchmaster'

        def __init__(self, parent):
            super(Engineers.Watchmaster, self).__init__(parent, points=50 - 8 * 4, gear=[
                Gear('Frag grenades'), Gear('Krak grenades'),
                Gear('Acid gas bombs'), Gear('Caparace armour')])
            self.melee = Weapon(self, 'Weapon', 'Close combat weapon')
            self.ranged = Weapon(self, '', 'Shotgun')
            Bombs(self)

    def get_transport(self):
        return SiegeTransport

    def __init__(self, parent):
        super(Engineers, self).__init__(parent)
        SubUnit(self, self.Watchmaster(self))
        self.opt = self.Options(self)
        self.models = self.Engineer(self, 'Engineer', 4, 9, 8, True)
        self.spec = self.Weapon(self)
        self.heavy = self.HeavyWeapon(self)
        self.transport = self.get_transport()(self)

    def get_count(self):
        return 1 + self.models.cur

    def build_statistics(self):
        return self.note_transport(super(Engineers, self).build_statistics())
