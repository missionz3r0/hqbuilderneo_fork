__author__ = 'Ivan Truskov'
__summary__ = ['Siege ov Vraks 2nd edition', 'Fall of Orpheus']


from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, LordsOfWarSection, UnitType
from builder.games.wh40k.fortifications import Fort

from .hq import CommandSquad, CommissarGeneral, QuartermasterCadre, AssaultCommandSquad,\
    VennerCommandSquad
from .elites import Grenadiers, HydraPlatform, Rapiers, FieldArtilleryBattery,\
    EliteEngineers, AssaultGrenadiers, AssaultEngineers, GriffonStrikeBattery,\
    ForwardTank, HydraBattery
from .troops import InfantryPlatoon, Engineers
from .fast import CyclopsSquad, HellhoundSquadron, DeathRiderPlatoon,\
    DeathRiderCommandPlatoon, AssaultHellhoundSquadron, SalamanderSquadron
from .heavy import ThundererSquadron, LemanRussSquadron, HeavyWeaponsPlatoon,\
    BombardBattery, HeavyArtilleryBattery, LemanRussAssaultSquadron,\
    OrdnanceBattery, ThunderSquadron, LightningSquadron, AvengerSquadron
from .lords import Baneblade, Macharius, MachariusVulcan, MachariusVanquisher
from builder.games.wh40k.imperial_armour.volume1 import Valdor, Crassus, Praetor, Gorgon, Malcador, MalcadorDefender, \
    MalcadorAnnihilator, MalcadorInfernus
from builder.games.wh40k.imperial_armour.apocalypse.imperial_navy import MaradeurBomber


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        UnitType(self, CommandSquad)
        UnitType(self, CommissarGeneral)
        UnitType(self, QuartermasterCadre)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Grenadiers)
        UnitType(self, HydraPlatform)
        UnitType(self, Rapiers)
        UnitType(self, FieldArtilleryBattery)
        UnitType(self, EliteEngineers)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, InfantryPlatoon)
        UnitType(self, Engineers)


class Fast(FastSection):
    def __init__(self, parent):
        super(Fast, self).__init__(parent)
        UnitType(self, CyclopsSquad)
        UnitType(self, HellhoundSquadron)
        UnitType(self, DeathRiderPlatoon)


class Heavy(HeavySection):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent)
        UnitType(self, ThundererSquadron)
        UnitType(self, LemanRussSquadron)
        UnitType(self, HeavyWeaponsPlatoon)
        UnitType(self, BombardBattery)
        UnitType(self, HeavyArtilleryBattery)


class Lords(LordsOfWarSection):
    def __init__(self, parent):
        super(Lords, self).__init__(parent)
        UnitType(self, Valdor)
        UnitType(self, Baneblade)
        UnitType(self, Crassus)
        UnitType(self, Praetor)
        UnitType(self, Gorgon)
        UnitType(self, Malcador)
        UnitType(self, MalcadorDefender)
        UnitType(self, MalcadorAnnihilator)
        UnitType(self, MalcadorInfernus)
        UnitType(self, MaradeurBomber)
        UnitType(self, Macharius)
        UnitType(self, MachariusVulcan)
        UnitType(self, MachariusVanquisher)


class AssaultHQ(HQSection):
    def __init__(self, parent):
        super(AssaultHQ, self).__init__(parent)
        self.squads = [UnitType(self, VennerCommandSquad),
                       UnitType(self, AssaultCommandSquad)]
        UnitType(self, QuartermasterCadre)


class AssaultElites(ElitesSection):
    def __init__(self, parent):
        super(AssaultElites, self).__init__(parent)
        UnitType(self, AssaultEngineers)
        UnitType(self, GriffonStrikeBattery)
        UnitType(self, ForwardTank)
        UnitType(self, HydraBattery)
        UnitType(self, Rapiers)


class AssaultTroops(TroopsSection):
    def __init__(self, parent):
        super(AssaultTroops, self).__init__(parent)
        UnitType(self, InfantryPlatoon)
        UnitType(self, AssaultGrenadiers)


class AssaultFast(FastSection):
    def __init__(self, parent):
        super(AssaultFast, self).__init__(parent)
        UnitType(self, AssaultHellhoundSquadron)
        UnitType(self, DeathRiderPlatoon)
        UnitType(self, SalamanderSquadron)


class AssaultHeavy(HeavySection):
    def __init__(self, parent):
        super(AssaultHeavy, self).__init__(parent)
        UnitType(self, HeavyWeaponsPlatoon)
        UnitType(self, ThundererSquadron)
        UnitType(self, LemanRussAssaultSquadron)
        UnitType(self, OrdnanceBattery)
        UnitType(self, FieldArtilleryBattery)
        UnitType(self, ThunderSquadron)
        UnitType(self, LightningSquadron)
        UnitType(self, AvengerSquadron)


class KriegSiegebase(Wh40kBase):
    army_id = 'krieg_siege_base'
    imperial_armour = True

    def is_siege(self):
        return True

    def __init__(self):
        super(KriegSiegebase, self).__init__(
            hq=HQ(self), elites=Elites(self),
            troops=Troops(self), fast=Fast(self),
            heavy=Heavy(self), lords=Lords(self),
            fort=Fort(parent=self)
        )


class KriegAssaultBase(Wh40kBase):
    army_id = 'krieg_assault_base'
    imperial_armour = True

    def is_siege(self):
        return False

    def __init__(self):
        super(KriegAssaultBase, self).__init__(
            hq=AssaultHQ(self), elites=AssaultElites(self),
            troops=AssaultTroops(self), fast=AssaultFast(self),
            heavy=AssaultHeavy(self), fort=Fort(self)
        )


class KriegSiegeCAD(KriegSiegebase, CombinedArmsDetachment):
    army_id = 'krieg_siege_cad'
    army_name = 'Death Korps of Krieg Siege Regiment (Combined Arms Detachment)'


class KriegSiegeAD(KriegSiegebase, AlliedDetachment):
    army_id = 'krieg_siege_ad'
    army_name = 'Death Korps of Krieg Siege Regiment (Allied Detachment)'


class KriegAssaultCAD(KriegAssaultBase, CombinedArmsDetachment):
    army_id = 'krieg_assault_cad'
    army_name = 'Death Korps of Krieg Assault Brigade (Combined Arms Detachment)'


class KriegAssaultAD(KriegAssaultBase, AlliedDetachment):
    army_id = 'krieg_assault_ad'
    army_name = 'Death Korps of Krieg Assault Brigade (Allied Detachment)'


class KriegSiegePA(KriegSiegebase, PlanetstrikeAttacker):
    army_id = 'krieg_siege_pa'
    army_name = 'Death Korps of Krieg Siege Regiment (Planetstrike attacker Detachment)'


class KriegSiegePD(KriegSiegebase, PlanetstrikeDefender):
    army_id = 'krieg_siege_pd'
    army_name = 'Death Korps of Krieg Siege Regiment (Planetstrike defender Detachment)'


class KriegAssaultPA(KriegAssaultBase, PlanetstrikeAttacker):
    army_id = 'krieg_assault_pa'
    army_name = 'Death Korps of Krieg Assault Brigade (Planetstrike attacker Detachment)'


class KriegAssaultPD(KriegAssaultBase, PlanetstrikeDefender):
    army_id = 'krieg_assault_pd'
    army_name = 'Death Korps of Krieg Assault Brigade (Planetstrike defender Detachment)'


class KriegSiegeSA(KriegSiegebase, SiegeAttacker):
    army_id = 'krieg_siege_sa'
    army_name = 'Death Korps of Krieg Siege Regiment (Siege War attacker Detachment)'


class KriegSiegeSD(KriegSiegebase, SiegeDefender):
    army_id = 'krieg_siege_sd'
    army_name = 'Death Korps of Krieg Siege Regiment (Siege War defender Detachment)'


class KriegAssaultSA(KriegAssaultBase, SiegeAttacker):
    army_id = 'krieg_assault_sa'
    army_name = 'Death Korps of Krieg Assault Brigade (Siege War attacker Detachment)'


class KriegAssaultSD(KriegAssaultBase, SiegeDefender):
    army_id = 'krieg_assault_sd'
    army_name = 'Death Korps of Krieg Assault Brigade (Siege War defender Detachment)'


class KriegDeathRiders(Wh40kBase):
    army_id = 'krieg_siege_riders'
    army_name = 'Krieg Death Rider Squadron Detachment'

    def is_siege(self):
        return True

    class RiderFast(FastSection):
        def __init__(self, parent):
            super(KriegDeathRiders.RiderFast, self).__init__(parent, 2, 6)
            self.riders = UnitType(self, DeathRiderPlatoon)
            UnitType(self, CyclopsSquad)
            UnitType(self, HellhoundSquadron)

        def check_rules(self):
            super(KriegDeathRiders.RiderFast, self).check_rules()
            if self.riders.count < 2:
                self.error("All compulsory Fast Attack choices must be filled with Death Rider Platoons")

    class RiderHq(HQSection):
        def __init__(self, parent):
            super(KriegDeathRiders.RiderHq, self).__init__(parent)
            self.command = UnitType(self, DeathRiderCommandPlatoon)
            UnitType(self, CommandSquad)
            UnitType(self, CommissarGeneral)
            UnitType(self, QuartermasterCadre)

        def check_rules(self):
            super(KriegDeathRiders.RiderHq, self).check_rules()
            if self.command.count != 1:
                self.error('Compulsory HQ choice must be Death Rider company command squad')

    def __init__(self):
        troops = Troops(self)
        troops.min, troops.max = (0, 2)
        elites = Elites(self)
        elites.max = 4
        heavy = Heavy(self)
        heavy.max = 4
        super(KriegDeathRiders, self).__init__(sections=[
            self.RiderHq(self), elites, troops, self.RiderFast(self), heavy, Lords(self)
        ])


class GorgonAssault(Wh40kBase):
    army_id = 'krieg_siege_gorgons'
    army_name = 'Gorgon Assault Squadron Detachment'

    def is_siege(self):
        return True

    class GorgonLords(LordsOfWarSection):
        def __init__(self, parent):
            super(GorgonAssault.GorgonLords, self).__init__(parent, 1, 3)
            UnitType(self, Gorgon)

    def __init__(self):
        troops = Troops(self)
        troops.min, troops.max = (1, 10)
        elites = Elites(self)
        elites.min = 1
        fast = Fast(self)
        fast.max = 2
        heavy = Heavy(self)
        heavy.max = 2
        super(GorgonAssault, self).__init__(sections=[
            HQ(self), elites, troops, fast, heavy, self.GorgonLords(self)
        ])


faction = 'Death Korps of Krieg'


class DeathKorps(Wh40k7ed):
    army_id = 'krieg'
    army_name = 'Death Korps of Krieg'
    imperial_armour = True

    @property
    def imperial_armour_on(self):
        return self.imperial_armour

    faction = faction

    def __init__(self):
        super(DeathKorps, self).__init__([
            KriegSiegeCAD, KriegDeathRiders,
            GorgonAssault, KriegAssaultCAD])


class DeathKorpsMissions(Wh40k7edMissions):
    army_id = 'krieg_mis'
    army_name = 'Death Korps of Krieg'
    imperial_armour = True

    @property
    def imperial_armour_on(self):
        return self.imperial_armour

    faction = faction

    def __init__(self):
        super(DeathKorpsMissions, self).__init__([
            KriegSiegeCAD, KriegDeathRiders,
            GorgonAssault, KriegAssaultCAD,
            KriegSiegePA, KriegSiegePD,
            KriegSiegeSA, KriegSiegeSD,
            KriegAssaultPA, KriegAssaultPD,
            KriegAssaultSA, KriegAssaultSD
        ])


detachments = [
    KriegSiegeCAD,
    KriegSiegeAD,
    KriegAssaultCAD,
    KriegAssaultAD,
    KriegDeathRiders,
    GorgonAssault,
    KriegSiegePA, KriegSiegePD,
    KriegSiegeSA, KriegSiegeSD,
    KriegAssaultPA, KriegAssaultPD,
    KriegAssaultSA, KriegAssaultSD
]
