__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.unit import Unit


class Magnus(Unit):
    type_name = 'Magnus The Red, Daemon Primarch of Tzeentch'
    type_id = 'magnus_v1'

    def __init__(self, parent):
        super(Magnus, self).__init__(parent, 'Magnus The Red', 650,
                                     [Gear('Veterans of the Long War'),
                                      Gear('The Blade of Magnus'),
                                      Gear('Crown of the Crimson King')],
                                     True, True)

    def count_charges(self):
        return 5

    def build_statistics(self):
        return self.note_charges(super(Magnus, self).build_statistics())
