__author__ = 'Denis Romanov'

from builder.core2 import *
from .armory import *
from builder.games.wh40k.imperial_armour.volume13.transport import ChaosDroppod, \
    IATransport, IATransportedUnit
from builder.games.wh40k.imperial_armour.volume13.options import ChaosSpaceMarinesBaseVehicle
from builder.games.wh40k.roster import Unit


class Rhino(ChaosSpaceMarinesBaseVehicle):
    def __init__(self, parent):
        super(Rhino, self).__init__(parent, name='Chaos Rhino', points=35,
                                    gear=[Gear('Combi-bolter'), Gear('Smoke launchers'), Gear('Searchlight')],
                                    tank=True, transport=True)
        Vehicle(self)


class RhinoTransport(IATransport):
    def __init__(self, parent, name='Transport', order=1001, **kwargs):
        super(RhinoTransport, self).__init__(parent, name=name, order=order, **kwargs)
        self.rhino = SubUnit(self, Rhino(parent=parent))
        self.models += [self.rhino]


class MarineTransport(ChaosDroppod, RhinoTransport):
    def __init__(self, parent):
        super(MarineTransport, self).__init__(parent, 'Transport')


class ChaosSpaceMarines(IconicUnit, IATransportedUnit):
    type_name = 'Chaos Space Marines'
    type_id = 'chaos_space_marines_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Space-Marines')

    model_points = 13

    class Marine(IconBearerGroup):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(ChaosSpaceMarines.Marine.Weapon1, self).__init__(parent=parent, name='Weapon')
                self.boltgun = self.variant('Boltgun', 0)
                self.closecombatweapon = self.variant('Close combat weapon', 0)
                self.flamer = self.variant('Flamer', 5)
                self.hb = self.variant('Heavy bolter', 10)
                self.meltagun = self.variant('Meltagun', 10)
                self.plasmagun = self.variant('Plasma gun', 15)
                self.autocannon = self.variant('Autocannon', 10)
                self.missilelauncher = self.variant('Missile launcher', 15)
                self.lascannon = self.variant('Lascannon', 20)
                self.heavy = [self.autocannon, self.missilelauncher, self.lascannon, self.hb]
                self.spec = [self.meltagun, self.plasmagun, self.flamer]

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(ChaosSpaceMarines.Marine.Weapon2, self).__init__(parent=parent, name='')
                self.boltpistol = self.variant('Bolt pistol', 0)
                self.plasmapistol = self.variant('Plasma pistol', 15)

        class Weapon3(OptionsList):
            def __init__(self, parent):
                super(ChaosSpaceMarines.Marine.Weapon3, self).__init__(parent=parent, name='', limit=None)
                self.closecombatweapon = self.variant('Close combat weapon', 2)

        def __init__(self, parent):
            super(ChaosSpaceMarines.Marine, self).__init__(
                parent, name='Chaos Space Marines', points=ChaosSpaceMarines.model_points, gear=Armour.power_armour_set,
                wrath=20, flame=15, despair=10, excess=30, vengeance=25
            )
            self.weapon1 = self.Weapon1(self)
            self.weapon2 = self.Weapon2(self)
            self.weapon3 = self.Weapon3(self)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.weapon1.cur in self.weapon1.heavy

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon1.cur in self.weapon1.spec

        @ListSubUnit.count_gear
        def count_pistol(self):
            return self.weapon2.cur != self.weapon2.boltpistol

        def activate_heavy(self, val):
            for o in self.weapon1.heavy:
                o.active = val

    class Champion(IconBearer):

        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Weapon3(Ranged, Boltgun):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(ChaosSpaceMarines.Champion.Options, self).__init__(parent, name='Options')
                self.variant('Melta bombs', 5)
                self.variant('Gift of mutation', 10)

        def __init__(self, parent):
            super(ChaosSpaceMarines.Champion, self).__init__(
                parent, points=75 - ChaosSpaceMarines.model_points * 4, name='Aspiring Champion',
                gear=Armour.power_armour_set,
                wrath=20, flame=15, despair=10, excess=30, vengeance=25
            )

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.wep3 = self.Weapon3(self, name='')
            self.Options(self)

        def check_rules(self):
            super(ChaosSpaceMarines.Champion, self).check_rules()
            self.wep1.set_unique_ranged(not self.wep2.has_unique_ranged() and not self.wep3.has_unique_ranged())
            self.wep2.set_unique_ranged(not self.wep1.has_unique_ranged() and not self.wep3.has_unique_ranged())
            self.wep3.set_unique_ranged(not self.wep1.has_unique_ranged() and not self.wep1.has_unique_ranged())

    def __init__(self, parent):
        super(ChaosSpaceMarines, self).__init__(parent)
        self.marks = Marks(self, khorne=2, tzeentch=2, nurgle=3, slaanesh=2, per_model=True)
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Marine, min_limit=4, max_limit=19)

        self.veteran = Veterans(self, points=1, per_model=True)
        self.transport = MarineTransport(self)

    def check_rules(self):
        super(ChaosSpaceMarines, self).check_rules()
        for o in self.models.units:
            o.activate_heavy(self.get_count() >= 10)
        heavy = sum(o.count_heavy() for o in self.models.units)
        spec = sum(o.count_spec() for o in self.models.units)
        pistol = sum(o.count_pistol() for o in self.models.units)

        if self.get_count() >= 10:
            if heavy > 1:
                self.error("Only one marine can exchange his bolter for heavy weapon. (taken: {})".format(heavy))
            if spec + pistol + heavy > 2:
                self.error("Only 2 marines can take special weapons (taken: {0}).".format(spec + pistol + heavy))
        else:
            if heavy > 0:
                self.error("Only if the squad numbers ten or more models one marine can exchange his bolter for "
                           "heavy weapon. (taken: {})".format(heavy))
            if spec + pistol > 1:
                self.error("Only 1 marine can take special weapons (taken: {0}).".format(spec + pistol))


class ChaosCultists(Unit):
    type_name = "Chaos Cultists"
    type_id = "chaos_cultists_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Cultists')

    model_gear = [Gear('Improvised armour'), Gear('Close combat weapon')]
    model_points = 4
    model_desc = UnitDescription('Chaos Cultist', points=model_points, options=model_gear)

    class Champion(OptionsList):
        def __init__(self, parent):
            super(ChaosCultists.Champion, self).__init__(parent=parent, name='Cultist Champion', limit=None)
            self.variant('Shotgun', 2)

        @property
        def description(self):
            return [
                UnitDescription(
                    'Cultist Champion',
                    points=50 - 9 * ChaosCultists.model_points + super(ChaosCultists.Champion, self).points,
                    options=ChaosCultists.model_gear + [Gear('Autopistol')] + super(ChaosCultists.Champion, self).description
                )
            ]

    class Count(Count):
        @property
        def description(self):
            return [ChaosCultists.model_desc.clone().add(Gear('Autopistol')).set_count(
                self.cur - sum(o.cur for o in [self.parent.gun, self.parent.stubb, self.parent.flame])
            )]

    def __init__(self, parent):
        super(ChaosCultists, self).__init__(parent, points=50 - 9 * self.model_points)
        self.champion = self.Champion(self)
        self.models = self.Count(self, 'Chaos Cultist', 9, 34, self.model_points)
        self.gun = Count(
            self, 'Autogun', 0, 9, 1,
            gear=ChaosCultists.model_desc.clone().add_points(1).add(Gear('Autogun'))
        )
        self.stubb = Count(
            self, 'Heavy stubber', 0, 1, 5,
            gear=ChaosCultists.model_desc.clone().add_points(5).add(Gear('Heavy stubber')),
        )
        self.flame = Count(
            self, 'Flamer', 0, 1, 5,
            gear=ChaosCultists.model_desc.clone().add_points(5).add(Gear('Flamer')),
        )
        self.marks = Marks(self, khorne=2, tzeentch=1, nurgle=2, slaanesh=1, per_model=True)

    def check_rules(self):
        super(ChaosCultists, self).check_rules()
        Count.norm_counts(0, self.get_count() / 10, [self.stubb, self.flame])
        self.gun.max = self.models.cur - sum(o.cur for o in [self.stubb, self.flame])

    def get_count(self):
        return self.models.cur + 1

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle


class Tzaangors(Unit):
    type_name = "Tzaangors"
    type_id = "tzaangors_v1"

    model_gear = [Gear('Close combat weapon', count=2)]
    model_points = 7
    model_desc = UnitDescription('Tzaangor', points=model_points, options=model_gear)

    class Leader(OptionsList):
        def __init__(self, parent):
            super(Tzaangors.Leader, self).__init__(parent, 'Upgrade')
            self.variant('Twistbray', 17, gear=[UnitDescription('Twistbray', 17, options=Tzaangors.model_gear)])

    class Count(Count):
        @property
        def description(self):
            return [Tzaangors.model_desc.clone().set_count(
                self.cur - self.parent.gun.cur)]

    def __init__(self, parent):
        super(Tzaangors, self).__init__(parent, gear=[Gear('Mark of Tzeentch')])
        self.champion = self.Leader(self)
        self.models = self.Count(self, 'Tzaangor', 10, 30, self.model_points)
        self.gun = Count(
            self, 'Autopistol and chainsword', 0, 9, 1,
            gear=UnitDescription('Tzaangor', 8, options=[
                Gear('Autopistol'), Gear('Chainsword')])
        )

    def check_rules(self):
        super(Tzaangors, self).check_rules()
        self.models.min, self.models.max = (10 - self.champion.any, 30 - self.champion.any)
        self.gun.max = self.models.cur

    def get_count(self):
        return self.models.cur + self.champion.any


class PlagueZombie(Unit):
    type_name = "Plague Zombies"
    type_id = "plague_zombies_v1"

    model_gear = [Gear('Improvised armour'), Gear('Close combat weapon')]
    model_points = 4
    champ_points = 50 - 9 * model_points

    def __init__(self, parent):
        super(PlagueZombie, self).__init__(
            parent, points=self.champ_points,
            gear=[UnitDescription('Plague Zombie Champion', points=self.champ_points, options=self.model_gear)]
        )
        self.models = Count(
            self, 'Plague Zombie', 9, 34, self.model_points,
            gear=UnitDescription('Plague Zombie', points=self.model_points, options=self.model_gear)
        )

    def get_count(self):
        return self.models.cur + 1
