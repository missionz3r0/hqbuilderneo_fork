# -*- coding: utf-8 -*-
__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'
summary = ['Codex Chaos Space Marines 6th Edition', 'Crimson Slaughter supplement',
           'Black Legion supplement', 'Helbrutes dataslate', "Traitor's Hate",
           'Wrath of Magnus']

from builder.games.wh40k.section import TroopsSection, FlyerSection
from builder.games.wh40k.fortifications import Fort
from builder.games.wh40k.imperial_armour.apocalypse import chaos as ia_apoc_chaos
from builder.games.wh40k.escalation.chaos import LordsOfWar as EscLordsOfWar
from builder.games.wh40k.dataslates.cypher import *
from builder.games.wh40k.dataslates.belakor import *
from builder.games.wh40k.imperial_armour.volume13 import *
from builder.games.wh40k.imperial_armour.volume5_7 import ChaosMarinesCharacters
from builder.games.wh40k.imperial_armour.dataslates import ChaosKnights
from builder.games.wh40k.renegades import PurgeSection

from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *
from .lords import Magnus
from .formations import *

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, PrimaryDetachment, Wh40kImperial, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.abaddon = UnitType(self, Abaddon)
        UnitType(self, Huron)
        self.kharn = UnitType(self, Kharn)
        self.ahriman = UnitType(self, Ahriman)
        self.typhus = UnitType(self, Typhus)
        self.lucius = UnitType(self, Lucius)
        UnitType(self, Fabius)
        self.lord = UnitType(self, Lord)
        self.sorc = UnitType(self, Sorcerer)
        self.exsorc = UnitType(self, ExaltedSorc)
        dp = UnitType(self, DaemonPrince)
        self.smith = UnitType(self, Warpsmith)
        da = UnitType(self, Apostle)
        self.regular = [self.exsorc, self.lord, self.sorc, dp,
                        self.smith, da]


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.chosen = UnitType(self, Chosen)
        self.possessed = UnitType(self, Possessed)
        self.terminators = UnitType(self, Terminators)
        UnitType(self, Helbrute)
        self.mut = UnitType(self, Mutilators)
        self.berz = UnitType(self, Berzerks)
        self.thousand = UnitType(self, RubricMarines)
        self.scarabs = UnitType(self, SOTerminators)
        UnitType(self, ThousandSons, visible=False)
        self.plague = UnitType(self, PlagueMarines)
        self.noise = UnitType(self, NoiseMarines)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, ChaosSpaceMarines)
        UnitType(self, ChaosCultists)
        self.tzaangors = UnitType(self, Tzaangors)
        self.zombie = UnitType(self, PlagueZombie)
        self.chosen = UnitType(self, Chosen)
        self.possessed = UnitType(self, Possessed)
        self.berz = UnitType(self, Berzerks)
        self.thousand = UnitType(self, RubricMarines)
        UnitType(self, ThousandSons, visible=False)
        self.plague = UnitType(self, PlagueMarines)
        self.noise = UnitType(self, NoiseMarines)
        self.terminators = UnitType(self, Terminators)
        self.mut = UnitType(self, Mutilators)
        self.obl = UnitType(self, Obliterators)
        self.raptors = UnitType(self, Raptors)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, Bikers)
        UnitType(self, Spawn)
        self.raptors = UnitType(self, Raptors)
        UnitType(self, WarpTalons)
        drake = UnitType(self, Heldrake)
        drake.visible = False
        UnitType(self, Heldrakes)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.hav = UnitType(self, Havoks)
        self.obl = UnitType(self, Obliterators)
        UnitType(self, Defiler)
        UnitType(self, Forgefiend)
        UnitType(self, Maulerfiend)
        UnitType(self, LandRaider)
        UnitType(self, Vindicator, visible=False)
        UnitType(self, ChaosVindicators)
        UnitType(self, Predator, visible=False)
        UnitType(self, ChaosPredators)


class BaseLordsOfWar(EscLordsOfWar):
    def __init__(self, parent):
        super(BaseLordsOfWar, self).__init__(parent)
        self.magnus = UnitType(self, Magnus)


class HQ(ChaosMarinesCharacters, BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.cypher = UnitType(self, Cypher, slot=0)
        UnitType(self, Belakor)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class LordsOfWar(ia_apoc_chaos.ChaosLordsOfWar, BaseLordsOfWar):
    pass


class HeavySupport(ia_apoc_chaos.ChaosHeavySupport, BaseHeavySupport):
    pass


class FastAttack(ia_apoc_chaos.ChaosFastAttack, BaseFastAttack):
    pass


class Elites(ia_apoc_chaos.ChaosElites, BaseElites):
    pass


class ChaosMarinesElites(MarinesElites, BaseElites):
    pass


class ChaosMarinesHeavy(MarinesHeavy, BaseHeavySupport):
    def __init__(self, parent):
        super(ChaosMarinesHeavy, self).__init__(parent)
        UnitType(self, DeimosVindicator)


class ChaosMarinesFast(MarinesFast, BaseFastAttack):
    pass


class ChaosMarinesLordsOfWar(ChaosKnights, MarinesLords, BaseLordsOfWar):
    pass


class PurgeHq(PurgeSection, ChaosMarinesCharacters, BaseHQ):
    def __init__(self, parent):
        super(PurgeHq, self).__init__(parent)
        self.undevoted += self.regular[1:]
        self.abaddon.visible = False
        self.ahriman.visible = False
        self.lucius.visible = False
        self.kharn.visible = False
        self.exsorc.visible = False
        self.zhufor.visible = False
        self.bodyguards.visible = False


class PurgeElites(PurgeSection, ElitesSection):
    def __init__(self, parent):
        super(PurgeElites, self).__init__(parent)
        UnitType(self, Helbrute)
        self.plague = UnitType(self, PlagueMarines)
        from builder.games.wh40k.imperial_armour.volume13.elites import\
            Decimator, ChaosContemptor, SpinedBeast
        self.undevoted += [
            UnitType(self, Chosen),
            UnitType(self, Possessed),
            UnitType(self, Terminators),
            UnitType(self, Mutilators),
            UnitType(self, Decimator),
            UnitType(self, ChaosContemptor),
            UnitType(self, SpinedBeast)
        ]


class PurgeTroops(PurgeSection, TroopsSection):
    def __init__(self, parent):
        super(PurgeTroops, self).__init__(parent)
        self.zombies = UnitType(self, PlagueZombie)
        self.plague = UnitType(self, PlagueMarines)
        self.undevoted += [
            UnitType(self, ChaosSpaceMarines),
            UnitType(self, ChaosCultists),
        ]


class PurgeFast(PurgeSection, FastSection):
    def __init__(self, parent):
        super(PurgeFast, self).__init__(parent)
        from builder.games.wh40k.imperial_armour.volume13.fast import\
            StormEagleGunship, HellBlade, HellTalon, Dreadclaw, BlightDrones
        UnitType(self, Heldrake)
        UnitType(self, StormEagleGunship)
        UnitType(self, HellBlade)
        UnitType(self, HellTalon)
        UnitType(self, Dreadclaw)
        UnitType(self, BlightDrones)
        self.undevoted += [
            UnitType(self, Bikers),
            UnitType(self, Spawn),
            UnitType(self, Raptors),
            UnitType(self, WarpTalons)
        ]


class PurgeHeavy(PurgeSection, BaseHeavySupport):
    def __init__(self, parent):
        super(PurgeHeavy, self).__init__(parent)
        from builder.games.wh40k.imperial_armour.volume13.lords import\
            Spartan
        from builder.games.wh40k.imperial_armour.volume13.heavy import\
            ChaosProteus, FireRaptor, ChaosRapiers, PlagueHulk
        UnitType(self, ChaosProteus)
        UnitType(self, Spartan)
        UnitType(self, FireRaptor)
        UnitType(self, ChaosRapiers)
        UnitType(self, PlagueHulk)
        self.undevoted += [
            self.hav, self.obl
        ]


class PurgeLords(PurgeSection, LordsOfWarSection):
    def __init__(self, parent):
        super(PurgeLords, self).__init__(parent)
        from builder.games.wh40k.imperial_armour.volume13.lords import\
            Fellblade, Typhon, ThunderhawkGunship, ChaosReaverTitan,\
            ChaosWarhoundTitan
        from builder.games.wh40k.imperial_armour.dataslates.chaos_knights import\
            ChaosErrant, ChaosPaladin
        UnitType(self, Fellblade)
        UnitType(self, Typhon)
        UnitType(self, ThunderhawkGunship)
        self.undevoted += [
            UnitType(self, ChaosErrant),
            UnitType(self, ChaosPaladin),
            UnitType(self, ChaosReaverTitan),
            UnitType(self, ChaosWarhoundTitan)
        ]


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Heldrakes])


class CsmV2Base(Wh40kBase):
    army_id = 'csm_v2_base'
    army_name = 'Chaos Space Marines'
    ia_enabled = True

    class SupplementOptions(OneOf):
        def __init__(self, parent):
            super(CsmV2Base.SupplementOptions, self).__init__(parent=parent, name='Codex')
            self.base = self.variant(name=CsmV2.army_name)
            self.black = self.variant(name='Black Legion')
            self.crimson = self.variant(name='Crimson Slaughter')

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(CsmV2Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

        self.codex = self.SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_black(self):
        return self.codex.cur == self.codex.black

    @property
    def is_crimson(self):
        return self.codex.cur == self.codex.crimson

    @property
    def has_abaddon(self):
        return self.hq.abaddon.count > 0

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return False

    @property
    def is_thousand(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    def check_rules(self):
        super(CsmV2Base, self).check_rules()

        if self.is_black:
            bringers = sum(u.is_bringers for u in self.elites.terminators.units)
            if bringers > 1:
                self.error('Only one Chaos Terminators unit can be ungraded to Bringers of Despair. '
                           '(taken: {})'.format(bringers))

        self.move_units((self.has_abaddon and type(self.parent.parent) is PrimaryDetachment) or self.is_black,
                        self.elites.chosen, self.troops.chosen)
        self.move_units(self.is_crimson, self.elites.possessed, self.troops.possessed)

        self.troops.zombie.active = self.hq.typhus.count > 0
        if self.hq.typhus.count == 0 and self.troops.zombie.count > 0:
            self.error("You can't have Plague Zombie without Typhus")

        if type(self.parent.parent) is PrimaryDetachment:
            self.move_units(self.hq.kharn.count + sum(u.marks.khorne for u in self.hq.lord.units) > 0,
                            self.elites.berz, self.troops.berz)
            self.move_units(self.hq.typhus.count + sum(u.marks.nurgle for u in self.hq.lord.units) > 0,
                            self.elites.plague, self.troops.plague)
            self.move_units(self.hq.lucius.count + sum(u.marks.slaanesh for u in self.hq.lord.units) > 0,
                            self.elites.noise, self.troops.noise)
            self.move_units(self.hq.ahriman.count + sum(u.marks.tzeentch for u in self.hq.sorc.units) > 0,
                            self.elites.thousand, self.troops.thousand)
        else:
            self.troops.berz.visible = self.troops.thousand.visible = self.troops.plague.visible = \
                self.troops.noise.visible = False
            self.troops.chosen.visible = self.is_black
            self.troops.possessed.visible = self.is_crimson


class CsmV2CAD(CsmV2Base, CombinedArmsDetachment):
    army_id = 'csm_v2_cad'
    army_name = 'Chaos Space Marines (Combined arms detachment)'


class CsmV2AD(CsmV2Base, AlliedDetachment):
    army_id = 'csm_v2_ad'
    army_name = 'Chaos Space Marines (Allied detachment)'


class BlackCrusade(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'Black Crusade Detachment'
    army_id = 'csm_v2_5_black_crusade'

    class CrusadeSubFormation(CommonChaosFormation):
        death_to_false_emperor = True

        def __init__(self, *args, **kwargs):
            super(BlackCrusade.CrusadeSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        @property
        def is_thousand(self):
            return self.parent.roster.is_thousand

        @property
        def is_base(self):
            return self.parent.roster.is_base

        @property
        def is_black(self):
            return self.parent.roster.is_black

        @property
        def is_crimson(self):
            return self.parent.roster.is_crimson

        @property
        def is_alpha(self):
            return self.parent.roster.is_alpha

        @property
        def is_iron(self):
            return self.parent.roster.is_iron

        @property
        def is_night(self):
            return self.parent.roster.is_night

        @property
        def is_word(self):
            return self.parent.roster.is_word

        @property
        def is_eater(self):
            return self.parent.roster.is_eater

        @property
        def is_guard(self):
            return self.parent.roster.is_guard

        @property
        def is_child(self):
            return self.parent.roster.is_child

    @staticmethod
    def make_crusade(formation):
        class BaseSubformation(BlackCrusade.CrusadeSubFormation, formation):
            pass
        return BaseSubformation

    class Lords(CrusadeSubFormation):
        army_name = 'Lords of Black Crusade'
        army_id = 'csm_v2_5_black_crusade_lords'

        def __init__(self):
            super(BlackCrusade.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, Sorcerer),
                UnitType(self, DaemonPrince),
                UnitType(self, Abaddon),
                UnitType(self, Huron),
                UnitType(self, Kharn),
                UnitType(self, Typhus),
                UnitType(self, Lucius),
                UnitType(self, Fabius),
                UnitType(self, Ahriman)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(BlackCrusade.Command, self).__init__(
                parent, 'command', 'Command',
                [BlackCrusade.Lords], 0, 5)

    class Core(Detachment):
        def __init__(self, parent):
            super(BlackCrusade.Core, self).__init__(
                parent, 'core', 'Core',
                [BlackCrusade.make_crusade(f) for f in[
                    ChaosWarband, Maelstorm, LostDamned
                ]], 1, None)

    class Veterans(CrusadeSubFormation):
        army_name = 'Veterans of the Legions'
        army_id = 'csm_v2_5_black_crusade_veterans'

        def __init__(self):
            super(BlackCrusade.Veterans, self).__init__()
            vets = [
                UnitType(self, Berzerks),
                UnitType(self, ThousandSons, visible=False),
                UnitType(self, RubricMarines),
                UnitType(self, PlagueMarines),
                UnitType(self, NoiseMarines)
            ]
            self.add_type_restriction(vets, 1, 4)

    class Spawn(CrusadeSubFormation):
        army_name = 'Spawn'
        army_id = 'csm_v2_5_black_crusade_spawn'

        def __init__(self):
            super(BlackCrusade.Spawn, self).__init__()
            UnitType(self, Spawn, min_limit=1, max_limit=3)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(BlackCrusade.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [BlackCrusade.make_crusade(f) for f in[
                    HellforgedWarpack, TerrorPack, DestructionCult,
                    FistOfGods, RaptorTalon, TerminatorAnnihilationForce,
                    Favoured
                ]] + [Trinity, BlackCrusade.Veterans, BlackCrusade.Spawn], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)
        self.codex = SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_black(self):
        return self.codex.cur == self.codex.black

    @property
    def is_crimson(self):
        return self.codex.cur == self.codex.crimson

    @property
    def is_thousand(self):
        return self.codex.cur == self.codex.thousand

    @property
    def is_alpha(self):
        return self.codex.cur == self.codex.alpha

    @property
    def is_iron(self):
        return self.codex.cur == self.codex.iron

    @property
    def is_night(self):
        return self.codex.cur == self.codex.night

    @property
    def is_word(self):
        return self.codex.cur == self.codex.word

    @property
    def is_eater(self):
        return self.codex.cur == self.codex.eater

    @property
    def is_guard(self):
        return self.codex.cur == self.codex.guard

    @property
    def is_child(self):
        return self.codex.cur == self.codex.child

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class BlackCrusadeSpeartip(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'Black Legion Speartip'
    army_id = 'csm_v2_5_black_speartip'

    @staticmethod
    def make_speartip(formation):
        class BaseSubformation(BlackFormation, formation):
            pass
        return BaseSubformation

    class Lords(BlackFormation):
        army_name = 'Lords of the Legion'
        army_id = 'csm_v2_5_black_speartip_lord'

        def __init__(self):
            super(BlackCrusadeSpeartip.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, Sorcerer),
                UnitType(self, DaemonPrince),
                UnitType(self, Apostle),
                UnitType(self, Abaddon)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(BlackCrusadeSpeartip.Command, self).__init__(
                parent, 'command', 'Command',
                [Bringers, Cabal, BlackChosen, BlackCrusadeSpeartip.Lords], 0, 5)

    class Core(Detachment):
        def __init__(self, parent):
            super(BlackCrusadeSpeartip.Core, self).__init__(
                parent, 'core', 'Core',
                [Warband, BlackHounds], 1, None)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(BlackCrusadeSpeartip.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [BlackCrusadeSpeartip.make_speartip(f) for f in[
                    LostDamned, HellforgedWarpack,
                    TerrorPack, DestructionCult,
                    FistOfGods, RaptorTalon,
                    TerminatorAnnihilationForce,
                    BlackCrusade.Spawn
                ]] + [EnginePack], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class InsurgencyForce(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'Alpha Legion Insurgency Force'
    army_id = 'csm_v2_5_insurgency'

    class SubFormation(CommonChaosFormation):

        def __init__(self, *args, **kwargs):
            super(InsurgencyForce.SubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        @property
        def is_thousand(self):
            return False

        @property
        def is_base(self):
            return False

        @property
        def is_black(self):
            return False

        @property
        def is_crimson(self):
            return False

        @property
        def is_alpha(self):
            return True

        @property
        def is_iron(self):
            return False

        @property
        def is_night(self):
            return False

        @property
        def is_word(self):
            return False

        @property
        def is_eater(self):
            return False

        @property
        def is_guard(self):
            return False

        @property
        def is_child(self):
            return False

    @staticmethod
    def make_insurgency(formation):
        class BaseSubformation(InsurgencyForce.SubFormation, formation):
            pass
        return BaseSubformation

    class Lords(SubFormation):
        army_name = 'Lord of the Legion'
        army_id = 'csm_v2_5_insurgency_lord'

        def __init__(self):
            super(InsurgencyForce.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, Sorcerer),
                UnitType(self, DaemonPrince),
                UnitType(self, Apostle)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(InsurgencyForce.Command, self).__init__(
                parent, 'command', 'Command',
                [InsurgencyForce.Lords], 0, 4)

    class Core(Detachment):
        def __init__(self, parent):
            super(InsurgencyForce.Core, self).__init__(
                parent, 'core', 'Core',
                [InsurgencyForce.make_insurgency(f) for f in[
                    ChaosWarband
                ]], 1, None)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(InsurgencyForce.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [InsurgencyForce.make_insurgency(f) for f in [
                    LostDamned, HellforgedWarpack, DestructionCult,
                    FistOfGods, RaptorTalon, TerrorPack, TerminatorAnnihilationForce,
                    Favoured, BlackCrusade.Spawn
                ]], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class GrandCompany(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'Iron Warriors Grand Company'
    army_id = 'csm_v2_5_grand_company'

    class SubFormation(CommonChaosFormation):

        def __init__(self, *args, **kwargs):
            super(GrandCompany.SubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        @property
        def is_thousand(self):
            return False

        @property
        def is_base(self):
            return False

        @property
        def is_black(self):
            return False

        @property
        def is_crimson(self):
            return False

        @property
        def is_alpha(self):
            return False

        @property
        def is_iron(self):
            return True

        @property
        def is_night(self):
            return False

        @property
        def is_word(self):
            return False

        @property
        def is_eater(self):
            return False

        @property
        def is_guard(self):
            return False

        @property
        def is_child(self):
            return False

    @staticmethod
    def make_gc(formation):
        class BaseSubformation(GrandCompany.SubFormation, formation):
            pass
        return BaseSubformation

    class Lords(SubFormation):
        army_name = 'Lord of the Legion'
        army_id = 'csm_v2_5_grand_company_lord'

        def __init__(self):
            super(GrandCompany.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, Sorcerer),
                UnitType(self, DaemonPrince),
                UnitType(self, Apostle)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(GrandCompany.Command, self).__init__(
                parent, 'command', 'Command',
                [GrandCompany.Lords], 0, 4)

    class Core(Detachment):
        def __init__(self, parent):
            super(GrandCompany.Core, self).__init__(
                parent, 'core', 'Core',
                [GrandCompany.make_gc(f) for f in[
                    ChaosWarband
                ]], 1, None)

    class Strongholds(Roster):
        army_name = 'Strongholds of Chaos'
        army_id = 'csm_v2_5_grand_company_strongholds'

        def __init__(self):
            fort = Fort(self)
            fort.min = 1
            fort.max = 3
            super(GrandCompany.Strongholds, self).__init__([fort])

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(GrandCompany.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [GrandCompany.Strongholds] + [GrandCompany.make_gc(f) for f in [
                    LostDamned, HellforgedWarpack, DestructionCult,
                    FistOfGods, RaptorTalon, TerminatorAnnihilationForce,
                    Favoured, BlackCrusade.Spawn
                ]], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class MurderTalon(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'Night Lords Murder Talon'
    army_id = 'csm_v2_5_murder_talon'

    class SubFormation(CommonChaosFormation):

        def __init__(self, *args, **kwargs):
            super(MurderTalon.SubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        @property
        def is_thousand(self):
            return False

        @property
        def is_base(self):
            return False

        @property
        def is_black(self):
            return False

        @property
        def is_crimson(self):
            return False

        @property
        def is_alpha(self):
            return False

        @property
        def is_iron(self):
            return False

        @property
        def is_night(self):
            return True

        @property
        def is_word(self):
            return False

        @property
        def is_eater(self):
            return False

        @property
        def is_guard(self):
            return False

        @property
        def is_child(self):
            return False

    @staticmethod
    def make_mt(formation):
        class BaseSubformation(MurderTalon.SubFormation, formation):
            pass
        return BaseSubformation

    class Lords(SubFormation):
        army_name = 'Lord of the Legion'
        army_id = 'csm_v2_5_murder_talon_lord'

        def __init__(self):
            super(MurderTalon.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, Sorcerer),
                UnitType(self, DaemonPrince),
                UnitType(self, Apostle)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(MurderTalon.Command, self).__init__(
                parent, 'command', 'Command',
                [MurderTalon.Lords], 0, 4)

    class Core(Detachment):
        def __init__(self, parent):
            super(MurderTalon.Core, self).__init__(
                parent, 'core', 'Core',
                [MurderTalon.make_mt(f) for f in[
                    ChaosWarband, RaptorTalon
                ]], 1, None)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(MurderTalon.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [MurderTalon.make_mt(f) for f in [
                    LostDamned, HellforgedWarpack, TerrorPack, DestructionCult,
                    FistOfGods, TerminatorAnnihilationForce,
                    Favoured, BlackCrusade.Spawn
                ]], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class GrandHost(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'Word Bearers Grand Host'
    army_id = 'csm_v2_5_grand_host'

    class SubFormation(CommonChaosFormation):

        def __init__(self, *args, **kwargs):
            super(GrandHost.SubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        @property
        def is_thousand(self):
            return False

        @property
        def is_base(self):
            return False

        @property
        def is_black(self):
            return False

        @property
        def is_crimson(self):
            return False

        @property
        def is_alpha(self):
            return False

        @property
        def is_iron(self):
            return False

        @property
        def is_night(self):
            return False

        @property
        def is_word(self):
            return True

        @property
        def is_eater(self):
            return False

        @property
        def is_guard(self):
            return False

        @property
        def is_child(self):
            return False

    @staticmethod
    def make_gh(formation):
        class BaseSubformation(GrandHost.SubFormation, formation):
            pass
        return BaseSubformation

    class Lords(SubFormation):
        army_name = 'Lord of the Legion'
        army_id = 'csm_v2_5_grand_host_lord'

        def __init__(self):
            super(GrandHost.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, Sorcerer),
                UnitType(self, DaemonPrince),
                UnitType(self, Apostle)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(GrandHost.Command, self).__init__(
                parent, 'command', 'Command',
                [GrandHost.Lords], 0, 4)

    class Core(Detachment):
        def __init__(self, parent):
            super(GrandHost.Core, self).__init__(
                parent, 'core', 'Core',
                [GrandHost.make_gh(f) for f in[
                    ChaosWarband, LostDamned
                ]], 1, None)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(GrandHost.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [GrandHost.make_gh(f) for f in [
                    HellforgedWarpack, TerrorPack, DestructionCult,
                    FistOfGods, RaptorTalon, TerminatorAnnihilationForce,
                    Favoured, BlackCrusade.Spawn
                ]], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class WEButcherhorde(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'World Eaters Butcherhorde'
    army_id = 'csm_v2_5_webutcherhorde'

    class SubFormation(CommonChaosFormation):

        def __init__(self, *args, **kwargs):
            super(WEButcherhorde.SubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        @property
        def is_thousand(self):
            return False

        @property
        def is_base(self):
            return False

        @property
        def is_black(self):
            return False

        @property
        def is_crimson(self):
            return False

        @property
        def is_alpha(self):
            return False

        @property
        def is_iron(self):
            return False

        @property
        def is_night(self):
            return False

        @property
        def is_word(self):
            return False

        @property
        def is_eater(self):
            return True

        @property
        def is_guard(self):
            return False

        @property
        def is_child(self):
            return False

    @staticmethod
    def make_horde(formation):
        class BaseSubformation(WEButcherhorde.SubFormation, formation):
            pass
        return BaseSubformation

    class Lords(SubFormation):
        army_name = 'Lord of the Legion'
        army_id = 'csm_v2_5_webutcherhorde_lord'

        def __init__(self):
            super(WEButcherhorde.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, DaemonPrince),
                UnitType(self, Apostle)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(WEButcherhorde.Command, self).__init__(
                parent, 'command', 'Command',
                [WEButcherhorde.Lords], 0, 4)

    class Core(Detachment):
        def __init__(self, parent):
            super(WEButcherhorde.Core, self).__init__(
                parent, 'core', 'Core',
                [WEButcherhorde.make_horde(f) for f in[
                    ChaosWarband, Maelstorm
                ]], 1, None)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(WEButcherhorde.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [WEButcherhorde.make_horde(f) for f in [
                    LostDamned, HellforgedWarpack, TerrorPack, DestructionCult,
                    FistOfGods, RaptorTalon, TerminatorAnnihilationForce,
                    Favoured, BlackCrusade.Spawn
                ]] + [Trinity], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class Vectorium(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'Death Guard Vectorium'
    army_id = 'csm_v2_5_vectorium'

    @staticmethod
    def make_vector(formation):
        class BaseSubformation(GuardFormation, formation):
            pass
        return BaseSubformation

    class Lords(GuardFormation):
        army_name = 'Lord of the Legion'
        army_id = 'csm_v2_5_vectorium_lord'

        def __init__(self):
            super(Vectorium.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, Sorcerer),
                UnitType(self, DaemonPrince),
                UnitType(self, Apostle)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(Vectorium.Command, self).__init__(
                parent, 'command', 'Command',
                [Vectorium.Lords], 0, 4)

    class Core(Detachment):
        def __init__(self, parent):
            super(Vectorium.Core, self).__init__(
                parent, 'core', 'Core',
                [Colony] + [Vectorium.make_vector(ChaosWarband)], 1, None)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Vectorium.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [Vectorium.make_vector(f) for f in [
                    LostDamned, HellforgedWarpack, TerrorPack, DestructionCult,
                    FistOfGods, RaptorTalon, TerminatorAnnihilationForce,
                    Favoured, BlackCrusade.Spawn
                ]], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class Rapture(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = "Emperor's Children Rapture Batallion"
    army_id = 'csm_v2_5_rapture'

    @staticmethod
    def make_rapture(formation):
        class BaseSubformation(ChildFormation, formation):
            pass
        return BaseSubformation

    class Lords(ChildFormation):
        army_name = 'Lord of the Legion'
        army_id = 'csm_v2_5_rapture_lord'

        def __init__(self):
            super(Rapture.Lords, self).__init__()
            lords = [
                UnitType(self, Lord),
                UnitType(self, Sorcerer),
                UnitType(self, DaemonPrince),
                UnitType(self, Apostle)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(Rapture.Command, self).__init__(
                parent, 'command', 'Command',
                [Rapture.Lords], 0, 4)

    class Core(Detachment):
        def __init__(self, parent):
            super(Rapture.Core, self).__init__(
                parent, 'core', 'Core',
                [Kakophoni, Rapture.make_rapture(ChaosWarband)], 1, None)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Rapture.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [Rapture.make_rapture(f) for f in [
                    LostDamned, HellforgedWarpack, TerrorPack, DestructionCult,
                    FistOfGods, RaptorTalon, TerminatorAnnihilationForce,
                    Favoured, BlackCrusade.Spawn
                ]], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class GrandCoven(Wh40k7ed, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'Thousand Sond Grand Coven'
    army_id = 'csm_v2_5_grand_coven'

    class Lords(ThousandFormation):
        army_name = 'Lords of the Legion'
        army_id = 'csm_v2_5_grand_coven_lords'

        def __init__(self):
            super(GrandCoven.Lords, self).__init__()
            lords = [
                UnitType(self, Magnus),
                UnitType(self, Ahriman),
                UnitType(self, TzeentchPrince),
                UnitType(self, ExaltedSorc)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(GrandCoven.Command, self).__init__(
                parent, 'command', 'Command',
                [GrandCoven.Lords,
                 Rehati, Exiles], 0, 4)

    class Core(Detachment):
        def __init__(self, parent):
            super(GrandCoven.Core, self).__init__(
                parent, 'core', 'Core',
                [WarCabal, SekhmetConclave], 1, None)

    class Engines(ChaosSpaceMarinesLegaciesFormation):
        army_name = 'Daemon Engines'
        army_id = 'csm_v2_5_grand_coven_engines'

        def __init__(self):
            super(GrandCoven.Engines, self).__init__()
            eng = [
                UnitType(self, Defiler),
                UnitType(self, Forgefiend),
                UnitType(self, Helbrute),
                UnitType(self, Heldrake),
                UnitType(self, Maulerfiend)
            ]
            self.add_type_restriction(eng, 1, 1)

    class Armoury(ChaosSpaceMarinesLegaciesFormation):
        army_name = 'Legion Armoury'
        army_id = 'csm_v2_5_grand_coven_armoury'

        def __init__(self):
            super(GrandCoven.Armoury, self).__init__()
            eng = [
                UnitType(self, LandRaider),
                UnitType(self, Predator),
                UnitType(self, Vindicator)
            ]
            self.add_type_restriction(eng, 1, 1)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(GrandCoven.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary',
                [WarCoven, Warherd, GrandCoven.Engines,
                 GrandCoven.Armoury], 1, None)

    def __init__(self):
        command = self.Command(self)
        core = self.Core(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class CsmV2_5Base(Wh40kBase, ChaosSpaceMarinesLegaciesRoster):
    army_id = 'csm_v2_5_base'
    army_name = 'Chaos Space Marines'

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = ChaosMarinesElites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = ChaosMarinesFast(parent=self)
        self.heavy = ChaosMarinesHeavy(parent=self)
        self.lords = ChaosMarinesLordsOfWar(parent=self)
        super(CsmV2_5Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=self.lords
        )

        self.codex = SupplementOptions(self)

    @property
    def is_alpha(self):
        return self.codex.cur == self.codex.alpha

    @property
    def is_iron(self):
        return self.codex.cur == self.codex.iron

    @property
    def is_guard(self):
        return self.codex.cur == self.codex.guard

    @property
    def is_child(self):
        return self.codex.cur == self.codex.child

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_black(self):
        return self.codex.cur == self.codex.black

    @property
    def is_crimson(self):
        return self.codex.cur == self.codex.crimson

    @property
    def is_thousand(self):
        return self.codex.cur == self.codex.thousand

    @property
    def is_night(self):
        return self.codex.cur == self.codex.night

    @property
    def is_word(self):
        return self.codex.cur == self.codex.word

    @property
    def is_eater(self):
        return self.codex.cur == self.codex.eater

    @property
    def has_abaddon(self):
        return self.hq.abaddon.count > 0

    def check_crimson(self):
        if self.is_crimson:
            for ut in self.hq.types:
                if hasattr(ut.unit_class, 'veterans') and ut.unit_class.veterans:
                    ut.active = False
                    if ut.count > 0:
                        self.error('Cannot take Veterans of the Long War in Crimson Slaughter detachment')
            ut_flags = [self.troops.tzaangors, self.elites.scarabs]
            if self.lords:
                ut_flags.append(self.lords.magnus)
            for ut in ut_flags:
                ut.active = False
                if ut.count > 0:
                    self.error('Cannot take Veterans of the Long War in Crimson Slaughter detachment')

    def check_thousand(self):
        if self.is_thousand:
            for ut in self.hq.types:
                if not (ut in self.hq.regular + [self.hq.ahriman]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("Only unique characters in Thousand Sons detachment may be Ahriman and Magnus")
            for ut in [self.elites.berz, self.elites.noise,
                       self.elites.plague]:
                ut.active = not self.is_thousand
                if ut.count > 0:
                    self.error("Only Mark Of Tzeentch may be taken in Thousand Sons detachment")

    def check_black(self):
        if self.is_black:
            for ut in self.hq.types:
                if not (ut in self.hq.regular + [self.hq.abaddon]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("Only unique character in Black Legion detachment may be Abaddon")
            if self.lords:
                self.lords.magnus.active = False
                if self.lords.magnus.count > 0:
                    self.error("Only unique character in Black Legion detachment may be Abaddon")

    def check_alpha(self):
        if self.is_alpha:
            for ut in self.hq.types:
                if not (ut in self.hq.regular[1:]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("Alpha Legion Detachment cannot include unique characters")
            for ut in [self.elites.scarabs, self.elites.thousand, self.troops.tzaangors,
                       self.elites.berz, self.elites.noise, self.elites.plague, self.hq.exsorc]\
                       + ([self.lords.magnus] if self.lords else []):
                ut.active = False
                if ut.count > 0:
                    self.error("Alpha Legion Detachment cannot include units with Mark of Chaos")

    def check_iron(self):
        if self.is_iron:
            for ut in self.hq.types:
                if not (ut in self.hq.regular[1:]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("Iron Warriors Detachment cannot include unique characters")
            for ut in [self.elites.scarabs, self.elites.thousand, self.troops.tzaangors,
                       self.elites.berz, self.elites.noise, self.elites.plague, self.hq.exsorc]\
                       + ([self.lords.magnus] if self.lords else []):
                ut.active = False
                if ut.count > 0:
                    self.error("Iron Warriors Detachment cannot include units with Mark of Chaos")

    def check_night(self):
        if self.is_night:
            for ut in self.hq.types:
                if not (ut in self.hq.regular[1:]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("Night Lords Detachment cannot include unique characters")
            for ut in [self.elites.scarabs, self.elites.thousand, self.troops.tzaangors,
                       self.elites.berz, self.elites.noise, self.elites.plague, self.hq.exsorc]\
                       + ([self.lords.magnus] if self.lords else []):
                ut.active = False
                if ut.count > 0:
                    self.error("Night Lords Detachment cannot include units with Mark of Chaos")

    def check_word(self):
        if self.is_word:
            for ut in self.hq.types:
                if not (ut in self.hq.regular[1:]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("Word Bearers Detachment cannot include unique characters")
            for ut in [self.elites.scarabs, self.elites.thousand, self.troops.tzaangors,
                       self.elites.berz, self.elites.noise, self.elites.plague, self.hq.exsorc]\
                       + ([self.lords.magnus] if self.lords else []):
                ut.active = False
                if ut.count > 0:
                    self.error("Word Bearers Detachment cannot include cult units")

    def check_eater(self):
        if self.is_eater:
            for ut in self.hq.types:
                if not (ut in self.hq.regular + [self.hq.kharn]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("World Eaters Detachment cannot include unique characters")
            for ut in [self.elites.scarabs, self.elites.thousand,
                       self.elites.noise, self.elites.plague, self.troops.tzaangors]\
                       + ([self.lords.magnus] if self.lords else []):
                ut.active = False
                if ut.count > 0:
                    self.error("Only Mark of Khorne may be taken in World Eaters detachment")
            for ut in [self.hq.sorc, self.hq.exsorc]:
                ut.active = False
                if ut.count > 0:
                    self.error("Psykers may not be included in Wolrd Eaters dertachment")

    def check_guard(self):
        if self.is_guard:
            for ut in self.hq.types:
                if not (ut in self.hq.regular[1:] + [self.hq.typhus]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("Death Guard Detachment cannot include unique characters except Typhus")
            for ut in [self.elites.scarabs, self.elites.thousand,
                       self.elites.berz, self.elites.noise, self.hq.exsorc, self.troops.tzaangors]\
                       + ([self.lords.magnus] if self.lords else []):
                ut.active = False
                if ut.count > 0:
                    self.error("Death Guard Detachment cannot include units with Mark of Chaos other then Mark of Nurgle")

    def check_children(self):
        if self.is_guard:
            for ut in self.hq.types:
                if not (ut in self.hq.regular[1:] + [self.hq.lucius]):
                    ut.active = False
                    if ut.count > 0:
                        self.error("Emperor's Children Detachment cannot include unique characters except Lucius")
            for ut in [self.elites.scarabs, self.elites.thousand,
                       self.elites.berz, self.elites.plague, self.hq.exsorc, self.troops.tzaangors]\
                       + ([self.lords.magnus] if self.lords else []):
                ut.active = False
                if ut.count > 0:
                    self.error("Emperor's Children Detachment cannot include units with Mark of Chaos other then Mark of Slaanesh")

    def perform_unit_move(self):
        troop_thousand_flag = self.is_thousand or \
                              (type(self.parent.parent) is PrimaryDetachment and
                               (self.hq.ahriman.count + sum(u.marks.tzeentch for u in self.hq.sorc.units) > 0))
        self.move_units(troop_thousand_flag, self.elites.thousand, self.troops.thousand)
        self.troops.thousand.visible = troop_thousand_flag
        self.move_units(self.is_black, self.elites.terminators, self.troops.terminators)
        self.troops.terminators.visible = self.is_black
        self.move_units(self.is_iron, self.elites.mut, self.troops.mut)
        self.troops.mut.visible = self.is_iron
        self.move_units(self.is_iron, self.heavy.obl, self.troops.obl)
        self.troops.obl.visible = self.is_iron
        self.move_units(self.is_night, self.fast.raptors, self.troops.raptors)
        self.troops.raptors.visible = self.is_night
        berz_flag = (type(self.parent.parent) is PrimaryDetachment and
                     (self.hq.kharn.count + sum(u.marks.khorne for u in self.hq.lord.units) > 0)) or\
                     self.is_eater
        self.move_units(berz_flag,
                        self.elites.berz, self.troops.berz)
        self.troops.berz.visible = berz_flag
        guard_flag = (type(self.parent.parent) is PrimaryDetachment and
                      (self.hq.typhus.count + sum(u.marks.nurgle for u in self.hq.lord.units) > 0)) or\
                      self.is_guard
        self.move_units(guard_flag,
                        self.elites.plague, self.troops.plague)
        self.troops.plague.visible = guard_flag
        child_flag = (type(self.parent.parent) is PrimaryDetachment and
                      (self.hq.lucius.count + sum(u.marks.slaanesh for u in self.hq.lord.units) > 0)) or\
                      self.is_child
        self.move_units(child_flag, self.elites.noise, self.troops.noise)
        self.troops.noise.visible = child_flag
        # chosen may be troops for different Legions
        chosen_flag = (self.has_abaddon and type(self.parent.parent) is PrimaryDetachment) or self.is_black or\
                      self.is_alpha
        self.move_units(chosen_flag,
                        self.elites.chosen, self.troops.chosen)
        self.troops.chosen.visible = chosen_flag
        possessed_flag = self.is_crimson or self.is_word
        self.move_units(possessed_flag, self.elites.possessed, self.troops.possessed)
        self.troops.possessed.visible = possessed_flag

    def check_rules(self):
        super(CsmV2_5Base, self).check_rules()

        self.troops.zombie.active = (self.hq.typhus.count + self.hq.necrosius.count > 0)
        if self.hq.typhus.count == 0 and self.troops.zombie.count > 0:
            if self.hq.necrosius.count == 0 or not (type(self.parent.parent) is PrimaryDetachment):
                self.error("You can't have Plague Zombie without Typhus in army or Necrosius as a warlord")
            else:
                if len(self.troops.units) - self.troops.zombie.count < self.troops.min:
                    self.error("Plague Zombies cannot be compulsory Troops choices")
        # restore flags for default case
        ut_flags = [self.troops.tzaangors, self.hq.exsorc, self.elites.scarabs]
        if self.lords:
            ut_flags.append(self.lords.magnus)
        for ut in ut_flags:
            ut.active = True
        self.perform_unit_move()
        # cromson slaughter detachment checks
        self.check_crimson()
        self.check_thousand()
        self.check_black()
        self.check_alpha()
        self.check_iron()
        self.check_night()
        self.check_word()
        self.check_eater()
        self.check_guard()
        self.check_children()
        # technomancer check
        technomancer = self.has_abaddon or self.hq.sorc.count > 0\
                       or self.hq.smith.count > 0
        relic_limit = 1 if type(self.parent.parent) is PrimaryDetachment else 0
        relic_count = self.elites.count_relics() + self.fast.count_relics()\
                      + self.heavy.count_relics()
        if relic_count > relic_limit and technomancer < 1:
            self.error('Detachment must include a Technomancer to take {} Infernal Relics'.format(relic_count))


class CsmV2_5CAD(CsmV2_5Base, CombinedArmsDetachment):
    army_id = 'csm_v2_5_cad'
    army_name = 'Chaos Space Marines (Combined arms detachment)'


class CsmV2_5AD(CsmV2_5Base, AlliedDetachment):
    army_id = 'csm_v2_5_ad'
    army_name = 'Chaos Space Marines (Allied detachment)'

class CsmV2_5PA(CsmV2_5Base, PlanetstrikeAttacker):
    army_id = 'csm_v2_5_pa'
    army_name = 'Chaos Space Marines (Planetstrike attacker detachment)'

class CsmV2_5PD(CsmV2_5Base, PlanetstrikeDefender):
    army_id = 'csm_v2_5_pd'
    army_name = 'Chaos Space Marines (Planetstrike defender detachment)'

class CsmV2_5SA(CsmV2_5Base, SiegeAttacker):
    army_id = 'csm_v2_5_sa'
    army_name = 'Chaos Space Marines (Siege War attacker detachment)'

class CsmV2_5SD(CsmV2_5Base, SiegeDefender):
    army_id = 'csm_v2_5_sd'
    army_name = 'Chaos Space Marines (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'csm_v2_5_asd'
    army_name = 'Chaos Space Marines (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class Purge(CombinedArmsDetachment, ChaosSpaceMarinesLegaciesRoster):
    army_name = 'The Purge'
    army_id = 'purge_csm_v1'
    imperial_armour = True
    ia_enabled = True

    @property
    def is_base(self):
        return True

    @property
    def is_black(self):
        return False

    @property
    def is_crimson(self):
        return False

    @property
    def is_thousand(self):
        return False

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return False

    def __init__(self):
        super(Purge, self).__init__(
            hq=PurgeHq(self), elites=PurgeElites(self), troops=PurgeTroops(self),
            fast=PurgeFast(self), heavy=PurgeHeavy(self), fort=Fort(self), lords=PurgeLords(self))
        self.troops.min, self.troops.max = (0, 8)
        self.elites.min, self.elites.max = (2, 6)
        self.heavy.max = 4

    def check_rules(self):
        super(Purge, self).check_rules()
        self.troops.zombies.active = (self.hq.typhus.count > 0) or \
                                     ((type(self.parent.parent) is PrimaryDetachment) and
                                      (self.hq.necrosius.count > 0))
        if self.hq.typhus.count == 0 and self.troops.zombies.count > 0:
            if (self.hq.necrosius.count == 0) \
               or not (type(self.parent.parent) is PrimaryDetachment):
                self.error("You can't have Plague Zombie units without Typhus in the army or Necrosius as a Warlord")
        if type(self.parent.parent) is PrimaryDetachment:
            self.move_units(self.hq.typhus.count + sum(u.marks.nurgle for u in self.hq.lord.units) > 0,
                            self.elites.plague, self.troops.plague)
        else:
            self.troops.plague.visible = False


class CsmV2_5KillTeam(Wh40kKillTeam):
    army_id = 'csm_v2_5_kt'
    army_name = 'Chaos Space Marines'
    ia_enabled = False

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(CsmV2_5KillTeam.KTTroops, self).__init__(parent)
            UnitType(self, ChaosSpaceMarines)
            UnitType(self, ChaosCultists)
            self.tzaangors = UnitType(self, Tzaangors)
            self.chosen = UnitType(self, Chosen)
            self.possessed = UnitType(self, Possessed)
            self.berz = UnitType(self, Berzerks)
            self.thousand = UnitType(self, RubricMarines)
            self.plague = UnitType(self, PlagueMarines)
            self.noise = UnitType(self, NoiseMarines)
            self.raptors = UnitType(self, Raptors)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(CsmV2_5KillTeam.KTElites, self).__init__(parent)
            self.chosen = UnitType(self, Chosen)
            self.possessed = UnitType(self, Possessed)
            self.berz = UnitType(self, Berzerks)
            self.thousand = UnitType(self, RubricMarines)
            self.plague = UnitType(self, PlagueMarines)
            self.noise = UnitType(self, NoiseMarines)

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(CsmV2_5KillTeam.KTFastAttack, self).__init__(parent)
            UnitType(self, Bikers)
            UnitType(self, Spawn)
            self.raptors = UnitType(self, Raptors)
            UnitType(self, WarpTalons)

    def __init__(self):
        self.troops = self.KTTroops(self)
        self.elites = self.KTElites(self)
        self.fast = self.KTFastAttack(self)
        super(CsmV2_5KillTeam, self).__init__(
            self.troops, self.elites, self.fast)
        self.codex = SupplementOptions(self)

    def check_crimson(self):
        if self.is_crimson:
            for ut in [self.elites.thousand]:
                ut.active = False
                if ut.count > 0:
                    self.error('Cannot take Veterans of the Long War in Crimson Slaughter detachment')

    def check_thousand(self):
        if self.is_thousand:
            for ut in [self.elites.berz, self.elites.noise,
                       self.elites.plague]:
                ut.active = not self.is_thousand
                if ut.count > 0:
                    self.error("Only Mark Of Tzeentch may be taken in Thousand Sons detachment")

    def check_marked(self):
        flag = self.is_alpha or self.is_night or self.is_iron
        if flag:
            legion_name = 'Alpha Legion' if self.is_alpha else\
                          ('Iron Warriors' if self.is_iron else
                           ('Night Lords'))
            for ut in [self.elites.thousand, self.troops.tzaangors,
                       self.elites.berz, self.elites.noise, self.elites.plague]:
                ut.active = False
                if ut.count > 0:
                    self.error("{} Detachment cannot include units with Mark of Chaos".format(legion_name))

    def check_word(self):
        if self.is_word:
            for ut in [self.elites.thousand, self.troops.tzaangors,
                       self.elites.berz, self.elites.noise, self.elites.plague]:
                ut.active = False
                if ut.count > 0:
                    self.error("Word Bearers Detachment cannot include cult units")

    def check_eater(self):
        if self.is_eater:
            for ut in [self.elites.thousand,
                       self.elites.noise, self.elites.plague, self.troops.tzaangors]:
                ut.active = False
                if ut.count > 0:
                    self.error("Only Mark of Khorne may be taken in World Eaters detachment")

    def check_guard(self):
        if self.is_guard:
            for ut in [self.elites.thousand,
                       self.elites.berz, self.elites.noise, self.troops.tzaangors]:
                ut.active = False
                if ut.count > 0:
                    self.error("Death Guard Detachment cannot include units with Mark of Chaos other then Mark of Nurgle")

    def check_children(self):
        if self.is_guard:
            for ut in [self.elites.thousand,
                       self.elites.berz, self.elites.plague, self.troops.tzaangors]:
                ut.active = False
                if ut.count > 0:
                    self.error("Emperor's Children Detachment cannot include units with Mark of Chaos other then Mark of Slaanesh")

    def perform_unit_move(self):
        Wh40kBase.move_units(self.is_thousand, self.elites.thousand, self.troops.thousand)
        self.troops.thousand.visible = self.is_thousand
        Wh40kBase.move_units(self.is_night, self.fast.raptors, self.troops.raptors)
        self.troops.raptors.visible = self.is_night
        Wh40kBase.move_units(self.is_eater, self.elites.berz, self.troops.berz)
        self.troops.berz.visible = self.is_eater
        Wh40kBase.move_units(self.is_guard, self.elites.plague, self.troops.plague)
        self.troops.plague.visible = self.is_guard
        Wh40kBase.move_units(self.is_child, self.elites.noise, self.troops.noise)
        self.troops.noise.visible = self.is_child
        # chosen may be troops for different Legions
        chosen_flag = self.is_black or self.is_alpha
        Wh40kBase.move_units(chosen_flag, self.elites.chosen, self.troops.chosen)
        self.troops.chosen.visible = chosen_flag
        possessed_flag = self.is_crimson or self.is_word
        Wh40kBase.move_units(possessed_flag, self.elites.possessed, self.troops.possessed)
        self.troops.possessed.visible = possessed_flag

    def check_rules(self):
        super(CsmV2_5KillTeam, self).check_rules()
        self.troops.tzaangors.active = True
        self.perform_unit_move()
        self.check_crimson()
        self.check_thousand()
        self.check_marked()
        self.check_guard()
        self.check_eater()
        self.check_children()
        self.check_word()

    @property
    def is_alpha(self):
        return self.codex.cur == self.codex.alpha

    @property
    def is_iron(self):
        return self.codex.cur == self.codex.iron

    @property
    def is_guard(self):
        return self.codex.cur == self.codex.guard

    @property
    def is_child(self):
        return self.codex.cur == self.codex.child

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_black(self):
        return self.codex.cur == self.codex.black

    @property
    def is_crimson(self):
        return self.codex.cur == self.codex.crimson

    @property
    def is_thousand(self):
        return self.codex.cur == self.codex.thousand

    @property
    def is_night(self):
        return self.codex.cur == self.codex.night

    @property
    def is_word(self):
        return self.codex.cur == self.codex.word

    @property
    def is_eater(self):
        return self.codex.cur == self.codex.eater


faction = 'Chaos Space Marines'


class CsmV2(Wh40k7ed):
    army_id = 'csm_v2'
    army_name = 'Chaos Space Marines'
    faction = faction
    obsolete = True

    def __init__(self):
        super(CsmV2, self).__init__([CsmV2CAD, Mayhem, Helcult, Helfist, FallenChampions], [CsmV2AD])

    def check_rules(self):
        super(CsmV2, self).check_rules()
        roster_check = lambda roster: 1 if roster.is_base else (2 if roster.is_black else 3)
        if any([isinstance(m.sub_roster.roster, CsmV2CAD)
               for m in self.primary.units]):
            ally_marines = [m.sub_roster.roster
                            for m in self.secondary.units
                            if isinstance(m.sub_roster.roster, CsmV2AD)]
            ally_chapters = set([roster_check(ally) for ally in ally_marines])
            own_chapter = set([roster_check(self.primary.units[0].sub_roster.roster)])
            if len(own_chapter & ally_chapters):
                self.error("Cannot have primary and allied detachment from same codex or supplement")


class CsmV2_5(Wh40kImperial, Wh40k7ed):
    army_id = 'csm_v2_5'
    army_name = 'Chaos Space Marines'
    faction = faction

    def __init__(self):
        super(CsmV2_5, self).__init__([CsmV2_5CAD, FlierDetachment, BlackCrusade,
                                       BlackCrusadeSpeartip, InsurgencyForce, GrandCompany,
                                       MurderTalon, GrandHost, WEButcherhorde,
                                       Vectorium, Rapture,
                                       GrandCoven, Purge, Mayhem, Helcult, Helfist,
                                       Butcherhorde, Bringers,
                                       BlackChosen, BlackHounds, Tormented,
                                       EnginePack, Cabal, Warband, Ravagers,
                                       MannonDisciples, DarkCovenant, SlaughterCult,
                                       SlaughterLord, Helguard, Onslaught, Wrathborn,
                                       ChaosWarband, Maelstorm, LostDamned,
                                       HellforgedWarpack, TerrorPack,
                                       DestructionCult, FistOfGods, RaptorTalon,
                                       TerminatorAnnihilationForce, Favoured, Trinity,
                                       WarCabal, WarCoven, Warherd, SekhmetConclave,
                                       Exiles, Rehati, Colony, Kakophoni],
                                      [CsmV2_5AD])

    def check_rules(self):
        super(CsmV2_5, self).check_rules()
        roster_check = lambda roster: 1 if roster.is_base else \
                       (2 if roster.is_black else
                        (3 if roster.is_crimson else 4))
        if any([isinstance(m.sub_roster.roster, CsmV2_5CAD)
               for m in self.primary.units]):
            ally_marines = [m.sub_roster.roster
                            for m in self.secondary.units
                            if isinstance(m.sub_roster.roster, CsmV2_5AD)]
            ally_chapters = set([roster_check(ally) for ally in ally_marines])
            own_chapter = set([roster_check(self.primary.units[0].sub_roster.roster)])
            if len(own_chapter & ally_chapters):
                self.error("Cannot have primary and allied detachment from same codex or supplement")

class CsmV2_5Missions(Wh40kImperial, Wh40k7edMissions):
    army_id = 'csm_v2_5'
    army_name = 'Chaos Space Marines'
    faction = faction

    def __init__(self):
        super(CsmV2_5Missions, self).__init__([CsmV2_5CAD, CsmV2_5PA, CsmV2_5PD, CsmV2_5SA, CsmV2_5SD,
                                       FlierDetachment, BlackCrusade,
                                       BlackCrusadeSpeartip, InsurgencyForce, GrandCompany,
                                       MurderTalon, GrandHost, WEButcherhorde,
                                       Vectorium, Rapture,
                                       GrandCoven, Purge, Mayhem, Helcult, Helfist,
                                       Butcherhorde, Bringers,
                                       BlackChosen, BlackHounds, Tormented,
                                       EnginePack, Cabal, Warband, Ravagers,
                                       MannonDisciples, DarkCovenant, SlaughterCult,
                                       SlaughterLord, Helguard, Onslaught, Wrathborn,
                                       ChaosWarband, Maelstorm, LostDamned,
                                       HellforgedWarpack, TerrorPack,
                                       DestructionCult, FistOfGods, RaptorTalon,
                                       TerminatorAnnihilationForce, Favoured, Trinity,
                                       WarCabal, WarCoven, Warherd, SekhmetConclave,
                                       Exiles, Rehati, Colony, Kakophoni],
                                      [CsmV2_5AD])

    def check_rules(self):
        super(CsmV2_5Missions, self).check_rules()
        roster_check = lambda roster: 1 if roster.is_base else \
                       (2 if roster.is_black else
                        (3 if roster.is_crimson else 4))
        if any([isinstance(m.sub_roster.roster, CsmV2_5CAD)
               for m in self.primary.units]):
            ally_marines = [m.sub_roster.roster
                            for m in self.secondary.units
                            if isinstance(m.sub_roster.roster, CsmV2_5AD)]
            ally_chapters = set([roster_check(ally) for ally in ally_marines])
            own_chapter = set([roster_check(self.primary.units[0].sub_roster.roster)])
            if len(own_chapter & ally_chapters):
                self.error("Cannot have primary and allied detachment from same codex or supplement")


detachments = [CsmV2_5CAD, CsmV2_5AD, CsmV2_5PA, CsmV2_5PD, CsmV2_5SA, CsmV2_5SD,
               BlackCrusade, BlackCrusadeSpeartip, InsurgencyForce,
               GrandCompany, MurderTalon, GrandHost,
               WEButcherhorde,
               Vectorium, Rapture,
               GrandCoven, Purge, FlierDetachment, Mayhem, Helfist,
               Helcult, Butcherhorde,
               Bringers, BlackChosen, BlackHounds,
               Tormented, EnginePack, Cabal, Warband, Ravagers,
               MannonDisciples, DarkCovenant, SlaughterCult,
               SlaughterLord, Helguard, Onslaught, Wrathborn,
               ChaosWarband, Maelstorm, LostDamned, HellforgedWarpack,
               TerrorPack,
               DestructionCult, FistOfGods, RaptorTalon,
               TerminatorAnnihilationForce, Favoured, Trinity,
               WarCabal, WarCoven, Warherd, SekhmetConclave,
               Exiles, Rehati, Colony, Kakophoni]

unit_types = [
    Abaddon, Huron, Kharn, Ahriman, Typhus, Lucius, Fabius,
    Lord, Sorcerer, ExaltedSorc, DaemonPrince, Warpsmith, Apostle,
    Chosen, Possessed, Terminators, Helbrute, Mutilators, Berzerks,
    RubricMarines, SOTerminators, PlagueMarines, NoiseMarines,
    ChaosSpaceMarines, ChaosCultists, Tzaangors, PlagueZombie, Bikers,
    Spawn, Raptors, WarpTalons, Heldrakes, Havoks, Obliterators,
    Defiler, Forgefiend, Maulerfiend, LandRaider, ChaosVindicators,
    ChaosPredators, Magnus, Belakor
] + csm_unit_types
