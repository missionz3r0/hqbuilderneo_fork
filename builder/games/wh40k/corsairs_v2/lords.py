__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf
from builder.games.wh40k.unit import Unit


class Manpire(Unit):
    type_name = 'Corsair Vampire Raider'
    type_id = 'vampire_v2'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Manpire.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Two twin-linked pulse lasers', 0, gear=[
                Gear('Twin-linked pulse laser', count=2)
            ])
            self.variant('Two twin-linked Phoenix missile launchers', 0, gear=[
                Gear('Twin-linked Phoenix missile launcher', count=2)
            ])
            self.variant('Twin-linked pulsar')

    def __init__(self, parent):
        super(Manpire, self).__init__(parent, 'Vampire Raider', 730,
                                      gear=[Gear('Scatter laser'),
                                            Gear('Corsair kinetic shroud')])
        self.Weapon(self)
