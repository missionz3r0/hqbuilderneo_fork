__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf, OptionsList, Count,\
    UnitDescription, ListSubUnit, UnitList, SubUnit, OptionalSubUnit
from .hq import CharacterTraits
from .transport import Falcon, Venom
from builder.games.wh40k.unit import Unit


class MalevolentBand(Unit):
    type_name = 'Corsair Malevolent Band'
    type_id = 'malevolent_v2'
    imperial_Armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(MalevolentBand.Options, self).__init__(parent, 'Options')
            self.variant('Corsair Jet Packs', 5, per_model=True)
            self.armour = self.variant('Heavy mesh armour', 5, per_model=True)

    class Special(OneOf):
        def __init__(self, parent):
            super(MalevolentBand.Special, self).__init__(parent, 'Special weapon')
            self.ccw = self.variant('Close combat weapon', 0)
            self.variant('Power weapon', 15)
            self.variant('Venom blade', 10)
            self.variant('Melta bombs', 5)

    def __init__(self, parent):
        super(MalevolentBand, self).__init__(parent)
        self.models = Count(self, 'Corsair Malevolents', 5, 10, 15,
                            per_model=True)
        self.special = [self.Special(self) for q in range(4)]
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur

    def check_rules(self):
        super(MalevolentBand, self).check_rules()
        spec_cnt = 2 * (self.models.cur / 5)
        for i in range(4):
            self.special[i].used = self.special[i].visible = i < spec_cnt

    def build_points(self):
        res = super(MalevolentBand, self).build_points()
        res += self.opt.points * (self.count - 1)
        return res

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        model = UnitDescription(name='Corsair Malevolent', points=15)
        model.add(self.opt.description).add_points(self.opt.points)
        if not self.opt.armour.value:
            model.add(Gear('Mesh armour'))
        model.add([Gear('Plasma grenades'), Gear('Brace of pistols')])
        spec_cnt = sum(ol.cur != ol.ccw for ol in self.special)
        def_cnt = self.count - spec_cnt
        if def_cnt:
            res.add(model.clone().add(Gear('Close combat weapon')).set_count(def_cnt))
        for spec in self.special:
            if spec.used and spec.cur != spec.ccw:
                res.add(model.clone().add(spec.description).add_points(spec.points))
        return res


class VoidstormBand(Unit):
    type_name = 'Corsair Voidstorm Band'
    type_id = 'voidstorm_v2'
    imperial_armour = True

    class Options1(OptionsList):
        def __init__(self, parent):
            super(VoidstormBand.Options1, self).__init__(parent, 'Options')
            self.packs = self.variant('Corsair jet pack', 5, per_model=True)
            self.variant('Voidplate harness', 10, per_model=True)

    class Options2(OptionsList):
        def __init__(self, parent):
            super(VoidstormBand.Options2, self).__init__(parent, '')
            self.variant('Haywire grenades', 25)
            self.variant('Tanglefield grenades', 10)
            self.variant('Void hardened armour', 10)

    class Felarch(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(VoidstormBand.Felarch.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Lasblaster')
                self.variant('Splinter rifle')
                self.variant('Shuriken catapult')
                self.special = [
                    self.variant('Flamer', 5),
                    self.variant('Fusion gun', 10),
                    self.variant('Schredder', 5),
                    self.variant('Blaster', 10)
                ]

            def is_special(self):
                return self.cur in self.special

        class Weapon2(OptionsList):
            def __init__(self, parent):
                super(VoidstormBand.Felarch.Weapon2, self).__init__(parent, '')
                self.variant('Close combat weapon')
                self.variant('Power weapon', 15)
                self.variant('Venom blade', 10)
                self.variant('Blast pistol', 20)
                self.variant('Dissonance pistol', 10)

        def __init__(self, parent):
            super(VoidstormBand.Felarch, self).__init__(parent, 'Corsair Felarch', 15, gear=[
                Gear('Shadowwave grenades'), Gear('Plasma grenades'), Gear('Heavy mesh armour')
            ])
            self.weapon = self.Weapon(self)
            self.Weapon2(self)
            CharacterTraits(self)

        @ListSubUnit.count_gear
        def has_special(self):
            return self.weapon.is_special()

        def build_description(self):
            res = super(VoidstormBand.Felarch, self).build_description()
            res.add(self.root_unit.options1.description)\
               .add_points(self.root_unit.options1.points)
            # res.add(self.root_unit.options2.description)
            return res

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(VoidstormBand.Transport, self).__init__(parent, 'Transport', limit=1)
            self.venom = SubUnit(self, Venom(parent=None))
            self.falcon = SubUnit(self, Falcon(parent=None))

        def check_rules(self):
            super(VoidstormBand.Transport, self).check_rules()
            self.used = self.visible = not self.parent.options1.packs.value
            if self.venom.visible and self.parent.get_count() > 5:
                self.parent.error("Corsair Voidstorm Band must be no more then 5 in number to take a Venom")
            if self.falcon.visible and self.parent.get_count() <= 5:
                self.parent.error("Corsair Voidstorm Band must be more then 5 in number to take a Falcon")

    def __init__(self, parent):
        # default cost for voidstorm is 65, but 15 * 5 = 75...
        super(VoidstormBand, self).__init__(parent, points=-10)
        self.options1 = self.Options1(self)
        self.options2 = self.Options2(self)
        self.models = UnitList(self, self.Felarch, 5, 10)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.models.count

    def build_points(self):
        return super(VoidstormBand, self).build_points()\
            + self.options1.points * (self.count - 1)

    def check_rules(self):
        super(VoidstormBand, self).check_rules()
        spec_cnt = sum(u.has_special() for u in self.models.units)
        if spec_cnt > self.get_count() / 5:
            self.error('Only one Felarch per 5 models may take a special weapon; taken: {}'.format(spec_cnt))

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.options2.description)
        res.add(self.models.description)
        res.add(self.transport.description)
        return res


class WaspSquadron(Unit):
    type_name = 'Corsair Wasp Squadron'
    type_id = 'wasps_v2'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(WaspSquadron.Options, self).__init__(parent, 'Options')
            self.variant('Corsair void burners', 5, per_model=True)

    class Wasp(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent, has_name=True):
                super(WaspSquadron.Wasp.Weapon, self).__init__(
                    parent, 'Weapon' if has_name else '')
                self.variant('Shuriken cannon')
                self.variant('Splinter cannon')
                self.variant('Scatter laser')
                self.variant('Starcannon', 5)
                self.variant('Bright lance', 5)
                self.variant('Dark lance', 5)
                self.variant('Eldar missile launcher', 15)

        def __init__(self, parent):
            super(WaspSquadron.Wasp, self).__init__(parent, 'Wasp', 70,
                                                    gear=[Gear('Corsair  kinetik shroud'),
                                                          Gear('Wasp jump pack')])
            self.Weapon(self)
            self.Weapon(self, False)

        def build_description(self):
            res = super(WaspSquadron.Wasp, self).build_description()
            res.add(self.root_unit.opt.description)\
               .add_points(self.root_unit.opt.points)
            return res

    def __init__(self, parent):
        super(WaspSquadron, self).__init__(parent)
        self.models = UnitList(self, self.Wasp, 1, 6)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.count

    def build_points(self):
        return super(WaspSquadron, self).build_points()\
            + self.opt.points * (self.count - 1)

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.models.description)
        return res
