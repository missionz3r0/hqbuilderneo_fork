from builder.core2 import Gear, OptionsList, SubUnit, OptionalSubUnit, UnitDescription, Count
from builder.games.wh40k.unit import Unit
from .fast import Rhino


class Prosecutors(Unit):
    type_name = 'Prosecutor Squad'
    type_id = 'prosecutors_v2'

    model_cost = 15
    single_model = UnitDescription('Prosecutor', options=[
        Gear('Bolter'), Gear('Close combat weapon'),
        Gear('Psyk-out grenades')
    ])

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Prosecutors.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Rhino(parent=None))

    class Models(Count):
        @property
        def description(self):
            return self.parent.single_model.clone().add_points(self.parent.model_cost).set_count(
                self.cur - self.parent.opt.any)

    class Upgrade(OptionsList):
        def __init__(self, parent):
            super(Prosecutors.Upgrade, self).__init__(parent, 'Options')
            desc = parent.single_model.clone().add_points(parent.model_cost + 10)
            desc.name = 'Sister Superior'
            self.variant('Upgrade to Sister Superior', 10, gear=[desc])

    def __init__(self, parent):
        super(Prosecutors, self).__init__(parent)
        self.opt = self.Upgrade(parent=self)
        self.models = self.Models(self, self.single_model.name, 5, 10, self.model_cost, True)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.models.cur

    def build_statistics(self):
        return self.note_transport(super(Prosecutors, self).build_statistics())


class Vigilators(Prosecutors):
    type_name = 'Vigilator Squad'
    type_id = 'vigilators_v2'

    model_cost = 13
    single_model = UnitDescription('Vigilator', options=[
        Gear('Executioner greatblade'),
        Gear('Psyk-out grenades')
    ])


class Witchseekers(Prosecutors):
    type_name = 'Witchseeker Squad'
    type_id = 'witchseekers_v2'

    model_cost = 17
    single_model = UnitDescription('Witchseeker', options=[
        Gear('Flamer'), Gear('Close combat weapon'),
        Gear('Psyk-out grenades')
    ])
