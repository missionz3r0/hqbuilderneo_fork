__author__ = 'Ivan Truskov'

from builder.games.wh40k.section import ElitesSection, UnitType
from builder.games.wh40k.roster import Wh40k7ed, Wh40kImperial, Wh40kBase
from .elites import Prosecutors, Vigilators, Witchseekers
from .fast import Rhino


class SisterSection(ElitesSection):
    def __init__(self, parent):
        super(SisterSection, self).__init__(parent, 1, 3)
        UnitType(self, Prosecutors)
        UnitType(self, Vigilators)
        UnitType(self, Witchseekers)


class NullMaidens(Wh40kBase):
    army_name = 'Null Maiden Task Force'
    army_id = 'sisters_silent_v2_task'

    def __init__(self):
        super(NullMaidens, self).__init__(section=SisterSection(self))


faction = 'Sisters of Silence'


class SistersOfSilenceV2(Wh40kImperial, Wh40k7ed):
    army_id = 'sisters_silent_v2'
    army_name = 'Sisters of Silence'
    faction = faction

    def __init__(self):
        super(SistersOfSilenceV2, self).__init__([NullMaidens])


detachments = [NullMaidens]
unit_types = [Prosecutors, Vigilators, Witchseekers, Rhino]
