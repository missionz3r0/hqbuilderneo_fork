__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear,\
    Count, SubUnit, OptionalSubUnit
from .elites import Junka
from builder.games.wh40k.roster import Unit
from builder.games.wh40k.orks_v3.hq import BigMek as CodexBigMek


class KustomMekaDread(Unit):
    type_name = 'Kustom Meka-Dread'
    type_id = 'meka_dread_v2'

    class Options(OneOf):
        def __init__(self, parent):
            super(KustomMekaDread.Options, self).__init__(parent, 'Options')
            self.variant('Mega charga', 15)
            self.variant('Rokkit-Bom Racks', 35)
            self.variant('Kustom Force Field', 75)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(KustomMekaDread.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Rippa Klaw', 0)
            self.variant('Big Zzappa', 15)
            self.variant('Shunta', 25)
            self.variant('Rattler Kannon', 10)

    def __init__(self, parent):
        super(KustomMekaDread, self).__init__(parent, points=180, gear=[
            Gear('Fixin\' Klaws'),
            Gear('Grot riggers'),
            Gear('Rippa klaw')])
        self.Options(self)
        self.Weapon(self)


class PainBoss(Unit):
    type_name = 'Pain Boss'
    type_id = 'pain_boss_v2'

    class Melee(OneOf):
        def __init__(self, parent):
            super(PainBoss.Melee, self).__init__(parent, 'Melee weapon')
            self.variant('Slugga', 0)
            self.variant('Big Choppa', 5)
            self.variant('Power klaw', 25)

    class Options(OptionsList):
        def __init__(self, parent):
            super(PainBoss.Options, self).__init__(parent, 'Options')
            self.variant("Attack squig", 15)
            self.variant("Cybork body", 10)
            self.variant("'eavy armour", 5)
            self.variant("Bosspole", 5)

    def __init__(self, parent):
        super(PainBoss, self).__init__(parent, points=50, gear=[
            Gear('\'Urty Siringe'),
            Gear('Doc\'s Tools')
        ])
        self.ccw = self.Melee(self)
        self.opt = self.Options(self)
        self.orderlies = Count(self, "Grot orderlies", 0, 3, 5)


class BigMek(CodexBigMek):

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(BigMek.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Junka(parent=None))

    def __init__(self, parent):
        super(BigMek, self).__init__(parent)
        self.transport = self.Transport(self)
        # disable bike

    def check_rules(self):
        super(BigMek, self).check_rules()
        self.wots.bike.visible = self.wots.bike.used = False

    def build_statistics(self):
        return self.note_transport(super(BigMek, self).build_statistics())
