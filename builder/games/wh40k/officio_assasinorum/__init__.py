__author__ = 'kis'

from builder.core2 import *
from builder.core2.section import Formation
from builder.games.wh40k.roster import Unit
from builder.games.wh40k.roster import Wh40kBase, ElitesSection, Wh40k7ed
#


class VindicareAssasin(Unit):
    type_name = "Vindicare Assasin"
    type_id = "vindicare_assasin_v1"
    wikilink = Unit.template_link.format(codex_name='officio-assassinorum', unit_name='Vindicare-Assassin')

    def __init__(self, parent, points=150):
        super(VindicareAssasin, self).__init__(parent, points=points, gear=[Gear('Exitus Ammo'), Gear('Blind Grenades'), Gear('Spy Mask'),
                                                                            Gear('Close combat weapon'), Gear('Exitus Rifle'), Gear('Exitus Pistol')])


class EversorAssasin(Unit):
    type_name = "Eversor Assasin"
    type_id = "eversor_assasin_v1"
    wikilink = Unit.template_link.format(codex_name='officio-assassinorum', unit_name='Eversor-Assassin')

    def __init__(self, parent, points=135):
        super(EversorAssasin, self).__init__(parent, points=points, gear=[Gear('Sentinel Array'), Gear('Melta Bombs'),
                                                                          Gear('Frenzon'), Gear('Neuro-gauntlet'),
                                                                          Gear('Power Sword'),
                                                                          Gear('Executioner Pistol')])


class CallidusAssasin(Unit):
    type_name = "Callidus Assasin"
    type_id = "callidus_assasin_v1"
    wikilink = Unit.template_link.format(codex_name='officio-assassinorum', unit_name='Callidus-Assassin')

    def __init__(self, parent, points=145):
        super(CallidusAssasin, self).__init__(parent, points=points, gear=[Gear('Neural Shredder'), Gear('Phase Sword'),
                                                                           Gear('Poison Blades')])


class CulexusAssasin(Unit):
    type_name = "Culexus Assasin"
    type_id = "culexus_assasin_v1"
    wikilink = Unit.template_link.format(codex_name='officio-assassinorum', unit_name='Culexus-Assassin')

    def __init__(self, parent, points=140):
        super(CulexusAssasin, self).__init__(parent, points=points, gear=[Gear('Etherium'), Gear('Animus Speculum'),
                                                                          Gear('Psyk-out Grenades')])


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, VindicareAssasin)
        UnitType(self, CulexusAssasin)
        UnitType(self, CallidusAssasin)
        UnitType(self, EversorAssasin)


class OfficioAssasinorum(Wh40kBase):
    army_id = 'officio_assasinorum'
    army_name = 'Officio Assasinorum Detachment'

    def __init__(self):
        self.elites = Elites(parent=self)
        self.elites.min, self.elites.max = (1, 1)
        super(OfficioAssasinorum, self).__init__(
            elites=self.elites,
        )


class ExecutionForce(Formation):
    army_id = 'officio_assasinorum_formation'
    army_name = 'Execution Force'

    def __init__(self):
        super(ExecutionForce, self).__init__()
        UnitType(self, VindicareAssasin, min_limit=1, max_limit=1)
        UnitType(self, CulexusAssasin, min_limit=1, max_limit=1)
        UnitType(self, CallidusAssasin, min_limit=1, max_limit=1)
        UnitType(self, EversorAssasin, min_limit=1, max_limit=1)


faction = 'Assasins'


class Assasin(Wh40k7ed):
    army_id = 'assasin'
    army_name = 'Officio Assasinorum'
    faction = faction

    def __init__(self):
        super(Assasin, self).__init__([OfficioAssasinorum, ExecutionForce])


detachments = [OfficioAssasinorum, ExecutionForce]


unit_types = [
    VindicareAssasin,
    CulexusAssasin,
    CallidusAssasin,
    EversorAssasin
]
