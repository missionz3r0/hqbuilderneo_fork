__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import Gear, OptionalSubUnit, SubUnit, Count,\
    OneOf, OptionsList, UnitDescription
from builder.games.wh40k.roster import Unit
from .armory import *
from .fast import Transport


class StrikeTeam(Unit):
    type_name = 'Strike Team'
    type_id = 'strike_team_v3'

    model_points = 9
    model_gear = [Gear('Photon grenades')]
    model_min = 5
    model_max = 12
    model_name = 'Fire Warrior'
    model = UnitDescription(model_name, points=model_points, options=model_gear)

    class ModelCount(Count):
        @property
        def description(self):
            return [StrikeTeam.model.clone().add(Gear('Pulse rifle')).set_count(self.cur - self.parent.carbine.cur)]

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(StrikeTeam.Leader, self).__init__(parent=parent, name='')
            self.shasui = SubUnit(self, StrikeTeam.Shasui())

    class Shasui(Unit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent=None):
            super(StrikeTeam.Shasui, self).__init__(parent=parent, points=StrikeTeam.model_points + 10,
                                                    gear=StrikeTeam.model_gear)
            self.Weapon(self)
            self.Options(self)
            self.drones = Drones(self, infantry=True)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(StrikeTeam.Shasui.Weapon, self).__init__(parent=parent, name='Weapon')
                self.pulserifle = self.variant('Pulse rifle', 0)
                self.pulsecarbine = self.variant('Pulse carbine', 0)

        class Options(OptionsList):
            def __init__(self, parent):
                super(StrikeTeam.Shasui.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.variant('Markerlight and target lock', 15, gear=[Gear('Markerlight'), Gear('Target lock')])

        def check_rules(self):
            self.drones.check_rules()

    class Options(Ritual):
        def __init__(self, parent):
            super(StrikeTeam.Options, self).__init__(parent=parent)
            self.variant('EMP grenades', 2, per_model=True)

    class Turret(OptionsList):
        def __init__(self, parent):
            super(StrikeTeam.Turret, self).__init__(parent, name='Tactical support turret', limit=1)
            self.variant('DS8 turret with missile pod', 10)
            self.variant('DS8 turret with smart missile system', 10)

    def __init__(self, parent):
        super(StrikeTeam, self).__init__(parent=parent)
        self.leader = self.Leader(self)
        self.warrior = self.ModelCount(self, self.model_name, min_limit=self.model_min, max_limit=self.model_max,
                                       points=StrikeTeam.model_points)
        self.carbine = Count(self, 'Pulse carbine', min_limit=0, max_limit=self.model_min, points=0,
                             gear=self.model.clone().add(Gear('Pulse carbine')))
        self.Options(self)
        self.Turret(self)
        self.transport = Transport(self)

    def check_rules(self):
        super(StrikeTeam, self).check_rules()
        self.warrior.min = self.model_min - self.leader.count
        self.warrior.max = self.model_max - self.leader.count
        self.carbine.max = self.warrior.cur

    def get_count(self):
        return self.warrior.cur + self.leader.count

    def build_statistics(self):
        res = super(StrikeTeam, self).build_statistics()
        if self.transport.any:
            res['Models'] += 3
            res['Units'] += 1
        if self.leader.any:
            res['Models'] += self.leader.shasui.unit.drones.count
        return res


class BreacherTeam(Unit):
    type_name = 'Breacher Team'
    type_id = 'breacher_team_v3'

    model_points = 9
    model_gear = [Gear('Photon grenades'), Gear('Pulse blaster'), Gear('Field amplifier relay')]
    model_min = 5
    model_max = 10
    model_name = 'Fire Warrior'
    model = UnitDescription(model_name, points=model_points, options=model_gear)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(BreacherTeam.Leader, self).__init__(parent=parent, name='')
            self.shasui = SubUnit(self, BreacherTeam.Shasui())

    class Shasui(Unit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent=None):
            super(BreacherTeam.Shasui, self).__init__(parent=parent, points=BreacherTeam.model_points + 10,
                                                      gear=BreacherTeam.model_gear)
            self.Options(self)
            self.drones = Drones(self, infantry=True)

        class Options(OptionsList):
            def __init__(self, parent):
                super(BreacherTeam.Shasui.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.variant('Markerlight and target lock', 15, gear=[Gear('Markerlight'), Gear('Target lock')])

        def check_rules(self):
            self.drones.check_rules()

    def __init__(self, parent):
        super(BreacherTeam, self).__init__(parent=parent)
        self.leader = self.Leader(self)
        self.warrior = Count(self, self.model_name, min_limit=self.model_min,
                             max_limit=self.model_max, points=BreacherTeam.model_points,
                             gear=self.model)
        StrikeTeam.Options(self)
        StrikeTeam.Turret(self)
        self.transport = Transport(self)

    def check_rules(self):
        super(BreacherTeam, self).check_rules()
        self.warrior.min = self.model_min - self.leader.count
        self.warrior.max = self.model_max - self.leader.count

    def get_count(self):
        return self.warrior.cur + self.leader.count

    def build_statistics(self):
        res = super(BreacherTeam, self).build_statistics()
        if self.transport.any:
            res['Models'] += 3
            res['Units'] += 1
        if self.leader.any:
            res['Models'] += self.leader.shasui.unit.drones.count
        return res


class KrootSquad(Unit):
    type_name = 'Kroot Carnivores'
    type_id = 'krootcarnivoresquad_v3'

    model_points = 6

    class Shaper(Unit):
        type_name = 'Shaper'
        type_id = 'shaper_v1'

        def __init__(self, parent=None):
            super(KrootSquad.Shaper, self).__init__(parent=parent, points=KrootSquad.model_points + 15)
            self.weapon = self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(KrootSquad.Shaper.Weapon, self).__init__(parent=parent, name='Weapon')

                self.krootrifle = self.variant('Kroot rifle', 0)
                self.pulserifle = self.variant('Pulse rifle', 4)
                self.pulsecarbine = self.variant('Pulse carbine', 4)

            def count_rifle(self):
                return self.cur == self.krootrifle

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(KrootSquad.Leader, self).__init__(parent=parent, name='')
            self.leader = SubUnit(self, KrootSquad.Shaper())

        def count_rifle(self):
            return self.leader.unit.weapon.count_rifle() and self.leader.used

    class Options(OptionsList):
        def __init__(self, parent):
            super(KrootSquad.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.sniperrounds = self.variant('Sniper rounds', 1, per_model=True)

        @property
        def points(self):
            return super(KrootSquad.Options, self).points * self.parent.rifles

    def __init__(self, parent):
        super(KrootSquad, self).__init__(parent=parent)

        self.leader = self.Leader(self)
        self.kroot = Count(
            self, 'Kroot', min_limit=10, max_limit=20, points=self.model_points,
            gear=UnitDescription('Kroot', self.model_points, options=[Gear('Kroot rifle')])
        )
        self.krootoxrider = Count(
            self, 'Krootox Rider', min_limit=0, max_limit=3, points=25,
            gear=UnitDescription('Krootox Rider', 25, options=[Gear('Kroot gun')])
        )
        self.kroothound = Count(
            self, 'Kroot Hound', min_limit=0, max_limit=10, points=5, gear=UnitDescription('Kroot Hound', 5)
        )
        self.Options(self)

    @property
    def rifles(self):
        return self.kroot.cur + self.leader.count_rifle()

    def build_statistics(self):
        res = {
            'Models': self.kroot.cur + self.krootoxrider.cur + self.kroothound.cur + self.leader.count,
            'Units': 1
        }
        return res
