__author__ = 'Ivan'

from builder.core2 import ListSubUnit, UnitList,\
    OneOf, Gear
from builder.games.wh40k.eldar_v3.armory import VehicleEquipment
from builder.games.wh40k.roster import Unit

ia_id = ' (IA vol.11)'


class Wasps(Unit):
    clear_name = 'Eldar Wasp Assault Walker Squadron'
    type_name = clear_name + ia_id
    type_id = 'wasps_v2'
    imperial_armour = True

    def __init__(self, parent):
        super(Wasps, self).__init__(parent=parent, name='Wasp Assault Walker Squadron')
        self.models = UnitList(parent=self, unit_class=self.Wasp, min_limit=1, max_limit=3)
        self.opt = VehicleEquipment(self, warwalker=True)

    class Wasp(ListSubUnit):
        type_name = 'Wasp Assault Walker'

        def __init__(self, parent):
            super(Wasps.Wasp, self).__init__(parent=parent, points=70, gear=[
                Gear('Power field'), Gear('Wasp jump pack')
            ])
            self.Weapon(self, name='Weapon')
            self.Weapon(self, name='')

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(Wasps.Wasp.Weapon, self).__init__(parent=parent, name=name)
                self.shurikencannon = self.variant('Shuriken cannon', 0)
                self.scatterlaser = self.variant('Scatter Laser', 0)
                self.starcannon = self.variant('Starcannon', 5)
                self.brightlance = self.variant('Bright Lance', 5)
                self.eldarmissilelauncher = self.variant('Eldar Missile Launcher', 15)

        def build_description(self):
            res = super(Wasps.Wasp, self).build_description()
            res.add(self.root_unit.opt.description)
            res.add_points(self.root_unit.opt.points)
            return res

    def build_points(self):
        return super(Wasps, self).build_points() + self.opt.points * (self.get_count() - 1)

    def get_count(self):
        return self.models.count
