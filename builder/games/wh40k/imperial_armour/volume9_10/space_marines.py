# coding: utf-8

from builder.core2 import Gear, OneOf
from builder.games.wh40k.roster import Unit


class Titus(Unit):
    type_name = 'Chaplain Dreadnought Titus, Revered Dreadnought Confessor of the Howling Griffons (IA vol.9)'
    type_id = 'titus_v1'
    imperial_armour = True
    chapter = 'ultra'

    class BuiltIn(OneOf):
        def __init__(self, parent):
            super(Titus.BuiltIn, self).__init__(parent, 'Built-in weapon')
            self.variant('Heavy flamer')
            self.variant('Storm bolter')

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Titus.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant('Lascannon')
            self.variant('Assault cannon')

    def __init__(self, parent):
        super(Titus, self).__init__(
            parent=parent, name='Chaplain Dreadnought Titus',
            points=205, unique=True, gear=[
                Gear('Extra armour'),
                Gear('Smoke launcher'),
                Gear('Searchlight'),
                Gear('Dreadnought close combat weapon')
            ])
        self.BuiltIn(self)
        self.Ranged(self)


class Anton(Unit):
    type_name = 'Leutenant Commander Anton Narvaez, master-locum of Marines Errant, Captain of Star Jackal (IA vol.9)'
    type_id = 'anton_v1'
    imperial_armour = True
    chapter = 'ultra'

    def __init__(self, parent):
        super(Anton, self).__init__(parent, 'Leutenant Commander Anton Narvaez', 155, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Power sword'),
            Gear('Thundershock'),
            Gear('The Actinic Halo'),
            Gear('Power armour'),
            Gear('Void Hardened armour')
        ], static=True, unique=True)


class Tarnus(Unit):
    type_name = 'Captain Tarnus Vale, Praetod of the Fire Angels 3rd Company, the Hero of Askerlon Plains (IA vol.9)'
    type_id = 'tarnus_v1'
    imperial_armour = True
    chapter = 'ultra'

    def __init__(self, parent):
        super(Tarnus, self).__init__(parent, 'Captain Tarnus Vale', 165, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Master-crafetd chainsword'),
            Gear('Plasma pistol'),
            Gear('Melta bombs'),
            Gear('Power armour')
        ], static=True, unique=True)


class Issodon(Unit):
    type_name = 'Lias Issodon, Chapter Master of the Raptors, the \'Grim\' (IA vol.9)'
    type_id = 'issodon_v1'
    imperial_armour = True
    chapter = 'raptor'

    def __init__(self, parent):
        super(Issodon, self).__init__(parent, 'Lias Issodon', 175, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Power sword'),
            Gear('Malice'),
            Gear('Locator beacon'),
            Gear('Artificer armour')
        ], static=True, unique=True)


class Blaylock(Unit):
    type_name = "Captain Mordaci Blaylock, Captain of the Novamarines 1st Company, the 'Stormbreaker' (IA vol.9)"
    type_id = 'blaylock_v1'
    imperial_armour = True
    chapter = 'ultra'

    def __init__(self, parent):
        super(Blaylock, self).__init__(parent, 'Captain Mordaci Blaylock', 185, [
            Gear('Terminator armour'),
            Gear('Iron Halo'),
            Gear('Foe Ripper'),
            Gear('Storm bolter')
        ], static=True, unique=True)


class Pellas(Unit):
    type_name = "Captain Pellas Mir'san, Captain of the Salamanders 2nd Company, the Winter Blade, Defender of Nocturne (IA vol.10)"
    type_id = 'pellas_v1'
    imperial_armour = True
    chapter = 'salamander'

    def __init__(self, parent):
        super(Pellas, self).__init__(parent, 'Captain Pellas Mir\'san', 150, [
            Gear('Combi-flamer'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Cinder Edge'),
            Gear('Close combat weapon'),
            Gear('Iron Halo'),
            Gear('Artificer armour')
        ], static=True, unique=True)


class Ashmantle(Unit):
    type_name = "Bray'arth Ashmantle, Venerable Ancient of the Salamanders, the Iron Dragon (IA vol.10)"
    type_id = 'ashmantle_v1'
    imperial_armour = True
    chapter = 'salamander'

    def __init__(self, parent):
        super(Ashmantle, self).__init__(
            parent=parent, name="Bray'arth Ashmantle",
            points=265, unique=True, gear=[
                Gear('Extra armour'),
                Gear('Smoke launcher'),
                Gear('Searchlight'),
                Gear('Dreadnought close combat weapon', count=2),
                Gear('Dreadfire heavy flamer', count=2)
            ], static=True)


class Harath(Unit):
    type_name = 'Master Harath Shen, Master Apothecary of the Salamanders Chapter, Defender of the Final Vault'
    type_id = 'harath_v1'
    imperial_armour = True
    chapter = 'salamander'

    def __init__(self, parent):
        super(Harath, self).__init__(parent, 'Master Harath Shen', 135, [
            Gear('Power sword'),
            Gear('Plasma pistol'),
            Gear('Digital weapons'),
            Gear('Artificer armour')
        ], static=True, unique=True)


class Ahazra(Unit):
    type_name = 'Ahazra Redth, Chief Librarian of the Mantis Warriors, the Dust Prophet, Guardian of the Endymion Cluster (IA vol.10)'
    type_id = 'ahazra_v1'
    imperial_armour = True
    chapter = 'mantis'

    def __init__(self, parent):
        super(Ahazra, self).__init__(parent, 'Ahazra Redth', 165, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Force sword'),
            Gear('The Talisman of Sundered Souls'),
            Gear('Psychic hood'),
            Gear('Power armour')
        ], static=True, unique=True)


class Thulsa(Unit):
    type_name = "High Chaplain Thulsa Kane, 'Old Night', High Mortiurge of the Executioners, Lord Speaker of the Dead (IA vol.10)"
    type_id = 'thulsa_v1'
    imperial_armour = True
    chapter = 'executioner'

    def __init__(self, parent):
        super(Thulsa, self).__init__(parent, 'High Chaplain Thulsa Kane', 190, [
            Gear('Plasma pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Rosarius'),
            Gear('The Lifetaker'),
            Gear('The Ænigmata Ferrum'),
            Gear("Grehdalin's Bones")
        ], static=True, unique=True)


class Androcles(Unit):
    type_name = "Captain Zhrukhal Androcles, Captain of the Star Phantoms 9th Company, 'Siegebreaker' (IA vol.10)"
    type_id = 'androcles_v1'
    imperial_armour = True
    chapter = 'star'

    def __init__(self, parent):
        super(Androcles, self).__init__(parent, 'Captain Zhrukhal Androcles', 145, [
            Gear('Combi-melta'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Power fist'),
            Gear('Iron Halo'),
            Gear('Power armour')
        ], static=True, unique=True)


class Vaylund(Unit):
    type_name = 'Vaylund Cal, Iron Thane of the Atropos War Clan, High Artificer of the Sons of Medusa, Scion of the Moirae (IA vol.10)'
    type_id = 'vaylund_v1'
    imperial_armour = True
    chapter = 'iron'

    def __init__(self, parent):
        super(Vaylund, self).__init__(parent, 'Vaylund Cal', 225, [
            Gear('Servo-harness'),
            Gear('Iron Halo'),
            Gear('Thunder hammer'),
            Gear('Artificer armour')
        ], static=True, unique=True)


class Courbray(Unit):
    type_name = "Captain Knight-Captain Elam Courbray, Grand Champion if the Tournament of Flame, the 'Young Master', Captain of the Fire Hawks 8th Company (IA vol.9)"
    type_id = 'courbray_v1'
    imperial_armour = True
    chapter = 'fire'

    def __init__(self, parent):
        super(Courbray, self).__init__(parent, 'Knight-Captain Elam Courbray', 185, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('The Sword Excellus'),
            Gear('Iron Halo'),
            Gear('Jump pack'),
            Gear('Power armour')
        ], static=True, unique=True)


class Silas(Unit):
    type_name = 'Captain Silas Alberec, Commander of the Exorcists 3rd Company, Wielder of the Hellslayer, Keeper of Vigils (IA vol.10)'
    type_id = 'silas_v1'
    imperial_armour = True
    chapter = 'exorcist'

    def __init__(self, parent):
        super(Silas, self).__init__(parent, 'Captain Silas Alberec', 185, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('The Hellslayer'),
            Gear('Iron Halo'),
            Gear('Teleport homer'),
            Gear('Power armour')
        ], static=True, unique=True)


class Huron(Unit):
    type_name = 'Lugft Huron, Master of the Astral Claws, Scourge of the Maelstorm, the Tyrant of Badab (IA vol.9)'
    type_id = 'huron_v1'
    imperial_armour = True
    chapter = 'astral'

    def __init__(self, parent):
        super(Huron, self).__init__(parent, 'Lugft Huron', 235, [
            Gear('Heavy flamer'),
            Gear('Iron Halo'),
            Gear('The Ghost Razors'),
            Gear('Terminator armour')
        ], static=True, unique=True)


class Corien(Unit):
    type_name = "Captain Corien Sumatris, The Tyrant's Champion, Warden of Piraeus, Captain of the Astral Claws 2nd Company (IA vol.9)"
    type_id = 'corien_v1'
    imperial_armour = True
    chapter = 'astral'

    def __init__(self, parent):
        super(Corien, self).__init__(parent, 'Captain Corien Sumatris', 165, [
            Gear('Spectre pattern bolter'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Goldenfang'),
            Gear('Storm shield'),
            Gear('Digital weapons'),
            Gear('Iron Halo'),
            Gear('Power armour')
        ], static=True, unique=True)


class Armenneus(Unit):
    type_name = 'Armenneus Valthex, The Alchemancer, Honoured Patriarch of the Forges of the Astral Claws (IA vol.9)'
    type_id = 'armenneus_v1'
    imperial_armour = True
    chapter = 'astral'

    def __init__(self, parent):
        super(Armenneus, self).__init__(parent, 'Armenneus Valthex', 145, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Conversion beamer'),
            Gear('Indynabula Array'),
            Gear('Artificer armour')
        ], static=True, unique=True)


class Carnac(Unit):
    type_name = 'Arch-centurion Carnac Commodus, Commander of the XXIInd Tyrant\'s Legion, the Bloody Scourge of Shaprias'
    type_id = 'carnac_v1'
    imperial_armour = True
    chapter = 'astral'

    def __init__(self, parent):
        super(Carnac, self).__init__(parent, 'Arch-centurion Carnac Commodus', 105, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Blood Biter'),
            Gear('Iron Halo'),
            Gear('Power armour'),
            Gear('Void Hardened armour')
        ], static=True, unique=True)


class Moloc(Unit):
    type_name = 'Lord Asterion Moloc, The Brazen Warlord, Master of the Minotaurs, Spear of Judgement  (IA vol.10)'
    type_id = 'moloc_v1'
    imperial_armour = True
    chapter = 'minotaurs'

    def __init__(self, parent):
        super(Moloc, self).__init__(parent, 'Lord Asterion Moloc', 235, [
            Gear('Storm shield'),
            Gear('Iron Halo'),
            Gear('The Black Spear'),
            Gear('Terminator armour')
        ], static=True, unique=True)


class Ivanus(Unit):
    type_name = 'Chaplain Ivanus Enkomi, Voice of the Minotaur, Reclusiarch of the Minotaurs (IA vol.10)'
    type_id = 'ivanus_v1'
    imperial_armour = True
    chapter = 'minotaurs'

    class Armour(OneOf):
        def __init__(self, parent):
            super(Ivanus.Armour, self).__init__(parent, 'Options')
            self.variant('Void hardened armour')
            self.variant('Jump pack')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Ivanus.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Crozius Arcanum', gear=[
                Gear('Crozius Arcanum'), Gear('Auxilary grenade launcher')])
            self.variant('Crozius Arkanos', 10)

    def __init__(self, parent):
        super(Ivanus, self).__init__(parent, 'Chaplain Ivanus Enkomi', 145, [
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Power fist'),
            Gear('Rozarius'),
            Gear('Power armour')
        ], unique=True)
        self.Weapon(self)
        self.Armour(self)


class Culln(Unit):
    type_name = 'Lord High Commander Carab Culln, Chapter Master of the Red Scorpions  (IA vol.9)'
    type_id = 'culln_v1'
    imperial_armour = True
    chapter = 'scorpions'

    def __init__(self, parent):
        super(Culln, self).__init__(parent, 'Lord High Commander Carab Culln', 215, [
            Gear('Master-crafted storm bolter'),
            Gear('Iron Halo'),
            Gear('The Blade of the Scorpion'),
            Gear('Terminator armour'),
            Gear('Teleport homer')
        ], static=True)

    def get_unique(self):
        return 'Carab Culln'


class Sevrin(Unit):
    type_name = 'Magister Sevrin Loth, Chief Librarian of the Red Scorpions, Witch-Bane (IA vol.9)'
    type_id = 'sevrin_v1'
    imperial_armour = True
    chapter = 'scorpions'

    def __init__(self, parent):
        super(Sevrin, self).__init__(parent, 'Magister Sevrin Loth', 175, [
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Force axe'),
            Gear('Indynabula Array'),
            Gear('The Armour of Selket')
        ], static=True, unique=True)


class Tyberos(Unit):
    type_name = 'Tyberos the Red Wake, Captain of the Nicor, Lord Reaper of the Void, Commander of Carchodons (IA vol.10)'
    type_id = 'tyberos_v1'
    imperial_armour = True
    chapter = 'karchodons'

    def __init__(self, parent):
        super(Tyberos, self).__init__(parent, 'Tyberos the Red Wake', 190, [
            Gear('Hunger & Slake'),
            Gear('Teleport homer'),
            Gear('Terminator armour')
        ], static=True, unique=True)
