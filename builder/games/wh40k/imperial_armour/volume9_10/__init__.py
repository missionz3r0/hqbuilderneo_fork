__author__ = 'Ivan Truskov'
__summary__ = ['Badab War Characters update']

from .space_marines import *
from .blood_angels import Phoros
from builder.games.wh40k.section import HQSection, UnitType


class SpaceMarinesBadabHq(HQSection):
    def __init__(self, parent):
        super(SpaceMarinesBadabHq, self).__init__(parent)
        self.culln = UnitType(self, Culln)
        self.sevrin = UnitType(self, Sevrin)
        self.tyberos = UnitType(self, Tyberos)
        UnitType(self, Titus)
        UnitType(self, Anton)
        UnitType(self, Tarnus)
        self.issodon = UnitType(self, Issodon)
        UnitType(self, Blaylock)
        UnitType(self, Pellas)
        UnitType(self, Ashmantle)
        UnitType(self, Harath)
        self.ahazra = UnitType(self, Ahazra)
        self.thulsa = UnitType(self, Thulsa)
        self.androcles = UnitType(self, Androcles)
        self.vayland = UnitType(self, Vaylund)
        UnitType(self, Courbray)
        UnitType(self, Silas)
        self.huron = UnitType(self, Huron)
        UnitType(self, Corien)
        self.armenneus = UnitType(self, Armenneus)
        UnitType(self, Carnac)
        self.moloc = UnitType(self, Moloc)
        UnitType(self, Ivanus)

    def chapter_master_count(self):
        return sum(_type.count for _type in [
            self.culln, self.tyberos, self.issodon,
            self.vayland, self.huron, self.moloc
        ])


class BloodAngelsBadabHq(HQSection):
    def __init__(self, parent):
        super(BloodAngelsBadabHq, self).__init__(parent)
        self.phoros = UnitType(self, Phoros)

    def chapter_master_count(self):
        return self.phoros.count
