__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear
from .transport import IAChaosDreadnought
from builder.games.wh40k.roster import Unit


class FerrumDreadnought(IAChaosDreadnought):
    type_name = 'Ferrum Infernus Chaos Dreadnought (IA vol.13)'
    type_id = 'ferrum_v1'
    imperial_armour = True

    class BuiltIn(OneOf):
        def __init__(self, parent):
            super(FerrumDreadnought.BuiltIn, self).__init__(parent, 'Built-in weapon')
            self.variant('Twin-linked bolter')
            self.variant('Heavy flamer', 5)

    class Arm1(OneOf):
        def __init__(self, parent):
            super(FerrumDreadnought.Arm1, self).__init__(parent, 'Weapon')
            self.builtin = FerrumDreadnought.BuiltIn(parent)
            self.variant('Multi-melta')
            self.variant('Twin-linked autocannon', 10)
            self.variant('Twin-linked lascannon', 25)
            self.variant('Twin-linked heavy bolters', 10)
            self.variant('Missile launcher', 5)
            self.ccw = self.variant('Dreadnought close combat weapon', 10)
            self.variant('Power scourge', 5)
            self.variant('Thunder hammer', 10)

        def check_rules(self):
            super(FerrumDreadnought.Arm1, self).check_rules()
            self.builtin.used = self.builtin.visible = self.used and self.cur == self.ccw

    class Arm2(OneOf):
        def __init__(self, parent):
            super(FerrumDreadnought.Arm2, self).__init__(parent, '')
            self.builtin = FerrumDreadnought.BuiltIn(parent)
            self.ccw = self.variant('Dreadnought close combat weapon', 0)
            self.variant('Missile launcher', 5)
            self.ccw2 = self.variant('Chainfist', 10)
            self.variant('Power scourge', 5)
            self.variant('Thunder hammer', 10)

        def check_rules(self):
            super(FerrumDreadnought.Arm2, self).check_rules()
            self.builtin.used = self.builtin.visible = self.used and self.cur in [self.ccw, self.ccw2]

    class Options(OptionsList):
        def __init__(self, parent):
            super(FerrumDreadnought.Options, self).__init__(parent, 'Options')
            self.variant('Extra armour', 10)
            self.variant('Malefic ammunition', 35)

    class Dedication(OptionsList):
        def __init__(self, parent):
            super(FerrumDreadnought.Dedication, self).__init__(parent, 'Dedication', limit=1)
            self.variant('Dedication to Khorne', 20)
            self.ng = self.variant('Dedication to Nurgle', 20)
            self.variant('Dedication to Tzeentch', 20)
            self.variant('Dedication to Slaanesh', 15)

    class Upgrade(OptionsList):
        def __init__(self, parent):
            super(FerrumDreadnought.Upgrade, self).__init__(parent, 'Upgrade', limit=1)
            self.variant('Lord of the Long War', 25)
            self.variant('Host of Daemonic Iron', 20)
            self.destroyer = self.variant('Destroyer of Cities', 30,
                                          gear=[
                                              Gear('Destroyer of Cities'),
                                              Gear('Flamestorm cannon'),
                                              Gear('Assault drill'),
                                              Gear('Heavy flamer')
                                          ])

    def __init__(self, parent):
        super(FerrumDreadnought, self).__init__(parent, 'Ferrum Infernus', 135,
                                                [Gear('Smoke launcher'), Gear('Searchlight')])
        self.arm1 = self.Arm1(self)
        self.arm2 = self.Arm2(self)
        self.Options(self)
        self.god = self.Dedication(self)
        self.up = self.Upgrade(self)

    def check_rules(self):
        super(FerrumDreadnought, self).check_rules()
        self.arm1.used = self.arm1.visible = self.arm2.used = self.arm2.visible = not self.up.destroyer.value
        self.arm1.check_rules()
        self.arm2.check_rules()

    def is_relic(self):
        return True

    def not_nurgle(self):
        return self.god.any and not self.god.ng.value


class SonicDreadnought(IAChaosDreadnought):
    type_name = 'Emperor\'s Children Sonic Dreadnought (IA vol.13)'
    type_id = 'sonic_v1'
    imperial_armour = True

    class BuiltIn(OneOf):
        def __init__(self, parent):
            super(SonicDreadnought.BuiltIn, self).__init__(parent, 'Built-in weapon')
            self.variant('Twin-linked bolter')
            self.variant('Heavy flamer', 5)

    class Arm1(OneOf):
        def __init__(self, parent):
            super(SonicDreadnought.Arm1, self).__init__(parent, 'Weapon')
            self.builtin = SonicDreadnought.BuiltIn(parent)
            self.variant('Twin-linked sonic blaster')
            self.variant('Blastmaster', 15)
            self.variant('Twin-linked lascannon', 25)
            self.variant('Twin-linked heavy bolters', 5)
            self.variant('Multi-melta')
            self.variant('Plasma cannon', 5)
            self.variant('Twin-linked autocannon', 5)
            self.ccw = self.variant('Dreadnought close combat weapon', 10)

        def check_rules(self):
            super(SonicDreadnought.Arm1, self).check_rules()
            self.builtin.used = self.builtin.visible = self.used and self.cur == self.ccw

    class Arm2(OneOf):
        def __init__(self, parent):
            super(SonicDreadnought.Arm2, self).__init__(parent, '')
            self.ccw2 = self.variant('Chainfist', gear=[
                Gear('Chainfist'), Gear('Storm bolter')
            ])
            self.variant('Missile launcher', 5)

    class Options(OptionsList):
        def __init__(self, parent):
            super(SonicDreadnought.Options, self).__init__(parent, 'Options')
            self.variant('Warp Amp', 15)
            self.variant('Extra armour', 10)
            self.variant('Malefic ammunition', 20)

    def __init__(self, parent):
        super(SonicDreadnought, self).__init__(parent, 'Sonic Dreadnought', 160,
                                                [Gear('Smoke launcher'), Gear('Searchlight'), Gear('Doom syren')])
        self.arm1 = self.Arm1(self)
        self.arm2 = self.Arm2(self)
        self.Options(self)


class ChaosContemptor(IAChaosDreadnought):
    type_name = 'Chaos Contemptor Dreadnought (IA vol.13)'
    type_id = 'chaos_contemptor_v1'
    imperial_armour = True

    class BuiltIn(OneOf):
        def __init__(self, parent):
            super(ChaosContemptor.BuiltIn, self).__init__(parent, 'Built-in weapon')
            self.variant('Combi-bolter')
            self.variant('Heavy flamer', 10)
            self.variant('Plasma blaster', 20)
            self.variant('Soulburner', 25)
            self.variant('Meltagun', 15)

    class Arm1(OneOf):
        def __init__(self, parent):
            super(ChaosContemptor.Arm1, self).__init__(parent, 'Weapon')
            self.builtin = ChaosContemptor.BuiltIn(parent)
            self.variant('Twin-linked heavy bolter')
            self.variant('Multi-melta')
            self.variant('Twin-linked autocannon', 5)
            self.variant('Plasma cannon', 10)
            self.variant('Butcher cannon', 25)
            self.variant('Twin-linked lascannon', 25)
            self.variant('Heavy conversion beamer', 35)
            self.ccw = self.variant('Dreadnought close combat weapon', 0)
            self.ccw2 = self.variant('Chainfist', 10)

        def check_rules(self):
            super(ChaosContemptor.Arm1, self).check_rules()
            self.builtin.used = self.builtin.visible = self.used and self.cur in [self.ccw, self.ccw2]

    class Arm2(OneOf):
        def __init__(self, parent):
            super(ChaosContemptor.Arm2, self).__init__(parent, '')
            self.builtin = ChaosContemptor.BuiltIn(parent)
            self.ccw = self.variant('Dreadnought close combat weapon', 0)
            self.ccw2 = self.variant('Chainfist', 10)
            self.variant('Twin-linked heavy bolter')
            self.variant('Multi-melta')
            self.variant('Twin-linked autocannon', 10)
            self.variant('Plasma cannon', 10)
            self.variant('Butcher cannon', 25)
            self.variant('Twin-linked lascannon', 25)

        def check_rules(self):
            super(ChaosContemptor.Arm2, self).check_rules()
            self.builtin.used = self.builtin.visible = self.used and self.cur in [self.ccw, self.ccw2]

    class Options(OptionsList):
        def __init__(self, parent):
            super(ChaosContemptor.Options, self).__init__(parent, 'Options')
            self.variant('Extra armour', 10)
            self.variant('Malefic ammunition', 40)
            self.variant('Havok launcher', 15)

    class Dedication(OptionsList):
        def __init__(self, parent):
            super(ChaosContemptor.Dedication, self).__init__(parent, 'Dedication', limit=1)
            self.variant('Dedication to Khorne', 20)
            self.ng = self.variant('Dedication to Nurgle', 20)
            self.variant('Dedication to Tzeentch', 20)
            self.variant('Dedication to Slaanesh', 15)

    def __init__(self, parent):
        super(ChaosContemptor, self).__init__(parent, 'Chaos Contemptor', 195,
                                                [Gear('Smoke launchers'), Gear('Searchlight')])
        self.arm1 = self.Arm1(self)
        self.arm2 = self.Arm2(self)
        self.Options(self)
        self.god = self.Dedication(self)

    def not_nurgle(self):
        return self.god.any and not self.god.ng.value


class Decimator(Unit):
    type_name = 'Chaos Decimator (IA vol.13)'
    type_id = 'decimator_v1'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent, name='Weapon'):
            super(Decimator.Weapon, self).__init__(parent, name)
            self.variant('Decimator siege claw', gear=[
                Gear('Decimator siege claw'), Gear('Heavy flamer')
            ])
            self.variant('Butcher cannon', 20)
            self.variant('Storm laser', 15)
            self.variant('Soulburner petard', 10)
            self.cb = self.variant('Heavy conversion beamer', 35)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Decimator.Options, self).__init__(parent, 'Options')
            self.variant('Searchlight', 1)
            self.variant('Smoke launchers', 3)

    class Dedication(OptionsList):
        def __init__(self, parent):
            super(Decimator.Dedication, self).__init__(parent, 'Dedication', limit=1)
            self.variant('Dedication to Khorne', 15)
            self.ng = self.variant('Dedication to Nurgle', 25)
            self.variant('Dedication to Slaanesh', 15)
            self.variant('Dedication to Tzeentch', 25)

    def __init__(self, parent):
        super(Decimator, self).__init__(parent, 'Chaos Decimator', points=201)
        self.arm1 = self.Weapon(self)
        self.arm2 = self.Weapon(self, '')
        self.Options(self)
        self.god = self.Dedication(self)

    def check_rules(self):
        super(Decimator, self).check_rules()
        self.arm1.cb.active = not self.arm2.cb == self.arm2.cur
        self.arm2.cb.active = not self.arm1.cb == self.arm1.cur

    def not_nurgle(self):
        return self.god.any and not self.god.ng.value


class SpinedBeast(Unit):
    type_name = 'Spined Chaos Beast (IA vol.13)'
    type_id = 'spined_beast_v1'
    imperial_armour = True

    class Dedication(OneOf):
        def __init__(self, parent):
            super(SpinedBeast.Dedication, self).__init__(parent, 'Dedication')
            self.variant('Daemon of Khorne', 0)
            self.ng = self.variant('Daemon of Nurgle', 15)
            self.sl = self.variant('Daemon of Slaanesh', 5)
            self.tz = self.variant('Daemon of Tzeentch', 15)

    def __init__(self, parent):
        super(SpinedBeast, self).__init__(parent, 'Spined Chaos Beast', 140)
        self.god = self.Dedication(self)

    def not_nurgle(self):
        return not self.god.cur == self.god.ng
