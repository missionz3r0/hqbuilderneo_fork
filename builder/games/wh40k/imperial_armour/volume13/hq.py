__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit

ia_id = ' (IA vol.13)'


class Mamon(Unit):
    clear_name = 'Mamon, Daemon Prince of Nurgle'
    type_name = clear_name + ia_id
    type_id = 'mamon_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Mamon, self).__init__(parent, name='Mamon', points=220, gear=[Gear('Contagion Spray')],
                                    static=True, unique=True)


class Uraka(Unit):
    clear_name = 'Uraka \'The Warfiend\', Daemon Prince of Khorne'
    type_name = clear_name + ia_id
    type_id = 'uraka_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Uraka, self).__init__(parent, name='Uraka', points=200, gear=[
            Gear('Executioner\'s Axe'),
            Gear('Collar of Khorne'),
            Gear('Warp Forged Armour'),
        ], static=True, unique=True)
