from builder.core2 import OptionsList, OneOf, Gear
from builder.games.wh40k.unit import Unit


__author__ = 'Ivan Truskov'

ia_id = ' (IA: Apoc)'


class MaradeurBomber(Unit):
    clear_name = 'Maradeur Bomber'
    type_name = clear_name + ia_id
    type_id = 'maradeur_bomber_v1'
    imperial_armour = True

    class Bombs(OneOf):
        def __init__(self, parent):
            super(MaradeurBomber.Bombs, self).__init__(parent, 'Payload')
            self.variant('Heavy bomb clusters', gear=[Gear('Heavy bomb cluster', count=4)])
            self.variant('Hellstorm bomb', gear=[Gear('Hellstorm bomb', count=2)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(MaradeurBomber.Options, self).__init__(parent, 'Options')
            self.variant('Flare/chaff launcher', 10)
            self.variant('Armoured cockpit', 15)
            self.variant('Infra-red targeting', 5)
            self.variant('Illum flares', 5)
            self.variant('Distinctive paint scheme', 10)

    def __init__(self, parent):
        super(MaradeurBomber, self).__init__(parent, self.clear_name, 400, [
            Gear('Twin-linked lascannon'), Gear('Twin-linked heavy bolter', count=2)
        ])
        self.Bombs(self)
        self.Options(self)


class MaradeurDestroyer(Unit):
    clear_name = 'Maradeur Destroyer'
    type_name = clear_name + ia_id
    type_id = 'maradeur_destroyer_v1'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(MaradeurDestroyer.Options, self).__init__(parent, 'Options')
            self.variant('Flare/chaff launcher', 10)
            self.variant('Armoured cockpit', 15)
            self.variant('Infra-red targeting', 5)
            self.variant('Illum flares', 5)
            self.variant('Distinctive paint scheme', 10)
            self.variant('Eight Hellstrike missiles', 80, gear=[Gear('Hellstrike missile', count=8)])

    def __init__(self, parent):
        super(MaradeurDestroyer, self).__init__(parent, self.clear_name, 425, [
            Gear('Twin-linked autocannon', count=3),
            Gear('Twin-linked assault cannon'), Gear('Twin-linked heavy bolter', count=2),
            Gear('heavy bomb cluster', count=3)
        ])
        self.Options(self)
