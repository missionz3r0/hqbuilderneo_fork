from builder.core2 import *
from builder.games.wh40k.section import HQSection, HeavySection, ElitesSection,\
    LordsOfWarSection, FastSection
from builder.games.wh40k.unit import Unit


__author__ = 'Denis Romanow'

ia_id = ' (IA: Apoc)'


class Scorpion(Unit):
    clear_name = 'Greater Brass Scorpion of Khorne'
    type_name = clear_name + ia_id
    type_id = 'scorpion_of_khorne_v1'

    def __init__(self, parent):
        super(Scorpion, self).__init__(parent, name=self.clear_name, points=700, gear=[
            Gear('Scorpion cannon'),
            Gear('Soulburner cannon'),
            Gear('Hallmaw cannon', count=2),
        ])


class Scabeiathrax(Unit):
    clear_name = 'Daemon Lord - Scabeiathrax the Bloated'
    type_name = clear_name + ia_id
    type_id = 'scabeiathrax_v1'

    def __init__(self, parent):
        super(Scabeiathrax, self).__init__(parent, name=self.clear_name, points=777, gear=[
            Gear('Blade of Decay'),
            Gear('Icon of Chaos '),
        ])


class Anggrath(Unit):
    clear_name = 'Daemon Lord - An\'ggrath the Unbound'
    type_name = clear_name + ia_id
    type_id = 'anggrath_v1'

    def __init__(self, parent):
        super(Anggrath, self).__init__(parent, name=self.clear_name, points=888, gear=[
            Gear('Axe of Khorne'),
            Gear('Bloodlash of Khorne'),
            Gear('Daemonic Armour'),
            Gear('Icon of Chaos '),
        ])


class Aetaosraukeres(Unit):
    clear_name = 'Daemon Lord - Aetaos\'rau\'keres'
    type_name = clear_name + ia_id
    type_id = 'aetaosraukeres_v1'

    def __init__(self, parent):
        super(Aetaosraukeres, self).__init__(parent, name=self.clear_name, points=999, gear=[
            Gear('Staff of Cataclysm'),
            Gear('Icon of Chaos '),
            Gear('Psyker (Mastery Level 4)')
        ])


class Zarakynel(Unit):
    clear_name = 'Daemon Lord - Zarakynel'
    type_name = clear_name + ia_id
    type_id = 'zarakynel_v1'

    def __init__(self, parent):
        super(Zarakynel, self).__init__(parent, name=self.clear_name, points=666, gear=[
            Gear('The Souleater sword'),
            Gear('Icon of Chaos '),
        ])


class Mamon(Unit):
    clear_name = 'Mamon, Daemon Prince of Nurgle'
    type_name = clear_name + ia_id
    type_id = 'mamon_v1'

    def __init__(self, parent):
        super(Mamon, self).__init__(parent, name=self.clear_name, points=220, gear=[Gear('Contagion Spray')])


class GiantChaosSpawn(Unit):
    clear_name = 'Giant Chaos Spawn'
    type_name = clear_name + ia_id
    type_id = 'giant_chaos_spawn_v1'

    def __init__(self, parent):
        super(GiantChaosSpawn, self).__init__(parent, name=self.clear_name, points=80)


class SpinedChaosBeast(Unit):
    clear_name = 'Spined Chaos Beast'
    type_name = clear_name + ia_id
    type_id = 'spined_chaos_beast_v1'

    def __init__(self, parent):
        super(SpinedChaosBeast, self).__init__(parent, name=self.clear_name, points=140)
        self.Daemon(self)

    class Daemon(OneOf):
        def __init__(self, parent):
            super(SpinedChaosBeast.Daemon, self).__init__(parent, name='Options')
            self.variant('Daemon of Khorne', 0)
            self.variant('Daemon of Nurgle', 15)
            self.variant('Daemon of Tzeentch', 5)
            self.variant('Daemon of Slaanesh', 15)


class Uraka(Unit):
    clear_name = 'Uraka \'The Warfiend\', Daemon Prince of Khorne'
    type_name = clear_name + ia_id
    type_id = 'uraka_v1'

    def __init__(self, parent):
        super(Uraka, self).__init__(parent, name=self.clear_name, points=200, gear=[
            Gear('Executioner\'s Axe'),
            Gear('Collar of Khorne'),
            Gear('Warp Forged Armour'),
        ])


class ReaverTitan(Unit):
    clear_name = 'Chaos Reaver Battle Titan'
    type_name = clear_name + ia_id
    type_id = 'chaos_reaver_titan_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ReaverTitan.Weapon1, self).__init__(parent, name='Carapace weapon')
            self.variant('Double-barrelled turbo laser destructor')
            self.variant('Plasma blastgun')
            self.variant('Inferno gun')
            self.variant('Vulcan mega-bolter')
            self.variant('Apocalypse missile-launcher')
            self.variant('Vortex missile')

    class Weapon2(OneOf):
        def __init__(self, parent, name):
            super(ReaverTitan.Weapon2, self).__init__(parent, name=name)
            self.variant('Gatling blaster')
            self.variant('Melta cannon')
            self.variant('Vulcano cannon')
            self.variant('Laser blaster')
            self.variant('Titan power first')

    class Daemon(OptionsList):
        def __init__(self, parent):
            super(ReaverTitan.Daemon, self).__init__(parent, name='Options', limit=1)
            self.variant('Daemon-Titan of Khorne', 100)
            self.variant('Daemon-Titan of Nurgle', 200)
            self.variant('Daemon-Titan of Tzeentch', 150)
            self.variant('Daemon-Titan of Slaanesh', 100)

    def __init__(self, parent):
        super(ReaverTitan, self).__init__(parent, name=self.clear_name, points=1460, gear=[
            Gear('Void shield', count=4),
            Gear('Dirge casters'),
        ])
        self.Weapon1(self)
        self.Weapon2(self, name='Arm weapon')
        self.Weapon2(self, name='')
        self.Daemon(self)


class WarhoundTitan(Unit):
    clear_name = 'Chaos Warhound Titan'
    type_name = clear_name + ia_id
    type_id = 'chaos_warhound_titan_v1'

    class Weapon1(OneOf):
        def __init__(self, parent, name):
            super(WarhoundTitan.Weapon1, self).__init__(parent, name=name)
            self.variant('Double-barrelled turbo laser destructor')
            self.variant('Plasma blastgun')
            self.variant('Inferno gun')
            self.variant('Vulcan mega-bolter')

    class Daemon(OptionsList):
        def __init__(self, parent):
            super(WarhoundTitan.Daemon, self).__init__(parent, name='Options', limit=1)
            self.variant('Daemon-Titan of Khorne', 50)
            self.variant('Daemon-Titan of Nurgle', 100)
            self.variant('Daemon-Titan of Tzeentch', 75)
            self.variant('Daemon-Titan of Slaanesh', 50)

    def __init__(self, parent):
        super(WarhoundTitan, self).__init__(parent, name=self.clear_name, points=730, gear=[
            Gear('Void shield', count=2),
            Gear('Dirge casters'),
        ])
        self.Weapon1(self, name='Arm weapon')
        self.Weapon1(self, name='')
        self.Daemon(self)


class ChaosThunderhawkGunship(Unit):
    clear_name = 'Chaos Space Marine Thunderhawk Gunship'
    type_name = clear_name + ia_id
    type_id = 'chaos_thunderhawk_gunship_v1'

    def __init__(self, parent):
        super(ChaosThunderhawkGunship, self) .__init__(parent=parent, name=self.clear_name, points=685, gear=[
            Gear('Twin-linked heavy bolter', count=4),
            Gear('Lascannon', count=2),
            Gear('Armoured Ceramite'),
        ])
        self.Weapon(self)
        self.Bombs(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ChaosThunderhawkGunship.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Thunderhawk cannon', 0)
            self.variant('Turbo-laser destructor', 90)

    class Bombs(OneOf):
        def __init__(self, parent):
            super(ChaosThunderhawkGunship.Bombs, self).__init__(parent=parent, name='')
            self.variant('Hellstrike missiles', 0, gear=Gear('Hellstrike missile', count=6))
            self.variant('Thunderhawk cluster bombs', 60, gear=Gear('Thunderhawk cluster bomb', count=6))

    class Options(OptionsList):
        def __init__(self, parent):
            super(ChaosThunderhawkGunship.Options, self).__init__(parent=parent, name='Options')
            self.variant('Daemonic transport', 50)


class StormEagleGunship(Unit):
    clear_name = 'Chaos Storm Eagle Assault Gunship'
    type_name = clear_name + ia_id
    type_id = 'chaos_storm_eagle_gunship_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormEagleGunship.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Twin-linked multi-melta', 15)
            self.variant('Havoc launcher', 5)
            self.variant('Reaper autocannon', 0)

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormEagleGunship.Options, self).__init__(parent, 'Options')
            self.hell = self.variant('Four hellstrike missiles', 40, gear=[Gear('Hellstrike missile', count=4)])
            self.las = self.variant('Two twin-linked lascannon', 60, gear=[Gear('Twin-linked lascannon', count=2)])
            self.variant('Extra Armour', 15)
            self.variant('Dirge Caster', 5)
            self.variant('Warpflame Gargoyles', 5)
            self.variant('Daemonic Possession', 15)

        def check_rules(self):
            self.process_limit([self.hell, self.las], 1)

    def __init__(self, parent):
        super(StormEagleGunship, self).__init__(parent=parent, name=self.clear_name, points=205, gear=[
            Gear('Vengeance launcher'),
            Gear('Armoured Ceramite'),
        ])
        self.wep1 = self.Weapon1(self)
        self.opt = self.Options(self)


class Spartan(Unit):
    type_id = "chaosspartan_v1"
    clear_name = 'Chaos Spartan Assault Tank'
    type_name = clear_name + ia_id

    class Options(OptionsList):
        def __init__(self, parent):
            super(Spartan.Options, self).__init__(parent, 'Options')
            self.variant('Frag assault launchers', 10)
            self.variant('Hunter-killer missile', 10)
            self.variant('Armoured Ceramite', 20)
            self.weapon = [
                self.variant('Comby bolter', 5),
                self.variant('Havoc launcher', 12),
                self.variant('Heavy bolter', 15),
                self.variant('Heavy flamer', 15),
                self.variant('Multi-melta', 20),
            ]
            self.variant('Daemonic Possession', 25)
            self.variant('Dirge Caster', 5)
            self.variant('Warpflame Gargoyles', 5)

        def check_rules(self):
            self.process_limit(self.weapon, 1)

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Spartan.Sponsons, self).__init__(parent, 'Weapon')
            self.variant('Quad lascannons', 0, gear=Gear('Quad lascannon', count=2))
            self.variant('Laser destroyers', 0, gear=Gear('Laser destroyer', count=2))

    class Hull(OneOf):
        def __init__(self, parent):
            super(Spartan.Hull, self).__init__(parent, '')
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Twin-linked heavy flamer', 0)

    def __init__(self, parent):
        super(Spartan, self) .__init__(parent=parent, name=self.clear_name, points=285, gear=[
            Gear('Extra armour'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.Hull(self)
        self.Sponsons(self)
        self.Options(self)


class RelicPredator(Unit):
    clear_name = "Chaos Relic Predator"
    type_name = clear_name + ia_id
    type_id = "relic_predator_v1"

    class Options(OptionsList):
        def __init__(self, parent):
            super(RelicPredator.Options, self).__init__(parent, 'Options')
            self.variant('Extra armour', 10)
            self.variant('Pintle-mounted combi-bolter', 5)
            self.variant('Dirge caster', 5)
            self.variant('Dozer blade', 5)
            self.variant('Warpflame gargoyles', 5)
            self.comby = [
                self.variant('Combi-melta', 10),
                self.variant('Combi-flamer', 10),
                self.variant('Combi-plasma', 10),
            ]
            self.variant('Havok launcher', 12)
            self.variant('Destroyer blades', 15)
            self.variant('Daemonic possession', 15)

        def check_rules(self):
            super(RelicPredator.Options, self).check_rules()
            self.process_limit(self.comby, 1)

    class Turret(OneOf):
        def __init__(self, parent):
            super(RelicPredator.Turret, self).__init__(parent=parent, name='Turret')
            self.variant('Autocannon', 0)
            self.variant('Magna-melta cannon', 45)
            self.variant('Flamestorm cannon', 15)
            self.variant('Heavy conversion beamer', 65)
            self.variant('Twin-linked lascannon', 25)
            self.variant('Plasma destroyer', 35)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(RelicPredator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.variant('Heavy bolters', 20, gear=Gear('Heavy bolter', count=2))
            self.variant('Heavy flamers', 20, gear=Gear('Heavy flamer', count=2))
            self.variant('Lascannons', 50, gear=Gear('Lascannon', count=2))

    def __init__(self, parent):
        super(RelicPredator, self).__init__(parent=parent, name=self.clear_name, points=75, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = self.Options(self)


class ChaosContemptorDreadnought(Unit):
    clear_name = "Chaos Contemptor Dreadnought"
    type_name = clear_name + ia_id
    type_id = "chaos_contemptor_dreadnought_v1"

    def __init__(self, parent):
        super(ChaosContemptorDreadnought, self).__init__(
            parent=parent, points=195, name=self.clear_name,
            gear=[Gear('Smoke launchers'), Gear('Searchlight')]
        )
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in1 = self.BuildIn(self)
        self.build_in2 = self.BuildIn(self)
        self.opt = self.Options(self)

    def check_rules(self):
        self.build_in1.visible = self.build_in1.used = self.wep1.cur == self.wep1.ccw
        self.build_in2.visible = self.build_in2.used = self.wep2.cur in [self.wep2.ccw, self.wep2.fist]

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ChaosContemptorDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Multi-melta', 0)
            self.variant('Twin-linked autocannon', 5)
            self.variant('Plasma cannon', 10)
            self.variant('Butcher cannon', 25)
            self.variant('Twin-linked lascannon', 25)
            self.variant('Heavy conversion beamer', 35)
            self.ccw = self.variant('Dreadnought close combat weapon', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(ChaosContemptorDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.ccw = self.variant('Dreadnought close combat weapon', 0)
            self.fist = self.variant('Chainfist', 10)
            self.variant('Multi-melta', 0)
            self.variant('Twin-linked autocannon', 10)
            self.variant('Plasma cannon', 10)
            self.variant('Butcher cannon', 25)
            self.variant('Twin-linked lascannon', 25)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(ChaosContemptorDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.variant('Built-in Comby bolter', 0, gear=Gear('Comby bolter'))
            self.variant('Built-in Heavy flamer', 10, gear=Gear('Heavy flamer'))
            self.variant('Built-in Plasma blaster', 20, gear=Gear('Plasma blaster'))
            self.variant('Built-in Soulburner', 25, gear=Gear('Soulburner'))
            self.variant('Built-in Meltagun', 15, gear=Gear('Meltagun'))

    class Options(OptionsList):
        def __init__(self, parent):
            super(ChaosContemptorDreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.variant('Extra armour', 10)
            self.variant('Carapace-mounted Havoc launcher', 25)
            self.marks = [
                self.variant('Dedicated of Khorne', 20),
                self.variant('Dedicated of Nurgle', 20),
                self.variant('Dedicated of Tzeentch', 20),
                self.variant('Dedicated of Slaanesh', 15)
            ]

        def check_rules(self):
            super(ChaosContemptorDreadnought.Options, self).check_rules()
            self.process_limit(self.marks, 1)


class ChaosDecimator(Unit):
    clear_name = "Chaos Decimator Daemon Engine"
    type_name = clear_name + ia_id
    type_id = "chaos_decimator_v1"

    def __init__(self, parent):
        super(ChaosDecimator, self).__init__(parent=parent, points=205, name=self.clear_name)
        self.wep1 = self.Weapon1(self, 'Weapon')
        self.wep2 = self.Weapon2(self, '')
        self.opt = self.Options(self)

    class Weapon1(OneOf):
        def __init__(self, parent, name):
            super(ChaosDecimator.Weapon1, self).__init__(parent=parent, name=name)
            self.variant('Siege claw with build-in heavy flamer', 0, gear=[Gear('Siege claw'), Gear('Heavy flamer')])
            self.variant('Butcher cannon', 20)
            self.variant('Storm laser', 15)
            self.variant('Soulburner petard', 10)

    class Weapon2(Weapon1):
        def __init__(self, parent, name):
            super(ChaosDecimator.Weapon2, self).__init__(parent=parent, name=name)
            self.variant('Heavy conversion beamer', 35)

    class Options(OptionsList):
        def __init__(self, parent):
            super(ChaosDecimator.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.variant('Searchlight', 1)
            self.variant('Smoke launchers', 3)
            self.marks = [
                self.variant('Dedicated of Khorne', 15),
                self.variant('Dedicated of Nurgle', 25),
                self.variant('Dedicated of Tzeentch', 25),
                self.variant('Dedicated of Slaanesh', 15)
            ]

        def check_rules(self):
            super(ChaosDecimator.Options, self).check_rules()
            self.process_limit(self.marks, 1)


class BlightDrones(Unit):
    clear_name = "Blight Drones"
    type_name = clear_name + ia_id
    type_id = "blight_drones_v1"

    def __init__(self, parent):
        super(BlightDrones, self).__init__(parent=parent, name=self.clear_name)
        self.models = Count(self, 'Blight Drone', 1, 3, 150, gear=UnitDescription('Blight Drone', options=[
            Gear('Mawcannon'), Gear('Reaper autocannon'),
        ]), per_model=True)

    def get_count(self):
        return self.models.cur


class BloodSlaughterers(Unit):
    clear_name = "Blood Slaughterers of Khorne"
    type_name = clear_name + ia_id
    type_id = "blood_slaughterers_v1"

    model_points = 130
    model_desc = UnitDescription('Blood Slaughterer', points=model_points,
                                 options=[Gear('Dreadnought close combat weapon')])

    class Count(Count):
        @property
        def description(self):
            return [BloodSlaughterers.model_desc.clone().set_count(self.cur - self.parent.imp.cur)]

    def __init__(self, parent):
        super(BloodSlaughterers, self).__init__(parent=parent, name=self.clear_name)
        self.models = self.Count(self, 'Blood Slaughterer', 1, 3, self.model_points, per_model=True)
        self.imp = Count(self, 'Impaler', 0, 1, 5, gear=self.model_desc.clone().add(Gear('Impaler')).add_points(5))

    def get_count(self):
        return self.models.cur

    def check_rules(self):
        super(BloodSlaughterers, self).check_rules()
        self.imp.max = self.models.cur


class PlagueHulk(Unit):
    clear_name = "Plague Hulk of Nurgle"
    type_name = clear_name + ia_id
    type_id = "plague_hulk_v1"

    def __init__(self, parent):
        super(PlagueHulk, self).__init__(parent=parent, points=150, name=self.clear_name, gear=[
            Gear('Rancid vomit'), Gear('Rot cannon')
        ])
        self.wep1 = self.Weapon1(self, 'Weapon')

    class Weapon1(OneOf):
        def __init__(self, parent, name):
            super(PlagueHulk.Weapon1, self).__init__(parent=parent, name=name)
            self.variant('Iron claw', 0)
            self.variant('Warpsword', 25)


class DaemonsHQ(HQSection):
    def __init__(self, parent):
        super(DaemonsHQ, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, Mamon),
            UnitType(self, Uraka),
        ])


class DaemonsElites(ElitesSection):
    def __init__(self, parent):
        super(DaemonsElites, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, ChaosDecimator),
        ])


class ChaosElites(ElitesSection):
    def __init__(self, parent):
        super(ChaosElites, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, ChaosContemptorDreadnought),
            UnitType(self, ChaosDecimator),
            UnitType(self, GiantChaosSpawn),
        ])


class ChaosFastAttack(FastSection):
    def __init__(self, parent):
        super(ChaosFastAttack, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, StormEagleGunship),
            UnitType(self, BlightDrones),
        ])


class DaemonFastAttack(FastSection):
    def __init__(self, parent):
        super(DaemonFastAttack, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, BlightDrones),
        ])


class ChaosHeavySupport(HeavySection):
    def __init__(self, parent):
        super(ChaosHeavySupport, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, Spartan),
            UnitType(self, RelicPredator),
            UnitType(self, BloodSlaughterers),
            UnitType(self, PlagueHulk),
        ])


class DaemonsHeavy(HeavySection):
    def __init__(self, parent):
        super(DaemonsHeavy, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, BloodSlaughterers),
            UnitType(self, PlagueHulk),
            UnitType(self, SpinedChaosBeast),
        ])


class DaemonLordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(DaemonLordsOfWar, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, Scabeiathrax),
            UnitType(self, Anggrath),
            UnitType(self, Aetaosraukeres),
            UnitType(self, Zarakynel),
        ])


class ChaosLordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(ChaosLordsOfWar, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, ReaverTitan),
            UnitType(self, WarhoundTitan),
            UnitType(self, Scorpion),
            UnitType(self, ChaosThunderhawkGunship),
        ])
