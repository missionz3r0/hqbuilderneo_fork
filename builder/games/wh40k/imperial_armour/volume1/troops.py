__author__ = 'Ivan Truskov'
ia_suffix = ' (IA vol.1)'


from builder.core2 import OptionsList, OneOf, Gear,\
    UnitDescription, ListSubUnit, UnitList


class SabreBattery(ListSubUnit):
    type_name = 'Sabre Weapon Battery'
    type_id = 'sabre_ia_v1'

    class ExtraCrew(OptionsList):
        def __init__(self, parent):
            super(SabreBattery.ExtraCrew, self).__init__(parent, 'Options')
            self.variant('Additional Crewman', 2, per_model=True, gear=[])

        @property
        def points(self):
            return super(SabreBattery.ExtraCrew, self).points *\
                self.parent.get_count()

    class Platform(ListSubUnit):
        type_name = 'Sabre Gun Platform'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(SabreBattery.Platform.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Two twin-linked heavy bolters', gear=[
                    Gear('Twin-linked heavy bolter', count=2)
                ])
                self.variant('Two twin-linked heavy stubbers', 10, gear=[
                    Gear('Twin-linked heavy stubber', count=2)
                ])
                self.variant('Twin-linked autocannon', 10)
                self.variant('Twin-linked lascannon', 20)
                self.variant('Defense searchlight')

        def __init__(self, parent):
            super(SabreBattery.Platform, self).__init__(parent, 'Platform',
                                                        30)
            self.Weapon(self)

        def build_description(self):
            res = super(SabreBattery.Platform, self).build_description()
            crew = UnitDescription('Crew', options=[
                Gear('Flak armour'), Gear('Lasgun'), Gear('Close combat weapon')
            ])
            crew.set_count(1 + self.root_unit.crew.any)
            res.add(crew)
            return res

    def __init__(self, parent):
        super(SabreBattery, self).__init__(parent)
        self.guns = UnitList(self, self.Platform, 1, 3)
        self.crew = self.ExtraCrew(self)
        self.models.points = 30

    def count_members(self):
        return self.guns.count * (1 + self.crew.any)
