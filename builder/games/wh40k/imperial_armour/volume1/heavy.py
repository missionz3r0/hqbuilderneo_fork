__author__ = 'Ivan Truskov'
ia_suffix = ' (IA vol.1)'


from builder.core2 import OptionsList, OneOf, Gear,\
    UnitDescription, Count, SubUnit, UnitList, ListSubUnit
from builder.games.wh40k.unit import Unit
from .armory import VehicleOptions, CamoSquadron
from .fast import Centaur


class LemanRuss(Unit):
    type_name = 'Leman Russ'

    class Type(OneOf):
        def __init__(self, parent):
            super(LemanRuss.Type, self).__init__(parent=parent, name='Type')
            self.variant('Leman Russ Battle Tank', 150, gear=[Gear('Battle cannon')])
            self.variant('Leman Russ Exterminator', 150, gear=[Gear('Exterminator autocannon')])
            self.vanq = self.variant('Leman Russ Vanquisher', 155, gear=[Gear('Vanquisher battle cannon')])
            self.variant('Leman Russ Eradicator', 160, gear=[Gear('Eradicator nova cannon')])
            self.variant('Leman Russ Demolisher', 165, gear=[Gear('Demolisher siege cannon')])
            self.variant('Leman Russ Punisher', 180, gear=[Gear('Punisher gatling cannon')])
            self.variant('Leman Russ Executioner', 190, gear=[Gear('Executioner plasma cannon')])
            self.variant('Leman Russ Conqueror', 150, gear=[Gear('Conqueror cannon'), Gear('Storm bolter')])
            self.variant('Leman Russ Annihilator', 130, gear=[Gear('Twin-linked lascannon')])

    class Weapon(OneOf):
        def __init__(self, parent, lascannon=False):
            super(LemanRuss.Weapon, self).__init__(parent=parent, name='Weapon')

            self.heavybolter = self.variant('Heavy bolter', 0)
            self.lascannon = lascannon and self.variant('Lascannon', lascannon)
            self.heavyflamer = self.variant('Heavy flamer', 0)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)

            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Heavy flamers', 20, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Multi-meltas', 30, gear=[Gear('Multimelta', count=2)])
            self.variant('Plasma cannons', 40, gear=[Gear('Plasma cannon', count=2)])

    def __init__(self, parent):
        super(LemanRuss, self).__init__(
            parent=parent, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.type = self.Type(self)
        self.Weapon(self, lascannon=15)
        self.Sponsons(self)
        VehicleOptions(self)

    def build_description(self):
        desc = super(LemanRuss, self).build_description()
        desc.name = self.type.cur.title
        return desc


class LemanRussSquadron(CamoSquadron):
    clear_name = 'Leman Russ Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'tanks_ia_v1'
    unit_class = LemanRuss
    imperial_armour = True


class DestroyerTankHunter(Unit):
    type_name = 'Destroyer Tank Hunter'

    def __init__(self, parent):
        super(DestroyerTankHunter, self).__init__(parent, points=160, gear=[
            Gear('Heavy laser destroyer array'), Gear('Smoke launchers'), Gear('Searchlight')
        ])
        VehicleOptions(self)


class DestroyerSquadron(CamoSquadron):
    clear_name = 'Destroyer Tank Hunter Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'destroyers_ia_v1'
    unit_class = DestroyerTankHunter
    imperial_armour = True


class Thunderer(Unit):
    type_name = 'Thunderer'

    def __init__(self, parent):
        super(Thunderer, self).__init__(parent, points=140, gear=[
            Gear('Demolisher cannon'), Gear('Smoke launchers'), Gear('Searchlight')
        ])
        VehicleOptions(self)


class ThundererSquadron(CamoSquadron):
    clear_name = 'Thunderer Siege Tank Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'thunderers_ia_v1'
    unit_class = Thunderer
    imperial_armour = True


class Ordnance(Unit):
    type_name = 'Ordnance vehicle'

    class Type(OneOf):
        def __init__(self, parent):
            super(Ordnance.Type, self).__init__(parent=parent, name='Type')
            self.variant('Basilisk Artillery Tank', 125,
                         gear=UnitDescription('Basilisk', options=[Gear('Earthshaker cannon')]))
            self.variant('Griffon heavy mortar carrier', 75,
                         gear=UnitDescription('Griffon', options=[Gear('Griffon heavy mortar')]))
            self.medusa = self.variant('Medusa Siege gun', 135,
                                       gear=UnitDescription('Medusa', options=[Gear('Medusa siege cannon')]))
            self.variant('Colossus Bombard', 140,
                         gear=UnitDescription('Colossus', options=[Gear('Colossus siege mortar')]))

    class Options(VehicleOptions):
        def __init__(self, parent, gun_type):
            super(Ordnance.Options, self).__init__(parent, 'Options')
            self.variant('Enclosed crew compartment', 15)
            self.gun_type = gun_type
            self.shells = self.variant('Breacher shells', 5)

        def check_rules(self):
            super(Ordnance.Options, self).check_rules()
            self.shells.used = self.shells.visible = self.gun_type.cur == self.gun_type.medusa

    def __init__(self, parent):
        super(Ordnance, self).__init__(parent, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.gun_type = self.Type(self)
        self.secondary = LemanRuss.Weapon(self)
        self.opt = self.Options(self, self.gun_type)

    def build_description(self):
        result = self.gun_type.cur.gear.clone()
        result.points = self.build_points()
        result.count = self.get_count()
        result.add(self.gear).add(self.secondary.description).add(self.opt.description)
        return result


class OrdnanceBattery(CamoSquadron):
    clear_name = 'Ordnance Battery'
    type_name = clear_name + ia_suffix
    type_id = 'ordnancebat_ia_v1'
    unit_class = Ordnance
    imperial_armour = True


class Hydra(Unit):
    type_name = 'Hydra'

    def __init__(self, parent):
        super(Hydra, self).__init__(parent=parent, points=75, gear=[
            Gear('Twin-linked hydra autocannon', count=2),
            Gear('Searchlight'),
            Gear('Smoke launchers')
        ])
        LemanRuss.Weapon(self)
        VehicleOptions(self)


class HydraBattery(CamoSquadron):
    clear_name = 'Hydra Flak tank battery'
    type_name = clear_name + ia_suffix
    type_id = 'hydras_ia_v1'
    unit_class = Hydra
    netting = 30
    imperial_armour = True


class Manticore(Unit):
    type_name = 'Manticore'

    class Payload(OneOf):
        def __init__(self, parent):
            super(Manticore.Payload, self).__init__(parent, 'Payload')
            self.variant('Four Manticore rockets', gear=[Gear('Manticure rocket', count=4)])
            self.variant('Four Storm Eagle rockets', 15, gear=[Gear('Storm eagle rocket', count=4)])
            self.variant('Four Sky Eagle rockets', gear=[Gear('Sky eagle rocket', count=4)])

    def __init__(self, parent):
        super(Manticore, self).__init__(parent=parent, points=170, gear=[
            Gear('Searchlight'),
            Gear('Smoke launchers')
        ])
        self.Payload(self)
        LemanRuss.Weapon(self)
        VehicleOptions(self)


class ManticoreBattery(CamoSquadron):
    clear_name = 'Manticore battery'
    type_name = clear_name + ia_suffix
    type_id = 'manticoress_ia_v1'
    unit_class = Manticore
    netting = 30
    imperial_armour = True


class HydraPlatform(Unit):
    clear_name = 'Hydra Platform battery'
    type_name = clear_name + ia_suffix
    type_id = 'hydra_platform_ia_v1'
    netting = 20
    launchers = False
    imperial_armour = True

    def __init__(self, parent):
        super(HydraPlatform, self).__init__(parent)
        self.models = Count(self, 'Hydra Platform', 1, 3, 50, True,
                            gear=UnitDescription('Hydra Platform', options=[
                                Gear('Twin-linked Hydra autocannon', count=2)
                            ]))
        self.net = CamoSquadron.SquadronOptions(self)

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return super(HydraPlatform, self).build_points() +\
            self.net.points * (self.models.cur - 1)


class ManticorePlatform(Unit):
    clear_name = 'Manticore Platform battery'
    type_name = clear_name + ia_suffix
    type_id = 'manticore_platform_ia_v1'
    netting = 20
    launchers = False
    imperial_armour = True

    def __init__(self, parent):
        super(ManticorePlatform, self).__init__(parent)
        self.models = Count(self, 'Manticore Platform', 1, 3, 100, True)
        self.sky = Count(self, 'Sky eagle payload', 0, 1,
                         gear=UnitDescription('Manticore Platform', options=[
                             Gear('Sky eagle rocket', count=4)
                         ]))
        self.net = CamoSquadron.SquadronOptions(self)

    def check_rules(self):
        super(ManticorePlatform, self).check_rules()
        self.sky.max = self.models.cur

    def build_description(self):
        res = UnitDescription(self.name, self.build_points(), self.count,
                              options=self.net.description)
        if self.sky.cur > 0:
            res.add(self.sky.description)
        if self.models.cur - self.sky.cur > 0:
            res.add(UnitDescription('Manticore Platform', options=[
                             Gear('Manticore rocket', count=4)
                         ], count=self.models.cur - self.sky.cur))
        return res

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return super(ManticorePlatform, self).build_points() +\
            self.net.points * (self.models.cur - 1)


class Trojan(Unit):
    type_name = 'Trojan support vehicle'
    type_id = 'trojan_ia_v1'

    def __init__(self, parent):
        super(Trojan, self).__init__(parent, 'Trojan', 35,
                                     [Gear('Searchlight'), Gear('Smoke launchers')])
        LemanRuss.Weapon(self)
        VehicleOptions(self, netting=20)


class FieldArtilleryBattery(Unit):
    clear_name = 'Field Artillery Battery'
    type_name = clear_name + ia_suffix
    type_id = 'field_battery_ia_v1'
    imperial_armour = True

    class ArtilleryPiece(ListSubUnit):
        type_name = 'Artillery Piece'

        class Gun(OneOf):
            def __init__(self, parent):
                super(FieldArtilleryBattery.ArtilleryPiece.Gun, self).__init__(parent, 'Gun')
                self.variant('Heavy Quad Launcher')
                self.variant('Heavy mortar')

        def __init__(self, parent):
            super(FieldArtilleryBattery.ArtilleryPiece, self).__init__(parent, points=75 - 6 * 3)
            self.gun = self.Gun(self)
            self.crew = Count(self, 'Crew', 3, 5, 6, True,
                              gear=UnitDescription('Imperial Guard Crew', options=[
                                  Gear('Flak armour'), Gear('Close combat weapon'), Gear('Frag grenades'), Gear('Lasgun')
                              ]))

    class SquadronTrojan(Trojan, ListSubUnit):
        pass

    class SquadronCentaur(Centaur, ListSubUnit):
        pass

    class Towers(OptionsList):
        def __init__(self, parent):
            super(FieldArtilleryBattery.Towers, self).__init__(parent, 'Towing vehicles')
            self.trojans = self.variant('Trojan towing vehicles', gear=[])
            self.centaurs = self.variant('Centaur artillery tractors', gear=[])

    class Options(OptionsList):
        def __init__(self, parent):
            super(FieldArtilleryBattery.Options, self).__init__(parent, 'Battery options')
            self.camo = self.variant('Camo netting', 30, per_model=True)

        @property
        def points(self):
            return super(FieldArtilleryBattery.Options, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(FieldArtilleryBattery, self).__init__(parent)
        self.pieces = UnitList(self, self.ArtilleryPiece, 1, 3)
        self.towers = self.Towers(self)
        self.trojans = UnitList(self, self.SquadronTrojan, 1, 3)
        self.centaurs = UnitList(self, self.SquadronCentaur, 1, 3)
        self.opt = self.Options(self)

    def get_count(self):
        return self.pieces.count

    def check_rules(self):
        super(FieldArtilleryBattery, self).check_rules()
        self.trojans.used = self.trojans.visible = self.towers.trojans.value
        tc = self.towers.trojans.value and self.trojans.count
        self.centaurs.used = self.centaurs.visible = self.towers.centaurs.value
        cc = self.towers.centaurs.value and self.centaurs.count
        if tc + cc > 0:
            if self.get_count() != tc + cc:
                self.error('Number of towing vehicles must be equal to number of artillery pieces')

    def build_statistics(self):
        trojans = self.towers.trojans.value and self.trojans.count
        centaurs = self.towers.centaurs.value and self.centaurs.count
        pieces = sum((1 + p.crew.cur) * p.count for p in self.pieces.units)
        return {'Models': pieces + trojans + centaurs,
                'Units': 1 + trojans + centaurs}


class CarriageBattery(Unit):
    clear_name = 'Heavy Artillery Carriage Battery'
    type_name = clear_name + ia_suffix
    type_id = 'carriage_battery_ia_v1'
    imperial_armour = True

    class ArtilleryPiece(ListSubUnit):
        type_name = 'Heavy Artillery Piece'

        class Gun(OneOf):
            def __init__(self, parent):
                super(CarriageBattery.ArtilleryPiece.Gun, self).__init__(parent, 'Gun')
                self.variant('Earthshaker Cannon')
                self.medusa = self.variant('Medusa Siege Gun', 25)

        def __init__(self, parent):
            super(CarriageBattery.ArtilleryPiece, self).__init__(parent, points=75 - 6 * 4)
            self.gun = self.Gun(self)
            self.crew = Count(self, 'Crew', 4, 8, 6, True,
                              gear=UnitDescription('Imperial Guard Crew', options=[
                                  Gear('Flak armour'), Gear('Close combat weapon'), Gear('Frag grenades'), Gear('Lasgun')
                              ]))

    class SquadronTrojan(Trojan, ListSubUnit):
        pass

    class Towers(OptionsList):
        def __init__(self, parent):
            super(CarriageBattery.Towers, self).__init__(parent, 'Towing vehicles')
            self.trojans = self.variant('Trojan artillery tractors', gear=[])

    class Options(OptionsList):
        def __init__(self, parent):
            super(CarriageBattery.Options, self).__init__(parent, 'Battery options')
            self.camo = self.variant('Camo netting', 30, per_model=True)
            self.shells = self.variant('Bastion Breacher shells', 5, per_model=True)

        def check_rules(self):
            super(CarriageBattery.Options, self).check_rules()
            self.shells.used = self.shells.visible = all(u.gun.cur == u.gun.medusa for u in self.parent.pieces.units)

        @property
        def points(self):
            return super(CarriageBattery.Options, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(CarriageBattery, self).__init__(parent)
        self.pieces = UnitList(self, self.ArtilleryPiece, 1, 3)
        self.towers = self.Towers(self)
        self.trojans = UnitList(self, self.SquadronTrojan, 1, 3)
        self.opt = self.Options(self)

    def get_count(self):
        return self.pieces.count

    def check_rules(self):
        super(CarriageBattery, self).check_rules()
        self.trojans.used = self.trojans.visible = self.towers.any
        if self.towers.any:
            if self.get_count() != self.trojans.count:
                self.error('Number of towing vehicles must be equal to number of artillery pieces')

    def build_statistics(self):
        trojans = self.towers.any and self.trojans.count
        pieces = sum((1 + p.crew.cur) * p.count for p in self.pieces.units)
        return {'Models': pieces + trojans,
                'Units': 1 + trojans}


class EarthshakerPlatform(Unit):
    clear_name = 'Earthshaker Platform battery'
    type_name = clear_name + ia_suffix
    type_id = 'earthshaker_platform_ia_v1'
    netting = 20
    launchers = False
    imperial_armour = True

    def __init__(self, parent):
        super(EarthshakerPlatform, self).__init__(parent)
        self.models = Count(self, 'Earthshaker platform', 1, 3, 75, True,
                            gear=UnitDescription('Earthshaker Platform', options=[
                                Gear('Earthshaker cannon')
                            ]))
        self.net = CamoSquadron.SquadronOptions(self)

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return super(EarthshakerPlatform, self).build_points() +\
            self.net.points * (self.models.cur - 1)


class Powerlifter(Unit):
    type_name = 'Sentinel Powerlifter'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Powerlifter.Options, self).__init__(parent, 'Options')
            self.variant('Dozer blade', 1)

    def __init__(self, parent):
        super(Powerlifter, self).__init__(parent, points=30)
        self.Options(self)


class SentinelSquadron(CamoSquadron):
    clear_name = 'Sentinel Powerlifter Squadron'
    type_name = clear_name + ia_suffix
    type_id = 'powerlifters_ia_v1'
    unit_class = Powerlifter
    launchers = 5
    netting = 10
    imperial_armour = True


class CyclopsSquad(Unit):
    clear_name = 'Cyclops Demolition Squad'
    type_name = clear_name + ia_suffix
    type_id = 'cyclops_ia_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(CyclopsSquad, self).__init__(parent, self.clear_name)
        self.models = Count(self, 'Cyclops Team', 1, 3, 30, True)

    def get_count(self):
        return self.models.cur

    def build_description(self):
        res = UnitDescription(self.name, self.build_points(),
                              self.get_count())
        cyclops = UnitDescription('Cyclops Demolition Vehicle', count=self.models.cur,
                                  options=[Gear('Cyclops Demolition charge')])
        operator = UnitDescription('Guardsman', count=self.models.cur,
                                   options=[Gear('Flak armour'), Gear('Lasgun'), Gear('Close combat weapon')])
        res.add(cyclops).add(operator)
        return res

    def build_statistics(self):
        return {'Models': 2 * self.get_count(), 'Units': 1}


class Hades(Unit):
    clear_name = 'Hades Breaching Drill'
    type_name = clear_name + ia_suffix
    type_id = 'hades_ia_v1'
    imperial_armour = True

    class Veterans(Unit):
        class SergeantWeapon(OneOf):
            def __init__(self, parent, melee=True):
                super(Hades.Veterans.SergeantWeapon, self).__init__(parent, 'Sergeant weapon' if melee else '')
                if melee:
                    self.variant('Close combat weapon')
                else:
                    self.variant('Shotgun')
                self.variant('Laspistol')
                self.variant('Bolt pistol', 2)
                self.variant('Power sword', 10)
                self.variant('Power axe', 10)
                self.variant('Plasma pistol', 10)
                self.variant('Power fist', 15)

        class SpecialWeapon(OneOf):
            def __init__(self, parent):
                super(Hades.Veterans.SpecialWeapon, self).__init__(parent, 'Special weapon')
                self.gun = self.variant('Shotgun')
                self.variant('Flamer', 5)
                self.variant('Grenade launcher', 5)
                self.variant('Meltagun', 10)
                self.variant('Plasma gun', 15)

        class SquadOptions(OptionsList):
            def __init__(self, parent):
                super(Hades.Veterans.SquadOptions, self).__init__(parent, 'Squad options')
                self.vox = self.variant('Vox-caster', 5)
                self.cap = self.variant('Caparace armour', 30)
                self.bombs = self.variant('Melta bombs', 20)

        def get_count(self):
            return 10

        def __init__(self, parent):
            super(Hades.Veterans, self).__init__(parent, 'Hades Assault Squad')
            self.sw1 = self.SergeantWeapon(self)
            self.sw2 = self.SergeantWeapon(self, False)
            self.opt = self.SquadOptions(self)
            self.spec = [self.SpecialWeapon(self) for i in range(0, 2)]

        def build_description(self):
            res = UnitDescription(self.name, self.build_points(), self.get_count())
            common_gear = [Gear('Caparace armour') if self.opt.cap.value else Gear('Flak armour')] + \
                          [Gear('Frag grenades'), Gear('Krak grenades')] + \
                          ([Gear('Meltabombs')] if self.opt.bombs.value else [])
            sarge = UnitDescription('Veteran Sergeant', options=self.sw1.description +
                                    self.sw2.description + common_gear)
            res.add(sarge)
            common_gear = [Gear('Close combat weapon')] + common_gear
            for wep in self.spec:
                res.add_dup(UnitDescription('Veteran', options=wep.description + common_gear))
            common_gear = [Gear('Shotgun')] + common_gear
            res.add_dup(UnitDescription('Veteran', count=7 - self.opt.vox.value, options=common_gear))
            if self.opt.vox.value:
                res.add(UnitDescription('Veteran', options=common_gear + [Gear('Vox caster')]))
            return res

    def __init__(self, parent):
        super(Hades, self).__init__(parent, self.clear_name, 100,
                                    [UnitDescription('Hades Drill')])
        SubUnit(self, self.Veterans(parent=self))

    def build_statistics(self):
        return {'Models': 11, 'Units': 2}


class Sentry(Unit):
    type_name = 'Sentry Gun'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Sentry.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked heavy bolters')
            self.variant('Twin-linked lascannon', 10)

    def __init__(self, parent):
        super(Sentry, self).__init__(parent, points=15)
        self.Weapon(self)


class SentryBattery(CamoSquadron):
    clear_name = 'Sentry Gun Battery'
    type_name = clear_name + ia_suffix
    type_id = 'tarantulas_ia_v1'
    unit_class = Sentry
    netting = 10
    imperial_armour = True


class Rapiers(Unit):
    clear_name = 'Rapier Laser Destroyer'
    type_name = clear_name + ia_suffix
    type_id = 'rapiers_ia_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Rapiers, self).__init__(parent, self.clear_name)
        self.guns = Count(self, 'Rapier', 1, 3, 40, True)
        self.crew = Count(self, 'Additional crew', 0, 1, 6, True)

    def check_rules(self):
        super(Rapiers, self).check_rules()
        self.crew.max = self.guns.cur

    def build_description(self):
        res = UnitDescription(self.name, self.build_points())
        rap = UnitDescription('Rapier', points=40, options=[Gear('Laser destroyer array')])
        crew = UnitDescription('Imperial Guard Crew', options=[Gear('Flak armour'),
                                                               Gear('Close combat weapon'),
                                                               Gear('Frag grenades'),
                                                               Gear('Lasgun')])
        if self.guns.cur > self.crew.cur:
            res.add(rap.clone().add(crew).set_count(self.guns.cur - self.crew.cur))
        if self.crew.cur > 0:
            crew.set_count(2)
            res.add(rap.clone().add_points(6).add(crew).set_count(self.crew.cur))
        return res

    def get_count(self):
        return self.guns.cur

    def build_statistics(self):
        return {'Models': 2 * self.guns.cur + self.crew.cur, 'Units': 1}
