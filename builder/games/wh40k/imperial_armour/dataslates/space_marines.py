from builder.core2 import OneOf, Gear, OptionsList
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedDreadnought,\
    DreadnoughtTransport, Unit


class Deredeo(IATransportedDreadnought):
    type_name = 'Deredeo Pattern Dreadnought (Dataslate)'
    type_id = 'deredeo_v1'
    imperial_armour = True

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(Deredeo.MainWeapon, self).__init__(parent, 'Main weapon')
            self.variant('Twin-linked Anvilus pattern autocannon battery')
            self.variant('Twin-linked hellfire plasma canonnade', 20)

    class TorsoWeapon(OneOf):
        def __init__(self, parent):
            super(Deredeo.TorsoWeapon, self).__init__(parent, 'Torso weapon')
            self.variant('Twin-linked heavy bolter')
            self.variant('Twin-linked heavy flamer')

    class Options(OptionsList):
        def __init__(self, parent):
            super(Deredeo.Options, self).__init__(parent, 'Options')
            self.variant('Armoured ceramite', 20)
            self.variant('Alolos missile launcher', 35)

    def __init__(self, parent):
        super(Deredeo, self).__init__(parent, 'Deredeo Dreadnought', 185, gear=[
            Gear('Smoke launcher'), Gear('Searchlight'), Gear('Extra armour')
        ], walker=True)
        self.MainWeapon(self)
        self.TorsoWeapon(self)
        self.Options(self)
        self.transport = DreadnoughtTransport(self)

    def is_relic(self):
        return True


class Xiphon(Unit):
    type_name = 'Xiphon pattern Interceptor'
    type_id = 'xiphon_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Xiphon.Options, self).__init__(parent, 'Options')
            self.variant('Chaff launcher', 5)
            self.variant('Armoured cockpit', 5)

    def __init__(self, parent):
        super(Xiphon, self).__init__(parent, points=205, gear=[
            Gear('Twin-linked lascannon', count=2),
            Gear('Xiphon rotary missile launcher'),
            Gear('Armoured ceramite')
        ])
        self.Options(self)


class BaseUnit(Unit):
    def __init__(self, *args, **kwargs):
        self.tank = kwargs.pop('tank', False)
        super(BaseUnit, self).__init__(*args, **kwargs)


class BaseDeimos(BaseUnit):
    type_name = 'Deimos Pattern Vindicator Tank Destroyer'
    type_id = 'deimos_vindicator_v1'
    imperial_armour = True

    def get_options(self):
        pass

    def __init__(self, parent):
        super(BaseDeimos, self).__init__(parent, 'Deimos Vindicator', 130,
                                         [Gear('Laser Destroyer array'),
                                          Gear('Smoke launchers'),
                                          Gear('Searchlight'),
                                          Gear('Power capacitor')], tank=True)
        self.get_options()(self)


class Leviathan(Unit):
    type_name = 'Relic Leviathan Dreadnought'
    type_id = 'leviathan_v1'

    class Arm(OneOf):
        def __init__(self, parent):
            super(Leviathan.Arm, self).__init__(parent, 'Weapon')
            self.variant('Leviathan siege claw', gear=[Gear('Leviathan siege claw'),
                                                       Gear('Meltagun')])
            self.variant('Leviathan siege drill', 5, gear=[Gear('Leviathan siege drill'),
                                                            Gear('Meltagun')])
            self.variant('Leviathan storm cannon', 20)
            self.variant('Cyclonic melta lance', 20)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Leviathan.Options, self).__init__(parent, 'Options')
            self.variant('Ceramite plating', 20)
            self.variant('Three hunter-killer missiles', 15,
                         gear=[Gear('Hunter-likker missile', count=3)])

    def __init__(self, parent):
        super(Leviathan, self).__init__(parent, 'Leviathan Siege Dreadnought', 285, [
            Gear('Heavy flamer', count=2), Gear('Smoke launcher'),
            Gear('Searchlight'), Gear('Frag grenades'), Gear('Extra armour')
        ])
        self.Arm(self)
        self.Arm(self)
        self.Options(self)
        self.transport = DreadnoughtTransport(self)

    def build_statistics(self):
        return self.note_transport(super(Leviathan, self).build_statistics())

    def is_relic(self):
        return True
