from .options import SpaceMarinesBaseVehicle
from builder.core2 import *
from builder.games.wh40k.roster import Unit

__author__ = 'dante'


class LuciusDropPod(SpaceMarinesBaseVehicle):
    type_name = 'Lucius Pattern Dreadnought Drop Pod (IA vol.2)'
    type_id = 'LuciusDropPod_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(LuciusDropPod, self).__init__(parent=parent, name='Lucius Dreadnought Drop Pod', points=50,
                                            deep_strike=True, transport=True)


class DreadnoughtTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(DreadnoughtTransport, self).__init__(parent=parent, name='Transport', limit=1)
        self.lucius = SubUnit(self, LuciusDropPod(parent=None))

    def get_unique_gear(self):
        return self.lucius.unit.get_unique_gear()

    def count_glory_legacies(self):
        return self.lucius.unit.count_glory_legacies()


class DamoclesCommandRhino(SpaceMarinesBaseVehicle):
    type_id = 'damocles_rhino_v1'
    type_name = "Damocles Command Rhino (IA vol.2)"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(DamoclesCommandRhino.Options, self).__init__(parent, 'Options')
            self.hkm = self.variant('Searchlight', 1)
            self.exarm = self.variant('Extra armour', 10)
            self.sbgun = self.variant('Storm bolter', 5)
            self.hkm = self.variant('Hunter-killer missile', 10)
            self.dblade = self.variant('Dozer blade', 5)

    def __init__(self, parent):
        super(DamoclesCommandRhino, self).__init__(name="Damocles Rhino", parent=parent, points=75, gear=[
            Gear('Storm-bolter'),
            Gear('Teleport beacon'),
            Gear('Command vox relay'),
        ], tank=True, transport=True)
        self.Options(self)


class InfernumRazorback(SpaceMarinesBaseVehicle):
    type_id = 'infernum_razorback_v1'
    type_name = "Infernum Pattern Razorback (IA vol.2)"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(InfernumRazorback.Options, self).__init__(parent, 'Options')
            self.dblade = self.variant('Dozer blade', 5)
            self.sbgun = self.variant('Storm bolter', 5)
            self.hkm = self.variant('Hunter-killer missile', 10)
            self.exarm = self.variant('Extra armour', 10)

    def __init__(self, parent):
        super(InfernumRazorback, self).__init__(name="Infernum Razorback", parent=parent, points=65, gear=[
            Gear('Multi-melta'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ], tank=True, transport=True)
        self.Options(self)


class IATransport(OptionalSubUnit):
    def __init__(self, parent, name):
        super(IATransport, self).__init__(parent=parent, name=name, limit=1)
        self.models = []
        self.ia_models = []

    def get_unique_gear(self):
        if not self.used:
            return []
        return sum((u.unit.get_unique_gear() for u in self.models + self.ia_models if u and u.used), [])

    def count_glory_legacies(self):
        return sum((u.unit.count_glory_legacies() for u in self.models + self.ia_models if u and u.used))

    def check_rules(self):
        self.options.set_visible(self.ia_models, self.roster.ia_enabled)


class IATransportedUnit(Unit):
    def __init__(self, *args, **kwargs):
        super(IATransportedUnit, self).__init__(*args, **kwargs)
        self.transport = None

    def get_unique_gear(self):
        default = super(IATransportedUnit, self).get_unique_gear()
        if not self.transport:
            return default
        return self.transport.get_unique_gear() +\
            ([] if default is None else default)

    def count_glory_legacies(self):
        return self.transport.count_glory_legacies() if self.transport is not None else 0

    def has_transport(self):
        return self.transport.any if self.transport is not None else False

    def build_statistics(self):
        return self.note_transport(super(IATransportedUnit, self).build_statistics())


class IATransportedDreadnought(SpaceMarinesBaseVehicle):
    def __init__(self, *args, **kwargs):
        super(IATransportedDreadnought, self).__init__(*args, **kwargs)
        self.transport = None

    def get_unique_gear(self):
        return super(IATransportedDreadnought, self).get_unique_gear() +\
            (self.transport.get_unique_gear() if self.transport is not None else [])

    def count_glory_legacies(self):
        return super(IATransportedDreadnought, self).count_glory_legacies() +\
            (self.transport.count_glory_legacies() if self.transport is not None else 0)

    def build_statistics(self):
        return self.note_transport(super(IATransportedDreadnought, self).build_statistics())
