__author__ = 'Ivan Truskov'


from builder.core2 import OptionsList, OneOf, Gear,\
    Count, UnitList, ListSubUnit
from builder.games.wh40k.roster import Unit


class FightaBommer(Unit):
    type_name = 'Fighta-bommer'
    type_id = 'fighta_bommer_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(FightaBommer, self).__init__(parent, 'Fighta-bommer', points=170, gear=[
            Gear('Twin-linked big shoota', count=5)])
        self.payload = [
            Count(self, 'Bomm', 0, 6, 0),
            Count(self, 'Grot Bomm', 0, 6, 15)
        ]

    def check_rules(self):
        Count.norm_counts(0, 6, self.payload)

    def build_description(self):
        desc = super(FightaBommer, self).build_description()
        rokkits = 6 - sum(c.cur for c in self.payload)
        if rokkits > 0:
            desc.add(Gear('Rokkit', count=rokkits))
        return desc


class FlakkTrukks(Unit):
    type_name = 'Flakk Trukk'
    type_id = 'flakk_trukk_v1'
    imperial_armour = True

    class FlakkTrukk(ListSubUnit):
        class SecondWeapon(OneOf):
            def __init__(self, parent):
                super(FlakkTrukks.FlakkTrukk.SecondWeapon, self).__init__(parent, 'Secondary weapon')
                self.variant('Big shoota', 0)
                self.variant('Rokkit launcha', 5)

        class Equipment(OptionsList):
            def __init__(self, parent):
                super(FlakkTrukks.FlakkTrukk.Equipment, self).__init__(parent, 'Options')
                self.variant('Red paint job', 5)
                self.variant('Stikkbomb chukka', 5)
                self.variant('Grot riggers', 5)
                self.variant('Wreckin\' ball', 10)
                self.ram = self.variant('Reinforced ram', 5)

        def __init__(self, parent):
            super(FlakkTrukks.FlakkTrukk, self).__init__(parent, 'Flakk Trukk', 75, [Gear('Flakka-gunz')])
            self.SecondWeapon(self)
            self.Equipment(self)

    def __init__(self, parent):
        super(FlakkTrukks, self).__init__(parent)
        self.models = UnitList(self, self.FlakkTrukk, 1, 3)

    def get_count(self):
        return self.models.count


class AttakFightas(Unit):
    type_name = 'Attack Fighta'
    type_id = 'attack_fighta_v1'
    imperial_armour = True

    class AttakFighta(Unit):
        class Missiles(OneOf):
            def __init__(self, parent):
                super(AttakFightas.AttakFighta.Missiles, self).__init__(
                    parent, 'Missile weapons')
                self.variant('Two bomms', 0, gear=[Gear('Bomms', count=2)])
                self.variant('Two rokkits', 0, gear=[Gear('Rokkit', count=2)])

        def __init__(self, parent):
            super(AttakFightas.AttakFighta, self).__init__(
                parent, 'Attack Fighta', 95,
                gear=[Gear('Twin-linked big shoota', count=2)])

    class SkwadronFighta(ListSubUnit, AttakFighta):
        pass

    def __init__(self, parent):
        super(AttakFightas, self).__init__(parent, 'Attack Fighta')
        self.models = UnitList(self, self.SkwadronFighta, 1, 3)

    def get_count(self):
        return self.models.count


class FlakkTrakk(Unit):
    type_name = 'Flakk Trakk'
    type_id = 'flakk_trakk_v1'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(FlakkTrakk.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Big shoota', 0)
            self.variant('Skorcha', 5)
            self.variant('Rokkit Launcha', 10)

    class Equipment1(OptionsList):
        def __init__(self, parent):
            super(FlakkTrakk.Equipment, self).__init__(parent, 'Options')
            self.variant('Boarding plank', 5)
            self.variant('\'Ard case', 10)
            self.variant('Stikkbomb chukka', 5)
            self.variant('Red paint job', 5)
            self.variant('Grot riggers', 5)

    class Equipment2(OptionsList):
        def __init__(self, parent):
            super(FlakkTrakk.Equipment2, self).__init__(parent, '', limit=1)
            self.variant('Reinforced ram', 10)
            self.variant('Deff rolla', 10)
            self.variant('Wreckin\' ball', 10)
            self.variant('Grabbin\' klaw', 10)

    def __init__(self, parent):
        super(FlakkTrakk, self).__init__(parent, 'Flakk Trakk',
                                         points=90, gear=[Gear('Flakka-gunz')])
        [self.Weapon(self) for r in range(0, 2)]
        self.pintle_mounted = [
            Count(self, 'Big shoota', 0, 2, 5),
            Count(self, 'Skorcha', 0, 2, 10),
            Count(self, 'Rokkit launcha', 0, 2, 15)
        ]
        self.Equipment1(self)
        self.Equipment2(self)
        Count(self, 'Grot sponson', 0, 2, 5)

    def check_rules(self):
        Count.norm_counts(0, 2, self.pintle_mounted)
