__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf, ListSubUnit, UnitList,\
    OptionsList, Count, UnitDescription
from builder.games.wh40k.unit import Unit

ia_suffix = ' (IA vol.4)'


class ScythedHierodule(Unit):
    clear_name = 'Scythed Hierodule'
    type_name = clear_name + ia_suffix
    type_id = 'sc_hierodule_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(ScythedHierodule, self).__init__(parent, self.clear_name, 535,
                                               [Gear('Scything talons', count=2),
                                                Gear('Bio-acid spray')], static=True)


class MalanthropeBrood(Unit):
    clear_name = 'Malanthrope Brood'
    type_name = clear_name + ia_suffix
    type_id = 'malanthrope_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(MalanthropeBrood, self).__init__(parent, self.clear_name)
        self.models = Count(self, 'Malanthropes', 1, 3, 85, True,
                            gear=UnitDescription('Malanthrope', options=[
                                Gear('Toxic miasma'),
                                Gear('Grasping tail'),
                                Gear('Regeneration')
                            ]))

    def get_count(self):
        return self.models.cur


class Dimachaeron(Unit):
    clear_name = 'Dimachaeron'
    type_name = clear_name + ia_suffix
    type_id = 'dimachaeron_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Dimachaeron, self).__init__(parent, self.clear_name, 200,
                                               [Gear('Sickle claws'),
                                                Gear('Grasping talons'),
                                                Gear('Thorax spine-maw'),
                                                Gear('Adrenal glands')], static=True)


class MeioticSpores(Unit):
    clear_name = 'Meiotic Spore Brood'
    type_name = clear_name + ia_suffix
    type_id = 'maiotic_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(MeioticSpores, self).__init__(parent, self.clear_name)
        self.models = Count(self, 'Meiotic Spores', 3, 9, 15, True,
                            gear=UnitDescription('Meiotic Spore', 15))

    def get_count(self):
        return self.models.cur


class SCCarnifexBrood(Unit):
    clear_name = 'Stone Crusher Carnifex Brood'
    type_name = clear_name + ia_suffix
    type_id = 'sc_carnifex_brood_v1'
    imperial_armour = True

    class Warrior(ListSubUnit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(SCCarnifexBrood.Warrior.Options, self).__init__(parent, name='Options')
                self.variant('Spine banks', 5, per_model=True)
                self.variant('Bio-plasma', 20, per_model=True)

        class Tail(OptionsList):
            def __init__(self, parent):
                super(SCCarnifexBrood.Warrior.Tail, self).__init__(parent, name='Tail biomorphs', limit=1)
                self.variant('Thresher scythe', 10, per_model=True)
                self.variant('Bone mace', 15, per_model=True)

        class Claws(OneOf):
            def __init__(self, parent):
                super(SCCarnifexBrood.Warrior.Claws, self).__init__(parent, name='Weapon')
                self.variant('Wrecker claws', gear=[Gear('Wrecker claw', count=2)])
                self.variant('Wrecker claw and bio-flail', 15,
                             gear=[Gear('Wrecker claw'), Gear('Bio-flail')])

        def __init__(self, parent):
            super(SCCarnifexBrood.Warrior, self).__init__(parent,
                                                          name='Stone Crusher Carnifex',
                                                          points=150,
                                                          gear=[Gear('Caparace chitin-rams')])

            self.Claws(self)
            self.Tail(self)
            self.Options(self)

    def __init__(self, parent):
        super(SCCarnifexBrood, self).__init__(parent=parent, name=self.clear_name)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count
