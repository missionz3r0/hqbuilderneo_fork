__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'
__summary__ = 'Codes Imperial Knights 7th edition'

from builder.core2 import UnitType
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, Formation,\
    LordsOfWarSection, PrimaryDetachment, Wh40kImperial
from .lords import Paladin, Errant, Warden, Crusader, Gallant, Gerantius
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusIKSection


class BaseLords(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLords, self).__init__(parent)
        UnitType(self, Paladin)
        UnitType(self, Errant)
        UnitType(self, Warden)
        UnitType(self, Crusader)
        UnitType(self, Gallant)
        UnitType(self, Gerantius)


class Lords(CerastusIKSection, BaseLords):
    pass


class ALance(Formation):
    army_id = 'adamantine_lance_v2'
    army_name = 'Adamantine Lance'

    def __init__(self, parent=None):
        super(ALance, self).__init__(parent)
        paladin = UnitType(self, Paladin)
        errant = UnitType(self, Errant)
        self.add_type_restriction([paladin, errant], 3, 3)

    def allow_relics(self):
        return False


class ExaltedCourt(Formation):
    army_id = 'imperial_knight_exalted'
    army_name = 'Exalted Court'

    def __init__(self, parent=None):
        super(ExaltedCourt, self).__init__(parent)
        paladin = UnitType(self, Paladin)
        errant = UnitType(self, Errant)
        warden = UnitType(self, Warden)
        gallant = UnitType(self, Gallant)
        crusader = UnitType(self, Crusader)
        self.add_type_restriction([paladin, errant, warden, gallant, crusader], 5, 5)

    def allow_relics(self):
        return True


class BaronialCourt(Formation):
    army_id = 'imperial_knight_baronial'
    army_name = 'Baronial Court'

    def __init__(self, parent=None):
        super(BaronialCourt, self).__init__(parent)
        paladin = UnitType(self, Paladin)
        errant = UnitType(self, Errant)
        warden = UnitType(self, Warden)
        gallant = UnitType(self, Gallant)
        crusader = UnitType(self, Crusader)
        self.add_type_restriction([paladin, errant, warden, gallant, crusader], 3, 5)

    def allow_relics(self):
        return True

    def check_rules(self):
        super(BaronialCourt, self).check_rules()
        if sum([len(u.get_unique_gear()) > 0 for u in self.units]) > 1:
            self.error('Only Baron may carry a Heirloom of the Knightly House')


class TripartiteLance(Formation):
    army_id = 'imperial_knight_tripartite'
    army_name = 'Tripartite Lance'

    def __init__(self, parent=None):
        super(TripartiteLance, self).__init__(parent)
        UnitType(self, Warden, min_limit=1, max_limit=1)
        UnitType(self, Gallant, min_limit=1, max_limit=1)
        UnitType(self, Crusader, min_limit=1, max_limit=1)

    def allow_relics(self):
        return False


class GallantLance(Formation):
    army_id = 'imperial_knight_gallant'
    army_name = 'Gallant Lance'

    def __init__(self, parent=None):
        super(GallantLance, self).__init__(parent)
        UnitType(self, Gallant, min_limit=3, max_limit=3)

    def allow_relics(self):
        return False


class SkyreaperLance(Formation):
    army_id = 'imperial_knight_skureaper'
    army_name = 'Skyreaper Lance'

    def __init__(self, parent=None):
        super(SkyreaperLance, self).__init__(parent)
        paladin = UnitType(self, Paladin)
        errant = UnitType(self, Errant)
        warden = UnitType(self, Warden)
        gallant = UnitType(self, Gallant)
        crusader = UnitType(self, Crusader)
        self.add_type_restriction([paladin, errant, warden, gallant, crusader], 3, 3)

    def allow_relics(self):
        return False

    def check_rules(self):
        super(SkyreaperLance, self).check_rules()
        if not all([u.cap.icarus.value for u in self.units]):
            self.error('All Knights must take twin Icarus autocannon')


class Household(Wh40kBase):
    army_id = 'imperial_knights_houselold'
    army_name = 'Household Detachment'

    def __init__(self):
        self.knights = Lords(parent=self)
        self.knights.min, self.knights.max = (3, 5)
        super(Household, self).__init__(
            lords=[self.knights]
        )

    def allow_relics(self):
        return type(self.parent.parent) is PrimaryDetachment

    def check_rules(self):
        super(Household, self).check_rules()
        if sum([len(u.get_unique_gear()) > 0 for u in self.knights.units if u.get_unique_gear() is not None]) > 1:
            self.error('Only Warlord may carry a Heirloom of the Knightly House')


class Oathsworn(Wh40kBase):
    army_id = 'imperial_knights_oathsworn'
    army_name = 'Oathsworn detachment'

    def __init__(self):
        self.knights = Lords(parent=self)
        self.knights.min, self.knights.max = (1, 3)
        super(Oathsworn, self).__init__(
            lords=[self.knights]
        )

    def allow_relics(self):
        return False


faction = 'Imperial Knights'


class ImperialKnightsV2(Wh40kImperial, Wh40k7ed):
    army_id = 'imperial_knights_v2'
    army_name = 'Imperial Knights'
    faction = faction
    # development = True

    def __init__(self):
        super(ImperialKnightsV2, self).__init__([
            Household,
            ALance,
            ExaltedCourt,
            BaronialCourt,
            TripartiteLance,
            GallantLance,
            SkyreaperLance
        ], [Oathsworn])


detachments = [
    Household,
    Oathsworn,
    ALance,
    ExaltedCourt,
    BaronialCourt,
    TripartiteLance,
    GallantLance,
    SkyreaperLance
]


unit_types = [
    Paladin, Errant, Warden, Crusader, Gallant, Gerantius
]
