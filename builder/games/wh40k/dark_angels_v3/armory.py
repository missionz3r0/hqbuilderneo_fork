__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianRelicsHoly, CadianWeaponHoly,\
    CadianWeaponArcana


class Vehicle(OptionsList):
    def __init__(self, parent, melta=False, shield=False, raider=False):
        super(Vehicle, self).__init__(parent, 'Options')
        self.sbgun = self.Variant(self, 'Storm bolter', 5)
        if not raider:
            self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
        self.exarm = self.Variant(self, 'Extra armour', 10)
        if melta:
            self.melta = self.Variant(self, 'Multi melta', 10)
        if shield:
            self.shield = self.Variant(self, 'Siege shield', 10)


class Weapon(OneOf):
    def __init__(self, parent, name, armour=None, melee=False, ranged=False, tda_ranged=False, tda_melee=False,
                 relic=False, pistol=False, bolt_gun=False, force=False, crozius=False, relic_blade=False, ccw=False):
        super(Weapon, self).__init__(parent, name)

        self.armour = armour
        self.pwr_weapon = []
        self.tda_weapon = []
        self.relic_weapon = []

        if pistol:
            self.pistol = self.variant('Bolt pistol', 0)
            self.pwr_weapon += [self.pistol]

        if bolt_gun:
            self.boltgun = self.variant('Boltgun', 0)
            self.pwr_weapon += [self.boltgun]

        if ccw:
            self.csw = self.variant('Chainsword', 0)

        if force:
            self.force = self.variant('Force weapon', 0)

        if crozius:
            self.roz = self.variant('Crozius Arcanum', 0)

        if melee:
            if not ccw:
                self.csw = self.variant('Chainsword', 0)
            self.claw = self.variant('Lightning claw', 15)
            self.pwr = self.variant('Power weapon', 15)
            self.fist = self.variant('Power fist', 25)
            self.hummer = self.variant('Thunder hammer', 30)
            self.pwr_weapon += [self.csw, self.claw, self.pwr, self.fist, self.hummer]

        if relic_blade:
            self.rblade = self.variant('Relic Blade', 25)
            self.pwr_weapon += [self.rblade]

        if ranged:
            if not bolt_gun:
                self.boltgun = self.variant('Boltgun', 0)
            self.storm_bolter = self.variant('Storm Bolter', 5)
            self.combi_melta = self.variant('Combi-melta', 10)
            self.combi_flamer = self.variant('Combi-flamer', 10)
            self.combi_plasma = self.variant('Combi-plasma', 10)
            self.combi_grav = self.variant('Combi-grav', 10)
            self.plasma_pistol = self.variant('Plasma pistol', 15)
            self.grav_pistol = self.variant('Grav pistol', 15)
            self.pwr_weapon += [self.boltgun, self.storm_bolter, self.combi_plasma, self.combi_melta, self.combi_flamer,
                                self.plasma_pistol, self.combi_grav, self.grav_pistol]

        if tda_ranged:
            self.tda_storm_bolter = self.variant('Storm Bolter', 0)
            self.tda_combi_melta = self.variant('Combi-melta', 5)
            self.tda_combi_flamer = self.variant('Combi-flamer', 5)
            self.tda_combi_plasma = self.variant('Combi-plasma', 5)
            self.tda_claw1 = self.variant('Lightning claw', 10)
            self.tda_hummer1 = self.variant('Thunder hammer', 30)
            self.tda_weapon += [self.tda_storm_bolter, self.tda_combi_flamer, self.tda_combi_melta,
                                self.tda_combi_plasma, self.tda_claw1, self.tda_hummer1]

        if tda_melee:
            self.tda_pwr = self.variant('Power sword', 0)
            self.tda_claw2 = self.variant('Lightning claw', 5)
            self.tda_ss2 = self.variant('Storm shield', 5)
            self.tda_pf2 = self.variant('Power fist', 10)
            self.tda_cf2 = self.variant('Chainfist', 15)
            self.tda_hummer2 = self.variant('Thunder hammer', 15)
            self.tda_weapon += [self.tda_pwr, self.tda_claw2, self.tda_ss2, self.tda_pf2,
                                self.tda_cf2, self.tda_hummer2]

        if relic_blade:
            self.tda_rblade = self.variant('Relic Blade', 10)
            self.tda_weapon += [self.tda_rblade]

        if relic:
            self.fs = self.variant('Foe-smiter', 15)
            self.lr = self.variant('Lion\'s Roar', 20)
            self.mor = self.variant('Mace or Redemption', 30)
            self.msoc = self.variant('Monster Slayer of Caliban', 40)
            self.relic_weapon = [self.fs, self.lr, self.mor, self.msoc]

    def check_rules(self):
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.used and self.cur in self.relic_weapon:
            return self.description
        return []


class HolyWeapon(CadianWeaponHoly, Weapon):
    def __init__(self, *args, **kwargs):
        super(HolyWeapon, self).__init__(*args, **kwargs)
        self.relic_weapon += [self.worthy_blade]


class ArcanaWeapon(CadianWeaponArcana, Weapon):
    def __init__(self, *args, **kwargs):
        super(ArcanaWeapon, self).__init__(*args, **kwargs)
        self.relic_weapon += [self.qann]


class Relics(OptionsList):
    def __init__(self, parent):
        super(Relics, self).__init__(parent, 'Relics of Caliban', limit=1)
        self.variant('Shroud of Heroes', 10)
        self.variant('The Eye of the Unseen', 40)

    def get_unique(self):
        return self.description


class HolyRelics(CadianRelicsHoly, Relics):
    pass


class Heavy(Weapon):

    class Flakk(OptionsList):
        def __init__(self, parent):
            super(Heavy.Flakk, self).__init__(parent=parent, name='', limit=None)
            self.flakkmissiles = self.variant('Flakk missiles', 10)

    def __init__(self, parent, *args, **kwargs):
        super(Heavy, self).__init__(parent, *args, **kwargs)
        self.heavybolter = self.variant('Heavy bolter', 10)
        self.multimelta = self.variant('Multi-melta', 10)
        self.missilelauncher = self.variant('Missile launcher', 15)
        self.plasmacannon = self.variant('Plasma cannon', 15)
        self.lascannon = self.variant('Lascannon', 20)
        self.grav = self.variant('Grav-cannon and grav-amp', 35,
                                 gear=[Gear('Grav-cannon'), Gear('Grav-amp')])
        self.heavy = [self.heavybolter, self.missilelauncher,
                      self.multimelta, self.plasmacannon, self.grav,
                      self.lascannon]
        self.flakk = self.Flakk(parent=parent)

    def is_heavy(self):
        return self.cur in self.heavy

    def check_rules(self):
        super(Heavy, self).check_rules()
        self.flakk.visible = self.flakk.used = self.cur == self.missilelauncher

    @property
    def description(self):
        desc = super(Heavy, self).description
        desc += self.flakk.description
        return desc


class TerminatorHeavy(Weapon):

    class Cyclone(OptionsList):
        def __init__(self, parent):
            super(TerminatorHeavy.Cyclone, self).__init__(parent=parent, name='', limit=None)
            self.launcher = self.variant('Cyclone missile launcher', 25)

    def __init__(self, parent, *args, **kwargs):
        super(TerminatorHeavy, self).__init__(parent, *args, **kwargs)
        self.heavyflamer = self.variant('Heavy flamer', 10)
        self.plasmacannon = self.variant('Plasma cannon', 15)
        self.asscannon = self.variant('Assault cammom', 20)
        self.tda_heavy = [self.heavyflamer, self.plasmacannon,
                          self.asscannon]
        self.cyclone = self.Cyclone(parent=parent)

    def is_heavy(self):
        return self.cur in self.tda_heavy

    def is_tda_heavy(self):
        return self.is_heavy() or (self.cyclone.used and self.cyclone.any)

    def check_rules(self):
        super(TerminatorHeavy, self).check_rules()
        self.cyclone.visible = self.cyclone.used = not self.is_heavy()


class Special(Weapon):

    def __init__(self, parent, *args, **kwargs):
        super(Special, self).__init__(parent, *args, **kwargs)
        self.flamer = self.variant('Flamer', 5)
        self.melta = self.variant('Meltagun', 10)
        self.gravgun = self.variant('Grav-gun', 15)
        self.plasmagun = self.variant('Plasma gun', 15)
        self.special = [self.flamer, self.melta, self.gravgun, self.plasmagun]

    def is_special(self):
        return self.cur in self.special


class SpecialIssue(OptionsList):
    def __init__(self, parent, armour=None, bike=False, jump=False, storm_shield=False):
        super(SpecialIssue, self).__init__(parent, 'Special issue wargear')
        self.armour = armour
        self.bike_option = bike
        self.ride = []

        self.ax = self.variant('Auspex', 5)
        self.csh = self.variant('Combat shield', 5)
        # self.iv = self.variant('Infravisor', 5)
        self.mbomb = self.variant('Melta bombs', 5)
        self.dig = self.variant('Digital weapons', 10)
        # self.pr = self.variant('Porta-rack', 10)
        self.cf = self.variant('Conversion field', 20)
        if jump:
            self.jpack = self.variant('Jump pack', 15)
            self.ride += [self.jpack]
        if bike:
            self.bike = self.variant('Space Marine Bike', 20)
            self.ride += [self.bike]
        # self.df = self.variant('Displacer field', 25)
        # self.cfg = self.variant('Power field generator', 30)
        self.ss = storm_shield and self.variant('Storm Shield', 15)

    def check_rules(self):
        if self.armour:
            for opt in self.ride:
                opt.visible = opt.used = not self.armour.is_tda()
        self.process_limit(self.ride, 1)

    def has_bike(self):
        return self.bike_option and self.bike.value


class Standards(OptionsList):
    def __init__(self, parent):
        super(Standards, self).__init__(parent, 'Dark Angels Standards', limit=1)
        self.variant('Company standard', 15)
        self.banner = self.variant('Chapter banner', 25)
        self.variant('Sacred standard', 35)

    def get_unique(self):
        if self.banner.value:
            return self.description
        else:
            return []


class Armour(OneOf):
    def __init__(self, parent, art=0, tda=40):
        super(Armour, self).__init__(parent, 'Armour')
        self.pwr = self.variant('Power armour', 0,
                                gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        if art:
            self.art = self.variant('Artificer armour', art,
                                    gear=[Gear('Artificer armour'), Gear('Frag grenades'), Gear('Krak grenades'), ])
        self.tda = self.variant('Terminator armour', tda)

    def is_tda(self):
        return self.cur == self.tda
