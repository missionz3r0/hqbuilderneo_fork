__author__ = 'dante'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import *
from .armory import Vehicle
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import InfernumRazorback, IATransport


class Transport(IATransport):
    def __init__(self, parent):
        super(Transport, self).__init__(parent=parent, name='Transport')
        self.rhino = SubUnit(self, Rhino(parent=parent))
        self.razor = SubUnit(self, Razorback(parent=parent))
        self.drop = SubUnit(self, DropPod(parent=parent))
        self.infernum = SubUnit(self, InfernumRazorback(parent=None))
        self.models += [self.rhino, self.razor, self.drop]
        self.ia_models += [self.infernum]


class TerminatorTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(TerminatorTransport, self).__init__(parent=parent, name='Transport', limit=1)
        self.lr = SubUnit(self, LandRaider(parent=None))
        self.lrc = SubUnit(self, LandRaiderCrusader(parent=None))
        self.lrr = SubUnit(self, LandRaiderRedeemer(parent=None))

    def get_unique_gear(self):
        return sum((u.unit.get_unique_gear() for u in [self.lrc, self.lr, self.lrr] if u), [])

    def count_glory_legacies(self):
        return sum(u.unit.count_glory_legacies() for u in [self.lr, self.lrc, self.lrr])


class DiscountedTransport(SpaceMarinesBaseVehicle):
    model_points = 0

    def build_points(self):
        res = super(DiscountedTransport, self).build_points()
        if hasattr(self.roster, 'discount') and self.roster.discount is True:
            res -= self.model_points
        return res

    def check_rules(self):
        super(DiscountedTransport, self).check_rules()
        # import pdb; pdb.set_trace()
        self.points = self.build_points()


class Rhino(DiscountedTransport):
    type_id = 'rhino_v3'
    type_name = "Rhino"
    model_points = 35

    def __init__(self, parent, points=model_points):
        super(Rhino, self).__init__(parent=parent, points=points, gear=[
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.opt = Vehicle(self)


class Razorback(DiscountedTransport):
    type_id = 'razorback_v3'
    type_name = 'Razorback'
    model_points = 55

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent, 'Weapon')
            self.tlhbgun = self.variant('Twin-linked heavy bolter', 0)
            self.tlhflame = self.variant('Twin-linked heavy flamer', 0)
            self.tlasscan = self.variant('Twin-linked assault cannon', 20)
            self.tllcannon = self.variant('Twin-linked lascannon', 20)
            self.lascplasgun = self.variant('Lascannon and twin-linked plasma gun', 20, gear=[
                Gear('Lascannon'), Gear('Twin-linked plasma gun')
            ])

    def __init__(self, parent):
        super(Razorback, self).__init__(parent=parent, points=Razorback.model_points, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.weapon = Razorback.Weapon(self)
        self.opt = Vehicle(self)


class DropPod(DiscountedTransport):
    type_id = 'drop_pod_v3'
    type_name = 'Drop Pod'
    model_points = 35

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant('Storm bolter', 0)
            self.dwind = self.variant('Deathwind launcher', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DropPod.Options, self).__init__(parent, 'Options')
            self.locator = self.variant('Locator beacon', 10)

    def __init__(self, parent, points=model_points):
        super(DropPod, self).__init__(parent=parent, points=points, deep_strike=True, transport=True)
        self.weapon = DropPod.Weapon(self)
        self.opt = DropPod.Options(self)


class LandRaider(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_v3'
    type_name = "Land Raider"

    def __init__(self, parent, deathwing=False):
        super(LandRaider, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.opt = Vehicle(self, melta=True, raider=True)


class LandRaiderCrusader(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_crusader_v3'
    type_name = "Land Raider Crusader"

    def __init__(self, parent, deathwing=False):
        super(LandRaiderCrusader, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Hurricane bolter', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher')
        ])
        self.opt = Vehicle(self, melta=True, raider=True)


class LandRaiderRedeemer(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_redeemer_v1'
    type_name = "Land Raider Redeemer"

    def __init__(self, parent, deathwing=False):
        super(LandRaiderRedeemer, self).__init__(parent=parent, points=245, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Flamestorm cannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher'),
        ])
        self.opt = Vehicle(self, melta=True, raider=True)
