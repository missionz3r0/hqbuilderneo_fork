__author__ = 'Ivan Truskov'

from builder.core2 import *
from builder.games.wh40k.roster import Unit


class Vehicle(OptionsList):
    def __init__(self, parent, psyflame=False, melta=False):
        super(Vehicle, self).__init__(parent, 'Options')

        self.searchlight = self.Variant(self, 'Searchlight', 1)
        self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.psybolt = self.Variant(self, 'Psybolt ammunition', 5)
        if psyflame:
            self.psyflame = self.Variant(self, 'Psyflame ammunition', 5)
        self.sbgun = self.Variant(self, 'Storm bolter', 10)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
        self.tsarm = self.Variant(self, 'Truesilver armour', 10)
        if melta:
            self.melta = self.Variant(self, 'Multi-melta', 10)
        self.exarm = self.Variant(self, 'Extra armour', 15)


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(parent=parent, name='Transport', limit=1)
        SubUnit(self, Rhino(parent=None))
        SubUnit(self, Razorback(parent=None))
        SubUnit(self, Chimera(parent=None))
        SubUnit(self, Valkyrie(parent=None))
        SubUnit(self, LandRaider(parent=None))
        SubUnit(self, LandRaiderCrusader(parent=None))
        SubUnit(self, LandRaiderRedeemer(parent=None))


class Rhino(Unit):
    type_id = 'rhino_v1'
    type_name = "Rhino"

    def __init__(self, parent):
        super(Rhino, self).__init__(parent=parent, points=35, gear=[
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
        ])
        self.opt = Vehicle(self)


class Razorback(Unit):
    type_id = 'razorback_v1'
    type_name = 'Razorback'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent, 'Weapon')
            self.tlhbgun = self.variant('Twin-linked heavy bolter', 0)
            self.tlhflame = self.variant('Twin-linked heavy flamer', 25)
            self.tlasscan = self.variant('Twin-linked assault cannon', 25)
            self.tllcannon = self.variant('Twin-linked lascannon', 35)
            self.lascplasgun = self.variant('Lascannon and twin-linked plasma gun', 35, gear=[
                Gear('Lascannon'), Gear('Twin-linked plasma gun')
            ])

    def __init__(self, parent):
        super(Razorback, self).__init__(parent=parent, points=40, gear=[
            Gear('Smoke launchers'),
        ])
        self.weapon = Razorback.Weapon(self)
        self.opt = Vehicle(self, psyflame=True)


class Chimera(Unit):
    type_id = 'chimera_v1'
    type_name = 'Chimera'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon1, self).__init__(parent, 'Weapon')
            self.tlhbgun = self.variant('Heavy bolter', 0)
            self.tlhflame = self.variant('Heavy flamer', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon2, self).__init__(parent, '')
            self.tlasscan = self.variant('Multi-laser', 0)
            self.tlhbgun = self.variant('Heavy bolter', 0)
            self.tlhflame = self.variant('Heavy flamer', 0)

    def __init__(self, parent):
        super(Chimera, self).__init__(parent=parent, points=55, gear=[
            Gear('Smoke launchers'),
        ])
        Chimera.Weapon1(self)
        Chimera.Weapon2(self)
        Vehicle(self)


class Valkyrie(Unit):
    type_id = 'valkyrie_v1'
    type_name = 'Valkyrie'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Multi-laser', 0)
            self.variant('Lascannon', 15)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon2, self).__init__(parent, '')
            self.variant('Two hellstrike missiles', 0, gear=Gear('Hellstrike missile', count=2))
            self.variant('Two multiple rocket pods', 30, gear=Gear('Multiple rocket pod', count=2))

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(Valkyrie.Weapon3, self).__init__(parent, '')
            self.variant('Two side sponsons with a heavy bolters', 10, gear=Gear('Heavy bolter', count=2))

    def __init__(self, parent):
        super(Valkyrie, self).__init__(parent=parent, points=100, gear=[
            Gear('Extra armour'),
            Gear('Searchlight'),
        ])
        Valkyrie.Weapon1(self)
        Valkyrie.Weapon2(self)
        Valkyrie.Weapon3(self)


class LandRaider(Unit):
    type_id = 'land_raider_v1'
    type_name = "Land Raider"

    def __init__(self, parent):
        super(LandRaider, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
        ])
        self.opt = Vehicle(self, melta=True)


class LandRaiderCrusader(Unit):
    type_id = 'land_raider_crusader_v1'
    type_name = "Land Raider Crusader"

    def __init__(self, parent):
        super(LandRaiderCrusader, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Hurricane bolter', count=2),
            Gear('Smoke launchers'),
            Gear('Frag Assault Launcher')
        ])
        self.opt = Vehicle(self, melta=True)


class LandRaiderRedeemer(Unit):
    type_id = 'land_raider_redeemer_v1'
    type_name = "Land Raider Redeemer"

    def __init__(self, parent):
        super(LandRaiderRedeemer, self).__init__(parent=parent, points=240, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Flamestorm cannon', count=2),
            Gear('Smoke launchers'),
            Gear('Frag Assault Launcher'),
        ])
        self.opt = Vehicle(self, psyflame=True, melta=True)


class Warband(Unit):
    type_name = 'Inquisitorial Henchmen Warband'
    type_id = 'inquisitorial_wenchmen_warband_v1'
    faction = 'Inquisition'

    class Acolyte(ListSubUnit):

        def __init__(self, parent):
            super(Warband.Acolyte, self).__init__(parent=parent, name='Acolyte', points=4, gear=[])
            self.armour = self.Armour(self)
            self.weapon1 = self.Weapon(self, 'Weapon', laspistol=True)
            self.weapon2 = self.Weapon(self, '', chainsword=True)
            self.opt = self.Options(self)

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon1.has_spec() or self.weapon2.has_spec()

        class Weapon(OneOf):
            def __init__(self, parent, name, laspistol=False, chainsword=False):
                super(Warband.Acolyte.Weapon, self).__init__(parent=parent, name=name)
                if laspistol:
                    self.variant('Laspistol', 0)
                if chainsword:
                    self.variant('Chainsword', 0)
                self.variant('Bolter', 1)
                self.variant('Storm bolter', 3)
                self.variant('Hot-shot lasgun', 5)
                self.spec = [
                    self.variant('Combi-flamer', 10),
                    self.variant('Combi-melta', 10),
                    self.variant('Combi-plasma', 10),
                    self.variant('Plasma gun', 10),
                    self.variant('Meltagun', 10),
                    self.variant('Flamer', 10),
                    self.variant('Power sword', 15),
                    self.variant('Plasma pistol', 15),
                    self.variant('Storm shield', 20),
                    self.variant('Power fist', 25)
                ]

            def has_spec(self):
                return self.cur in self.spec

        class Armour(OneOf):
            def __init__(self, parent):
                super(Warband.Acolyte.Armour, self).__init__(parent=parent, name='Armour')
                self.flakarmour = self.variant('Flak armour', 0)
                self.caparacearmour = self.variant('Caparace armour', 4)
                self.powerarmour = self.variant('Power armour', 10)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Warband.Acolyte.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.meltabombs = self.variant('Melta bombs', 5)

    class Servitor(ListSubUnit):

        def __init__(self, parent):
            super(Warband.Servitor, self).__init__(parent=parent, name='Servitor', points=10,
                                                   gear=[Gear('Carapace armour')])
            self.weapon1 = self.Weapon(self, 'Weapon')

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(Warband.Servitor.Weapon, self).__init__(parent=parent, name=name)

                self.variant('Servo-arm', 0)
                self.spec = [
                    self.variant('Heavy bolter', 0),
                    self.variant('Multi-melta', 0),
                    self.variant('Plasma cannon', 10),
                ]

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon1.cur in self.weapon1.spec

    class Priest(ListSubUnit):

        def __init__(self, parent):
            super(Warband.Priest, self).__init__(parent=parent, name='Ministorum Priest', points=25, gear=[
                Gear('Flak armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Rosarius')
            ])
            self.weapon1 = self.Weapon(self, 'Weapon', laspistol=True)
            self.weapon2 = self.Weapon(self, '', ccw=True)
            self.opt = self.Options(self)

        def check_rules(self):
            super(Warband.Priest, self).check_rules()
            for w in self.weapon1.melee:
                w.active = self.weapon2.cur not in self.weapon2.melee
            for w in self.weapon1.ranged:
                w.active = self.weapon2.cur not in self.weapon2.ranged
            for w in self.weapon2.melee:
                w.active = self.weapon1.cur not in self.weapon1.melee
            for w in self.weapon2.ranged:
                w.active = self.weapon1.cur not in self.weapon1.ranged

        class Weapon(OneOf):
            def __init__(self, parent, name, laspistol=False, ccw=False):
                super(Warband.Priest.Weapon, self).__init__(parent=parent, name=name)
                if laspistol:
                    self.variant('Laspistol', 0)
                if ccw:
                    self.variant('Close combat weapon', 0)

                self.melee = [
                    self.variant('Chainsword', 0),
                    self.variant('Power sword', 15),
                    self.variant('Eviscerator', 30),
                ]
                self.ranged = [
                    self.variant('Autogun', 0),
                    self.variant('Bolt pistol', 1),
                    self.variant('Boltgun', 1),
                    self.variant('Shotgun', 1),
                    self.variant('Storm bolter', 5),
                    self.variant('Combi-flamer', 10),
                    self.variant('Combi-melta', 10),
                    self.variant('Combi-plasma', 10),
                    self.variant('Condemnor boltgun', 10),
                    self.variant('Plasma gun', 15),
                    self.variant('Plasma pistol', 15),
                ]

        class Options(OptionsList):
            def __init__(self, parent):
                super(Warband.Priest.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.meltabombs = self.variant('Melta bombs', 5)

    class Units(OptionsList):
        def __init__(self, parent):
            super(Warband.Units, self).__init__(parent, '')
            self.acolyte = self.variant('Acolytes')
            self.acolyte.used = False
            self.acolyte_units = UnitList(parent, Warband.Acolyte, 1, 12)
            self.priests = self.variant('Ministorum Priests')
            self.priests.used = False
            self.priests_units = UnitList(parent, Warband.Priest, 1, 12)
            self.serv = self.variant('Servitors')
            self.serv.used = False
            self.serv_units = UnitList(parent, Warband.Servitor, 1, 12)

        def get_count(self):
            return sum(u.count for u in (self.serv_units, self.acolyte_units, self.priests_units) if u.used)

        def check_rules(self):
            super(Warband.Units, self).check_rules()
            self.acolyte_units.used = self.acolyte_units.visible = self.acolyte.value
            self.priests_units.used = self.priests_units.visible = self.priests.value
            self.serv_units.used = self.serv_units.visible = self.serv.value

    def __init__(self, parent):
        Unit.__init__(self, parent)
        self.units = self.Units(self)
        self.models = [
            Count(
                self, name=m['name'], min_limit=0, max_limit=12, points=m['points'],
                gear=UnitDescription(m['name'], points=m['points'], options=m['options'])
            ) for m in (
                dict(name='Arco-flagellant', options=[Gear('Arco-flails')], points=15),
                dict(name='Crusader', points=15, options=[Gear('Flak armour'), Gear('Power weapon'),
                                                          Gear('Storm shield')]),
                dict(name='Daemonhost', points=10, options=[Gear('Close combat weapon')]),
                dict(name='Death Cult Assassin', points=15, options=[Gear('Flak armour'),
                                                                     Gear('Power weapon', count=2)]),
                dict(name='Jokaero Weaponsmith', points=35, options=[Gear('Defence orbs'), Gear('Digital weapon')]),
                dict(name='Mystic', points=10, options=[Gear('Flak armour'), Gear('Laspistol')]),
                dict(name='Psyker', points=10, options=[Gear('Flak armour'), Gear('Laspistol')]),
            )
        ]
        self.transport = Transport(self)

    def get_count(self):
        return sum(c.cur for c in self.models) + self.units.get_count()

    def check_rules(self):
        super(Warband, self).check_rules()
        spec = sum(u.count_spec() for u in self.units.acolyte_units.units)
        if spec > 3:
            self.error('Only 3 Acolytes can take special weapon. (Taken: {0})'.format(spec))
        spec = sum(u.count_spec() for u in self.units.serv_units.units)
        if spec > 3:
            self.error('Only 3 Servitors can take special weapon. (Taken: {0})'.format(spec))
        if not 3 <= self.get_count() <= 12:
            self.error(self.name + ' must include 3-12 henchmen. Taken: {0}'.format(self.get_count()))

    def count_charges(self):
        count = sum(u.cur for u in self.models if u.name == 'Psyker')
        if count > 0:
            return 1
        return 0

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(Warband, self).build_statistics()))
