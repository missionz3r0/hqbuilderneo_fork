__author__ = 'Ivan Truskov'
__summary__ = ['Gathering Storm 2: The Fracture of Biel-Tan']

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed,\
    Wh40kImperial
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, LordsOfWarSection, UnitType, Detachment, Formation,\
    Section
from .hq import Yvraine, Visarch
from .lords import Yncarne


class YnnariFormation(Formation):
    is_ynnari = True
    is_iyanden = False


class Triumvirate(YnnariFormation):
    army_name = 'Triumvirate of Ynnhead'
    army_id = 'ynnari_v1_triumvirate'

    def __init__(self):
        super(Triumvirate, self).__init__()
        UnitType(self, Yvraine, min_limit=1, max_limit=1)
        UnitType(self, Visarch, min_limit=1, max_limit=1)
        UnitType(self, Yncarne, min_limit=1, max_limit=1)


class Vanguard(YnnariFormation):
    army_name = 'Soulbound Vanguard'
    army_id = 'ynnari_v1_vanguard'

    def __init__(self):
        super(Vanguard, self).__init__()
        from builder.games.wh40k.dark_eldar_v2 import Incubi, Wyches
        from builder.games.wh40k.eldar_v3 import Avengers
        UnitType(self, Avengers, min_limit=2, max_limit=2)
        UnitType(self, Incubi, min_limit=1, max_limit=1)
        UnitType(self, Wyches, min_limit=1, max_limit=1)


class Bladehost(YnnariFormation):
    army_name = 'Aeldari Bladehost'
    army_id = 'ynnari_v1_bladehost'

    def __init__(self):
        super(Bladehost, self).__init__()
        from builder.games.wh40k.dark_eldar_v2 import Wyches
        from builder.games.wh40k.eldar_v3 import StormGuardians, BlackGuardians
        from builder.games.wh40k.harlequins import Troupe
        UnitType(self, Wyches, min_limit=2, max_limit=2)
        guardians = [UnitType(self, StormGuardians),
                     UnitType(self, BlackGuardians)]
        self.add_type_restriction(guardians, 2, 2)
        UnitType(self, Troupe, min_limit=1, max_limit=1)


class Net(YnnariFormation):
    army_name = "Ynnead's Net"
    army_id = 'ynnari_v1_net'

    def __init__(self):
        super(Net, self).__init__()
        from builder.games.wh40k.dark_eldar_v2 import Reavers
        from builder.games.wh40k.eldar_v3 import Warlocks, Windriders
        from builder.games.wh40k.harlequins import Skyweavers
        self.locks = UnitType(self, Warlocks, min_limit=1, max_limit=1)
        UnitType(self, Windriders, min_limit=1, max_limit=1)
        UnitType(self, Reavers, min_limit=1, max_limit=1)
        UnitType(self, Skyweavers, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Net, self).check_rules()
        for unit in self.locks.units:
            if not unit.is_skyrunner():
                self.error("All Warlocks must be upgraded to Warlock Skyrunners")
                return


class GhostHall(YnnariFormation):
    army_name = 'Whispering Ghost Hall'
    army_id = 'ynnari_v1_ghost_hall'

    def __init__(self):
        super(GhostHall, self).__init__()
        from builder.games.wh40k.harlequins import Shadowseer
        from builder.games.wh40k.eldar_v3 import Farseer, Spiritseer, Wraithlord,\
            Wraithblades, Wraithguards
        UnitType(self, Farseer, min_limit=1, max_limit=1)
        UnitType(self, Shadowseer, min_limit=1, max_limit=1)
        UnitType(self, Spiritseer, min_limit=1, max_limit=1)
        UnitType(self, Wraithlord, min_limit=2, max_limit=2)
        guards = [UnitType(self, Wraithblades),
                  UnitType(self, Wraithguards)]
        self.add_type_restriction(guards, 3, 3)


class RebornWarhost(Wh40kBase):
    is_ynnari = True
    army_name = 'Reborn Warhost'
    army_id = 'ynnari_v1_reborn'
    is_iyanden = False

    def is_coven(self):
        return False

    class RebornHQ(HQSection):
        def __init__(self, parent):
            super(RebornWarhost.RebornHQ, self).__init__(parent)
            UnitType(self, Yvraine)
            UnitType(self, Visarch)
            from builder.games.wh40k.eldar_v3 import Eldrad, Yriel, Illic, JainZar, Asurmen, Karandras, Fuegan, \
                Baharroth, Maugan, Autarch, Farseer, Spiritseer, Warlocks
            UnitType(self, Eldrad)
            UnitType(self, Yriel)
            UnitType(self, Illic)
            UnitType(self, Asurmen)
            UnitType(self, JainZar)
            UnitType(self, Karandras)
            UnitType(self, Fuegan)
            UnitType(self, Baharroth)
            UnitType(self, Maugan)
            UnitType(self, Autarch)
            UnitType(self, Farseer)
            UnitType(self, Warlocks)
            UnitType(self, Spiritseer)
            from builder.games.wh40k.dark_eldar_v2 import Archon, Court, Succubus, Lelith
            UnitType(self, Archon)
            UnitType(self, Court)
            UnitType(self, Succubus)
            UnitType(self, Lelith)

    class RebornFast(FastSection):
        def __init__(self, parent):
            super(RebornWarhost.RebornFast, self).__init__(parent)
            from builder.games.wh40k.eldar_v3 import WaveSerpent, Hawks, Spiders, Spears,\
                VyperSquadron, HemlockWraithfighter, CrimsonHunters
            UnitType(self, WaveSerpent)
            UnitType(self, Hawks)
            UnitType(self, Spiders)
            UnitType(self, Spears)
            UnitType(self, CrimsonHunters)
            UnitType(self, VyperSquadron)
            UnitType(self, HemlockWraithfighter)
            from builder.games.wh40k.dark_eldar_v2 import Beastmasters, Raider, Venom,\
                Reavers, Hellions, Razorwing, Scourges
            UnitType(self, Beastmasters)
            UnitType(self, Raider)
            UnitType(self, Venom)
            UnitType(self, Reavers)
            UnitType(self, Hellions)
            UnitType(self, Razorwing)
            UnitType(self, Scourges)
            from builder.games.wh40k.harlequins import Skyweavers, Starweaver
            UnitType(self, Skyweavers)
            UnitType(self, Starweaver)

    class RebornTroops(TroopsSection):
        def __init__(self, parent):
            super(RebornWarhost.RebornTroops, self).__init__(parent)
            from builder.games.wh40k.eldar_v3 import GuardianDefenders, Windriders,\
                Rangers, Avengers, StormGuardians
            UnitType(self, GuardianDefenders)
            UnitType(self, StormGuardians)
            UnitType(self, Windriders)
            UnitType(self, Rangers)
            UnitType(self, Avengers)
            from builder.games.wh40k.dark_eldar_v2 import KabaliteWarriors, Wyches
            UnitType(self, KabaliteWarriors)
            UnitType(self, Wyches)
            from builder.games.wh40k.harlequins import Troupe
            UnitType(self, Troupe)

    class RebornElites(ElitesSection):
        def __init__(self, parent):
            super(RebornWarhost.RebornElites, self).__init__(parent)
            from builder.games.wh40k.eldar_v3 import Banshees, Scorpions, Dragons, Wraithguards, Wraithblades,\
                BlackGuardians, BlackWindriders, BlackVypers, BlackWarWalkers
            UnitType(self, Banshees)
            UnitType(self, Scorpions)
            UnitType(self, Dragons)
            UnitType(self, Wraithguards)
            UnitType(self, Wraithblades)
            from builder.games.wh40k.dark_eldar_v2 import Incubi
            UnitType(self, Incubi)
            from builder.games.wh40k.harlequins import Shadowseer, DeathJester, Solitare
            UnitType(self, DeathJester)
            UnitType(self, Shadowseer)
            UnitType(self, Solitare)
            UnitType(self, BlackGuardians)
            UnitType(self, BlackWindriders)
            UnitType(self, BlackVypers)
            UnitType(self, BlackWarWalkers)

    class RebornLords(LordsOfWarSection):
        def __init__(self, parent):
            super(RebornWarhost.RebornLords, self).__init__(parent)
            from builder.games.wh40k.eldar_v3 import Wraithknight
            UnitType(self, Wraithknight)
            UnitType(self, Yncarne)

    class RebornHeavy(HeavySection):
        def __init__(self, parent):
            super(RebornWarhost.RebornHeavy, self).__init__(parent)
            from builder.games.wh40k.eldar_v3 import DarkReapers, Battery, Falcons, FirePrisms, NightSpinners,\
                WarWalkers, Wraithlord
            UnitType(self, DarkReapers)
            UnitType(self, Battery)
            UnitType(self, Falcons)
            UnitType(self, FirePrisms)
            UnitType(self, NightSpinners)
            UnitType(self, WarWalkers)
            UnitType(self, Wraithlord)
            from builder.games.wh40k.dark_eldar_v2 import Ravager, Voidraven
            UnitType(self, Ravager)
            UnitType(self, Voidraven)
            from builder.games.wh40k.harlequins import Voidweavers
            UnitType(self, Voidweavers.Voidweaver)

    class RebornFormations(Section):
        def __init__(self, parent):
            super(RebornWarhost.RebornFormations, self).__init__(parent, 'form', 'Formatuons',
                                                                 None, None)
            from builder.games.wh40k.eldar_v3 import GuardianBattlehost, GuardianStormhost,\
                WindriderHost, SeerCouncil, AspectHost, DireAvengerShrine, CrimsonDeatch,\
                WraithHost
            from builder.games.wh40k.dark_eldar_v2 import RaidingParty
            from builder.games.wh40k.harlequins import Revenge, Brood, Cast, Jest, Path, Blade
            for formation in [Triumvirate, Vanguard, Bladehost, Net, GhostHall]:
                Detachment.build_detach(self, formation, formation.faction)
            for formation in [GuardianBattlehost, GuardianStormhost,
                              WindriderHost, SeerCouncil, AspectHost, DireAvengerShrine,
                              CrimsonDeatch, WraithHost, RaidingParty, Revenge, Brood, Cast,
                              Jest, Path, Blade]:
                class DerivedFormation(formation, YnnariFormation):
                    pass
                Detachment.build_detach(self, DerivedFormation, DerivedFormation.faction)

    def __init__(self):
        super(RebornWarhost, self).__init__(sections=[
            self.RebornHQ(self), self.RebornElites(self),
            self.RebornTroops(self), self.RebornFast(self),
            self.RebornHeavy(self), self.RebornLords(self),
            self.RebornFormations(self)
        ])


faction = 'Ynnari'


class Ynnari(Wh40kImperial, Wh40k7ed):
    army_id = 'ynnari_v1'
    army_name = 'Ynnari'
    faction = faction

    def __init__(self):
        super(Ynnari, self).__init__([
            RebornWarhost,
            Triumvirate,
            Vanguard,
            Bladehost,
            Net,
            GhostHall])


detachments = [
    RebornWarhost,
    Triumvirate,
    Vanguard,
    Bladehost,
    Net,
    GhostHall
]


unit_types = [
    Yvraine, Visarch, Yncarne
]
