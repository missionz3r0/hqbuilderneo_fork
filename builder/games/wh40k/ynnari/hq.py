__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class Yvraine(Unit):
    type_name = 'Yvraine, Emissary of Ynnhead'
    type_id = 'yvraine_v1'

    def __init__(self, parent):
        super(Yvraine, self).__init__(parent, 'Yvraine', 200, [
            Gear('Close combat weapon'),
            Gear('Runesuit'),
            Gear('Kha-vir, the Sword of Sorrows')
        ], unique=True, static=True)

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Yvraine, self).build_statistics())


class Visarch(Unit):
    type_name = 'The Visarch, Sword of Ynnhead'
    type_id = 'visarch_v1'

    def __init__(self, parent):
        super(Visarch, self).__init__(parent, 'The Visarch', 150,
                                      [Gear('Asu-var, theSword of Silent Screams')],
                                      static=True, unique=True)
