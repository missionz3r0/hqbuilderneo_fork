__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class Yncarne(Unit):
    type_name = 'The Yncarne, Avatar of Ynnhead'
    type_id = 'yncarne_v1'

    def __init__(self, parent):
        super(Yncarne, self).__init__(parent, 'The Yncarne', 275,
                                      [Gear('Vilith-zhar, the Sword of Souls')],
                                      static=True, unique=True)

    def count_charges(self):
        return 3

    def build_statistics(self):
        return self.note_charges(super(Yncarne, self).build_statistics())
