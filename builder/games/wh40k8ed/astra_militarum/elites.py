__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class OrdnanceMaster(ElitesUnit, armory.RegimentUnit):
    type_name = get_name(units.MasterOfOrdnance)
    type_id = 'ordnance_master_v1'

    keywords = ['Character', 'Infantry']
    power = 2

    def __init__(self, parent):
        super(OrdnanceMaster, self).__init__(
            parent, gear=create_gears(ranged.Laspistol, ranged.ArtilleryBarrage),
            static=True, points=get_cost(units.MasterOfOrdnance))


class PCommander(ElitesUnit, armory.RegimentUnit):
    type_name = get_name(units.PlatoonCommander)
    type_id = 'plat_commander_v1'

    keywords = ['Character', 'Officer', 'Infantry']
    power = 2

    class Weapon1(OptionsList):
        def __init__(self, parent):
            super(PCommander.Weapon1, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Chainsword)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(PCommander.Weapon2, self).__init__(parent, 'Pistol')
            self.variant(*ranged.Laspistol)
            armory.add_ranged_weapons(self)

    def __init__(self, parent):
        super(PCommander, self).__init__(parent, gear=[
            Gear('Frag grenades')
        ], points=get_cost(units.PlatoonCommander))
        self.Weapon1(self)
        self.Weapon2(self)


class Veteran(ListSubUnit):
    type_name = 'Veteran'

    class BaseWeapon(OneOf):
        def __init__(self, parent):
            super(Veteran.BaseWeapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.Lasgun)
            self.pistol = self.variant(*ranged.Laspistol)
            self.hf = self.variant(*ranged.HeavyFlamer)
            armory.add_special_weapons(self, precise=True)

    class Sword(OptionsList):
        def __init__(self, parent):
            super(Veteran.Sword, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Chainsword)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Veteran.Options, self).__init__(parent, name='Options', limit=1)
            self.vox = self.variant(*wargear.VoxCaster)
            self.banner = self.variant(*wargear.RegimentalStandard)
            self.medic = self.variant(*wargear.MediPack)

    def __init__(self, parent):
        super(Veteran, self).__init__(parent, points=get_cost(units.CommandSquad),
                                      gear=[Gear('Frag grenades')])
        self.wep = self.BaseWeapon(self)
        self.sw = self.Sword(self)
        self.opt = self.Options(self)
        self.base_unit = self.root_unit

    def check_rules(self):
        super(Veteran, self).check_rules()
        for o in [self.wep.hf] + self.wep.special:
            o.active = not self.opt.any
        self.opt.visible = self.opt.used = self.wep.cur not in [self.wep.hf] + self.wep.special
        self.sw.visible = self.sw.used = self.wep.cur == self.wep.pistol

    @ListSubUnit.count_gear
    def count_vox(self):
        return self.opt.vox.value

    @ListSubUnit.count_gear
    def count_flame(self):
        return self.wep.hf == self.wep.cur

    @ListSubUnit.count_gear
    def count_banner(self):
        return self.opt.banner.value

    @ListSubUnit.count_gear
    def count_medic(self):
        return self.opt.medic.value


class CommandWeaponTeam(Unit):
    type_name = 'Veteran Weapon Team'
    base_points = 2 * get_cost(units.CommandSquad)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CommandWeaponTeam.Weapon, self).__init__(parent, 'Heavy weapon')
            armory.add_heavy_weapons(self)

    def __init__(self, parent):
        super(CommandWeaponTeam, self).__init__(parent, points=self.base_points,
                                                gear=[Gear('Frag grenades'), Gear('Lasgun')])
        self.wep = self.Weapon(self)


class CommandSquad(ElitesUnit, armory.RegimentUnit):
    type_name = get_name(units.CommandSquad)
    type_id = 'comsquad_v1'

    keywords = ['Veterans', 'Infantry']
    power = 2

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent)
        self.vets = UnitList(self, Veteran, 2, 4, start_value=4)
        self.team = self.Members(self)

    class Members(OptionalSubUnit):
        def __init__(self, parent):
            super(CommandSquad.Members, self).__init__(parent, name='')
            self.heavy = SubUnit(self, CommandWeaponTeam(parent))

    def check_rules(self):
        super(CommandSquad, self).check_rules()
        # self.vets.update_range(2 if self.team.any else 4,
        #                        2 if self.team.any else 4)
        if sum(u.count_banner() for u in self.vets.units) > 1:
            self.error('Only one veteran may have regimental standard')
        if sum(u.count_medic() for u in self.vets.units) > 1:
            self.error('Only one veteran may have medi-pack')
        if sum(u.count_vox() for u in self.vets.units) > 1:
            self.error('Only one veteran may have vox-caster')
        if sum(u.count_flame() for u in self.vets.units) > 1:
            self.error('Only one veteran may have heavy flamer')
        if self.get_count() != 4:
            self.error('Command squad must include 4 members')

    def get_count(self):
        return self.vets.count + 2 * self.team.any


class SpecialWeaponSquad(ElitesUnit, armory.RegimentUnit):
    type_name = get_name(units.SpecialWeaponsSquad)
    type_id = 'swep_squad_v1'

    keywords = ['Infantry']
    power = 2
    member = UnitDescription('Guardsman', options=[Gear('Frag grenades')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SpecialWeaponSquad.Weapon, self).__init__(parent, 'Special Weapon')
            self.variant(*ranged.Lasgun)
            # self.variant('Demolition charge', 5, gear=[Gear('Lasgun'), Gear('Demolition charge')])
            armory.add_special_weapons(self)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(SpecialWeaponSquad, self).__init__(parent, points=get_cost(units.SpecialWeaponsSquad) * 6,
                                                 gear=SpecialWeaponSquad.member.clone().set_count(3).add(Gear('Lasgun')))
        self.weps = [self.Weapon(self) for i in range(0, 3)]

    def get_count(self):
        return 6

    def build_description(self):
        res = super(SpecialWeaponSquad, self).build_description()
        for wep in self.weps:
            res.add_dup(SpecialWeaponSquad.member.clone().add(wep.cur.gear))
        return res


class Veteran2(ListSubUnit):
    type_name = 'Veteran'

    class BaseWeapon(OneOf):
        def __init__(self, parent):
            super(Veteran2.BaseWeapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.Lasgun)
            self.variant(*ranged.Shotgun)
            self.variant(*ranged.Autogun)
            self.hf = self.variant(*ranged.HeavyFlamer)
            armory.add_special_weapons(self, precise=True)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Veteran2.Options, self).__init__(parent, name='Options', limit=1)
            self.vox = self.variant(*wargear.VoxCaster)

    def __init__(self, parent):
        super(Veteran2, self).__init__(parent, points=get_cost(units.Veterans),
                                       gear=[Gear('Frag grenades')])
        self.wep = self.BaseWeapon(self)
        self.opt = self.Options(self)

    def check_rules(self):
        super(Veteran2, self).check_rules()
        for o in [self.wep.hf] + self.wep.special:
            o.active = not self.opt.any
        self.opt.visible = self.opt.used = self.wep.cur not in [self.wep.hf] + self.wep.special

    @ListSubUnit.count_gear
    def count_vox(self):
        return self.opt.vox.value

    @ListSubUnit.count_gear
    def count_flame(self):
        return self.wep.hf == self.wep.cur

    @ListSubUnit.count_gear
    def count_spec(self):
        return self.wep.cur in self.wep.special


class VeteranSquad(ElitesUnit, armory.RegimentUnit):
    type_name = get_name(units.Veterans)
    type_id = 'vetsquad_v1'

    keywords = ['Veterans', 'Infantry']
    power = 5

    class Sergeant(Unit):
        type_name = 'Veteran Sergeant'

        class Weapon1(OptionsList):
            def __init__(self, parent):
                super(VeteranSquad.Sergeant.Weapon1, self).__init__(parent, 'Melee weapon', limit=1)
                self.variant('Chainsword')
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(VeteranSquad.Sergeant.Weapon2, self).__init__(parent, 'Pistol')
                self.variant('Laspistol')
                armory.add_ranged_weapons(self)

        def __init__(self, parent):
            super(VeteranSquad.Sergeant, self).__init__(parent, points=get_cost(units.Veterans),
                                                        gear=[Gear('Frag grenades')])
            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(VeteranSquad, self).__init__(parent)
        SubUnit(self, self.Sergeant(self))
        self.vets = UnitList(self, Veteran2, 7, 9, start_value=9)
        self.team = self.Members(self)

    class Members(OptionalSubUnit):
        def __init__(self, parent):
            super(VeteranSquad.Members, self).__init__(parent, name='')
            class VeteranWeaponTeam(CommandWeaponTeam):
                base_points = 2 * get_cost(units.Veterans)

            self.heavy = SubUnit(self, VeteranWeaponTeam(parent))

    def check_rules(self):
        super(VeteranSquad, self).check_rules()
        # self.vets.update_range(7 if self.team.any else 9,
        #                        7 if self.team.any else 9)
        if sum(u.count_vox() for u in self.vets.units) > 1:
            self.error('Only one veteran may have vox-caster')
        if sum(u.count_flame() for u in self.vets.units) > 1:
            self.error('Only one veteran may have heavy flamer')
        if sum(u.count_spec() for u in self.vets.units) > 3:
            self.error('No more then 3 veterans may have special weapons')
        if self.get_count() != 10:
            self.error('Veteran squad must include 10 members')

    def get_count(self):
        return 1 + self.vets.count + 2 * self.team.any


class Kell(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.ColourSergeantKell)
    type_id = 'kell_v1'

    faction = ['Cadian']
    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        super(Kell, self).__init__(
            parent, gear=[
            Gear('Laspistol'),
            Gear('Power fist'),
            Gear('Power sword')
        ], unique=True, static=True,
        points=get_cost(units.ColourSergeantKell))                       


class Harker(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.SergeantHarker)
    type_id = 'harker_v1'

    faction = ['Catachan']
    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        super(Harker, self).__init__(parent, gear=[
            Gear('Payback'),
            Gear('Frag grenades'),
            Gear('Krak grenades')
        ], unique=True, static=True, points=get_cost(units.SergeantHarker))


class Commissar(ElitesUnit, armory.PrefectusUnit):
    type_name = get_name(units.Commissar)
    type_id = 'commissar_v1'

    keywords = ['Character', 'Infantry']
    power = 2

    class MWeapon(OptionsList):
        def __init__(self, parent):
            super(Commissar.MWeapon, self).__init__(parent, 'Melee weapon', limit=1)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Commissar.Weapon2, self).__init__(parent, 'Pistol')
            armory.add_ranged_weapons(self)

    def __init__(self, parent):
        super(Commissar, self).__init__(parent, points=get_cost(units.Commissar))
        self.MWeapon(self)
        self.MWeapon(self)
        self.Weapon2(self)


class Tempestus(ListSubUnit):
    type_name = 'Tempestus Scion'

    class BaseWeapon(OneOf):
        def __init__(self, parent):
            super(Tempestus.BaseWeapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HotShotLasgun)
            self.variant(*ranged.Flamer)
            self.variant(*ranged.MeltagunPrecise)
            self.variant(*ranged.PlasmaGunPrecise)
            self.variant(*ranged.GrenadeLauncher)
            self.variant(*ranged.HotShotVolleyGun)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Tempestus.Options, self).__init__(parent, name='Options', limit=1)
            self.vox = self.variant(
                join_and(wargear.VoxCaster, ranged.HotShotLaspistol),
                points=get_costs(wargear.VoxCaster, ranged.HotShotLaspistol),
                gear=create_gears(wargear.VoxCaster, ranged.HotShotLaspistol))
            # self.vox2 = self.variant('Vox-caster and hot-shot lasgun', gear=[
            #     Gear('Vox-caster'), Gear('Hot-shot laspistol'), Gear('Hot-shot lasgun')
            # ], points=5 + 1 + 2)
            self.medic = self.variant(
                join_and(wargear.MediPack, ranged.HotShotLaspistol),
                points=get_costs(wargear.MediPack, ranged.HotShotLaspistol),
                gear=create_gears(wargear.MediPack, ranged.HotShotLaspistol))
            # self.medic2 = self.variant('Medi-pack and hpt-shot lasgun', gear=[
            #     Gear('Medi-pack'), Gear('Hot-shot laspistol'), Gear('Hot-shot lasgun')
            # ], points=10 + 1 + 1)
            self.banner = self.variant('Platoon standard',
                                       gear=create_gears(wargear.RegimentalStandard, ranged.HotShotLasgun),
                                       points=get_costs(wargear.RegimentalStandard, ranged.HotShotLasgun))

    def __init__(self, parent):
        super(Tempestus, self).__init__(parent, points=get_cost(units.MilitarumTempestusCommandSquad),
                                        gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.wep = self.BaseWeapon(self)
        self.opt = self.Options(self)
        self.base_unit = self.root_unit

    def check_rules(self):
        super(Tempestus, self).check_rules()
        self.wep.used = self.wep.visible = not self.opt.any

    @ListSubUnit.count_gear
    def count_vox(self):
        return self.opt.vox.value  # or self.opt.vox2.value

    @ListSubUnit.count_gear
    def count_banner(self):
        return self.opt.banner.value

    @ListSubUnit.count_gear
    def count_medic(self):
        return self.opt.medic.value  # or self.opt.medic2.value


class PriestV2(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.MinistorumPriest)
    type_id = 'priest_v2'
    faction = ['Adeptus Ministorum']
    keywords = ['Character', 'Infantry']
    wiki_faction = 'Adeptus Ministorum'
    power = 2

    class Weapons(OptionsList):
        def __init__(self, parent):
            super(PriestV2.Weapons, self).__init__(parent, 'Weapons')
            self.variant(*ranged.Autogun)
            self.variant(*melee.Chainsword)

    def __init__(self, parent):
        super(PriestV2, self).__init__(parent, points=get_cost(units.MinistorumPriest),
                                       gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                             Gear('Laspistol')])
        self.Weapons(self)


class Crusaders(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.Crusaders)
    type_id = 'crusaders_v1'
    faction = ['Adeptus Ministorum']
    keywords = ['Infantry']

    power = 2

    def __init__(self, parent):
        super(Crusaders, self).__init__(parent)
        self.models = Count(self, 'Crusaders', 2, 10, per_model=True,
                            points=points_price(get_cost(units.Crusaders), melee.PowerSword),
                            gear=UnitDescription('Crusader', options=[Gear('Power sword')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power + (self.models.cur - 1) / 2


class IGServitors(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.Servitors)
    type_id = 'ig_servitors_v1'
    keywords = ['Infantry']
    power = 3
    faction = ['Adeptus Mechanicus', '<Forge World>']

    @classmethod
    def calc_faction(cls):
        return ['MARS', 'GRAIA', 'METALICA', 'LUCIUS',
                'AGRIPINAA', 'STYGIES VIII', 'RYZA']


    class Weapons(OneOf):
        def __init__(self, parent):
            super(IGServitors.Weapons, self).__init__(parent, 'Weapon')
            self.variant(*melee.ServoArm)
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.PlasmaCannon)
            self.variant(*ranged.MultiMelta)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(IGServitors, self).__init__(parent=parent, gear=[
            UnitDescription('Servitor', options=[Gear('Servo-arm')], count=2)
        ], points=4 * get_cost(units.Servitors) + 2 * get_cost(melee.ServoArm))
        self.weps = [self.Weapons(self), self.Weapons(self)]

    def build_description(self):
        desc = super(IGServitors, self).build_description()
        for w in self.weps:
            desc.add_dup(UnitDescription('Servitor').add(w.cur.gear))
        return desc

    def get_count(self):
        return 4


class TempCommandSquad(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.MilitarumTempestusCommandSquad)
    type_id = 'temp_comsquad_v1'

    faction = ['Militarum Tempestus']
    keywords = ['Infantry']
    power = 3

    def __init__(self, parent):
        super(TempCommandSquad, self).__init__(parent)
        self.vets = UnitList(self, Tempestus, 4, 4)

    def check_rules(self):
        super(TempCommandSquad, self).check_rules()
        if sum(u.count_banner() for u in self.vets.units) > 1:
            self.error('Only one Tempestus may have platoon standard')
        if sum(u.count_medic() for u in self.vets.units) > 1:
            self.error('Only one Tempestus may have medi-pack')
        if sum(u.count_vox() for u in self.vets.units) > 1:
            self.error('Only one Tempestus may have vox-caster')

    def get_count(self):
        return 4


class Ogryns(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.Ogryns)
    type_id = 'ogryns_v1'

    faction = ['Militarum Auxilla']
    keywords = ['Infantry']
    # power = 5

    def __init__(self, parent):
        super(Ogryns, self).__init__(parent, points=get_cost(units.Ogryns),
                                     gear=[UnitDescription("Ogryn Bone'ead",
                                                           options=[Gear('Frag bombs'),
                                                                    Gear('Ripper gun')])])
        self.models = Count(self, 'Ogryns', 2, 8, per_model=True, points=30,
                            gear=UnitDescription('Ogryn', options=[Gear('Frag bombs'), Gear('Ripper gun')]))

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return 1 + 4 * ((self.models.cur + 3) / 3)


class Bullgryns(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.Bullgryns)
    type_id = 'bullgryns_v1'

    faction = ['Militarum Auxilla']
    keywords = ['Infantry']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Bullgryns.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.GrenadierGauntlet)
            self.variant(*melee.BullgrynMaul)

    class Shield(OneOf):
        def __init__(self, parent):
            super(Bullgryns.Shield, self).__init__(parent, 'Shield')
            self.variant(*wargear.Slabshield)
            self.variant(*wargear.BruteShield)

    class Bullgryn(ListSubUnit):
        type_name = 'Bullgryn'

        def __init__(self, parent):
            super(Bullgryns.Bullgryn, self).__init__(parent, points=get_cost(units.Bullgryns))
            Bullgryns.Weapon(self)
            Bullgryns.Shield(self)

    class BullgrynHead(Unit):
        type_name = "Bullgryn Bone'ead"

        def __init__(self, parent):
            super(Bullgryns.BullgrynHead, self).__init__(parent, points=get_cost(units.Bullgryns))
            Bullgryns.Weapon(self)
            Bullgryns.Shield(self)

    def __init__(self, parent):
        super(Bullgryns, self).__init__(parent)
        SubUnit(self, self.BullgrynHead(self))
        self.models = UnitList(self, self.Bullgryn, 2, 8)

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return 1 + 6 * ((self.models.count + 3) / 3)


class Ratlings(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.Ratlings)
    type_id = 'ratlings_v1'

    faction = ['Militarum Auxilla']
    keywords = ['Infantry']

    def __init__(self, parent):
        super(Ratlings, self).__init__(parent)
        self.models = Count(self, 'Ratlings', 5, 10, per_model=True,
                            points=points_price(get_cost(units.Ratlings), ranged.SniperRifle),
                            gear=UnitDescription('Ratling', options=[Gear('Sniper rifle')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 2 + 1 * (self.models.cur > 5)


class Nork(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.NorkDeddog)
    type_id = 'nork_v1'

    faction = ['Militarum Auxilla']
    keywords = ['Character', 'Infantry', 'Ogryn']
    power = 4

    def __init__(self, parent):
        super(Nork, self).__init__(parent, gear=[
            Gear('Ripper gun'),
            Gear('Huge knife'),
            Gear('Frag bombs')
        ], unique=True, static=True, points=get_cost(units.NorkDeddog))


class FleetOfficer(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.OfficerOfTheFleet)
    type_id = 'fleet_liason_v1'

    faction = ['Aeronautica Imperialis']
    keywords = ['Character', 'Infantry']
    power = 2

    def __init__(self, parent):
        super(FleetOfficer, self).__init__(parent, gear=[
            Gear('Laspistol'),
        ], static=True, points=get_cost(units.OfficerOfTheFleet))


class Wyrdvanes(ElitesUnit, armory.PsykanaUnit):
    type_name = get_name(units.WyrdvanePsykers)
    type_id = 'wyrdvanes_v1'

    keywords = ['Infantry', 'Psyker']

    def __init__(self, parent):
        super(Wyrdvanes, self).__init__(parent)
        self.models = Count(self, 'Wyrdvane Psykers', 3, 9, per_model=True, points=get_cost(units.WyrdvanePsykers),
                            gear=UnitDescription('Wyrdvane Psyker', options=[Gear('Laspistol'),
                                                                             Gear('Wydsvane stave')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return (self.models.cur + 2) / 3


class Astropath(ElitesUnit, armory.PsykanaUnit):
    type_name = get_name(units.Astropath)
    type_id = 'astropath_v1'

    keywords = ['Character', 'Infantry', 'Psyker']
    power = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Astropath.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.TelepathicaStave)
            self.variant(*ranged.Laspistol)

    def __init__(self, parent):
        super(Astropath, self).__init__(parent, points=get_cost(units.Astropath))
        self.Weapon(self)


class OgrynBodyguard(ElitesUnit, armory.IGUnit):
    type_name = get_name(units.OgrynBodyguard)
    type_id = 'ogr_bodyguard_v1'
    keywords = ['Character', 'Infantry', 'Ogryn']
    power = 4

    faction = ['Militarum Auxilla']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(OgrynBodyguard.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.RipperGun)
            self.variant(*ranged.GrenadierGauntlet)
            self.variant(*melee.BullgrynMaul)

    class Knife(OneOf):
        def __init__(self, parent):
            super(OgrynBodyguard.Knife, self).__init__(parent, '')
            self.variant(*melee.HugeKnife)
            self.variant(*wargear.Slabshield)
            self.variant(*wargear.BruteShield)

    class Plate(OptionsList):
        def __init__(self, parent):
            super(OgrynBodyguard.Plate, self).__init__(parent, 'Armour')
            self.variant(*wargear.BullgrynPlate)

    def __init__(self, parent):
        super(OgrynBodyguard, self).__init__(parent, points=get_cost(units.OgrynBodyguard),
                                             gear=create_gears(ranged.FragBomb))
        self.Weapon(self)
        self.Knife(self)
        self.Plate(self)
