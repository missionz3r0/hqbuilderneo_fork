__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList, Count
from . import ranged, melee, wargear


def add_ranged_weapons(instance):
    instance.variant(*ranged.BoltPistol)
    instance.variant(*ranged.Boltgun)
    instance.variant(*ranged.PlasmaPistol)


def add_melee_weapons(instance):
    instance.variant(*melee.PowerSword)
    instance.variant(*melee.PowerFist)


def add_special_weapons(instance, rider=False, precise=False):
    instance.special = []
    if not rider:
        instance.special += [instance.variant(*ranged.SniperRifle)]
    instance.special += [
        instance.variant(*ranged.Flamer),
        instance.variant(*ranged.GrenadeLauncher)]
    if precise:
        instance.special.append(instance.variant(*ranged.MeltagunPrecise))
        instance.special.append(instance.variant(*ranged.PlasmaGunPrecise))
    else:
        instance.special.append(instance.variant(*ranged.Meltagun))
        instance.special.append(instance.variant(*ranged.PlasmaGun))


def add_heavy_weapons(instance):
    instance.variant(*ranged.Mortar)
    instance.variant(*ranged.Autocannon)
    instance.variant(*ranged.HeavyBolter)
    instance.variant(*ranged.MissileLauncher)
    instance.variant(*ranged.Lascannon)


class VehicleEquipment(OptionsList):
    def __init__(self, parent):
        super(VehicleEquipment, self).__init__(parent, name='Options')
        self.variant(*wargear.AugurArray)
        self.variant(*wargear.DozerBlade)
        self.hs = self.variant(*ranged.HeavyStubber)
        self.variant(*ranged.HunterKillerMissile)
        self.sb = self.variant(*ranged.StormBolter)
        self.variant(*wargear.TrackGuards)

    def check_rules(self):
        super(VehicleEquipment, self).check_rules()
        if self.sb and self.hs:
            self.process_limit([self.hs, self.sb], 1)


class IGUnit(Unit):
    faction = ['Imperium', 'Astra Militarum']
    wiki_faction = 'Astra Militarum'


class RegimentUnit(IGUnit):
    faction = ['<Regiment>']

    @classmethod
    def calc_faction(cls):
        return ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN',
                'ARMAGEDDON', 'TALLARN', 'MORDIAN']


class PrefectusUnit(IGUnit):
    faction = ['Officio Prefectus']


class PsykanaUnit(IGUnit):
    faction = ['Astra Telepathica', 'Scholastica Psykana']
    wiki_faction = 'Astra Telepathica'

