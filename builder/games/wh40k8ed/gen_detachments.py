from .rosters import *

from .necrons import *
from .adepta_sororitas import *
from .craftworld import *
from .chaos_other import *
from .heretic_astartes import *
from .drukhari import *
from .aeldari_other import *
from .chaos_demons import *
from .imperium_other import *
from .astra_militarum import *
from .adeptus_mechanicum import *
from .tau import *
from .space_marines import *
from .blood_angels import *
from .dark_angels import *
from .space_wolves import *
from .deathwatch import *
from .tyranids import *
from .orks import *
from .grey_knights import *
from .genestealer_cult import *
from .death_guard import *
from .adeptus_custodes import *
from .thousand_sons import *
from .imperial_knights import *
from .chaos_knights import *

from .obsolete_detachments import *

detachments = []

#################################################################
# Code below is generated
#################################################################


class DetachAuxilary_canoptek(DetachAuxilary):
    army_name = 'Canoptek (Auxilary Support Detachment)'
    faction_base = 'CANOPTEK'
    alternate_factions = []
    army_id = 'Auxilary_canoptek'
    army_factions = ['CANOPTEK']

    def __init__(self, parent=None):
        super(DetachAuxilary_canoptek, self).__init__(*[], **{'heavy': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([Spyders])
        self.fast.add_classes([Scarabs, Wraiths])
        return None


class SeptRoster(Roster):
    faction_base = '<SEPT>'
    alternate_factions = ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT", "KE'LSHAN SEPT"]
    army_factions = ['<SEPT>', "T'AU SEPT", "T'AU EMPIRE", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT", "KE'LSHAN SEPT"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(SeptRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import tau
        for sec in self.sections:
            sec.add_classes(tau.unit_types)


class DetachPatrol__sept_(SeptRoster, DetachPatrol):
    army_name = '<Sept> (Patrol detachment)'
    army_id = 'patrol__sept_'


class DetachBatallion__sept_(SeptRoster, DetachBatallion):
    army_name = '<Sept> (Batallion detachment)'
    army_id = 'batallion__sept_'


class DetachBrigade__sept_(SeptRoster, DetachBrigade):
    army_name = '<Sept> (Brigade detachment)'
    army_id = 'brigade__sept_'


class DetachVanguard__sept_(SeptRoster, DetachVanguard):
    army_name = '<Sept> (Vanguard detachment)'
    army_id = 'vanguard__sept_'


class DetachSpearhead__sept_(SeptRoster, DetachSpearhead):
    army_name = '<Sept> (Spearhead detachment)'
    army_id = 'spearhead__sept_'


class DetachOutrider__sept_(SeptRoster, DetachOutrider):
    army_name = '<Sept> (Outrider detachment)'
    army_id = 'outrider__sept_'


class DetachCommand__sept_(SeptRoster, DetachCommand):
    army_name = '<Sept> (Supreme command detachment)'
    army_id = 'command__sept_'


class DetachSuperHeavy__sept_(SeptRoster, DetachSuperHeavy):
    army_name = '<Sept> (Super-Heavy detachment)'
    army_id = 'super_heavy__sept_'


class DetachSuperHeavyAux__sept_(SeptRoster, DetachSuperHeavyAux):
    army_name = '<Sept> (Super-Heavy auxilary detachment)'
    army_id = 'super_heavy_aux__sept_'


class DetachAirWing__sept_(SeptRoster, DetachAirWing):
    army_name = '<Sept> (Air Wing detachment)'
    army_id = 'air_wing__sept_'


class DetachFort__sept_(DetachFort):
    army_name = '<Sept> (Fortification Network)'
    faction_base = '<SEPT>'
    army_id = 'fort__sept_'
    army_factions = ["T'AU EMPIRE", "T'AU SEPT", '<SEPT>', 'FARSIGHT ENCLAVES', "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT", "KE'LSHAN SEPT"]

    def __init__(self, parent=None):
        super(DetachFort__sept_, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([Shieldline, Droneport, Gunrig, RemoteSensorTower, DroneSentryTurrets])
        return None


class DetachAuxilary__sept_(DetachAuxilary):
    army_name = '<Sept> (Auxilary Support Detachment)'
    faction_base = '<SEPT>'
    alternate_factions = ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT", "KE'LSHAN SEPT"]
    army_id = 'Auxilary__sept_'
    army_factions = ["T'AU EMPIRE", "T'AU SEPT", '<SEPT>', 'FARSIGHT ENCLAVES', "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT", "KE'LSHAN SEPT"]

    def __init__(self, parent=None):
        super(DetachAuxilary__sept_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam, Rvarna, HeavyGunDrones, HBHammerhead, FSHammerhead])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark, RemoraSquadron, TigerShark, TigerSharkAX10, Barracuda])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman, Dx4Drones, HazardTeam])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike, ShasOrMyr, ShasOrAlai, XV81Commander, XV84Commander])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones, Yvahra, Tetras, PiranhaTX42Team])
        self.transports.add_classes([Devilfish])
        return None


class RavenwingRoster(Roster):
    faction_base = 'RAVENWING'
    alternate_factions = []
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'DARK ANGELS', 'RAVENWING', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(RavenwingRoster, self).__init__(*[], **{'hq': True, 'elite': True, 'fast': True, 'fliers': True, 'parent': parent, })
        from . import dark_angels
        for sec in self.sections:
            sec.add_classes(dark_angels.unit_types)


class DetachVanguard_ravenwing(RavenwingRoster, DetachVanguard):
    army_name = 'Ravenwing (Vanguard detachment)'
    army_id = 'vanguard_ravenwing'


class DetachOutrider_ravenwing(RavenwingRoster, DetachOutrider):
    army_name = 'Ravenwing (Outrider detachment)'
    army_id = 'outrider_ravenwing'


class DetachCommand_ravenwing(RavenwingRoster, DetachCommand):
    army_name = 'Ravenwing (Supreme command detachment)'
    army_id = 'command_ravenwing'


class DetachAirWing_ravenwing(DetachAirWing):
    army_name = 'Ravenwing (Air Wing detachment)'
    faction_base = 'RAVENWING'
    alternate_factions = []
    army_id = 'air_wing_ravenwing'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'DARK ANGELS', 'RAVENWING', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_ravenwing, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class DetachAuxilary_ravenwing(DetachAuxilary):
    army_name = 'Ravenwing (Auxilary Support Detachment)'
    faction_base = 'RAVENWING'
    alternate_factions = []
    army_id = 'Auxilary_ravenwing'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'DARK ANGELS', 'RAVENWING', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAuxilary_ravenwing, self).__init__(*[], **{'hq': True, 'elite': True, 'fast': True, 'fliers': True, 'parent': parent, })
        self.hq.add_classes([Sammael, SpeederSammael, RavenwingTalonmaster])
        self.elite.add_classes([RWAncient, RWApothecary, RWChampion])
        self.fast.add_classes([RavenwingBikeSquad, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, LandSpeederVengeance])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class BloodAngelRoster(Roster):
    faction_base = 'BLOOD ANGELS'
    alternate_factions = ['FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>']
    army_factions = ['IMPERIUM', 'FLESH TEARERS', 'BLOOD ANGELS', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES']
    
    def __init__(self, parent=None):
        super(BloodAngelRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import blood_angels
        from . import space_marines
        for sec in self.sections:
            sec.add_classes(blood_angels.unit_types + space_marines.unit_types)


class DetachPatrol_blood_angels(BloodAngelRoster, DetachPatrol):
    army_name = 'Blood Angels (Patrol detachment)'
    army_id = 'patrol_blood_angels'


class DetachBatallion_blood_angels(BloodAngelRoster, DetachBatallion):
    army_name = 'Blood Angels (Batallion detachment)'
    army_id = 'batallion_blood_angels'


class DetachBrigade_blood_angels(BloodAngelRoster, DetachBrigade):
    army_name = 'Blood Angels (Brigade detachment)'
    army_id = 'brigade_blood_angels'


class DetachVanguard_blood_angels(BloodAngelRoster, DetachVanguard):
    army_name = 'Blood Angels (Vanguard detachment)'
    army_id = 'vanguard_blood_angels'


class DetachSpearhead_blood_angels(BloodAngelRoster, DetachSpearhead):
    army_name = 'Blood Angels (Spearhead detachment)'
    army_id = 'spearhead_blood_angels'


class DetachOutrider_blood_angels(BloodAngelRoster, DetachOutrider):
    army_name = 'Blood Angels (Outrider detachment)'
    army_id = 'outrider_blood_angels'


class DetachCommand_blood_angels(BloodAngelRoster, DetachCommand):
    army_name = 'Blood Angels (Supreme command detachment)'
    army_id = 'command_blood_angels'


class DetachSuperHeavy_blood_angels(BloodAngelRoster, DetachSuperHeavy):
    army_name = 'Blood Angels (Super-Heavy detachment)'
    army_id = 'super_heavy_blood_angels'


class DetachSuperHeavyAux_blood_angels(BloodAngelRoster, DetachSuperHeavyAux):
    army_name = 'Blood Angels (Super-Heavy auxilary detachment)'
    army_id = 'super_heavy_aux_blood_angels'


class DetachAirWing_blood_angels(BloodAngelRoster, DetachAirWing):
    army_name = 'Blood Angels (Air Wing detachment)'
    army_id = 'air_wing_blood_angels'


class DetachAuxilary_blood_angels(DetachAuxilary):
    army_name = 'Blood Angels (Auxilary Support Detachment)'
    faction_base = 'BLOOD ANGELS'
    alternate_factions = ['FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>']
    army_id = 'Auxilary_blood_angels'
    army_factions = ['IMPERIUM', 'BLOOD ANGELS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAuxilary_blood_angels, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class AsuryaniRoster(Roster):
    faction_base = 'ASURYANI'
    alternate_factions = []
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(AsuryaniRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import craftworld
        for sec in self.sections:
            sec.add_classes(craftworld.unit_types)


class DetachPatrol_asuryani(AsuryaniRoster, DetachPatrol):
    army_name = 'Asuryani (Patrol detachment)'
    army_id = 'patrol_asuryani'


class DetachBatallion_asuryani(AsuryaniRoster, DetachBatallion):
    army_name = 'Asuryani (Batallion detachment)'
    army_id = 'batallion_asuryani'


class DetachBrigade_asuryani(AsuryaniRoster, DetachBrigade):
    army_name = 'Asuryani (Brigade detachment)'
    army_id = 'brigade_asuryani'


class DetachVanguard_asuryani(AsuryaniRoster, DetachVanguard):
    army_name = 'Asuryani (Vanguard detachment)'
    army_id = 'vanguard_asuryani'
    army_factions = AsuryaniRoster.army_factions + ['SPIRIT HOST']


class DetachSpearhead_asuryani(AsuryaniRoster, DetachSpearhead):
    army_name = 'Asuryani (Spearhead detachment)'
    army_id = 'spearhead_asuryani'
    army_factions = AsuryaniRoster.army_factions + ['SPIRIT HOST']


class DetachOutrider_asuryani(AsuryaniRoster, DetachOutrider):
    army_name = 'Asuryani (Outrider detachment)'
    army_id = 'outrider_asuryani'


class DetachCommand_asuryani(AsuryaniRoster, DetachCommand):
    army_name = 'Asuryani (Supreme command detachment)'
    army_id = 'command_asuryani'
    army_factions = AsuryaniRoster.army_factions + ['SPIRIT HOST']


class DetachSuperHeavy_asuryani(DetachSuperHeavy):
    army_name = 'Asuryani (Super-Heavy detachment)'
    faction_base = 'ASURYANI'
    alternate_factions = []
    army_id = 'super_heavy_asuryani'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_asuryani, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachSuperHeavyAux_asuryani(DetachSuperHeavyAux):
    army_name = 'Asuryani (Super-Heavy auxilary detachment)'
    faction_base = 'ASURYANI'
    alternate_factions = []
    army_id = 'super_heavy_aux_asuryani'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_asuryani, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachAirWing_asuryani(DetachAirWing):
    army_name = 'Asuryani (Air Wing detachment)'
    faction_base = 'ASURYANI'
    alternate_factions = []
    army_id = 'air_wing_asuryani'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'ASPECT WARRIOR', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAirWing_asuryani, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        return None


class DetachAuxilary_asuryani(DetachAuxilary):
    army_name = 'Asuryani (Auxilary Support Detachment)'
    faction_base = 'ASURYANI'
    alternate_factions = []
    army_id = 'Auxilary_asuryani'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'ASPECT WARRIOR', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAuxilary_asuryani, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class SlaaneshRoster(Roster):
    faction_base = 'SLAANESH'
    alternate_factions = []
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'HERETIC ASTARTES', 'SLAANESH', 'IRON WARRIORS', 'RED CORSAIRS', 'RENEGADES', "EMPEROR'S CHILDREN"]

    hq_sec = ApostleHQ
    elite_sec = DiscipleElites

    def __init__(self, parent=None):
        super(SlaaneshRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        from . import heretic_astartes
        from . import chaos_demons
        from . import chaos_other
        for sec in self.sections:
            sec.add_classes(heretic_astartes.unit_types + chaos_demons.unit_types + chaos_other.unit_types)


class DetachPatrol_slaanesh(SlaaneshRoster, DetachPatrol):
    army_name = 'Slaanesh (Patrol detachment)'
    army_id = 'patrol_slaanesh'


class DetachBatallion_slaanesh(SlaaneshRoster, DetachBatallion):
    army_name = 'Slaanesh (Batallion detachment)'
    army_id = 'batallion_slaanesh'


class DetachBrigade_slaanesh(SlaaneshRoster, DetachBrigade):
    army_name = 'Slaanesh (Brigade detachment)'
    army_id = 'brigade_slaanesh'


class DetachVanguard_slaanesh(SlaaneshRoster, DetachVanguard):
    army_name = 'Slaanesh (Vanguard detachment)'
    army_id = 'vanguard_slaanesh'


class DetachSpearhead_slaanesh(SlaaneshRoster, DetachSpearhead):
    army_name = 'Slaanesh (Spearhead detachment)'
    army_id = 'spearhead_slaanesh'


class DetachOutrider_slaanesh(SlaaneshRoster, DetachOutrider):
    army_name = 'Slaanesh (Outrider detachment)'
    army_id = 'outrider_slaanesh'


class DetachCommand_slaanesh(SlaaneshRoster, DetachCommand):
    army_name = 'Slaanesh (Supreme command detachment)'
    army_id = 'command_slaanesh'


class DetachAirWing_slaanesh(SlaaneshRoster, DetachAirWing):
    army_name = 'Slaanesh (Air Wing detachment)'
    army_id = 'air_wing_slaanesh'


class DetachAuxilary_slaanesh(DetachAuxilary):
    army_name = 'Slaanesh (Auxilary Support Detachment)'
    faction_base = 'SLAANESH'
    alternate_factions = []
    army_id = 'Auxilary_slaanesh'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'RED CORSAIRS', 'SLAANESH', 'WORD BEARERS', 'IRON WARRIORS', 'HERETIC ASTARTES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachAuxilary_slaanesh, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachAuxilary_skitarii(DetachAuxilary):
    army_name = 'Skitarii (Auxilary Support Detachment)'
    faction_base = 'SKITARII'
    alternate_factions = []
    army_id = 'Auxilary_skitarii'
    army_factions = ['SKITARII', 'IMPERIUM', 'ADEPTUS MECHANICUS', '<FORGE WORLD>',
                     'MARS', 'GRAIA', 'METALICA', 'LUCIUS',
                     'AGRIPINAA', 'STYGIES VIII', 'RYZA']

    def __init__(self, parent=None):
        super(DetachAuxilary_skitarii, self).__init__(*[], **{'heavy': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([Dunecrawler])
        self.elite.add_classes([Infiltrators, Ruststalkers])
        self.troops.add_classes([SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachVanguard_deathwing(DetachVanguard):
    army_name = 'Deathwing (Vanguard detachment)'
    faction_base = 'DEATHWING'
    alternate_factions = []
    army_id = 'vanguard_deathwing'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'DARK ANGELS', 'DEATHWING']

    hq_sec = DWCommand
    elite_sec = DWElites
    heavy_sec = DWHeavy

    def __init__(self, parent=None):
        super(DetachVanguard_deathwing, self).__init__(*[], heavy=True, **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([BikeLibrarian, Librarian, TermoLibrarian, CompanyMaster, Azrael, Belial, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, DATermoLibrarian, DAPhobosLibrarian])
        self.elite.add_classes([VenDreadnought, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad])
        self.heavy.add_classes([])
        return None


class DetachSpearhead_deathwing(DetachSpearhead):
    army_name = 'Deathwing (Spearhead detachment)'
    faction_base = 'DEATHWING'
    alternate_factions = []
    army_id = 'spearhead_deathwing'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'DARK ANGELS', 'DEATHWING']

    hq_sec = DWCommand
    elite_sec = DWElites
    heavy_sec = DWHeavy

    def __init__(self, parent=None):
        super(DetachSpearhead_deathwing, self).__init__(*[], heavy=True, **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([BikeLibrarian, Librarian, TermoLibrarian, CompanyMaster, Azrael, Belial, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, DATermoLibrarian, DAPhobosLibrarian])
        self.elite.add_classes([VenDreadnought, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad])
        self.heavy.add_classes([])
        return None


class DetachCommand_deathwing(DetachCommand):
    army_name = 'Deathwing (Supreme command detachment)'
    faction_base = 'DEATHWING'
    alternate_factions = []
    army_id = 'command_deathwing'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'DARK ANGELS', 'DEATHWING']

    hq_sec = DWCommand
    elite_sec = DWElites

    def __init__(self, parent=None):
        super(DetachCommand_deathwing, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([BikeLibrarian, Librarian, TermoLibrarian, CompanyMaster, Azrael, Belial, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, DATermoLibrarian, DAPhobosLibrarian])
        self.elite.add_classes([VenDreadnought, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad])
        return None


class DetachAuxilary_deathwing(DetachAuxilary):
    army_name = 'Deathwing (Auxilary Support Detachment)'
    faction_base = 'DEATHWING'
    alternate_factions = []
    army_id = 'Auxilary_deathwing'
    army_factions = ['DEATHWING', 'ADEPTUS ASTARTES', 'DARK ANGELS', 'DEATHWING']

    def __init__(self, parent=None):
        super(DetachAuxilary_deathwing, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([BikeLibrarian, Librarian, TermoLibrarian, CompanyMaster, Azrael, Belial, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, DATermoLibrarian, DAPhobosLibrarian])
        self.elite.add_classes([VenDreadnought, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad])
        return None


class DetachVanguard_astra_telepathica(DetachVanguard):
    army_name = 'Astra Telepathica (Vanguard detachment)'
    faction_base = 'ASTRA TELEPATHICA'
    alternate_factions = []
    army_id = 'vanguard_astra_telepathica'
    army_factions = ['SCHOLASTICA PSYKANA', 'ASTRA TELEPATHICA', 'IMPERIUM', 'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachVanguard_astra_telepathica, self).__init__(*[], **{'elite': True, 'hq': True, 'transports': True, 'parent': parent, })
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers, Wyrdvanes, Astropath])
        self.hq.add_classes([Primaris])
        self.transports.add_classes([NullRhino])
        return None


class DetachCommand_astra_telepathica(DetachCommand):
    army_name = 'Astra Telepathica (Supreme command detachment)'
    faction_base = 'ASTRA TELEPATHICA'
    alternate_factions = []
    army_id = 'command_astra_telepathica'
    army_factions = ['SCHOLASTICA PSYKANA', 'ASTRA TELEPATHICA', 'IMPERIUM', 'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachCommand_astra_telepathica, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([NullRhino])
        self.hq.add_classes([Primaris])
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers, Wyrdvanes, Astropath])
        return None


class DetachAuxilary_astra_telepathica(DetachAuxilary):
    army_name = 'Astra Telepathica (Auxilary Support Detachment)'
    faction_base = 'ASTRA TELEPATHICA'
    alternate_factions = []
    army_id = 'Auxilary_astra_telepathica'
    army_factions = ['SCHOLASTICA PSYKANA', 'ASTRA TELEPATHICA', 'IMPERIUM', 'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachAuxilary_astra_telepathica, self).__init__(*[], **{'elite': True, 'hq': True, 'transports': True, 'parent': parent, })
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers, Wyrdvanes, Astropath])
        self.hq.add_classes([Primaris])
        self.transports.add_classes([NullRhino])
        return None


class DetachVanguard_incubi(DetachVanguard):
    army_name = 'Incubi (Vanguard detachment)'
    faction_base = 'INCUBI'
    alternate_factions = []
    army_id = 'vanguard_incubi'
    army_factions = ['INCUBI', 'DRUKHARI', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachVanguard_incubi, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Drazhar])
        self.elite.add_classes([Incubi])
        return None


class DetachAuxilary_incubi(DetachAuxilary):
    army_name = 'Incubi (Auxilary Support Detachment)'
    faction_base = 'INCUBI'
    alternate_factions = []
    army_id = 'Auxilary_incubi'
    army_factions = ['INCUBI', 'DRUKHARI', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachAuxilary_incubi, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Drazhar])
        self.elite.add_classes([Incubi])
        return None


class BroodBrothersRoster(Roster):
    faction_base = 'ASTRA MILITARUM'
    alternate_factions = []

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['TYRANIDS', 'GENESTEALER CULTS']
    disable_selector = True

    @classmethod
    def infect(cls, unit_type):
        class Infected(unit_type):
            starrable = False
            faction = ['TYRANIDS', 'GENESTEALER CULTS', 'BROOD_BROTHERS']
        return Infected

    def __init__(self, parent=None):
        super(BroodBrothersRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import astra_militarum
        rest_units = [Priest, EnginseerV2, Enginseer]
        allowed_types = [BroodBrothersRoster.infect(unit_type) for unit_type in astra_militarum.unit_types + rest_units
                         if unit_type not in astra_militarum.named_units]
        for sec in self.sections:
            sec.add_classes(allowed_types)

    def build_statistics(self):
        res = super(BroodBrothersRoster, self).build_statistics()
        cp = res.get('Command points', 0)
        res['Command points'] = (cp + 1) / 2
        return res


class DetachPatrol_brood_brothers(BroodBrothersRoster, DetachPatrol):
    army_name = 'Astra Militarum (Patrol detachment)'
    army_id = 'patrol_astra_militarum'


class DetachBatallion_brood_brothers(BroodBrothersRoster, DetachBatallion):
    army_name = 'Astra Militarum (Batallion detachment)'
    army_id = 'batallion_astra_mil;itarum'


class DetachBrigade_brood_brothers(BroodBrothersRoster, DetachBrigade):
    army_name = 'Astra Militarum (Brigade detachment)'
    army_id = 'brigade_astra_militarum'


class DetachVanguard_brood_brothers(BroodBrothersRoster, DetachVanguard):
    army_name = 'Astra Militarum (Vanguard detachment)'
    army_id = 'vanguard_astra_militarum'


class DetachSpearhead_brood_brothers(BroodBrothersRoster, DetachSpearhead):
    army_name = 'Astra Militarum (Spearhead detachment)'
    army_id = 'spearhead_astra_militarum'


class DetachOutrider_brood_brothers(BroodBrothersRoster, DetachOutrider):
    army_name = 'Astra Militarum (Outrider detachment)'
    army_id = 'outrider_astra_militarum'


class DetachCommand_brood_brothers(BroodBrothersRoster, DetachCommand):
    army_name = 'Astra Militarum (Supreme command detachment)'
    army_id = 'command_astra_militarum'

class DetachSuperHeavy_brood_brothers(BroodBrothersRoster, DetachSuperHeavy):
    army_name = 'Astra Militarum (Super-Heavy detachment)'
    army_id = 'super_heavy_astra_militarum'


class DetachSuperHeavyAux_brood_brothers(BroodBrothersRoster, DetachSuperHeavyAux):
    army_name = 'Astra Militarum (Super-Heavy auxilary detachment)'
    army_id = 'super_heavy_aux_astra_militarum'


class DetachAirWing_brood_brothers(BroodBrothersRoster, DetachAirWing):
    army_name = 'Astra Militarum (Air Wing detachment)'
    army_id = 'air_wing_astra_militarum'


class AstraMilitarumRoster(Roster):
    faction_base = 'ASTRA MILITARUM'
    alternate_factions = []

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON', 'MILITARUM TEMPESTUS']

    def __init__(self, parent=None):
        super(AstraMilitarumRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import astra_militarum
        rest_units = [Priest, EnginseerV2, Enginseer]
        for sec in self.sections:
            sec.add_classes(astra_militarum.unit_types + rest_units)


class DetachPatrol_astra_militarum(AstraMilitarumRoster, DetachPatrol):
    army_name = 'Astra Militarum (Patrol detachment)'
    army_id = 'patrol_astra_militarum'


class DetachBatallion_astra_militarum(AstraMilitarumRoster, DetachBatallion):
    army_name = 'Astra Militarum (Batallion detachment)'
    army_id = 'batallion_astra_militarum'


class DetachBrigade_astra_militarum(AstraMilitarumRoster, DetachBrigade):
    army_name = 'Astra Militarum (Brigade detachment)'
    army_id = 'brigade_astra_militarum'


class DetachVanguard_astra_militarum(AstraMilitarumRoster, DetachVanguard):
    army_name = 'Astra Militarum (Vanguard detachment)'
    army_id = 'vanguard_astra_militarum'

    army_factions = AstraMilitarumRoster.army_factions + ['AGRIPINAA', 'METALICA', 'GRAIA', 'ASTRA TELEPATHICA', 'CULT MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'VOSTROYAN', 'RYZA', 'SCHOLASTICA PSYKANA', '<FORGE WORLD>', 'ADEPTUS MECHANICUS', 'MARS', 'OFFICIO PREFECTUS']


class DetachSpearhead_astra_militarum(AstraMilitarumRoster, DetachSpearhead):
    army_name = 'Astra Militarum (Spearhead detachment)'
    army_id = 'spearhead_astra_militarum'


class DetachOutrider_astra_militarum(AstraMilitarumRoster, DetachOutrider):
    army_name = 'Astra Militarum (Outrider detachment)'
    army_id = 'outrider_astra_militarum'


class DetachCommand_astra_militarum(AstraMilitarumRoster, DetachCommand):
    army_name = 'Astra Militarum (Supreme command detachment)'
    army_id = 'command_astra_militarum'

    army_factions = AstraMilitarumRoster.army_factions + ['AGRIPINAA', 'METALICA', 'GRAIA', 'ASTRA TELEPATHICA', 'CULT MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'VOSTROYAN', 'RYZA', 'SCHOLASTICA PSYKANA', '<FORGE WORLD>', 'ADEPTUS MECHANICUS', 'MARS', 'OFFICIO PREFECTUS']


class DetachSuperHeavy_astra_militarum(AstraMilitarumRoster, DetachSuperHeavy):
    army_name = 'Astra Militarum (Super-Heavy detachment)'
    army_id = 'super_heavy_astra_militarum'


class DetachSuperHeavyAux_astra_militarum(AstraMilitarumRoster, DetachSuperHeavyAux):
    army_name = 'Astra Militarum (Super-Heavy auxilary detachment)'
    army_id = 'super_heavy_aux_astra_militarum'


class DetachAirWing_astra_militarum(AstraMilitarumRoster, DetachAirWing):
    army_name = 'Astra Militarum (Air Wing detachment)'
    army_id = 'air_wing_astra_militarum'


class DetachAuxilary_astra_militarum(DetachAuxilary):
    army_name = 'Astra Militarum (Auxilary Support Detachment)'
    faction_base = 'ASTRA MILITARUM'
    alternate_factions = []
    army_id = 'Auxilary_astra_militarum'
    army_factions = AstraMilitarumRoster.army_factions + ['AGRIPINAA', 'METALICA', 'GRAIA', 'ASTRA TELEPATHICA', 'CULT MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'VOSTROYAN', 'RYZA', 'SCHOLASTICA PSYKANA', '<FORGE WORLD>', 'ADEPTUS MECHANICUS', 'MARS', 'OFFICIO PREFECTUS', 'ADEPTUS MINISTORUM', 'AERONAUTICA IMPERIALIS']

    def __init__(self, parent=None):
        super(DetachAuxilary_astra_militarum, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachPatrol__regiment_(DetachPatrol):
    army_name = '<Regiment> (Patrol detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'patrol__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPatrol__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachBatallion__regiment_(DetachBatallion):
    army_name = '<Regiment> (Batallion detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'batallion__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachBatallion__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachBrigade__regiment_(DetachBrigade):
    army_name = '<Regiment> (Brigade detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'brigade__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachBrigade__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachVanguard__regiment_(DetachVanguard):
    army_name = '<Regiment> (Vanguard detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'vanguard__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachVanguard__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachSpearhead__regiment_(DetachSpearhead):
    army_name = '<Regiment> (Spearhead detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'spearhead__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachSpearhead__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachOutrider__regiment_(DetachOutrider):
    army_name = '<Regiment> (Outrider detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'outrider__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachOutrider__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachCommand__regiment_(DetachCommand):
    army_name = '<Regiment> (Supreme command detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'command__regiment_'

    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachCommand__regiment_, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        self.transports.add_classes([Chimera, Taurox])
        return None


class DetachSuperHeavy__regiment_(DetachSuperHeavy):
    army_name = '<Regiment> (Super-Heavy detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'super_heavy__regiment_'
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__regiment_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        return None


class DetachSuperHeavyAux__regiment_(DetachSuperHeavyAux):
    army_name = '<Regiment> (Super-Heavy auxilary detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'super_heavy_aux__regiment_'
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__regiment_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword])
        return None


class DetachAuxilary__regiment_(DetachAuxilary):
    army_name = '<Regiment> (Auxilary Support Detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'Auxilary__regiment_'
    army_factions = ['<REGIMENT>']

    def __init__(self, parent=None):
        super(DetachAuxilary__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachAuxilary_legion_of_the_damned(DetachAuxilary):
    army_name = 'Legion Of The Damned (Auxilary Support Detachment)'
    faction_base = 'LEGION OF THE DAMNED'
    alternate_factions = []
    army_id = 'Auxilary_legion_of_the_damned'
    army_factions = ['LEGION OF THE DAMNED']

    def __init__(self, parent=None):
        super(DetachAuxilary_legion_of_the_damned, self).__init__(*[], **{'elite': True, 'parent': parent, })
        self.elite.add_classes([TheDamned])
        return None


class DetachPatrol__mascue_(DetachPatrol):
    army_name = '<Mascue> (Patrol detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'patrol__mascue_'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPatrol__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachBatallion__mascue_(DetachBatallion):
    army_name = '<Mascue> (Batallion detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'batallion__mascue_'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachBatallion__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachBrigade__mascue_(DetachBrigade):
    army_name = '<Mascue> (Brigade detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'brigade__mascue_'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachBrigade__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachVanguard__mascue_(DetachVanguard):
    army_name = '<Mascue> (Vanguard detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'vanguard__mascue_'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachVanguard__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachSpearhead__mascue_(DetachSpearhead):
    army_name = '<Mascue> (Spearhead detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'spearhead__mascue_'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachSpearhead__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachOutrider__mascue_(DetachOutrider):
    army_name = '<Mascue> (Outrider detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'outrider__mascue_'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachOutrider__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachCommand__mascue_(DetachCommand):
    army_name = '<Mascue> (Supreme command detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'command__mascue_'
    army_factions = ['<MASCUE>', 'YNNARI', 'HARLEQUINS', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand__mascue_, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachAuxilary__mascue_(DetachAuxilary):
    army_name = '<Mascue> (Auxilary Support Detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'Auxilary__mascue_'
    army_factions = ['<MASCUE>']

    def __init__(self, parent=None):
        super(DetachAuxilary__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DeathwatchRoster(Roster):
    faction_base = 'DEATHWATCH'
    alternate_factions = []
    army_factions = ['DEATHWATCH', 'IMPERIUM', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DeathwatchRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import space_marines
        from . import deathwatch
        for sec in self.sections:
            sec.add_classes(space_marines.unit_types + deathwatch.unit_types)


class DetachPatrol_deathwatch(DeathwatchRoster, DetachPatrol):
    army_name = 'Deathwatch (Patrol detachment)'
    army_id = 'patrol_deathwatch'


class DetachBatallion_deathwatch(DeathwatchRoster, DetachBatallion):
    army_name = 'Deathwatch (Batallion detachment)'
    army_id = 'batallion_deathwatch'


class DetachBrigade_deathwatch(DeathwatchRoster, DetachBrigade):
    army_name = 'Deathwatch (Brigade detachment)'
    army_id = 'brigade_deathwatch'


class DetachVanguard_deathwatch(DeathwatchRoster, DetachVanguard):
    army_name = 'Deathwatch (Vanguard detachment)'
    army_id = 'vanguard_deathwatch'


class DetachSpearhead_deathwatch(DeathwatchRoster, DetachSpearhead):
    army_name = 'Deathwatch (Spearhead detachment)'
    army_id = 'spearhead_deathwatch'


class DetachOutrider_deathwatch(DeathwatchRoster, DetachOutrider):
    army_name = 'Deathwatch (Outrider detachment)'
    army_id = 'outrider_deathwatch'


class DetachCommand_deathwatch(DeathwatchRoster, DetachCommand):
    army_name = 'Deathwatch (Supreme command detachment)'
    army_id = 'command_deathwatch'


class DetachSuperHeavy_deathwatch(DetachSuperHeavy):
    army_name = 'Deathwatch (Super-Heavy detachment)'
    faction_base = 'DEATHWATCH'
    alternate_factions = []
    army_id = 'super_heavy_deathwatch'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'DEATHWATCH']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_deathwatch, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_deathwatch(DetachSuperHeavyAux):
    army_name = 'Deathwatch (Super-Heavy auxilary detachment)'
    faction_base = 'DEATHWATCH'
    alternate_factions = []
    army_id = 'super_heavy_aux_deathwatch'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'DEATHWATCH']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_deathwatch, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_deathwatch(DetachAirWing):
    army_name = 'Deathwatch (Air Wing detachment)'
    faction_base = 'DEATHWATCH'
    alternate_factions = []
    army_id = 'air_wing_deathwatch'
    army_factions = ['DEATHWATCH', 'IMPERIUM', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_deathwatch, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Corvus])
        return None


class DetachAuxilary_deathwatch(DetachAuxilary):
    army_name = 'Deathwatch (Auxilary Support Detachment)'
    faction_base = 'DEATHWATCH'
    alternate_factions = []
    army_id = 'Auxilary_deathwatch'
    army_factions = ['DEATHWATCH']

    def __init__(self, parent=None):
        super(DetachAuxilary_deathwatch, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachVanguard_officio_prefectus(DetachVanguard):
    army_name = 'Officio Prefectus (Vanguard detachment)'
    faction_base = 'OFFICIO PREFECTUS'
    alternate_factions = []
    army_id = 'vanguard_officio_prefectus'
    army_factions = ['IMPERIUM', 'OFFICIO PREFECTUS', 'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachVanguard_officio_prefectus, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([LordCommissar, Yarrik])
        self.elite.add_classes([Commissar])
        return None


class DetachCommand_officio_prefectus(DetachCommand):
    army_name = 'Officio Prefectus (Supreme command detachment)'
    faction_base = 'OFFICIO PREFECTUS'
    alternate_factions = []
    army_id = 'command_officio_prefectus'
    army_factions = ['IMPERIUM', 'OFFICIO PREFECTUS', 'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachCommand_officio_prefectus, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([LordCommissar, Yarrik])
        self.elite.add_classes([Commissar])
        return None


class DetachAuxilary_officio_prefectus(DetachAuxilary):
    army_name = 'Officio Prefectus (Auxilary Support Detachment)'
    faction_base = 'OFFICIO PREFECTUS'
    alternate_factions = []
    army_id = 'Auxilary_officio_prefectus'
    army_factions = ['OFFICIO PREFECTUS']

    def __init__(self, parent=None):
        super(DetachAuxilary_officio_prefectus, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([LordCommissar, Yarrik])
        self.elite.add_classes([Commissar])
        return None


class SpaceWolvesRoster(Roster):
    faction_base = 'SPACE WOLVES'
    alternate_factions = []
    army_factions = ['IMPERIUM', 'SPACE WOLVES', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(SpaceWolvesRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import space_marines
        from . import space_wolves
        for sec in self.sections:
            sec.add_classes(space_marines.unit_types + space_wolves.unit_types)


class DetachPatrol_space_wolves(SpaceWolvesRoster, DetachPatrol):
    army_name = 'Space Wolves (Patrol detachment)'
    army_id = 'patrol_space_wolves'


class DetachBatallion_space_wolves(SpaceWolvesRoster, DetachBatallion):
    army_name = 'Space Wolves (Batallion detachment)'
    army_id = 'batallion_space_wolves'


class DetachBrigade_space_wolves(SpaceWolvesRoster, DetachBrigade):
    army_name = 'Space Wolves (Brigade detachment)'
    army_id = 'brigade_space_wolves'


class DetachVanguard_space_wolves(SpaceWolvesRoster, DetachVanguard):
    army_name = 'Space Wolves (Vanguard detachment)'
    army_id = 'vanguard_space_wolves'


class DetachSpearhead_space_wolves(SpaceWolvesRoster, DetachSpearhead):
    army_name = 'Space Wolves (Spearhead detachment)'
    army_id = 'spearhead_space_wolves'


class DetachOutrider_space_wolves(SpaceWolvesRoster, DetachOutrider):
    army_name = 'Space Wolves (Outrider detachment)'
    army_id = 'outrider_space_wolves'


class DetachCommand_space_wolves(SpaceWolvesRoster, DetachCommand):
    army_name = 'Space Wolves (Supreme command detachment)'
    army_id = 'command_space_wolves'


class DetachSuperHeavy_space_wolves(DetachSuperHeavy):
    army_name = 'Space Wolves (Super-Heavy detachment)'
    faction_base = 'SPACE WOLVES'
    alternate_factions = []
    army_id = 'super_heavy_space_wolves'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'SPACE WOLVES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_space_wolves, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_space_wolves(DetachSuperHeavyAux):
    army_name = 'Space Wolves (Super-Heavy auxilary detachment)'
    faction_base = 'SPACE WOLVES'
    alternate_factions = []
    army_id = 'super_heavy_aux_space_wolves'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'SPACE WOLVES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_space_wolves, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_space_wolves(DetachAirWing):
    army_name = 'Space Wolves (Air Wing detachment)'
    faction_base = 'SPACE WOLVES'
    alternate_factions = []
    army_id = 'air_wing_space_wolves'
    army_factions = ['IMPERIUM', 'SPACE WOLVES', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_space_wolves, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        return None


class DetachAuxilary_space_wolves(DetachAuxilary):
    army_name = 'Space Wolves (Auxilary Support Detachment)'
    faction_base = 'SPACE WOLVES'
    alternate_factions = []
    army_id = 'Auxilary_space_wolves'
    army_factions = ['SPACE WOLVES']

    def __init__(self, parent=None):
        super(DetachAuxilary_space_wolves, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer,
                                SWEliminators])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors], SWInfiltratorSquad)
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord,
                             PhobosWolflord, PhobosRunePriest, PhobosBattleLeader])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors, SWSuppressors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class TzeentchRoster(Roster):
    faction_base = 'TZEENTCH'
    alternate_factions = []
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'HERETIC ASTARTES', 'IRON WARRIORS', 'THOUSAND SONS', 'RED CORSAIRS', 'RENEGADES']

    hq_sec = ApostleHQ
    elite_sec = DiscipleElites
    
    def __init__(self, parent=None):
        super(TzeentchRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import chaos_demons
        from . import heretic_astartes
        from . import thousand_sons
        for sec in self.sections:
            sec.add_classes(chaos_demons.unit_types + heretic_astartes.unit_types + thousand_sons.unit_types)


class DetachPatrol_tzeentch(TzeentchRoster, DetachPatrol):
    army_name = 'Tzeentch (Patrol detachment)'
    army_id = 'patrol_tzeentch'


class DetachBatallion_tzeentch(TzeentchRoster, DetachBatallion):
    army_name = 'Tzeentch (Batallion detachment)'
    army_id = 'batallion_tzeentch'


class DetachBrigade_tzeentch(TzeentchRoster, DetachBrigade):
    army_name = 'Tzeentch (Brigade detachment)'
    army_id = 'brigade_tzeentch'


class DetachVanguard_tzeentch(TzeentchRoster, DetachVanguard):
    army_name = 'Tzeentch (Vanguard detachment)'
    army_id = 'vanguard_tzeentch'


class DetachSpearhead_tzeentch(TzeentchRoster, DetachSpearhead):
    army_name = 'Tzeentch (Spearhead detachment)'
    army_id = 'spearhead_tzeentch'


class DetachOutrider_tzeentch(TzeentchRoster, DetachOutrider):
    army_name = 'Tzeentch (Outrider detachment)'
    army_id = 'outrider_tzeentch'


class DetachCommand_tzeentch(TzeentchRoster, DetachCommand):
    army_name = 'Tzeentch (Supreme command detachment)'
    army_id = 'command_tzeentch'


class DetachSuperHeavyAux_tzeentch(DetachSuperHeavyAux):
    army_name = 'Tzeentch (Super-Heavy auxilary detachment)'
    faction_base = 'TZEENTCH'
    alternate_factions = []
    army_id = 'super_heavy_aux_tzeentch'
    army_factions = ['THOUSAND SONS', 'TZEENTCH', 'CHAOS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_tzeentch, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Magnus])
        return None


class DetachAirWing_tzeentch(DetachAirWing):
    army_name = 'Tzeentch (Air Wing detachment)'
    faction_base = 'TZEENTCH'
    alternate_factions = []
    army_id = 'air_wing_tzeentch'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'RED CORSAIRS', 'THOUSAND SONS', 'WORD BEARERS', 'IRON WARRIORS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_tzeentch, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_tzeentch(DetachAuxilary):
    army_name = 'Tzeentch (Auxilary Support Detachment)'
    faction_base = 'TZEENTCH'
    alternate_factions = []
    army_id = 'Auxilary_tzeentch'
    army_factions = ['TZEENTCH']

    def __init__(self, parent=None):
        super(DetachAuxilary_tzeentch, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith, Havocsv2, Venomcrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators, GreaterPossessed, DarkDisciples])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer, MasterOfPossession, MasterOfExecutions, LordDiscordant])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPatrol_drukhari(DetachPatrol):
    army_name = 'Drukhari (Patrol detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'patrol_drukhari'
    army_factions = ['DRUKHARI', 'AELDARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', '<WYCH CULT>']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachPatrol_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachBatallion_drukhari(DetachBatallion):
    army_name = 'Drukhari (Batallion detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'batallion_drukhari'
    army_factions = ['DRUKHARI', 'AELDARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', '<WYCH CULT>']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachBatallion_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachBrigade_drukhari(DetachBrigade):
    army_name = 'Drukhari (Brigade detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'brigade_drukhari'
    army_factions = ['YNNARI', 'DRUKHARI', 'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachBrigade_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachVanguard_drukhari(DetachVanguard):
    army_name = 'Drukhari (Vanguard detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'vanguard_drukhari'
    army_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', '<WYCH CULT>', 'AELDARI', 'INCUBI', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', 'DRUKHARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachVanguard_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachSpearhead_drukhari(DetachSpearhead):
    army_name = 'Drukhari (Spearhead detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'spearhead_drukhari'
    army_factions = ['AELDARI', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', 'DRUKHARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachSpearhead_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachOutrider_drukhari(DetachOutrider):
    army_name = 'Drukhari (Outrider detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'outrider_drukhari'
    army_factions = ['<WYCH CULT>', 'DRUKHARI', 'YNNARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast

    def __init__(self, parent=None):
        super(DetachOutrider_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachCommand_drukhari(DetachCommand):
    army_name = 'Drukhari (Supreme command detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'command_drukhari'
    army_factions = ['AELDARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'INCUBI', '<WYCH CULT>', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', 'DRUKHARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachCommand_drukhari, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        return None


class DetachAirWing_drukhari(DetachAirWing):
    army_name = 'Drukhari (Air Wing detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'air_wing_drukhari'
    army_factions = ['AELDARI', 'DRUKHARI', '<WYCH CULT>', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachAirWing_drukhari, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Razorwing, Voidraven])
        return None


class DetachAuxilary_drukhari(DetachAuxilary):
    army_name = 'Drukhari (Auxilary Support Detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'Auxilary_drukhari'
    army_factions = ['DRUKHARI']

    def __init__(self, parent=None):
        super(DetachAuxilary_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class HereticAstartesRoster(Roster):
    faction_base = 'HERETIC ASTARTES'
    alternate_factions = []
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'KHORNE', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'RED CORSAIRS', 'RENEGADES', "EMPEROR'S CHILDREN"]

    hq_sec = ApostleHQ
    elite_sec = DiscipleElites
    
    def __init__(self, parent=None):
        super(HereticAstartesRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import death_guard
        from . import thousand_sons
        from . import heretic_astartes
        for sec in self.sections:
            sec.add_classes(death_guard.unit_types + thousand_sons.unit_types + heretic_astartes.unit_types)


class DetachPatrol_heretic_astartes(HereticAstartesRoster, DetachPatrol):
    army_name = 'Heretic Astartes (Patrol detachment)'
    army_id = 'patrol_heretic_astartes'


class DetachBatallion_heretic_astartes(HereticAstartesRoster, DetachBatallion):
    army_name = 'Heretic Astartes (Batallion detachment)'
    army_id = 'batallion_heretic_astartes'


class DetachBrigade_heretic_astartes(HereticAstartesRoster, DetachBrigade):
    army_name = 'Heretic Astartes (Brigade detachment)'
    army_id = 'brigade_heretic_astartes'


class DetachVanguard_heretic_astartes(HereticAstartesRoster, DetachVanguard):
    army_name = 'Heretic Astartes (Vanguard detachment)'
    army_id = 'vanguard_heretic_astartes'


class DetachSpearhead_heretic_astartes(HereticAstartesRoster, DetachSpearhead):
    army_name = 'Heretic Astartes (Spearhead detachment)'
    army_id = 'spearhead_heretic_astartes'


class DetachOutrider_heretic_astartes(HereticAstartesRoster, DetachOutrider):
    army_name = 'Heretic Astartes (Outrider detachment)'
    army_id = 'outrider_heretic_astartes'


class DetachCommand_heretic_astartes(HereticAstartesRoster, DetachCommand):
    army_name = 'Heretic Astartes (Supreme command detachment)'
    army_id = 'command_heretic_astartes'


class DetachSuperHeavy_heretic_astartes(DetachSuperHeavy):
    army_name = 'Heretic Astartes (Super-Heavy detachment)'
    faction_base = 'HERETIC ASTARTES'
    alternate_factions = []
    army_id = 'super_heavy_heretic_astartes'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'KHORNE', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_heretic_astartes, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([LordOfSkulls, Mortarion, Magnus])
        return None


class DetachSuperHeavyAux_heretic_astartes(DetachSuperHeavyAux):
    army_name = 'Heretic Astartes (Super-Heavy auxilary detachment)'
    faction_base = 'HERETIC ASTARTES'
    alternate_factions = []
    army_id = 'super_heavy_aux_heretic_astartes'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'KHORNE', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_heretic_astartes, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([LordOfSkulls, Mortarion, Magnus])
        return None


class DetachAirWing_heretic_astartes(DetachAirWing):
    army_name = 'Heretic Astartes (Air Wing detachment)'
    faction_base = 'HERETIC ASTARTES'
    alternate_factions = []
    army_id = 'air_wing_heretic_astartes'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'RED CORSAIRS', 'SLAANESH', 'THOUSAND SONS', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'KHORNE', 'HERETIC ASTARTES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachAirWing_heretic_astartes, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_heretic_astartes(DetachAuxilary):
    army_name = 'Heretic Astartes (Auxilary Support Detachment)'
    faction_base = 'HERETIC ASTARTES'
    alternate_factions = []
    army_id = 'Auxilary_heretic_astartes'
    army_factions = ['HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachAuxilary_heretic_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith, Venomcrawler, Havocsv2])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators, DarkDisciples, GreaterPossessed])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer, MasterOfPossession, MasterOfExecutions, LordDiscordant])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPatrol_thousand_sons(DetachPatrol):
    army_name = 'Thousand Sons (Patrol detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'patrol_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPatrol_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_thousand_sons(DetachBatallion):
    army_name = 'Thousand Sons (Batallion detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'batallion_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachBatallion_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_thousand_sons(DetachBrigade):
    army_name = 'Thousand Sons (Brigade detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'brigade_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachBrigade_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_thousand_sons(DetachVanguard):
    army_name = 'Thousand Sons (Vanguard detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'vanguard_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachVanguard_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_thousand_sons(DetachSpearhead):
    army_name = 'Thousand Sons (Spearhead detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'spearhead_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSpearhead_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_thousand_sons(DetachOutrider):
    army_name = 'Thousand Sons (Outrider detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'outrider_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachOutrider_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_thousand_sons(DetachCommand):
    army_name = 'Thousand Sons (Supreme command detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'command_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachCommand_thousand_sons, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Magnus])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_thousand_sons(DetachSuperHeavy):
    army_name = 'Thousand Sons (Super-Heavy detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'super_heavy_thousand_sons'
    army_factions = ['THOUSAND SONS', 'TZEENTCH', 'CHAOS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_thousand_sons, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Magnus])
        return None


class DetachSuperHeavyAux_thousand_sons(DetachSuperHeavyAux):
    army_name = 'Thousand Sons (Super-Heavy auxilary detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'super_heavy_aux_thousand_sons'
    army_factions = ['THOUSAND SONS', 'TZEENTCH', 'CHAOS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_thousand_sons, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Magnus])
        return None


class DetachAirWing_thousand_sons(DetachAirWing):
    army_name = 'Thousand Sons (Air Wing detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'air_wing_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachAirWing_thousand_sons, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_thousand_sons(DetachAuxilary):
    army_name = 'Thousand Sons (Auxilary Support Detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'Auxilary_thousand_sons'
    army_factions = ['THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachAuxilary_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class LegionRoster(Roster):
    faction_base = '<LEGION>'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN"]
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'KHORNE', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'RED CORSAIRS', 'RENEGADES', "EMPEROR'S CHILDREN"]

    hq_sec = ApostleHQ
    elite_sec = DiscipleElites
    
    def __init__(self, parent=None):
        super(LegionRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import heretic_astartes
        for sec in self.sections:
            sec.add_classes(heretic_astartes.unit_types)


class DetachPatrol__legion_(LegionRoster, DetachPatrol):
    army_name = '<Legion> (Patrol detachment)'
    army_id = 'patrol__legion_'


class DetachBatallion__legion_(LegionRoster, DetachBatallion):
    army_name = '<Legion> (Batallion detachment)'
    army_id = 'batallion__legion_'


class DetachBrigade__legion_(LegionRoster, DetachBrigade):
    army_name = '<Legion> (Brigade detachment)'
    army_id = 'brigade__legion_'


class DetachVanguard__legion_(LegionRoster, DetachVanguard):
    army_name = '<Legion> (Vanguard detachment)'
    army_id = 'vanguard__legion_'


class DetachSpearhead__legion_(LegionRoster, DetachSpearhead):
    army_name = '<Legion> (Spearhead detachment)'
    army_id = 'spearhead__legion_'


class DetachOutrider__legion_(LegionRoster, DetachOutrider):
    army_name = '<Legion> (Outrider detachment)'
    army_id = 'outrider__legion_'


class DetachCommand__legion_(LegionRoster, DetachCommand):
    army_name = '<Legion> (Supreme command detachment)'
    army_id = 'command__legion_'


class DetachSuperHeavy__legion_(DetachSuperHeavy):
    army_name = '<Legion> (Super-Heavy detachment)'
    faction_base = '<LEGION>'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN"]
    army_id = 'super_heavy__legion_'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'KHORNE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__legion_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([LordOfSkulls])
        return None


class DetachSuperHeavyAux__legion_(DetachSuperHeavyAux):
    army_name = '<Legion> (Super-Heavy auxilary detachment)'
    faction_base = '<LEGION>'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN"]
    army_id = 'super_heavy_aux__legion_'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'KHORNE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__legion_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([LordOfSkulls])
        return None


class DetachAirWing__legion_(DetachAirWing):
    army_name = '<Legion> (Air Wing detachment)'
    faction_base = '<LEGION>'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN"]
    army_id = 'air_wing__legion_'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'RED CORSAIRS', 'SLAANESH', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'KHORNE', 'HERETIC ASTARTES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachAirWing__legion_, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary__legion_(DetachAuxilary):
    army_name = '<Legion> (Auxilary Support Detachment)'
    faction_base = '<LEGION>'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN", 'THOUSAND SONS']
    army_id = 'Auxilary__legion_'
    army_factions = ['<LEGION>']

    def __init__(self, parent=None):
        super(DetachAuxilary__legion_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, Havocsv2, Venomcrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines,  PlagueMarinesV2, GreaterPossessed, DarkDisciples])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, MasterOfExecutions, MasterOfExecutions, LordDiscordant])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DarkAngelsRoster(Roster):
    faction_base = 'DARK ANGELS'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'DARK ANGELS']

    def __init__(self, parent=None):
        super(DarkAngelsRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import space_marines
        from . import dark_angels
        for sec in self.sections:
            sec.add_classes(space_marines.unit_types + dark_angels.unit_types)


class DetachPatrol_dark_angels(DarkAngelsRoster, DetachPatrol):
    army_name = 'Dark Angels (Patrol detachment)'
    army_id = 'patrol_dark_angels'


class DetachBatallion_dark_angels(DarkAngelsRoster, DetachBatallion):
    army_name = 'Dark Angels (Batallion detachment)'
    army_id = 'batallion_dark_angels'


class DetachBrigade_dark_angels(DarkAngelsRoster, DetachBrigade):
    army_name = 'Dark Angels (Brigade detachment)'
    army_id = 'brigade_dark_angels'


class DetachVanguard_dark_angels(DarkAngelsRoster, DetachVanguard):
    army_name = 'Dark Angels (Vanguard detachment)'
    army_id = 'vanguard_dark_angels'


class DetachSpearhead_dark_angels(DarkAngelsRoster, DetachSpearhead):
    army_name = 'Dark Angels (Spearhead detachment)'
    army_id = 'spearhead_dark_angels'


class DetachOutrider_dark_angels(DarkAngelsRoster, DetachOutrider):
    army_name = 'Dark Angels (Outrider detachment)'
    army_id = 'outrider_dark_angels'


class DetachCommand_dark_angels(DarkAngelsRoster, DetachCommand):
    army_name = 'Dark Angels (Supreme command detachment)'
    army_id = 'command_dark_angels'


class DetachSuperHeavy_dark_angels(DetachSuperHeavy):
    army_name = 'Dark Angels (Super-Heavy detachment)'
    faction_base = 'DARK ANGELS'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'super_heavy_dark_angels'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', '<DARK ANGELS SUCCESSORS>', 'DARK ANGELS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_dark_angels, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_dark_angels(DetachSuperHeavyAux):
    army_name = 'Dark Angels (Super-Heavy auxilary detachment)'
    faction_base = 'DARK ANGELS'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'super_heavy_aux_dark_angels'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', '<DARK ANGELS SUCCESSORS>', 'DARK ANGELS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_dark_angels, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_dark_angels(DetachAirWing):
    army_name = 'Dark Angels (Air Wing detachment)'
    faction_base = 'DARK ANGELS'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'air_wing_dark_angels'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'DARK ANGELS', 'RAVENWING', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_dark_angels, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter])
        return None


class DetachAuxilary_dark_angels(DetachAuxilary):
    army_name = 'Dark Angels (Auxilary Support Detachment)'
    faction_base = 'DARK ANGELS'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'Auxilary_dark_angels'
    army_factions = ['DARK ANGELS']

    def __init__(self, parent=None):
        super(DetachAuxilary_dark_angels, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexDevastators, CodexPredator], DAEliminators)
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad, DAInfiltratorSquad])
        self.fliers.add_classes([RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, RelicContemptor, RelicDeredeo])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, PhobosMaster, DAPhobosLibrarian, DAPhobosLieutenant, DALibrarian])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, DASupressors])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class KhorneRoster(Roster):
    faction_base = 'KHORNE'
    alternate_factions = []
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'KHORNE', 'HERETIC ASTARTES', 'WORLD EATERS', 'IRON WARRIORS', 'RED CORSAIRS', 'RENEGADES']

    hq_sec = ApostleHQ
    elite_sec = DiscipleElites
    
    def __init__(self, parent=None):
        super(KhorneRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import chaos_demons
        from . import heretic_astartes
        for sec in self.sections:
            sec.add_classes(chaos_demons.unit_types + heretic_astartes.unit_types)


class DetachPatrol_khorne(KhorneRoster, DetachPatrol):
    army_name = 'Khorne (Patrol detachment)'
    army_id = 'patrol_khorne'


class DetachBatallion_khorne(KhorneRoster, DetachBatallion):
    army_name = 'Khorne (Batallion detachment)'
    army_id = 'batallion_khorne'


class DetachBrigade_khorne(KhorneRoster, DetachBrigade):
    army_name = 'Khorne (Brigade detachment)'
    army_id = 'brigade_khorne'


class DetachVanguard_khorne(KhorneRoster, DetachVanguard):
    army_name = 'Khorne (Vanguard detachment)'
    army_id = 'vanguard_khorne'


class DetachSpearhead_khorne(KhorneRoster, DetachSpearhead):
    army_name = 'Khorne (Spearhead detachment)'
    army_id = 'spearhead_khorne'


class DetachOutrider_khorne(KhorneRoster, DetachOutrider):
    army_name = 'Khorne (Outrider detachment)'
    army_id = 'outrider_khorne'


class DetachCommand_khorne(KhorneRoster, DetachCommand):
    army_name = 'Khorne (Supreme command detachment)'
    army_id = 'command_khorne'


class DetachSuperHeavy_khorne(DetachSuperHeavy):
    army_name = 'Khorne (Super-Heavy detachment)'
    faction_base = 'KHORNE'
    alternate_factions = []
    army_id = 'super_heavy_khorne'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'KHORNE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_khorne, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([LordOfSkulls])
        return None


class DetachSuperHeavyAux_khorne(DetachSuperHeavyAux):
    army_name = 'Khorne (Super-Heavy auxilary detachment)'
    faction_base = 'KHORNE'
    alternate_factions = []
    army_id = 'super_heavy_aux_khorne'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'KHORNE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_khorne, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([LordOfSkulls])
        return None


class DetachAirWing_khorne(DetachAirWing):
    army_name = 'Khorne (Air Wing detachment)'
    faction_base = 'KHORNE'
    alternate_factions = []
    army_id = 'air_wing_khorne'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'RED CORSAIRS', 'WORD BEARERS', 'IRON WARRIORS', 'KHORNE', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_khorne, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_khorne(DetachAuxilary):
    army_name = 'Khorne (Auxilary Support Detachment)'
    faction_base = 'KHORNE'
    alternate_factions = []
    army_id = 'Auxilary_khorne'
    army_factions = ['KHORNE']

    def __init__(self, parent=None):
        super(DetachAuxilary_khorne, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, Havocsv2, Venomcrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers, GreaterPossessed, DarkDisciples])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone, MasterOfExecutions])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_questor_traitoris(KnightSuperHeavy):
    army_name = 'Chaos Knights (Super-Heavy detachment)'
    faction_base = 'CHAOS KNIGHTS'
    alternate_factions = []
    army_id = 'super_heavy_questor_traitoris'
    army_factions = ['QUESTOR TRAITORIS', 'CHAOS', 'CHAOS KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_questor_traitoris, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([WarDog, KnightDespoiler, KnightTyrant, KnightDesecrator, KnightRampager] +
                               [RenegadeKnight, RenegadeDominus, RenegadeArmigers])
        return None


class DetachSuperHeavyAux_questor_traitoris(DetachSuperHeavyAux):
    army_name = 'Chaos Knights (Super-Heavy auxilary detachment)'
    faction_base = 'CHAOS KNIGHTS'
    alternate_factions = []
    army_id = 'super_heavy_questor_traitoris'
    army_factions = ['QUESTOR TRAITORIS', 'CHAOS', 'CHAOS KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_questor_traitoris, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([WarDog, KnightDespoiler, KnightTyrant, KnightDesecrator, KnightRampager] +
                               [RenegadeKnight, RenegadeDominus, RenegadeArmigers])
        return None


class DetachVanguard_scholastica_psykana(DetachVanguard):
    army_name = 'Scholastica Psykana (Vanguard detachment)'
    faction_base = 'SCHOLASTICA PSYKANA'
    alternate_factions = []
    army_id = 'vanguard_scholastica_psykana'
    army_factions = ['SCHOLASTICA PSYKANA', 'ASTRA TELEPATHICA', 'IMPERIUM', 'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachVanguard_scholastica_psykana, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Primaris])
        self.elite.add_classes([Wyrdvanes, Astropath])
        return None


class DetachCommand_scholastica_psykana(DetachCommand):
    army_name = 'Scholastica Psykana (Supreme command detachment)'
    faction_base = 'SCHOLASTICA PSYKANA'
    alternate_factions = []
    army_id = 'command_scholastica_psykana'
    army_factions = ['SCHOLASTICA PSYKANA', 'ASTRA TELEPATHICA', 'IMPERIUM', 'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachCommand_scholastica_psykana, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Primaris])
        self.elite.add_classes([Wyrdvanes, Astropath])
        return None


class DetachAuxilary_scholastica_psykana(DetachAuxilary):
    army_name = 'Scholastica Psykana (Auxilary Support Detachment)'
    faction_base = 'SCHOLASTICA PSYKANA'
    alternate_factions = []
    army_id = 'Auxilary_scholastica_psykana'
    army_factions = ['SCHOLASTICA PSYKANA']

    def __init__(self, parent=None):
        super(DetachAuxilary_scholastica_psykana, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Primaris])
        self.elite.add_classes([Wyrdvanes, Astropath])
        return None


class DetachPatrol__forge_world_(DetachPatrol):
    army_name = '<Forge World> (Patrol detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'patrol__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPatrol__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachBatallion__forge_world_(DetachBatallion):
    army_name = '<Forge World> (Batallion detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'batallion__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachBatallion__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachBrigade__forge_world_(DetachBrigade):
    army_name = '<Forge World> (Brigade detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'brigade__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachBrigade__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachVanguard__forge_world_(DetachVanguard):
    army_name = '<Forge World> (Vanguard detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'vanguard__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'ASTRA MILITARUM', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachVanguard__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachSpearhead__forge_world_(DetachSpearhead):
    army_name = '<Forge World> (Spearhead detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'spearhead__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSpearhead__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachOutrider__forge_world_(DetachOutrider):
    army_name = '<Forge World> (Outrider detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'outrider__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachOutrider__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachCommand__forge_world_(DetachCommand):
    army_name = '<Forge World> (Supreme command detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'command__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'ASTRA MILITARUM', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachCommand__forge_world_, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        return None


class DetachAuxilary__forge_world_(DetachAuxilary):
    army_name = '<Forge World> (Auxilary Support Detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'Auxilary__forge_world_'
    army_factions = ['<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachAuxilary__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPatrol_necrons(DetachPatrol):
    army_name = 'Necrons (Patrol detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'patrol_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachPatrol_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachBatallion_necrons(DetachBatallion):
    army_name = 'Necrons (Batallion detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'batallion_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachBatallion_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachBrigade_necrons(DetachBrigade):
    army_name = 'Necrons (Brigade detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'brigade_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachBrigade_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachVanguard_necrons(DetachVanguard):
    army_name = 'Necrons (Vanguard detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'vanguard_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachVanguard_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombSentinel])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachSpearhead_necrons(DetachSpearhead):
    army_name = 'Necrons (Spearhead detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'spearhead_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachSpearhead_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachOutrider_necrons(DetachOutrider):
    army_name = 'Necrons (Outrider detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'outrider_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachOutrider_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachCommand_necrons(DetachCommand):
    army_name = 'Necrons (Supreme command detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'command_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', 'MAYNARKH', '<DYNASTY>', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachCommand_necrons, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.transports.add_classes([GhostArk])
        return None


class DetachSuperHeavy_necrons(DetachSuperHeavy):
    army_name = 'Necrons (Super-Heavy detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'super_heavy_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', 'MAYNARKH', '<DYNASTY>', 'NECRONS', "C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavy_necrons, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        return None


class DetachSuperHeavyAux_necrons(DetachSuperHeavyAux):
    army_name = 'Necrons (Super-Heavy auxilary detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'super_heavy_aux_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', 'MAYNARKH', '<DYNASTY>', 'NECRONS', "C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_necrons, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        return None


class DetachAirWing_necrons(DetachAirWing):
    army_name = 'Necrons (Air Wing detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'air_wing_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', 'MAYNARKH', '<DYNASTY>', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachAirWing_necrons, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        return None


class DetachAuxilary_necrons(DetachAuxilary):
    army_name = 'Necrons (Auxilary Support Detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'Auxilary_necrons'
    army_factions = ['NECRONS']

    def __init__(self, parent=None):
        super(DetachAuxilary_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker, TombStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachVanguard_fallen(DetachVanguard):
    army_name = 'Fallen (Vanguard detachment)'
    faction_base = 'FALLEN'
    alternate_factions = []
    army_id = 'vanguard_fallen'
    army_factions = ['IMPERIUM', 'FALLEN', 'CHAOS']

    def __init__(self, parent=None):
        super(DetachVanguard_fallen, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Cypher])
        self.elite.add_classes([Fallen])
        return None


class DetachCommand_fallen(DetachCommand):
    army_name = 'Fallen (Supreme command detachment)'
    faction_base = 'FALLEN'
    alternate_factions = []
    army_id = 'command_fallen'
    army_factions = ['IMPERIUM', 'FALLEN', 'CHAOS']

    def __init__(self, parent=None):
        super(DetachCommand_fallen, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Cypher])
        self.elite.add_classes([Fallen])
        return None


class DetachAuxilary_fallen(DetachAuxilary):
    army_name = 'Fallen (Auxilary Support Detachment)'
    faction_base = 'FALLEN'
    alternate_factions = []
    army_id = 'Auxilary_fallen'
    army_factions = ['FALLEN']

    def __init__(self, parent=None):
        super(DetachAuxilary_fallen, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Cypher])
        self.elite.add_classes([Fallen])
        return None


class DetachAuxilary_kroot(DetachAuxilary):
    army_name = 'Kroot (Auxilary Support Detachment)'
    faction_base = 'KROOT'
    alternate_factions = []
    army_id = 'Auxilary_kroot'
    army_factions = ['KROOT']

    def __init__(self, parent=None):
        super(DetachAuxilary_kroot, self).__init__(*[], **{'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.elite.add_classes([Shaper, KrootoxSquad])
        self.troops.add_classes([KrootSquad])
        self.fast.add_classes([KroothoundSquad])
        return None


class DetachPatrol_adeptus_ministorum(DetachPatrol):
    army_name = 'Adeptus Ministorum (Patrol detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'patrol_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachPatrol_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachBatallion_adeptus_ministorum(DetachBatallion):
    army_name = 'Adeptus Ministorum (Batallion detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'batallion_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachBatallion_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachBrigade_adeptus_ministorum(DetachBrigade):
    army_name = 'Adeptus Ministorum (Brigade detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'brigade_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', '<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachBrigade_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachVanguard_adeptus_ministorum(DetachVanguard):
    army_name = 'Adeptus Ministorum (Vanguard detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'vanguard_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', 'ASTRA MILITARUM', '<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachVanguard_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachSpearhead_adeptus_ministorum(DetachSpearhead):
    army_name = 'Adeptus Ministorum (Spearhead detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'spearhead_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachSpearhead_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachOutrider_adeptus_ministorum(DetachOutrider):
    army_name = 'Adeptus Ministorum (Outrider detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'outrider_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachOutrider_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachCommand_adeptus_ministorum(DetachCommand):
    army_name = 'Adeptus Ministorum (Supreme command detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'command_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ASTRA MILITARUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachCommand_adeptus_ministorum, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachAuxilary_adeptus_ministorum(DetachAuxilary):
    army_name = 'Adeptus Ministorum (Auxilary Support Detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'Auxilary_adeptus_ministorum'
    army_factions = ['ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachAuxilary_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachAirWing_aeronautica_imperialis(DetachAirWing):
    army_name = 'Aeronautica Imperialis (Air Wing detachment)'
    faction_base = 'AERONAUTICA IMPERIALIS'
    alternate_factions = []
    army_id = 'air_wing_aeronautica_imperialis'
    army_factions = ['AERONAUTICA IMPERIALIS', 'IMPERIUM', 'ASTRA MILITARUM']

    def __init__(self, parent=None):
        super(DetachAirWing_aeronautica_imperialis, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Valkyries])
        return None


class DetachAuxilary_aeronautica_imperialis(DetachAuxilary):
    army_name = 'Aeronautica Imperialis (Auxilary Support Detachment)'
    faction_base = 'AERONAUTICA IMPERIALIS'
    alternate_factions = []
    army_id = 'Auxilary_aeronautica_imperialis'
    army_factions = ['AERONAUTICA IMPERIALIS']

    def __init__(self, parent=None):
        super(DetachAuxilary_aeronautica_imperialis, self).__init__(*[], **{'elite': True, 'fliers': True, 'parent': parent, })
        self.elite.add_classes([FleetOfficer])
        self.fliers.add_classes([Valkyries])
        return None


class DetachFort_chaos(DetachFort):
    army_name = 'Chaos (Fortification Network)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'fort_chaos'
    army_factions = ['DAEMON', 'CHAOS', 'NURGLE']

    def __init__(self, parent=None):
        super(DetachFort_chaos, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([ChaosBastion, FeculentGnarlmaws, NoctilithCrown])
        return None


class DetachSuperHeavy_c_tan_shards(DetachSuperHeavy):
    army_name = "C'tan Shards (Super-Heavy detachment)"
    faction_base = "C'TAN SHARDS"
    alternate_factions = []
    army_id = 'super_heavy_c_tan_shards'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', '<DYNASTY>', 'NECRONS', "C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavy_c_tan_shards, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([TesseractVault])
        return None


class DetachSuperHeavyAux_c_tan_shards(DetachSuperHeavyAux):
    army_name = "C'tan Shards (Super-Heavy auxilary detachment)"
    faction_base = "C'TAN SHARDS"
    alternate_factions = []
    army_id = 'super_heavy_aux_c_tan_shards'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', '<DYNASTY>', 'NECRONS', "C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_c_tan_shards, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([TesseractVault])
        return None


class DetachAuxilary_c_tan_shards(DetachAuxilary):
    army_name = "C'tan Shards (Auxilary Support Detachment)"
    faction_base = "C'TAN SHARDS"
    alternate_factions = []
    army_id = 'Auxilary_c_tan_shards'
    army_factions = ["C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachAuxilary_c_tan_shards, self).__init__(*[], **{'heavy': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([TranscendentCtan])
        self.elite.add_classes([Deciever, Nightbringer])
        return None


class DetachVanguard_inquisition(DetachVanguard):
    army_name = 'Inquisition (Vanguard detachment)'
    faction_base = 'INQUISITION'
    alternate_factions = []
    army_id = 'vanguard_inquisition'
    army_factions = ['<ORDO>', 'INQUISITION', 'IMPERIUM', 'ORDO HERETICUS', 'ORDO XENOS', 'ORDO MALLEUS']

    def __init__(self, parent=None):
        super(DetachVanguard_inquisition, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus])
        self.elite.add_classes([Acolytes, Jokaero, Daemonhost])
        return None


class DetachCommand_inquisition(DetachCommand):
    army_name = 'Inquisition (Supreme command detachment)'
    faction_base = 'INQUISITION'
    alternate_factions = []
    army_id = 'command_inquisition'
    army_factions = ['<ORDO>', 'INQUISITION', 'IMPERIUM', 'ORDO HERETICUS', 'ORDO XENOS', 'ORDO MALLEUS']

    def __init__(self, parent=None):
        super(DetachCommand_inquisition, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus])
        self.elite.add_classes([Acolytes, Jokaero, Daemonhost])
        return None


class DetachAuxilary_inquisition(DetachAuxilary):
    army_name = 'Inquisition (Auxilary Support Detachment)'
    faction_base = 'INQUISITION'
    alternate_factions = []
    army_id = 'Auxilary_inquisition'
    army_factions = ['INQUISITION']

    def __init__(self, parent=None):
        super(DetachAuxilary_inquisition, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus])
        self.elite.add_classes([Acolytes, Jokaero, Daemonhost])
        return None
class DetachFort_tyranids(DetachFort):
    army_name = 'Tyranids (Fortification Network)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'fort_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachFort_tyranids, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([Sporocyst])
        return None


class TauEmpireRoster(Roster):
    faction_base = "T'AU EMPIRE"
    alternate_factions = []
    army_factions = ['<SEPT>', "T'AU EMPIRE"]

    hq_sec = CommanderHQ

    def __init__(self, parent=None):
        super(TauEmpireRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import tau
        for sec in self.sections:
            sec.add_classes(tau.unit_types)


class DetachPatrol_t_au_empire(TauEmpireRoster, DetachPatrol):
    army_name = "T'au Empire (Patrol detachment)"
    army_id = 'patrol_t_au_empire'


class DetachBatallion_t_au_empire(TauEmpireRoster, DetachBatallion):
    army_name = "T'au Empire (Batallion detachment)"
    army_id = 'batallion_t_au_empire'


class DetachBrigade_t_au_empire(TauEmpireRoster, DetachBrigade):
    army_name = "T'au Empire (Brigade detachment)"
    army_id = 'brigade_t_au_empire'


class DetachVanguard_t_au_empire(TauEmpireRoster, DetachVanguard):
    army_name = "T'au Empire (Vanguard detachment)"
    army_id = 'vanguard_t_au_empire'


class DetachSpearhead_t_au_empire(TauEmpireRoster, DetachSpearhead):
    army_name = "T'au Empire (Spearhead detachment)"
    army_id = 'spearhead_t_au_empire'


class DetachOutrider_t_au_empire(TauEmpireRoster, DetachOutrider):
    army_name = "T'au Empire (Outrider detachment)"
    army_id = 'outrider_t_au_empire'


class DetachCommand_t_au_empire(TauEmpireRoster, DetachCommand):
    army_name = "T'au Empire (Supreme command detachment)"
    army_id = 'command_t_au_empire'


class DetachSuperHeavy_t_au_empire(TauEmpireRoster, DetachSuperHeavy):
    army_name = "T'au Empire (Super-Heavy detachment)"
    army_id = 'super_heavy_t_au_empire'


class DetachSuperHeavyAux_t_au_empire(TauEmpireRoster, DetachSuperHeavyAux):
    army_name = "T'au Empire (Super-Heavy auxilary detachment)"
    army_id = 'super_heavy_aux_t_au_empire'



class DetachAirWing_t_au_empire(TauEmpireRoster, DetachAirWing):
    army_name = "T'au Empire (Air Wing detachment)"
    army_id = 'air_wing_t_au_empire'



class DetachFort_t_au_empire(DetachFort):
    army_name = "T'au Empire (Fortification Network)"
    faction_base = "T'AU EMPIRE"
    alternate_factions = []
    army_id = 'fort_t_au_empire'
    army_factions = ["T'AU EMPIRE", '<SEPT>']

    def __init__(self, parent=None):
        super(DetachFort_t_au_empire, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([Shieldline, Droneport, Gunrig, RemoteSensorTower, DroneSentryTurrets])
        return None


class DetachAuxilary_t_au_empire(DetachAuxilary):
    army_name = "T'au Empire (Auxilary Support Detachment)"
    faction_base = "T'AU EMPIRE"
    alternate_factions = []
    army_id = 'Auxilary_t_au_empire'
    army_factions = ["T'AU EMPIRE", "<SEPT>"]

    def __init__(self, parent=None):
        super(DetachAuxilary_t_au_empire, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam, Rvarna, HeavyGunDrones, HBHammerhead, FSHammerhead])
        self.fliers.add_classes([RazorShark, SunShark, RemoraSquadron, TigerShark, TigerSharkAX10, Barracuda])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman, Dx4Drones, HazardTeam])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike, ShasOrMyr, ShasOrAlai, XV81Commander, XV84Commander])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones, Yvahra, Tetras, PiranhaTX42Team])
        self.transports.add_classes([Devilfish])
        return None


class DetachPatrol_adepta_sororitas(DetachPatrol):
    army_name = 'Adepta Sororitas (Patrol detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'patrol_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', '<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachPatrol_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachBatallion_adepta_sororitas(DetachBatallion):
    army_name = 'Adepta Sororitas (Batallion detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'batallion_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', '<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachBatallion_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachBrigade_adepta_sororitas(DetachBrigade):
    army_name = 'Adepta Sororitas (Brigade detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'brigade_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachBrigade_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachVanguard_adepta_sororitas(DetachVanguard):
    army_name = 'Adepta Sororitas (Vanguard detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'vanguard_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachVanguard_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachSpearhead_adepta_sororitas(DetachSpearhead):
    army_name = 'Adepta Sororitas (Spearhead detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'spearhead_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', '<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachSpearhead_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachOutrider_adepta_sororitas(DetachOutrider):
    army_name = 'Adepta Sororitas (Outrider detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'outrider_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachOutrider_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachCommand_adepta_sororitas(DetachCommand):
    army_name = 'Adepta Sororitas (Supreme command detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'command_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachCommand_adepta_sororitas, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachAuxilary_adepta_sororitas(DetachAuxilary):
    army_name = 'Adepta Sororitas (Auxilary Support Detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'Auxilary_adepta_sororitas'
    army_factions = ['ADEPTA SORORITAS']

    def __init__(self, parent=None):
        super(DetachAuxilary_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachPatrol__dynasty_(DetachPatrol):
    army_name = '<Dynasty> (Patrol detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'patrol__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachPatrol__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachBatallion__dynasty_(DetachBatallion):
    army_name = '<Dynasty> (Batallion detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'batallion__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'NECRONS', 'MAYNARKH']

    def __init__(self, parent=None):
        super(DetachBatallion__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArc])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachBrigade__dynasty_(DetachBrigade):
    army_name = '<Dynasty> (Brigade detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'brigade__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachBrigade__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachVanguard__dynasty_(DetachVanguard):
    army_name = '<Dynasty> (Vanguard detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'vanguard__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachVanguard__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachSpearhead__dynasty_(DetachSpearhead):
    army_name = '<Dynasty> (Spearhead detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'spearhead__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachSpearhead__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachOutrider__dynasty_(DetachOutrider):
    army_name = '<Dynasty> (Outrider detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'outrider__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachOutrider__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachCommand__dynasty_(DetachCommand):
    army_name = '<Dynasty> (Supreme command detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'command__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', 'MAYNARKH', '<DYNASTY>', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachCommand__dynasty_, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.transports.add_classes([GhostArk])
        return None


class DetachSuperHeavy__dynasty_(DetachSuperHeavy):
    army_name = '<Dynasty> (Super-Heavy detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'super_heavy__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', 'MAYNARKH', '<DYNASTY>', 'NECRONS', "C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavy__dynasty_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk])
        return None


class DetachSuperHeavyAux__dynasty_(DetachSuperHeavyAux):
    army_name = '<Dynasty> (Super-Heavy auxilary detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'super_heavy_aux__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', 'MAYNARKH', '<DYNASTY>', 'NECRONS', "C'TAN SHARDS"]

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__dynasty_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([TesseractVault, Obelisk, GaussPylon])
        return None


class DetachAirWing__dynasty_(DetachAirWing):
    army_name = '<Dynasty> (Air Wing detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'air_wing__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'SAUTEKH', 'MAYNARKH', '<DYNASTY>', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachAirWing__dynasty_, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        return None


class DetachAuxilary__dynasty_(DetachAuxilary):
    army_name = '<Dynasty> (Auxilary Support Detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = 'Auxilary__dynasty_'
    army_factions = ['<DYNASTY>']

    def __init__(self, parent=None):
        super(DetachAuxilary__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, SentryPylon, TesseractArk])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe, NightShroud])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard, TombStalker])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh, Kutlakh, Toholk])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths, Acanthrites, TombSentinel])
        self.transports.add_classes([GhostArk])
        return None


class DetachFort_aeldari(DetachFort):
    army_name = 'Aeldari (Fortification Network)'
    faction_base = 'AELDARI'
    army_id = 'fort_aeldari'
    army_factions = ["AELDARI"]

    def __init__(self, parent=None):
        super(DetachFort_aeldari, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([WebwayGate])
        return None


class DetachPatrol__hive_fleet_(DetachPatrol):
    army_name = '<Hive Fleet> (Patrol detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'patrol__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachPatrol__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachBatallion__hive_fleet_(DetachBatallion):
    army_name = '<Hive Fleet> (Batallion detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'batallion__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachBatallion__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachBrigade__hive_fleet_(DetachBrigade):
    army_name = '<Hive Fleet> (Brigade detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'brigade__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachBrigade__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachVanguard__hive_fleet_(DetachVanguard):
    army_name = '<Hive Fleet> (Vanguard detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'vanguard__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachVanguard__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachSpearhead__hive_fleet_(DetachSpearhead):
    army_name = '<Hive Fleet> (Spearhead detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'spearhead__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachSpearhead__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachOutrider__hive_fleet_(DetachOutrider):
    army_name = '<Hive Fleet> (Outrider detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'outrider__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachOutrider__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachCommand__hive_fleet_(DetachCommand):
    army_name = '<Hive Fleet> (Supreme command detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'command__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachCommand__hive_fleet_, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([Tyrannocyte])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        return None


class DetachAirWing__hive_fleet_(DetachAirWing):
    army_name = '<Hive Fleet> (Air Wing detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'air_wing__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachAirWing__hive_fleet_, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Harpy, HiveCrone])
        return None


class DetachFort__hive_fleet_(DetachFort):
    army_name = '<Hive Fleet> (Fortification Network)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'fort__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachFort__hive_fleet_, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([Sporocyst])
        return None


class DetachAuxilary__hive_fleet_(DetachAuxilary):
    army_name = '<Hive Fleet> (Auxilary Support Detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'Auxilary__hive_fleet_'
    army_factions = ['<HIVE FLEET>']

    def __init__(self, parent=None):
        super(DetachAuxilary__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class ChapterRoster(Roster):
    faction_base = '<CHAPTER>'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS', 'IRON HANDS', 'BLOOD RAVENS']
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'RAVEN GUARD', 'BLACK TEMPLARS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'IMPERIAL FISTS', 'ULTRAMARINES', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(ChapterRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import space_marines
        for sec in self.sections:
            sec.add_classes(space_marines.unit_types)


class DetachPatrol__chapter_(ChapterRoster, DetachPatrol):
    army_name = '<Chapter> (Patrol detachment)'
    army_id = 'patrol__chapter_'


class DetachBatallion__chapter_(ChapterRoster, DetachBatallion):
    army_name = '<Chapter> (Batallion detachment)'
    army_id = 'batallion__chapter_'


class DetachBrigade__chapter_(ChapterRoster, DetachBrigade):
    army_name = '<Chapter> (Brigade detachment)'
    army_id = 'brigade__chapter_'


class DetachVanguard__chapter_(ChapterRoster, DetachVanguard):
    army_name = '<Chapter> (Vanguard detachment)'
    army_id = 'vanguard__chapter_'


class DetachSpearhead__chapter_(ChapterRoster, DetachSpearhead):
    army_name = '<Chapter> (Spearhead detachment)'
    army_id = 'spearhead__chapter_'


class DetachOutrider__chapter_(ChapterRoster, DetachOutrider):
    army_name = '<Chapter> (Outrider detachment)'
    army_id = 'outrider__chapter_'


class DetachCommand__chapter_(ChapterRoster, DetachCommand):
    army_name = '<Chapter> (Supreme command detachment)'
    army_id = 'command__chapter_'


class DetachSuperHeavy__chapter_(DetachSuperHeavy):
    army_name = '<Chapter> (Super-Heavy detachment)'
    faction_base = '<CHAPTER>'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS', 'IRON HANDS', 'BLOOD RAVENS']
    army_id = 'super_heavy__chapter_'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__chapter_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux__chapter_(DetachSuperHeavyAux):
    army_name = '<Chapter> (Super-Heavy auxilary detachment)'
    faction_base = '<CHAPTER>'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS', 'IRON HANDS', 'BLOOD RAVENS']
    army_id = 'super_heavy_aux__chapter_'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__chapter_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing__chapter_(DetachAirWing):
    army_name = '<Chapter> (Air Wing detachment)'
    faction_base = '<CHAPTER>'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS', 'IRON HANDS', 'BLOOD RAVENS']
    army_id = 'air_wing__chapter_'
    army_factions = ['IMPERIUM', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'RAVEN GUARD', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'IMPERIAL FISTS', 'ULTRAMARINES', 'IRON HANDS']

    def __init__(self, parent=None):
        super(DetachAirWing__chapter_, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        return None


class DetachAuxilary__chapter_(DetachAuxilary):
    army_name = '<Chapter> (Auxilary Support Detachment)'
    faction_base = '<CHAPTER>'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS', 'IRON HANDS', 'BLOOD RAVENS']
    army_id = 'Auxilary__chapter_'
    army_factions = ['<CHAPTER>']

    def __init__(self, parent=None):
        super(DetachAuxilary__chapter_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, Eliminators, RepulsorExecutioner])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, InfiltratorSquad, IncursorSquad, IntercessorsV2])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, TermoAncient, InvictorWarsuit])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, PhobosCaptain, PhobosLibrarian, PhobosLieutenants, GabrielAngelos, PrimarisKhan, KhanOnBike, PrimarisTigurius, PrimarisShrike, Feirros])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, Suppressors])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, Impulsor])
        return None


class DetachPatrol_death_guard(DetachPatrol):
    army_name = 'Death Guard (Patrol detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'patrol_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPatrol_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachBatallion_death_guard(DetachBatallion):
    army_name = 'Death Guard (Batallion detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'batallion_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBatallion_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachBrigade_death_guard(DetachBrigade):
    army_name = 'Death Guard (Brigade detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'brigade_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachBrigade_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachVanguard_death_guard(DetachVanguard):
    army_name = 'Death Guard (Vanguard detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'vanguard_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachVanguard_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachSpearhead_death_guard(DetachSpearhead):
    army_name = 'Death Guard (Spearhead detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'spearhead_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachSpearhead_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachOutrider_death_guard(DetachOutrider):
    army_name = 'Death Guard (Outrider detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'outrider_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachOutrider_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachCommand_death_guard(DetachCommand):
    army_name = 'Death Guard (Supreme command detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'command_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachCommand_death_guard, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Mortarion])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavyAux_death_guard(DetachSuperHeavyAux):
    army_name = 'Death Guard (Super-Heavy auxilary detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'super_heavy_aux_death_guard'
    army_factions = ['HERETIC ASTARTES', 'NURGLE', 'DEATH GUARD', 'CHAOS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_death_guard, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Mortarion])
        return None


class DetachAuxilary_death_guard(DetachAuxilary):
    army_name = 'Death Guard (Auxilary Support Detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'Auxilary_death_guard'
    army_factions = ['DEATH GUARD']

    def __init__(self, parent=None):
        super(DetachAuxilary_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachAuxilary_vespid(DetachAuxilary):
    army_name = 'Vespid (Auxilary Support Detachment)'
    faction_base = 'VESPID'
    alternate_factions = []
    army_id = 'Auxilary_vespid'
    army_factions = ['VESPID']

    def __init__(self, parent=None):
        super(DetachAuxilary_vespid, self).__init__(*[], **{'fast': True, 'parent': parent, })
        self.fast.add_classes([Vespids])
        return None


class DetachPatrol_harlequins(DetachPatrol):
    army_name = 'Harlequins (Patrol detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'patrol_harlequins'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPatrol_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachBatallion_harlequins(DetachBatallion):
    army_name = 'Harlequins (Batallion detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'batallion_harlequins'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachBatallion_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachBrigade_harlequins(DetachBrigade):
    army_name = 'Harlequins (Brigade detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'brigade_harlequins'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachBrigade_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachVanguard_harlequins(DetachVanguard):
    army_name = 'Harlequins (Vanguard detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'vanguard_harlequins'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachVanguard_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachSpearhead_harlequins(DetachSpearhead):
    army_name = 'Harlequins (Spearhead detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'spearhead_harlequins'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachSpearhead_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachOutrider_harlequins(DetachOutrider):
    army_name = 'Harlequins (Outrider detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'outrider_harlequins'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachOutrider_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachCommand_harlequins(DetachCommand):
    army_name = 'Harlequins (Supreme command detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'command_harlequins'
    army_factions = ['<MASCUE>', 'YNNARI', 'HARLEQUINS', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand_harlequins, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachAuxilary_harlequins(DetachAuxilary):
    army_name = 'Harlequins (Auxilary Support Detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'Auxilary_harlequins'
    army_factions = ['HARLEQUINS']

    def __init__(self, parent=None):
        super(DetachAuxilary_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPatrol_ork(DetachPatrol):
    army_name = 'Ork (Patrol detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'patrol_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPatrol_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachBatallion_ork(DetachBatallion):
    army_name = 'Ork (Batallion detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'batallion_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachBatallion_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachBrigade_ork(DetachBrigade):
    army_name = 'Ork (Brigade detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'brigade_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachBrigade_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachVanguard_ork(DetachVanguard):
    army_name = 'Ork (Vanguard detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'vanguard_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachVanguard_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachSpearhead_ork(DetachSpearhead):
    army_name = 'Ork (Spearhead detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'spearhead_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachSpearhead_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachOutrider_ork(DetachOutrider):
    army_name = 'Ork (Outrider detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'outrider_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachOutrider_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachCommand_ork(DetachCommand):
    army_name = 'Ork (Supreme command detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'command_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachCommand_ork, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Stompa])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.transports.add_classes([Trukk])
        return None


class DetachSuperHeavy_ork(DetachSuperHeavy):
    army_name = 'Ork (Super-Heavy detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'super_heavy_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_ork, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Stompa])
        return None


class DetachSuperHeavyAux_ork(DetachSuperHeavyAux):
    army_name = 'Ork (Super-Heavy auxilary detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'super_heavy_aux_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_ork, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Stompa])
        return None


class DetachFort_ork(DetachFort):
    army_name = 'Ork (Fortification Network)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'fort_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachFort_ork, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([MekboyWorkshop])
        return None


class DetachAirWing_ork(DetachAirWing):
    army_name = 'Ork (Air Wing detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'air_wing_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachAirWing_ork, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        return None


class DetachAuxilary_ork(DetachAuxilary):
    army_name = 'Ork (Auxilary Support Detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'Auxilary_ork'
    army_factions = ['ORK']

    def __init__(self, parent=None):
        super(DetachAuxilary_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPatrol_militarum_tempestus(DetachPatrol):
    army_name = 'Militarum Tempestus (Patrol detachment)'
    faction_base = 'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = 'patrol_militarum_tempestus'
    army_factions = ['IMPERIUM', 'ASTRA MILITARUM', 'MILITARUM TEMPESTUS']

    hq_sec = GuardHQ
    elite_sec = GuardElites

    def __init__(self, parent=None):
        super(DetachPatrol_militarum_tempestus, self).__init__(*[], **{'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.hq.add_classes([TempestorPrime])
        self.elite.add_classes([TempCommandSquad])
        self.troops.add_classes([TempestusSquad])
        return None


class DetachBatallion_militarum_tempestus(DetachBatallion):
    army_name = 'Militarum Tempestus (Batallion detachment)'
    faction_base = 'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = 'batallion_militarum_tempestus'
    army_factions = ['IMPERIUM', 'ASTRA MILITARUM', 'MILITARUM TEMPESTUS']

    hq_sec = GuardHQ
    elite_sec = GuardElites

    def __init__(self, parent=None):
        super(DetachBatallion_militarum_tempestus, self).__init__(*[], **{'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.hq.add_classes([TempestorPrime])
        self.elite.add_classes([TempCommandSquad])
        self.troops.add_classes([TempestusSquad])
        return None


class DetachCommand_militarum_tempestus(DetachCommand):
    army_name = 'Militarum Tempestus (Supreme command detachment)'
    faction_base = 'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = 'command_militarum_tempestus'
    army_factions = ['IMPERIUM', 'ASTRA MILITARUM', 'MILITARUM TEMPESTUS']

    hq_sec = GuardHQ
    elite_sec = GuardElites

    def __init__(self, parent=None):
        super(DetachCommand_militarum_tempestus, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([TempestorPrime])
        self.elite.add_classes([TempCommandSquad])
        return None


class DetachAuxilary_militarum_tempestus(DetachAuxilary):
    army_name = 'Militarum Tempestus (Auxilary Support Detachment)'
    faction_base = 'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = 'Auxilary_militarum_tempestus'
    army_factions = ['MILITARUM TEMPESTUS']

    def __init__(self, parent=None):
        super(DetachAuxilary_militarum_tempestus, self).__init__(*[], **{'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.hq.add_classes([TempestorPrime])
        self.elite.add_classes([TempCommandSquad])
        self.troops.add_classes([TempestusSquad])
        return None


class GenestealerCultsRoster(Roster):
    faction_base = 'GENESTEALER CULTS'
    alternate_factions = []

    hq_sec = CultHQ
    elite_sec = CultElites
    army_factions = ['TYRANIDS', 'GENESTEALER CULTS', '<CULT>', 'CULT OF THE FOUR_ARMED EMPEROR', 'THE PAUPER PRINCES', 'THE HIVECULT', 'THE BLADED COG', 'THE RUSTED CLAW', 'THE TWISTED HELIX']

    def __init__(self, parent=None):
        super(GenestealerCultsRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        from . import genestealer_cult
        for sec in self.sections:
            sec.add_classes(genestealer_cult.unit_types)


class DetachPatrol_genestealer_cults(GenestealerCultsRoster, DetachPatrol):
    army_name = 'Genestealer Cults (Patrol detachment)'
    army_id = 'patrol_genestealer_cults'


class DetachBatallion_genestealer_cults(GenestealerCultsRoster, DetachBatallion):
    army_name = 'Genestealer Cults (Batallion detachment)'
    army_id = 'batallion_genestealer_cults'


class DetachBrigade_genestealer_cults(GenestealerCultsRoster, DetachBrigade):
    army_name = 'Genestealer Cults (Brigade detachment)'
    army_id = 'brigade_genestealer_cults'


class DetachVanguard_genestealer_cults(GenestealerCultsRoster, DetachVanguard):
    army_name = 'Genestealer Cults (Vanguard detachment)'
    army_id = 'vanguard_genestealer_cults'


class DetachSpearhead_genestealer_cults(GenestealerCultsRoster, DetachSpearhead):
    army_name = 'Genestealer Cults (Spearhead detachment)'
    army_id = 'spearhead_genestealer_cults'


class DetachOutrider_genestealer_cults(GenestealerCultsRoster, DetachOutrider):
    army_name = 'Genestealer Cults (Outrider detachment)'
    army_id = 'outrider_genestealer_cults'


class DetachCommand_genestealer_cults(GenestealerCultsRoster, DetachCommand):
    army_name = 'Genestealer Cults (Supreme command detachment)'
    army_id = 'command_genestealer_cults'


class DetachAuxilary_genestealer_cults(DetachAuxilary):
    army_name = 'Genestealer Cults (Auxilary Support Detachment)'
    faction_base = 'GENESTEALER CULTS'
    alternate_factions = []
    army_id = 'Auxilary_genestealer_cults'
    army_factions = ['TYRANIDS', 'GENESTEALER CULTS', '<CULT>', 'CULT OF THE FOUR_ARMED EMPEROR', 'THE PAUPER PRINCES', 'THE HIVECULT', 'THE BLADED COG', 'THE RUSTED CLAW', 'THE TWISTED HELIX']

    def __init__(self, parent=None):
        super(DetachAuxilary_genestealer_cults, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([CultLemanRuss, GoliathRockgrinder, BroodHeavyWeaponSquad])
        self.troops.add_classes([AcolyteHybrids, NeophyteHybrids, BroodInfantry])
        self.transports.add_classes([GoliathTruck, CultChimera])
        self.hq.add_classes([Patriarch, Magus, Primus, Iconward, Abominant, JackalAlphus])
        self.fast.add_classes([CultScoutSentinelSquad, CultArmouredSentinelSquad, RidgerunnerSquadron, AtalanJackals])
        self.elite.add_classes([HybridMetamorphs, Aberrants, PurestrainGenestealers, Clamavus, Sanctus, Kellermorph, Biophagus, Nexos, Locus])
        return None


class DetachFort_genestealer_cults(DetachFort):
    army_name = 'Genestealer Cults (Fortification Network)'
    faction_base = 'GENESTEALER CULTS'
    alternate_factions = []
    army_id = 'fort_genestealer_cults'
    army_factions = ['GENESTEALER CULTS']
    disable_selector = True

    def __init__(self, parent=None):
        super(DetachFort_genestealer_cults, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([TectonicFragdrill])
        self.fac_sel = None
        return None


class DetachPatrol__craftworld_(DetachPatrol):
    army_name = '<Craftworld> (Patrol detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'patrol__craftworld_'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachPatrol__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachBatallion__craftworld_(DetachBatallion):
    army_name = '<Craftworld> (Batallion detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'batallion__craftworld_'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachBatallion__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachBrigade__craftworld_(DetachBrigade):
    army_name = '<Craftworld> (Brigade detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'brigade__craftworld_'
    army_factions = ['SAIM-HANN', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachBrigade__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachVanguard__craftworld_(DetachVanguard):
    army_name = '<Craftworld> (Vanguard detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'vanguard__craftworld_'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachVanguard__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachSpearhead__craftworld_(DetachSpearhead):
    army_name = '<Craftworld> (Spearhead detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'spearhead__craftworld_'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachSpearhead__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachOutrider__craftworld_(DetachOutrider):
    army_name = '<Craftworld> (Outrider detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'outrider__craftworld_'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachOutrider__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachCommand__craftworld_(DetachCommand):
    army_name = '<Craftworld> (Supreme command detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'command__craftworld_'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachCommand__craftworld_, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachSuperHeavy__craftworld_(DetachSuperHeavy):
    army_name = '<Craftworld> (Super-Heavy detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'super_heavy__craftworld_'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__craftworld_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachSuperHeavyAux__craftworld_(DetachSuperHeavyAux):
    army_name = '<Craftworld> (Super-Heavy auxilary detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'super_heavy_aux__craftworld_'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__craftworld_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachAirWing__craftworld_(DetachAirWing):
    army_name = '<Craftworld> (Air Wing detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'air_wing__craftworld_'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAirWing__craftworld_, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        return None


class DetachAuxilary__craftworld_(DetachAuxilary):
    army_name = '<Craftworld> (Auxilary Support Detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'Auxilary__craftworld_'
    army_factions = ['<CRAFTWORLD>']

    def __init__(self, parent=None):
        super(DetachAuxilary__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPatrol__kabal_(DetachPatrol):
    army_name = '<Kabal> (Patrol detachment)'
    faction_base = '<KABAL>'
    alternate_factions = ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE']
    army_id = 'patrol__kabal_'
    army_factions = ['YNNARI', 'DRUKHARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE', '<KABAL>', 'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachPatrol__kabal_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachBatallion__kabal_(DetachBatallion):
    army_name = '<Kabal> (Batallion detachment)'
    faction_base = '<KABAL>'
    alternate_factions = ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE']
    army_id = 'batallion__kabal_'
    army_factions = ['YNNARI', 'DRUKHARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE', '<KABAL>', 'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachBatallion__kabal_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachVanguard__kabal_(DetachVanguard):
    army_name = '<Kabal> (Vanguard detachment)'
    faction_base = '<KABAL>'
    alternate_factions = ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE']
    army_id = 'vanguard__kabal_'
    army_factions = ['YNNARI', 'DRUKHARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE', '<KABAL>', 'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachVanguard__kabal_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachSpearhead__kabal_(DetachSpearhead):
    army_name = '<Kabal> (Spearhead detachment)'
    faction_base = '<KABAL>'
    alternate_factions = ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE']
    army_id = 'spearhead__kabal_'
    army_factions = ['YNNARI', 'DRUKHARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE', '<KABAL>', 'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachSpearhead__kabal_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachCommand__kabal_(DetachCommand):
    army_name = '<Kabal> (Supreme command detachment)'
    faction_base = '<KABAL>'
    alternate_factions = ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE']
    army_id = 'command__kabal_'
    army_factions = ['YNNARI', 'DRUKHARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE', '<KABAL>', 'AELDARI']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachCommand__kabal_, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachAirWing__kabal_(DetachAirWing):
    army_name = '<Kabal> (Air Wing detachment)'
    faction_base = '<KABAL>'
    alternate_factions = ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE']
    army_id = 'air_wing__kabal_'
    army_factions = ['AELDARI', 'DRUKHARI', '<WYCH CULT>', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE', '<KABAL>', 'WYCH CULT OF STRIFE']
    hq_sec = ArchonHQ
    elite_sec = DEElites

    def __init__(self, parent=None):
        super(DetachAirWing__kabal_, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Razorwing, Voidraven])
        return None


class DetachAuxilary__kabal_(DetachAuxilary):
    army_name = '<Kabal> (Auxilary Support Detachment)'
    faction_base = '<KABAL>'
    alternate_factions = ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE']
    army_id = 'Auxilary__kabal_'
    army_factions = ['<KABAL>']

    def __init__(self, parent=None):
        super(DetachAuxilary__kabal_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachPatrol__clan_(DetachPatrol):
    army_name = '<Clan> (Patrol detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'patrol__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPatrol__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachBatallion__clan_(DetachBatallion):
    army_name = '<Clan> (Batallion detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'batallion__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachBatallion__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachBrigade__clan_(DetachBrigade):
    army_name = '<Clan> (Brigade detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'brigade__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachBrigade__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachVanguard__clan_(DetachVanguard):
    army_name = '<Clan> (Vanguard detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'vanguard__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachVanguard__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachSpearhead__clan_(DetachSpearhead):
    army_name = '<Clan> (Spearhead detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'spearhead__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachSpearhead__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachOutrider__clan_(DetachOutrider):
    army_name = '<Clan> (Outrider detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'outrider__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachOutrider__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachCommand__clan_(DetachCommand):
    army_name = '<Clan> (Supreme command detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'command__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachCommand__clan_, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Stompa])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.transports.add_classes([Trukk])
        return None


class DetachSuperHeavy__clan_(DetachSuperHeavy):
    army_name = '<Clan> (Super-Heavy detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'super_heavy__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__clan_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Stompa])
        return None


class DetachSuperHeavyAux__clan_(DetachSuperHeavyAux):
    army_name = '<Clan> (Super-Heavy auxilary detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'super_heavy_aux__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__clan_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Stompa])
        return None


class DetachAirWing__clan_(DetachAirWing):
    army_name = '<Clan> (Air Wing detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'air_wing__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ', 'ORK', 'BLOOD AXES']

    def __init__(self, parent=None):
        super(DetachAirWing__clan_, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        return None


class DetachAuxilary__clan_(DetachAuxilary):
    army_name = '<Clan> (Auxilary Support Detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'Auxilary__clan_'
    army_factions = ['<CLAN>']

    def __init__(self, parent=None):
        super(DetachAuxilary__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachFort__clan_(DetachFort):
    army_name = '<Clan> (Fortification Network)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'fort__clan_'
    army_factions = ['<CLAN>']

    def __init__(self, parent=None):
        super(DetachFort__clan_, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([MekboyWorkshop])
        return None


class DetachVanguard__ordo_(DetachVanguard):
    army_name = '<Ordo> (Vanguard detachment)'
    faction_base = '<ORDO>'
    alternate_factions = ['ORDO MALLEUS', 'ORDO HERETICUS', 'ORDO XENOS']
    army_id = 'vanguard__ordo_'
    army_factions = ['<ORDO>', 'INQUISITION', 'IMPERIUM', 'ORDO HERETICUS', 'ORDO XENOS', 'ORDO MALLEUS']

    def __init__(self, parent=None):
        super(DetachVanguard__ordo_, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor])
        self.elite.add_classes([Acolytes, Jokaero])
        return None


class DetachCommand__ordo_(DetachCommand):
    army_name = '<Ordo> (Supreme command detachment)'
    faction_base = '<ORDO>'
    alternate_factions = ['ORDO MALLEUS', 'ORDO HERETICUS', 'ORDO XENOS']
    army_id = 'command__ordo_'
    army_factions = ['<ORDO>', 'INQUISITION', 'IMPERIUM', 'ORDO HERETICUS', 'ORDO XENOS', 'ORDO MALLEUS']

    def __init__(self, parent=None):
        super(DetachCommand__ordo_, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor])
        self.elite.add_classes([Acolytes, Jokaero])
        return None


class DetachAuxilary__ordo_(DetachAuxilary):
    army_name = '<Ordo> (Auxilary Support Detachment)'
    faction_base = '<ORDO>'
    alternate_factions = ['ORDO MALLEUS', 'ORDO HERETICUS', 'ORDO XENOS']
    army_id = 'Auxilary__ordo_'
    army_factions = ['<ORDO>']

    def __init__(self, parent=None):
        super(DetachAuxilary__ordo_, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor])
        self.elite.add_classes([Acolytes, Jokaero])
        return None


class DetachPatrol_adeptus_astartes(DetachPatrol):
    army_name = 'Adeptus Astartes (Patrol detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'patrol_adeptus_astartes'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'ULTRAMARINES', 'IMPERIAL FISTS', 'SPACE WOLVES', 'GREY KNIGHTS','IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachPatrol_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachBatallion_adeptus_astartes(DetachBatallion):
    army_name = 'Adeptus Astartes (Batallion detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'batallion_adeptus_astartes'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'ULTRAMARINES', 'IMPERIAL FISTS', 'SPACE WOLVES', 'GREY KNIGHTS', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachBatallion_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachBrigade_adeptus_astartes(DetachBrigade):
    army_name = 'Adeptus Astartes (Brigade detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'brigade_adeptus_astartes'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'GREY KNIGHTS', 'IMPERIAL FISTS', 'SPACE WOLVES', 'ULTRAMARINES', 'BLOOD RAVENS', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachBrigade_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, GabrielAngelos, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachVanguard_adeptus_astartes(DetachVanguard):
    army_name = 'Adeptus Astartes (Vanguard detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'vanguard_adeptus_astartes'
    army_factions = ['ULTRAMARINES', 'IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'DEATH COMPANY', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'RAVENWING', 'IMPERIAL FISTS', 'DEATHWING', 'SPACE WOLVES', 'GREY KNIGHTS', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachVanguard_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSpearhead_adeptus_astartes(DetachSpearhead):
    army_name = 'Adeptus Astartes (Spearhead detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'spearhead_adeptus_astartes'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'ULTRAMARINES', 'IMPERIAL FISTS', 'SPACE WOLVES', 'GREY KNIGHTS', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachSpearhead_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachOutrider_adeptus_astartes(DetachOutrider):
    army_name = 'Adeptus Astartes (Outrider detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'outrider_adeptus_astartes'
    army_factions = ['ULTRAMARINES', 'IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'RAVENWING', 'IMPERIAL FISTS', 'SPACE WOLVES', 'GREY KNIGHTS', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachOutrider_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachCommand_adeptus_astartes(DetachCommand):
    army_name = 'Adeptus Astartes (Supreme command detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'command_adeptus_astartes'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'GREY KNIGHTS', 'DARK ANGELS', 'BLACK TEMPLARS', 'DEATH COMPANY', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'RAVENWING', 'IMPERIAL FISTS', 'DEATHWING', 'SPACE WOLVES', 'ULTRAMARINES', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachCommand_adeptus_astartes, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, GabrielAngelos])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSuperHeavy_adeptus_astartes(DetachSuperHeavy):
    army_name = 'Adeptus Astartes (Super-Heavy detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'super_heavy_adeptus_astartes'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_adeptus_astartes, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachSuperHeavyAux_adeptus_astartes(DetachSuperHeavyAux):
    army_name = 'Adeptus Astartes (Super-Heavy auxilary detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'super_heavy_aux_adeptus_astartes'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_adeptus_astartes, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Guilliman, TerminusUltra, RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade, RelicMastodon])
        return None


class DetachAirWing_adeptus_astartes(DetachAirWing):
    army_name = 'Adeptus Astartes (Air Wing detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'air_wing_adeptus_astartes'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'GREY KNIGHTS', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'RAVENWING', 'IMPERIAL FISTS', 'SPACE WOLVES', 'ULTRAMARINES', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachAirWing_adeptus_astartes, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        return None


class DetachAuxilary_adeptus_astartes(DetachAuxilary):
    army_name = 'Adeptus Astartes (Auxilary Support Detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'Auxilary_adeptus_astartes'
    army_factions = ['ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachAuxilary_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, MortisDreadnought, SiegeDreadnought, ContemptorMortis, LeviathanDreadnought])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachPatrol_cult_mechanicus(DetachPatrol):
    army_name = 'Cult Mechanicus (Patrol detachment)'
    faction_base = 'CULT MECHANICUS'
    alternate_factions = []
    army_id = 'patrol_cult_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPatrol_cult_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachBatallion_cult_mechanicus(DetachBatallion):
    army_name = 'Cult Mechanicus (Batallion detachment)'
    faction_base = 'CULT MECHANICUS'
    alternate_factions = []
    army_id = 'batallion_cult_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachBatallion_cult_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachVanguard_cult_mechanicus(DetachVanguard):
    army_name = 'Cult Mechanicus (Vanguard detachment)'
    faction_base = 'CULT MECHANICUS'
    alternate_factions = []
    army_id = 'vanguard_cult_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'ASTRA MILITARUM', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachVanguard_cult_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachSpearhead_cult_mechanicus(DetachSpearhead):
    army_name = 'Cult Mechanicus (Spearhead detachment)'
    faction_base = 'CULT MECHANICUS'
    alternate_factions = []
    army_id = 'spearhead_cult_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSpearhead_cult_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachCommand_cult_mechanicus(DetachCommand):
    army_name = 'Cult Mechanicus (Supreme command detachment)'
    faction_base = 'CULT MECHANICUS'
    alternate_factions = []
    army_id = 'command_cult_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'ASTRA MILITARUM', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachCommand_cult_mechanicus, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        return None


class DetachAuxilary_cult_mechanicus(DetachAuxilary):
    army_name = 'Cult Mechanicus (Auxilary Support Detachment)'
    faction_base = 'CULT MECHANICUS'
    alternate_factions = []
    army_id = 'Auxilary_cult_mechanicus'
    army_factions = ['CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachAuxilary_cult_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachAuxilary_sisters_of_silence(DetachAuxilary):
    army_name = 'Sisters Of Silence (Auxilary Support Detachment)'
    faction_base = 'SISTERS OF SILENCE'
    alternate_factions = []
    army_id = 'Auxilary_sisters_of_silence'
    army_factions = ['SISTERS OF SILENCE']

    def __init__(self, parent=None):
        super(DetachAuxilary_sisters_of_silence, self).__init__(*[], **{'elite': True, 'transports': True, 'parent': parent, })
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers])
        self.transports.add_classes([NullRhino])
        return None


class DetachPatrol_ynnari(DetachPatrol):
    army_name = 'Ynnari (Patrol detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'patrol_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['DRUKHARI', 'SAIM-HANN', 'IYANDEN', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ALATOIC', '<MASCUE>', 'BIEL-TAN', 'WARHOST', '<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachPatrol_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachBatallion_ynnari(DetachBatallion):
    army_name = 'Ynnari (Batallion detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'batallion_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['DRUKHARI', 'SAIM-HANN', 'IYANDEN', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ALATOIC', '<MASCUE>', 'BIEL-TAN', 'WARHOST', '<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachBatallion_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachBrigade_ynnari(DetachBrigade):
    army_name = 'Ynnari (Brigade detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'brigade_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['SAIM-HANN', 'AELDARI', '<MASCUE>', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'HARLEQUINS', 'ALATOIC', 'BIEL-TAN', 'WARHOST', 'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachBrigade_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachVanguard_ynnari(DetachVanguard):
    army_name = 'Ynnari (Vanguard detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'vanguard_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['DRUKHARI', 'SAIM-HANN', 'SPIRIT HOST', 'IYANDEN', '<WYCH CULT>', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ALATOIC', '<MASCUE>', 'BIEL-TAN', 'WARHOST', 'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachVanguard_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachSpearhead_ynnari(DetachSpearhead):
    army_name = 'Ynnari (Spearhead detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'spearhead_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'IYANDEN', 'DRUKHARI', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ASPECT WARRIOR', 'BIEL-TAN', 'WARHOST', '<MASCUE>']

    def __init__(self, parent=None):
        super(DetachSpearhead_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachOutrider_ynnari(DetachOutrider):
    army_name = 'Ynnari (Outrider detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'outrider_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['DRUKHARI', 'SAIM-HANN', 'IYANDEN', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'HARLEQUINS', 'ALATOIC', '<MASCUE>', 'BIEL-TAN', 'WARHOST', '<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachOutrider_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachCommand_ynnari(DetachCommand):
    army_name = 'Ynnari (Supreme command detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'command_ynnari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', '<WYCH CULT>', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ASPECT WARRIOR', '<MASCUE>', 'BIEL-TAN', 'WARHOST', 'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachCommand_ynnari, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachSuperHeavy_ynnari(DetachSuperHeavy):
    army_name = 'Ynnari (Super-Heavy detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'super_heavy_ynnari'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_ynnari, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachSuperHeavyAux_ynnari(DetachSuperHeavyAux):
    army_name = 'Ynnari (Super-Heavy auxilary detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'super_heavy_aux_ynnari'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_ynnari, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachAirWing_ynnari(DetachAirWing):
    army_name = 'Ynnari (Air Wing detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'air_wing_ynnari'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'DRUKHARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ASPECT WARRIOR', 'BIEL-TAN', '<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachAirWing_ynnari, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachAuxilary_ynnari(DetachAuxilary):
    army_name = 'Ynnari (Auxilary Support Detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'Auxilary_ynnari'
    army_factions = ['YNNARI']

    def __init__(self, parent=None):
        super(DetachAuxilary_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class NurgleRoster(Roster):
    faction_base = 'NURGLE'
    alternate_factions = []
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'HERETIC ASTARTES', 'NURGLE', 'IRON WARRIORS', 'RED CORSAIRS', 'RENEGADES']

    hq_sec = ApostleHQ
    elite_sec = DiscipleElites
    
    def __init__(self, parent=None):
        super(NurgleRoster, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'lords': True, 'parent': parent, })
        from . import chaos_demons
        from . import heretic_astartes
        from . import death_guard
        for sec in self.sections:
            sec.add_classes(chaos_demons.unit_types + heretic_astartes.unit_types + death_guard.unit_types)


class DetachPatrol_nurgle(NurgleRoster, DetachPatrol):
    army_name = 'Nurgle (Patrol detachment)'
    army_id = 'patrol_nurgle'


class DetachBatallion_nurgle(NurgleRoster, DetachBatallion):
    army_name = 'Nurgle (Batallion detachment)'
    army_id = 'batallion_nurgle'


class DetachBrigade_nurgle(NurgleRoster, DetachBrigade):
    army_name = 'Nurgle (Brigade detachment)'
    army_id = 'brigade_nurgle'


class DetachVanguard_nurgle(NurgleRoster, DetachVanguard):
    army_name = 'Nurgle (Vanguard detachment)'
    army_id = 'vanguard_nurgle'


class DetachSpearhead_nurgle(NurgleRoster, DetachSpearhead):
    army_name = 'Nurgle (Spearhead detachment)'
    army_id = 'spearhead_nurgle'


class DetachOutrider_nurgle(NurgleRoster, DetachOutrider):
    army_name = 'Nurgle (Outrider detachment)'
    army_id = 'outrider_nurgle'


class DetachCommand_nurgle(NurgleRoster, DetachCommand):
    army_name = 'Nurgle (Supreme command detachment)'
    army_id = 'command_nurgle'


class DetachSuperHeavyAux_nurgle(DetachSuperHeavyAux):
    army_name = 'Nurgle (Super-Heavy auxilary detachment)'
    faction_base = 'NURGLE'
    alternate_factions = []
    army_id = 'super_heavy_aux_nurgle'
    army_factions = ['HERETIC ASTARTES', 'NURGLE', 'DEATH GUARD', 'CHAOS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_nurgle, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Mortarion])
        return None


class DetachAirWing_nurgle(DetachAirWing):
    army_name = 'Nurgle (Air Wing detachment)'
    faction_base = 'NURGLE'
    alternate_factions = []
    army_id = 'air_wing_nurgle'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'RED CORSAIRS', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachAirWing_nurgle, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachFort_nurgle(DetachFort):
    army_name = 'Nurgle (Fortification Network)'
    faction_base = 'NURGLE'
    alternate_factions = []
    army_id = 'fort_nurgle'
    army_factions = ['DAEMON', 'NURGLE', 'CHAOS']

    def __init__(self, parent=None):
        super(DetachFort_nurgle, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([FeculentGnarlmaws])
        return None


class DetachAuxilary_nurgle(DetachAuxilary):
    army_name = 'Nurgle (Auxilary Support Detachment)'
    faction_base = 'NURGLE'
    alternate_factions = []
    army_id = 'Auxilary_nurgle'
    army_factions = ['NURGLE']

    def __init__(self, parent=None):
        super(DetachAuxilary_nurgle, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler, Havocsv2, Venomcrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, DarkDisciples, GreaterPossessed])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, MasterOfPossession, MasterOfExecutions, LordDiscordant])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPatrol__haemunculus_coven_(DetachPatrol):
    army_name = '<Haemunculus Coven> (Patrol detachment)'
    faction_base = '<HAEMUNCULUS COVEN>'
    alternate_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']
    army_id = 'patrol__haemunculus_coven_'
    army_factions = ['<HAEMUNCULUS COVEN>', 'AELDARI', 'DRUKHARI', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachPatrol__haemunculus_coven_, self).__init__(*[], **{'heavy': True, 'elite': True, 'hq': True, 'transports': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachBatallion__haemunculus_coven_(DetachBatallion):
    army_name = '<Haemunculus Coven> (Batallion detachment)'
    faction_base = '<HAEMUNCULUS COVEN>'
    alternate_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']
    army_id = 'batallion__haemunculus_coven_'
    army_factions = ['<HAEMUNCULUS COVEN>', 'AELDARI', 'DRUKHARI', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachBatallion__haemunculus_coven_, self).__init__(*[], **{'heavy': True, 'elite': True, 'hq': True, 'transports': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachVanguard__haemunculus_coven_(DetachVanguard):
    army_name = '<Haemunculus Coven> (Vanguard detachment)'
    faction_base = '<HAEMUNCULUS COVEN>'
    alternate_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']
    army_id = 'vanguard__haemunculus_coven_'
    army_factions = ['<HAEMUNCULUS COVEN>', 'AELDARI', 'DRUKHARI', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachVanguard__haemunculus_coven_, self).__init__(*[], **{'heavy': True, 'elite': True, 'hq': True, 'transports': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachSpearhead__haemunculus_coven_(DetachSpearhead):
    army_name = '<Haemunculus Coven> (Spearhead detachment)'
    faction_base = '<HAEMUNCULUS COVEN>'
    alternate_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']
    army_id = 'spearhead__haemunculus_coven_'
    army_factions = ['<HAEMUNCULUS COVEN>', 'AELDARI', 'DRUKHARI', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachSpearhead__haemunculus_coven_, self).__init__(*[], **{'heavy': True, 'elite': True, 'hq': True, 'transports': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachCommand__haemunculus_coven_(DetachCommand):
    army_name = '<Haemunculus Coven> (Supreme command detachment)'
    faction_base = '<HAEMUNCULUS COVEN>'
    alternate_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']
    army_id = 'command__haemunculus_coven_'
    army_factions = ['<HAEMUNCULUS COVEN>', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'DRUKHARI', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand__haemunculus_coven_, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.elite.add_classes([Grotesques])
        return None


class DetachAuxilary__haemunculus_coven_(DetachAuxilary):
    army_name = '<Haemunculus Coven> (Auxilary Support Detachment)'
    faction_base = '<HAEMUNCULUS COVEN>'
    alternate_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']
    army_id = 'Auxilary__haemunculus_coven_'
    army_factions = ['<HAEMUNCULUS COVEN>', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']

    def __init__(self, parent=None):
        super(DetachAuxilary__haemunculus_coven_, self).__init__(*[], **{'heavy': True, 'elite': True, 'hq': True, 'transports': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachSuperHeavy__household_(KnightSuperHeavy):
    army_name = '<Household> (Super-Heavy detachment)'
    faction_base = '<HOUSEHOLD>'
    alternate_factions = ['TERRIN', 'GRIFFIN', 'HAWKSHROUD', 'CADMUS', 'MORTAN', 'RAVEN', 'TARANIS', 'KRAST', 'VULKER']
    army_id = 'super_heavy__household_'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy__household_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, ArmigerHelverins, KnightPreceptor, KnightCastellan, CanisRex, KnightValiant, KnightPorphyron, KnightAcheron, KnightAtropos, KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix])
        return None


class DetachSuperHeavyAux__household_(DetachSuperHeavyAux):
    army_name = '<Household> (Super-Heavy auxilary detachment)'
    faction_base = '<HOUSEHOLD>'
    alternate_factions = ['TERRIN', 'GRIFFIN', 'HAWKSHROUD', 'CADMUS', 'MORTAN', 'RAVEN', 'TARANIS', 'KRAST', 'VULKER']
    army_id = 'super_heavy_aux__household_'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux__household_, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, ArmigerHelverins, KnightPreceptor, KnightCastellan, CanisRex, KnightValiant, KnightPorphyron, KnightAcheron, KnightAtropos, KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix])
        return None


class DetachSuperHeavy_imperial_knights(KnightSuperHeavy):
    army_name = 'Imperial Knights (Super-Heavy detachment)'
    faction_base = 'IMPERIAL KNIGHTS'
    alternate_factions = []
    army_id = 'super_heavy_imperial_knights'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_imperial_knights, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, ArmigerHelverins, KnightPreceptor, KnightCastellan, CanisRex, KnightValiant, KnightPorphyron, KnightAcheron, KnightAtropos, KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix])
        return None


class DetachSuperHeavyAux_imperial_knights(DetachSuperHeavyAux):
    army_name = 'Imperial Knights (Super-Heavy auxilary detachment)'
    faction_base = 'IMPERIAL KNIGHTS'
    alternate_factions = []
    army_id = 'super_heavy_aux_imperial_knights'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_imperial_knights, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, ArmigerHelverins, KnightPreceptor, KnightCastellan, CanisRex, KnightValiant, KnightPorphyron, KnightAcheron, KnightAtropos, KnightCastigator, KnightLancer, KnightMagaera, KnightStyrix])
        return None


class DetachFort_imperial_knights(DetachFort):
    army_name = 'Imperial Knights (Fortification Network)'
    faction_base = 'IMPERIAL KNIGHTS'
    alternate_factions = []
    army_id = 'fort_imperial_knights'
    army_factions = ['IMPERIUM', 'IMPERIAL KNIGHTS']

    def __init__(self, parent=None):
        super(DetachFort_imperial_knights, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([SacristanForgeshrine])
        return None


class DetachVanguard_death_company(DetachVanguard):
    army_name = 'Death Company (Vanguard detachment)'
    faction_base = 'DEATH COMPANY'
    alternate_factions = []
    army_id = 'vanguard_death_company'
    army_factions = ['DEATH COMPANY', 'IMPERIUM', 'BLOOD ANGELS', 'ADEPTUS ASTARTES']

    hq_sec = DCCommand

    def __init__(self, parent=None):
        super(DetachVanguard_death_company, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([DCTycho, Lemartes])
        self.elite.add_classes([DeathCompany, DeathCompanyDreadnought])
        return None


class DetachCommand_death_company(DetachCommand):
    army_name = 'Death Company (Supreme command detachment)'
    faction_base = 'DEATH COMPANY'
    alternate_factions = []
    army_id = 'command_death_company'
    army_factions = ['DEATH COMPANY', 'IMPERIUM', 'BLOOD ANGELS', 'ADEPTUS ASTARTES']

    hq_sec = DCCommand

    def __init__(self, parent=None):
        super(DetachCommand_death_company, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([DCTycho, Lemartes])
        self.elite.add_classes([DeathCompany, DeathCompanyDreadnought])
        return None


class DetachAuxilary_death_company(DetachAuxilary):
    army_name = 'Death Company (Auxilary Support Detachment)'
    faction_base = 'DEATH COMPANY'
    alternate_factions = []
    army_id = 'Auxilary_death_company'
    army_factions = ['DEATH COMPANY']

    def __init__(self, parent=None):
        super(DetachAuxilary_death_company, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([DCTycho, Lemartes])
        self.elite.add_classes([DeathCompany, DeathCompanyDreadnought])
        return None


class DetachVanguard_spirit_host(DetachVanguard):
    army_name = 'Spirit Host (Vanguard detachment)'
    faction_base = 'SPIRIT HOST'
    alternate_factions = []
    army_id = 'vanguard_spirit_host'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachVanguard_spirit_host, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'fliers': True, 'parent': parent, })
        self.heavy.add_classes([Wraithlord])
        self.hq.add_classes([Spiritseer])
        self.elite.add_classes([Wraithblades, Wraithguard])
        self.fliers.add_classes([Hemlock])
        return None


class DetachSpearhead_spirit_host(DetachSpearhead):
    army_name = 'Spirit Host (Spearhead detachment)'
    faction_base = 'SPIRIT HOST'
    alternate_factions = []
    army_id = 'spearhead_spirit_host'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSpearhead_spirit_host, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'fliers': True, 'parent': parent, })
        self.heavy.add_classes([Wraithlord])
        self.hq.add_classes([Spiritseer])
        self.elite.add_classes([Wraithblades, Wraithguard])
        self.fliers.add_classes([Hemlock])
        return None


class DetachCommand_spirit_host(DetachCommand):
    army_name = 'Spirit Host (Supreme command detachment)'
    faction_base = 'SPIRIT HOST'
    alternate_factions = []
    army_id = 'command_spirit_host'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachCommand_spirit_host, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Spiritseer])
        self.elite.add_classes([Wraithblades, Wraithguard])
        return None


class DetachSuperHeavy_spirit_host(DetachSuperHeavy):
    army_name = 'Spirit Host (Super-Heavy detachment)'
    faction_base = 'SPIRIT HOST'
    alternate_factions = []
    army_id = 'super_heavy_spirit_host'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_spirit_host, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachSuperHeavyAux_spirit_host(DetachSuperHeavyAux):
    army_name = 'Spirit Host (Super-Heavy auxilary detachment)'
    faction_base = 'SPIRIT HOST'
    alternate_factions = []
    army_id = 'super_heavy_aux_spirit_host'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_spirit_host, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachAirWing_spirit_host(DetachAirWing):
    army_name = 'Spirit Host (Air Wing detachment)'
    faction_base = 'SPIRIT HOST'
    alternate_factions = []
    army_id = 'air_wing_spirit_host'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAirWing_spirit_host, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Hemlock])
        return None


class DetachAuxilary_spirit_host(DetachAuxilary):
    army_name = 'Spirit Host (Auxilary Support Detachment)'
    faction_base = 'SPIRIT HOST'
    alternate_factions = []
    army_id = 'Auxilary_spirit_host'
    army_factions = ['SPIRIT HOST']

    def __init__(self, parent=None):
        super(DetachAuxilary_spirit_host, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'fliers': True, 'parent': parent, })
        self.heavy.add_classes([Wraithlord])
        self.hq.add_classes([Spiritseer])
        self.elite.add_classes([Wraithblades, Wraithguard])
        self.fliers.add_classes([Hemlock])
        return None


class DetachPatrol__order_(DetachPatrol):
    army_name = '<Order> (Patrol detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'patrol__order_'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachPatrol__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachBatallion__order_(DetachBatallion):
    army_name = '<Order> (Batallion detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'batallion__order_'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachBatallion__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachBrigade__order_(DetachBrigade):
    army_name = '<Order> (Brigade detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'brigade__order_'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachBrigade__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachVanguard__order_(DetachVanguard):
    army_name = '<Order> (Vanguard detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'vanguard__order_'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachVanguard__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachSpearhead__order_(DetachSpearhead):
    army_name = '<Order> (Spearhead detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'spearhead__order_'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachSpearhead__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachOutrider__order_(DetachOutrider):
    army_name = '<Order> (Outrider detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'outrider__order_'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachOutrider__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachCommand__order_(DetachCommand):
    army_name = '<Order> (Supreme command detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'command__order_'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', '<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachCommand__order_, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachAuxilary__order_(DetachAuxilary):
    army_name = '<Order> (Auxilary Support Detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'Auxilary__order_'
    army_factions = ['<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']

    def __init__(self, parent=None):
        super(DetachAuxilary__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachAuxilary_officio_assassinorum(DetachAuxilary):
    army_name = 'Officio Assassinorum (Auxilary Support Detachment)'
    faction_base = 'OFFICIO ASSASSINORUM'
    alternate_factions = []
    army_id = 'Auxilary_officio_assassinorum'
    army_factions = ['OFFICIO ASSASSINORUM']

    def __init__(self, parent=None):
        super(DetachAuxilary_officio_assassinorum, self).__init__(*[], **{'elite': True, 'parent': parent, })
        self.elite.add_classes([VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin])
        return None


class DetachPatrol_adeptus_custodes(DetachPatrol):
    army_name = 'Adeptus Custodes (Patrol detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'patrol_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachPatrol_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachBatallion_adeptus_custodes(DetachBatallion):
    army_name = 'Adeptus Custodes (Batallion detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'batallion_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachBatallion_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachBrigade_adeptus_custodes(DetachBrigade):
    army_name = 'Adeptus Custodes (Brigade detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'brigade_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachBrigade_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachVanguard_adeptus_custodes(DetachVanguard):
    army_name = 'Adeptus Custodes (Vanguard detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'vanguard_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachVanguard_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachSpearhead_adeptus_custodes(DetachSpearhead):
    army_name = 'Adeptus Custodes (Spearhead detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'spearhead_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachSpearhead_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachOutrider_adeptus_custodes(DetachOutrider):
    army_name = 'Adeptus Custodes (Outrider detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'outrider_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachOutrider_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachCommand_adeptus_custodes(DetachCommand):
    army_name = 'Adeptus Custodes (Supreme command detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'command_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachCommand_adeptus_custodes, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        return None


class DetachAuxilary_adeptus_custodes(DetachAuxilary):
    army_name = 'Adeptus Custodes (Auxilary Support Detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'Auxilary_adeptus_custodes'
    army_factions = ['ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachAuxilary_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachPatrol_aspect_warrior(DetachPatrol):
    army_name = 'Aspect Warrior (Patrol detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'patrol_aspect_warrior'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachPatrol_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachBatallion_aspect_warrior(DetachBatallion):
    army_name = 'Aspect Warrior (Batallion detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'batallion_aspect_warrior'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachBatallion_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachBrigade_aspect_warrior(DetachBrigade):
    army_name = 'Aspect Warrior (Brigade detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'brigade_aspect_warrior'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachBrigade_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachVanguard_aspect_warrior(DetachVanguard):
    army_name = 'Aspect Warrior (Vanguard detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'vanguard_aspect_warrior'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachVanguard_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachSpearhead_aspect_warrior(DetachSpearhead):
    army_name = 'Aspect Warrior (Spearhead detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'spearhead_aspect_warrior'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSpearhead_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachOutrider_aspect_warrior(DetachOutrider):
    army_name = 'Aspect Warrior (Outrider detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'outrider_aspect_warrior'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachOutrider_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachCommand_aspect_warrior(DetachCommand):
    army_name = 'Aspect Warrior (Supreme command detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'command_aspect_warrior'
    army_factions = ['SAIM-HANN', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachCommand_aspect_warrior, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachAirWing_aspect_warrior(DetachAirWing):
    army_name = 'Aspect Warrior (Air Wing detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'air_wing_aspect_warrior'
    army_factions = ['SAIM-HANN', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachAirWing_aspect_warrior, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        return None


class DetachAuxilary_aspect_warrior(DetachAuxilary):
    army_name = 'Aspect Warrior (Auxilary Support Detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'Auxilary_aspect_warrior'
    army_factions = ['ASPECT WARRIOR']

    def __init__(self, parent=None):
        super(DetachAuxilary_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachPatrol_grey_knights(DetachPatrol):
    army_name = 'Grey Knights (Patrol detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'patrol_grey_knights'
    army_factions = ['IMPERIUM', 'GREY KNIGHTS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPatrol_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachBatallion_grey_knights(DetachBatallion):
    army_name = 'Grey Knights (Batallion detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'batallion_grey_knights'
    army_factions = ['IMPERIUM', 'GREY KNIGHTS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBatallion_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachBrigade_grey_knights(DetachBrigade):
    army_name = 'Grey Knights (Brigade detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'brigade_grey_knights'
    army_factions = ['IMPERIUM', 'GREY KNIGHTS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachBrigade_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachVanguard_grey_knights(DetachVanguard):
    army_name = 'Grey Knights (Vanguard detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'vanguard_grey_knights'
    army_factions = ['IMPERIUM', 'GREY KNIGHTS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachVanguard_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachSpearhead_grey_knights(DetachSpearhead):
    army_name = 'Grey Knights (Spearhead detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'spearhead_grey_knights'
    army_factions = ['IMPERIUM', 'GREY KNIGHTS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachSpearhead_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachOutrider_grey_knights(DetachOutrider):
    army_name = 'Grey Knights (Outrider detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'outrider_grey_knights'
    army_factions = ['IMPERIUM', 'GREY KNIGHTS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachOutrider_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachCommand_grey_knights(DetachCommand):
    army_name = 'Grey Knights (Supreme command detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'command_grey_knights'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachCommand_grey_knights, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([GKRhino, GKRazorback])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        return None


class DetachAirWing_grey_knights(DetachAirWing):
    army_name = 'Grey Knights (Air Wing detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'air_wing_grey_knights'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachAirWing_grey_knights, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        return None


class DetachAuxilary_grey_knights(DetachAuxilary):
    army_name = 'Grey Knights (Auxilary Support Detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'Auxilary_grey_knights'
    army_factions = ['GREY KNIGHTS']

    def __init__(self, parent=None):
        super(DetachAuxilary_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DaemonRoster(Roster):
    faction_base = 'DAEMON'
    alternate_factions = []
    army_factions = ['DAEMON', 'TZEENTCH', 'CHAOS', 'KHORNE', 'SLAANESH', 'NURGLE']

    def __init__(self, parent=None):
        super(DaemonRoster, self).__init__(*[], **{'hq': True, 'elite': True, 'fast': True, 'fliers': False, 'parent': parent, })
        from . import chaos_demons
        for sec in self.sections:
            sec.add_classes(chaos_demons.unit_types)


class DetachPatrol_daemon(DaemonRoster, DetachPatrol):
    army_name = 'Daemon (Patrol detachment)'
    army_id = 'patrol_daemon'


class DetachBatallion_daemon(DaemonRoster, DetachBatallion):
    army_name = 'Daemon (Batallion detachment)'
    army_id = 'batallion_daemon'


class DetachBrigade_daemon(DaemonRoster, DetachBrigade):
    army_name = 'Daemon (Brigade detachment)'
    army_id = 'brigade_daemon'


class DetachVanguard_daemon(DaemonRoster, DetachVanguard):
    army_name = 'Daemon (Vanguard detachment)'
    army_id = 'vanguard_daemon'


class DetachSpearhead_daemon(DaemonRoster, DetachSpearhead):
    army_name = 'Daemon (Spearhead detachment)'
    army_id = 'spearhead_daemon'


class DetachOutrider_daemon(DaemonRoster, DetachOutrider):
    army_name = 'Daemon (Outrider detachment)'
    army_id = 'outrider_daemon'


class DetachCommand_daemon(DaemonRoster, DetachCommand):
    army_name = 'Daemon (Supreme command detachment)'
    army_id = 'command_daemon'


class DetachFort_daemon(DetachFort):
    army_name = 'Daemon (Fortification Network)'
    faction_base = 'DAEMON'
    alternate_factions = []
    army_id = 'fort_daemon'
    army_factions = ['DAEMON', 'NURGLE', 'CHAOS']

    def __init__(self, parent=None):
        super(DetachFort_daemon, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([FeculentGnarlmaws])
        return None


class DetachAuxilary_daemon(DetachAuxilary):
    army_name = 'Daemon (Auxilary Support Detachment)'
    faction_base = 'DAEMON'
    alternate_factions = []
    army_id = 'Auxilary_daemon'
    army_factions = ['DAEMON']

    def __init__(self, parent=None):
        super(DetachAuxilary_daemon, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, ShalaxiHelbane, SyllEsske, ContortedEpitome, InfernalEnrapturess])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachPatrol_adeptus_mechanicus(DetachPatrol):
    army_name = 'Adeptus Mechanicus (Patrol detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'patrol_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPatrol_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachBatallion_adeptus_mechanicus(DetachBatallion):
    army_name = 'Adeptus Mechanicus (Batallion detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'batallion_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachBatallion_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachBrigade_adeptus_mechanicus(DetachBrigade):
    army_name = 'Adeptus Mechanicus (Brigade detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'brigade_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachBrigade_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachVanguard_adeptus_mechanicus(DetachVanguard):
    army_name = 'Adeptus Mechanicus (Vanguard detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'vanguard_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'ASTRA MILITARUM', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachVanguard_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachSpearhead_adeptus_mechanicus(DetachSpearhead):
    army_name = 'Adeptus Mechanicus (Spearhead detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'spearhead_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSpearhead_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachOutrider_adeptus_mechanicus(DetachOutrider):
    army_name = 'Adeptus Mechanicus (Outrider detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'outrider_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachOutrider_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachCommand_adeptus_mechanicus(DetachCommand):
    army_name = 'Adeptus Mechanicus (Supreme command detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'command_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'ASTRA MILITARUM', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachCommand_adeptus_mechanicus, self).__init__(*[], **{'hq': True, 'elite': True, 'parent': parent, })
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        return None


class DetachAuxilary_adeptus_mechanicus(DetachAuxilary):
    army_name = 'Adeptus Mechanicus (Auxilary Support Detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'Auxilary_adeptus_mechanicus'
    army_factions = ['ADEPTUS MECHANICUS']

    def __init__(self, parent=None):
        super(DetachAuxilary_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachAuxilary_jokaero(DetachAuxilary):
    army_name = 'Jokaero (Auxilary Support Detachment)'
    faction_base = 'JOKAERO'
    alternate_factions = []
    army_id = 'Auxilary_jokaero'
    army_factions = ['JOKAERO']

    def __init__(self, parent=None):
        super(DetachAuxilary_jokaero, self).__init__(*[], **{'elite': True, 'parent': parent, })
        self.elite.add_classes([Jokaero])
        return None


class DetachPatrol_warhost(DetachPatrol):
    army_name = 'Warhost (Patrol detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'patrol_warhost'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachPatrol_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachBatallion_warhost(DetachBatallion):
    army_name = 'Warhost (Batallion detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'batallion_warhost'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachBatallion_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachBrigade_warhost(DetachBrigade):
    army_name = 'Warhost (Brigade detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'brigade_warhost'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachBrigade_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachVanguard_warhost(DetachVanguard):
    army_name = 'Warhost (Vanguard detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'vanguard_warhost'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachVanguard_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachSpearhead_warhost(DetachSpearhead):
    army_name = 'Warhost (Spearhead detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'spearhead_warhost'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachSpearhead_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachOutrider_warhost(DetachOutrider):
    army_name = 'Warhost (Outrider detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'outrider_warhost'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachOutrider_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachCommand_warhost(DetachCommand):
    army_name = 'Warhost (Supreme command detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'command_warhost'
    army_factions = ['SAIM-HANN', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachCommand_warhost, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.elite.add_classes([Bonesinger])
        return None


class DetachAuxilary_warhost(DetachAuxilary):
    army_name = 'Warhost (Auxilary Support Detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'Auxilary_warhost'
    army_factions = ['WARHOST']

    def __init__(self, parent=None):
        super(DetachAuxilary_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachPatrol__wych_cult_(DetachPatrol):
    army_name = '<Wych Cult> (Patrol detachment)'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF']
    army_id = 'patrol__wych_cult_'
    army_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'YNNARI', 'DRUKHARI', '<WYCH CULT>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPatrol__wych_cult_, self).__init__(*[], **{'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachBatallion__wych_cult_(DetachBatallion):
    army_name = '<Wych Cult> (Batallion detachment)'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF']
    army_id = 'batallion__wych_cult_'
    army_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'YNNARI', 'DRUKHARI', '<WYCH CULT>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachBatallion__wych_cult_, self).__init__(*[], **{'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachVanguard__wych_cult_(DetachVanguard):
    army_name = '<Wych Cult> (Vanguard detachment)'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF']
    army_id = 'vanguard__wych_cult_'
    army_factions = ['DRUKHARI', 'YNNARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', '<WYCH CULT>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachVanguard__wych_cult_, self).__init__(*[], **{'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachOutrider__wych_cult_(DetachOutrider):
    army_name = '<Wych Cult> (Outrider detachment)'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF']
    army_id = 'outrider__wych_cult_'
    army_factions = ['DRUKHARI', 'YNNARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', '<WYCH CULT>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachOutrider__wych_cult_, self).__init__(*[], **{'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachCommand__wych_cult_(DetachCommand):
    army_name = '<Wych Cult> (Supreme command detachment)'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF']
    army_id = 'command__wych_cult_'
    army_factions = ['DRUKHARI', 'YNNARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', '<WYCH CULT>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachCommand__wych_cult_, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachAirWing__wych_cult_(DetachAirWing):
    army_name = '<Wych Cult> (Air Wing detachment)'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF']
    army_id = 'air_wing__wych_cult_'
    army_factions = ['AELDARI', 'DRUKHARI', '<WYCH CULT>', 'YNNARI', '<KABAL>', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF']

    def __init__(self, parent=None):
        super(DetachAirWing__wych_cult_, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Razorwing, Voidraven])
        return None


class DetachAuxilary__wych_cult_(DetachAuxilary):
    army_name = '<Wych Cult> (Auxilary Support Detachment)'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF']
    army_id = 'Auxilary__wych_cult_'
    army_factions = ['<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachAuxilary__wych_cult_, self).__init__(*[], **{'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None


class DetachAuxilary_militarum_auxilla(DetachAuxilary):
    army_name = 'Militarum Auxilla (Auxilary Support Detachment)'
    faction_base = 'MILITARUM AUXILLA'
    alternate_factions = []
    army_id = 'Auxilary_militarum_auxilla'
    army_factions = ['MILITARUM AUXILLA']

    def __init__(self, parent=None):
        super(DetachAuxilary_militarum_auxilla, self).__init__(*[], **{'elite': True, 'parent': parent, })
        self.elite.add_classes([Bullgryns, Ogryns, Ratlings, Nork, OgrynBodyguard])
        return None

detachments = [DetachAuxilary_canoptek, DetachPatrol__sept_,
               DetachBatallion__sept_, DetachBrigade__sept_,
               DetachVanguard__sept_, DetachSpearhead__sept_,
               DetachOutrider__sept_, DetachCommand__sept_,
               DetachSuperHeavy__sept_, DetachSuperHeavyAux__sept_,
               DetachAirWing__sept_, DetachFort__sept_,
               DetachAuxilary__sept_, DetachVanguard_ravenwing,
               DetachOutrider_ravenwing, DetachCommand_ravenwing,
               DetachAirWing_ravenwing, DetachAuxilary_ravenwing,
               DetachPatrol_blood_angels,
               DetachBatallion_blood_angels,
               DetachBrigade_blood_angels,
               DetachVanguard_blood_angels,
               DetachSpearhead_blood_angels,
               DetachOutrider_blood_angels,
               DetachCommand_blood_angels,
               DetachSuperHeavy_blood_angels,
               DetachSuperHeavyAux_blood_angels,
               DetachAirWing_blood_angels,
               DetachAuxilary_blood_angels, DetachPatrol_asuryani,
               DetachBatallion_asuryani, DetachBrigade_asuryani,
               DetachVanguard_asuryani, DetachSpearhead_asuryani,
               DetachOutrider_asuryani, DetachCommand_asuryani,
               DetachSuperHeavy_asuryani,
               DetachSuperHeavyAux_asuryani, DetachAirWing_asuryani,
               DetachAuxilary_asuryani, DetachPatrol_slaanesh,
               DetachBatallion_slaanesh, DetachBrigade_slaanesh,
               DetachVanguard_slaanesh, DetachSpearhead_slaanesh,
               DetachOutrider_slaanesh, DetachCommand_slaanesh,
               DetachAirWing_slaanesh, DetachAuxilary_slaanesh,
               DetachAuxilary_skitarii, DetachVanguard_deathwing,
               DetachCommand_deathwing, DetachAuxilary_deathwing,
               DetachSpearhead_deathwing,
               DetachVanguard_astra_telepathica,
               DetachCommand_astra_telepathica,
               DetachAuxilary_astra_telepathica,
               DetachVanguard_incubi,
               DetachAuxilary_incubi, DetachPatrol_astra_militarum,
               DetachBatallion_astra_militarum,
               DetachBrigade_astra_militarum,
               DetachVanguard_astra_militarum,
               DetachSpearhead_astra_militarum,
               DetachOutrider_astra_militarum,
               DetachCommand_astra_militarum,
               DetachSuperHeavy_astra_militarum,
               DetachSuperHeavyAux_astra_militarum,
               DetachAirWing_astra_militarum,
               DetachAuxilary_astra_militarum,
               DetachPatrol__regiment_, DetachBatallion__regiment_,
               DetachBrigade__regiment_, DetachVanguard__regiment_,
               DetachSpearhead__regiment_, DetachOutrider__regiment_,
               DetachCommand__regiment_, DetachSuperHeavy__regiment_,
               DetachSuperHeavyAux__regiment_,
               DetachAuxilary__regiment_,
               DetachAuxilary_legion_of_the_damned,
               DetachPatrol__mascue_, DetachBatallion__mascue_,
               DetachBrigade__mascue_, DetachVanguard__mascue_,
               DetachSpearhead__mascue_, DetachOutrider__mascue_,
               DetachCommand__mascue_, DetachAuxilary__mascue_,
               DetachPatrol_deathwatch, DetachBatallion_deathwatch,
               DetachBrigade_deathwatch, DetachVanguard_deathwatch,
               DetachSpearhead_deathwatch, DetachOutrider_deathwatch,
               DetachCommand_deathwatch,
               DetachSuperHeavy_deathwatch, DetachSuperHeavyAux_deathwatch,
               DetachAirWing_deathwatch,
               DetachAuxilary_deathwatch,
               DetachVanguard_officio_prefectus,
               DetachCommand_officio_prefectus,
               DetachAuxilary_officio_prefectus,
               DetachPatrol_space_wolves,
               DetachBatallion_space_wolves,
               DetachBrigade_space_wolves,
               DetachVanguard_space_wolves,
               DetachSpearhead_space_wolves,
               DetachOutrider_space_wolves,
               DetachCommand_space_wolves,
               DetachSuperHeavy_space_wolves, DetachSuperHeavyAux_space_wolves,
               DetachAirWing_space_wolves,
               DetachAuxilary_space_wolves, DetachPatrol_tzeentch,
               DetachBatallion_tzeentch, DetachBrigade_tzeentch,
               DetachVanguard_tzeentch, DetachSpearhead_tzeentch,
               DetachOutrider_tzeentch, DetachCommand_tzeentch,
               DetachSuperHeavyAux_tzeentch, DetachAirWing_tzeentch,
               DetachAuxilary_tzeentch, DetachPatrol_drukhari,
               DetachBatallion_drukhari, DetachBrigade_drukhari,
               DetachVanguard_drukhari, DetachSpearhead_drukhari,
               DetachOutrider_drukhari, DetachCommand_drukhari,
               DetachAirWing_drukhari, DetachAuxilary_drukhari,
               DetachPatrol_heretic_astartes,
               DetachBatallion_heretic_astartes,
               DetachBrigade_heretic_astartes,
               DetachVanguard_heretic_astartes,
               DetachSpearhead_heretic_astartes,
               DetachOutrider_heretic_astartes,
               DetachCommand_heretic_astartes,
               DetachSuperHeavy_heretic_astartes,
               DetachSuperHeavyAux_heretic_astartes,
               DetachAirWing_heretic_astartes,
               DetachAuxilary_heretic_astartes, DetachPatrol__legion_,
               DetachBatallion__legion_, DetachBrigade__legion_,
               DetachVanguard__legion_, DetachSpearhead__legion_,
               DetachOutrider__legion_, DetachCommand__legion_,
               DetachSuperHeavy__legion_,
               DetachSuperHeavyAux__legion_, DetachAirWing__legion_,
               DetachAuxilary__legion_, DetachPatrol_dark_angels,
               DetachBatallion_dark_angels, DetachBrigade_dark_angels,
               DetachVanguard_dark_angels,
               DetachSpearhead_dark_angels,
               DetachOutrider_dark_angels, DetachCommand_dark_angels,
               DetachSuperHeavy_dark_angels, DetachSuperHeavyAux_dark_angels,
               DetachAirWing_dark_angels, DetachAuxilary_dark_angels,
               DetachPatrol_khorne, DetachBatallion_khorne,
               DetachBrigade_khorne, DetachVanguard_khorne,
               DetachSpearhead_khorne, DetachOutrider_khorne,
               DetachCommand_khorne, DetachSuperHeavy_khorne,
               DetachSuperHeavyAux_khorne, DetachAirWing_khorne,
               DetachAuxilary_khorne,
               DetachSuperHeavy_questor_traitoris,
               DetachSuperHeavyAux_questor_traitoris,
               DetachVanguard_scholastica_psykana,
               DetachCommand_scholastica_psykana,
               DetachAuxilary_scholastica_psykana,
               DetachPatrol__forge_world_,
               DetachBatallion__forge_world_,
               DetachBrigade__forge_world_,
               DetachVanguard__forge_world_,
               DetachSpearhead__forge_world_,
               DetachOutrider__forge_world_,
               DetachCommand__forge_world_,
               DetachAuxilary__forge_world_, DetachPatrol_necrons,
               DetachBatallion_necrons, DetachBrigade_necrons,
               DetachVanguard_necrons, DetachSpearhead_necrons,
               DetachOutrider_necrons, DetachCommand_necrons,
               DetachSuperHeavy_necrons, DetachSuperHeavyAux_necrons,
               DetachAirWing_necrons, DetachAuxilary_necrons,
               DetachVanguard_fallen, DetachCommand_fallen,
               DetachAuxilary_fallen, DetachAuxilary_kroot,
               DetachPatrol_adeptus_ministorum,
               DetachBatallion_adeptus_ministorum,
               DetachBrigade_adeptus_ministorum,
               DetachVanguard_adeptus_ministorum,
               DetachSpearhead_adeptus_ministorum,
               DetachOutrider_adeptus_ministorum,
               DetachCommand_adeptus_ministorum,
               DetachAuxilary_adeptus_ministorum,
               DetachPatrol_imperium, DetachBatallion_imperium,
               DetachBrigade_imperium, DetachVanguard_imperium,
               DetachSpearhead_imperium, DetachOutrider_imperium,
               DetachCommand_imperium, DetachSuperHeavy_imperium,
               DetachSuperHeavyAux_imperium, DetachAirWing_imperium,
               DetachAuxilary_imperium,
               DetachSuperHeavy_imperial_knights,
               DetachSuperHeavyAux_imperial_knights,
               DetachFort_imperial_knights,
               DetachSuperHeavy_questor_imperialis,
               DetachSuperHeavyAux_questor_imperialis,
               DetachAirWing_aeronautica_imperialis,
               DetachAuxilary_aeronautica_imperialis,
               DetachPatrol_chaos, DetachBatallion_chaos,
               DetachBrigade_chaos, DetachVanguard_chaos,
               DetachSpearhead_chaos, DetachOutrider_chaos,
               DetachCommand_chaos, DetachSuperHeavy_chaos,
               DetachSuperHeavyAux_chaos, DetachAirWing_chaos,
               DetachFort_chaos, DetachAuxilary_chaos,
               DetachSuperHeavy_c_tan_shards,
               DetachSuperHeavyAux_c_tan_shards,
               DetachAuxilary_c_tan_shards,
               DetachVanguard_inquisition, DetachCommand_inquisition,
               DetachAuxilary_inquisition, DetachPatrol_tyranids,
               DetachBatallion_tyranids, DetachBrigade_tyranids,
               DetachVanguard_tyranids, DetachSpearhead_tyranids,
               DetachOutrider_tyranids, DetachCommand_tyranids,
               DetachAirWing_tyranids, DetachFort_tyranids,
               DetachAuxilary_tyranids, DetachPatrol_t_au_empire,
               DetachBatallion_t_au_empire, DetachBrigade_t_au_empire,
               DetachVanguard_t_au_empire,
               DetachSpearhead_t_au_empire,
               DetachOutrider_t_au_empire, DetachCommand_t_au_empire,
               DetachSuperHeavy_t_au_empire,
               DetachSuperHeavyAux_t_au_empire,
               DetachAirWing_t_au_empire, DetachFort_t_au_empire,
               DetachAuxilary_t_au_empire,
               DetachPatrol_adepta_sororitas,
               DetachBatallion_adepta_sororitas,
               DetachBrigade_adepta_sororitas,
               DetachVanguard_adepta_sororitas,
               DetachSpearhead_adepta_sororitas,
               DetachOutrider_adepta_sororitas,
               DetachCommand_adepta_sororitas,
               DetachAuxilary_adepta_sororitas,
               DetachPatrol__dynasty_, DetachBatallion__dynasty_,
               DetachBrigade__dynasty_, DetachVanguard__dynasty_,
               DetachSpearhead__dynasty_, DetachOutrider__dynasty_,
               DetachCommand__dynasty_, DetachSuperHeavy__dynasty_,
               DetachSuperHeavyAux__dynasty_, DetachAirWing__dynasty_,
               DetachAuxilary__dynasty_, DetachPatrol_aeldari,
               DetachBatallion_aeldari, DetachBrigade_aeldari,
               DetachVanguard_aeldari, DetachSpearhead_aeldari,
               DetachOutrider_aeldari, DetachCommand_aeldari,
               DetachSuperHeavy_aeldari, DetachSuperHeavyAux_aeldari,
               DetachAirWing_aeldari, DetachAuxilary_aeldari,
               DetachFort_aeldari,
               DetachPatrol__hive_fleet_,
               DetachBatallion__hive_fleet_,
               DetachBrigade__hive_fleet_,
               DetachVanguard__hive_fleet_,
               DetachSpearhead__hive_fleet_,
               DetachOutrider__hive_fleet_,
               DetachCommand__hive_fleet_, DetachAirWing__hive_fleet_,
               DetachFort__hive_fleet_, DetachAuxilary__hive_fleet_,
               DetachPatrol__chapter_, DetachBatallion__chapter_,
               DetachBrigade__chapter_, DetachVanguard__chapter_,
               DetachSpearhead__chapter_, DetachOutrider__chapter_,
               DetachCommand__chapter_, DetachSuperHeavy__chapter_,
               DetachSuperHeavyAux__chapter_, DetachAirWing__chapter_,
               DetachAuxilary__chapter_,
               DetachSuperHeavy_questor_mechanicus,
               DetachSuperHeavyAux_questor_mechanicus,
               DetachPatrol_death_guard, DetachBatallion_death_guard,
               DetachBrigade_death_guard, DetachVanguard_death_guard,
               DetachSpearhead_death_guard,
               DetachOutrider_death_guard, DetachCommand_death_guard,
               DetachSuperHeavyAux_death_guard,
               DetachAuxilary_death_guard, DetachAuxilary_vespid,
               DetachPatrol_harlequins, DetachBatallion_harlequins,
               DetachBrigade_harlequins, DetachVanguard_harlequins,
               DetachSpearhead_harlequins, DetachOutrider_harlequins,
               DetachCommand_harlequins, DetachAuxilary_harlequins,
               DetachPatrol_ork, DetachBatallion_ork,
               DetachBrigade_ork, DetachVanguard_ork,
               DetachSpearhead_ork, DetachOutrider_ork,
               DetachCommand_ork, DetachSuperHeavy_ork,
               DetachSuperHeavyAux_ork, DetachAirWing_ork,
               DetachAuxilary_ork, DetachPatrol_militarum_tempestus,
               DetachBatallion_militarum_tempestus,
               DetachCommand_militarum_tempestus,
               DetachAuxilary_militarum_tempestus,
               DetachPatrol_genestealer_cults,
               DetachBatallion_genestealer_cults,
               DetachBrigade_genestealer_cults,
               DetachVanguard_genestealer_cults,
               DetachSpearhead_genestealer_cults,
               DetachOutrider_genestealer_cults,
               DetachCommand_genestealer_cults,
               DetachAuxilary_genestealer_cults,
               DetachPatrol__craftworld_,
               DetachBatallion__craftworld_,
               DetachBrigade__craftworld_,
               DetachVanguard__craftworld_,
               DetachSpearhead__craftworld_,
               DetachOutrider__craftworld_,
               DetachCommand__craftworld_,
               DetachSuperHeavy__craftworld_,
               DetachSuperHeavyAux__craftworld_,
               DetachAirWing__craftworld_,
               DetachAuxilary__craftworld_, DetachPatrol__kabal_,
               DetachBatallion__kabal_, DetachVanguard__kabal_,
               DetachSpearhead__kabal_, DetachCommand__kabal_,
               DetachAirWing__kabal_, DetachAuxilary__kabal_,
               DetachPatrol__clan_, DetachBatallion__clan_,
               DetachBrigade__clan_, DetachVanguard__clan_,
               DetachSpearhead__clan_, DetachOutrider__clan_,
               DetachCommand__clan_, DetachSuperHeavy__clan_,
               DetachSuperHeavyAux__clan_, DetachAirWing__clan_,
               DetachAuxilary__clan_, DetachVanguard__ordo_,
               DetachCommand__ordo_, DetachAuxilary__ordo_,
               DetachPatrol_adeptus_astartes,
               DetachBatallion_adeptus_astartes,
               DetachBrigade_adeptus_astartes,
               DetachVanguard_adeptus_astartes,
               DetachSpearhead_adeptus_astartes,
               DetachOutrider_adeptus_astartes,
               DetachCommand_adeptus_astartes,
               DetachSuperHeavy_adeptus_astartes,
               DetachSuperHeavyAux_adeptus_astartes,
               DetachAirWing_adeptus_astartes,
               DetachAuxilary_adeptus_astartes,
               DetachPatrol_cult_mechanicus,
               DetachBatallion_cult_mechanicus,
               DetachVanguard_cult_mechanicus,
               DetachSpearhead_cult_mechanicus,
               DetachCommand_cult_mechanicus,
               DetachAuxilary_cult_mechanicus,
               DetachAuxilary_sisters_of_silence, DetachPatrol_ynnari,
               DetachBatallion_ynnari, DetachBrigade_ynnari,
               DetachVanguard_ynnari, DetachSpearhead_ynnari,
               DetachOutrider_ynnari, DetachCommand_ynnari,
               DetachSuperHeavy_ynnari, DetachSuperHeavyAux_ynnari,
               DetachAirWing_ynnari, DetachAuxilary_ynnari,
               DetachPatrol_nurgle, DetachBatallion_nurgle,
               DetachBrigade_nurgle, DetachVanguard_nurgle,
               DetachSpearhead_nurgle, DetachOutrider_nurgle,
               DetachCommand_nurgle, DetachSuperHeavyAux_nurgle,
               DetachAirWing_nurgle, DetachAuxilary_nurgle,
               DetachPatrol__haemunculus_coven_,
               DetachBatallion__haemunculus_coven_,
               DetachVanguard__haemunculus_coven_,
               DetachSpearhead__haemunculus_coven_,
               DetachCommand__haemunculus_coven_,
               DetachAuxilary__haemunculus_coven_,
               DetachSuperHeavy__household_,
               DetachSuperHeavyAux__household_,
               DetachVanguard_death_company,
               DetachCommand_death_company,
               DetachAuxilary_death_company,
               DetachVanguard_spirit_host,
               DetachSpearhead_spirit_host, DetachCommand_spirit_host,
               DetachSuperHeavy_spirit_host,
               DetachSuperHeavyAux_spirit_host,
               DetachAirWing_spirit_host, DetachAuxilary_spirit_host,
               DetachPatrol__order_, DetachBatallion__order_,
               DetachBrigade__order_, DetachVanguard__order_,
               DetachSpearhead__order_, DetachOutrider__order_,
               DetachCommand__order_, DetachAuxilary__order_,
               DetachAuxilary_officio_assassinorum,
               DetachPatrol_adeptus_custodes,
               DetachBatallion_adeptus_custodes,
               DetachBrigade_adeptus_custodes,
               DetachVanguard_adeptus_custodes,
               DetachSpearhead_adeptus_custodes,
               DetachOutrider_adeptus_custodes,
               DetachCommand_adeptus_custodes,
               DetachAuxilary_adeptus_custodes,
               DetachPatrol_aspect_warrior,
               DetachBatallion_aspect_warrior,
               DetachBrigade_aspect_warrior,
               DetachVanguard_aspect_warrior,
               DetachSpearhead_aspect_warrior,
               DetachOutrider_aspect_warrior,
               DetachCommand_aspect_warrior,
               DetachAirWing_aspect_warrior,
               DetachAuxilary_aspect_warrior,
               DetachPatrol_grey_knights,
               DetachBatallion_grey_knights,
               DetachBrigade_grey_knights,
               DetachVanguard_grey_knights,
               DetachSpearhead_grey_knights,
               DetachOutrider_grey_knights,
               DetachCommand_grey_knights, DetachAirWing_grey_knights,
               DetachAuxilary_grey_knights, DetachPatrol_daemon,
               DetachBatallion_daemon, DetachBrigade_daemon,
               DetachVanguard_daemon, DetachSpearhead_daemon,
               DetachOutrider_daemon, DetachCommand_daemon,
               DetachFort_daemon, DetachAuxilary_daemon,
               DetachPatrol_adeptus_mechanicus,
               DetachBatallion_adeptus_mechanicus,
               DetachBrigade_adeptus_mechanicus,
               DetachVanguard_adeptus_mechanicus,
               DetachSpearhead_adeptus_mechanicus,
               DetachOutrider_adeptus_mechanicus,
               DetachCommand_adeptus_mechanicus,
               DetachAuxilary_adeptus_mechanicus,
               DetachAuxilary_jokaero, DetachPatrol_warhost,
               DetachBatallion_warhost, DetachBrigade_warhost,
               DetachVanguard_warhost, DetachSpearhead_warhost,
               DetachOutrider_warhost, DetachCommand_warhost,
               DetachAuxilary_warhost, DetachPatrol__wych_cult_,
               DetachBatallion__wych_cult_,
               DetachVanguard__wych_cult_, DetachOutrider__wych_cult_,
               DetachCommand__wych_cult_, DetachAirWing__wych_cult_,
               DetachAuxilary__wych_cult_,
               DetachAuxilary_militarum_auxilla,
               DetachPatrol_thousand_sons,
               DetachBatallion_thousand_sons,
               DetachBrigade_thousand_sons,
               DetachVanguard_thousand_sons,
               DetachSpearhead_thousand_sons,
               DetachOutrider_thousand_sons,
               DetachCommand_thousand_sons,
               DetachSuperHeavy_thousand_sons,
               DetachSuperHeavyAux_thousand_sons,
               DetachAirWing_thousand_sons,
               DetachAuxilary_thousand_sons]

class DetachPlanetstrikeAttacker__sept_(DetachPlanetstrikeAttacker):
    army_name = '<Sept> (Planetstrike Attacker detachment)'
    faction_base = '<SEPT>'
    alternate_factions = ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]
    army_id = 'planetstrike attacker__sept_'
    army_factions = ['<SEPT>', "T'AU SEPT", "T'AU EMPIRE", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__sept_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachPlanetstrikeDefender__sept_(DetachPlanetstrikeDefender):
    army_name = '<Sept> (Planetstrike Defender detachment)'
    faction_base = '<SEPT>'
    alternate_factions = ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]
    army_id = 'planetstrike attacker__sept_'
    army_factions = ['<SEPT>', "T'AU SEPT", "T'AU EMPIRE", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__sept_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([PiranhaTeam, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachFortPlanetstrike__sept_(DetachFortPlanetstrike):
    army_name = '<Sept> (Fortification Network)'
    faction_base = '<SEPT>'
    alternate_factions = ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]
    army_id = 'fort__sept_'
    army_factions = ["T'AU EMPIRE", "T'AU SEPT", '<SEPT>', 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike__sept_, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([Shieldline, Droneport, Gunrig])
        return None


class DetachPlanetstrikeAttacker_ravenwing(DetachPlanetstrikeAttacker):
    army_name = 'Ravenwing (Planetstrike Attacker detachment)'
    faction_base = 'RAVENWING'
    alternate_factions = []
    army_id = 'planetstrike attacker_ravenwing'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'DARK ANGELS', 'RAVENWING', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_ravenwing, self).__init__(*[], **{'hq': True, 'elite': True, 'fast': True, 'fliers': True, 'parent': parent, })
        self.hq.add_classes([Sammael, SpeederSammael, RavenwingTalonmaster])
        self.elite.add_classes([RWAncient, RWApothecary, RWChampion])
        self.fast.add_classes([RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, LandSpeederVengeance])
        self.fliers.add_classes([RavenwingDarkTalon, NephilimJetfighter])
        return None


class DetachPlanetstrikeAttacker_blood_angels(DetachPlanetstrikeAttacker):
    army_name = 'Blood Angels (Planetstrike Attacker detachment)'
    faction_base = 'BLOOD ANGELS'
    alternate_factions = ['FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>']
    army_id = 'planetstrike attacker_blood_angels'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'RAVEN GUARD', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', 'CRIMSON FISTS', 'WHITE SCARS', 'IMPERIAL FISTS', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_blood_angels, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexPredator, BADevastators, BaalPredator])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeDefender_blood_angels(DetachPlanetstrikeDefender):
    army_name = 'Blood Angels (Planetstrike Defender detachment)'
    faction_base = 'BLOOD ANGELS'
    alternate_factions = ['FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>']
    army_id = 'planetstrike attacker_blood_angels'
    army_factions = ['IMPERIUM', 'FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_blood_angels, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, CodexVindicator, Whirlwind, CodexWhirlwind, CodexPredator, BADevastators, BaalPredator])
        self.troops.add_classes([BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2])
        self.fliers.add_classes([StormravenGunship])
        self.elite.add_classes([CataphractiiTerminatorSquad, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, ImperialSM, PrimarisAncient, Servitors, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexTerminatorSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought])
        self.hq.add_classes([GravisCaptain, LandRaiderExcelsior, PrimarisLieutenants, RhinoPrimaris, TerminatorChaplain, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth])
        self.fast.add_classes([Inceptors, LandSpeeders, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeAttacker_asuryani(DetachPlanetstrikeAttacker):
    army_name = 'Asuryani (Planetstrike Attacker detachment)'
    faction_base = 'ASURYANI'
    alternate_factions = []
    army_id = 'planetstrike attacker_asuryani'
    army_factions = ['SAIM-HANN', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_asuryani, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPlanetstrikeDefender_asuryani(DetachPlanetstrikeDefender):
    army_name = 'Asuryani (Planetstrike Defender detachment)'
    faction_base = 'ASURYANI'
    alternate_factions = []
    army_id = 'planetstrike attacker_asuryani'
    army_factions = ['SAIM-HANN', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_asuryani, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPlanetstrikeAttacker_slaanesh(DetachPlanetstrikeAttacker):
    army_name = 'Slaanesh (Planetstrike Attacker detachment)'
    faction_base = 'SLAANESH'
    alternate_factions = []
    army_id = 'planetstrike attacker_slaanesh'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'SLAANESH', "EMPEROR'S CHILDREN", 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_slaanesh, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_slaanesh(DetachPlanetstrikeDefender):
    army_name = 'Slaanesh (Planetstrike Defender detachment)'
    faction_base = 'SLAANESH'
    alternate_factions = []
    army_id = 'planetstrike attacker_slaanesh'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'HERETIC ASTARTES', 'SLAANESH', 'IRON WARRIORS', 'RED CORSAIRS', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_slaanesh, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SeekerChariot, ExSeekerChariot])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionNoises, Daemonettes])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, Possessed, Fiends])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Lucius, ExaltedChampion, ChaosDaemonPrince, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Seekers, Hellflayer])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker_astra_militarum(DetachPlanetstrikeAttacker):
    army_name = 'Astra Militarum (Planetstrike Attacker detachment)'
    faction_base = 'ASTRA MILITARUM'
    alternate_factions = []
    army_id = 'planetstrike attacker_astra_militarum'
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_astra_militarum, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachPlanetstrikeDefender_astra_militarum(DetachPlanetstrikeDefender):
    army_name = 'Astra Militarum (Planetstrike Defender detachment)'
    faction_base = 'ASTRA MILITARUM'
    alternate_factions = []
    army_id = 'planetstrike attacker_astra_militarum'
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_astra_militarum, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer])
        self.hq.add_classes([Jacobus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, EnginseerV2])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime])
        return None


class DetachPlanetstrikeAttacker__regiment_(DetachPlanetstrikeAttacker):
    army_name = '<Regiment> (Planetstrike Attacker detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'planetstrike attacker__regiment_'
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachPlanetstrikeDefender__regiment_(DetachPlanetstrikeDefender):
    army_name = '<Regiment> (Planetstrike Defender detachment)'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = 'planetstrike attacker__regiment_'
    army_factions = ['IMPERIUM', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__regiment_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns])
        self.troops.add_classes([Conscripts, InfantrySquad])
        self.transports.add_classes([Chimera, Taurox])
        self.hq.add_classes([CCommander, Creed, Pask, Straken, SlyMarbo, TankCommander])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels])
        self.elite.add_classes([CommandSquad, Harker, Kell, OrdnanceMaster, PCommander, SpecialWeaponSquad, VeteranSquad])
        return None


class DetachPlanetstrikeAttacker__mascue_(DetachPlanetstrikeAttacker):
    army_name = '<Mascue> (Planetstrike Attacker detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'planetstrike attacker__mascue_'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPlanetstrikeDefender__mascue_(DetachPlanetstrikeDefender):
    army_name = '<Mascue> (Planetstrike Defender detachment)'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = 'planetstrike attacker__mascue_'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__mascue_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPlanetstrikeAttacker_deathwatch(DetachPlanetstrikeAttacker):
    army_name = 'Deathwatch (Planetstrike Attacker detachment)'
    faction_base = 'DEATHWATCH'
    alternate_factions = []
    army_id = 'planetstrike attacker_deathwatch'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', 'CRIMSON FISTS', 'WHITE SCARS', 'IMPERIAL FISTS', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_deathwatch, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([LandRaider, LandRaiderCrusader, LandRaiderRedeemer, DWHellblasters])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachPlanetstrikeDefender_deathwatch(DetachPlanetstrikeDefender):
    army_name = 'Deathwatch (Planetstrike Defender detachment)'
    faction_base = 'DEATHWATCH'
    alternate_factions = []
    army_id = 'planetstrike attacker_deathwatch'
    army_factions = ['DEATHWATCH', 'IMPERIUM', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_deathwatch, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([LandRaider, LandRaiderCrusader, LandRaiderRedeemer, DWHellblasters])
        self.troops.add_classes([KillTeam, DWIntercessors, VeteranKillTeam, IntercessorKillTeam])
        self.fliers.add_classes([Corvus])
        self.elite.add_classes([RedemptorDreadnought, PrimarisApothecary, DWReivers, Agressors, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, CodexDWDreadnought, CodexDWVenDreadnought, CodexDWTerminators, CodexDWVanguard])
        self.hq.add_classes([PrimarisLieutenants, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, CodexDWCaptain, CodexTerminatorDWCaptain, PrimarisWatchCapitan, CodexDWLibrarian, CodexTermoDWLibrarian, CodexDWChaplain, CodexTermoDWChaplain])
        self.fast.add_classes([Inceptors, DWBikers, CodexDWBikers])
        self.transports.add_classes([Rhino, DropPod, Repulsor, CodexDWRazorback])
        return None


class DetachPlanetstrikeAttacker_space_wolves(DetachPlanetstrikeAttacker):
    army_name = 'Space Wolves (Planetstrike Attacker detachment)'
    faction_base = 'SPACE WOLVES'
    alternate_factions = []
    army_id = 'planetstrike attacker_space_wolves'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'RAVEN GUARD', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', 'CRIMSON FISTS', 'WHITE SCARS', 'IMPERIAL FISTS', 'SPACE WOLVES', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_space_wolves, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachPlanetstrikeDefender_space_wolves(DetachPlanetstrikeDefender):
    army_name = 'Space Wolves (Planetstrike Defender detachment)'
    faction_base = 'SPACE WOLVES'
    alternate_factions = []
    army_id = 'planetstrike attacker_space_wolves'
    army_factions = ['IMPERIUM', 'SPACE WOLVES', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_space_wolves, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([LongFangs, SWHellblasters, SWHunter,
                                SWStalker, SWWhirlwind, SWPredator, SWVindicator, SWLandRaider,
                                SWLandRaiderCrusader, SWLandRaiderRedeemer])
        self.troops.add_classes([BloodClaws, GreyHunters, SWIntercessors])
        self.fliers.add_classes([Stormwolf, Stormfang, SWStormhawk])
        self.elite.add_classes([ImperialSM, Servitors, SWDreadnought,
                                SWVenDreadnought, Lucas, WolfScouts,
                                Wulfen, LoneWolf, TermLoneWolf,
                                Murderfang, WolfGuards,
                                BikeWolfGuards, WolfGuardTerminators,
                                SWCompanyAncient, SWPrimarisAncient,
                                GreatCompanyChampion,
                                WolfGuardCataphractii,
                                WolfGuardTartaros, SWContemptor,
                                SWRedemptor, SWReivers, SWAggressors,
                                WulfenDreadnought])
        self.hq.add_classes([LandRaiderExcelsior, RhinoPrimaris,
                             Bjorn, Arjac, BikeRunePriest,
                             BikeWolfLord, BikeWolfPriest, Canis,
                             GravisWolfLord, Harald, Krom, Logan,
                             Njal, PimpLogan, Ragnar, RunePriest,
                             TermRunePriest, TermWolfLord,
                             TerminatorWolfPriest, TermoNjal,
                             ThunderwolfLord, Ulrik, WolfLord,
                             WolfPriest, WGBattleLeader,
                             TermWGBattleLeader, WolfWGBattleLeader,
                             BikeWGBattleLeader, IronPriest,
                             BikeIronPriest, WolfIronPriest,
                             PrimarisWolfPriest, PrimarisRunePriest,
                             PrimarisBattleLeader, IronPriestV2,
                             PrimarisWolfLord, CataphractiiWolfLord])
        self.fast.add_classes([Cyberwolves, Swiftclaws,
                               SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack,
                               SWLandSpeeders, SWInceptors])
        self.transports.add_classes([SWRhino, SWRazorback, SWDropPod,
                                     SWLandSpeederStorm, SWRepulsor])
        return None


class DetachPlanetstrikeAttacker_tzeentch(DetachPlanetstrikeAttacker):
    army_name = 'Tzeentch (Planetstrike Attacker detachment)'
    faction_base = 'TZEENTCH'
    alternate_factions = []
    army_id = 'planetstrike attacker_tzeentch'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_tzeentch, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_tzeentch(DetachPlanetstrikeDefender):
    army_name = 'Tzeentch (Planetstrike Defender detachment)'
    faction_base = 'TZEENTCH'
    alternate_factions = []
    army_id = 'planetstrike attacker_tzeentch'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'HERETIC ASTARTES', 'IRON WARRIORS', 'THOUSAND SONS', 'RED CORSAIRS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_tzeentch, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, BurningChariot, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, Horrors, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, RubricMarines, Flamers, ExFlamer, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Changecaster, Fateskimmer, Fluxmaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Screamers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker_drukhari(DetachPlanetstrikeAttacker):
    army_name = 'Drukhari (Planetstrike Attacker detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'planetstrike attacker_drukhari'
    army_factions = ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'DRUKHARI', 'YNNARI', '<WYCH CULT>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachPlanetstrikeDefender_drukhari(DetachPlanetstrikeDefender):
    army_name = 'Drukhari (Planetstrike Defender detachment)'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'planetstrike attacker_drukhari'
    army_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'AELDARI', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', 'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_drukhari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Ravager, Talos])
        self.troops.add_classes([KabaliteWarriors, Wracks, Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.elite.add_classes([Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul])
        self.hq.add_classes([Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus])
        self.fast.add_classes([ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges])
        self.transports.add_classes([Raider, Venom])
        return None


class DetachPlanetstrikeAttacker_heretic_astartes(DetachPlanetstrikeAttacker):
    army_name = 'Heretic Astartes (Planetstrike Attacker detachment)'
    faction_base = 'HERETIC ASTARTES'
    alternate_factions = []
    army_id = 'planetstrike attacker_heretic_astartes'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'KHORNE', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'RED CORSAIRS', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_heretic_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_heretic_astartes(DetachPlanetstrikeDefender):
    army_name = 'Heretic Astartes (Planetstrike Defender detachment)'
    faction_base = 'HERETIC ASTARTES'
    alternate_factions = []
    army_id = 'planetstrike attacker_heretic_astartes'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'KHORNE', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'RED CORSAIRS', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_heretic_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, ExaltedChampion, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker__legion_(DetachPlanetstrikeAttacker):
    army_name = '<Legion> (Planetstrike Attacker detachment)'
    faction_base = '<LEGION>'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN", 'THOUSAND SONS']
    army_id = 'planetstrike attacker__legion_'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'KHORNE', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'RED CORSAIRS', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__legion_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker_thousand_sons(DetachPlanetstrikeAttacker):
    army_name = 'Thousand Sons (Planetstrike Attacker detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'planetstrike attacker_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_thousand_sons(DetachPlanetstrikeDefender):
    army_name = 'Thousand Sons (Planetstrike Defender detachment)'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'planetstrike attacker_thousand_sons'
    army_factions = ['CHAOS', 'TZEENTCH', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_thousand_sons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Maulerfiend, Mutalith])
        self.troops.add_classes([ChaosCultists, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Helbrute, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([DaemonPrince, DiscSorcerer, Sorcerer, TermoSorcerer, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosSpawn, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender__legion_(DetachPlanetstrikeDefender):
    army_name = '<Legion> (Planetstrike Defender detachment)'
    faction_base = '<LEGION>'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN", 'THOUSAND SONS']
    army_id = 'planetstrike attacker__legion_'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'KHORNE', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'RED CORSAIRS', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__legion_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionRubrics, Tzaangors, LegionPlagues, LegionNoises])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, ScarabOccultTerminators, PlagueMarinesV2])
        self.hq.add_classes([Abaddon, Haarken, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Ahriman, ExSorcerer, Lucius, ExaltedChampion])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker_dark_angels(DetachPlanetstrikeAttacker):
    army_name = 'Dark Angels (Planetstrike Attacker detachment)'
    faction_base = 'DARK ANGELS'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'planetstrike attacker_dark_angels'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', 'CRIMSON FISTS', 'WHITE SCARS', 'RAVENWING', 'IMPERIAL FISTS', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_dark_angels, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexDevastators, CodexPredator])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeDefender_dark_angels(DetachPlanetstrikeDefender):
    army_name = 'Dark Angels (Planetstrike Defender detachment)'
    faction_base = 'DARK ANGELS'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'planetstrike attacker_dark_angels'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'SALAMANDERS', 'CRIMSON FISTS', 'WHITE SCARS', 'IMPERIAL FISTS', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_dark_angels, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Devastators, Hellblasters, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Vindicator, Whirlwind, CodexDevastators, CodexPredator])
        self.troops.add_classes([Intercessors, ScoutSquad, TacticalSquad, DATacticalSquad, DAScoutSquad])
        self.fliers.add_classes([RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter])
        self.elite.add_classes([Apothecary, CompanyAncient, Dreadnought, ImperialSM, PrimarisAncient, Servitors, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought])
        self.hq.add_classes([BikeLibrarian, BikeTechmarine, Chaplain, LandRaiderExcelsior, Librarian, PrimarisLieutenants, RhinoPrimaris, Techmarine, TermoLibrarian, PrimarisChaplain, PrimarisLibrarian, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants])
        self.fast.add_classes([AssaultSquad, Inceptors, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance])
        self.transports.add_classes([Razorback, Rhino, DropPod, CodexDropPod, CodexRazorback, Repulsor])
        return None


class DetachPlanetstrikeAttacker_khorne(DetachPlanetstrikeAttacker):
    army_name = 'Khorne (Planetstrike Attacker detachment)'
    faction_base = 'KHORNE'
    alternate_factions = []
    army_id = 'planetstrike attacker_khorne'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'KHORNE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_khorne, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_khorne(DetachPlanetstrikeDefender):
    army_name = 'Khorne (Planetstrike Defender detachment)'
    faction_base = 'KHORNE'
    alternate_factions = []
    army_id = 'planetstrike attacker_khorne'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'KHORNE', 'HERETIC ASTARTES', 'WORLD EATERS', 'IRON WARRIORS', 'RED CORSAIRS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_khorne, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, Bloodletters])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Helbrute, Mutilators, Possessed, Bloodcrushers])
        self.hq.add_classes([Abaddon, BikeLord, ChaosLord, DaemonPrince, DarkApostle, JugLord, TermoLord, Warpsmith, Kharn, ExaltedChampion, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, BloodMaster, Skullmaster, BloodThrone])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeAttacker__forge_world_(DetachPlanetstrikeAttacker):
    army_name = '<Forge World> (Planetstrike Attacker detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'planetstrike attacker__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPlanetstrikeDefender__forge_world_(DetachPlanetstrikeDefender):
    army_name = '<Forge World> (Planetstrike Defender detachment)'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = 'planetstrike attacker__forge_world_'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__forge_world_, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPlanetstrikeAttacker_necrons(DetachPlanetstrikeAttacker):
    army_name = 'Necrons (Planetstrike Attacker detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'planetstrike attacker_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths])
        self.transports.add_classes([GhostArk])
        return None


class DetachPlanetstrikeDefender_necrons(DetachPlanetstrikeDefender):
    army_name = 'Necrons (Planetstrike Defender detachment)'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'planetstrike attacker_necrons'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_necrons, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders, TranscendentCtan])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe])
        self.elite.add_classes([Deathmarks, Deciever, FlayedOnes, Lychguard, Nightbringer, Praetorians, TriarchStalker])
        self.hq.add_classes([Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths])
        self.transports.add_classes([GhostArk])
        return None


class DetachPlanetstrikeAttacker_adeptus_ministorum(DetachPlanetstrikeAttacker):
    army_name = 'Adeptus Ministorum (Planetstrike Attacker detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'planetstrike attacker_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', '<ORDER>']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachPlanetstrikeDefender_adeptus_ministorum(DetachPlanetstrikeDefender):
    army_name = 'Adeptus Ministorum (Planetstrike Defender detachment)'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'planetstrike attacker_adeptus_ministorum'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', '<ORDER>']
    elite_sec = ConclaveElites

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adeptus_ministorum, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Missionary])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, PriestV2, Crusaders, Geminae, Preacher, ConclaveCrusaders])
        return None


class DetachFortPlanetstrike_chaos(DetachFortPlanetstrike):
    army_name = 'Chaos (Fortification Network)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'fort_chaos'
    army_factions = ['DAEMON', 'CHAOS', 'NURGLE']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_chaos, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([ChaosBastion, FeculentGnarlmaws])
        return None


class DetachFortPlanetstrike_tyranids(DetachFortPlanetstrike):
    army_name = 'Tyranids (Fortification Network)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'fort_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_tyranids, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([Sporocyst])
        return None


class DetachPlanetstrikeAttacker_t_au_empire(DetachPlanetstrikeAttacker):
    army_name = "T'au Empire (Planetstrike Attacker detachment)"
    faction_base = "T'AU EMPIRE"
    alternate_factions = []
    army_id = 'planetstrike attacker_t_au_empire'
    army_factions = ["T'AU EMPIRE", "T'AU SEPT", '<SEPT>', 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_t_au_empire, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachPlanetstrikeDefender_t_au_empire(DetachPlanetstrikeDefender):
    army_name = "T'au Empire (Planetstrike Defender detachment)"
    faction_base = "T'AU EMPIRE"
    alternate_factions = []
    army_id = 'planetstrike attacker_t_au_empire'
    army_factions = ['<SEPT>', "T'AU SEPT", "T'AU EMPIRE", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_t_au_empire, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([SniperDrones, Skyray, Hammerhead, BroadsideTeam])
        self.troops.add_classes([StrikeTeam, BreacherTeam, KrootSquad])
        self.fliers.add_classes([RazorShark, SunShark])
        self.elite.add_classes([Shaper, KrootoxSquad, StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide, Marksman])
        self.hq.add_classes([Commander, EnforcerCommander, ColdstarCommander, Ethereal, Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike])
        self.fast.add_classes([KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones])
        self.transports.add_classes([Devilfish])
        return None


class DetachFortPlanetstrike_t_au_empire(DetachFortPlanetstrike):
    army_name = "T'au Empire (Fortification Network)"
    faction_base = "T'AU EMPIRE"
    alternate_factions = []
    army_id = 'fort_t_au_empire'
    army_factions = ["T'AU EMPIRE", "T'AU SEPT", '<SEPT>', 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_t_au_empire, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([Shieldline, Droneport, Gunrig])
        return None


class DetachPlanetstrikeAttacker_adepta_sororitas(DetachPlanetstrikeAttacker):
    army_name = 'Adepta Sororitas (Planetstrike Attacker detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'planetstrike attacker_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachPlanetstrikeDefender_adepta_sororitas(DetachPlanetstrikeDefender):
    army_name = 'Adepta Sororitas (Planetstrike Defender detachment)'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'planetstrike attacker_adepta_sororitas'
    army_factions = ['IMPERIUM', 'ADEPTUS MINISTORUM', 'ADEPTA SORORITAS', '<ORDER>']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adepta_sororitas, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Celestine, Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia, Geminae])
        return None


class DetachPlanetstrikeAttacker__dynasty_(DetachPlanetstrikeAttacker):
    army_name = '<Dynasty> (Planetstrike Attacker detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH']
    army_id = 'planetstrike attacker__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths])
        self.transports.add_classes([GhostArk])
        return None


class DetachPlanetstrikeDefender__dynasty_(DetachPlanetstrikeDefender):
    army_name = '<Dynasty> (Planetstrike Defender detachment)'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH']
    army_id = 'planetstrike attacker__dynasty_'
    army_factions = ['MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', '<DYNASTY>', 'SAUTEKH', 'NECRONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__dynasty_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([AnnihilationBarge, DoomsdayArk, HeavyDestroyers, Monolith, Spyders])
        self.troops.add_classes([Warriors, Immortals])
        self.fliers.add_classes([NightScythe, DoomScythe])
        self.elite.add_classes([Deathmarks, FlayedOnes, Lychguard])
        self.hq.add_classes([CommandBarge, Cryptek, DLord, Imotekh, Lord, Obyron, Orikan, Overlord, Trazyn, Zahndrekh])
        self.fast.add_classes([Destroyers, Scarabs, TombBlades, Wraiths])
        self.transports.add_classes([GhostArk])
        return None


class DetachPlanetstrikeAttacker__hive_fleet_(DetachPlanetstrikeAttacker):
    army_name = '<Hive Fleet> (Planetstrike Attacker detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'planetstrike attacker__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachPlanetstrikeDefender__hive_fleet_(DetachPlanetstrikeDefender):
    army_name = '<Hive Fleet> (Planetstrike Defender detachment)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'planetstrike attacker__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__hive_fleet_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine])
        self.transports.add_classes([Tyrannocyte])
        return None


class DetachFortPlanetstrike__hive_fleet_(DetachFortPlanetstrike):
    army_name = '<Hive Fleet> (Fortification Network)'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = 'fort__hive_fleet_'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'LEVIATHAN', 'KRONOS']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike__hive_fleet_, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([Sporocyst])
        return None


class DetachPlanetstrikeAttacker__chapter_(DetachPlanetstrikeAttacker):
    army_name = '<Chapter> (Planetstrike Attacker detachment)'
    faction_base = '<CHAPTER>'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS', 'IRON HANDS', 'BLOOD RAVENS']
    army_id = 'planetstrike attacker__chapter_'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'RAVEN GUARD', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'IMPERIAL FISTS', 'ULTRAMARINES', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__chapter_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, Eliminators, RepulsorExecutioner])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, InfiltratorSquad, IncursorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, TermoAncient, VitrixGuard, InvictorWarsuit])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, PrimarisCalgar, PrimarisShrike, PrimarisTigurius, PrimarisKhan, Feirros, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, Suppressors])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, Impulsor])
        return None


class DetachPlanetstrikeDefender__chapter_(DetachPlanetstrikeDefender):
    army_name = '<Chapter> (Planetstrike Defender detachment)'
    faction_base = '<CHAPTER>'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS']
    army_id = 'planetstrike attacker__chapter_'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'RAVEN GUARD', 'BLACK TEMPLARS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'IMPERIAL FISTS', 'ULTRAMARINES', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__chapter_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, Eliminators, RepulsorExecutioner])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, InfiltratorSquad, IncursorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, InvictorWarsuit, VitrixGuard, TermoAncient])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, PrimarisCalgar, PrimarisTigurius, PrimarisKhan, PrimarisShrike, Feirros, GabrielAngelos])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, Suppressors])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, Impulsor])
        return None


class DetachPlanetstrikeAttacker_death_guard(DetachPlanetstrikeAttacker):
    army_name = 'Death Guard (Planetstrike Attacker detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'planetstrike attacker_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'HERETIC ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachPlanetstrikeDefender_death_guard(DetachPlanetstrikeDefender):
    army_name = 'Death Guard (Planetstrike Defender detachment)'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'planetstrike attacker_death_guard'
    army_factions = ['NIGHT LORDS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_death_guard, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, Defiler, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, LegionPlagues, DGPlagueMarinesV2, Poxwalkers])
        self.transports.add_classes([ChaosRhino])
        self.hq.add_classes([PalLord, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosSpawn, BloatDrone, BlightHaulers])
        self.elite.add_classes([Helbrute, Possessed, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        return None


class DetachPlanetstrikeAttacker_harlequins(DetachPlanetstrikeAttacker):
    army_name = 'Harlequins (Planetstrike Attacker detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'planetstrike attacker_harlequins'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPlanetstrikeDefender_harlequins(DetachPlanetstrikeDefender):
    army_name = 'Harlequins (Planetstrike Defender detachment)'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'planetstrike attacker_harlequins'
    army_factions = ['HARLEQUINS', 'YNNARI', '<MASCUE>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_harlequins, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Voidweavers])
        self.troops.add_classes([Troupe])
        self.transports.add_classes([Starweaver])
        self.hq.add_classes([TroupeMaster, Shadowseer])
        self.fast.add_classes([Skyweavers])
        self.elite.add_classes([DeathJester, Solitaire])
        return None


class DetachPlanetstrikeAttacker_ork(DetachPlanetstrikeAttacker):
    army_name = 'Ork (Planetstrike Attacker detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'planetstrike attacker_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPlanetstrikeDefender_ork(DetachPlanetstrikeDefender):
    army_name = 'Ork (Planetstrike Defender detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'planetstrike attacker_ork'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_ork, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPlanetstrikeAttacker_genestealer_cults(DetachPlanetstrikeAttacker):
    army_name = 'Genestealer Cults (Planetstrike Attacker detachment)'
    faction_base = 'GENESTEALER CULTS'
    alternate_factions = []
    army_id = 'planetstrike attacker_genestealer_cults'
    army_factions = ['IMPERIUM', 'GENESTEALER CULTS', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'TYRANIDS', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_genestealer_cults, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([CCommander, LordCommissar, Primaris, TankCommander, TempestorPrime, EnginseerV2, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime, GoliathTruck, CultChimera])
        return None


class DetachPlanetstrikeDefender_genestealer_cults(DetachPlanetstrikeDefender):
    army_name = 'Genestealer Cults (Planetstrike Defender detachment)'
    faction_base = 'GENESTEALER CULTS'
    alternate_factions = []
    army_id = 'planetstrike attacker_genestealer_cults'
    army_factions = ['IMPERIUM', 'GENESTEALER CULTS', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'TYRANIDS', 'BLOOD BROTHERS', 'MORDIAN', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_genestealer_cults, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([Conscripts, InfantrySquad, TempestusSquad, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Valkyries])
        self.elite.add_classes([Priest, Bullgryns, CommandSquad, Commissar, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([CCommander, LordCommissar, Primaris, TankCommander, TempestorPrime, EnginseerV2, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Chimera, Taurox, TauroxPrime, GoliathTruck, CultChimera])
        return None


class DetachPlanetstrikeAttacker__craftworld_(DetachPlanetstrikeAttacker):
    army_name = '<Craftworld> (Planetstrike Attacker detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'planetstrike attacker__craftworld_'
    army_factions = ['SAIM-HANN', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPlanetstrikeDefender__craftworld_(DetachPlanetstrikeDefender):
    army_name = '<Craftworld> (Planetstrike Defender detachment)'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = 'planetstrike attacker__craftworld_'
    army_factions = ['SAIM-HANN', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'ASPECT WARRIOR', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__craftworld_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger])
        self.hq.add_classes([Autarch, Avatar, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders])
        self.transports.add_classes([WaveSerpent])
        return None


class DetachPlanetstrikeDefender__kabal_(DetachPlanetstrikeDefender):
    army_name = '<Kabal> (Planetstrike Defender detachment)'
    faction_base = '<KABAL>'
    alternate_factions = []
    army_id = 'planetstrike attacker__kabal_'
    army_factions = ['YNNARI', 'DRUKHARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__kabal_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Ravager])
        self.troops.add_classes([KabaliteWarriors])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Archon])
        self.elite.add_classes([KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul])
        return None


class DetachPlanetstrikeAttacker__clan_(DetachPlanetstrikeAttacker):
    army_name = '<Clan> (Planetstrike Attacker detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'planetstrike attacker__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPlanetstrikeDefender__clan_(DetachPlanetstrikeDefender):
    army_name = '<Clan> (Planetstrike Defender detachment)'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = 'planetstrike attacker__clan_'
    army_factions = ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'ORK', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__clan_, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([BigGunz, MekGunz, Battlewagon, Gunwagon, Bonebreaka, KillaKans, Morkanaut, Gorkanaut, Lootas, FlashGitz, DeffDreads])
        self.troops.add_classes([Boyz, Gretchin])
        self.fliers.add_classes([Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet])
        self.elite.add_classes([Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek, NobWithBanner, Nobz, NobzOnWarbikes, Painboy, PainboyV2, MekV2, PainboyOnWarbike, Runtherd, TankBustas])
        self.hq.add_classes([BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk, GhazkullThraka, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy, ShokkBigMek, DeffkillaWartrike])
        self.fast.add_classes([Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies, ShokkjumpDragstas, WarBuggies])
        self.transports.add_classes([Trukk])
        return None


class DetachPlanetstrikeAttacker_adeptus_astartes(DetachPlanetstrikeAttacker):
    army_name = 'Adeptus Astartes (Planetstrike Attacker detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'planetstrike attacker_adeptus_astartes'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'GREY KNIGHTS', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'RAVENWING', 'IMPERIAL FISTS', 'SPACE WOLVES', 'ULTRAMARINES', 'IRON HANDS', 'BLOOD RAVENS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeDefender_adeptus_astartes(DetachPlanetstrikeDefender):
    army_name = 'Adeptus Astartes (Planetstrike Defender detachment)'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'planetstrike attacker_adeptus_astartes'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'DEATHWATCH', 'RAVEN GUARD', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'GREY KNIGHTS', 'IMPERIAL FISTS', 'SPACE WOLVES', 'ULTRAMARINES', 'IRON HANDS', "BLOOD RAVENS"]

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adeptus_astartes, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, RelicContemptor, RelicDeredeo, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad])
        self.transports.add_classes([Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeDefender_cult_mechanicus(DetachPlanetstrikeDefender):
    army_name = 'Cult Mechanicus (Planetstrike Defender detachment)'
    faction_base = 'CULT MECHANICUS'
    alternate_factions = []
    army_id = 'planetstrike attacker_cult_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_cult_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([Enginseer, Fulgurites, Corpuscarii, Datasmith])
        self.troops.add_classes([Breachers, KatDestroyers])
        return None


class DetachPlanetstrikeAttacker_ynnari(DetachPlanetstrikeAttacker):
    army_name = 'Ynnari (Planetstrike Attacker detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'planetstrike attacker_ynnari'
    army_factions = ['SAIM-HANN', 'AELDARI', 'DRUKHARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', '<WYCH CULT>', 'YNNARI', 'HARLEQUINS', 'ASPECT WARRIOR', '<MASCUE>', 'BIEL-TAN', 'WARHOST', 'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachPlanetstrikeDefender_ynnari(DetachPlanetstrikeDefender):
    army_name = 'Ynnari (Planetstrike Defender detachment)'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'planetstrike attacker_ynnari'
    army_factions = ['SAIM-HANN', 'AELDARI', '<MASCUE>', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ALATOIC', 'BIEL-TAN', 'WARHOST', 'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_ynnari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Ravager, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Incubi, KabaliteTrueborn, Lhamaean, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Lelith, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        self.fac_sel.default_value.used = self.fac_sel.default_value.visible = False
        return None


class DetachPlanetstrikeAttacker_nurgle(DetachPlanetstrikeAttacker):
    army_name = 'Nurgle (Planetstrike Attacker detachment)'
    faction_base = 'NURGLE'
    alternate_factions = []
    army_id = 'planetstrike attacker_nurgle'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'NURGLE', 'IRON WARRIORS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_nurgle, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_nurgle(DetachPlanetstrikeDefender):
    army_name = 'Nurgle (Planetstrike Defender detachment)'
    faction_base = 'NURGLE'
    alternate_factions = []
    army_id = 'planetstrike attacker_nurgle'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'CHAOS', 'DEATH GUARD', 'HERETIC ASTARTES', 'NURGLE', 'IRON WARRIORS', 'RED CORSAIRS', 'RENEGADES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_nurgle, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, PlagueCrawler])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionPlagues, Plaguebearers, Nurglings, DGPlagueMarinesV2, Poxwalkers])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([ChaosTerminators, Chosen, Helbrute, Mutilators, PlagueMarines, Possessed, PlagueMarinesV2, NurgleBeasts, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, PalLord, PalSorcerer, Sorcerer, TermoLord, TermoSorcerer, Warpsmith, ExaltedChampion, ChaosDaemonPrince, Epidemius, Unclean, HNurgle, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, PlagueDrones, BloatDrone, BlightHaulers])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachFortPlanetstrike_nurgle(DetachFortPlanetstrike):
    army_name = 'Nurgle (Fortification Network)'
    faction_base = 'NURGLE'
    alternate_factions = []
    army_id = 'fort_nurgle'
    army_factions = ['DAEMON', 'NURGLE', 'CHAOS']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_nurgle, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([FeculentGnarlmaws])
        return None


class DetachPlanetstrikeDefender__haemunculus_coven_(DetachPlanetstrikeDefender):
    army_name = '<Haemunculus Coven> (Planetstrike Defender detachment)'
    faction_base = '<HAEMUNCULUS COVEN>'
    alternate_factions = ['PROPHETS OF FLESH']
    army_id = 'planetstrike attacker__haemunculus_coven_'
    army_factions = ['<HAEMUNCULUS COVEN>', 'AELDARI', 'DRUKHARI', 'PROPHETS OF FLESH']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__haemunculus_coven_, self).__init__(*[], **{'heavy': True, 'elite': True, 'hq': True, 'transports': True, 'troops': True, 'parent': parent, })
        self.heavy.add_classes([Cronos, Talos])
        self.elite.add_classes([Grotesques])
        self.hq.add_classes([Haemunculus, Rakarth])
        self.transports.add_classes([Raider, Venom])
        self.troops.add_classes([Wracks])
        return None


class DetachPlanetstrikeAttacker__order_(DetachPlanetstrikeAttacker):
    army_name = '<Order> (Planetstrike Attacker detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'planetstrike attacker__order_'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachPlanetstrikeDefender__order_(DetachPlanetstrikeDefender):
    army_name = '<Order> (Planetstrike Defender detachment)'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = 'planetstrike attacker__order_'
    army_factions = ['IMPERIUM', 'ADEPTA SORORITAS', '<ORDER>', 'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender__order_, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors])
        self.troops.add_classes([BattleSisters])
        self.transports.add_classes([ASRhino, Immolator])
        self.hq.add_classes([Canoness])
        self.fast.add_classes([Dominions, Seraphims])
        self.elite.add_classes([Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Repentia])
        return None


class DetachPlanetstrikeAttacker_adeptus_custodes(DetachPlanetstrikeAttacker):
    army_name = 'Adeptus Custodes (Planetstrike Attacker detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'planetstrike attacker_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachPlanetstrikeDefender_adeptus_custodes(DetachPlanetstrikeDefender):
    army_name = 'Adeptus Custodes (Planetstrike Defender detachment)'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'planetstrike attacker_adeptus_custodes'
    army_factions = ['IMPERIUM', 'ADEPTUS CUSTODES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adeptus_custodes, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([VenLandRaider])
        self.hq.add_classes([Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.troops.add_classes([CustodianSquad, CustodianGuard])
        self.fast.add_classes([VertusPraetors])
        return None


class DetachPlanetstrikeAttacker_aspect_warrior(DetachPlanetstrikeAttacker):
    army_name = 'Aspect Warrior (Planetstrike Attacker detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'planetstrike attacker_aspect_warrior'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachPlanetstrikeDefender_aspect_warrior(DetachPlanetstrikeDefender):
    army_name = 'Aspect Warrior (Planetstrike Defender detachment)'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'planetstrike attacker_aspect_warrior'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_aspect_warrior, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Reapers])
        self.troops.add_classes([DireAvengers])
        self.fliers.add_classes([CrimsonHunter, HunterExarch])
        self.hq.add_classes([Asurmen, Avatar, Baharroth, Fuegan, JainZar, Karandras, Maugan])
        self.fast.add_classes([Hawks, Spears, Spiders])
        self.elite.add_classes([Banshees, Dragons, Scorpions])
        return None


class DetachPlanetstrikeAttacker_grey_knights(DetachPlanetstrikeAttacker):
    army_name = 'Grey Knights (Planetstrike Attacker detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'planetstrike attacker_grey_knights'
    army_factions = ['IMPERIUM', 'GREY KNIGHTS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeDefender_grey_knights(DetachPlanetstrikeDefender):
    army_name = 'Grey Knights (Planetstrike Defender detachment)'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'planetstrike attacker_grey_knights'
    army_factions = ['IMPERIUM', 'GREY KNIGHTS', 'ADEPTUS ASTARTES']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_grey_knights, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer])
        self.troops.add_classes([StrikeSquad, GKTerminatorSquad])
        self.fliers.add_classes([GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors])
        self.hq.add_classes([Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian])
        self.fast.add_classes([InterceptorSquad])
        self.transports.add_classes([GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeAttacker_daemon(DetachPlanetstrikeAttacker):
    army_name = 'Daemon (Planetstrike Attacker detachment)'
    faction_base = 'DAEMON'
    alternate_factions = []
    army_id = 'planetstrike attacker_daemon'
    army_factions = ['DAEMON', 'TZEENTCH', 'CHAOS', 'KHORNE', 'SLAANESH', 'NURGLE']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_daemon, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachPlanetstrikeDefender_daemon(DetachPlanetstrikeDefender):
    army_name = 'Daemon (Planetstrike Defender detachment)'
    faction_base = 'DAEMON'
    alternate_factions = []
    army_id = 'planetstrike attacker_daemon'
    army_factions = ['DAEMON', 'TZEENTCH', 'CHAOS', 'KHORNE', 'SLAANESH', 'NURGLE']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_daemon, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot])
        self.hq.add_classes([Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener])
        self.elite.add_classes([Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends])
        self.troops.add_classes([Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes])
        self.fast.add_classes([Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer])
        return None


class DetachFortPlanetstrike_daemon(DetachFortPlanetstrike):
    army_name = 'Daemon (Fortification Network)'
    faction_base = 'DAEMON'
    alternate_factions = []
    army_id = 'fort_daemon'
    army_factions = ['DAEMON', 'NURGLE', 'CHAOS']

    def __init__(self, parent=None):
        super(DetachFortPlanetstrike_daemon, self).__init__(*[], **{'fort': True, 'parent': parent, })
        self.fort.add_classes([FeculentGnarlmaws])
        return None


class DetachPlanetstrikeAttacker_adeptus_mechanicus(DetachPlanetstrikeAttacker):
    army_name = 'Adeptus Mechanicus (Planetstrike Attacker detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'planetstrike attacker_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPlanetstrikeDefender_adeptus_mechanicus(DetachPlanetstrikeDefender):
    army_name = 'Adeptus Mechanicus (Planetstrike Defender detachment)'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'planetstrike attacker_adeptus_mechanicus'
    army_factions = ['RYZA', 'IMPERIUM', 'ADEPTUS MECHANICUS', 'STYGIES VIII', 'LUCIUS', 'AGRIPINAA', 'METALICA', 'MARS', 'GRAIA', '<FORGE WORLD>', 'CULT MECHANICUS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_adeptus_mechanicus, self).__init__(*[], **{'heavy': True, 'hq': True, 'elite': True, 'troops': True, 'fast': True, 'parent': parent, })
        self.heavy.add_classes([KastelanManiple, Dunecrawler])
        self.hq.add_classes([Cawl, Dominus, EnginseerV2])
        self.elite.add_classes([IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers])
        self.troops.add_classes([Breachers, KatDestroyers, SkitariiRangers, Vanguard])
        self.fast.add_classes([Dragoons, Ironstriders])
        return None


class DetachPlanetstrikeAttacker_warhost(DetachPlanetstrikeAttacker):
    army_name = 'Warhost (Planetstrike Attacker detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'planetstrike attacker_warhost'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachPlanetstrikeDefender_warhost(DetachPlanetstrikeDefender):
    army_name = 'Warhost (Planetstrike Defender detachment)'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'planetstrike attacker_warhost'
    army_factions = ['SAIM-HANN', 'IYANDEN', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN', 'WARHOST']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_warhost, self).__init__(*[], **{'heavy': True, 'troops': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, VaulWrath, Walkers])
        self.troops.add_classes([GuardianDefenders, Rangers, StormGuardians])
        self.transports.add_classes([WaveSerpent])
        self.hq.add_classes([Autarch, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Illic, JumpingAutarch, SwoopingAutarch, Warlock, WarlockConclave, Yriel])
        self.fast.add_classes([Vypers, Windriders])
        self.elite.add_classes([Bonesinger])
        return None


class DetachPlanetstrikeAttacker__wych_cult_(DetachPlanetstrikeAttacker):
    army_name = '<Wych Cult> (Planetstrike Attacker detachment)'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['WYCH CULT OF STRIFE']
    army_id = 'planetstrike attacker__wych_cult_'
    army_factions = ['DRUKHARI', 'YNNARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', '<WYCH CULT>', 'AELDARI']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker__wych_cult_, self).__init__(*[], **{'troops': True, 'fliers': True, 'transports': True, 'hq': True, 'fast': True, 'elite': True, 'parent': parent, })
        self.troops.add_classes([Wyches])
        self.fliers.add_classes([Razorwing, Voidraven])
        self.transports.add_classes([Raider, Venom])
        self.hq.add_classes([Lelith, Succubus])
        self.fast.add_classes([Hellions, Reavers])
        self.elite.add_classes([Beastmaster, Bloodbrides])
        return None
mission_detachments = {'Planetstrike (Attacker)':
                       [DetachPlanetstrikeAttacker__sept_,
                        DetachPlanetstrikeAttacker_ravenwing,
                        DetachPlanetstrikeAttacker_blood_angels,
                        DetachPlanetstrikeAttacker_asuryani,
                        DetachPlanetstrikeAttacker_slaanesh,
                        DetachPlanetstrikeAttacker_astra_militarum,
                        DetachPlanetstrikeAttacker__regiment_,
                        DetachPlanetstrikeAttacker__mascue_,
                        DetachPlanetstrikeAttacker_deathwatch,
                        DetachPlanetstrikeAttacker_space_wolves,
                        DetachPlanetstrikeAttacker_tzeentch,
                        DetachPlanetstrikeAttacker_drukhari,
                        DetachPlanetstrikeAttacker_heretic_astartes,
                        DetachPlanetstrikeAttacker__legion_,
                        DetachPlanetstrikeAttacker_dark_angels,
                        DetachPlanetstrikeAttacker_khorne,
                        DetachPlanetstrikeAttacker__forge_world_,
                        DetachPlanetstrikeAttacker_necrons,
                        DetachPlanetstrikeAttacker_adeptus_ministorum,
                        DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_chaos,
                        DetachPlanetstrikeAttacker_tyranids,
                        DetachPlanetstrikeAttacker_t_au_empire,
                        DetachPlanetstrikeAttacker_adepta_sororitas,
                        DetachPlanetstrikeAttacker__dynasty_,
                        DetachPlanetstrikeAttacker_aeldari,
                        DetachPlanetstrikeAttacker__hive_fleet_,
                        DetachPlanetstrikeAttacker__chapter_,
                        DetachPlanetstrikeAttacker_death_guard,
                        DetachPlanetstrikeAttacker_harlequins, DetachPlanetstrikeAttacker_ork,
                        DetachPlanetstrikeAttacker_genestealer_cults,
                        DetachPlanetstrikeAttacker__craftworld_,
                        DetachPlanetstrikeAttacker__clan_,
                        DetachPlanetstrikeAttacker_adeptus_astartes,
                        DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker_nurgle,
                        DetachPlanetstrikeAttacker__order_,
                        DetachPlanetstrikeAttacker_adeptus_custodes,
                        DetachPlanetstrikeAttacker_aspect_warrior,
                        DetachPlanetstrikeAttacker_grey_knights,
                        DetachPlanetstrikeAttacker_daemon,
                        DetachPlanetstrikeAttacker_adeptus_mechanicus,
                        DetachPlanetstrikeAttacker_warhost,
                        DetachPlanetstrikeAttacker__wych_cult_,
                        DetachPlanetstrikeAttacker_thousand_sons],
                       'Planetstrike (Defender)':
                       [DetachPlanetstrikeDefender__sept_, DetachFortPlanetstrike__sept_,
                        DetachPlanetstrikeDefender_blood_angels,
                        DetachPlanetstrikeDefender_asuryani,
                        DetachPlanetstrikeDefender_slaanesh,
                        DetachPlanetstrikeDefender_astra_militarum,
                        DetachPlanetstrikeDefender__regiment_,
                        DetachPlanetstrikeDefender__mascue_,
                        DetachPlanetstrikeDefender_deathwatch,
                        DetachPlanetstrikeDefender_space_wolves,
                        DetachPlanetstrikeDefender_tzeentch,
                        DetachPlanetstrikeDefender_drukhari,
                        DetachPlanetstrikeDefender_heretic_astartes,
                        DetachPlanetstrikeDefender__legion_,
                        DetachPlanetstrikeDefender_dark_angels,
                        DetachPlanetstrikeDefender_khorne,
                        DetachPlanetstrikeDefender__forge_world_,
                        DetachPlanetstrikeDefender_necrons,
                        DetachPlanetstrikeDefender_adeptus_ministorum,
                        DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_chaos,
                        DetachFortPlanetstrike_chaos, DetachPlanetstrikeDefender_tyranids,
                        DetachFortPlanetstrike_tyranids,
                        DetachPlanetstrikeDefender_t_au_empire,
                        DetachFortPlanetstrike_t_au_empire,
                        DetachPlanetstrikeDefender_adepta_sororitas,
                        DetachPlanetstrikeDefender__dynasty_,
                        DetachPlanetstrikeDefender_aeldari,
                        DetachPlanetstrikeDefender__hive_fleet_,
                        DetachFortPlanetstrike__hive_fleet_,
                        DetachPlanetstrikeDefender__chapter_,
                        DetachPlanetstrikeDefender_death_guard,
                        DetachPlanetstrikeDefender_harlequins, DetachPlanetstrikeDefender_ork,
                        DetachPlanetstrikeDefender_genestealer_cults,
                        DetachPlanetstrikeDefender__craftworld_,
                        DetachPlanetstrikeDefender__kabal_, DetachPlanetstrikeDefender__clan_,
                        DetachPlanetstrikeDefender_adeptus_astartes,
                        DetachPlanetstrikeDefender_cult_mechanicus,
                        DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender_nurgle,
                        DetachFortPlanetstrike_nurgle,
                        DetachPlanetstrikeDefender__haemunculus_coven_,
                        DetachPlanetstrikeDefender__order_,
                        DetachPlanetstrikeDefender_adeptus_custodes,
                        DetachPlanetstrikeDefender_aspect_warrior,
                        DetachPlanetstrikeDefender_grey_knights,
                        DetachPlanetstrikeDefender_daemon, DetachFortPlanetstrike_daemon,
                        DetachPlanetstrikeDefender_adeptus_mechanicus,
                        DetachPlanetstrikeDefender_warhost,
                        DetachPlanetstrikeDefender_thousand_sons], }

detachments += [DamnedVanguard, SilenceVanguard, AssassinVanguard]
