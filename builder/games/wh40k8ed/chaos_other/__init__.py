from .lords import RenegadeKnight, RenegadeDominus, RenegadeArmigers
from .fort import ChaosBastion, NoctilithCrown

unit_types = [RenegadeKnight, RenegadeDominus, RenegadeArmigers,
              ChaosBastion, NoctilithCrown]
