__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from . import melee, ranged


def add_special_weapons(instance):
    instance.variant(*ranged.ArcRifle)
    instance.variant(*ranged.PlasmaCaliver)
    instance.variant(*ranged.TransuranicArquebus)


def add_pistol_weapons(instance):
    instance.variant(*ranged.ArcPistol)
    instance.variant(*ranged.PhosphorBlastPistol)
    instance.variant(*ranged.RadiumPistol)


def add_melee_weapons(instance):
    instance.variant(*melee.ArcMaul)
    instance.variant(*melee.PowerSword)
    instance.variant(*melee.TaserGoad)


def add_caparace_weapons(instance):
    instance.variant(*ranged.TwinIcarusAutocannon)
    instance.variant(*ranged.StormspearRocketPod)
    instance.variant(*ranged.IronstormMissilePod)


class ForgeworldUnit(Unit):
    faction = ['Imperium', 'Adeptus Mechanicus', '<Forge World>']
    wiki_faction = 'Adeptus Mechanicus'

    @classmethod
    def calc_faction(cls):
        return ['MARS', 'GRAIA', 'METALICA', 'LUCIUS',
                'AGRIPINAA', 'STYGIES VIII', 'RYZA']


class CultUnit(ForgeworldUnit):
    faction = ['Cult Mechanicus']


class SkitariiUnit(ForgeworldUnit):
    faction = ['Skitarii']


class KnightUnit(Unit):
    faction = ['Questor Mechanicus', 'Imperium', '<Household>']
    keywords = ['Titanic', 'Vehicle']
    wiki_faction = 'Questor Imperialis'
