ArcPistol = ('Arc pistol', 3)
ArcRifle = ('Arc rifle', 4)
AvengerGatlingCannon = ('Avenger gatling cannon', 75)
CognisFlamer = ('Cognis flamer', 7)
CognisHeavyStubber = ('Cognis heavy stubber', 2)
EradicationBeamer = ('Eradication beamer', 30)
EradicationRay = ('Eradication ray', 10)
FlechetteBlaster = ('Flechette blaster', 2)
GalvanicRifle = ('Galvanic rifle', 0)
GammaPistol = ('Gamma pistol', 10)
HeavyArcRifle = ('Heavy arc rifle', 6)
HeavyBolter = ('Heavy bolter', 10)
HeavyFlamer = ('Heavy flamer', 14)
HeavyGravCannon = ('Heavy grav-cannon', 30)
HeavyPhosphorBlaster = ('Heavy phosphor blaster', 15)
HeavyStubber = ('Heavy stubber', 2)
IcarusArray = ('Icarus array', 40)
IncendineCombustor = ('Incendine combustor', 15)
IronstormMissilePod = ('Ironstorm missile pod', 16)
Laspistol = ('Laspistol', 0)
Macrostubber = ('Macrostubber', 2)
Meltagun = ('Meltagun', 14)
MultiMelta = ('Multi-melta', 22)
NeutronLaser = ('Neutron laser', 45)
PhosphorBlastPistol = ('Phosphor blast pistol', 3)
PhosphorBlaster = ('Phosphor blaster', 6)
PhosphorSerpenta = ('Phosphor serpenta', 4)
PlasmaCaliver = ('Plasma caliver', 11)
PlasmaCannon = ('Plasma cannon', 16)
PlasmaCulverin = ('Plasma culverin', 27)
RadiumCarbine = ('Radium carbine', 0)
RadiumJezzail = ('Radium jezzail', 4)
RadiumPistol = ('Radium pistol', 0)
RapidFireBattleCannon = ('Rapid-fire battle cannon', 100)
StormspearRocketPod = ('Stormspear rocket pod', 45)
Stubcarbine = ('Stubcarbine', 2)
ThermalCannon = ('Thermal cannon', 76)
TorsionCannon = ('Torsion cannon', 20)
TransuranicArquebus = ('Transuranic arquebus', 15)
TwinCognisAutocannon = ('Twin cognis autocannon', 20)
TwinCognisLascannon = ('Twin cognis lascannon', 40)
TwinHeavyPhosphorBlaster = ('Twin heavy phosphor blaster', 30)
TwinIcarusAutocannon = ('Twin Icarus autocannon', 20)
VolkiteBlaster = ('Volkite blaster', 8)
