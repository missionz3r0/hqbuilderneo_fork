__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
from . import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class Cawl(HqUnit, Unit):
    type_name = get_name(units.BelisariusCawl)
    type_id = 'cawl_v1'
    faction = ['Imperium', 'Adeptus Mechanicus', 'Cult Mechanicus', 'Mars']
    keywords = ['Character', 'Infantry', 'Tech-priest']
    power = 13

    def __init__(self, parent):
        super(Cawl, self).__init__(parent, 'Belisarius Cawl', points=get_cost(units.BelisariusCawl),
                                   gear=[Gear('Arc scourge'), Gear('Omnissiah axe'),
                                         Gear('Mechadendrite Hive'),
                                         Gear('Solar atomizer')],
                                   static=True, unique=True)


class Dominus(HqUnit, armory.CultUnit):
    type_id = 'dominus_v1'
    type_name = get_name(units.TechPriestDominus)
    power = 7

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Dominus.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.VolkiteBlaster)
            self.variant(*ranged.EradicationRay)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Dominus.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.Macrostubber)
            self.variant(*ranged.PhosphorSerpenta)

    def __init__(self, parent):
        super(Dominus, self).__init__(
            parent, points=get_costs(units.TechPriestDominus, melee.OmnissianAxe),
            gear=create_gears(melee.OmnissianAxe))
        self.weapons = [
            self.Weapon1(self),
            self.Weapon2(self)
        ]


class EnginseerV2(HqUnit, armory.CultUnit):
    type_name = get_name(units.TechPriestEnginseer)
    type_id = 'enginseer_v2'
    kwname = 'Enginseer'

    faction = ['Astra Militarum']
    keywords = ['Character', 'Infantry', 'Tech-priest']
    power = 3

    @classmethod
    def check_faction(cls, fac):
        return super(EnginseerV2, cls).check_faction(fac) or fac in ['TYRANIDS', 'GENESTEALER CULTS']

    def __init__(self, parent):
        gear = [melee.OmnissianAxe, ranged.Laspistol, melee.ServoArm]
        cost = points_price(get_cost(units.TechPriestEnginseer), *gear)
        super(EnginseerV2, self).__init__(parent, points=cost, static=True,
                                          gear=create_gears(*gear))
