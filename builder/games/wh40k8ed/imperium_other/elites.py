__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import Gear, UnitDescription, OneOf,\
    ListSubUnit, UnitList, Count
from . import armory
from builder.games.wh40k8ed.utils import *
from . import units, wargear


class VindicareAssasin(ElitesUnit, armory.AssassinUnit):
    type_name = get_name(units.Vindicare)
    type_id = "vindicare_assasin_v1"
    keywords = ['Infantry', 'Character']
    power = 5

    def __init__(self, parent):
        super(VindicareAssasin, self).__init__(parent, points=get_cost(units.Vindicare), gear=[
            Gear('Blind Grenades'),
            Gear('Exitus Rifle'), Gear('Exitus Pistol')])


class EversorAssasin(ElitesUnit, armory.AssassinUnit):
    type_name = get_name(units.Eversor)
    type_id = "eversor_assasin_v1"
    keywords = ['Infantry', 'Character']
    power = 4

    def __init__(self, parent):
        super(EversorAssasin, self).__init__(parent, points=get_cost(units.Eversor),
                                             gear=[Gear('Melta Bombs'),
                                                   Gear('Neuro-gauntlet'),
                                                   Gear('Power Sword'),
                                                   Gear('Executioner Pistol')])


class CallidusAssasin(ElitesUnit, armory.AssassinUnit):
    type_name = get_name(units.Callidus)
    type_id = "callidus_assasin_v1"
    keywords = ['Infantry', 'Character']
    power = 5

    def __init__(self, parent):
        super(CallidusAssasin, self).__init__(parent, points=get_cost(units.Callidus),
                                              gear=[Gear('Neural Shredder'), Gear('Phase Sword'),
                                                    Gear('Poison Blades')])


class CulexusAssasin(ElitesUnit, armory.AssassinUnit):
    type_name = get_name(units.Culexus)
    type_id = "culexus_assasin_v1"
    keywords = ['Infantry', 'Character']
    power = 5

    def __init__(self, parent):
        super(CulexusAssasin, self).__init__(parent, points=get_cost(units.Culexus),
                                             gear=[Gear('Animus Speculum'),
                                                   Gear('Psyk-out Grenades')])


class Acolytes(ElitesUnit, armory.OrdoUnit):
    type_name = 'Acolytes'
    type_id = 'inq_acolyte_v1'

    keywords = ['Infantry']

    class Acolyte(ListSubUnit):
        type_name = 'Acolyte'

        def __init__(self, parent):
            super(Acolytes.Acolyte, self).__init__(parent=parent, points=8)
            self.Weapon1(self)
            self.Weapon2(self)

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Acolytes.Acolyte.Weapon1, self).__init__(parent, 'Pistol')
                self.variant('Laspistol')
                armory.add_inq_pistol_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Acolytes.Acolyte.Weapon2, self).__init__(parent, 'Weapon')
                self.variant('Chainsword')
                armory.add_inq_melee_weapons(self)
                armory.add_inq_ranged_weapons(self)

    def __init__(self, parent):
        super(Acolytes, self).__init__(parent)
        self.models = UnitList(self, self.Acolyte, 1, 6)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.get_count()


class Daemonhost(ElitesUnit, armory.InquisitionUnit):
    type_name = 'Daemonhost'
    type_id = 'dhost_v1'

    keywords = ['Daemon', 'Infantry']
    power = 1

    def __init__(self, parent):
        super(Daemonhost, self).__init__(parent, gear=[
            Gear('Unholy gaze'), Gear('Warp grasp')
        ], static=True, points=25)
    

class Jokaero(ElitesUnit, armory.OrdoUnit):
    type_name = 'Jokaero Waponsmith'
    type_id = 'monkey_v1'
    faction = ['Jokaero']
    keywords = ['Infantry']
    power = 2

    def __init__(self, parent):
        super(Jokaero, self).__init__(parent, gear=[
            Gear('Digital weapons')
        ], static=True, points=18)


class Prosecutors(ElitesUnit, armory.SSisterUnit):
    type_name = get_name(units.Prosecutors)
    type_id = 'prosecutors_v1'

    keywords = ['Infantry']
    power = 3

    model_cost = points_price(get_cost(units.Prosecutors), wargear.Boltgun)
    common_gear = [Gear('Boltgun'), Gear('Psyk-out grenades')]
    single_model = UnitDescription('Prosecutor').add(common_gear)

    def __init__(self, parent):
        super(Prosecutors, self).__init__(parent, points=self.model_cost,
                                          gear=UnitDescription('Sister Superior',
                                                               options=self.common_gear))
        self.models = Count(self, self.single_model.name, 4, 9, self.model_cost, True,
                            gear=self.single_model.clone())

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * (1 + (self.models.cur > 4))


class Vigilators(Prosecutors):
    type_name = get_name(units.Vigilators)
    type_id = 'vigilators_v1'

    power = 4

    model_cost = points_price(get_cost(units.Vigilators), wargear.Greatblade)
    common_gear = [Gear('Executioner greatblade'), Gear('Psyk-out grenades')]
    single_model = UnitDescription('Vigilator').add(common_gear)


class Witchseekers(Prosecutors):
    type_name = get_name(units.Witchseekers)
    type_id = 'witchseekers_v1'

    power = 5

    model_cost = points_price(get_cost(units.Witchseekers), wargear.Flamer)
    common_gear = [Gear('Flamer'), Gear('Psyk-out grenades')]
    single_model = UnitDescription('Witchseeker').add(common_gear)


class EspernLocarno(ElitesUnit, Unit):
    type_name = get_name(units.EspernLocarno)
    type_id = 'locarno_v1'
    keywords = ['Character', 'Infantry', 'Psyker', 'Navigator']
    faction = ['Imperium', 'Novis Nobilite']
    power = 2

    def __init__(self, parent):
        super(EspernLocarno, self).__init__(parent, points=get_cost(units.EspernLocarno),
                                            gear=[Gear('Laspistol'),
                                                  Gear('Force-orb cane')],
                                            static=True, unique=True)


class PiousVorne(ElitesUnit, Unit):
    type_name = get_name(units.PiousVorne)
    type_id = 'vorne_v1'
    keywords = ['Character', 'Infantry', 'Missionary Zealot']
    faction = ['Imperium', 'Adeptus ministorum']
    power = 2

    def __init__(self, parent):
        super(PiousVorne, self).__init__(parent, points=get_cost(units.PiousVorne),
                                         gear=[Gear('Vindicator')],
                                         static=True, unique=True)


class UR25(ElitesUnit, Unit):
    type_name = get_name(units.UR25)
    type_id = 'ur025_v1'
    keywords = ['Character', 'Infantry', 'Imperial Robot']
    faction = ['Imperium', 'Robotica Imperialis']
    power = 2

    def __init__(self, parent):
        super(UR25, self).__init__(parent, points=get_cost(units.UR25),
                                         gear=[Gear('Assault cannon'),
                                               Gear('Power claw')],
                                         static=True, unique=True)


class ReinRaus(ElitesUnit, Unit):
    type_name = get_name(units.ReinRaus)
    type_id = 'rein_raus_v1'
    kwname = 'Rein'
    keywords = ['Character', 'Infantry', 'Ratling', 'Raus']
    faction = ['Imperium', 'Astra Militarum', 'Militarum Auxilla']
    power = 2

    def __init__(self, parent):
        super(ReinRaus, self).__init__(
            parent, points=get_cost(units.ReinRaus),
            gear=[UnitDescription('Rein', options=[Gear('Sniper rifle'), Gear('Stub pistol')]),
                  UnitDescription('Raus', options=[Gear('Stub pistol'), Gear('Demolition charge')])],
            static=True, unique=True)

    def get_count(self):
        return 2
    
