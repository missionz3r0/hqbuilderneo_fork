__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit


class AssassinUnit(Unit):
    faction = ['Imperium', 'Officio Assassinorum']
    wiki_faction = "Officio Assassinorum"

def add_inq_melee_weapons(instance, inq=False):
    if inq:
        instance.variant('Nemesis Daemon hammer', 25)
        instance.variant('Null rod', 4)
    instance.variant('Power sword', 4)
    instance.variant('Power maul', 4)
    instance.variant('Power fist', 20)
    instance.variant('Thunder hammer', 25)

def add_inq_ranged_weapons(instance, inq=False):
    instance.variant('Boltgun')
    instance.variant('Combi-flamer', 11)
    instance.variant('Combi-melta', 19)
    instance.variant('Combi-plasma', 15)
    if inq:
        instance.variant('Condemnor boltgun', 1)
    instance.variant('Flamer', 9)
    instance.variant('Hot-shot lasgun', 4)
    if inq:
        instance.variant('Incinerator', 20)
    instance.variant('Meltagun', 17)
    instance.variant('Plasma gun', 13)
    instance.variant('Storm bolter', 2)

def add_inq_pistol_weapons(instance, inq=False):
    instance.variant('Bolt pistol')
    if inq:
        instance.variant('Inferno pistol', 12)
    instance.variant('Needle pistol', 2)
    instance.variant('Plasma pistol', 7)

def add_inq_force_weapons(instance):
    instance.variant('Force axe', 16)
    instance.variant('Force sword', 12)
    instance.variant('Force stave', 14)


class InquisitionUnit(Unit):
    wiki_faction = "Inquisition"
    faction = ['Imperium', 'Inquisition']


class OrdoUnit(InquisitionUnit):
    faction = ['<Ordo>']

    @classmethod
    def calc_faction(cls):
        return ['ORDO MALLEUS', 'ORDO HERETICUS', 'ORDO XENOS']


class CustodesUnit(Unit):
    faction = ['Imperium', 'Adeptus Custodes']
    wiki_faction = 'Adeptus Custodes'


class SSisterUnit(Unit):
    wiki_faction = "Sisters of silence"
    faction = ['Imperium', 'Astra Telepathica', 'Sisters of Silence']
