__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import LordsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList, ListSubUnit, UnitList


class KnightUnit(Unit):
    faction = ['Questor Imperialis', 'Imperium', '<Household>']
    keywords = ['Titanic', 'Vehicle']
    wiki_faction = 'Questor Imperialis'


class KnightErrant(LordsUnit, KnightUnit):
    type_name = 'Knight Errant'
    type_id = 'kn_errant_v1'

    power = 23

    class CaparaceWeapons(OptionsList):
        def __init__(self, parent):
            super(KnightErrant.CaparaceWeapons, self).__init__(parent, 'Caparace Weapon',
                                                               limit=1)
            self.variant('Ironstorm missile pod', 16)
            self.variant('Stormspear rocket pod', 45)
            self.variant('Twin Icarus autocannon', 30)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightErrant.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Reaper chainsword', 30)
            self.variant('Thunderstrike gauntlet', 35)

    class SecondaryWeapon(OneOf):
        def __init__(self, parent):
            super(KnightErrant.SecondaryWeapon, self).__init__(parent, 'Secondary weapon')
            self.variant('Heavy stubber', 4)
            self.variant('Meltagun', 17)

    def __init__(self, parent):
        super(KnightErrant, self).__init__(parent, points=320 + 76,
                                           gear=[Gear('Thermal cannon'),
                                                 Gear('Titanic feet')])
        self.Weapon1(self)
        self.SecondaryWeapon(self)
        self.CaparaceWeapons(self)


class KnightPaladin(LordsUnit, KnightUnit):
    type_name = 'Knight Paladin'
    type_id = 'kn_paladin_v1'

    power = 24

    def __init__(self, parent):
        super(KnightPaladin, self).__init__(parent, points=320 + 4 + 100,
                                            gear=[Gear('Heavy stubber'),
                                                  Gear('Rapid-fire battle cannon'),
                                                  Gear('Titanic feet')])
        KnightErrant.Weapon1(self)
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class KnightWarden(LordsUnit, KnightUnit):
    type_name = 'Knight Warden'
    type_id = 'kn_warden_v1'

    power = 25

    def __init__(self, parent):
        super(KnightWarden, self).__init__(parent, points=320 + 17 + 95,
                                           gear=[Gear('Heavy flamer'),
                                                 Gear('Avenger gatling cannon'),
                                                 Gear('Titanic feet')])
        KnightErrant.Weapon1(self)
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class KnightGallant(LordsUnit, KnightUnit):
    type_name = 'Knight Gallant'
    type_id = 'kn_gallant_v1'

    power = 21

    def __init__(self, parent):
        super(KnightGallant, self).__init__(parent, points=320 + 30 + 35,
                                            gear=[Gear('Reaper chainsword'),
                                                  Gear('Thunderstrike gauntlet'),
                                                  Gear('Titanic feet')])
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class KnightCrusader(LordsUnit, KnightUnit):
    type_name = 'Knight Crusader'
    type_id = 'kn_Crusader_v1'

    power = 27

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightCrusader.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Thermal cannon', 76)
            self.variant('Rapid-fire battle cannon and heavy stubber', 100 + 4,
                         gear=[Gear('Rapid-fire battle cannon'),
                               Gear('Heavy stubber')])

    def __init__(self, parent):
        super(KnightCrusader, self).__init__(parent, points=320 + 95 + 17,
                                             gear=[Gear('Heavy flamer'),
                                                   Gear('Avenger gatling cannon'),
                                                   Gear('Titanic feet')])
        self.Weapon1(self)
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class ArmigerWarglaives(Unit, LordsUnit):
    type_name = 'Armiger Warglaives'
    kwname = 'Armiger Warglaive'
    type_id = 'armigers_v1'
    power = 12

    faction = ['Imperium', '<Household>']
    keywords = ['Armiger', 'Vehicle']

    @classmethod
    def calc_faction(cls):
        return super(ArmigerWarglaives, cls).calc_faction() + ['QUESTOR IMPERIALIS',
                                                               'QUESTOR MECHANICUS']

    class Warglaive(ListSubUnit):
        type_name = 'Armiger Warglaive'

        class Caparace(OneOf):
            def __init__(self, parent):
                super(ArmigerWarglaives.Warglaive.Caparace, self).__init__(parent, 'Caparace weapon')
                self.variant('Heavy stubber', 4)
                self.variant('Meltagun', 17)

        def __init__(self, parent):
            super(ArmigerWarglaives.Warglaive, self).__init__(parent, points=223,
                                                              gear=[Gear('Thermal spear'), Gear('Reaper chain-cleaver')])
            self.Caparace(self)

    def __init__(self, parent):
        super(ArmigerWarglaives, self).__init__(parent)
        self.models = UnitList(self, self.Warglaive, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count
