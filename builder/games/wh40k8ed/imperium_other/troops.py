__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import Gear, UnitDescription, OneOf,\
    ListSubUnit, UnitList, SubUnit, OptionsList
from . import armory


class CustodianSquad(TroopsUnit, armory.CustodesUnit):
    type_name = 'Custodian Guard Squad'
    type_id = 'custodians_v1'
    keywords = ['Infantry']
    obsolete = True

    class Custodian(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(CustodianSquad.Custodian.Weapon, self).__init__(parent, 'Weapon')
                self.spear = self.variant('Guardian spear', 12)
                self.variant('Sentinel blade', 9)
                self.vexilla = self.variant('Custodes Vexilla', 25 + 5, gear=[
                    Gear('Custodes Vexilla'), Gear('Power knife')
                ])

        class Shield(OptionsList):
            def __init__(self, parent, knife=False):
                super(CustodianSquad.Custodian.Shield, self).__init__(parent, '', limit=1)
                self.variant('Storm shield', 5)
                if knife:
                    self.variant('Power knife', 5)

        def __init__(self, parent):
            super(CustodianSquad.Custodian, self).__init__(parent, 'Custodian', 40)
            self.wep = self.Weapon(self)
            self.shield = self.Shield(self)

        def check_rules(self):
            super(CustodianSquad.Custodian, self).check_rules()
            self.shield.visible = self.shield.used = self.wep.cur != self.wep.spear

        @ListSubUnit.count_gear
        def get_standard(self):
            return self.wep.description if self.wep.cur == self.wep.vexilla else []

    class ShieldCaptain(Unit):
        def __init__(self, parent):
            super(CustodianSquad.ShieldCaptain, self).__init__(parent, 'Shield-Captain', 40)
            self.wep = CustodianSquad.Custodian.Weapon(self)
            self.wep.vexilla.visible = False
            self.shield = CustodianSquad.Custodian.Shield(self, True)

        def check_rules(self):
            super(CustodianSquad.ShieldCaptain, self).check_rules()
            self.shield.visible = self.shield.used = self.wep.cur != self.wep.spear

    def __init__(self, parent):
        super(CustodianSquad, self).__init__(parent)
        SubUnit(self, self.ShieldCaptain(parent=self))
        self.models = UnitList(self, self.Custodian, 4, 9)

    def get_count(self):
        return 1 + self.models.count

    def count_vexilla(self):
        return sum((c.get_standard() for c in self.models.units))

    def check_rules(self):
        super(CustodianSquad, self).check_rules()
        if self.count_vexilla() > 1:
            self.error('No more then one Custodes Vexilla can be taken per detachment')

    def build_power(self):
        return 14 + 13 * (self.models.count > 4)
