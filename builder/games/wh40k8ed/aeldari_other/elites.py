__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, ElitesUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList,\
    Gear, UnitDescription, Count
from . import armory, melee, ranged, units
from builder.games.wh40k8ed.utils import *


class DeathJester(ElitesUnit, armory.HarlequinUnit):
    type_name = get_name(units.DeathJester)
    type_id = 'd_jester_v1'

    keywords = ['Character', 'Infantry']

    power = 4

    def __init__(self, parent):
        super(DeathJester, self).__init__(parent, points=get_cost(units.DeathJester), static=True,
                                          gear=[Gear('Shrieker cannon')])


class Solitaire(ElitesUnit, armory.HarlequinUnit):
    type_name = get_name(units.Solitaire)
    type_id = 'solitaire_v1'

    keywords = ['Character', 'Infantry']

    power = 6

    def __init__(self, parent):
        gear = [melee.Caress, melee.Kiss]
        cost = points_price(get_cost(units.Solitaire), *gear)
        super(Solitaire, self).__init__(parent, points=cost, static=True, unique=True,
                                        gear=create_gears(*gear))
