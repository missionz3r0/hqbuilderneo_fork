__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, HeavyUnit
from builder.games.wh40k8ed.options import Gear, Count, UnitDescription
from . import armory, melee, ranged, units
from builder.games.wh40k8ed.utils import *


class Voidweavers(HeavyUnit, armory.HarlequinUnit):
    type_name = get_name(units.Voidweavers)
    type_id = 'voidweavers_v1'

    keywords = ['Vehicle', 'Fly']
    member = UnitDescription('Voidweaver', options=[Gear('Shuriken cannon', count=2)])

    class Weaver(Count):
        @property
        def description(self):
            return Voidweavers.member.clone().add(Gear('Haywire cannon'))\
                                             .set_count(self.cur - self.parent.prism.cur)

    def __init__(self, parent):
        super(Voidweavers, self).__init__(parent, static=True)
        cost = points_price(get_cost(units.Voidweavers), ranged.ShurikenCannon,
                            ranged.ShurikenCannon, ranged.HaywireCannon)
        self.members = self.Weaver(self, 'Voidweavers', 1, 3, points=cost, per_model=True)
        self.prism = Count(self, 'Prismatic cannon', 0, 1, points=get_cost(ranged.PrismaticCannon) - get_cost(ranged.HaywireCannon),
                           gear=Voidweavers.member.clone().add(Gear('Prismatic cannon')))

    def check_rules(self):
        super(Voidweavers, self).check_rules()
        self.prism.max = self.members.cur

    def get_count(self):
        return self.members.cur

    def build_power(self):
        return 6 * self.members.cur
