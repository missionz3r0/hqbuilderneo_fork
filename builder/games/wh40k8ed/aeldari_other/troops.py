__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, TroopsUnit
from builder.games.wh40k8ed.options import OneOf, ListSubUnit,\
    Gear, UnitDescription, UnitList
from . import armory, melee, ranged, units
from builder.games.wh40k8ed.utils import *


class Troupe(TroopsUnit, armory.HarlequinUnit):
    type_name = get_name(units.Troupe)
    type_id = 'troupe_v1'

    keywords = ['Infantry']

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Troupe.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.ShurikenPistol)
            self.variant(*ranged.NeuroDisruptor)
            self.variant(*ranged.FusionPistol)

    class Melee(OneOf):
        def __init__(self, parent):
            super(Troupe.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Blade)
            self.variant(*melee.Embrace)
            self.variant(*melee.Kiss)
            self.variant(*melee.Caress)

    class Player(ListSubUnit):
        def __init__(self, parent):
            super(Troupe.Player, self).__init__(parent, 'Player', points=get_cost(units.Troupe),
                                                gear=[Gear('Prismatic grenades')])
            Troupe.Melee(self)
            Troupe.Ranged(self)

    def __init__(self, parent):
        super(Troupe, self).__init__(parent, 'Troupe')
        self.troupe = UnitList(self, self.Player, 5, 12)

    def get_count(self):
        return self.troupe.count

    def build_power(self):
        return 2 + self.get_count()
