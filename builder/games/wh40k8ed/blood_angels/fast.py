from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList, OptionalSubUnit
from . import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class BAAssaultSquad(FastUnit, armory.BAUnit):
    type_name = 'Blood Angels Assault Squad'
    type_id = 'ba_assault_squad_v1'
    keywords = ['Infantry']

    model_points = get_cost(units.AssaultSquad)
    model_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(armory.ClawUser):
        type_name = 'Space Marine Sergeant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(BAAssaultSquad.Sergeant.Weapon1, self).__init__(parent, 'Weapon')
                armory.add_pistol_options(self)
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(BAAssaultSquad.Sergeant.Weapon2, self).__init__(parent, '')
                armory.add_melee_weapons(self)
                self.evs = self.variant(*melee.Eviscerator)

        class Options(OptionsList):
            def __init__(self, parent):
                super(BAAssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant(*ranged.MeltaBombs)
                self.combatshiled = self.variant(*wargear.CombatShield)

        def __init__(self, parent):
            super(BAAssaultSquad.Sergeant, self).__init__(
                parent=parent, points=BAAssaultSquad.model_points,
                gear=BAAssaultSquad.model_gear
            )
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(BAAssaultSquad.Sergeant, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.has_evs()

        def has_evs(self):
            return self.wep2.cur == self.wep2.evs

    class Jumppack(OptionsList):
        def __init__(self, parent):
            super(BAAssaultSquad.Jumppack, self).__init__(parent=parent, name='Movement options')
            self.jumppack = self.variant(
                'Jump Packs',
                get_cost(units.JumpAssaultSquad) - get_cost(units.AssaultSquad),
                per_model=True)

    # def build_points(self):
    #     return super(AssaultSquad, self).build_points() + self.pack.points * self.marines.cur * self.pack.used

    def __init__(self, parent):
        super(BAAssaultSquad, self).__init__(parent=parent,
                                                name=get_name(units.AssaultSquad))
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.marines = Count(parent=self, name='Space Marine',
                             min_limit=4, max_limit=9,
                             points=self.model_points,
                             per_model=True)
        self.hf = Count(self, 'Hand flamer', 0, 2,
                        get_cost(ranged.HandFlamer))
        self.ip = Count(self, 'Inferno pistol', 0, 2,
                        get_cost(ranged.InfernoPistol))
        self.pp = Count(self, 'Plasma pistol', 0, 2,
                        get_cost(ranged.PlasmaPistol))
        self.flame = Count(self, 'Flamer', 0, 2,
                           get_cost(ranged.Flamer))
        self.melta = Count(self, 'Meltagun', 0, 2,
                           get_cost(ranged.Meltagun))
        self.plasma = Count(self, 'Plasma gun', 0, 2,
                            get_cost(ranged.PlasmaGun))
        self.evs = Count(self, 'Eviscerator', 0, 2,
                         get_cost(melee.Eviscerator))
        self.pack = self.Jumppack(parent=self)

    def check_rules(self):
        super(BAAssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.pp, self.hf,
                                 self.ip, self.melta, self.plasma])
        evs_max = (self.get_count() / 5) - self.sergeant.unit.has_evs()
        self.evs.used = self.evs.visible = evs_max > 0
        self.evs.max = evs_max

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points,
                               count=self.get_count(), options=self.pack.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=BAAssaultSquad.model_gear
        )
        if self.pack.any and self.pack.used:
            marine.add(Gear('Jump Pack'))
            marine.add_points(self.pack.points)
        if self.evs.cur:
            desc.add(marine.clone().
                     add(Gear('Eviscerator')).
                     set_count(self.evs.cur))
        count = self.marines.cur - self.evs.cur
        for cnt in [self.flame, self.melta, self.plasma]:
            if cnt.cur:
                desc.add_dup(marine.clone().add(cnt.gear).set_count(cnt.cur))
                count -= cnt.cur
        for cnt in [self.hf, self.ip, self.pp]:
            if cnt.cur:
                desc.add_dup(marine.clone().add(Gear('Chainsword')).add(cnt.gear).set_count(cnt.cur))
                count -= cnt.cur
        desc.add(marine.add([Gear('Chainsword'), Gear('Bolt pistol')]).set_count(count))
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return 1 + (4 + (self.pack.any)) * (1 + self.marines.cur > 4)

    def build_points(self):
        return super(BAAssaultSquad, self).build_points() + self.pack.points * self.marines.cur


class BABikeSquad(FastUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.BikeSquad)
    type_id = 'ba_bike_squad_v1'
    power = 5
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.TwinBoltgun]
    model_points = points_price(get_cost(units.BikeSquad), *model_gear)

    class Sergeant(Unit):
        type_name = 'Biker Sergeant'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BABikeSquad.Sergeant.Weapon, self).__init__(parent, 'Weapon')
                armory.add_pistol_options(self)          
                armory.add_melee_weapons(self)
        
        def __init__(self, parent):
            super(BABikeSquad.Sergeant, self).__init__(
                parent=parent, points=parent.model_points,
                gear=armory.create_gears(*BABikeSquad.model_gear)
            )
            self.wep = self.Weapon(self)

    class AttackBike(Unit):
        type_name = 'Attack Bike'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BABikeSquad.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant(*ranged.HeavyBolter)
                self.multimelta = self.variant(*ranged.MultiMelta)

        def __init__(self, parent):
            attack_points = points_price(get_cost(units.AttackBike), ranged.TwinBoltgun)
            super(BABikeSquad.AttackBike, self).__init__(
                parent=parent, points=attack_points,
                gear=[Gear('Twin boltgun'), UnitDescription('Space Marine', count=2,
                                                            options=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                                     Gear('Bolt pistol')])])
            self.wep = self.Weapon(self)

    class OptUnits(OptionalSubUnit):
        def __init__(self, parent):
            super(BABikeSquad.OptUnits, self).__init__(parent=parent, name='')
            self.attackbike = SubUnit(self, BABikeSquad.AttackBike(parent=parent))

    class Biker(Count):
        @property
        def description(self):
            return[UnitDescription('Space Marine Biker',
                                   options=create_gears(*BABikeSquad.model_gear) + [Gear('Bolt pistol')])\
                   .set_count(self.cur - self.parent.sword.cur - sum(c.cur for c in self.parent.spec))]

    def variant(self, name, points):
        return Count(self, name, 0, 2, points,
                     gear=UnitDescription('Space Marine Biker', options=armory.create_gears(*BABikeSquad.model_gear) + [Gear(name)]))

    def __init__(self, parent):
        super(BABikeSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.opt_units = self.OptUnits(self)

        self.bikers = self.Biker(self, 'Space Marine Bikers', 2, 7, self.model_points, per_model=True)
        self.sword = Count(self, 'Chainsword', 0, 2, gear=UnitDescription('Space Marine Biker',
                                                                          options=armory.create_gears(*BABikeSquad.model_gear) + [Gear('Chainsword')]))
        self.spec = []
        armory.add_special_weapons(self)

    def check_rules(self):
        super(BABikeSquad, self).check_rules()
        Count.norm_counts(0, 2, self.spec)
        self.sword.max = self.bikers.cur - sum(c.cur for c in self.spec)

    def get_count(self):
        return self.opt_units.count + self.bikers.cur + 1

    def build_power(self):
        return 3 * self.opt_units.count + self.power +\
            (6 if self.bikers.cur > 5 else
             (4 if self.bikers.cur > 3 else 0))


class BAScoutBikers(FastUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.ScoutBikeSquad)
    type_id = 'ba_scout_bike_squad_v1'
    keywords = ['Biker', 'Scout']

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.AstartesShotgun, melee.CombatKnife]
    model_points = armory.get_cost(units.ScoutBikeSquad)

    power = 4

    class Sergeant(Unit):
        type_name = 'Scout Biker Sergeant'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BAScoutBikers.Sergeant.Weapon, self).__init__(parent, 'Weapon')
                armory.add_pistol_options(self)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            gear = BAScoutBikers.model_gear + [ranged.TwinBoltgun]
            super(BAScoutBikers.Sergeant, self).__init__(
                parent=parent, points=armory.points_price(BAScoutBikers.model_points, *gear),
                gear=armory.create_gears(*gear))
            self.wep = self.Weapon(self)

    class Bikers(Count):
        @property
        def description(self):
            return [UnitDescription('Scout Biker', options=armory.create_gears(*BAScoutBikers.model_gear) + [
                Gear('Bolt pistol'), Gear('Twin boltgun')
            ]).set_count(self.cur - self.parent.grenade.cur)]

    def __init__(self, parent):
        super(BAScoutBikers, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))

        self.bikers = self.Bikers(self, 'Scout Biker', 2, 9, self.model_points, per_model=True)
        self.grenade = Count(self, 'Astartes grenade launcher', 0, 3, armory.get_cost(ranged.AstartesGrenadeLauncher),
                             gear=UnitDescription('Scout Biker', options=armory.create_gears(BAScoutBikers.model_gear) + [
                                 Gear('Bolt pistol'), Gear('Astartes grenade launcher')
                             ]))

    def get_count(self):
        return self.bikers.cur + 1

    def check_rules(self):
        super(BAScoutBikers, self).check_rules()
        self.grenade.max = min(self.bikers.cur, 3)

    def build_power(self):
        return self.power * ((self.bikers.cur + 3) / 3)

    def build_points(self):
        # take twin boltgun costs into account
        return super(BAScoutBikers, self).build_points() + get_cost(ranged.TwinBoltgun) * (self.bikers.cur - self.grenade.cur)


class BAAttackBikes(FastUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.AttackBikeSquad)
    type_id = 'ba_attack_bike_squad_v1'
    power = 3
    keywords = ['Biker']

    class ListBike(BABikeSquad.AttackBike, ListSubUnit):
        pass

    def __init__(self, parent):
        super(BAAttackBikes, self).__init__(parent, name=get_name(units.AttackBikeSquad))
        self.models = UnitList(self, self.ListBike, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.get_count()


class BASuppressors(FastUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.SuppressorSquad)
    type_id = 'ba_supressors_v1'

    keywords = ['Infantry', 'Primaris', 'Jump pack', 'Fly']

    power = 5

    def __init__(self, parent):
        gear = [ranged.AcceleratorAutocannon, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.SuppressorSquad), *gear)
        super(BASuppressors, self).__init__(parent, name=get_name(units.SuppressorSquad),
                                           points=3 * cost,
                                           gear=[UnitDescription('Suppressor Sergeant', options=create_gears(*gear)),
                                                 UnitDescription('Suppressor', options=create_gears(*gear), count=2)],
                                           static=True)

    def get_count(self):
        return 3
