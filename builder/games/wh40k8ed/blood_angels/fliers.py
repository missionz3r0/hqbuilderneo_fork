from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
from builder.games.wh40k8ed.utils import *
from . import armory, units, ranged


class BAStormhawk(FlierUnit, armory.BAUnit):
    type_name = 'Blood Anghels ' + armory.get_name(units.StormhawkInterceptor)
    type_id = 'ba_stormhawk_v1'

    keywords = ['Vehicle', 'Fly']
    power = 10

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BAStormhawk.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.IcarusStormcannon)
            self.variant(*ranged.LasTalon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(BAStormhawk.Weapon2, self).__init__(parent, '')
            self.variant('Two heavy bolters', armory.get_cost(ranged.HeavyBolter) * 2,
                         gear=armory.create_gears(ranged.HeavyBolter, ranged.HeavyBolter))
            self.variant(*ranged.SkyhammerMissileLauncher)
            self.variant(*ranged.TyphoonMissileLauncher)

    def __init__(self, parent):
        gear = [ranged.AssaultCannon, ranged.AssaultCannon]
        cost = armory.points_price(armory.get_cost(units.StormhawkInterceptor), *gear)
        super(BAStormhawk, self).__init__(parent,
                                          name=get_name(units.StormhawkInterceptor),
                                          gear=armory.create_gears(*gear),
                                          points=cost)
        self.Weapon1(self)
        self.Weapon2(self)


class BAStormtalon(FlierUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + armory.get_name(units.StormtalonGunship)
    type_id = 'ba_stormtalongunship_v1'

    keywords = ['Vehicle', 'Fly']
    power = 9

    def __init__(self, parent):
        gear = [ranged.TwinAssaultCannon]
        super(BAStormtalon, self).__init__(parent=parent,
                                           name=get_name(units.StormtalonGunship),
                                           points=armory.points_price(armory.get_cost(units.StormtalonGunship), *gear),
                                           gear=armory.create_gears(*gear))
        self.weapon = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(BAStormtalon.Weapon, self).__init__(parent=parent, name='Weapon')

            self.twinlinkedheavybolter = self.variant('Two heavy bolters', 2 * armory.get_cost(ranged.HeavyBolter),
                                                      gear=[Gear('Heavy bolter', count=2)])
            self.twinlinkedlascannon = self.variant('Two lascannons', 2 * armory.get_cost(ranged.Lascannon),
                                                    gear=[Gear('Lascannon', count=2)])
            self.skyhammermissilelauncher = self.variant(*ranged.SkyhammerMissileLauncher)
            self.typhoonmissilelauncher = self.variant(*ranged.TyphoonMissileLauncher)
