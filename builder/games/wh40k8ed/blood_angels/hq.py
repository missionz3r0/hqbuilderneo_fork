from builder.games.wh40k8ed.space_marines.hq import Captain, BikeCaptain, Chaplain,\
    BikeChaplain, Librarian, BikeLibrarian, BikeTechmarine
from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, UnitList, ListSubUnit
from . import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class BABikeCaptain(armory.BAUnit, BikeCaptain):
    type_name = 'Blood Angels Captain on Bike'
    type_id = 'ba_bike_captain_v1'

    armory = armory


class BACaptain(HqUnit, armory.ClawUser, armory.BAUnit):
    type_name = 'Blood Angels Captain'
    type_id = 'ba_captain_v1'

    keywords = ['Character', 'Infantry']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BACaptain.Weapon1, self).__init__(parent, 'Weapon 1')
            self.variant(*ranged.MCBoltgun)
            armory.add_pistol_options(self)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(BACaptain.Weapon2, self).__init__(parent, 'Weapon 2')
            armory.add_melee_weapons(self)
            self.variant(*wargear.StormShieldCharacter)
            self.variant(*melee.RelicBlade)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(BACaptain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack',
                                     armory.get_cost(units.JumpCaptain) - armory.get_cost(units.Captain))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        cost = armory.points_price(armory.get_cost(units.Captain), *gear)
        super(BACaptain, self).__init__(parent, name=armory.get_name(units.Captain), points=cost,
                                           gear=armory.create_gears(*gear))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class BATerminatorCaptain(HqUnit, armory.ClawUser, armory.BAUnit):
    type_name = 'Blood Angels ' + armory.get_name(units.TerminatorCaptain)
    type_id = 'ba_termo_captain_v1'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 7

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BATerminatorCaptain.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.StormBolter)
            armory.add_term_combi_weapons(self)
            armory.add_term_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(BATerminatorCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.RelicBlade)
            self.variant(*melee.PowerSword)
            self.variant(*melee.Chainfist)
            armory.add_term_melee_weapons(self)

    class Launcher(OptionsList):
        def __init__(self, parent):
            super(BATerminatorCaptain.Launcher, self).__init__(parent, 'Launcher')
            self.variant(*ranged.WristLauncher)

    def __init__(self, parent):
        super(BATerminatorCaptain, self).__init__(parent, armory.get_name(units.TerminatorCaptain),
                                                     points=armory.get_cost(units.TerminatorCaptain))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.launcher = self.Launcher(self)

    def check_rules(self):
        super(BATerminatorCaptain, self).check_rules()
        fist = self.wep1.fist == self.wep1.cur or self.wep2.fist == self.wep2.cur
        self.launcher.visible = self.launcher.used = fist


class BACataphractiiCaptain(HqUnit, armory.ClawUser, armory.BAUnit):
    type_name = 'Blood Angels ' + armory.get_name(units.CataphractiiCaptain)
    type_id = 'ba_cataphractii_captain_v1'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 8

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BACataphractiiCaptain.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.CombiBolter)
            armory.add_term_combi_weapons(self)
            armory.add_term_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(BACataphractiiCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.Chainfist)
            self.variant(*melee.PowerSword)
            self.variant(*melee.RelicBlade)
            armory.add_term_melee_weapons(self)

    def __init__(self, parent):
        super(BACataphractiiCaptain, self).__init__(parent, armory.get_name(units.CataphractiiCaptain),
                                                       points=armory.get_cost(units.CataphractiiCaptain))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class BAPrimarisCaptain(HqUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + armory.get_name(units.PrimarisCaptain)
    type_id = 'ba_primaris_captain_v1'
    kwname = 'Captain'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 6

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(BAPrimarisCaptain.RangedWeapon, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.MCAutoBoltRifle)
            self.variant(*ranged.MCStalkerBoltRifle)

    class Pistol(OneOf):
        def __init__(self, parent):
            super(BAPrimarisCaptain.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*ranged.BoltPistol)
            self.pp = self.variant(join_and(ranged.PlasmaPistol, melee.PowerFist),
                                   get_costs(ranged.PlasmaPistol, melee.PowerFist),
                                   gear=create_gears((ranged.PlasmaPistol, melee.PowerFist)))
    
    class Melee(OptionsList):
        def __init__(self, parent):
            super(BAPrimarisCaptain.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerSword)

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        super(BAPrimarisCaptain, self).__init__(parent,
                                                points=armory.points_price(armory.get_cost(units.PrimarisCaptain), *gear),
                                                gear=armory.create_gears(*gear))
        self.rng = self.RangedWeapon(self)
        self.pist = self.Pistol(self)
        self.sw = self.Melee(self)

    def check_rules(self):
        super(BAPrimarisCaptain, self).check_rules()
        self.rng.used = self.rng.visible = self.sw.used = self.sw.visible = self.pist.cur != self.pist.pp


class BAChaplainV1(armory.BAUnit, Chaplain):
    type_name = 'Blood Angels Chaplain (Index)'
    type_id = 'ba_chaplain_v1'

    armory = armory


class BAChaplainV2(HqUnit, armory.BAUnit):
    type_name = armory.get_name(units.Chaplain) + ' (Codex)'
    type_id = 'ba_chaplain_v2'

    keywords = ['Character', 'Infantry']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BAChaplainV2.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_pistol_options(self)

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(BAChaplainV2.Weapon2, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerFist)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(BAChaplainV2.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', armory.get_cost(units.JumpChaplain) -
                                     armory.get_cost(units.Chaplain))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.CroziusArcanum]
        cost = points_price(armory.get_cost(units.Chaplain), *gear)
        super(BAChaplainV2, self).__init__(parent, armory.get_name(units.Chaplain),
                                           points=cost, gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class BABikeChaplain(armory.BAUnit, BikeChaplain):
    type_name = 'Blood Angels Chaplain on Bike'
    type_id = 'ba_bike_chaplain_v1'
    armory = armory
    relative = BAChaplainV1


class BALibrarian(armory.BAUnit, Librarian):
    type_name = 'Blood Angels Librarian'
    type_id = 'ba_librarian_v1'

    armory = armory


class BABikeLibrarian(armory.BAUnit, BikeLibrarian):
    type_name = 'Blood Angels Librarian on Bike'
    type_id = 'ba_bike_librarian_v1'
    armory = armory
    relative = BALibrarian


class BATechmarine(HqUnit, armory.BAUnit):
    type_name = get_name(units.IndexTechmarine) + ' (Index)'
    type_id = 'ba_techmarine_v1'

    keywords = ['Character', 'Infantry']
    power = 5

    armory = armory

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(BATechmarine.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.PowerAxe)
            self.parent.armory.add_melee_weapons(self, axe=False)

    class Arm(OneOf):
        def __init__(self, parent):
            super(BATechmarine.Arm, self).__init__(parent, 'Various')
            self.variant(*melee.ServoArm)
            self.pack = self.variant('Jump pack', get_cost(units.JumpTechmarine) -
                                     get_cost(units.IndexTechmarine))
            self.beamer = self.variant(*ranged.ConversionBeamer)

    class Harness(OptionsList):
        def __init__(self, parent):
            super(BATechmarine.Harness, self).__init__(parent, 'Harness')
            gear = [melee.ServoArm, ranged.Meltagun, ranged.Flamer]
            self.harness = self.variant('Servo-harness', get_costs(*gear),
                                        gear=create_gears(*gear))

    def __init__(self, parent):
        super(BATechmarine, self).__init__(parent, get_name(units.IndexTechmarine),
                                           points=get_cost(units.IndexTechmarine), gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ])
        BALibrarian.Weapon1(self)
        self.Weapon2(self)
        self.arm = self.Arm(self)
        self.harness = self.Harness(self)

    def build_power(self):
        return self.power + 1 * (self.arm.cur == self.arm.pack)

    def check_rules(self):
        super(BATechmarine, self).check_rules()
        self.arm.beamer.visible = not (self.harness.used and self.harness.any)


class BATechmarineV2(HqUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + armory.get_name(units.Techmarine) + ' (Codex)'
    type_id = 'ba_techmarine_v2'

    keywords = ['Character', 'Infantry']
    power = 4

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BATechmarineV2.Weapon1, self).__init__(parent, 'Pistol')
            armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)
            armory.add_combi_weapons(self)

    class Harness(OptionsList):
        def __init__(self, parent):
            super(BATechmarineV2.Harness, self).__init__(parent, 'Harness')
            gear = [melee.ServoArm, ranged.Flamer, ranged.PlasmaCutter]
            self.harness = self.variant('Servo-harness', armory.get_costs(*gear),
                                        gear=armory.create_gears(*gear))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.PowerAxe, melee.ServoArm]
        cost = armory.points_price(armory.get_cost(units.Techmarine), *gear)
        super(BATechmarineV2, self).__init__(parent, name=armory.get_name(units.Techmarine),
                                             points=cost,
                                             gear=armory.create_gears(*gear))
        self.Weapon1(self)
        self.harness = self.Harness(self)


class BABikeTechmarine(armory.BAUnit, BikeTechmarine):
    type_name = 'Blood Angels Techmarine on Bike'
    type_id = 'ba_bike_techmarine_v1'

    relative = BALibrarian
    armory = armory


class Dante(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.CommanderDante)
    type_id = 'dante_v1'
    faction = ['Blood Angels']
    keywords = ['Character', 'Infantry', 'Chapter master', 'Jump pack', 'Fly']
    power = 11

    def __init__(self, parent):
        super(Dante, self).__init__(parent, points=get_cost(units.CommanderDante), gear=[
            Gear('The Axe Mortalis'),
            Gear('Inferno pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades')], static=True, unique=True)


class Tycho(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.CaptainTycho)
    type_id = 'tycho_v1'
    faction = ['Blood Angels']
    kwname = 'Tycho'
    keywords = ['Character', 'Infantry', 'Captain']
    power = 5

    def __init__(self, parent):
        super(Tycho, self).__init__(parent, gear=[
            Gear('Blood song'), Gear('Bolt pistol'),
            Gear("Dead Man's hand"),
            Gear('Frag grenades'), Gear('Krak grenades')],
                                    static=True, points=get_cost(units.CaptainTycho))

    def get_unique(self):
        return 'TYCHO'


class DCTycho(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.TychoTheLost)
    type_id = 'dc_tycho_v1'
    faction = ['Blood Angels', 'Death Company']
    kwname = 'Tycho'
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        super(Tycho, self).__init__(parent, gear=[
            Gear('Blood song'), Gear('Bolt pistol'),
            Gear("Dead Man's hand"),
            Gear('Frag grenades'), Gear('Krak grenades')],
                                    static=True, points=get_cost(units.TychoTheLost))

    def get_unique(self):
        return 'TYCHO'


class LibrarianDreadnought(HqUnit, armory.BAUnit):
    type_name = get_name(units.LibrarianDreadnought)
    type_id = 'lib_dread_v1'
    keywords = ['Character', 'Vehicle', 'Dreadnought', 'Librarian', 'Psyker']
    power = 9

    class BuiltInWeapon(OneOf):
        def __init__(self, parent):
            super(LibrarianDreadnought.BuiltInWeapon, self).__init__(
                parent, 'Built-in weapon')
            self.variant(*ranged.StormBolter)
            self.variant(*ranged.HeavyFlamer)
            self.variant(*ranged.Meltagun)

    def __init__(self, parent):
        gear = [melee.FuriosoFist, melee.FuriosoForceHalberd]
        cost = points_price(get_cost(units.LibrarianDreadnought), *gear)
        super(LibrarianDreadnought, self).__init__(
            parent, gear=create_gears(*gear), points=cost)
        self.BuiltInWeapon(self)


class Mephiston(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.Mephiston)
    type_id = 'mephiston_v1'
    keywords = ['Character', 'Infantry', 'Librarian', 'Psyker']
    faction = ['Blood Angels']
    power = 8

    def __init__(self, parent):
        super(Mephiston, self).__init__(parent, points=get_cost(units.Mephiston), gear=[
            Gear('Plasma pistol'),
            Gear('Sanguine sword'), Gear('Frag grenades'),
            Gear('Krak grenades')], static=True, unique=True)


class Sanguinor(HqUnit, armory.AstartesUnit):
    type_name = (get_name(units.Sanguinor))
    type_id = 'sanguinor_v1'
    keywords = ['Character', 'Infantry', 'Jump pack', 'Fly']
    faction = ['Blood Angels']
    power = 9

    def __init__(self, parent):
        super(Sanguinor, self).__init__(parent, points=get_cost(units.Sanguinor), gear=[
            Gear('Encarmine broadsword'),
            Gear('Frag grenades'),
            Gear('Krak grenades')], static=True, unique=True)


class Astorath(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.Astorath)
    type_id = 'astorath_v1'
    keywords = ['Character', 'Infantry', 'Chaplain', 'Jump pack']
    faction = ['Blood Angels']
    power = 8

    def __init__(self, parent):
        super(Astorath, self).__init__(parent, points=get_cost(units.Astorath), gear=[
            Gear('The Executioner\'s Axe'), Gear('Bolt Pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades')], static=True, unique=True)


class SanguinaryPriest(HqUnit, armory.BAUnit):
    type_name = get_name(units.SanguinaryPriest) + ' (Index)'
    type_id = 'sangpriest_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    class Ranged(OneOf):
        def __init__(self, parent):
            super(SanguinaryPriest.Ranged, self).__init__(parent, 'Ranged weapon')
            armory.add_pistol_options(self)
            self.variant('Boltgun')
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Melee(OneOf):
        def __init__(self, parent):
            super(SanguinaryPriest.Melee, self).__init__(parent, 'Melee weapons')
            armory.add_melee_weapons(self)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(SanguinaryPriest.Pack, self).__init__(parent, 'Options')
            self.variant('Jump pack', get_cost(units.JumpSanguinaryPriest) - get_cost(units.SanguinaryPriest))

    def __init__(self, parent):
        super(SanguinaryPriest, self).__init__(
            parent, gear=[
                Gear('Frag grenades'), Gear('Krak grenades')
            ], points=get_cost(units.SanguinaryPriest),
            name=get_name(units.SanguinaryPriest))
        self.Ranged(self)
        self.Melee(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + self.pack.any


class SanguinaryPriestV2(HqUnit, armory.BAUnit):
    type_name = get_name(units.SanguinaryPriest)
    type_id = 'sangpriest_v2'
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        super(SanguinaryPriestV2, self).__init__(parent, gear=[
            Gear('Bolt pistol'), Gear('Chainsword'),
            Gear('Frag grenades'), Gear('Krak grenades')
        ], points=get_cost(units.SanguinaryPriest), static=True)


class BikeSanguinaryPriest(HqUnit, armory.BAUnit):
    type_name = get_name(units.BikeSanguinaryPriest)
    type_id = 'bike_sangpriest_v1'
    keywords = ['Character', 'Biker']
    power = 6

    def __init__(self, parent):
        gear = [ranged.TwinBoltgun]
        super(BikeSanguinaryPriest, self).__init__(parent, gear=[
            Gear('Frag grenades'), Gear('Krak grenades') 
        ] + create_gears(*gear), points=points_price(get_cost(units.BikeSanguinaryPriest), *gear))
        SanguinaryPriest.Ranged(self)
        SanguinaryPriest.Melee(self)


class Corbulo(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.BrotherCorbulo)
    type_id = 'corbulo_v1'
    faction = ['Blood Angels']
    keywords = ['Character', 'Infantry', 'Sanguinary priest']
    power = 5

    def __init__(self, parent):
        super(Corbulo, self).__init__(parent, points=get_cost(units.BrotherCorbulo),
                                      gear=[Gear('Heaven\'s Teeth'), Gear('Bolt pistol'),
                                            Gear('Frag grenades'), Gear('Krak grenades')],
                                      unique=True, static=True)


class Lemartes(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.Lemartes)
    type_id = 'lemartes_v1'
    faction = ['Blood Angels', 'Death Company']
    keywords = ['Character', 'Infantry', 'Chaplain', 'Jump pack', 'Fly']
    power = 7

    def __init__(self, parent):
        super(Lemartes, self).__init__(parent, points=get_cost(units.Lemartes),
                                       gear=[Gear('Blood Crozius'), Gear('Bolt pistol'),
                                             Gear('Frag grenades'), Gear('Krak grenades')],
                                       unique=True, static=True)


class Seth(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.GabrielSeth)
    type_id = 'seth_v1'
    faction = ['Flesh Tearers']
    keywords = ['Character', 'Infantry', 'Chapter master']
    power = 7

    def __init__(self, parent):
        super(Seth, self).__init__(parent, points=get_name(units.GabrielSeth), gear=[
            Gear('Blood Reaver'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades')], static=True, unique=True)


class BALieutenants(HqUnit, armory.BAUnit):
    type_name = armory.get_name(units.Lieutenants)
    type_id = 'ba_leutanents_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    class Leutenant(armory.ClawUser, ListSubUnit):
        type_name = 'Leutenant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(BALieutenants.Leutenant.Weapon1, self).__init__(parent, 'Weapon 1')
                self.variant(*ranged.MCBoltgun)
                armory.add_pistol_options(self)
                armory.add_combi_weapons(self)
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(BALieutenants.Leutenant.Weapon2, self).__init__(parent, 'Weapon 2')
                armory.add_melee_weapons(self)

        class Pack(OptionsList):
            def __init__(self, parent):
                super(BALieutenants.Leutenant.Pack, self).__init__(parent, 'Pack')
                self.pack = self.variant('Jump pack', armory.get_cost(units.JumpLieutenants) -
                                         armory.get_cost(units.Lieutenants))

        def __init__(self, parent):
            gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]
            cost = armory.points_price(armory.get_cost(units.Lieutenants), *gear)
            super(BALieutenants.Leutenant, self).__init__(parent, 'Leutenant', cost,
                                                       gear=armory.create_gears(*gear))
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.Pack(self)

    def __init__(self, parent):
        super(BALieutenants, self).__init__(parent)
        self.models = UnitList(self, self.Leutenant, 1, 2)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count




class BAPhobosCaptain(HqUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.PhobosCaptain)
    type_id = 'ba_phobos_captain_v1'
    kwname = 'Captain'
    keywords = ['Character', 'Infantry', 'Phobos', 'Primaris']
    power = 6

    def __init__(self, parent):
        gear = [melee.CombatKnife, ranged.MCInstigatorBoltCarbine, ranged.BoltPistol,
                wargear.CamoCloak, ranged.FragGrenades, ranged.KrakGrenades]
        super(BAPhobosCaptain, self).__init__(
            parent, get_name(units.PhobosCaptain),
            points=armory.points_price(armory.get_cost(units.PhobosCaptain), *gear),
            gear=armory.create_gears(*gear), static=True)


class BAPhobosLibrarian(HqUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.PhobosLibrarian)
    type_id = 'ba_phobos_librarian_v1'
    kwname = 'Librarian'

    keywords = ['Character', 'Infantry', 'Phobos', 'Primaris', 'Psyker']
    power = 6

    def __init__(self, parent):
        gear = [melee.ForceSword, ranged.BoltPistol,
                wargear.CamoCloak, ranged.FragGrenades, ranged.KrakGrenades]
        super(BAPhobosLibrarian, self).__init__(
            parent, get_name(units.PhobosLibrarian),
            points=armory.points_price(armory.get_cost(units.PhobosLibrarian), *gear),
            gear=armory.create_gears(*gear), static=True)


class BAPhobosLieutenant(HqUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.PhobosLieutenant)
    type_id = 'ba_phobos_lieutenant_v1'
    kwname = 'Lieutenant'

    keywords = ['Character', 'Infantry', 'Phobos', 'Primaris']
    power = 4

    class Lieutenant(ListSubUnit):
        type_name = armory.get_name(units.PhobosLieutenant)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BAPhobosLieutenant.Lieutenant.Weapon, self).__init__(parent, 'Weapon')
                gears1 = [ranged.MCOcculusBoltCarbine, ranged.BoltPistol, melee.PairedCombatBlades]
                self.variant(join_and(*gears1), get_costs(wargear.GravChute, *gears1),
                             gear=create_gears(wargear.GravChute, *gears1))
                gears2 = [ranged.HeavyBoltPistol, melee.CombatKnife]
                self.variant(join_and(*gears2), get_costs(wargear.SmokeGrenades, *gears2),
                             gear=create_gears(wargear.SmokeGrenades, *gears2))

        def __init__(self, parent):
            super(BAPhobosLieutenant.Lieutenant, self).__init__(
                parent, points=get_cost(units.PhobosLieutenant),
                gear=create_gears(ranged.FragGrenades, ranged.KrakGrenades))
            self.Weapon(self)

    def __init__(self, parent):
        super(BAPhobosLieutenant, self).__init__(parent)
        self.units = UnitList(self, self.Lieutenant, 1, 2)

    def get_count(self):
        return self.units.count

    def build_power(self):
        return self.power * self.get_count()
