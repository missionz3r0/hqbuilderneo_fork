__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit
from builder.games.wh40k8ed.options import OptionsList
from . import armory
from . import units
from . import ranged
from builder.games.wh40k8ed.utils import *


class VenLandRaider(HeavyUnit, armory.CustodesUnit):
    type_name = get_name(units.VenerableLandRaider)
    type_id = 'vland_raider_v2'
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 21

    class Options(OptionsList):
        def __init__(self, parent):
            super(VenLandRaider.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.StormBolter)
            self.variant(*ranged.HunterKillerMissile)

    def __init__(self, parent):
        gear = [ranged.TwinHeavyBolter, ranged.TwinLascannon, ranged.TwinLascannon]
        cost = points_price(get_cost(units.VenerableLandRaider), *gear)
        super(VenLandRaider, self).__init__(parent, points=cost,
                                            gear=create_gears(*gear))
        self.Options(self)
