AllarusCustodians = ('Allarus Custodians', 65)
CustodianGuard = ('Custodian Guard', 40)
CustodianWardens = ('Custodian Wardens', 43)
ShieldCaptain = ('Shield-Captain', 100)
AllarusShieldCaptain = ('Shield-Captain in Allarus Terminator Armour', 110)
BikeShieldCaptain = ('Shield-Captain on Dawneagle Jetbike', 150)
VenerableContemptorDreadnought = ('Venerable Contemptor Dreadnought', 90)
VenerableLandRaider = ('Venerable Land Raider', 217)
VertusPraetors = ('Vertus Praetors', 80)
VexilusPraetor = ('Vexilus Praetor', 80)
AllarusVexilusPraetor = ('Vexilus Praetor in Allarus Terminator Armour', 100)
TrajanValoris = ('Captain-General Trajann Valoris', 185)
