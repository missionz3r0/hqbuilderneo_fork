from .hq import Valoris, ShieldCaptain, AllarusShieldCaptain,\
    BikeShieldCaptain
from .troops import CustodianGuard
from .elites import CustodianWardens, AllarusVexilus, Vexilus,\
    AllarusCustodians, VenerableContemptor
from .fast import VertusPraetors
from .heavy import VenLandRaider

unit_types = [Valoris, ShieldCaptain, AllarusShieldCaptain,
              BikeShieldCaptain, CustodianGuard, CustodianWardens,
              AllarusVexilus, Vexilus, AllarusCustodians,
              VenerableContemptor, VertusPraetors, VenLandRaider]
