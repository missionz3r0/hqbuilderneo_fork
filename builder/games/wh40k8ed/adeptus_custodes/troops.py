__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import Gear, UnitDescription, OneOf,\
    ListSubUnit, UnitList, SubUnit, OptionsList
from . import armory
from . import units
from . import wargear
from . import ranged
from . import melee
from builder.games.wh40k8ed.utils import *


class CustodianGuard(TroopsUnit, armory.CustodesUnit):
    type_name = get_name(units.CustodianGuard)
    type_id = 'custodians_v2'
    keywords = ['Infantry']

    power = 8

    class Custorian(ListSubUnit):
        type_name = get_name(units.CustodianGuard)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(CustodianGuard.Custorian.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.GuardianSpear)
                self.variant(join_and(ranged.SentinelBlade, wargear.StormShield),
                             get_costs(ranged.SentinelBlade, wargear.StormShield),
                             gear=create_gears(ranged.SentinelBlade, wargear.StormShield))

        def __init__(self, parent):
            super(CustodianGuard.Custorian, self).__init__(parent, points=get_cost(units.CustodianGuard))
            self.Weapon(self)
            armory.Misericordia(self)

    def __init__(self, parent):
        super(CustodianGuard, self).__init__(parent)
        self.models = UnitList(self, self.Custorian, 3, 10)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power + 3 * (self.models.count - 3)
