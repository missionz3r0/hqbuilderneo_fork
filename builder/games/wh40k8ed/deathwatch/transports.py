from . import armory
from . import ranged
from . import units
from . import wargear
from builder.games.wh40k8ed.options import OneOf, OptionsList
from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.utils import get_cost, get_name, create_gears, points_price, get_costs


class DWRazorback(TransportUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Razorback (Index)'
    type_id = 'dwatch_razorback_v1'

    keywords = ['Vehicle', 'Transport']
    power = 5
    obsolete = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(DWRazorback.Options, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 6)
            self.variant('Storm bolter', 2)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DWRazorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.variant('Twin Heavy Bolter', 17)
            self.variant('Twin Lascannon', 50)
            self.variant('Twin Assault Cannon', 35)

    def __init__(self, parent):
        super(DWRazorback, self).__init__(parent=parent, points=65)
        self.Weapon(self)
        self.Options(self)


class DWRhino(TransportUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Rhino (Codex)'
    type_id = 'dwatch_rhino_v1'

    keywords = ['Vehicle', 'Transport']
    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(DWRhino.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.VehicleStormBolter)

    def __init__(self, parent):
        gear = [ranged.VehicleStormBolter]
        super(DWRhino, self).__init__(
            parent, gear=create_gears(*gear),
            points=points_price(armory.get_cost(units.Rhino), *gear))
        self.Options(self)


class CodexDWRazorback(TransportUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Razorback (Codex)'
    type_id = 'dwatch_razorback_v2'

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Options(OptionsList):
        def __init__(self, parent):
            super(CodexDWRazorback.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.VehicleStormBolter)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CodexDWRazorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)
            self.variant(*ranged.TwinAssaultCannon)

    def __init__(self, parent):
        super(CodexDWRazorback, self).__init__(parent=parent, points=get_cost(units.Razorback))
        self.Weapon(self)
        self.Options(self)


class CodexDWDropPod(TransportUnit, armory.DeathwatchUnit):
    type_id = 'dwatch_drop_pod_v2'
    type_name = get_name(units.DropPod) + ' (Codex)'

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CodexDWDropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant(*ranged.VehicleStormBolter)
            self.dwind = self.variant(*ranged.DeathwindLauncher)

    def __init__(self, parent):
        super(CodexDWDropPod, self).__init__(parent=parent, name=get_name(units.DropPod),
                                             points=get_cost(units.DropPod))
        self.Weapon(self)


class DWRepulsor(TransportUnit, armory.DeathwatchUnit):
    type_name = get_name(units.Repulsor) + ' (Codex)'
    type_id = 'dwatch_repulsor_v1'
    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 16

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DWRepulsor.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DWRepulsor.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.LasTalon)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(DWRepulsor.Weapon3, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon4(OneOf):
        def __init__(self, parent):
            super(DWRepulsor.Weapon4, self).__init__(parent, '')
            self.variant('Two stormbolters', points=get_costs(*[ranged.VehicleStormBolter] * 2),
                         gear=create_gears(*[ranged.StormBolter] * 2))
            self.variant('Two fragstorm grenade launchers', points=get_costs(*[ranged.FragstormGrenadeLauncher] * 2),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))

    class Weapon5(OneOf):
        def __init__(self, parent):
            super(DWRepulsor.Weapon5, self).__init__(parent, 'AA turret')
            self.variant(*ranged.IcarusIronhailHeavyStubber)
            self.variant(*ranged.IcarusRocketPod)
            self.variant(*ranged.StormBolter)
            self.variant(*ranged.FragstormGrenadeLauncher)

    class Weapon6(OneOf):
        def __init__(self, parent):
            super(DWRepulsor.Weapon6, self).__init__(parent, 'Launchers')
            self.variant(*wargear.AutoLaunchers)
            self.variant('Two fragstorm grenade launchers', points=get_costs(*[ranged.FragstormGrenadeLauncher] * 2),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))

    class Weapon7(OptionsList):
        def __init__(self, parent):
            super(DWRepulsor.Weapon7, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)

    def __init__(self, parent):
        gear = [ranged.KrakstormGrenadeLauncher] * 2
        super(DWRepulsor, self).__init__(parent, points=points_price(get_cost(units.Repulsor), *gear),
                                         gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)
        self.Weapon7(self)
        self.Weapon4(self)
        self.Weapon5(self)
        self.Weapon6(self)
