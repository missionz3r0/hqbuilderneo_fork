from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList,\
    SubUnit, UnitList
from . import armory
from . import melee
from . import ranged
from . import units
from . import wargear
from .troops import Terminator, Vanguard, CodexTerminator, CodexVanguard
from builder.games.wh40k8ed.utils import get_cost, get_name, create_gears, points_price, get_costs


class DWDreadnought(ElitesUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Dreadnought (Index)'
    type_id = 'dwatch_dreadnought_v1'
    keywords = ['Vehicle']
    obsolete = True
    power = 7

    model_points = 70

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(DWDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant('Storm bolter', 2)
            self.builtinheavyflamer = self.variant('Heavy flamer', 17)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DWDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DWDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant('Dreadnought combat weapon', 40)
            self.missilelauncher = self.variant('Missile launcher', 25)

    def __init__(self, parent):
        super(DWDreadnought, self).__init__(parent=parent, points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(DWDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class DWVenDreadnought(DWDreadnought):
    type_name = 'Deathwatch Venerable Dreadnought'
    type_id = 'dwatch_ven_dreadnought_v1'
    obsolete = True
    power = 8
    model_points = 90


class DWTerminators(ElitesUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Terminators'
    type_id = 'dwatch_term_v1'
    kwname = 'Terminators'
    keywords = ['Infantry']
    power = 17
    obsolete = True

    class Sergeant(Unit):
        type_name = 'Deathwatch Terminator Sergeant'

        def __init__(self, parent):
            super(DWTerminators.Sergeant, self).__init__(parent, points=Terminator.model_points)
            self.wep = Terminator.Melee(self, sword=True)

        def check_rules(self):
            super(DWTerminators.Sergeant, self).check_rules()
            self.gear = [] if self.wep.cur in self.wep.double else [Gear('Storm bolter')]

        def build_points(self):
            return super(DWTerminators.Sergeant, self).build_points()\
                + (2 if self.wep.cur not in self.wep.double else 0)

    def __init__(self, parent):
        super(DWTerminators, self).__init__(parent)
        self.ldr = SubUnit(self, self.Sergeant(parent=self))
        self.models = UnitList(self, Terminator, 4, 9)

    def get_count(self):
        return 1 + self.models.count

    def check_rules(self):
        super(DWTerminators, self).check_rules()
        thc = sum(t.has_heavy() for t in self.models.units)
        if thc > 3:
            self.error('Up to 3 Deathwatch Terminators may take heavy weapons; taken: {}'.format(thc))

    def build_power(self):
        return self.power + 13 * (self.models.count > 4)


class DWVanguard(ElitesUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Vanguard Veterans'
    type_id = 'dwatch_vanguard_v1'
    kwname = 'Vanguard Veterans'
    keywords = ['Infantry', 'Jump pack', 'Fly']
    obsolete = True
    power = 10

    class Sergeant(Unit):
        type_name = 'Deathwatch Vanguard Sergeant'

        class Melee(OneOf):
            def __init__(self, parent):
                super(DWVanguard.Sergeant.Melee, self).__init__(parent, 'Weapon')
                armory.add_vanguard_weapons(self, bolt=False)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(DWVanguard.Sergeant.Ranged, self).__init__(parent, '')
                armory.add_vanguard_weapons(self)

        def __init__(self, parent):
            super(DWVanguard.Sergeant, self).__init__(parent, points=Vanguard.model_points,
                                                      gear=[Gear('Frag grenades'), Gear('Krak grenades')])
            self.mle = self.Melee(self)
            self.rng = self.Ranged(self)

        def build_points(self):
            res = super(DWVanguard.Sergeant, self).build_points()
            if self.rng.cur == self.rng.claw and self.mle.cur == self.mle.claw:
                res -= 5
            return res

    def __init__(self, parent):
        super(DWVanguard, self).__init__(parent)
        SubUnit(self, self.Sergeant(parent=self))
        self.models = UnitList(self, Vanguard, 4, 9)

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power * (1 + (self.models.count > 4))


class DWPrimarisApothecary(ElitesUnit, armory.DeathwatchUnit):
    type_name = get_name(units.PrimarisApothecary)
    type_id = 'dwatch_primaris_apothecary_v1'
    kwname = 'Apothecary'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 4

    def __init__(self, parent):
        gear = [ranged.ReductorPistol, ranged.AbsolvorBoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(armory.get_cost(units.PrimarisApothecary), *gear)
        super(DWPrimarisApothecary, self).__init__(parent, points=cost, gear=create_gears(*gear),
                                                   static=True)


class CodexDWDreadnought(ElitesUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Dreadnought (Codex)'
    type_id = 'dwatch_dreadnought_v2'
    keywords = ['Vehicle']
    power = 7

    model_points = get_cost(units.Dreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(CodexDWDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.VehicleStormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexDWDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexDWDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtCombatWeapon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)

    def __init__(self, parent):
        super(CodexDWDreadnought, self).__init__(parent=parent, points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(CodexDWDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class CodexDWVenDreadnought(CodexDWDreadnought):
    type_name = 'Deathwatch Venerable Dreadnought (Codex)'
    type_id = 'dwatch_ven_dreadnought_v2'

    power = 8
    model_points = get_cost(units.VenerableDreadnought)


class DWRedemptorDreadnought(ElitesUnit, armory.DeathwatchUnit):
    type_name = get_name(units.RedemptorDreadnought)
    type_id = 'dwatch_redemptor_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 10
    gears = [melee.RedemptorFist]

    class Options(OptionsList):
        def __init__(self, parent):
            super(DWRedemptorDreadnought.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.IcarusRocketPod)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DWRedemptorDreadnought.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HeavyFlamer)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DWRedemptorDreadnought.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.MacroPlasmaIncinerator)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(DWRedemptorDreadnought.Weapon3, self).__init__(parent, 'Secondary weapon')
            self.variant('Two fragstorm grenade launchers', 2 * armory.get_cost(ranged.FragstormGrenadeLauncher),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))
            self.variant('Two storm bolters', 2 * armory.get_cost(ranged.StormBolter),
                         gear=create_gears(*[ranged.StormBolter] * 2))

    def __init__(self, parent):
        cost = points_price(armory.get_cost(units.RedemptorDreadnought), *self.gears)
        gear = create_gears(*self.gears)
        super(DWRedemptorDreadnought, self).__init__(parent, points=cost, gear=gear)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)
        self.Options(self)


class CodexDWTerminators(ElitesUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Terminators (Codex)'
    type_id = 'dwatch_term_v2'
    kwname = 'Terminators'
    keywords = ['Infantry']
    power = 16

    class Sergeant(Unit):
        type_name = 'Deathwatch Terminator Sergeant'

        def __init__(self, parent):
            super(CodexDWTerminators.Sergeant, self).__init__(parent, points=CodexTerminator.model_points)
            self.wep = CodexTerminator.Melee(self, sword=True)

        def check_rules(self):
            super(CodexDWTerminators.Sergeant, self).check_rules()
            self.gear = [] if self.wep.cur in self.wep.double else [Gear('Storm bolter')]

        def build_points(self):
            return super(CodexDWTerminators.Sergeant, self).build_points()\
                + (2 if self.wep.cur not in self.wep.double else 0)

    def __init__(self, parent):
        super(CodexDWTerminators, self).__init__(parent)
        self.ldr = SubUnit(self, self.Sergeant(parent=self))
        self.models = UnitList(self, CodexTerminator, 4, 9)

    def get_count(self):
        return 1 + self.models.count

    def check_rules(self):
        super(CodexDWTerminators, self).check_rules()
        thc = sum(t.has_heavy() for t in self.models.units)
        if thc > 3:
            self.error('Up to 3 Deathwatch Terminators may take heavy weapons; taken: {}'.format(thc))

    def build_power(self):
        return self.power + 12 * (self.models.count > 4)


class CodexDWVanguard(ElitesUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Vanguard Veterans (Codex)'
    type_id = 'dwatch_vanguard_v2'
    kwname = 'Vanguard Veterans'
    keywords = ['Infantry', 'Jump pack', 'Fly']

    power = 9

    class Sergeant(Unit):
        type_name = 'Deathwatch Vanguard Sergeant'

        class Melee(OneOf):
            def __init__(self, parent):
                super(CodexDWVanguard.Sergeant.Melee, self).__init__(parent, 'Weapon')
                armory.add_vanguard_weapons(self)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(CodexDWVanguard.Sergeant.Ranged, self).__init__(parent, '')
                armory.add_vanguard_weapons(self)

        def __init__(self, parent):
            super(CodexDWVanguard.Sergeant, self).__init__(parent, points=CodexVanguard.model_points,
                                                      gear=[Gear('Frag grenades'), Gear('Krak grenades')])
            self.mle = self.Melee(self)
            self.rng = self.Ranged(self)

        def build_points(self):
            res = super(CodexDWVanguard.Sergeant, self).build_points()
            if self.rng.cur == self.rng.claw and self.mle.cur == self.mle.claw:
                res -= 5
            return res

    def __init__(self, parent):
        super(CodexDWVanguard, self).__init__(parent)
        SubUnit(self, self.Sergeant(parent=self))
        self.models = UnitList(self, CodexVanguard, 4, 9)

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power * (1 + (self.models.count > 4))


class DWAgressors(ElitesUnit, armory.DeathwatchUnit):
    type_name = get_name(units.Aggressors)
    type_id = 'dwatch_agressors_v1'

    keywords = ['Infantry', 'Primaris', 'Mk X gravis']

    power = 6

    class Weapons(OneOf):
        def __init__(self, parent):
            super(DWAgressors.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Auto boltstorm gauntlets and fragstorm grenade launcher',
                         get_costs(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher),
                         gear=create_gears(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher))
            self.variant(*ranged.FlamestormGauntlets)

        @property
        def description(self):
            return [UnitDescription('Aggressor Sergeant').add(self.cur.gear)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Aggressor').add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(DWAgressors, self).__init__(parent, points=get_cost(units.Aggressors))
        self.wep = self.Weapons(self)
        self.models = self.Marines(self, 'Agressors', 2, 5, points=get_cost(units.Aggressors),
                                   per_model=True)

    def get_count(self):
        return 1 + self.models.cur

    def build_points(self):
        return super(DWAgressors, self).build_points() + self.wep.points * self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))


class DWReivers(ElitesUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch ' + get_name(units.Reivers)
    type_id = 'dwatch_reivers_v1'
    keywords = ['Infantry', 'Primaris']

    power = 6

    model_gear = [ranged.HeavyBoltPistol, ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.ShockGrenades]

    class Wargear(OptionsList):
        def __init__(self, parent):
            super(DWReivers.Wargear, self).__init__(parent, 'Squad wargear', used=False)
            self.variant(*wargear.GravChute, per_model=True)
            self.variant(*wargear.GrapnelLauncher, per_model=True)

    class SergeantKnife(OneOf):
        def __init__(self, parent):
            super(DWReivers.SergeantKnife, self).__init__(parent, 'Sergeant weapons')
            self.variant('Bolt carbine and heavy bolt pistol',
                         points=get_costs(ranged.BoltCarbine, ranged.HeavyBoltPistol),
                         gear=create_gears(ranged.BoltCarbine, ranged.HeavyBoltPistol))
            self.variant('Bolt carbine and combat knife',
                         points=get_costs(ranged.BoltCarbine, melee.CombatKnife),
                         gear=create_gears(ranged.BoltCarbine, melee.CombatKnife))
            self.variant('Heavy bolt pistol',
                         points=get_costs(ranged.HeavyBoltPistol, melee.CombatKnife),
                         gear=create_gears(ranged.HeavyBoltPistol, melee.CombatKnife))

        @property
        def description(self):
            return [UnitDescription('Reiver Sergeant', options=create_gears(*DWReivers.model_gear[1:])).\
                    add(super(DWReivers.SergeantKnife, self).description).add(self.parent.wgear.description)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Reiver', options=create_gears(*DWReivers.model_gear)).
                                    add(self.parent.wep.cur.gear).
                                    add(self.parent.wgear.description).set_count(self.cur)]

    class SquadWeapon(OneOf):
        def __init__(self, parent):
            super(DWReivers.SquadWeapon, self).__init__(parent, 'Squad weapons', used=False)
            self.variant(*ranged.BoltCarbine)
            self.variant(*melee.CombatKnife)

    def __init__(self, parent):
        sarge_cost = armory.points_price(armory.get_cost(units.Reivers), *DWReivers.model_gear[1:])
        super(DWReivers, self).__init__(parent, name=get_name(units.Reivers), points=sarge_cost)
        self.wgear = self.Wargear(self)
        self.wep = self.SquadWeapon(self)
        self.sarge = self.SergeantKnife(self)
        self.models = self.Marines(self, 'Reivers', 4, 9, sarge_cost + armory.get_cost(DWReivers.model_gear[0]),
                                   per_model=True)

    def get_count(self):
        return self.models.cur + 1

    def build_points(self):
        return super(DWReivers, self).build_points() +\
            (self.wgear.points + self.wep.points) * (1 + self.models.cur)

    def build_power(self):
        return self.power * (1 + (self.models.cur > 4))
