from . import armory
from . import ranged
from . import units
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
from builder.games.wh40k8ed.unit import HeavyUnit
from builder.games.wh40k8ed.utils import get_cost, get_name, create_gears, points_price


class DWHellblasters(HeavyUnit, armory.DeathwatchUnit):
    type_name = get_name(units.Hellblasters) + ' (Codex)'
    type_id = 'dwatch_hellblasters_v1'
    keywords = ['Infantry', 'Primaris']
    power = 8

    model_gear = [
        Gear('Frag grenades'), Gear('Krak grenades'),
        Gear('Bolt pistol')
    ]

    class Sergeant(OneOf):
        def __init__(self, parent):
            super(DWHellblasters.Sergeant, self).__init__(parent, 'Sergeant weapon')
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.PlasmaPistol)

        @property
        def description(self):
            return [UnitDescription('Hellblaster Sergeant', options=DWHellblasters.model_gear[0:2]).add(
                super(DWHellblasters.Sergeant, self).description).add(self.parent.wep.cur.gear)]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DWHellblasters.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*ranged.PlasmaIncinerator)
            self.variant(*ranged.AssaultPlasmaIncinerator)
            self.variant(*ranged.HeavyPlasmaIncinerator)

        @property
        def description(self):
            return []

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Hellblaster', options=DWHellblasters.model_gear).add(
                self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(DWHellblasters, self).__init__(parent, points=armory.get_cost(units.Hellblasters))
        self.sarge = self.Sergeant(self)
        self.wep = self.Weapon(self)
        self.marines = self.Marines(self, 'Hellblasters', 4, 9,
                                    armory.get_cost(units.Hellblasters), per_model=True)

    def build_points(self):
        return super(DWHellblasters, self).build_points() + self.wep.points * self.marines.cur

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))


class DWLandRaider(HeavyUnit, armory.DeathwatchUnit):
    type_id = 'dwatch_land_raider_v1'
    type_name = get_name(units.LandRaider) + ' (Codex)'

    keywords = ['Vehicle', 'Transport']
    power = 19

    class Options(OptionsList):
        def __init__(self, parent):
            super(DWLandRaider.Options, self).__init__(parent, name='Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.VehicleStormBolter)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        gear = [ranged.TwinHeavyBolter, ranged.TwinLascannon, ranged.TwinLascannon]
        cost = points_price(armory.get_cost(units.LandRaider), *gear)
        super(DWLandRaider, self).__init__(parent=parent, points=cost,
                                           gear=create_gears(*gear))
        self.opt = self.Options(self)


class DWLandRaiderCrusader(HeavyUnit, armory.DeathwatchUnit):
    type_id = 'dwatch_land_raider_crusader_v1'
    type_name = get_name(units.LandRaiderCrusader) + ' (Codex)'
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 16

    def __init__(self, parent):
        gear = [ranged.TwinAssaultCannon, ranged.HurricaneBolter, ranged.HurricaneBolter]
        cost = points_price(get_cost(units.LandRaiderCrusader), *gear)
        super(DWLandRaiderCrusader, self).__init__(parent=parent, gear=create_gears(*gear),
                                                   points=cost)
        self.opt = DWLandRaider.Options(self)


class DWLandRaiderRedeemer(HeavyUnit, armory.DeathwatchUnit):
    type_id = 'dwatch_land_raider_redeemer_v1'
    type_name = get_name(units.LandRaiderRedeemer) + ' (Codex)'
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 18

    def __init__(self, parent):
        gear = [ranged.TwinAssaultCannon, ranged.FlamestormCannon, ranged.FlamestormCannon]
        cost = points_price(get_cost(units.LandRaiderRedeemer), *gear)
        super(DWLandRaiderRedeemer, self).__init__(parent=parent, points=cost,
                                                   gear=create_gears(*gear))
        self.opt = DWLandRaider.Options(self)
