from builder.core2 import SubUnit, UnitList
from builder.games.wh40k8ed.unit import FastUnit, Unit, ListSubUnit
from builder.games.wh40k8ed.options import OneOf
from builder.games.wh40k8ed.utils import *
from . import armory
from . import ranged
from . import units


class InterceptorSquad(FastUnit, armory.GKUnit):
    type_name = get_name(units.InterceptorSquad)
    type_id = 'interceptor_squad_v1'
    keywords = ['Infantry', 'Psyker']
    power = 8

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class MeleeWeapon(OneOf):
        def __init__(self, parent):
            super(InterceptorSquad.MeleeWeapon, self).__init__(parent=parent, name='Melee Weapon')
            armory.add_melee_weapons(self)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(InterceptorSquad.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_ranged_weapons(self)

    class Interceptor(ListSubUnit):
        def __init__(self, parent):
            super(InterceptorSquad.Interceptor, self).__init__(parent=parent, name='Interceptor',
                                                               points=get_cost(units.InterceptorSquad),
                                                               gear=create_gears(*InterceptorSquad.model_gear))
            self.melee = InterceptorSquad.MeleeWeapon(self)
            self.ranged = InterceptorSquad.SpecialWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.ranged.cur != self.ranged.sb

        def check_rules(self):
            super(InterceptorSquad.Interceptor, self).check_rules()
            self.melee.used = self.melee.visible = not self.count_special()

    class Sergeant(Unit):
        def __init__(self, parent):
            super(InterceptorSquad.Sergeant, self).__init__(parent=parent, name='Interceptor Justicar',
                                                            points=get_cost(units.InterceptorSquad) +
                                                                   get_cost(ranged.StormBolter),
                                                            gear=create_gears(
                                                                *(InterceptorSquad.model_gear + [ranged.StormBolter])
                                                            ))
            self.melee = InterceptorSquad.MeleeWeapon(self)

    def __init__(self, parent):
        super(InterceptorSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=self))
        self.interceptor = UnitList(self, self.Interceptor, min_limit=4, max_limit=9)

    def get_count(self):
        return self.interceptor.count + 1

    def build_power(self):
        return self.power * (1 + self.interceptor.count > 4)

    def check_rules(self):
        super(InterceptorSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.interceptor.units)
        if 5 * spec_total > self.get_count():
            self.error('In Intercaptor squad may be only 1 special weapon per 5 models (taken: {0})'.format(spec_total))
