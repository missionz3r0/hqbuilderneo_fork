from .units import Elucia, Larsen, Sanistasia, KnossoPrond, NitschSquad
from builder.games.wh40k8ed.rosters import DetachPatrol, DetachVanguard, DetachAuxilary


class DetachPatrol_elucidian_starstriders(DetachPatrol):
    army_name = 'Elucidian Starstriders (Patrol detachment)'
    faction_base = 'ELUCIDIAN STARSTRIDERS'
    army_id = 'patrol_elucidian_starstriders'
    army_factions = ['ELUCIDIAN STARSTRIDERS', 'IMPERIUM']

    def __init__(self, parent=None):
        super(DetachPatrol_elucidian_starstriders, self).__init__(parent=parent, hq=True, elite=True, troops=True)
        self.troops.add_classes([NitschSquad])
        self.elite.add_classes([Larsen, Sanistasia, KnossoPrond])
        self.hq.add_classes([Elucia])


class DetachVanguard_elucidian_starstriders(DetachVanguard):
    army_name = 'Elucidian Starstriders (Vanguard detachment)'
    faction_base = 'ELUCIDIAN STARSTRIDERS'
    army_id = 'vanguard_elucidian_starstriders'
    army_factions = ['ELUCIDIAN STARSTRIDERS', 'IMPERIUM']

    def __init__(self, parent=None):
        super(DetachVanguard_elucidian_starstriders, self).__init__(parent=parent, hq=True, elite=True, troops=True)
        self.troops.add_classes([NitschSquad])
        self.elite.add_classes([Larsen, Sanistasia, KnossoPrond])
        self.hq.add_classes([Elucia])


class DetachAuxilary_elucidian_starstriders(DetachAuxilary):
    army_name = 'Elucidian Starstriders (Auxilary Support Detachment)'
    faction_base = 'ELUCIDIAN STARSTRIDERS'
    army_id = 'vanguard_elucidian_starstriders'
    army_factions = ['ELUCIDIAN STARSTRIDERS', 'IMPERIUM']

    def __init__(self, parent=None):
        super(DetachAuxilary_elucidian_starstriders, self).__init__(parent=parent, hq=True, elite=True, troops=True)
        self.troops.add_classes([NitschSquad])
        self.elite.add_classes([Larsen, Sanistasia, KnossoPrond])
        self.hq.add_classes([Elucia])


unit_types = [Elucia, Larsen, Sanistasia, KnossoPrond, NitschSquad]
detachments = [DetachPatrol_elucidian_starstriders,
               DetachVanguard_elucidian_starstriders,
               DetachAuxilary_elucidian_starstriders]
