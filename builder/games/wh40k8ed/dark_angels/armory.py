from builder.games.wh40k8ed.options import OneOf
from builder.games.wh40k8ed.space_marines.armory import SMUnit
from . import melee, ranged, wargear
from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.utils import get_cost


class DAUnit(SMUnit):
    faction_substitution = {'Dark Angels': '<Chapter>'}
    wiki_faction = 'Dark Angels'

    @classmethod
    def calc_faction(cls):
        return ['<DARK ANGELS SUCCESSORS>']


class SergeantWeapon(OneOf):
    sword = True

    def add_single(self):
        self.single = [
            self.variant(*ranged.Boltgun),
            self.variant(*ranged.CombiFlamer),
            self.variant(*ranged.CombiGrav),
            self.variant(*ranged.CombiMelta),
            self.variant(*ranged.CombiPlasma),
            self.variant(*ranged.StormBolter)
        ]

    def add_double(self):
        self.double = [
                          self.variant(*ranged.BoltPistol),
                          self.variant(*ranged.GravPistol),
                          self.variant(*ranged.PlasmaPistol)] + \
                      ([self.variant(*melee.Chainsword)] if self.sword else []) + \
                      [self.variant(*melee.PowerSword),
                       self.variant(*melee.PowerAxe),
                       self.variant(*melee.PowerMaul),
                       self.variant(*melee.ThunderHammer)]
        self.claw = self.variant(*melee.SingleLightningClaw)
        self.double += [self.claw,
                        self.variant(*melee.PowerFist)]

    def __init__(self, parent, double=False):
        super(SergeantWeapon, self).__init__(parent, 'Sergeant Weapon')
        if double:
            self.add_double()
        self.add_single()
        if not double:
            self.add_double()

    @staticmethod
    def ensure_pairs(first, second):
        for opt in second.single:
            opt.active = not first.cur in first.single
        for opt in first.single:
            opt.active = not second.cur in second.single


def add_pistol_options(obj):
    obj.variant(*ranged.BoltPistol)
    obj.variant(*ranged.GravPistol)
    obj.variant(*ranged.PlasmaPistol)


def add_combi_weapons(obj):
    obj.combi = [
        obj.variant(*ranged.CombiFlamer),
        obj.variant(*ranged.CombiGrav),
        obj.variant(*ranged.CombiMelta),
        obj.variant(*ranged.CombiPlasma),
        obj.variant(*ranged.StormBolter)]


def add_melee_weapons(obj, axe=True):
    obj.variant(*melee.Chainsword)
    obj.variant(*melee.PowerSword)
    if not axe:
        obj.variant(*melee.PowerAxe)
    obj.variant(*melee.PowerMaul)
    obj.claw = obj.variant(*melee.SingleLightningClaw)
    obj.variant(*melee.PowerFist)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant(*melee.CharacterThunderHammer)
    else:
        obj.variant(*melee.ThunderHammer)


def add_special_weapons(obj):
    obj.spec = [
        obj.variant(*ranged.Flamer),
        obj.variant(*ranged.PlasmaGun),
        obj.variant(*ranged.Meltagun),
        obj.variant(*ranged.GravGun)]


def add_heavy_weapons(obj):
    obj.heavy = [
        obj.variant(*ranged.GravCannon),
        obj.variant(*ranged.HeavyBolter),
        obj.variant(*ranged.Lascannon),
        obj.variant(*ranged.MissileLauncher),
        obj.variant(*ranged.MultiMelta),
        obj.variant(*ranged.PlasmaCannon)]


def add_term_melee_weapons(obj):
    obj.claw = obj.variant(*melee.SingleLightningClaw)
    obj.fist = obj.variant(*melee.PowerFist)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant(*wargear.StormShieldCharacters)
        obj.variant(*melee.CharacterThunderHammer)
    else:
        obj.variant(*wargear.StormShield)
        obj.variant(*melee.ThunderHammer)


def add_term_combi_weapons(obj):
    obj.variant(*ranged.StormBolter)
    obj.variant(*ranged.CombiPlasma)
    obj.variant(*ranged.CombiFlamer)
    obj.variant(*ranged.CombiMelta)


def add_term_heavy_weapons(obj):
    obj.heavy = [
        obj.variant(*ranged.HeavyFlamer),
        obj.variant(*ranged.AssaultCannon)]
    # obj.variant('Cyclone missile launcher and storm bolter',
    #             get_costs(ranged.CycloneMissileLauncher, ranged.StormBolter),
    #             gear=create_gears(ranged.CycloneMissileLauncher, ranged.StormBolter))]


def add_dred_heavy_weapons(obj):
    obj.variant(*ranged.HeavyPlasmaCannon)
    obj.variant(*ranged.MultiMelta)
    obj.variant(*ranged.TwinLascannon)

class ClawUser(Unit):
    def build_points(self):
        res = super(ClawUser, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -=  get_cost(melee.PairLightningClaws) - get_cost(melee.SingleLightningClaw)
        return res
