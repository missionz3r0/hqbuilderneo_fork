from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit, \
    SubUnit, UnitList, OptionalSubUnit
from . import armory
from . import units
from . import ranged
from . import wargear
from . import melee
from builder.games.wh40k8ed.utils import get_name, create_gears, points_price, get_costs, get_cost


class RavenwingBikeSquad(FastUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingBikeSquad)
    type_id = 'ravenwing_bikers_v1'
    faction = ['Ravenwing']
    keywords = ['Biker']
    armory = armory
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.TwinBoltgun]
    model_points = points_price(armory.get_cost(units.RavenwingBikeSquad),
                                *model_gear)
    attack_points = armory.get_cost(units.RavenwingAttackBike)
    power = 5

    class Sergeant(Unit):
        type_name = 'Ravenwing Sergeant'

        def __init__(self, parent):
            super(RavenwingBikeSquad.Sergeant, self).__init__(
                parent=parent, points=parent.model_points,
                gear=create_gears(*RavenwingBikeSquad.model_gear)
            )
            class Weapon(OneOf):
                def __init__(self, parent):
                    super(Weapon, self).__init__(parent, 'Weapon')
                    armory.add_melee_weapons(self)
                    armory.add_pistol_options(self)
            Weapon(self)

    class RavenwingAttackBike(Unit):
        type_name = 'Ravenwing Attack Bike'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(RavenwingBikeSquad.RavenwingAttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant(*ranged.HeavyBolter)
                self.multimelta = self.variant(*ranged.MultiMelta)

        def __init__(self, parent):
            cost = points_price(parent.attack_points, ranged.TwinBoltgun)
            super(RavenwingBikeSquad.RavenwingAttackBike, self).__init__(
                parent=parent, points=cost,
                gear=[Gear('Twin boltgun'), UnitDescription('Dark Angel', count=2,
                                                            options=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                                     Gear('Bolt pistol')])])
            self.wep = self.Weapon(self)

    class OptUnits(OptionalSubUnit):
        def __init__(self, parent):
            super(RavenwingBikeSquad.OptUnits, self).__init__(parent=parent, name='')
            self.attackbike = SubUnit(self, RavenwingBikeSquad.RavenwingAttackBike(parent=parent))

    class Biker(Count):
        @property
        def description(self):
            return [UnitDescription('Ravenwing Biker',
                                    options=create_gears(*RavenwingBikeSquad.model_gear) + [Gear('Bolt pistol')]) \
                        .set_count(self.cur - self.parent.sword.cur - sum(c.cur for c in self.parent.spec))]

    def variant(self, name, points):
        return Count(self, name, 0, 2, points,
                     gear=UnitDescription('Space Marine Biker',
                                          options=create_gears(*RavenwingBikeSquad.model_gear) + [Gear(name)]))

    def __init__(self, parent):
        super(RavenwingBikeSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.opt_units = self.OptUnits(self)

        self.bikers = self.Biker(self, 'Ravenwing Bikers', 2, 7, self.model_points, per_model=True)
        self.sword = Count(self, 'Chainsword', 0, 2, gear=UnitDescription('Ravenwing Biker',
                                                                          options=create_gears(
                                                                              *RavenwingBikeSquad.model_gear) + [
                                                                                      Gear('Chainsword')]))
        self.spec = []
        self.armory.add_special_weapons(self)

    def check_rules(self):
        super(RavenwingBikeSquad, self).check_rules()
        Count.norm_counts(0, 2, self.spec)
        self.sword.max = self.bikers.cur - sum(c.cur for c in self.spec)

    def get_count(self):
        return self.opt_units.count + self.bikers.cur + 1

    def build_power(self):
        return 3 * self.opt_units.count + ((6 + self.power) if self.bikers.cur > 5 else
                                           ((4 + self.power) if self.bikers.cur > 3 else self.power))


class RavenwingAttackBikeSquad(FastUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingAttackBikeSquad)
    type_id = 'attack_bikes_v1'
    faction = ['Ravenwing']
    keywords = ['Biker']

    model_points = points_price(armory.get_cost(units.RavenwingAttackBikeSquad), ranged.TwinBoltgun)
    model = UnitDescription('Ravenwing Attack Bike', options=[
        Gear('Twin boltgun'), UnitDescription('Dark Angel', count=2,
                                              options=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                       Gear('Bolt pistol')])])

    class Biker(Count):
        @property
        def description(self):
            return [RavenwingAttackBikeSquad.model.clone().add(Gear('Heavy bolter'))
                        .set_count(self.cur - self.parent.melta.cur)]

    def __init__(self, parent):
        super(RavenwingAttackBikeSquad, self).__init__(parent)
        self.bikers = self.Biker(self, 'Ravenwing Attack bikes', 1, 3, self.model_points, per_model=True)
        self.melta = Count(self, 'Multi-melta', 0, 1, armory.get_cost(ranged.MultiMelta),
                           gear=RavenwingAttackBikeSquad.model.clone().add(Gear('Multi-melta')))

    def check_rules(self):
        super(RavenwingAttackBikeSquad, self).check_rules()
        self.melta.max = self.bikers.cur

    def get_count(self):
        return self.bikers.cur

    def build_power(self):
        return 3 * self.bikers.cur

    def build_points(self):
        # take HB into account
        return super(RavenwingAttackBikeSquad, self).build_points() + armory.get_cost(ranged.HeavyBolter) * (
            self.bikers.cur - self.melta.cur)


class RavenwingSpeeders(FastUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingLandSpeeders)
    type_id = "ravenwing_speeders_v1"
    faction = ['Ravenwing']
    keywords = ['Vehicle', 'Land Speeder', 'Fly']

    class LandSpeeder(ListSubUnit):
        type_name = "Ravenwing Land Speeder"

        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(RavenwingSpeeders.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant(*ranged.HeavyBolter)
                self.multimelta = self.variant(*ranged.MultiMelta)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(RavenwingSpeeders.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade',
                                                                             limit=1)
                self.assaultcannon = self.variant(*ranged.AssaultCannon)
                self.heavyflamer = self.variant(*ranged.HeavyFlamer)
                self.typhoonmissilelauncher = self.variant(*ranged.TyphoonMissileLauncher)

        def __init__(self, parent):
            super(RavenwingSpeeders.LandSpeeder, self).__init__(parent=parent,
                                                                points=get_cost(units.RavenwingLandSpeeders))
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

    def __init__(self, parent):
        super(RavenwingSpeeders, self).__init__(parent=parent)
        self.speeders = UnitList(parent=self, unit_class=self.LandSpeeder, min_limit=1, max_limit=5)

    def get_count(self):
        return self.speeders.count

    def build_power(self):
        return 6 * self.speeders.count


class RavenwingDarkShroud(FastUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingDarkshroud)
    type_id = 'ravenwing_darkshroud_v1'
    faction = ['Ravenwing']
    keywords = ['Vehicle', 'Land Speeder', 'Fly']
    power = 7

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RavenwingDarkShroud.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant(*ranged.HeavyBolter)
            self.assaultcannon = self.variant(*ranged.AssaultCannon)

    def __init__(self, parent):
        super(RavenwingDarkShroud, self).__init__(parent=parent, points=get_cost(units.RavenwingDarkshroud))
        self.wep = self.Weapon(self)


class RavenwingBlackKnights(FastUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingBlackKnights)
    type_id = 'ravenwing_black_knights_v1'
    faction = ['Ravenwing']

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.BoltPistol]
    model_points = points_price(armory.get_cost(units.RavenwingBlackKnights),
                                *model_gear)

    class Huntmaster(Unit):
        type_name = 'Ravenwing Huntmaster'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(RavenwingBlackKnights.Huntmaster.Weapon, self).__init__(parent=parent, name='Weapon')
                self.corvushummer = self.variant(*melee.CorvusHammer)
                self.variant(*melee.PowerSword)
                self.variant(*melee.PowerMaul)

        class Options(OptionsList):
            def __init__(self, parent):
                super(RavenwingBlackKnights.Huntmaster.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant(*ranged.MeltaBombs)

        def __init__(self, parent):
            super(RavenwingBlackKnights.Huntmaster, self).__init__(
                parent=parent, points=RavenwingBlackKnights.model_points,
                gear=create_gears(ranged.PlasmaTalon, *RavenwingBlackKnights.model_gear))
            self.weapon = self.Weapon(self)
            self.opt = self.Options(self)

    class Biker(Count):
        @property
        def description(self):
            return [UnitDescription('Ravenwing Black Knight', count=self.cur - self.parent.grenade.cur,
                                    options=create_gears(
                                        ranged.PlasmaTalon, melee.CorvusHammer,
                                        *RavenwingBlackKnights.model_gear))]

    def __init__(self, parent):
        super(RavenwingBlackKnights, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Huntmaster(self))
        self.marines = self.Biker(self, 'Ravenwing Black Knights', 2, 9, self.model_points, per_model=True)
        self.grenade = Count(self, 'Ravenwing grenade launcher', 0, 1, get_cost(ranged.RavenwingGrenadeLauncher),
                             gear=UnitDescription(
                                 'Ravenwing Black Knight',
                                 options=create_gears(
                                     RavenwingBlackKnights.model_gear + [ranged.RavenwingGrenadeLauncher,
                                                                         melee.CorvusHammer])))

    def get_count(self):
        return self.marines.cur + 1

    def check_rules(self):
        super(RavenwingBlackKnights, self).check_rules()
        self.grenade.max = int(self.get_count() / 3)

    def build_power(self):
        return 7 + (17 if self.marines.cur > 4 else
                    (5 if self.marines.cur > 2 else 0))


class LandSpeederVengeance(FastUnit, armory.DAUnit):
    type_name = get_name(units.RavenwingLandSpeederVengeance)
    type_id = "land_speeder_vengeance_v1"
    faction = ['Ravenwing']
    keywords = ['Vehicle', 'Land speeder', 'Fly']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LandSpeederVengeance.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant(*ranged.HeavyBolter)
            self.assaultcannon = self.variant(*ranged.AssaultCannon)

    def __init__(self, parent):
        gear = [ranged.PlasmaStormBattery]
        cost = points_price(get_cost(units.RavenwingLandSpeederVengeance), *gear)
        super(LandSpeederVengeance, self).__init__(parent=parent, points=cost, gear=create_gears(*gear))
        self.weapon = self.Weapon(self)


class DAScoutBikers(FastUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.ScoutBikeSquad)
    type_id = 'da_scout_bike_squad_v1'
    keywords = ['Biker', 'Scout']

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.AstartesShotgun, melee.CombatKnife]
    model_points = get_cost(units.ScoutBikeSquad)

    armory = armory

    class Sergeant(Unit):
        type_name = 'Scout Biker Sergeant'

        def __init__(self, parent):
            gear = DAScoutBikers.model_gear + [ranged.TwinBoltgun]
            super(DAScoutBikers.Sergeant, self).__init__(
                parent=parent, points=points_price(DAScoutBikers.model_points, *gear),
                gear=create_gears(*gear))
            self.wep = parent.armory.SergeantWeapon(self, double=True)

    class Bikers(Count):
        @property
        def description(self):
            return [UnitDescription('Scout Biker', options=create_gears(DAScoutBikers.model_gear) + [
                Gear('Bolt pistol'), Gear('Twin boltgun')
            ]).set_count(self.cur - self.parent.grenade.cur)]

    def __init__(self, parent):
        super(DAScoutBikers, self).__init__(parent=parent, name=get_name(units.ScoutBikeSquad))
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))

        self.bikers = self.Bikers(self, 'Scout Biker', 2, 9, self.model_points, per_model=True)
        self.grenade = Count(self, 'Astartes grenade launcher', 0, 3, get_cost(ranged.AstartesGrenadeLauncher),
                             gear=UnitDescription('Scout Biker', options=create_gears(DAScoutBikers.model_gear) + [
                                 Gear('Bolt pistol'), Gear('Astartes grenade launcher')
                             ]))

    def get_count(self):
        return self.bikers.cur + 1

    def check_rules(self):
        super(DAScoutBikers, self).check_rules()
        self.grenade.max = min(self.bikers.cur, 3)

    def build_power(self):
        return 1 + 4 * ((self.bikers.cur + 3) / 3)

    def build_points(self):
        # take twin boltgun costs into account
        return super(DAScoutBikers, self).build_points() + 2 * (self.bikers.cur - self.grenade.cur)


class DACodexAssaultSquad(FastUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.AssaultSquad)
    type_id = 'da_assault_squad_v2'
    keywords = ['Infantry']

    model_points = get_cost(units.AssaultSquad)
    model_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(armory.ClawUser):
        type_name = 'Space Marine Sergeant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(DACodexAssaultSquad.Sergeant.Weapon1, self).__init__(parent, 'Weapon')
                armory.add_pistol_options(self)
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(DACodexAssaultSquad.Sergeant.Weapon2, self).__init__(parent, '')
                armory.add_melee_weapons(self)
                self.evs = self.variant(*melee.Eviscerator)

        class Options(OptionsList):
            def __init__(self, parent):
                super(DACodexAssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant(*ranged.MeltaBombs)
                self.combatshiled = self.variant(*wargear.CombatShield)

        def __init__(self, parent):
            super(DACodexAssaultSquad.Sergeant, self).__init__(
                parent=parent, points=DACodexAssaultSquad.model_points,
                gear=DACodexAssaultSquad.model_gear
            )
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(DACodexAssaultSquad.Sergeant, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.has_evs()

        def has_evs(self):
            return self.wep2.cur == self.wep2.evs

    class Jumppack(OptionsList):
        def __init__(self, parent):
            super(DACodexAssaultSquad.Jumppack, self).__init__(parent=parent, name='Movement options')
            self.jumppack = self.variant(
                'Jump Packs',
                armory.get_cost(units.JumpAssaultSquad) - armory.get_cost(units.AssaultSquad),
                per_model=True)

    # def build_points(self):
    #     return super(AssaultSquad, self).build_points() + self.pack.points * self.marines.cur * self.pack.used

    def __init__(self, parent):
        super(DACodexAssaultSquad, self).__init__(parent=parent,
                                                  name=get_name(units.AssaultSquad))
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.marines = Count(parent=self, name='Space Marine',
                             min_limit=4, max_limit=9,
                             points=self.model_points,
                             per_model=True)
        self.flame = Count(self, 'Flamer', 0, 2,
                           armory.get_cost(ranged.Flamer))
        self.pp = Count(self, 'Plasma pistol', 0, 2,
                        armory.get_cost(ranged.PlasmaPistol))
        self.evs = Count(self, 'Eviscerator', 0, 2,
                         armory.get_cost(melee.Eviscerator))
        self.pack = self.Jumppack(parent=self)

    def check_rules(self):
        super(DACodexAssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.pp])
        evs_max = (self.get_count() / 5) - self.sergeant.unit.has_evs()
        self.evs.used = self.evs.visible = evs_max > 0
        self.evs.max = evs_max

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points,
                               count=self.get_count(), options=self.pack.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=DACodexAssaultSquad.model_gear
        )
        if self.pack.any and self.pack.used:
            marine.add(Gear('Jump Pack'))
            marine.add_points(self.pack.points)
        if self.evs.cur:
            desc.add(marine.clone().
                     add(Gear('Eviscerator')).
                     set_count(self.evs.cur))
        count = self.marines.cur - self.evs.cur
        if self.flame.cur:
            desc.add_dup(marine.clone().add(Gear('Flamer')).set_count(self.flame.cur))
            count -= self.flame.cur
        if self.pp.cur:
            desc.add_dup(marine.clone().add([Gear('Chainsword'), Gear('Plasma pistol')]).set_count(self.pp.cur))
            count -= self.pp.cur
        desc.add(marine.add([Gear('Chainsword'), Gear('Bolt pistol')]).set_count(count))
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return 1 + (4 + self.pack.any) * (1 + self.marines.cur > 4)

    def build_points(self):
        return super(DACodexAssaultSquad, self).build_points() + self.pack.points * self.marines.cur


class DAInceptors(FastUnit, armory.DAUnit):
    type_name = get_name(units.InceptorSquad)
    type_id = 'da_inceptors_v1'

    keywords = ['Infantry', 'Primaris', 'Jump pack', 'Mk X gravis', 'Fly']

    power = 10

    @classmethod
    def calc_faction(cls):
        return super(DAInceptors, cls).calc_faction() + ['DEATHWATCH']

    class Weapons(OneOf):
        def __init__(self, parent):
            super(DAInceptors.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Two assault bolters', get_costs(*[ranged.AssaultBolter] * 2),
                         gear=create_gears(*[ranged.AssaultBolter] * 2))
            self.variant('Two plasma exterminators', get_costs(*[ranged.PlasmaExterminator] * 2),
                         gear=create_gears(*[ranged.PlasmaExterminator] * 2))

        @property
        def description(self):
            return [UnitDescription('Inceptor Sergeant').add(self.cur.gear)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Inceptor').add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(DAInceptors, self).__init__(parent, points=get_cost(units.InceptorSquad))
        self.wep = self.Weapons(self)
        self.models = self.Marines(self, 'Inceptors', 2, 5, points=get_cost(units.InceptorSquad),
                                   per_model=True)

    def get_count(self):
        return 1 + self.models.cur

    def build_points(self):
        return super(DAInceptors, self).build_points() + self.wep.points * self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))


class DASuppressors(FastUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.SuppressorSquad)
    type_id = 'da_supressors_v1'

    keywords = ['Infantry', 'Primaris', 'Jump pack', 'Fly']

    power = 5

    def __init__(self, parent):
        gear = [ranged.AcceleratorAutocannon, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.SuppressorSquad), *gear)
        super(DASuppressors, self).__init__(parent, name=get_name(units.SuppressorSquad),
                                           points=3 * cost,
                                           gear=[UnitDescription('Suppressor Sergeant', options=create_gears(*gear)),
                                                 UnitDescription('Suppressor', options=create_gears(*gear), count=2)],
                                           static=True)

    def get_count(self):
        return 3
