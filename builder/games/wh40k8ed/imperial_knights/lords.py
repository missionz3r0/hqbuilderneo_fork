from . import armory
from . import melee
from . import ranged
from . import units
from builder.core2 import UnitList
from builder.games.wh40k8ed.options import OneOf, OptionsList, ListSubUnit
from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.utils import *


class ArmigerWarglaives(LordsUnit, armory.ArmigerUnit):
    type_name = get_name(units.ArmigerWarglaive)
    kwname = 'Armiger Warglaive'
    type_id = 'ik_armigers_warglave_v1'
    power = 9

    class Warglaive(ListSubUnit):
        type_name = 'Armiger Warglaive'

        class Caparace(OneOf):
            def __init__(self, parent):
                super(ArmigerWarglaives.Warglaive.Caparace, self).__init__(parent, 'Caparace weapon')
                self.variant(*ranged.HeavyStubber)
                self.variant(*ranged.Meltagun)

        def __init__(self, parent):
            gear = [ranged.ThermalSpear, melee.ReaperChainCleaver]
            cost = points_price(get_cost(units.ArmigerWarglaive), *gear)
            super(ArmigerWarglaives.Warglaive, self).__init__(parent, points=cost,
                                                              gear=create_gears(*gear))
            self.Caparace(self)

    def __init__(self, parent):
        super(ArmigerWarglaives, self).__init__(parent)
        self.models = UnitList(self, self.Warglaive, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class ArmigerHelverins(LordsUnit, armory.ArmigerUnit):
    type_name = get_name(units.ArmigerHelverin)
    kwname = 'Armiger Helverine'
    type_id = 'ik_armigers_helverine_v1'
    power = 9

    class Helverine(ListSubUnit):
        type_name = 'Armiger Helverine'

        def __init__(self, parent):
            gear = [ranged.ArmigerAutocannon, ranged.ArmigerAutocannon]
            cost = points_price(get_cost(units.ArmigerHelverin), *gear)
            super(ArmigerHelverins.Helverine, self).__init__(parent, points=cost,
                                                             gear=create_gears(*gear))
            ArmigerWarglaives.Warglaive.Caparace(self)

    def __init__(self, parent):
        super(ArmigerHelverins, self).__init__(parent)
        self.models = UnitList(self, self.Helverine, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class KnightErrant(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(units.KnightErrant)
    type_id = 'kn_errant_v1'

    power = 22

    class CaparaceWeapons(OptionsList):
        def __init__(self, parent):
            super(KnightErrant.CaparaceWeapons, self).__init__(parent, 'Caparace Weapon',
                                                               limit=1)
            armory.add_caparace_weapons(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightErrant.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.ReaperChainsword)
            self.variant(*melee.ThunderstrikeGauntlet)

    class SecondaryWeapon(OneOf):
        def __init__(self, parent):
            super(KnightErrant.SecondaryWeapon, self).__init__(parent, 'Secondary weapon')
            self.variant(*ranged.HeavyStubber)
            self.variant(*ranged.Meltagun)

    def __init__(self, parent):
        gear = [ranged.ThermalCannon, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightErrant), *gear)
        super(KnightErrant, self).__init__(parent, get_name(units.KnightErrant), points=cost,
                                           gear=create_gears(*gear))
        self.Weapon1(self)
        self.SecondaryWeapon(self)
        self.CaparaceWeapons(self)


class KnightPaladin(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(units.KnightPaladin)
    type_id = 'kn_paladin_v1'

    power = 23

    def __init__(self, parent):
        gear = [ranged.RapidFireBattleCannon, ranged.HeavyStubber, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightPaladin), *gear)
        super(KnightPaladin, self).__init__(parent, get_name(units.KnightPaladin), points=cost,
                                            gear=create_gears(*gear))
        KnightErrant.Weapon1(self)
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class KnightWarden(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(units.KnightWarden)
    type_id = 'kn_warden_v1'

    power = 23

    def __init__(self, parent):
        gear = [ranged.AvengerGatlingCannon, ranged.HeavyFlamer, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightWarden), *gear)
        super(KnightWarden, self).__init__(parent, get_name(units.KnightWarden), points=cost,
                                           gear=create_gears(*gear))
        KnightErrant.Weapon1(self)
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class KnightGallant(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(units.KnightGallant)
    type_id = 'kn_gallant_v1'

    power = 20

    def __init__(self, parent):
        gear = [melee.ReaperChainsword, melee.ThunderstrikeGauntlet, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightPaladin), *gear)
        super(KnightGallant, self).__init__(parent, get_name(units.KnightGallant), points=cost,
                                            gear=create_gears(*gear))
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class KnightCrusader(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(units.KnightCrusader)
    type_id = 'kn_Crusader_v1'

    power = 25

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightCrusader.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.ThermalCannon)
            self.variant(join_and(ranged.RapidFireBattleCannon, ranged.HeavyStubber),
                         get_costs(ranged.RapidFireBattleCannon, ranged.HeavyStubber),
                         gear=create_gears(ranged.RapidFireBattleCannon, ranged.HeavyStubber))

    def __init__(self, parent):
        gear = [ranged.AvengerGatlingCannon, ranged.HeavyFlamer, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightCrusader), *gear)
        super(KnightCrusader, self).__init__(parent, get_name(units.KnightCrusader), points=cost,
                                             gear=create_gears(*gear))
        self.Weapon1(self)
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class KnightPreceptor(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(units.KnightPreceptor)
    type_id = 'ik_preceptor_v1'

    power = 23

    def __init__(self, parent):
        gear = [ranged.LasImpulsor, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightPreceptor), *gear)
        super(KnightPreceptor, self).__init__(parent, get_name(units.KnightPreceptor), points=cost,
                                              gear=create_gears(*gear))
        KnightErrant.Weapon1(self)
        KnightErrant.SecondaryWeapon(self)
        KnightErrant.CaparaceWeapons(self)


class CanisRex(LordsUnit, armory.QuestorisUnit):
    type_name = get_name(units.CanisRex)
    type_id = 'dw_artemis_v1'
    keywords = ['Character', 'Knight Preceptor']
    power = 23

    def __init__(self, parent):
        super(CanisRex, self).__init__(parent, points=get_cost(units.CanisRex), gear=[
            Gear('Las-Impulsor'), Gear('Multi laser'), Gear("Freedom's Hand"), Gear('Titanic feet')], static=True,
                                       unique=True)


class KnightCastellan(LordsUnit, armory.DominusUnit):
    type_name = get_name(units.KnightCastellan)
    type_id = 'ik_castellan_v1'

    power = 30

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightCastellan.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.TwinSiegebreakerCannon)
            self.variant('Two shieldbreaker missiles', 2 * get_cost(ranged.ShieldbreakerMissile),
                         gear=Gear('Shieldbreaker missile', count=2))

    def __init__(self, parent):
        gear = [ranged.PlasmaDecimator, ranged.VolcanoLance, ranged.ShieldbreakerMissile, ranged.ShieldbreakerMissile,
                ranged.TwinMeltagun, ranged.TwinMeltagun, ranged.TwinSiegebreakerCannon, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightCastellan), *gear)
        super(KnightCastellan, self).__init__(parent, get_name(units.KnightCastellan), points=cost,
                                              gear=create_gears(*gear))
        self.Weapon1(self)


class KnightValiant(LordsUnit, armory.DominusUnit):
    type_name = get_name(units.KnightValiant)
    type_id = 'ik_valiant_v1'

    power = 30

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightValiant.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Two shieldbreaker missiles', 2 * get_cost(ranged.ShieldbreakerMissile),
                         gear=Gear('Shieldbreaker missile', count=2))
            self.variant(*ranged.TwinSiegebreakerCannon)

    def __init__(self, parent):
        gear = [ranged.ThundercoilHarpoon, ranged.ConflagrationCannon, ranged.TwinSiegebreakerCannon,
                ranged.TwinMeltagun, ranged.TwinMeltagun, ranged.ShieldbreakerMissile, ranged.ShieldbreakerMissile,
                melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightValiant), *gear)
        super(KnightValiant, self).__init__(parent, get_name(units.KnightValiant), points=cost,
                                            gear=create_gears(*gear))
        self.Weapon1(self)
