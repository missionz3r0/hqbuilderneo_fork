from builder.core2 import OptionsList as CoreOptionsList,\
    OneOf as CoreOneOf, Count as CoreCount, Gear, SubUnit,\
    UnitDescription, ListSubUnit as CoreListSubUnit, UnitList,\
    OptionalSubUnit as CoreOptionalSubUnit


class OptionsList(CoreOptionsList):
    """
    For display purposes, set all options' cost to zero if power points are used
    """
    def variant(self, name, points=None, *args, **kwargs):
        used_points = None if self.parent.use_power else points
        return super(OptionsList, self).variant(name, used_points, *args, **kwargs)


class OptionalSubUnit(CoreOptionalSubUnit):
    class PowerOptionsList(CoreOptionalSubUnit.OptionsList):
        def __init__(self, parent, name, units, limit, order=0):
            pflag = parent.parent.parent.use_power
            super(OptionalSubUnit.OptionsList, self).__init__(parent=parent, name=name, limit=limit, order=order)
            self.opts = {self.Variant(self, sub_unit.unit_name, (sub_unit.base_points if not pflag else sub_unit.unit.power)): sub_unit
                         for sub_unit in units}

    def get_options_list(self):
        return self.PowerOptionsList

    @property
    def use_power(self):
        return self.parent.use_power


class OneOf(CoreOneOf):
    """
    For display purposes, zet displayed (or all) costs to zero if power points are used
    """
    def variant(self, name, points=None, *args, **kwargs):
        used_points = None if self.parent.use_power else points
        return super(OneOf, self).variant(name, used_points, *args, **kwargs)


class Count(CoreCount):
    """
    For display purposes, zet displayed (or all) costs to zero if power points are used
    """
    def __init__(self, parent, name, min_limit, max_limit, points=None, *args, **kwargs):
        used_points = None if parent.use_power else points
        super(Count, self).__init__(parent, name, min_limit, max_limit, used_points, *args, **kwargs)


class ListSubUnit(CoreListSubUnit):
    keywords = []
    power = 0

    @property
    def use_power(self):
        return self.root_unit.use_power

    def check_rules_chain(self):
        super(ListSubUnit, self).check_rules_chain()
        if self.use_power:
            self.points = 0
            self.description = self.build_description().dump()

    def __init__(self, parent, name=None, points=0, *args, **kwargs):
        if parent.parent.use_power:
            points = self.power
        super(ListSubUnit, self).__init__(parent, name, points, *args, **kwargs)
