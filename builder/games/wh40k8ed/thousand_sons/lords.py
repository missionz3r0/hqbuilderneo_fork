__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import LordsUnit, Unit
from builder.games.wh40k8ed.options import Gear, UnitDescription,\
    OptionsList, OneOf, ListSubUnit, SubUnit, UnitList
from . import armory
from . import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class Magnus(LordsUnit, armory.SonsUnit):
    type_name = get_name(units.MagnusTheRed)
    type_id = 'magnus_v1'

    keywords = ['Character', 'Monster', 'Daemon', 'Primarch', 'Fly', 'Psyker']

    power = 23

    def __init__(self, parent):
        super(Magnus, self).__init__(parent, unique=True, static=True, points=get_cost(units.MagnusTheRed),
                                     gear=[Gear('The Blade of Magnus')])
