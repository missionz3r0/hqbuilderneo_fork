__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory
from . import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class Mutalith(HeavyUnit, armory.SonsUnit):
    type_name = get_name(units.MutalithVortexBeast)
    type_id = 'mutalith_v1'

    power = 8

    def __init__(self, parent):
        super(Mutalith, self).__init__(parent, points=get_cost(units.MutalithVortexBeast),
                                       gear=create_gears(melee.EnormousClaws, melee.BetentacledMaw),
                                       static=True)
