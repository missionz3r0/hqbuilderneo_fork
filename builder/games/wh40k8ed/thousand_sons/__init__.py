from .hq import Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer,\
    TSTermoSorcerer
from .troops import LegionRubrics, Tzaangors
from .elites import Shaman, ScarabOccultTerminators
from .fast import Enlightened
from .heavy import Mutalith
from .lords import Magnus

unit_types = [Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer,
              TSTermoSorcerer, Shaman, ScarabOccultTerminators,
              LegionRubrics, Tzaangors, Enlightened, Mutalith, Magnus]
