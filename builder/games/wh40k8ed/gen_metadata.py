from .rosters import *
from . import necrons
from . import adepta_sororitas
from . import craftworld
from . import chaos_other
from . import heretic_astartes
from . import drukhari
from . import aeldari_other
from . import chaos_demons
from . import imperium_other
from . import astra_militarum
from . import adeptus_mechanicum
from . import tau
from . import space_marines
from . import blood_angels
from . import dark_angels
from . import space_wolves
from . import tyranids
from . import deathwatch
from . import orks
from . import grey_knights
from . import death_guard
from . import genestealer_cult
from . import adeptus_custodes
from . import thousand_sons
from . import imperial_knights

all_unit_types = necrons.unit_types + adepta_sororitas.unit_types +\
                 craftworld.unit_types + chaos_other.unit_types +\
                 heretic_astartes.unit_types + drukhari.unit_types +\
                 aeldari_other.unit_types + chaos_demons.unit_types +\
                 imperium_other.unit_types + astra_militarum.unit_types +\
                 adeptus_mechanicum.unit_types + tau.unit_types +\
                 space_marines.unit_types + blood_angels.unit_types +\
                 dark_angels.unit_types + space_wolves.unit_types +\
                 tyranids.unit_types + deathwatch.unit_types +\
                 grey_knights.unit_types + orks.unit_types +\
                 death_guard.unit_types + genestealer_cult.unit_types +\
                 adeptus_custodes.unit_types + imperial_knights.unit_types


alternate_factions = {
    '<DYNASTY>': ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NIHILAKH', 'NEPREKH', 'MAYNARKH'],
    '<CRAFTWORLD>': ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN'],
    '<LEGION>': ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN", 'THOUSAND SONS'],
    '<HAEMUNCULUS COVEN>': ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE'],
    '<WYCH CULT>': ['CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF'],
    '<REGIMENT>': ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN',
                   'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS'],
    '<FORGE WORLD>': ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA'],
    '<ORDO>': ['ORDO MALLEUS', 'ORDO HERETICUS', 'ORDO XENOS'],
    '<SEPT>': ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT", "KE'LSHAN SEPT"],
    '<CHAPTER>': ['ULTRAARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS', 'IRON HANDS', 'BLOOD RAVENS'],
    'BLOOD ANGELS': ['FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>'],
    'DARK ANGELS': ['<DARK ANGELS SUCCESSORS>'],
    '<CLAN>': ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ'],
    '<HIVE FLEET>': ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND',
                     'HYDRA', 'KRONOS', 'LEVIATHAN'],
    '<KABAL>': ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL',
                'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE'],
    '<HOUSEHOLD>': ['TERRIN', 'GRIFFIN', 'HAWKSHROUD', 'CADMUS', 'MORTAN', 'RAVEN', 'TARANIS', 'KRAST', 'VULKER'],
    '<ORDER>': ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE'],
    '<CULT>': ['CULT OF THE FOUR_ARMED EMPEROR', 'THE PAUPER PRINCES', 'THE HIVECULT', 'THE BLADED COG', 'THE RUSTED CLAW', 'THE TWISTED HELIX']
}

second_factions = {
    'ASTRA MILITARUM': ['<REGIMENT>', 'CADIAN',
                        'CATACHAN', 'VALHALLAN',
                        'VOSTROYAN', 'ARMAGEDDON',
                        'TALLARN', 'MORDIAN', 'BLOOD BROTHERS'],
    "T'AU EMPIRE": ['<SEPT>', "T'AU SEPT", 'FARSIGHT ENCLAVES',
                    "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT",
                    "BORK'AN SEPT", "KE'LSHAN SEPT"],
    'NECRONS': ['<DYNASTY>', 'SAUTEKH', 'MEPHRIT',
                'NOVOKH', 'NIHILAKH', 'NEPREKH', 'MAYNARKH'],
    'DRUKHARI': ['<OBSCESSION>', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF',
                 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE',
                 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL',
                 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE'],
    'YNNARI': ['<SUBFACTION>', 'DRUKHARI', 'ASURYANI', 'HARLEQUINS'],
    'IMPERIAL KNIGHTS': ['<HOUSEHOLD>', 'TERRIN', 'GRIFFIN', 'HAWKSHROUD', 'CADMUS',
                         'MORTAN', 'RAVEN', 'TARANIS', 'KRAST', 'VULKER'],
    'ORK': ['<CLAN>', 'GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ'],
    'ADEPTA SORORITAS': ['<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE'],
    'ADEPTUS MINISTORUM': ['<ORDER>', 'VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE'],
    'GENESTEALER CULTS': ['<CULT>', 'CULT OF THE FOUR_ARMED EMPEROR', 'THE PAUPER PRINCES', 'THE HIVECULT', 'THE BLADED COG', 'THE RUSTED CLAW', 'THE TWISTED HELIX']
}

complex_keywords = {
    'IMPERIUM': ['ADEPTUS CUSTODES', 'ADEPTUS ASTARTES', 'ASTRA TELEPATHICA', 'ADEPTUS MINISTORUM', 'ADEPTUS MECHANICUS', 'ASTRA MILITARUM', 'QUESTOR IMPERIALIS', 'QUESTOR MECHANICUS', 'INQUISITION', 'OFFICIO ASSASSINORUM'],
    'CHAOS': ['HERETIC ASTARTES', 'DAEMON', 'QUESTOR TRAITORIS'],
    'TYRANIDS': ['<HIVE FLEET>', 'GENESTEALER CULTS'],
    'AELDARI': ['DRUKHARI', 'ASURYANI', 'HARLEQUINS', 'YNNARI'],
    'ADEPTUS ASTARTES': ['<CHAPTER>', 'BLOOD ANGELS', 'DARK ANGELS', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', 'CRIMSON FISTS', 'WHITE SCARS', 'ULTRAMARINES', 'SPACE WOLVES', 'DEATHWATCH', 'GREY KNIGHTS', 'IMPERIAL FISTS', 'IRON HANDS', 'BLOOD RAVENS'],
    'NURGLE': ['DAEMON', 'HERETIC ASTARTES'],
    'KHORNE': ['DAEMON', 'HERETIC ASTARTES'],
    'TZEENTCH': ['DAEMON', 'HERETIC ASTARTES'],
    'SLAANESH': ['DAEMON', 'HERETIC ASTARTES'],
    'ASURYANI': ['ASPECT WARRIOR', 'WARHOST', 'SPIRIT HOST'],
    'DAEMON': ['TZEENTCH', 'KHORNE', 'SLAANESH', 'NURGLE'],
    'ADEPTUS MECHANICUS': ['CULT MECHANICUS', 'SKITARII', 'QUESTOR MECHANICUS'],
    'ASTRA MILITARUM': ['<REGIMENT>', 'CATACHAN', 'CADIAN', 'OFFICIO PREFECTUS', 'MILITARUM TEMPESTUS', 'ADEPTUS MECHANICUS', 'ADEPTUS MINISTORUM', 'SCHOLASTICA PSYKANA', 'AERONAUTICA IMPERIALIS', 'MILITARUM AUXILLA'],
    'YNNARI': ['DRUKHARI', 'ASURYANI', 'HARLEQUINS'],
    'DRUKHARI': ['<HAEMUNCULUS COVEN>', 'PROPHETS OF FLESH', '<KABAL>', '<WYCH CULT>', 'WYCH CULT OF STRIFE', 'INCUBI'],
    'HERETIC ASTARTES': ['<LEGION>', 'BLACK LEGION', 'WORLD EATERS', 'THOUSAND SONS', 'DEATH GUARD', "EMPEROR'S CHILDREN"],
    "T'AU EMPIRE": ['KROOT', 'VESPID', '<SEPT>', "T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT",
                    "DAL'YTH SEPT", "KE'LSHAN SEPT"],
    'NECRONS': ['CANOPTEK', "C'TAN SHARDS", '<DYNASTY>', 'SAUTEKH', 'MAYNARKH', 'NIHILAKH'],
    'ADEPTUS MINISTORUM': ['ADEPTA SORORITAS'],
    'GENESTEALER CULTS': ['BROOD BROTHERS']
}

exclusive_factions = {
    'KHORNE': ['SLAANESH', 'NURGLE', 'TZEENTCH', 'DEATH GUARD', 'THOUSAND SONS', "EMPEROR'S CHILDREN"],
    'SLAANESH': ['KHORNE', 'NURGLE', 'TZEENTCH', 'DEATH GUARD', 'THOUSAND SONS', 'WORLD EATERS'],
    'TZEENTCH': ['SLAANESH', 'NURGLE', 'KHORNE', 'DEATH GUARD', 'WORLD EATERS', "EMPEROR'S CHILDREN"],
    'NURGLE': ['SLAANESH', 'KHORNE', 'TZEENTCH', 'WORLD EATERS', 'THOUSAND SONS', "EMPEROR'S CHILDREN"],
    'THOUSAND SONS': ['SLAANESH', 'NURGLE', 'KHORNE', '<LEGION>'],
    'WORLD EATERS': ['SLAANESH', 'NURGLE', 'TZEENTCH'],
    'DEATH GUARD': ['SLAANESH', 'KHORNE', 'TZEENTCH', '<LEGION>'],
    '<LEGION>': ['DEATH GUARD', 'THOUSAND SONS'],
    "EMPEROR'S CHILDREN": ['KHORNE', 'NURGLE', 'TZEENTCH'],
    'BLOOD ANGELS': ['<CHAPTER>', 'DARK ANGELS', 'DEATHWING', 'SPACE WOLVES', 'DEATHWATCH'],
    '<CHAPTER>': ['BLOOD ANGELS', 'DARK ANGELS', 'DEATHWING', 'SPACE WOLVES', 'DEATHWATCH'],
    'DARK ANGELS': ['<CHAPTER>', 'BLOOD ANGELS', 'SPACE WOLVES', 'DEATHWATCH'],
    'DEATHWING': ['<CHAPTER>', 'BLOOD ANGELS', 'SPACE WOLVES', 'DEATHWATCH'],
    'SPACE WOLVES': ['<CHAPTER>', 'DARK ANGELS', 'DEATHWING', 'BLOOD ANGELS', 'DEATHWATCH'],
    'DEATHWATCH': ['<CHAPTER>', 'DARK ANGELS', 'DEATHWING', 'BLOOD ANGELS', 'SPACE WOLVES']
}

excluded_keywords = ['CHAOS', 'IMPERIUM', 'AELDARI', 'TYRANIDS', 'QUESTOR IMPERIALIS',
                     'QUESTOR MECHANICUS'] # YNNARI', 

detach_metadata = [
    {'Base': DetachPatrol,
     'Sections': {'hq': True, 'elite': False,
                  'troops': True, 'fast': False,
                  'heavy': False, 'fliers': False,
                  'transports': False}},
    {'Base': DetachBatallion,
     'Sections': {'hq': True, 'elite': False,
                  'troops': True, 'fast': False,
                  'heavy': False, 'fliers': False,
                  'transports': False}},
    {'Base': DetachBrigade,
     'Sections': {'hq': True, 'elite': True,
                  'troops': True, 'fast': True,
                  'heavy': True, 'fliers': False,
                  'transports': False}},
    {'Base': DetachVanguard,
     'Sections': {'hq': True, 'elite': True,
                  'troops': False, 'fast': False,
                  'heavy': False, 'fliers': False,
                  'transports': False}},
    {'Base': DetachSpearhead,
     'Sections': {'hq': True, 'elite': False,
                  'troops': False, 'fast': False,
                  'heavy': True, 'fliers': False,
                  'transports': False}},
    {'Base': DetachOutrider,
     'Sections': {'hq': True, 'elite': False,
                  'troops': False, 'fast': True,
                  'heavy': False, 'fliers': False,
                  'transports': False}},
    {'Base': DetachCommand,
     'Sections': {'hq': True, 'elite': False,
                  'lords': False, 'transports': False}},
    {'Base': DetachSuperHeavy,
     'Sections': {'lords': True}},
    {'Base': DetachSuperHeavyAux,
     'Sections': {'lords': True}},
    {'Base': DetachAirWing,
     'Sections': {'fliers': True}},
    {'Base': DetachFort,
     'Sections': {'fort': True}},
    {'Base': DetachAuxilary,
     'Sections': {'hq': False, 'elite': False,
                  'troops': False, 'fast': False,
                  'heavy': False, 'fliers': False,
                  'transports': False}}
]

detach_metadata_mission = [
    {'Mission': 'Planetstrike (Attacker)',
     'Base': Wh8edPlanetstrikeAttacker,
     'Detachments': [
         {'Base': DetachPlanetstrikeAttacker,
          'Sections': {'hq': True, 'elite': True,
                       'troops': False, 'fast': True,
                       'heavy': False, 'fliers': False,
                       'transports': False}}
     ]},
    {'Mission': 'Planetstrike (Defender)',
     'Base': Wh8edPlanetstrikeDefender,
     'Detachments': [
         {'Base': DetachPlanetstrikeDefender,
          'Sections': {'hq': True, 'elite': False,
                       'troops': True, 'fast': False,
                       'heavy': True, 'fliers': False,
                       'transports': False}},
         {'Base': DetachFortPlanetstrike,
          'Sections': {'fort': True}}
     ]}
]
