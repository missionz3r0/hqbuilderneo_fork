__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import LordsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory
from . import units
from builder.games.wh40k8ed.utils import *


class Mortarion(LordsUnit, armory.GuardUnit):
    type_name = get_name(units.Mortarion)
    type_id = 'mortarion_v1'
    keywords = ['Monster', 'Character', 'Daemon', 'Primarch', 'Psyker', 'Fly']
    power = 24

    def __init__(self, parent):
        super(Mortarion, self).__init__(parent, points=get_cost(units.Mortarion),
                                        gear=[Gear('Silence'), Gear('The Lantern'),
                                              Gear('Phosphex bombs')],
                                        static=True, unique=True)

