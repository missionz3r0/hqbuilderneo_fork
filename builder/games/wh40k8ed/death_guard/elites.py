__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory
from . import ranged, melee, units
from builder.games.wh40k8ed.utils import *


class Blightbringer(ElitesUnit, armory.GuardUnit):
    type_name = get_name(units.NoxiousBlightbringer)
    type_id = 'blightbringer_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        gear = [ranged.BlightGrenades, ranged.KrakGrenades,
                melee.CursedPlagueBell, ranged.PlasmaPistol]
        cost = points_price(get_cost(units.NoxiousBlightbringer), *gear)
        super(Blightbringer, self).__init__(parent=parent, points=cost,
                                            gear=create_gears(*gear), static=True)


class Blightspawn(ElitesUnit, armory.GuardUnit):
    type_name = get_name(units.FoulBlightspawn)
    type_id = 'blightspawn_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        gear = [ranged.BlightGrenades, ranged.KrakGrenades,
                ranged.PlagueSprayer]
        cost = points_price(get_cost(units.FoulBlightspawn), *gear)
        super(Blightspawn, self).__init__(parent=parent, points=cost,
                                            gear=create_gears(*gear), static=True)


class Putrifier(ElitesUnit, armory.GuardUnit):
    type_name = get_name(units.BiologusPutrifier)
    type_id = 'putrifier_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        gear = [ranged.HyperBlightGrenades, ranged.KrakGrenades,
                melee.PlagueKnife, ranged.InjectorPistol]
        cost = points_price(get_cost(units.BiologusPutrifier), *gear)
        super(Putrifier, self).__init__(parent=parent, points=cost,
                                        gear=create_gears(*gear), static=True)


class PlagueSurgeon(ElitesUnit, armory.GuardUnit):
    type_name = get_name(units.PlagueSurgeon)
    type_id = 'plag_surgeon_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        gear = [ranged.BlightGrenades, ranged.KrakGrenades,
                melee.Balesword, ranged.BoltPistol]
        cost = points_price(get_cost(units.PlagueSurgeon), *gear)
        super(PlagueSurgeon, self).__init__(parent=parent, points=cost,
                                            gear=create_gears(*gear), static=True)


class Tallyman(ElitesUnit, armory.GuardUnit):
    type_name = get_name(units.Tallyman)
    type_id = 'tallyman_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        gear = [ranged.BlightGrenades, ranged.KrakGrenades,
                ranged.PlasmaPistol]
        cost = points_price(get_cost(units.Tallyman), *gear)
        super(Tallyman, self).__init__(parent=parent, points=cost,
                                       gear=create_gears(*gear), static=True)


class Deathshroud(ElitesUnit, armory.GuardUnit):
    type_name = get_name(units.DeathshroudTerminators)
    type_id = 'deathshroud_v1'
    kwname = 'Deathshroud'
    keywords = ['Infantry', 'Terminator']
    power = 11

    model_gear = [melee.Manreaper, ranged.PlaguespurtGauntlet]
    model_cost = points_price(get_cost(units.DeathshroudTerminators),
                              *model_gear)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Deathshroud.Weapon, self).__init__(parent, 'Champion extra weapon')
            self.variant(*ranged.PlaguespurtGauntlet)

        @property
        def description(self):
            return [UnitDescription('Deathshroud Champion',
                                    options=create_gears(*Deathshroud.model_gear)).
                    add(super(Deathshroud.Weapon, self).description)]

    def __init__(self, parent):
        super(Deathshroud, self).__init__(parent, points=self.model_cost)
        self.Weapon(self)
        self.models = Count(self, 'Deathshroud terminators', 2, 5,
                            self.model_cost, per_model=True,
                            gear=UnitDescription('Deathshroud Terminator',
                                                 options=create_gears(*Deathshroud.model_gear)))

    def get_count(self):
        return 1 + self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))


class BlightlordTerminators(ElitesUnit, armory.GuardUnit):
    type_name = get_name(units.BlightlordTerminators)
    type_id = 'blightlords_v1'
    keywords = ['Infantry', 'Terminator']
    power = 14

    class Champion(Unit):
        type_name = 'Blightlord Champion'

        class Melee(OneOf):
            def __init__(self, parent):
                super(BlightlordTerminators.Champion.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Balesword)
                self.variant(*melee.BuboticAxe)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(BlightlordTerminators.Champion.Ranged, self).__init__(parent, 'Ranged weapon')
                armory.add_combi_weapons(self)

        def __init__(self, parent):
            super(BlightlordTerminators.Champion, self).__init__(parent, points=get_cost(units.BlightlordTerminators))
            self.Melee(self)
            self.Ranged(self)

    class Terminator(ListSubUnit):
        type_name = 'Blightlord Terminator'

        class Melee(OneOf):
            def __init__(self, parent):
                super(BlightlordTerminators.Terminator.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Balesword)
                self.variant(*melee.BuboticAxe)
                self.flail = self.variant(*melee.FlailOfCorruption)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(BlightlordTerminators.Terminator.Ranged, self).__init__(parent, 'Ranged weapon')
                armory.add_combi_weapons(self)
                self.heavy = [
                    self.variant(*ranged.PlagueSpewer),
                    self.variant(*ranged.ReaperAutocannon),
                    self.variant(*ranged.BlightLauncher)
                ]

        def __init__(self, parent):
            super(BlightlordTerminators.Terminator, self).__init__(parent, points=get_cost(units.BlightlordTerminators))
            self.mle = self.Melee(self)
            self.rng = self.Ranged(self)

        def check_rules(self):
            super(BlightlordTerminators.Terminator, self).check_rules()
            self.rng.used = self.rng.visible = self.mle.cur != self.mle.flail

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.rng.used and self.rng.cur in self.rng.heavy

        @ListSubUnit.count_gear
        def has_flail(self):
            return self.mle.cur == self.mle.flail

    def __init__(self, parent):
        super(BlightlordTerminators, self).__init__(parent)
        SubUnit(self, self.Champion(self))
        self.models = UnitList(self, self.Terminator, 4, 9)

    def check_rules(self):
        super(BlightlordTerminators, self).check_rules()
        hcnt = sum(u.has_heavy() for u in self.models.units)
        if hcnt > self.get_count() / 5:
            self.error('Only one Blightlord Terminator per five models may take heavy weapon; taken: {}'.format(hcnt))
        fcnt = sum(u.has_flail() for u in self.models.units)
        if fcnt > self.get_count() / 5:
            self.error('Only one Blightlord Terminator per five models may take flail; taken: {}'.format(fcnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + 13 * (self.models.count > 4)
