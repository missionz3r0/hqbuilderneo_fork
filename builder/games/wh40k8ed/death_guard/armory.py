__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList
from builder.games.wh40k8ed.heretic_astartes.armory import AstartesUnit
from . import melee, ranged


def add_melee_weapons(instance):
    instance.variant(*melee.Balesword)
    instance.variant(*melee.Chainaxe)
    instance.claw = instance.variant(*melee.LightningClaws)
    instance.variant(*melee.PowerAxe)
    instance.variant(*melee.PowerFist)
    instance.variant(*melee.PowerMaul)
    instance.variant(*melee.PowerSword)


def add_combi_weapons(instance):
    instance.combi = [instance.variant(*ranged.CombiBolter),
                      instance.variant(*ranged.CombiFlamer),
                      instance.variant(*ranged.CombiMelta),
                      instance.variant(*ranged.CombiPlasma)]


def add_terminator_melee(instance, sword=True):
    instance.variant(*melee.Balesword)
    instance.variant(*melee.Chainfist)
    instance.claw = instance.variant(*melee.LightningClaws)
    instance.variant(*melee.PowerAxe)
    instance.variant(*melee.PowerFist)
    instance.variant(*melee.PowerMaul)
    if sword:
        instance.variant(*melee.PowerSword)


class ClawUser(Unit):
    def build_points(self):
        res = super(ClawUser, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 4
        return res    

class GuardUnit(AstartesUnit):
    faction = ['Nurgle', 'Death Guard']
    wiki_faction = "Death Guard"
