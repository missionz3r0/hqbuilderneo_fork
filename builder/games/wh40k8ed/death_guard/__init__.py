from .hq import DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince,\
    ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster
from .troops import DGPlagueMarinesV2, Poxwalkers
from .elites import Blightbringer, Blightspawn, Putrifier, PlagueSurgeon,\
    Tallyman, Deathshroud, BlightlordTerminators
from .fast import BloatDrone, BlightHaulers
from .heavy import PlagueCrawler
from .lords import Mortarion


unit_types = [DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince,
              ContagionLord, DGSorcerer, DGTermoSorcerer,
              Plaguecaster, DGPlagueMarinesV2, Poxwalkers,
              Blightbringer, Blightspawn, Putrifier, PlagueSurgeon,
              Tallyman, Deathshroud, BlightlordTerminators,
              BloatDrone, BlightHaulers, PlagueCrawler, Mortarion]
