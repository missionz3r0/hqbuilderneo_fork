AutoLaunchers = ('Auto launchers', 0)
AuxilaryGrenadeLauncher = ('Auxilary grenade launcher', 1)
BlizzardShield = ('Blizzard shield', 15)
CamoCloak = ('Camo cloak', 3)
GrapnelLauncher = ('Grapnel launcher', 2)
GravChute = ('Grav-chute', 2)
PsychicHood = ('Psychic hood', 5)
RunicArmour = ('Runic armour', 7)
RunicTerminatorArmour = ('Runic Terminator armour', 5)
StormShieldCavalry = ('Storm shield', 10)
StormShieldCharacter = ('Storm shield', 10)
StormShield = ('Storm shield', 2)
WolfStandard = ('Wolf standard', 10)
TeleportHomer = ('Teleport homer', 0)
InfiltratorCommsArray = ('Infiltrator comms array', 10)
