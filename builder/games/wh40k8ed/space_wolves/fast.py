from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList, OptionalSubUnit
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Skyclaws(FastUnit, armory.SWUnit):
    type_name = "Skyclaws"
    type_id = "skyclaws__v1"
    keywords = ['Infantry', 'Blood claw', 'Jump pack', 'Fly']
    model_gear = [Gear('Frag grenades'),
                  Gear('Krak grenades')]
    model = UnitDescription('Skyclaw', options=model_gear + [Gear('Chainsword')])
    model_points = get_cost(units.Skyclaws)

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(Skyclaws.WeaponRanged, self).__init__(parent, 'Ranged weapon')
            self.bp = self.variant('Bolt pistol')
            self.variant(*ranged.PlasmaPistol)
            armory.add_special_weapons(self)

    class Leader(Unit):
        type_name = 'Skyclaw Pack leader'

        class WeaponMelee(OneOf):
            def __init__(self, parent):
                super(Skyclaws.Leader.WeaponMelee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Chainsword)
                self.variant(*melee.PowerAxe)
                self.variant(*melee.PowerFist)
                self.variant(*melee.PowerSword)
                # armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(Skyclaws.Leader, self).__init__(
                parent=parent, gear=Skyclaws.model_gear, points=Skyclaws.model_points)
            self.wep1 = self.WeaponMelee(self)
            self.wep2 = Skyclaws.WeaponRanged(self)

        def has_spec(self):
            return self.wep2.cur != self.wep2.bp

    class SkyWolfGuard(armory.WolfClawUser, Unit):
        type_name = 'Wolf Guard Sky Leader'
        power = 2

        class WeaponMelee(OneOf):
            def __init__(self, parent):
                super(Skyclaws.SkyWolfGuard.WeaponMelee, self).__init__(parent, 'Melee weapon')
                self.variant('Chainsword')
                self.variant(*ranged.PlasmaPistol)
                self.variant(*wargear.StormShield)
                armory.add_melee_weapons(self, chain=False)

        class WeaponRanged(OneOf):
            def __init__(self, parent):
                super(Skyclaws.SkyWolfGuard.WeaponRanged, self).__init__(parent, 'Ranged weapon')
                self.bp = self.variant('Bolt pistol')
                self.variant(*ranged.PlasmaPistol)
                armory.add_melee_weapons(self)
                armory.add_combi_weapons(self)

        def __init__(self, parent):
            super(Skyclaws.SkyWolfGuard, self).__init__(
                parent, gear=Skyclaws.model_gear, points=get_cost(units.WolfGuardSkyLeader))
            self.wep1 = self.WeaponRanged(self)
            self.wep2 = self.WeaponMelee(self)

    class Uncle(OptionalSubUnit):
        def __init__(self, parent):
            super(Skyclaws.Uncle, self).__init__(parent, 'Wolf Guard Leader')
            SubUnit(self, Skyclaws.SkyWolfGuard(parent=parent))

    def __init__(self, parent):
        super(Skyclaws, self).__init__(parent)
        self.ldr = SubUnit(self, self.Leader(parent=self))
        self.marines = Count(self, 'Skyclaws', 4, 9, Skyclaws.model_points, per_model=True)
        self.swep = [self.WeaponRanged(self) for i in range(0, 2)]
        self.wg = self.Uncle(self)

    def check_rules(self):
        super(Skyclaws, self).check_rules()
        self.swep[1].used = self.swep[1].visible = not self.ldr.unit.has_spec()

    def get_count(self):
        return self.marines.cur + 1 + self.wg.count

    def build_power(self):
        return 5 + 4 * (self.marines.cur > 4) + self.SkyWolfGuard.power * self.wg.count

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        res.add(self.ldr.description)
        delta = sum(w.used for w in self.swep)
        res.add(self.model.clone().add(Gear('Bolt pistol')).set_count(self.marines.cur - delta))
        for w in self.swep:
            if w.used:
                res.add_dup(self.model.clone().add(w.description))
        res.add(self.wg.description)
        return res


class Swiftclaws(FastUnit, armory.SWUnit):
    type_name = "Swiftclaws"
    type_id = "swiftclaws_v1"
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.TwinBoltgun]
    keywords = ['Biker', 'Blood claw']
    model_points = points_price(get_cost(units.Swiftclaws), *model_gear)
    power = 4

    class Marine(ListSubUnit):
        type_name = 'Swiftclaw'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Swiftclaws.Marine.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Bolt pistol')
                self.variant('Chainsword')
                self.ppist = self.variant(*ranged.PlasmaPistol)
                armory.add_special_weapons(self)

        def __init__(self, parent):
            super(Swiftclaws.Marine, self).__init__(
                parent=parent, gear=create_gears(*Swiftclaws.model_gear),
                points=Swiftclaws.model_points)
            self.wep1 = self.Weapon(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep1.cur in self.wep1.spec or self.wep1.cur == self.wep1.ppist

    class AttackBike(Unit):
        type_name = 'Swiftclaw Attack Bike'
        model_points = points_price(get_cost(units.SwiftclawAttackBikes), ranged.TwinBoltgun)
        power = 3

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Swiftclaws.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant(*ranged.HeavyBolter)
                self.multimelta = self.variant(*ranged.MultiMelta)

        def __init__(self, parent):
            super(Swiftclaws.AttackBike, self).__init__(
                parent=parent, points=self.model_points,
                gear=[Gear('Twin boltgun'), UnitDescription('Swiftclaw', count=2,
                                                            options=create_gears(*Swiftclaws.model_gear) + [Gear('Bolt pistol')])])
            self.wep = self.Weapon(self)

    class WolfGuardBike(Unit):
        type_name = 'Wolf Guard Bike Leader'
        power = 2

        class WeaponRanged(OneOf):
            def __init__(self, parent):
                super(Swiftclaws.WolfGuardBike.WeaponRanged, self).__init__(parent, 'Weapon')
                self.variant('Bolt pistol')
                self.variant(*ranged.PlasmaPistol)
                self.variant(*wargear.StormShield)
                armory.add_melee_weapons(self)
                armory.add_combi_weapons(self)

        def __init__(self, parent):
            super(Swiftclaws.WolfGuardBike, self).__init__(
                parent, gear=create_gears(*Swiftclaws.model_gear),
                points=points_price(get_cost(units.WolfGuardBikeLeader), *Swiftclaws.model_gear))
            self.WeaponRanged(self)

    class Leader(Unit):
        type_name = 'Swiftclaw Pack Leader'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Swiftclaws.Leader.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Bolt pistol')
                self.variant('Chainsword')
                self.ppist = self.variant(*ranged.PlasmaPistol)
                armory.add_special_weapons(self)
                self.variant(*melee.PowerAxe)
                self.variant(*melee.PowerFist)
                self.variant(*melee.PowerSword)
                # armory.add_melee_weapons(self, chain=False)

        def __init__(self, parent):
            super(Swiftclaws.Leader, self).__init__(
                parent=parent, gear=create_gears(*Swiftclaws.model_gear), points=Swiftclaws.model_points)
            self.wep1 = self.Weapon(self)

        def has_spec(self):
            return self.wep1.cur in self.wep1.spec or self.wep1.cur == self.wep1.ppist

    class OptUnits(OptionalSubUnit):
        def __init__(self, parent):
            super(Swiftclaws.OptUnits, self).__init__(parent=parent, name='')
            self.attackbike = SubUnit(self, Swiftclaws.AttackBike(parent=parent))
            self.wg = SubUnit(self, Swiftclaws.WolfGuardBike(parent=parent))

    def __init__(self, parent):
        super(Swiftclaws, self).__init__(parent)
        self.ldr = SubUnit(self, self.Leader(parent=self))
        self.marines = UnitList(self, self.Marine, 2, 14)
        self.other = self.OptUnits(self)

    def check_rules(self):
        super(Swiftclaws, self).check_rules()

        if sum(u.has_spec() for u in self.marines.units) + self.ldr.unit.has_spec() > 1:
            self.error('Only one Swiftclaw or Swiftclaw Pack Leader may upgrade his bolt pistol to plasma pistol or a special weapon')

    def get_count(self):
        return self.marines.count + 1 + self.other.count

    def build_power(self):
        return self.power * ((self.marines.count + 3) / 3) +\
            sum(sunit.unit.power for sunit in self.other.units if sunit.used)


class SwiftclawAttackBikes(FastUnit, armory.SWUnit):
    type_name = 'Swiftclaw Attack Bikes'
    type_id = 'swiftclaw_attack_v1'
    keywords = ['Biker', 'Blood claw']
    power = 3
    model_points = points_price(get_cost(units.SwiftclawAttackBikes), ranged.TwinBoltgun)
    model = UnitDescription('Swiftclaw Attack Bike', options=[
        Gear('Twin boltgun'), UnitDescription('Swiftclaw', count=2,
                                              options=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                       Gear('Bolt pistol')])])

    class Biker(Count):
        @property
        def description(self):
            return[SwiftclawAttackBikes.model.clone().add(Gear('Heavy bolter'))
                   .set_count(self.cur - self.parent.melta.cur)]

    def __init__(self, parent):
        super(SwiftclawAttackBikes, self).__init__(parent)
        self.bikers = self.Biker(self, 'Swiftclaw Attack bikes', 1, 3, self.model_points, per_model=True)
        self.melta = Count(self, 'Multi-melta', 0, 1, get_cost(ranged.MultiMelta),
                           gear=SwiftclawAttackBikes.model.clone().add(Gear('Multi-melta')))

    def check_rules(self):
        super(SwiftclawAttackBikes, self).check_rules()
        self.melta.max = self.bikers.cur

    def get_count(self):
        return self.bikers.cur

    def build_power(self):
        return self.power * self.bikers.cur

    def build_points(self):
        # take HB into account
        return super(SwiftclawAttackBikes, self).build_points() + get_cost(ranged.HeavyBolter) * (self.bikers.cur - self.melta.cur)


class SWLandSpeeders(FastUnit, armory.SWUnit):
    type_name = get_name(units.LandSpeeders)
    type_id = 'sw_land_speeders_1'
    keywords = ['Vehicle', 'Fly']
    power = 6

    class LandSpeeder(ListSubUnit):
        type_name = 'Land Speeder'

        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(SWLandSpeeders.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant(*ranged.HeavyBolter)
                self.multimelta = self.variant(*ranged.MultiMelta)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(SWLandSpeeders.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Optional weapon', limit=1)
                self.assaultcannon = self.variant(*ranged.AssaultCannon)
                self.heavyflamer = self.variant(*ranged.HeavyFlamer)
                self.typhoonmissilelauncher = self.variant(*ranged.TyphoonMissileLauncher)

        def __init__(self, parent):
            super(SWLandSpeeders.LandSpeeder, self).__init__(parent=parent, points=get_cost(units.LandSpeeders))
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

    def __init__(self, parent):
        super(SWLandSpeeders, self).__init__(parent, 'Land Speeders')
        self.models = UnitList(self, self.LandSpeeder, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class SWInceptors(FastUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Inceptors)
    type_id = 'sw_inceptors_v1'

    keywords = ['Infantry', 'Primaris', 'Jump pack', 'Mk X gravis', 'Fly']

    power = 10

    class Weapons(OneOf):
        def __init__(self, parent):
            super(SWInceptors.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Two assault bolters', get_costs(*[ranged.AssaultBolter] * 2),
                         gear=create_gears(*[ranged.AssaultBolter] * 2))
            self.variant('Two plasma exterminators', get_costs(*[ranged.PlasmaExterminator] * 2),
                         gear=create_gears(*[ranged.PlasmaExterminator] * 2))

        @property
        def description(self):
            return [UnitDescription('Inceptor Pack Leader').add(self.cur.gear)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Inceptor').add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(SWInceptors, self).__init__(parent, get_name(units.Inceptors), points=get_cost(units.Inceptors))
        self.wep = self.Weapons(self)
        self.models = self.Marines(self, 'Inceptors', 2, 5, points=get_cost(units.Inceptors),
                                   per_model=True)

    def get_count(self):
        return 1 + self.models.cur

    def build_points(self):
        return super(SWInceptors, self).build_points() + self.wep.points * self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))


class ThunderwolfCavalry(FastUnit, armory.SWUnit):
    type_name = 'Thunderwolf Cavalry'
    type_id = 'thunderwolf_calvalry_v1'
    keywords = ['Cavalry', 'Wolf Guard']
    power = 8

    class Leader(armory.WolfClawUser, Unit):
        type_name = 'Thunderwolf Cavalry Pack Leader'

        class MeleeWeapon(OneOf):
            def __init__(self, parent):
                super(ThunderwolfCavalry.Leader.MeleeWeapon, self).__init__(parent, 'Melee weapon')
                self.variant('Chainsword')
                self.variant(*wargear.StormShieldCavalry)
                armory.add_melee_weapons(self, chain=False)

        class RangedWeapon(OneOf):
            def __init__(self, parent):
                super(ThunderwolfCavalry.Leader.RangedWeapon, self).__init__(parent, 'Ranged weapon')
                self.variant('Bolt pistol')
                self.variant('Boltgun')
                self.variant(*ranged.PlasmaPistol)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(ThunderwolfCavalry.Leader, self).__init__(
                parent, self.type_name, gear=[
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    UnitDescription('Thunderwolf', options=[Gear('Crushing teeth and claws')])
                ], points=get_cost(units.ThunderwolfCavalry)
            )
            self.wep1 = self.MeleeWeapon(self)
            self.wep2 = self.RangedWeapon(self)

    class Marine(Leader, ListSubUnit):
        type_name = 'Thunderwolf Cavalry'

    def __init__(self, parent):
        super(ThunderwolfCavalry, self).__init__(parent, self.type_name)
        SubUnit(self, self.Leader(parent=self))
        self.marines = UnitList(self, self.Marine, 2, 5)

    def get_count(self):
        return 1 + self.marines.count

    def build_power(self):
        return self.power * (1 + (self.marines.count > 2))


class WolfPack(FastUnit, armory.SWUnit):
    type_name = "Fenrisian Wolfes"
    type_id = "fenrisian_wolf_pack_v1"
    keywords = ['Beast']
    power = 2

    def __init__(self, parent):
        super(WolfPack, self).__init__(parent)
        self.wolves = Count(
            self, 'Fenrisian Wolf', 5, 15, get_cost(units.FenrisianWolves),
            gear=UnitDescription('Fenrisian Wolf', options=[Gear('Teeth and claws')])
        )
        self.opt = self.Options(self)

    def check_rules(self):
        super(WolfPack, self).check_rules()
        Count.norm_counts(5 - self.opt.any, 15 - self.opt.any, [self.wolves])

    class Options(OptionsList):
        def __init__(self, parent):
            super(WolfPack.Options, self).__init__(parent=parent, name='Options')
            self.variant('Cyberwolf', get_cost(units.Cyberwolves),
                         gear=UnitDescription('Cyberwolf', options=[Gear('Teeth and claws')]))

    def get_count(self):
        return self.wolves.cur + self.opt.count

    def build_power(self):
        return self.power * ((self.wolves.cur + 4) / 5) + self.opt.any


class WolfScoutBikers(FastUnit, armory.SWUnit):
    type_name = get_name(units.WolfScoutBikers)
    type_id = 'sw_scout_bike_squad_v1'
    keywords = ['Biker', 'Scout']

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.AstartesShotgun, melee.CombatKnife]
    model_points = get_cost(units.WolfScoutBikers)
    power = 5

    class Sergeant(Unit):
        type_name = 'Wolf Scout Biker Pack Leader'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(WolfScoutBikers.Sergeant.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.BoltPistol)
                self.variant(*ranged.PlasmaPistol)
                self.variant(*melee.PowerAxe)
                self.variant(*melee.PowerSword)

        def __init__(self, parent):
            gear = WolfScoutBikers.model_gear + []
            super(WolfScoutBikers.Sergeant, self).__init__(
                parent=parent, points=points_price(WolfScoutBikers.model_points, *gear),
                gear=create_gears(*gear))
            self.wep = self.Weapon(self)

    class Bikers(Count):
        @property
        def description(self):
            return [UnitDescription('Wolf Scout Biker', options=create_gears(WolfScoutBikers.model_gear) + [
                Gear('Bolt pistol'), Gear('Twin boltgun')
            ]).set_count(self.cur - self.parent.grenade.cur)]

    def __init__(self, parent):
        super(WolfScoutBikers, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))

        self.bikers = self.Bikers(self, 'Wolf Scout Bikers', 2, 8, self.model_points, per_model=True)
        self.grenade = Count(self, 'Astartes grenade launcher', 0, 3, get_cost(ranged.AstartesGrenadeLauncher),
                             gear=UnitDescription('Scout Biker', options=create_gears(WolfScoutBikers.model_gear) + [
                                 Gear('Bolt pistol'), Gear('Astartes grenade launcher')
                             ]))

    def get_count(self):
        return self.bikers.cur + 1

    def check_rules(self):
        super(WolfScoutBikers, self).check_rules()
        self.grenade.max = min(self.bikers.cur, 3)

    def build_power(self):
        return self.power * (self.bikers.cur + 3) / 3

    def build_points(self):
        # take twin boltgun costs into account
        return super(WolfScoutBikers, self).build_points() + get_cost(ranged.TwinBoltgun) * (self.bikers.cur - self.grenade.cur)


class Cyberwolves(FastUnit, armory.SWUnit):
    type_name = 'Cyberwolves'
    type_id = 'cwolves_v1'
    keywords = ['Beast']

    def __init__(self, parent):
        super(Cyberwolves, self).__init__(parent)
        self.models = Count(self, 'Cyberwolves', 1, 5, get_cost(units.Cyberwolves), per_model=True,
                            gear=UnitDescription('Cyberwolf', options=[Gear('Teeth and claws')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.models.cur


class SWSuppressors(FastUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.SuppressorSquad)
    type_id = 'sw_supressors_v1'

    keywords = ['Infantry', 'Primaris', 'Jump pack', 'Fly']

    power = 5

    def __init__(self, parent):
        gear = [ranged.AcceleratorAutocannon, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.SuppressorSquad), *gear)
        super(SWSuppressors, self).__init__(parent, name=get_name(units.SuppressorSquad),
                                           points=3 * cost,
                                           gear=[UnitDescription('Suppressor Sergeant', options=create_gears(*gear)),
                                                 UnitDescription('Suppressor', options=create_gears(*gear), count=2)],
                                           static=True)

    def get_count(self):
        return 3
