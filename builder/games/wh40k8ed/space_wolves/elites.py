from builder.games.wh40k8ed.space_marines.elites import CompanyAncient
from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList, OptionalSubUnit
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *
from .troops import WGPackLeader


class WolfScouts(ElitesUnit, armory.SWUnit):
    type_name = get_name(units.WolfScouts)
    type_id = 'sw_scouts_v1'
    keywords = ['Infantry', 'Scout']
    model_points = 11

    class Scout(ListSubUnit):
        type_name = 'Wolf Scout'
        model_gear = [Gear('Frag grenades'),
                      Gear('Krak grenades')]

        class Bolter(OneOf):
            def __init__(self, parent):
                super(WolfScouts.Scout.Bolter, self).__init__(parent, 'Gun')
                self.ranged = [self.variant('Boltgun'),
                               self.variant('Chainsword'),
                               self.variant('Astartes shotgun', 0),
                               self.variant('Combat knife', 0),
                               self.variant(*ranged.SniperRifle)]
                self.heavy = [self.variant(*ranged.HeavyBolter),
                              self.variant(*ranged.MissileLauncher)]
                armory.add_special_weapons(self)
                self.ranged += self.heavy + self.spec
                self.variant(*melee.PowerAxe)
                self.variant(*melee.PowerSword)

        class Pistol(OneOf):
            def __init__(self, parent):
                super(WolfScouts.Scout.Pistol, self).__init__(parent, 'Pistol')
                self.variant('Bolt pistol')
                self.ppist = self.variant(*ranged.PlasmaPistol)

        class Cloak(OptionsList):
            def __init__(self, parent):
                super(WolfScouts.Scout.Cloak, self).__init__(parent, 'Options')
                self.variant(*wargear.CamoCloak)

        def __init__(self, parent):
            super(WolfScouts.Scout, self).__init__(parent, points=WolfScouts.model_points,
                                                   gear=self.model_gear)
            self.wep1 = self.Bolter(self)
            self.wep2 = self.Pistol(self)
            self.Cloak(self)

        def check_rules(self):
            super(WolfScouts.Scout, self).check_rules()
            self.wep2.ppist.active = self.wep1.cur in self.wep1.ranged

        @ListSubUnit.count_gear
        def has_plasma_power(self):
            return self.wep2.cur == self.wep2.ppist\
                or self.wep1.cur not in self.wep1.ranged

        @ListSubUnit.count_gear
        def has_heavy_special(self):
            return self.wep1.cur in self.wep1.heavy + self.wep1.spec

    class WolfGuard(armory.WolfClawUser):
        type_name = 'Wolf Guard Pack leader'
        power = 2

        class PistolWeapon(OneOf):
            def __init__(self, parent):
                super(WolfScouts.WolfGuard.PistolWeapon, self).__init__(parent, 'Pistol')
                self.variant('Bolt pistol')
                self.variant(*ranged.PlasmaPistol)
                self.variant(*wargear.StormShield)
                armory.add_melee_weapons(self)

        class Bolter(OneOf):
            def __init__(self, parent):
                super(WolfScouts.WolfGuard.Bolter, self).__init__(parent, 'Gun')
                self.variant('Boltgun')
                self.variant(*ranged.PlasmaPistol)
                self.variant(*wargear.StormShield)
                armory.add_melee_weapons(self)
                armory.add_combi_weapons(self)

        def __init__(self, parent):
            super(WolfScouts.WolfGuard, self).__init__(
                parent, points=WolfScouts.model_points, gear=WolfScouts.Scout.model_gear)
            self.wep1 = self.PistolWeapon(self)
            self.wep2 = self.Bolter(self)

    class Uncle(OptionalSubUnit):
        def __init__(self, parent):
            super(WolfScouts.Uncle, self).__init__(parent, 'Wolf Guard Pack leader')
            SubUnit(self, WolfScouts.WolfGuard(parent=parent))

    class Leader(Unit):
        type_name = 'Wolf Scout Pack leader'

        class Bolter(OneOf):
            def __init__(self, parent):
                super(WolfScouts.Leader.Bolter, self).__init__(parent, 'Gun')
                self.ranged = [self.variant('Boltgun'),
                               self.variant('Chainsword'),
                               self.variant('Astartes shotgun', 0),
                               self.variant('Combat knife', 0),
                               self.variant(*ranged.SniperRifle)]
                self.variant(*melee.PowerAxe)
                self.variant(*melee.PowerSword)

        def __init__(self, parent):
            super(WolfScouts.Leader, self).__init__(parent, points=WolfScouts.model_points,
                                                    gear=WolfScouts.Scout.model_gear)
            self.wep1 = self.Bolter(self)
            self.wep2 = WolfScouts.Scout.Pistol(self)
            WolfScouts.Scout.Cloak(self)

        def check_rules(self):
            super(WolfScouts.Leader, self).check_rules()
            self.wep2.ppist.active = self.wep1.cur in self.wep1.ranged

    def __init__(self, parent):
        super(WolfScouts, self).__init__(parent)
        SubUnit(self, self.Leader(parent=self))
        self.marines = UnitList(self, self.Scout, 4, 9)
        self.wg = self.Uncle(self)

    def get_count(self):
        return self.marines.count + 1 + self.wg.count

    def check_rules(self):
        super(WolfScouts, self).check_rules()

        if sum(u.has_plasma_power() for u in self.marines.units) > 1:
            self.error('Only one Wolf Scouts may take plasma pistol or power weapon')
        if sum(u.has_heavy_special() for u in self.marines.units) > 1:
            self.error('Only one Wolf Scout can take heavy or special weapon')

    def build_power(self):
        return 4 * (1 + (self.marines.count > 4)) +\
            sum(sunit.unit.power for sunit in self.wg.units if sunit.used)


class SWReivers(ElitesUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Reivers)
    type_id = 'sw_reivers_v1'
    keywords = ['Infantry', 'Primaris']

    power = 5

    model_gear = [ranged.HeavyBoltPistol, ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.ShockGrenades]

    class Wargear(OptionsList):
        def __init__(self, parent):
            super(SWReivers.Wargear, self).__init__(parent, 'Squad wargear', used=False)
            self.variant(*wargear.GravChute, per_model=True)
            self.variant(*wargear.GrapnelLauncher, per_model=True)

    class SergeantKnife(OneOf):
        def __init__(self, parent):
            super(SWReivers.SergeantKnife, self).__init__(parent, 'Sergeant weapons')
            self.variant('Bolt carbine and heavy bolt pistol',
                         points=get_costs(ranged.BoltCarbine, ranged.HeavyBoltPistol),
                         gear=create_gears(ranged.BoltCarbine, ranged.HeavyBoltPistol))
            self.variant('Bolt carbine and combat knife',
                         points=get_costs(ranged.BoltCarbine, melee.CombatKnife),
                         gear=create_gears(ranged.BoltCarbine, melee.CombatKnife))
            self.variant('Heavy bolt pistol and combat knife',
                         points=get_costs(ranged.HeavyBoltPistol, melee.CombatKnife),
                         gear=create_gears(ranged.HeavyBoltPistol, melee.CombatKnife))

        @property
        def description(self):
            return [UnitDescription('Reiver Pack Leader', options=create_gears(*SWReivers.model_gear[1:])).\
                    add(super(SWReivers.SergeantKnife, self).description).add(self.parent.wgear.description)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Reiver', options=create_gears(*SWReivers.model_gear)).
                                    add(self.parent.wep.cur.gear).
                                    add(self.parent.wgear.description).set_count(self.cur)]

    class SquadWeapon(OneOf):
        def __init__(self, parent):
            super(SWReivers.SquadWeapon, self).__init__(parent, 'Squad weapons', used=False)
            self.variant(*ranged.BoltCarbine)
            self.variant(*melee.CombatKnife)

    def __init__(self, parent):
        sarge_cost = points_price(get_cost(units.Reivers), *SWReivers.model_gear[1:])
        super(SWReivers, self).__init__(parent, points=sarge_cost)
        self.wgear = self.Wargear(self)
        self.wep = self.SquadWeapon(self)
        self.sarge = self.SergeantKnife(self)
        self.models = self.Marines(self, 'Reivers', 4, 9, sarge_cost + get_costs(*SWReivers.model_gear),
                                   per_model=True)

    def get_count(self):
        return self.models.cur + 1

    def build_points(self):
        return super(SWReivers, self).build_points() +\
            (self.wgear.points + self.wep.points) * (1 + self.models.cur)

    def build_power(self):
        return self.power * (1 + (self.models.cur > 4))


class SWAggressors(ElitesUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Aggressors)
    type_id = 'sw_agressors_v1'

    keywords = ['Infantry', 'Primaris', 'Mk X gravis']

    power = 6

    class Weapons(OneOf):
        def __init__(self, parent):
            super(SWAggressors.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Auto boltstorm gauntlets and fragstorm grenade launcher',
                         get_costs(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher),
                         gear=create_gears(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher))
            self.variant(*ranged.FlamestormGauntlets)

        @property
        def description(self):
            return [UnitDescription('Aggressor Pack Leader').add(self.cur.gear)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Aggressor').add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(SWAggressors, self).__init__(parent, get_name(units.Aggressors),
                                           points=get_cost(units.Aggressors))
        self.wep = self.Weapons(self)
        self.models = self.Marines(self, 'Aggressors', 2, 5, points=get_cost(units.Aggressors),
                                   per_model=True)

    def get_count(self):
        return 1 + self.models.cur

    def build_points(self):
        return super(SWAggressors, self).build_points() + self.wep.points * self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))


class Lucas(ElitesUnit, armory.SWUnit):
    type_name = "Lucas the Trickster"
    type_id = 'lucas_v1'
    keywords = ['Character', 'Infantry', 'Blood claw']
    power = 6

    def __init__(self, parent):
        super(Lucas, self).__init__(parent, unique=True, static=True, gear=[
                                        Gear('Claw of the Jackalwolf'),
                                        Gear('Plasma Pistol'),
                                        Gear('Frag grenades'),
                                        Gear('Krak grenades')
                                    ], points=get_cost(units.LukasTheTrickster))


class WulfenDreadnought(ElitesUnit, armory.SWUnit):
    type_name = get_name(units.WulfenDreadnought)
    type_id = 'sw_wulfen_dreadnought_v1'

    power = 8
    model_points = get_cost(units.WulfenDreadnought)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(WulfenDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant(*melee.FenrisianGreatAxe)
            self.shield = self.variant(*wargear.BlizzardShield)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(WulfenDreadnought.Weapon2, self).__init__(parent, name='Weapon')
            self.variant(*melee.GreatWolfClaw)
            self.variant(*wargear.BlizzardShield)

    def __init__(self, parent):
        super(WulfenDreadnought, self).__init__(parent, points=self.model_points)
        self.w1 = self.Weapon1(self)
        self.bi1 = SWDreadnought.BuildIn(self)
        self.w2 = self.Weapon2(self)
        self.bi2 = SWDreadnought.BuildIn(self)

    def check_rules(self):
        super(WulfenDreadnought, self).check_rules()
        self.bi1.used = self.bi1.visible = self.w1.cur == self.w1.shield


class SWCompanyAncient(armory.SWUnit, CompanyAncient):
    type_name = get_name(units.GreatCompanyAncient)
    type_id = 'cw_company_ancient_v1'

    keywords = ['Character', 'Infantry', 'Ancient']
    power = 4

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(SWCompanyAncient.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Bolt pistol')
            self.variant('Boltgun')
            self.variant(*ranged.PlasmaPistol)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(CompanyAncient, self).__init__(parent, points=get_cost(units.GreatCompanyAncient),
                                             gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.Weapon1(self)


class SWPrimarisAncient(ElitesUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.PrimarisAncient)
    type_id = 'sw_primaris_ancient_v1'
    kwname = 'Ancient'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 5

    def __init__(self, parent):
        super(SWPrimarisAncient, self).__init__(parent, name=get_name(units.PrimarisAncient),
                                                points=get_cost(units.PrimarisAncient), gear=[
                                                    Gear("Bolt rifle"), Gear("Bolt pistol"),
                                                    Gear('Frag grenades'), Gear('Krak grenades')
                                                ], static=True)


class GreatCompanyChampion(ElitesUnit, armory.SWUnit):
    type_name = get_name(units.GreatCompanyChampion)
    type_id = 'sw_company_champion_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol,
                melee.MasterCraftedPowerSword]
        cost = points_price(get_cost(units.GreatCompanyChampion), *gear)
        super(GreatCompanyChampion, self).__init__(parent, points=cost,
                                                   gear=create_gears(*gear),
                                                   static=True)


class WolfGuards(ElitesUnit, armory.SWUnit):
    type_name = get_name(units.WolfGuard)
    type_id = "wolf_guards_v1"
    keywords = ['Infantry']
    power = 8

    class WolfGuardLeader(armory.WolfClawUser):
        type_name = 'Wolf Guard Pack Leader'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(WolfGuards.WolfGuardLeader.Weapon1, self).__init__(parent, 'Gun')
                self.variant('Boltgun')
                self.variant(*ranged.PlasmaPistol)
                self.variant(*wargear.StormShield)
                armory.add_combi_weapons(self)
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(WolfGuards.WolfGuardLeader.Weapon2, self).__init__(parent, 'Pistol')
                self.variant('Bolt pistol')
                self.variant(*wargear.StormShield)
                self.variant(*ranged.PlasmaPistol)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(WolfGuards.WolfGuardLeader, self).__init__(parent, gear=[
                Gear('Frag grenades'), Gear('Krak grenades')
            ], points=get_cost(units.WolfGuard))
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            # self.wep3 = self.Weapon3(self)

    class Marine(WolfGuardLeader, ListSubUnit):
        type_name = "Wolf Guard"

    class Ride(OptionsList):
        def __init__(self, parent):
            super(WolfGuards.Ride, self).__init__(parent, 'Movement options', limit=1)
            self.pack = self.variant('Jump packs', get_cost(units.WolfGuardWithJumpPacks) - get_cost(units.WolfGuard),
                                     per_model=True)

    def __init__(self, parent):
        super(WolfGuards, self).__init__(parent, name="Wolf Guard")
        self.ldr = SubUnit(self, self.WolfGuardLeader(parent=self))
        self.models = UnitList(self, self.Marine, 4, 9)
        self.opt = self.Ride(self)

    def get_count(self):
        return self.models.count + 1

    # def build_points(self):
    #     return self.ldr.points + self.models.points + (self.transport.points if self.transport else 0)

    def build_power(self):
        return (self.power + self.opt.any) * ((5 + self.models.count) / 5)

    def build_points(self):
        return super(WolfGuards, self).build_points() + self.opt.points * (self.get_count() - 1)


class WolfGuardTerminators(ElitesUnit, armory.SWUnit):
    type_name = get_name(units.WolfGuardTerminators)
    type_id = 'wolf_guard_term_v1'
    keywords = ['Infantry', 'Terminator']
    power = 13

    class Leader(armory.WolfClawUser):
        type_name = 'Wolf Guard Terminator Leader'
        fist = False

        class WeaponRanged(OneOf):
            def __init__(self, parent):
                super(WolfGuardTerminators.Leader.WeaponRanged, self).__init__(
                    parent, 'Ranged weapon')
                self.variant(*ranged.StormBolter)
                self.variant(*wargear.StormShield)
                armory.add_combi_weapons(self)
                armory.add_term_melee_weapons(self)

        class WeaponMelee(OneOf):
            def __init__(self, parent):
                super(WolfGuardTerminators.Leader.WeaponMelee, self).__init__(parent, 'Melee weapon')
                if parent.fist:
                    self.variant(*melee.PowerFist)
                else:
                    self.variant(*melee.PowerSword)
                self.variant(*wargear.StormShield)
                armory.add_term_melee_weapons(self, sword=not parent.fist, fist=parent.fist)

        def __init__(self, parent):
            super(WolfGuardTerminators.Leader, self).__init__(parent, points=31)
            self.wep1 = self.WeaponRanged(self)
            self.wep2 = self.WeaponMelee(self)

    class Marine(armory.WolfClawUser, ListSubUnit):
        type_name = 'Wolf Guard Terminator'
        fist = True

        class WeaponRanged(OneOf):
            def __init__(self, parent):
                super(WolfGuardTerminators.Marine.WeaponRanged, self).__init__(
                    parent, 'Ranged weapon')
                self.variant(*ranged.StormBolter)
                self.variant(*wargear.StormShield)
                armory.add_combi_weapons(self)
                armory.add_term_heavy_weapons(self)
                armory.add_term_melee_weapons(self)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.wep1.cur in self.wep1.heavy

        def __init__(self, parent):
            super(WolfGuardTerminators.Marine, self).__init__(parent, points=31)
            self.wep1 = self.WeaponRanged(self)
            self.wep2 = WolfGuardTerminators.Leader.WeaponMelee(self)

    def __init__(self, parent):
        super(WolfGuardTerminators, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Leader(parent=self))
        self.marines = UnitList(self, self.Marine, 2, 9)

    def get_count(self):
        return 1 + self.marines.count

    def check_rules(self):
        super(WolfGuardTerminators, self).check_rules()
        limit = self.get_count() / 5
        taken = sum(u.count_heavy() for u in self.marines.units)
        if taken > limit:
            self.error('Only one heavy weapon or missile launcher allowed per 5 terminators; taken: {} (in {})'.format(taken, self.count))

    def build_power(self):
        return self.power * (1 + (self.marines.count > 4))


class SWDreadnought(ElitesUnit, armory.SWUnit):
    type_name = 'Space Wolves Dreadnought'
    type_id = 'sw_dreadnought_v1'
    keywords = ['Vehicle']
    power = 7

    model_points = get_cost(units.Dreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(SWDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(SWDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(SWDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtWeapon)
            self.claw = self.variant(*melee.GreatWolfClaw)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)
            # self.twinlinkedautocannon = self.variant(*ranged.TwinLascannon)

    def get_melee_weapon(self):
        return SWDreadnought.Weapon2

    def __init__(self, parent):
        super(SWDreadnought, self).__init__(parent=parent, points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.get_melee_weapon()(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(SWDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = not self.wep2.cur in [
            self.wep2.missilelauncher
        ]


class WolfGuardCataphractii(ElitesUnit, armory.SWUnit):
    type_name = get_name(units.WolfGuardCataphractiiTerminators)
    type_id = 'wg_cataphractii_v1'
    kwname = 'Cataphractii Terminators'
    keywords = ['Infantry', 'Terminator', 'Wolf Guard']
    power = 12

    model_points = get_cost(units.WolfGuardCataphractiiTerminators)

    class Veteran(armory.ClawUser, ListSubUnit):
        type_name = 'Wolf Guard Cataphractii Terminator'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(WolfGuardCataphractii.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant(*melee.PowerFist)
                self.chain_fist = self.variant(*melee.Chainfist)
                self.claw = self.variant(*melee.LightningClaws)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(WolfGuardCataphractii.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.claw = self.variant(*melee.LightningClaws)
                self.hf = self.variant(*ranged.HeavyFlamer)

            def has_spec(self):
                return self.cur == self.hf

        def __init__(self, parent):
            super(WolfGuardCataphractii.Veteran, self).__init__(
                parent=parent, points=WolfGuardCataphractii.model_points
            )

            self.wep1 = self.LeftWeapon(self)
            self.wep2 = self.RightWeapon(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep2.has_spec()

    class Sergeant(armory.ClawUser):
        type_name = 'Wolf Guard Cataphractii Pack Leader'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(WolfGuardCataphractii.Sergeant.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.sword = self.variant(*melee.PowerSword)
                self.chain_fist = self.variant(*melee.Chainfist)
                self.power_fist = self.variant(*melee.PowerFist)
                self.claw = self.variant(*melee.LightningClaws)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(WolfGuardCataphractii.Sergeant.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.claw = self.variant(*melee.LightningClaws)

            def has_claw(self):
                return self.cur == self.claw

        def __init__(self, parent):
            super(WolfGuardCataphractii.Sergeant, self).__init__(
                parent=parent, points=WolfGuardCataphractii.model_points
            )

            self.wep1 = self.LeftWeapon(self)
            self.wep2 = self.RightWeapon(self)
            self.opt = self.Options(self)

        class Options(OptionsList):
            def __init__(self, parent):
                super(WolfGuardCataphractii.Sergeant.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.harness = self.variant(*ranged.GrenadeHarness)

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(WolfGuardCataphractii, self).__init__(parent)
        self.leader = SubUnit(self, self.get_leader()(self))
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Wolf Guard Cataphractii Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))

    def build_power(self):
        return self.power * (1 + (self.terms.count > 4))


class WolfGuardTartaros(ElitesUnit, armory.SWUnit):
    type_name = get_name(units.WolfGuardTartarosTerminators)
    type_id = 'wg_tartaros_v1'
    kwname = 'Tertaros Terminators'
    keywords = ['Infantry', 'Terminator', 'Wolf Guard']
    power = 12
    model_points = get_cost(units.WolfGuardTartarosTerminators)

    class Harness(OptionsList):
        def __init__(self, parent):
            super(WolfGuardTartaros.Harness, self).__init__(parent, '')
            self.variant(*ranged.GrenadeHarness)

    class Veteran(ListSubUnit):
        type_name = 'Wolf Guard Tartaros Terminator'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(WolfGuardTartaros.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant(*melee.PowerFist)
                self.claws = self.variant('Two lightning claws', get_cost(*melee.LightningClawsPair),
                                          gear=[Gear('Lightning claw', count=2)])
                self.chain_fist = self.variant(*melee.Chainfist)

            def has_claws(self):
                return self.cur == self.claws

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(WolfGuardTartaros.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.hf = self.variant(*ranged.HeavyFlamer)
                self.variant(*ranged.ReaperAutocannon)

            def has_spec(self):
                return self.used and self.cur != self.bolt

        def __init__(self, parent):
            super(WolfGuardTartaros.Veteran, self).__init__(
                parent=parent, points=WolfGuardTartaros.model_points
            )
            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.grenade = WolfGuardTartaros.Harness(self)

        def check_rules(self):
            super(WolfGuardTartaros.Veteran, self).check_rules()
            self.right_weapon.used = self.right_weapon.visible = not self.left_weapon.has_claws()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def has_harness(self):
            return self.grenade.any

    class Sergeant(Unit):
        type_name = 'Wolf Guard Tartaros Pack Leader'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(WolfGuardTartaros.Sergeant.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.sword = self.variant(*melee.PowerSword)
                self.chain_fist = self.variant(*melee.Chainfist)
                self.power_fist = self.variant(*melee.PowerFist)
                self.claws = self.variant('Two lightning claws', get_cost(melee.LightningClawsPair),
                                          gear=[Gear('Lightning claw', count=2)])

            def has_claws(self):
                return self.cur == self.claws

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(WolfGuardTartaros.Sergeant.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.variant(*ranged.PlasmaBlaster)
                self.variant(*ranged.VolkiteCharger)

        def __init__(self, parent):
            super(WolfGuardTartaros.Sergeant, self).__init__(
                parent=parent, points=WolfGuardTartaros.model_points
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.grenade = WolfGuardTartaros.Harness(self)

        def check_rules(self):
            super(WolfGuardTartaros.Sergeant, self).check_rules()
            self.right_weapon.used = self.right_weapon.visible = not self.left_weapon.has_claws()

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(WolfGuardTartaros, self).__init__(parent)
        self.leader = SubUnit(self, self.get_leader()(self))
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        super(WolfGuardTartaros, self).check_rules()
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Tartaros Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))
        gren = sum(c.has_harness() for c in self.terms.units) + self.leader.unit.grenade.any
        if gren > spec_limit:
            self.error('Tartaros Terminators may take only {0} grenade harnesses (taken {1})'.format(spec_limit, gren))

    def build_power(self):
        return self.power * (1 + (self.terms.count > 4))


class SWVenDreadnought(SWDreadnought):
    type_name = 'Space Wolves Venerable Dreadnought'
    type_id = 'sw_ven_dreadnought_v1'

    power = 9
    model_points = get_cost(units.VenerableDreadnought)

    class Melee(SWDreadnought.Weapon2):
        def __init__(self, parent):
            super(SWVenDreadnought.Melee, self).__init__(parent)
            self.axe = self.variant('Fenrisian great axe and blizzard shield',
                                    gear=create_gears(melee.FenrisianGreatAxe, wargear.BlizzardShield),
                                    points=get_costs(melee.FenrisianGreatAxe, wargear.BlizzardShield))

    def get_melee_weapon(self):
        return self.Melee

    def check_rules(self):
        super(SWVenDreadnought, self).check_rules()
        self.wep1.used = self.wep1.visible = self.wep2.cur != self.wep2.axe


class SWContemptor(ElitesUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.ContemptorDreadnought)
    type_id = 'sw_contemptor_dreadnought_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SWContemptor.Weapon, self).__init__(parent=parent, name='Weapon')
            self.melta = self.variant(*ranged.MultiMelta)
            self.cannon = self.variant(*ranged.KheresPatternAssaultCannon)

    def __init__(self, parent):
        gear = [ranged.CombiBolter, melee.DreadnoughtWeapon]
        cost = points_price(get_cost(units.ContemptorDreadnought), *gear)
        super(SWContemptor, self).__init__(parent=parent, name=get_name(units.ContemptorDreadnought),
                                           points=cost,
                                           gear=create_gears(*gear))
        self.wep = self.Weapon(self)


class SWRedemptor(ElitesUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.RedemptorDreadnought)
    type_id = 'sw_redemptor_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 10

    @classmethod
    def calc_faction(cls):
        return super(SWRedemptor, cls).calc_faction() + ['DEATHWATCH']

    gears = [melee.RedemptorFist]

    class Options(OptionsList):
        def __init__(self, parent):
            super(SWRedemptor.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.IcarusRocketPod)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(SWRedemptor.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HeavyFlamer)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(SWRedemptor.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.MacroPlasmaIncinerator)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(SWRedemptor.Weapon3, self).__init__(parent, 'Secondary weapon')
            self.variant('Two fragstorm grenade launchers', 2 * get_cost(ranged.FragstormGrenadeLauncher),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))
            self.variant('Two storm bolters', 2 * get_cost(ranged.StormBolter),
                         gear=create_gears(*[ranged.StormBolter] * 2))

    def __init__(self, parent):
        cost = points_price(get_cost(units.RedemptorDreadnought), *self.gears)
        gear = create_gears(*self.gears)
        super(SWRedemptor, self).__init__(parent, get_name(units.RedemptorDreadnought),
                                          points=cost, gear=gear)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)
        self.Options(self)


class Wulfen(ElitesUnit, armory.SWUnit):
    type_name = "Wulfen"
    type_id = "wulfen_v1"
    keywords = ['Infantry']
    model_points = get_cost(units.Wulfen)
    power = 11
    
    class Marine(ListSubUnit):
        type_name = 'Wulfen'

        class Launchers(OptionsList):
            def __init__(self, parent):
                super(Wulfen.Marine.Launchers, self).__init__(parent, "Launcher")
                self.variant(*ranged.StormfragAutoLauncher)

        class Melee(OneOf):
            def __init__(self, parent):
                super(Wulfen.Marine.Melee, self).__init__(parent, 'Melee weapon')
                self.variant('Wulfen claws')
                self.variant(*melee.FrostClaws)
                self.variant(*melee.GreatFrostAxe)
                self.variant('Thunder hammer and storm shield', get_costs(melee.ThunderHammer, wargear.StormShield),
                             gear=create_gears(melee.ThunderHammer, wargear.StormShield))

        def __init__(self, parent):
            super(Wulfen.Marine, self).__init__(
                parent=parent, points=Wulfen.model_points
            )
            self.wep1 = self.Melee(self)
            self.Launchers(self)

    class PackLeader(Unit):
        type_name = 'Wulfen Pack Leader'

        def __init__(self, parent):
            super(Wulfen.PackLeader, self).__init__(
                parent, points=points_price(Wulfen.model_points, melee.FrostClaws),
                gear=create_gears(melee.FrostClaws))
            Wulfen.Marine.Launchers(self)

    def __init__(self, parent):
        super(Wulfen, self).__init__(parent)
        SubUnit(self, self.PackLeader(parent=self))
        self.marines = UnitList(self, self.Marine, 4, 9)

    def get_count(self):
        return self.marines.count + 1

    def build_power(self):
        return self.power * (1 + (self.marines.count > 4))


class LoneWolf(armory.WolfClawUser, ElitesUnit, armory.SWUnit):
    type_name = 'Lone Wolf' + ' (Index)'
    type_id = 'lone_wolf_v1'
    keywords = ['Character', 'Infantry']
    power = 5

    class WeaponMelee(OneOf):
        def __init__(self, parent):
            super(LoneWolf.WeaponMelee, self).__init__(parent, 'Melee weapon')
            self.variant('Chainsword')
            self.variant(*ranged.PlasmaPistol)
            self.variant(*wargear.StormShieldCharacter)
            armory.add_melee_weapons(self, chain=False)

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(LoneWolf.WeaponRanged, self).__init__(parent, 'Ranged weapon')
            self.variant('Bolt pistol')
            self.variant(*wargear.StormShieldCharacter)
            self.variant(*ranged.PlasmaPistol)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(LoneWolf, self).__init__(parent, 'Lone Wolf', points=get_cost(units.LoneWolf),
                                       gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.wep1 = self.WeaponMelee(self)
        self.wep2 = self.WeaponRanged(self)


class TermLoneWolf(armory.WolfClawUser, ElitesUnit, armory.SWUnit):
    type_name = 'Lone Wolf in Terminator Armour' + ' (Index)'
    type_id = 'term_lone_wolf_v1'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 7

    class WeaponMelee(OneOf):
        def __init__(self, parent):
            super(TermLoneWolf.WeaponMelee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerSword)
            self.variant(*wargear.StormShieldCharacter)
            armory.add_term_melee_weapons(self, sword=False)

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(TermLoneWolf.WeaponRanged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.StormBolter)
            self.variant(*wargear.StormShieldCharacter)
            armory.add_combi_weapons(self)
            armory.add_term_melee_weapons(self)

    def __init__(self, parent):
        super(TermLoneWolf, self).__init__(parent, get_name(units.LoneWolfInTerminatorArmour),
                                           points=get_cost(units.LoneWolfInTerminatorArmour))
        self.wep1 = self.WeaponMelee(self)
        self.wep2 = self.WeaponRanged(self)


class Murderfang(ElitesUnit, armory.SWUnit):
    type_id = 'murderfang_v1'
    type_name = 'Murderfang'
    keywords = ['Character', 'Vehicle', 'Dreadnought']
    power = 9

    def __init__(self, parent):
        super(Murderfang, self).__init__(parent, gear=[
            Gear('The Murderclaws'), Gear('Storm bolter'),
            Gear('Heavy flamer')
        ], unique=True, static=True, points=get_cost(units.Murderfang))


class BikeWolfGuards(ElitesUnit, armory.SWUnit):
    type_name = "Wolf Guard on Bikes" + ' (Index)'
    type_id = "bike_wolf_guards_v1"
    keywords = ['Biker']

    class WolfGuardLeader(armory.WolfClawUser, Unit):
        type_name = 'Wolf Guard Pack Leader on Bike'

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(BikeWolfGuards.WolfGuardLeader.Weapon2, self).__init__(parent, 'Pistol')
                self.variant('Bolt pistol')
                self.variant('Boltgun')
                self.variant(*ranged.PlasmaPistol)
                armory.add_melee_weapons(self)
                armory.add_combi_weapons(self)

        class Weapon3(OneOf):
            def __init__(self, parent):
                super(BikeWolfGuards.WolfGuardLeader.Weapon3, self).__init__(parent, 'Melee weapon')
                self.variant('Chainsword')
                self.variant(*ranged.PlasmaPistol)
                self.variant(*wargear.StormShield)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(BikeWolfGuards.WolfGuardLeader, self).__init__(parent, gear=[
                Gear('Frag grenades'), Gear('Krak grenades'), Gear('Twin boltgun')
            ], points=points_price(get_cost(units.WolfGuardOnBikes), ranged.TwinBoltgun))
            self.wep1 = self.Weapon2(self)
            self.wep2 = self.Weapon3(self)

        def build_points(self):
            res = super(BikeWolfGuards.WolfGuardLeader, self).build_points()
            if (self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw):
                res -= 5
            return res

    class Marine(WolfGuardLeader, ListSubUnit):
        type_name = "Wolf Guard on bike"

    def __init__(self, parent):
        super(BikeWolfGuards, self).__init__(parent, 'Wolf Guard on Bikes')
        self.ldr = SubUnit(self, self.WolfGuardLeader(parent=self))
        self.models = UnitList(self, self.Marine, 4, 9)

    def get_count(self):
        return self.models.count + 1

    # def build_points(self):
    #     return self.ldr.points + self.models.points + (self.transport.points if self.transport else 0)

    def build_power(self):
        return 14 * ((5 + self.models.count) / 5)
