from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList, OptionalSubUnit
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *
from .troops import WGPackLeader


class TermWolfGuardLeader(armory.WolfClawUser):
    type_name = get_name(units.WolfGuardTerminatorPack)
    power = 3

    class WeaponMelee(OneOf):
        def __init__(self, parent):
            super(TermWolfGuardLeader.WeaponMelee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerSword)
            self.variant(*wargear.StormShield)
            armory.add_term_melee_weapons(self, sword=False)

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(TermWolfGuardLeader.WeaponRanged, self).__init__(parent, 'Ranged weapon')
            armory.add_combi_weapons(self)
            armory.add_term_melee_weapons(self)
            armory.add_term_heavy_weapons(self)

    def __init__(self, parent):
        super(TermWolfGuardLeader, self).__init__(parent, points=get_cost(units.WolfGuardTerminatorPack))
        self.wep1 = self.WeaponRanged(self)
        self.wep2 = self.WeaponMelee(self)


class LongFangs(HeavyUnit, armory.SWUnit):
    type_name = get_name(units.LongFangs)
    type_id = "long_fangs_v1"
    keywords = ['Infantry']
    model_gear = [Gear('Frag Grenades'), Gear('Krak Grenades')]
    model_points = get_cost(units.LongFangs)
    power = 8

    class LongFangPackLeader(Unit):
        type_name = 'Long Fang Pack leader'

        class WeaponMelee(OneOf):
            def __init__(self, parent):
                super(LongFangs.LongFangPackLeader.WeaponMelee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Chainsword)
                self.variant(*melee.PowerAxe)
                self.variant(*melee.PowerFist)
                self.variant(*melee.PowerSword)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(LongFangs.LongFangPackLeader.Weapon2, self).__init__(parent, 'Ranged weapon')
                self.variant('Boltgun and bolt pistol', gear=[Gear('Boltgun'), Gear('Bolt pistol')])
                self.variant(*ranged.PlasmaPistol)
                armory.add_special_weapons(self)

        def __init__(self, parent):
            super(LongFangs.LongFangPackLeader, self).__init__(parent, points=LongFangs.model_points,
                                                            gear=LongFangs.model_gear)
            self.wep1 = self.WeaponMelee(self)
            self.wep2 = self.Weapon2(self)

    class LongFang(ListSubUnit):
        type_name = "Long Fang"

        class Weapon(OneOf):
            def __init__(self, parent):
                super(LongFangs.LongFang.Weapon, self).__init__(parent, 'Gun')
                self.variant('Boltgun')
                armory.add_heavy_weapons(self)

        def __init__(self, parent):
            super(LongFangs.LongFang, self).__init__(parent, points=LongFangs.model_points,
                                                     gear=LongFangs.model_gear + [Gear("Bolt pistol")])
            self.wep = self.Weapon(self)

    class GuardAncient(OptionalSubUnit):
        def __init__(self, parent):
            super(LongFangs.GuardAncient, self).__init__(parent, 'Wolf Guard leader', limit=1)
            SubUnit(self, WGPackLeader(parent=parent))
            SubUnit(self, TermWolfGuardLeader(parent=parent))

    def __init__(self, parent):
        super(LongFangs, self).__init__(parent)
        self.leader = SubUnit(self, self.LongFangPackLeader(self))
        self.marines = UnitList(self, self.LongFang, 4, 5)
        self.wg = self.GuardAncient(self)

    def get_count(self):
        return self.marines.count + 1 + self.wg.count

    def build_power(self):
        return self.power + (self.marines.count > 4) +\
            sum(sunit.unit.power for sunit in self.wg.units if sunit.used)


class SWHellblasters(HeavyUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Hellblasters)
    type_id = 'sw_hellblasters_v1'
    keywords = ['Infantry', 'Primaris']
    power = 8

    model_gear = [
        Gear('Frag grenades'), Gear('Krak grenades'),
        Gear('Bolt pistol')
    ]

    class Sergeant(OneOf):
        def __init__(self, parent):
            super(SWHellblasters.Sergeant, self).__init__(parent, 'Sergeant weapon')
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.PlasmaPistol)

        @property
        def description(self):
            return [UnitDescription('Hellblaster Pack Leader', options=SWHellblasters.model_gear[0:2]).
                    add(super(SWHellblasters.Sergeant, self).description).
                    add(self.parent.wep.cur.gear)]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SWHellblasters.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*ranged.PlasmaIncinerator)
            self.variant(*ranged.AssaultPlasmaIncinerator)
            self.variant(*ranged.HeavyPlasmaIncinerator)

        @property
        def description(self):
            return []

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Hellblaster', options=SWHellblasters.model_gear).
                    add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(SWHellblasters, self).__init__(parent, get_name(units.Hellblasters),
                                             points=get_cost(units.Hellblasters))
        self.sarge = self.Sergeant(self)
        self.wep = self.Weapon(self)
        self.marines = self.Marines(self, 'Hellblasters', 4, 9,
                                    get_cost(units.Hellblasters), per_model=True)

    def build_points(self):
        return super(SWHellblasters, self).build_points() + self.wep.points * self.marines.cur

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))


class SWHunter(HeavyUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Hunter)
    type_id = "sw_hunter_v1"
    keywords = ['Vehicle']
    power = 5

    def __init__(self, parent):
        super(SWHunter, self).__init__(parent=parent, name=get_name(units.Hunter),
                                       points=get_cost(units.Hunter),
                                       gear=[Gear('Skyspear missile launcher')])
        self.opt = SWPredator.Options(self)


class SWStalker(HeavyUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Stalker)
    type_id = "sw_stalker_v1"
    keywords = ['Vehicle']
    power = 6

    def __init__(self, parent):
        gear = [ranged.IcarusStormcannon] * 2
        super(SWStalker, self).__init__(parent=parent, name=get_name(units.Stalker),
                                        points=points_price(get_cost(units.Stalker), *gear),
                                        gear=create_gears(*gear))
        self.opt = SWPredator.Options(self)


class SWWhirlwind(HeavyUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Whirlwind)
    type_id = "sw_whirlwind_v1"
    keywords = ['Vehicle']
    power = 6

    class Turret(OneOf):
        def __init__(self, parent):
            super(SWWhirlwind.Turret, self).__init__(parent=parent, name='Turret')
            self.variant(*ranged.WhirlwindVengeanceLauncher)
            self.variant(*ranged.WhirlwindCastellanLauncher)

    def __init__(self, parent):
        super(SWWhirlwind, self).__init__(parent=parent, name=get_name(units.Whirlwind),
                                          points=get_cost(units.Whirlwind))
        self.turret = self.Turret(self)
        self.opt = SWPredator.Options(self)


class SWPredator(HeavyUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Predator)
    type_id = "sw_predator_v1"
    keywords = ['Vehicle']
    power = 9

    class Turret(OneOf):
        def __init__(self, parent):
            super(SWPredator.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant(*ranged.PredatorAutocannon)
            self.twinlinkedlascannon = self.variant(*ranged.TwinLascannon)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(SWPredator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.sponsonswithheavybolters = self.variant('Two heavy bolters', 2 * get_cost(ranged.HeavyBolter),
                                                         gear=create_gears(*[ranged.HeavyBolter] * 2))
            self.sponsonswithlascannons = self.variant('Two lascannons', 2 * get_cost(ranged.Lascannon),
                                                       gear=create_gears(*[ranged.Lascannon] * 2))

    class Options(OptionsList):
        def __init__(self, parent):
            super(SWPredator.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        super(SWPredator, self).__init__(parent=parent, name=get_name(units.Predator),
                                         points=get_cost(units.Predator))
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = self.Options(self)


class SWVindicator(HeavyUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Vindicator)
    type_id = "sw_vindicator_v1"
    keywords = ['Vehicle']
    power = 7

    def __init__(self, parent):
        super(SWVindicator, self).__init__(parent=parent, name=get_name(units.Vindicator),
                                           points=get_cost(units.Vindicator),
                                           gear=[Gear('Demolisher cannon')])
        self.opt = SWPredator.Options(self)


class SWLandRaider(HeavyUnit, armory.SWUnit):
    type_id = 'sw_land_raider_v1'
    type_name = 'Space Wolves ' + get_name(units.LandRaider)

    keywords = ['Vehicle', 'Transport']
    power = 19

    class Options(SWPredator.Options):
        def __init__(self, parent):
            super(SWLandRaider.Options, self).__init__(parent)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        gear = [ranged.TwinHeavyBolter, ranged.TwinLascannon, ranged.TwinLascannon]
        cost = points_price(get_cost(units.LandRaider), *gear)
        super(SWLandRaider, self).__init__(parent=parent, points=cost,
                                           gear=create_gears(*gear),
                                           name=get_name(units.LandRaider))
        self.opt = self.Options(self)


class SWLandRaiderCrusader(HeavyUnit, armory.SWUnit):
    type_id = 'sw_land_raider_crusader_v1'
    type_name = 'Space Wolves ' + get_name(units.LandRaiderCrusader)
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 16

    def __init__(self, parent):
        gear = [ranged.TwinAssault, ranged.HurricaneBolter, ranged.HurricaneBolter]
        cost = points_price(get_cost(units.LandRaiderCrusader), *gear)
        super(SWLandRaiderCrusader, self).__init__(parent=parent, gear=create_gears(*gear),
                                                   points=cost, name=get_name(units.LandRaiderCrusader))
        self.opt = SWLandRaider.Options(self)


class SWLandRaiderRedeemer(HeavyUnit, armory.SWUnit):
    type_id = 'sw_land_raider_redeemer_v1'
    type_name = 'Space Wolves ' + get_name(units.LandRaiderRedeemer)
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 18

    def __init__(self, parent):
        gear = [ranged.TwinAssault, ranged.FlamestormCannon, ranged.FlamestormCannon]
        cost = points_price(get_cost(units.LandRaiderRedeemer), *gear)
        super(SWLandRaiderRedeemer, self).__init__(parent=parent, points=cost,
                                                   gear=create_gears(*gear),
                                                   name=get_name(units.LandRaider))
        self.opt = SWLandRaider.Options(self)


class SWEliminators(HeavyUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.EliminatorSquad)
    type_id = 'sw_eliminators_v1'

    keywords = ['Infantry', 'Primaris', 'Phobos']
    model_cost = get_cost(units.EliminatorSquad)
    gearlist = [ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades, wargear.CamoCloak]
    power = 4

    class Sergeant(OneOf):
        def __init__(self, parent):
            super(SWEliminators.Sergeant, self).__init__(parent, 'Sergeant weapon')
            self.variant(*ranged.BoltSniperRifle)
            self.variant(*ranged.InstigatorBoltCarbine)
            self.variant(*ranged.LasFusil)

        @property
        def description(self):
            return [UnitDescription('Eliminator Sergeant', options=create_gears(*SWEliminators.gearlist))
                    .add(super(SWEliminators.Sergeant, self).description)]

    class SquadWeapons(OneOf):
        def __init__(self, parent):
            super(SWEliminators.SquadWeapons, self).__init__(parent, 'Squad weapons')
            self.variant(*ranged.BoltSniperRifle)
            self.variant(*ranged.LasFusil)

        @property
        def description(self):
            return [UnitDescription('Eliminator', count=2, options=create_gears(*SWEliminators.gearlist))
                    .add(super(SWEliminators.SquadWeapons, self).description)]

    def __init__(self, parent):
        cost = points_price(get_cost(units.EliminatorSquad), *self.gearlist)
        super(SWEliminators, self).__init__(parent, points=3 * cost)
        self.Sergeant(self)
        self.sqw = self.SquadWeapons(self)

    def build_points(self):
        return super(SWEliminators, self).build_points() + self.sqw.points

    def get_count(self):
        return 3
