from builder.games.wh40k8ed.unit import Unit
from . import ranged
from builder.games.wh40k8ed.utils import join_and, create_gears, get_costs


def add_knight_weapons(instance):
    instance.variant(join_and(ranged.AvengerGatlingCannon, ranged.HeavyFlamer),
                     get_costs(ranged.AvengerGatlingCannon, ranged.HeavyFlamer),
                     gear=create_gears(ranged.AvengerGatlingCannon, ranged.HeavyFlamer))
    instance.variant(join_and(ranged.RapidFireBattleCannon, ranged.HeavyStubber),
                     get_costs(ranged.RapidFireBattleCannon, ranged.HeavyStubber),
                     gear=create_gears(ranged.RapidFireBattleCannon, ranged.HeavyStubber))
    instance.variant(*ranged.ThermalCannon)


def add_caparace_weapons(instance):
    instance.variant(*ranged.TwinIcarusAutocannon)
    instance.variant(*ranged.StormspearRocketPod)
    instance.variant(*ranged.IronstormMissilePod)


class ChaosKnightUnit(Unit):
    faction = ['Chaos', 'Chaos Knights']
    wiki_faction = 'Questor Traitors'


class KnightUnit(ChaosKnightUnit):
    faction = ['<Questor Traitoris>']
    keywords = ['Titanic', 'Vehicle']

    @classmethod
    def calc_faction(cls):
        return ['ICONOCLAST', 'INFERNAL']
