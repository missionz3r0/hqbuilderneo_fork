from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList
from builder.games.wh40k8ed.utils import *
from . import armory
from . import ia_units, ia_ranged, ia_melee, ia_wargear


class RelicContemptor(ElitesUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.ContemptorDreadnought) + ' (Imperial Armor)'
    type_id = 'ia_relic_contemptor_v1'

    keywords = ['Vehicle', 'Dreadnought', 'Relic']
    power = 13

    @classmethod
    def calc_faction(cls):
        return super(RelicContemptor, cls).calc_faction() + ['DEATHWATCH']

    class BuiltIn(OneOf):
        def __init__(self, parent):
            super(RelicContemptor.BuiltIn, self).__init__(parent, 'Built-in weapon')
            self.variant(*ia_ranged.StormBolter)
            self.variant(*ia_ranged.HeavyFlamer)
            self.variant(*ia_ranged.PlasmaBlastgun)
            self.variant(*ia_ranged.GravitonBlaster)

    class ArmWeapon(OneOf):
        def __init__(self, parent):
            super(RelicContemptor.ArmWeapon, self).__init__(parent, 'Arm weapon')
            self.dcw = self.variant(*ia_melee.DreadnoughtCombatWeapon)
            self.dcf = self.variant(*ia_melee.DreadnoughtChainfist)
            self.ccw = [self.dcw, self.dcf]
            self.variant(*ia_ranged.TwinHeavyBolter)
            self.variant(*ia_ranged.MultiMelta)
            self.variant(*ia_ranged.TwinLascannon)
            self.variant(*ia_ranged.TwinAutocannon)
            self.variant(*ia_ranged.HeavyPlasmaCannon)
            self.variant(*ia_ranged.KheresAssaultCannon)
            self.variant(*ia_ranged.CBeamCannon)

    def __init__(self, parent):
        super(RelicContemptor, self).__init__(parent, get_name(ia_units.ContemptorDreadnought),
                                              points=get_cost(ia_units.ContemptorDreadnought))
        self.wep1 = self.ArmWeapon(self)
        self.biw1 = self.BuiltIn(self)
        self.wep2 = self.ArmWeapon(self)
        self.biw2 = self.BuiltIn(self)

    def check_rules(self):
        super(RelicContemptor, self).check_rules()
        self.biw1.used = self.biw1.visible = self.wep1.cur in self.wep1.ccw
        self.biw2.used = self.biw2.visible = self.wep2.cur in self.wep2.ccw

    def build_points(self):
        res = super(RelicContemptor, self).build_points()
        if self.wep1.cur == self.wep1.dcw and self.wep2.cur == self.wep2.dcw:
            res -= 2 * get_cost(ia_melee.DreadnoughtCombatWeapon) - get_cost(ia_melee.DreadnoughtCombatWeaponPair)
        if self.wep1.cur == self.wep1.dcf and self.wep2.cur == self.wep2.dcf:
            res -= 2 * get_cost(ia_melee.DreadnoughtChainfist) - get_cost(ia_melee.DreadnoughtChainfistPair)
        return res


class RelicDeredeo(ElitesUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.DeredeoDreadnought) + ' (Imperial Armor)'
    type_id = 'ia_deredeo_dreadnought_v1'

    keywords = ['Vehicle', 'Dreadnought', 'Relic']
    power = 14

    @classmethod
    def calc_faction(cls):
        # magic slicing to leave off Space Wolves
        return super(RelicDeredeo, cls).calc_faction() + ['DEATHWATCH']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(RelicDeredeo.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ia_ranged.TwinHeavyBolter)
            self.variant(*ia_ranged.TwinHeavyFlamer)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(RelicDeredeo.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*ia_ranged.AnvillusAutocannonBattery)
            self.variant(*ia_ranged.HellfirePlasmaCarronade)
            self.variant(*ia_ranged.ArachnusHeavyLascannonBattery)

    class WeaponOption(OptionsList):
        def __init__(self, parent):
            super(RelicDeredeo.WeaponOption, self).__init__(parent, 'Options', limit=1)
            self.variant(*ia_ranged.AiolosMissileLauncher)
            self.variant(*ia_wargear.AtomanticPavaise)

    def __init__(self, parent):
        super(RelicDeredeo, self).__init__(parent, get_name(ia_units.DeredeoDreadnought),
                                           points=get_cost(ia_units.DeredeoDreadnought))
        self.Weapon1(self)
        self.Weapon2(self)
        self.WeaponOption(self)
