from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList,\
    ListSubUnit, UnitList, SubUnit
from . import armory, old_armory, melee, ranged, units, wargear
from builder.games.wh40k8ed.utils import *


class Devastators(HeavyUnit, armory.CodexDAUnit):
    type_name = 'Devastator Squad' + ' (Index)'
    type_id = 'devastators_v1'
    keywords = ['Infantry']
    obsolete = True

    model_gear = [Gear('Frag grenades'), Gear('Krak grenades'), Gear('Bolt pistol')]

    armory = old_armory

    class Sergeant(Unit):
        type_name = 'Space Marine Sergeant'

        def __init__(self, parent):
            super(Devastators.Sergeant, self).__init__(
                parent, gear=Devastators.model_gear, points=13
            )
            self.wep1 = parent.armory.SergeantWeapon(self)

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(Devastators.HeavyWeapon, self).__init__(parent, 'Heavy Weapon')
            self.bolt = self.variant('Boltgun')
            parent.armory.add_heavy_weapons(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Devastators.Options, self).__init__(parent, 'Options')
            self.variant('Armorium Cherub', points=5, gear=[UnitDescription('Armorium Cherub')])

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(Devastators, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.get_leader()(self))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=13,
                             per_model=True)
        self.heavy = [self.HeavyWeapon(self) for i in range(0, 4)]
        self.opt = self.Options(self)

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points,
                               count=self.get_count(), options=self.opt.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=Devastators.model_gear,
        )
        for o in self.heavy:
            spec_marine = marine.clone()
            spec_marine.add(o.description)
            desc.add_dup(spec_marine)

        marine.add(Gear('Boltgun')).set_count(self.marines.cur - 4)
        desc.add_dup(marine)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return 7 + 4 * (self.marines.cur > 4)


class CodexDevastators(HeavyUnit, armory.CodexDAUnit):
    type_name = armory.get_name(units.DevastatorSquad)
    type_id = 'devastators_v2'
    keywords = ['Infantry']
    power = 6

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]

    class Sergeant(armory.ClawUser):
        type_name = 'Space Marine Sergeant'

        class SecondWeaponFlag(OptionsList):
            def __init__(self, parent):
                super(CodexDevastators.Sergeant.SecondWeaponFlag, self).__init__(parent, '')
                self.variant('Second weapon', 0, gear=[])

        def __init__(self, parent):
            super(CodexDevastators.Sergeant, self).__init__(
                parent, gear=armory.create_gears(*CodexDevastators.model_gear),
                points=armory.get_cost(units.DevastatorSquad)
            )
            self.wep1 = armory.SergeantWeapon(self)
            self.flag = self.SecondWeaponFlag(self)
            self.wep2 = armory.SergeantWeapon(self, double=True)

        def check_rules(self):
            super(CodexDevastators.Sergeant, self).check_rules()
            self.wep2.used = self.wep2.visible = self.flag.any
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(CodexDevastators.HeavyWeapon, self).__init__(parent, 'Heavy Weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_heavy_weapons(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(CodexDevastators.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.ArmoriumCherub)

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(CodexDevastators, self).__init__(parent=parent, name=armory.get_name(units.DevastatorSquad))
        self.sergeant = SubUnit(parent=self, unit=self.get_leader()(self))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9,
                             points=armory.get_cost(units.DevastatorSquad),
                             per_model=True)
        self.heavy = [self.HeavyWeapon(self) for i in range(0, 4)]
        self.opt = self.Options(self)

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points,
                               count=self.get_count(), options=self.opt.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=armory.create_gears(*CodexDevastators.model_gear),
        )
        for o in self.heavy:
            spec_marine = marine.clone()
            spec_marine.add(o.description)
            desc.add_dup(spec_marine)

        marine.add(Gear('Boltgun')).set_count(self.marines.cur - 4)
        desc.add_dup(marine)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return self.power + 3 * (self.marines.cur > 4)


class CenturionDevastators(HeavyUnit, armory.SMUnit):
    type_name = armory.get_name(units.CenturionDevastatorSquad)
    type_id = 'centurion_devastator_squad_v1'
    keywords = ['Infantry', 'Centurion']
    power = 12

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CenturionDevastators.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.hurricanebolter = self.variant(*ranged.HurricaneBolter)
            self.missilelauncher = self.variant(*ranged.CenturionMissileLauncher)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CenturionDevastators.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Two heavy bolters', 2 * armory.get_cost(ranged.HeavyBolter),
                         gear=armory.create_gears(*[ranged.HeavyBolter] * 2))
            self.variant('Two lascannons', 2 * armory.get_cost(ranged.Lascannon),
                         gear=armory.create_gears(*[ranged.Lascannon] * 2))
            self.variant(*ranged.GravCannonAmp)

    class Centurion(ListSubUnit):
        def __init__(self, parent):
            super(CenturionDevastators.Centurion, self).__init__(parent=parent, name='Centurion',
                                                                 points=armory.get_cost(units.CenturionDevastatorSquad))
            self.wep1 = CenturionDevastators.Weapon1(self)
            self.wep2 = CenturionDevastators.Weapon2(self)

    class Sergeant(Unit):

        def __init__(self, parent):
            super(CenturionDevastators.Sergeant, self).__init__(parent=parent, name='Centurion Sergeant',
                                                                points=armory.get_cost(units.CenturionDevastatorSquad))
            self.wep1 = CenturionDevastators.Weapon1(self)
            self.wep2 = CenturionDevastators.Weapon2(self)

    def __init__(self, parent):
        super(CenturionDevastators, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=self))
        self.cent = UnitList(self, self.Centurion, min_limit=2, max_limit=5)

    def get_count(self):
        return self.cent.count + 1

    def build_power(self):
        return self.power * (1 + self.cent.count > 2)


class Hellblasters(HeavyUnit, armory.CommonSMUnit):
    type_name = 'Hellblaster Squad'
    type_id = 'hellblasters_v1'
    keywords = ['Infantry', 'Primaris']
    power = 6

    model_gear = [
        Gear('Frag grenades'), Gear('Krak grenades'),
        Gear('Bolt pistol')
    ]

    @classmethod
    def calc_faction(cls):
        return super(Hellblasters, cls).calc_faction() + ['DEATHWATCH']

    class Sergeant(OneOf):
        def __init__(self, parent):
            super(Hellblasters.Sergeant, self).__init__(parent, 'Sergeant weapon')
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.PlasmaPistol)

        @property
        def description(self):
            return [UnitDescription('Hellblaster Sergeant', options=Hellblasters.model_gear[0:2]).
                    add(super(Hellblasters.Sergeant, self).description).
                    add(self.parent.wep.cur.gear)]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hellblasters.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*ranged.PlasmaIncinerator)
            self.variant(*ranged.AssaultPlasmaIncinerator)
            self.variant(*ranged.HeavyPlasmaIncinerator)

        @property
        def description(self):
            return []

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Hellblaster', options=Hellblasters.model_gear).
                    add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(Hellblasters, self).__init__(parent, points=armory.get_cost(units.HellblasterSquad))
        self.sarge = self.Sergeant(self)
        self.wep = self.Weapon(self)
        self.marines = self.Marines(self, 'Hellblasters', 4, 9,
                                    armory.get_cost(units.HellblasterSquad), per_model=True)

    def build_points(self):
        return super(Hellblasters, self).build_points() + self.wep.points * self.marines.cur

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))


class Thunderfire(HeavyUnit, armory.SMUnit):
    type_name = "Thunderfire cannon"
    type_id = 'thunderfire_v1'
    keywords = ['Vehicle', 'Artillery', 'Character', 'Infantry', 'Techmarine']
    power = 4

    gunner_gear = [ranged.BoltPistol, ranged.PlasmaCutter,
                   ranged.Flamer, melee.ServoArm,
                   melee.ServoArm]
    gunner = UnitDescription(name='Techmarine gunner',
                             options=armory.create_gears(*gunner_gear))

    def __init__(self, parent):
        super(Thunderfire, self).__init__(parent=parent, static=True, gear=[
            UnitDescription('Thunderfire cannon'),
            Thunderfire.gunner.clone()
        ], points=armory.get_cost(units.ThunderfireCannon) +
                                          armory.points_price(armory.get_cost(units.TechmarineGunner),
                                                              *self.gunner_gear))


class Predator(HeavyUnit, armory.CommonSMUnit):
    type_name = "Predator" + ' (Index)'
    type_id = "predator_v1"
    keywords = ['Vehicle']
    power = 9
    obsolete = True

    class Turret(OneOf):
        def __init__(self, parent):
            super(Predator.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant('Predator autocannon', 49)
            self.twinlinkedlascannon = self.variant('Twin lascannon', 50)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Predator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.sponsonswithheavybolters = self.variant('Two heavy bolters', 2 * 10,
                                                         gear=Gear('Heavy bolter', count=2))
            self.sponsonswithlascannons = self.variant('Two lascannons', 2 * 25,
                                                       gear=Gear('Lascannon', count=2))

    class Options(OptionsList):
        def __init__(self, parent, blade=False):
            super(Predator.Options, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 6)
            self.variant('Storm bolter', 2)

    def __init__(self, parent):
        super(Predator, self).__init__(parent=parent, name='Predator', points=102)
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = self.Options(self)


class CodexPredator(HeavyUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.Predator)
    type_id = "predator_v2"
    keywords = ['Vehicle']
    power = 8

    class Turret(OneOf):
        def __init__(self, parent):
            super(CodexPredator.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant(*ranged.PredatorAutocannon)
            self.twinlinkedlascannon = self.variant(*ranged.TwinLascannon)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(CodexPredator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.sponsonswithheavybolters = self.variant('Two heavy bolters', 2 * armory.get_cost(ranged.HeavyBolter),
                                                         gear=armory.create_gears(*[ranged.HeavyBolter] * 2))
            self.sponsonswithlascannons = self.variant('Two lascannons', 2 * armory.get_cost(ranged.Lascannon),
                                                       gear=armory.create_gears(*[ranged.Lascannon] * 2))

    class Options(OptionsList):
        def __init__(self, parent, blade=False):
            super(CodexPredator.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKiller)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        super(CodexPredator, self).__init__(parent=parent, name=armory.get_name(units.Predator),
                                       points=armory.get_cost(units.Predator))
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = self.Options(self)


class Whirlwind(HeavyUnit, armory.CommonSMUnit):
    type_name = "Whirlwind" + ' (Index)'
    type_id = "whirlwind_v1"
    keywords = ['Vehicle']
    power = 6
    obsolete = True

    class Turret(OneOf):
        def __init__(self, parent):
            super(Whirlwind.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant(*ranged.WhirlwindVengeance)
            self.autocannon = self.variant(*ranged.WhirlwindCastellan)

    def __init__(self, parent):
        super(Whirlwind, self).__init__(parent=parent, name='Whirlwind', points=90)
        self.turret = self.Turret(self)
        self.opt = Predator.Options(self)


class CodexWhirlwind(HeavyUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.Whirlwind)
    type_id = "whirlwind_v2"
    keywords = ['Vehicle']
    power = 4

    @classmethod
    def calc_faction(cls):
        return super(CodexWhirlwind, cls).calc_faction() + ['DARK ANGELS',
                                                            '<DARK ANGELS SUCCESSORS>']

    class Turret(OneOf):
        def __init__(self, parent):
            super(CodexWhirlwind.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant(*ranged.WhirlwindVengeance)
            self.autocannon = self.variant(*ranged.WhirlwindCastellan)

    def __init__(self, parent):
        super(CodexWhirlwind, self).__init__(parent=parent, name=armory.get_name(units.Whirlwind),
                                             points=armory.get_cost(units.Whirlwind))
        self.turret = self.Turret(self)
        self.opt = CodexPredator.Options(self)


class CodexVindicator(HeavyUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.Vindicator)
    type_id = "vindicator_v2"
    keywords = ['Vehicle']
    power = 6

    @classmethod
    def calc_faction(cls):
        return super(CodexVindicator, cls).calc_faction() + ['DARK ANGELS',
                                                             '<DARK ANGELS SUCCESSORS>']

    def __init__(self, parent):
        super(CodexVindicator, self).__init__(parent=parent, name=armory.get_name(units.Vindicator),
                                              points=armory.get_cost(units.Vindicator),
                                              gear=[Gear('Demolisher cannon')])
        self.opt = CodexPredator.Options(self)


class Vindicator(HeavyUnit, armory.CommonSMUnit):
    type_name = "Vindicator" + ' (Index)'
    type_id = "vindicator_v1"
    keywords = ['Vehicle']
    obsolete = True
    power = 8

    def __init__(self, parent):
        super(Vindicator, self).__init__(parent=parent, name='Vindicator',
                                         points=160, gear=[Gear('Demolisher cannon')])
        self.opt = Predator.Options(self)


class Hunter(HeavyUnit, armory.SMUnit):
    type_name = armory.get_name(units.Hunter)
    type_id = "hunter_v1"
    keywords = ['Vehicle']
    power = 4

    def __init__(self, parent):
        super(Hunter, self).__init__(parent=parent, points=armory.get_cost(units.Hunter),
                                     gear=[Gear('Skyspear missile launcher')])
        self.opt = CodexPredator.Options(self)


class Stalker(HeavyUnit, armory.SMUnit):
    type_name = armory.get_name(units.Stalker)
    type_id = "stalker_v1"
    keywords = ['Vehicle']
    power = 5

    def __init__(self, parent):
        gear = [ranged.IcarusStormcannon] * 2
        super(Stalker, self).__init__(parent=parent,
                                      points=armory.points_price(armory.get_cost(units.Stalker), *gear),
                                      gear=armory.create_gears(*gear))
        self.opt = CodexPredator.Options(self)


class LandRaider(HeavyUnit, armory.CommonSMUnit):
    type_id = 'land_raider_v1'
    type_name = armory.get_name(units.LandRaider)

    keywords = ['Vehicle', 'Transport']
    power = 15

    @classmethod
    def calc_faction(cls):
        return super(LandRaider, cls).calc_faction() + ['DEATHWATCH']

    class Options(CodexPredator.Options):
        def __init__(self, parent):
            super(LandRaider.Options, self).__init__(parent)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        gear = [ranged.TwinHeavyBolter, ranged.TwinLascannon, ranged.TwinLascannon]
        cost = armory.points_price(armory.get_cost(units.LandRaider), *gear)
        super(LandRaider, self).__init__(parent=parent, points=cost,
                                         gear=armory.create_gears(*gear))
        self.opt = self.Options(self)


class LandRaiderCrusader(HeavyUnit, armory.CommonSMUnit):
    type_id = 'land_raider_crusader_v1'
    type_name = armory.get_name(units.LandRaiderCrusader)
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 14

    @classmethod
    def calc_faction(cls):
        return super(LandRaiderCrusader, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ranged.TwinAssault, ranged.HurricaneBolter, ranged.HurricaneBolter]
        cost = armory.points_price(armory.get_cost(units.LandRaiderCrusader), *gear)
        super(LandRaiderCrusader, self).__init__(parent=parent, gear=armory.create_gears(*gear),
                                                 points=cost)
        self.opt = LandRaider.Options(self)


class LandRaiderRedeemer(HeavyUnit, armory.CommonSMUnit):
    type_id = 'land_raider_redeemer_v1'
    type_name = armory.get_name(units.LandRaiderRedeemer)
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 14

    @classmethod
    def calc_faction(cls):
        return super(LandRaiderRedeemer, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ranged.TwinAssault, ranged.FlamestormCannon, ranged.FlamestormCannon]
        cost = armory.points_price(armory.get_cost(units.LandRaiderRedeemer), *gear)
        super(LandRaiderRedeemer, self).__init__(parent=parent, points=cost,
                                                 gear=armory.create_gears(*gear))
        self.opt = LandRaider.Options(self)


class RepulsorExecutioner(HeavyUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.RepulsorExecutioner)
    type_id = 'repulsor_ex_v1'
    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 15

    @classmethod
    def calc_faction(cls):
        return super(RepulsorExecutioner, cls).calc_faction() + ['DEATHWATCH']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(RepulsorExecutioner.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.MacroPlasmaIncinerator)
            self.variant(*ranged.HeavyLaserDestroyer)

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(RepulsorExecutioner.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)
            self.variant(*ranged.IcarusRocketPod)

    def __init__(self, parent):
        gear = [ranged.FragstormGrenadeLauncher, ranged.FragstormGrenadeLauncher,
                ranged.HeavyOnslaughtGatlingCannon, ranged.StormBolter,
                ranged.StormBolter, ranged.TwinHeavyBolter, ranged.TwinIcarusIronhailHeavyStubber,
                wargear.AutoLaunchers]
        super(RepulsorExecutioner, self).__init__(parent, points=armory.points_price(armory.get_cost(units.RepulsorExecutioner), *gear),
                                       gear=armory.create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)


class Eliminators(HeavyUnit, armory.SMUnit):
    type_name = armory.get_name(units.EliminatorSquad)
    type_id = 'eliminators_v1'

    keywords = ['Infantry', 'Primaris', 'Phobos']
    model_cost = get_cost(units.EliminatorSquad)
    gearlist = [ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades, wargear.CamoCloak]
    power = 4

    class Sergeant(OneOf):
        def __init__(self, parent):
            super(Eliminators.Sergeant, self).__init__(parent, 'Sergeant weapon')
            self.variant(*ranged.BoltSniperRifle)
            self.variant(*ranged.InstigatorBoltCarbine)
            self.variant(*ranged.LasFusil)

        @property
        def description(self):
            return [UnitDescription('Eliminator Sergeant', options=armory.create_gears(*Eliminators.gearlist))
                    .add(super(Eliminators.Sergeant, self).description)]

    class SquadWeapons(OneOf):
        def __init__(self, parent):
            super(Eliminators.SquadWeapons, self).__init__(parent, 'Squad weapons')
            self.variant(*ranged.BoltSniperRifle)
            self.variant(*ranged.LasFusil)

        @property
        def description(self):
            return [UnitDescription('Eliminator', count=2, options=armory.create_gears(*Eliminators.gearlist))
                    .add(super(Eliminators.SquadWeapons, self).description)]

    def __init__(self, parent):
        cost = points_price(get_cost(units.EliminatorSquad), *self.gearlist)
        super(Eliminators, self).__init__(parent, points=3 * cost)
        self.Sergeant(self)
        self.sqw = self.SquadWeapons(self)

    def build_points(self):
        return super(Eliminators, self).build_points() + self.sqw.points

    def get_count(self):
        return 3
