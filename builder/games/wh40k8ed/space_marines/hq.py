from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, \
    ListSubUnit, UnitList
from builder.games.wh40k8ed.utils import *
from . import armory, units, ranged, melee, old_armory, wargear


class Captain(HqUnit, armory.SMUnit):
    type_name = 'Captain (Index)'
    type_id = 'captain_v1'

    keywords = ['Character', 'Infantry']
    power = 5

    armory = old_armory
    unit_name = 'Captain'
    obsolete = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Captain.Weapon1, self).__init__(parent, 'Weapon 1')
            self.variant('Master-crafted boltgun', 3)
            self.parent.armory.add_pistol_options(self)
            self.parent.armory.add_combi_weapons(self)
            self.parent.armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Captain.Weapon2, self).__init__(parent, 'Weapon2')
            self.parent.armory.add_melee_weapons(self)
            self.variant('Storm shield', 15)
            self.variant('Relic blade', 21)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(Captain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', 93 - 74)

    def __init__(self, parent):
        super(Captain, self).__init__(parent, name=self.unit_name, points=74, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_points(self):
        res = super(Captain, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 5
        return res

    def build_power(self):
        return self.power + 1 * self.pack.any


class CodexCaptain(HqUnit, armory.ClawUser, armory.SMUnit):
    type_name = armory.get_name(units.Captain)
    type_id = 'captain_v2'

    keywords = ['Character', 'Infantry']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexCaptain.Weapon1, self).__init__(parent, 'Weapon 1')
            self.variant(*ranged.MCBoltgun)
            armory.add_pistol_options(self)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexCaptain.Weapon2, self).__init__(parent, 'Weapon 2')
            armory.add_melee_weapons(self)
            self.variant(*melee.RelicBlade)
            self.variant(*wargear.StormShieldChar)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(CodexCaptain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack',
                                     armory.get_cost(units.JumpCaptain) - armory.get_cost(units.Captain))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        cost = armory.points_price(armory.get_cost(units.Captain), *gear)
        super(CodexCaptain, self).__init__(parent, name=armory.get_name(units.Captain), points=cost,
                                           gear=armory.create_gears(*gear))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class TerminatorCaptain(HqUnit, armory.SMUnit):
    type_name = 'Captain in Terminator Armour' + ' (Index)'
    type_id = 'termo_captain_v1'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 8

    armory = old_armory
    unit_name = 'Captain in Terminator Armour'
    obsolete = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TerminatorCaptain.Weapon1, self).__init__(parent, 'Bolter')
            # self.variant('Storm bolter', 2)
            self.parent.armory.add_term_combi_weapons(self)
            self.parent.armory.add_term_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TerminatorCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant('Power sword', 4)
            self.variant('Chainfist', 22)
            self.variant('Storm shield', 15)
            self.variant('Relic blade', 21)
            self.parent.armory.add_term_melee_weapons(self)

    class Launcher(OptionsList):
        def __init__(self, parent):
            super(TerminatorCaptain.Launcher, self).__init__(parent, 'Launcher')
            self.variant('Wrist-mounted grenade launcher', 4)

    def __init__(self, parent):
        super(TerminatorCaptain, self).__init__(parent, self.unit_name,
                                                points=122)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.launcher = self.Launcher(self)

    def build_points(self):
        res = super(TerminatorCaptain, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 5
        return res

    def check_rules(self):
        super(TerminatorCaptain, self).check_rules()
        fist = self.wep1.fist == self.wep1.cur or self.wep2.fist == self.wep2.cur
        self.launcher.visible = self.launcher.used = fist


class CodexTerminatorCaptain(HqUnit, armory.ClawUser, armory.SMUnit):
    type_name = armory.get_name(units.TerminatorCaptain)
    type_id = 'termo_captain_v2'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexTerminatorCaptain.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.StormBolter)
            armory.add_term_combi_weapons(self)
            armory.add_term_melee_weapons(self)
            self.variant(*wargear.StormShieldChar)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexTerminatorCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.PowerSword)
            self.variant(*melee.RelicBlade)
            self.variant(*melee.Chainfist)
            armory.add_term_melee_weapons(self)
            self.variant(*wargear.StormShieldChar)

    class Launcher(OptionsList):
        def __init__(self, parent):
            super(CodexTerminatorCaptain.Launcher, self).__init__(parent, 'Launcher')
            self.variant(*ranged.WristLauncher)

    def __init__(self, parent):
        super(CodexTerminatorCaptain, self).__init__(parent, armory.get_name(units.TerminatorCaptain),
                                                     points=armory.get_cost(units.TerminatorCaptain))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.launcher = self.Launcher(self)

    def check_rules(self):
        super(CodexTerminatorCaptain, self).check_rules()
        fist = self.wep1.fist == self.wep1.cur or self.wep2.fist == self.wep2.cur
        self.launcher.visible = self.launcher.used = fist


class CataphractiiCaptain(HqUnit, armory.SMUnit):
    type_name = 'Captain in Cataphractii Armour' + ' (Index)'
    type_id = 'cataphractii_captain_v1'
    obsolete = True

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 8

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CataphractiiCaptain.Weapon1, self).__init__(parent, 'Bolter')
            self.variant('Combi bolter', 2)
            old_armory.add_term_combi_weapons(self)
            old_armory.add_term_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CataphractiiCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant('Power sword', 4)
            self.variant('Relic blade', 21)
            self.variant('Chainfist', 22)
            old_armory.add_term_melee_weapons(self)

    def __init__(self, parent):
        super(CataphractiiCaptain, self).__init__(parent, 'Captain in Cataphractii Armour',
                                                  points=126)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)

    def build_points(self):
        res = super(CataphractiiCaptain, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 5
        return res


class CodexCataphractiiCaptain(HqUnit, armory.ClawUser, armory.SMUnit):
    type_name = armory.get_name(units.CataphractiiCaptain)
    type_id = 'cataphractii_captain_v2'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexCataphractiiCaptain.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.CombiBolter)
            armory.add_term_combi_weapons(self)
            armory.add_term_melee_weapons(self)
            self.variant(*wargear.StormShieldChar)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexCataphractiiCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.PowerSword)
            self.variant(*melee.RelicBlade)
            self.variant(*melee.Chainfist)
            armory.add_term_melee_weapons(self)
            self.variant(*wargear.StormShieldChar)

    def __init__(self, parent):
        super(CodexCataphractiiCaptain, self).__init__(parent, armory.get_name(units.CataphractiiCaptain),
                                                       points=armory.get_cost(units.CataphractiiCaptain))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class GravisCaptain(HqUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.GravisCaptain)
    type_id = 'gravis_captain_v1'

    keywords = ['Character', 'Infantry', 'Mk X Gravis', 'Primaris']
    power = 6

    def __init__(self, parent):
        gear = [melee.MCPowerSword, ranged.BoltstormGauntlet]
        super(GravisCaptain, self).__init__(parent,
                                            points=armory.points_price(armory.get_cost(units.GravisCaptain), *gear),
                                            gear=armory.create_gears(*gear), static=True)


class PhobosCaptain(HqUnit, armory.SMUnit):
    type_name = armory.get_name(units.PhobosCaptain)
    type_id = 'phobos_captain_v1'
    kwname = 'Captain'
    keywords = ['Character', 'Infantry', 'Phobos', 'Primaris']
    power = 5

    def __init__(self, parent):
        gear = [melee.CombatKnife, ranged.MCInstigatorBoltCarbine, ranged.BoltPistol,
                wargear.CamoCloak, ranged.FragGrenades, ranged.KrakGrenades]
        super(PhobosCaptain, self).__init__(parent,
                                            points=armory.points_price(armory.get_cost(units.PhobosCaptain), *gear),
                                            gear=armory.create_gears(*gear), static=True)


class PhobosLibrarian(HqUnit, armory.SMUnit):
    type_name = armory.get_name(units.PhobosLibrarian)
    type_id = 'phobos_librarian_v1'
    kwname = 'Librarian'

    keywords = ['Character', 'Infantry', 'Phobos', 'Primaris', 'Psyker']
    power = 5

    @classmethod
    def calc_faction(cls):
        # magic slice to exclude black templars
        return super(PhobosLibrarian, cls).calc_faction()[1:]

    def __init__(self, parent):
        gear = [melee.ForceSword, ranged.BoltPistol,
                wargear.CamoCloak, ranged.FragGrenades, ranged.KrakGrenades]
        super(PhobosLibrarian, self).__init__(parent,
                                              points=armory.points_price(armory.get_cost(units.PhobosLibrarian), *gear),
                                              gear=armory.create_gears(*gear), static=True)


class PhobosLieutenants(HqUnit, armory.SMUnit):
    type_name = 'Lieutenants in Phobos armour'
    type_id = 'phobos_lieutenant_v1'
    kwname = 'Lieutenant'

    keywords = ['Character', 'Infantry', 'Phobos', 'Primaris']
    power = 4

    class Lieutenant(ListSubUnit):
        type_name = armory.get_name(units.PhobosLieutenant)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(PhobosLieutenants.Lieutenant.Weapon, self).__init__(parent, 'Weapon')
                gears1 = [ranged.MCOcculusBoltCarbine, ranged.BoltPistol, melee.PairedCombatBlades]
                self.variant(join_and(*gears1), get_costs(wargear.GravChute, *gears1),
                             gear=create_gears(wargear.GravChute, *gears1))
                gears2 = [ranged.HeavyBoltPistol, melee.CombatKnife]
                self.variant(join_and(*gears2), get_costs(wargear.SmokeGrenades, *gears2),
                             gear=create_gears(wargear.SmokeGrenades, *gears2))

        def __init__(self, parent):
            super(PhobosLieutenants.Lieutenant, self).__init__(
                parent, points=get_cost(units.PhobosLieutenant),
                gear=create_gears(ranged.FragGrenades, ranged.KrakGrenades))
            self.Weapon(self)

    def __init__(self, parent):
        super(PhobosLieutenants, self).__init__(parent)
        self.units = UnitList(self, self.Lieutenant, 1, 2)

    def get_count(self):
        return self.units.count

    def build_power(self):
        return self.power * self.get_count()


class BikeCaptain(HqUnit, armory.SMUnit):
    type_name = 'Captain on Bike' + ' (Index)'
    type_id = 'bike_captain_v1'
    unit_name = 'Captain on Bike'
    keywords = ['Character', 'Biker']
    power = 7
    obsolete = True
    armory = old_armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BikeCaptain.Weapon1, self).__init__(parent, 'Ranged')
            self.variant('Master-crafted boltgun', 3)
            self.parent.armory.add_pistol_options(self)
            self.parent.armory.add_combi_weapons(self)
            self.parent.armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(BikeCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.parent.armory.add_melee_weapons(self)
            self.variant('Storm shield', 15)

    def __init__(self, parent):
        super(BikeCaptain, self).__init__(parent, self.unit_name, points=98 + 2, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Twin boltgun')
        ])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)

    def build_points(self):
        res = super(BikeCaptain, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 5
        return res


class CodexBikeCaptain(HqUnit, armory.ClawUser, armory.SMUnit):
    type_name = armory.get_name(units.BikeCaptain)
    type_id = 'bike_captain_v2'

    keywords = ['Character', 'Biker']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexBikeCaptain.Weapon1, self).__init__(parent, 'Pistol')
            armory.add_pistol_options(self)
            self.variant(*ranged.MCBoltgun)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexBikeCaptain.Weapon2, self).__init__(parent, 'Weapon')
            armory.add_melee_weapons(self)
            self.variant(*wargear.StormShieldChar)

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.TwinBoltgun]
        cost = armory.points_price(armory.get_cost(units.BikeCaptain), *gear)
        super(CodexBikeCaptain, self).__init__(parent, armory.get_name(units.BikeCaptain),
                                               points=cost,
                                               gear=armory.create_gears(*gear))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class Librarian(HqUnit, armory.SMUnit):
    type_name = armory.get_name(units.Librarian)
    type_id = 'librarian_v1'

    keywords = ['Character', 'Infantry', 'Psyker']
    power = 5

    armory = armory

    @classmethod
    def calc_faction(cls):
        # magic slice to exclude black templars
        return super(Librarian, cls).calc_faction()[1:]

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Librarian.Weapon1, self).__init__(parent, 'Pistol')
            self.parent.armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)
            self.parent.armory.add_combi_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Librarian.Weapon2, self).__init__(parent, 'Force')
            self.variant(*melee.ForceStave)
            self.variant(*melee.ForceSword)
            self.variant(*melee.ForceAxe)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(Librarian.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', armory.get_cost(units.JumpLibrarian) -
                                     armory.get_cost(units.Librarian))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        cost = armory.points_price(armory.get_cost(units.Librarian), *gear)
        super(Librarian, self).__init__(parent, points=cost,
                                        gear=armory.create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class TermoLibrarian(HqUnit, armory.CodexBAUnit):
    type_name = 'Librarian in Terminator Armor' + ' (Index)'
    type_id = 'termo_librarian_v1'
    obsolete = True
    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 9

    @classmethod
    def calc_faction(cls):
        # magic slice to exclude black templars
        return super(TermoLibrarian, cls).calc_faction()[1:] + ['DEATHWING',
                                                                'DARK ANGELS',
                                                                '<DARK ANGELS SUCCESSORS>']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TermoLibrarian.Weapon1, self).__init__(parent, 'Bolter')
            self.variant('Storm bolter', 2)
            self.variant('Storm shield', 15)
            old_armory.add_term_combi_weapons(self)

    def __init__(self, parent):
        super(TermoLibrarian, self).__init__(parent, 'Librarian in Terminator Armor',
                                             points=145)
        self.Weapon1(self)
        Librarian.Weapon2(self)


class CodexTermoLibrarian(HqUnit, armory.SMUnit):
    type_name = armory.get_name(units.TermoLibrarian)
    type_id = 'termo_librarian_v2'

    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 6

    class Weapon1(OptionsList):
        def __init__(self, parent):
            super(CodexTermoLibrarian.Weapon1, self).__init__(parent, 'Ranged weapon', limit=1)
            # self.variant(*ranged.StormBolter)
            # self.variant(*wargear.StormShieldChar)
            armory.add_term_combi_weapons(self)

    def __init__(self, parent):
        super(CodexTermoLibrarian, self).__init__(parent, armory.get_name(units.TermoLibrarian),
                                                  points=armory.get_cost(units.TermoLibrarian))
        self.Weapon1(self)
        Librarian.Weapon2(self)


class BikeLibrarian(HqUnit, armory.CodexDAUnit):
    type_name = 'Librarian on Bike'
    type_id = 'bike_librarian_v1'

    keywords = ['Character', 'Biker', 'Psyker']
    power = 8

    relative = Librarian
    armory = armory

    @classmethod
    def calc_faction(cls):
        # magic slice to exclude black templars
        return super(BikeLibrarian, cls).calc_faction()[1:] + ['DEATHWING']

    def __init__(self, parent):
        super(BikeLibrarian, self).__init__(parent, points=119 + 2, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Twin boltgun')
        ])
        self.relative.Weapon1(self)
        self.relative.Weapon2(self)


class Techmarine(HqUnit, armory.CodexDAUnit):
    type_name = 'Techmarine' + ' (Index)'
    type_id = 'techmarine_v1'

    keywords = ['Character', 'Infantry']
    power = 5
    armory = old_armory

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Techmarine.Weapon2, self).__init__(parent, 'Weapon')
            self.variant('Power axe', 5)
            self.parent.armory.add_melee_weapons(self, axe=False)

    class Arm(OneOf):
        def __init__(self, parent):
            super(Techmarine.Arm, self).__init__(parent, 'Arm')
            self.variant('Servo-arm', 12)
            self.beamer = self.variant('Conversion beamer', 20)

    class Harness(OptionsList):
        def __init__(self, parent):
            super(Techmarine.Harness, self).__init__(parent, 'Harness')
            self.harness = self.variant('Servo-harness', 12 + 7 + 9)

    def __init__(self, parent):
        super(Techmarine, self).__init__(parent, points=58, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ])
        Librarian.Weapon1(self)
        self.Weapon2(self)
        self.arm = self.Arm(self)
        self.harness = self.Harness(self)

    def build_power(self):
        return self.power + 1 * self.harness.any

    def check_rules(self):
        super(Techmarine, self).check_rules()
        if self.harness.harness.value:
            self.arm.beamer.visible = False


class CodexTechmarine(HqUnit, armory.SMUnit):
    type_name = armory.get_name(units.Techmarine) + ' (Codex)'
    type_id = 'techmarine_v2'

    keywords = ['Character', 'Infantry']
    power = 4
    armory = armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexTechmarine.Weapon1, self).__init__(parent, 'Pistol')
            armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)
            self.parent.armory.add_combi_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexTechmarine.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.PowerAxe)
            armory.add_melee_weapons(self, axe=False)

    class Harness(OptionsList):
        def __init__(self, parent):
            super(CodexTechmarine.Harness, self).__init__(parent, 'Harness')
            gear = [melee.ServoArm, ranged.Flamer, ranged.PlasmaCutter]
            self.harness = self.variant('Servo-harness', armory.get_costs(*gear),
                                        gear=armory.create_gears(*gear))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.ServoArm]
        cost = armory.points_price(armory.get_cost(units.Techmarine), *gear)
        super(CodexTechmarine, self).__init__(parent, name=armory.get_name(units.Techmarine),
                                              points=cost,
                                              gear=armory.create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.harness = self.Harness(self)


class BikeTechmarine(HqUnit, armory.CodexDAUnit):
    type_name = 'Techmarine on Bike'
    type_id = 'bike_techmarine_v1'

    keywords = ['Character', 'Biker']
    power = 6

    relative = Librarian
    armory = armory

    def __init__(self, parent):
        super(BikeTechmarine, self).__init__(parent, points=70 + 2, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Twin boltgun'),
        ])
        self.relative.Weapon1(self)
        CodexTechmarine.Weapon2(self)
        self.arm = Techmarine.Arm(self)
        self.harness = CodexTechmarine.Harness(self)

    def build_power(self):
        return self.power + 1 * self.harness.any

    def check_rules(self):
        super(BikeTechmarine, self).check_rules()
        if self.harness.harness.value:
            self.arm.beamer.visible = False


class Chaplain(HqUnit, armory.CodexDAUnit):
    type_name = 'Chaplain' + ' (Index)'
    type_id = 'chaplain_v1'
    obsolete = True
    keywords = ['Character', 'Infantry']
    power = 5
    obsolete = True

    armory = armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Chaplain.Weapon1, self).__init__(parent, 'Weapon')
            self.parent.armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)
            self.parent.armory.add_combi_weapons(self)
            self.variant(*melee.PowerFist)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(Chaplain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', 90 - 72)

    def __init__(self, parent):
        super(Chaplain, self).__init__(parent, points=72, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Crozius Arcanum')
        ], name='Chaplain')
        self.Weapon1(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class CodexChaplain(HqUnit, armory.SMUnit):
    type_name = armory.get_name(units.Chaplain)
    type_id = 'chaplain_v2'

    keywords = ['Character', 'Infantry']
    power = 4

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexChaplain.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)
            armory.add_combi_weapons(self)
            self.variant(*melee.PowerFist)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(CodexChaplain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', armory.get_cost(units.JumpChaplain) -
                                     armory.get_cost(units.Chaplain))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.CroziusArcanum]
        cost = armory.points_price(armory.get_cost(units.Chaplain), *gear)
        super(CodexChaplain, self).__init__(parent, armory.get_name(units.Chaplain),
                                            points=cost, gear=armory.create_gears(*gear))
        self.Weapon1(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class TerminatorChaplain(HqUnit, armory.CodexBAUnit):
    type_name = 'Chaplain in Terminator Armour'
    type_id = 'termo_chaplain_v1'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TerminatorChaplain.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.StormBolter)
            armory.add_term_combi_weapons(self)

    def __init__(self, parent):
        super(TerminatorChaplain, self).__init__(parent, points=armory.get_cost(units.TerminatorChaplain),
                                                 gear=[Gear('Crozius Arcanum')])
        self.Weapon1(self)


class BikeChaplain(HqUnit, armory.SMUnit):
    type_name = 'Chaplain on Bike'
    type_id = 'bike_chaplain_v1'

    keywords = ['Character', 'Biker']
    power = 6

    relative = Chaplain
    armory = armory

    def __init__(self, parent):
        super(BikeChaplain, self).__init__(parent, points=95 + 2, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Crozius Arcanum'), Gear('Twin boltgun'),
        ])
        self.relative.Weapon1(self)


class PrimarisLieutenants(HqUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.PrimarisLieutenants)
    type_id = 'lieutenants_v1'

    keywords = ['Character', 'Infantry', 'Primaris', 'Lieutenants']

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    model = UnitDescription('Primaris Lieutenant', options=common_gear + [Gear('Bolt pistol')])

    power = 5

    @classmethod
    def calc_faction(cls):
        return super(PrimarisLieutenants, cls).calc_faction() + ['DEATHWATCH']

    class Lieutenant(Count):
        @property
        def description(self):
            return PrimarisLieutenants.model.clone().add(armory.create_gears(ranged.MCAutoBoltRifle)). \
                set_count(self.cur - self.parent.sword.cur - self.parent.stalker.cur)

    def __init__(self, parent):
        super(PrimarisLieutenants, self).__init__(parent)
        self.models = self.Lieutenant(self, 'Primaris Lieutenants', 1, 2, per_model=True,
                                      points=armory.points_price(armory.get_cost(units.PrimarisLieutenants),
                                                                 ranged.MCAutoBoltRifle))
        self.stalker = Count(self, 'Master-crafted stalker bolt rifle', 0, 1,
                             armory.get_cost(ranged.MCStalkerBoltRifle) - armory.get_cost(ranged.MCAutoBoltRifle),
                             gear=PrimarisLieutenants.model.clone().add(armory.create_gears(ranged.MCStalkerBoltRifle)))
        self.sword = Count(self, 'Power sword', 0, 1,
                           armory.get_cost(melee.PowerSword) - armory.get_cost(ranged.MCAutoBoltRifle),
                           gear=PrimarisLieutenants.model.clone().add(armory.create_gears(melee.PowerSword)))

    def check_rules(self):
        super(PrimarisLieutenants, self).check_rules()
        Count.norm_counts(0, self.models.cur, [self.sword, self.stalker])

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.models.cur * self.power


class Lieutenants(HqUnit, armory.SMUnit):
    type_name = armory.get_name(units.Lieutenants)
    type_id = 'sm_leutanenta_v1'
    keywords = ['Character', 'Infantry']
    power = 5

    class Leutenant(armory.ClawUser, ListSubUnit):
        type_name = 'Leutenant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Lieutenants.Leutenant.Weapon1, self).__init__(parent, 'Weapon 1')
                self.variant(*ranged.MCBoltgun)
                armory.add_pistol_options(self)
                armory.add_combi_weapons(self)
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Lieutenants.Leutenant.Weapon2, self).__init__(parent, 'Weapon 2')
                armory.add_melee_weapons(self)

        class Pack(OptionsList):
            def __init__(self, parent):
                super(Lieutenants.Leutenant.Pack, self).__init__(parent, 'Pack')
                self.pack = self.variant('Jump pack', armory.get_cost(units.JumpLieutenants) -
                                         armory.get_cost(units.Lieutenants))

        def __init__(self, parent):
            gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]
            cost = armory.points_price(armory.get_cost(units.Lieutenants), *gear)
            super(Lieutenants.Leutenant, self).__init__(parent, 'Leutenant', cost,
                                                        gear=armory.create_gears(*gear))
            self.keywords = Lieutenants.keywords
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.Pack(self)

    def __init__(self, parent):
        super(Lieutenants, self).__init__(parent)
        self.models = UnitList(self, self.Leutenant, 1, 2)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class RhinoPrimaris(HqUnit, armory.CommonSMUnit):
    type_name = 'Rhino Primaris'
    type_id = 'rhino_primaris_v1'

    keywords = ['Vehicle', 'Transport', 'Rhino']
    power = 9

    class Options(OptionsList):
        def __init__(self, parent):
            super(RhinoPrimaris.Options, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 6)

    def __init__(self, parent):
        super(RhinoPrimaris, self).__init__(
            parent, gear=[
                Gear('Twin plasma gun'),
                Gear('Orbital array')
            ], points=100 + 50 + 20)
        self.Options(self)


class LandRaiderExcelsior(HqUnit, armory.CommonSMUnit):
    type_name = 'Land Raider Excelsior'
    type_id = 'land_raider_excelsior_v1'

    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 24

    class ExcelsiorOptions(OptionsList):
        def __init__(self, parent):
            super(LandRaiderExcelsior.ExcelsiorOptions, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 6)
            self.variant('Storm bolter', 2)
            self.variant('Multi-melta', 27)
            self.variant('Combi-plasma', 15)

    def __init__(self, parent):
        super(LandRaiderExcelsior, self).__init__(
            parent, gear=[
                Gear('Grav cannon and grav-amp'),
                Gear('Twin lascannon', count=2),
            ], points=300 + 28 + 50 * 2)
        self.ExcelsiorOptions(self)


class Calgar(HqUnit, armory.AstartesUnit):
    type_id = 'calgar_v1'
    type_name = armory.get_name(units.MarneusCalgar)
    faction = ['Ultramarines']
    power = 13
    keywords = ['Character', 'Infantry', 'Chapter master', 'Terminator']

    def __init__(self, parent):
        super(Calgar, self).__init__(parent=parent, static=True, gear=[
            Gear('Gauntlets of Ultramar'),
            Gear('Relic blade')
        ], points=armory.get_cost(units.MarneusCalgar))

    def get_unique(self):
        return 'MARNEUS CALGAR'

    def build_statistics(self):
        res = super(Calgar, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 2
        return res


class PrimarisCalgar(HqUnit, armory.AstartesUnit):
    type_id = 'primcalgar_v1'
    type_name = armory.get_name(units.PrimarisCalgar)
    faction = ['Ultramarines']
    power = 11
    keywords = ['Character', 'Infantry', 'Chapter master', 'Mk X Gravis', 'Primaris']

    def __init__(self, parent):
        super(PrimarisCalgar, self).__init__(parent=parent, static=True, gear=[
            Gear('Gauntlets of Ultramar')
        ], points=armory.get_cost(units.PrimarisCalgar))

    def get_unique(self):
        return 'MARNEUS CALGAR'

    def build_statistics(self):
        res = super(PrimarisCalgar, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 2
        return res


class ArtCalgar(HqUnit, armory.AstartesUnit):
    type_id = 'art_calgar_v1'
    type_name = 'Marneus Calgar in Artificer Armour'
    faction = ['Ultramarines']
    power = 12
    kwname = 'Marneus Calgar'
    keywords = ['Character', 'Infantry', 'Chapter master']

    def __init__(self, parent):
        super(ArtCalgar, self).__init__(parent=parent, static=True, gear=[
            Gear('Gauntlets of Ultramar'),
            Gear('Relic blade')
        ], points=185)

    def get_unique(self):
        return 'MARNEUS CALGAR'

    def build_statistics(self):
        res = super(ArtCalgar, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 2
        return res


class Sicarius(HqUnit, armory.AstartesUnit):
    type_id = 'captain_sicarius_v1'
    type_name = armory.get_name(units.CaptainSicarius)
    faction = ['Ultramarines']
    power = 5
    kwname = 'Sicarius'
    keywords = ['Character', 'Infantry', 'Captain']

    def __init__(self, parent):
        super(Sicarius, self).__init__(parent=parent, points=armory.get_cost(units.CaptainSicarius),
                                       unique=True, static=True, gear=[
                Gear('Talassarian Tempest Blade'),
                Gear('Artisan plasma pistol'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
            ])


class Tigurius(HqUnit, armory.AstartesUnit):
    type_id = 'tigurius_v1'
    type_name = armory.get_name(units.Tigurius)
    faction = ['Ultramarines']
    power = 7
    kwname = 'Tigurius'
    keywords = ['Character', 'Infantry', 'Librarian', 'Psyker']

    def __init__(self, parent):
        super(Tigurius, self).__init__(parent=parent, points=armory.get_cost(units.Tigurius),
                                       unique=True, static=True, gear=[
                Gear('Rod of Tigurius'),
                Gear('Bolt pistol'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
            ])


class Cassius(HqUnit, armory.AstartesUnit):
    type_id = 'cassius_v3'
    type_name = armory.get_name(units.ChaplainCassius)
    faction = ['Ultramarines']
    power = 5
    kwname = 'Cassius'
    keywords = ['Character', 'Infantry', 'Chaplain']

    def __init__(self, parent):
        super(Cassius, self).__init__(parent=parent, points=armory.get_cost(units.ChaplainCassius),
                                      unique=True, static=True, gear=[
                Gear('Infernus'),
                Gear('Bolt pistol'),
                Gear('Crozius Arcanum'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
            ])


class Telion(HqUnit, armory.AstartesUnit):
    type_id = 'telion_v1'
    type_name = armory.get_name(units.SergeantTelion)
    faction = ['Ultramarines']
    power = 4
    kwname = 'Telion'
    keywords = ['Character', 'Infantry', 'Scout']

    def __init__(self, parent):
        super(Telion, self).__init__(parent, points=armory.get_cost(units.SergeantTelion), gear=[
            Gear('Quietus'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ], static=True, unique=True)


class Chronus(HqUnit, armory.AstartesUnit):
    type_id = 'chronus_v1'
    type_name = armory.get_name(units.SergeantChronus)
    faction = ['Ultramarines']
    power = 2
    kwname = 'Chronus'
    keywords = ['Character', 'Infantry']

    def __init__(self, parent):
        super(Chronus, self).__init__(parent, points=armory.get_cost(units.SergeantChronus), gear=[
            Gear('Bolt pistol'),
            Gear('Servo-arm'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ], static=True, unique=True)


class Lysander(HqUnit, armory.AstartesUnit):
    type_id = 'lysander_v1'
    type_name = armory.get_name(units.CaptainLysander)

    faction = ['Imperial Fists']
    power = 8
    kwname = 'Lysander'
    keywords = ['Character', 'Infantry', 'Captain', 'Terminator']

    def __init__(self, parent):
        super(Lysander, self).__init__(parent=parent, points=armory.get_cost(units.CaptainLysander), gear=[
            Gear('The Fist of Dorn'),
        ], unique=True, static=True)


class Kantor(HqUnit, armory.AstartesUnit):
    type_id = 'kantor_v1'
    type_name = armory.get_name(units.PedroKantor)
    faction = ['Crimson Fists']
    power = 9

    keywords = ['Character', 'Infantry', 'Chapter master']

    def __init__(self, parent):
        super(Kantor, self).__init__(parent=parent, points=armory.get_cost(units.PedroKantor),
                                     static=True, gear=[
                Gear("Dorn's Arrow"),
                Gear('Power fist'),
                Gear('Frag grenades'),
                Gear('Krak grenades')
            ], unique=True)


class Helbrecht(HqUnit, armory.AstartesUnit):
    type_id = 'helbrecht_v1'
    type_name = armory.get_name(units.Helbrecht)
    faction = ['Black Templars']
    power = 9

    keywords = ['Character', 'Infantry', 'Chapter master']

    def __init__(self, parent):
        super(Helbrecht, self).__init__(parent=parent, points=armory.get_cost(units.Helbrecht), gear=[
            Gear('Sword of High Marshals'),
            Gear('Combi-melta'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ], unique=True, static=True)


class Champion(HqUnit, armory.AstartesUnit):
    type_id = 'champion_v1'
    type_name = armory.get_name(units.EmperorsChampion)
    faction = ['Black Templars']
    power = 6

    keywords = ['Character', 'Infantry']

    def __init__(self, parent):
        super(Champion, self).__init__(parent=parent, points=armory.get_cost(units.EmperorsChampion), gear=[
            Gear('Black Sword'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ], unique=True, static=True)


class Grimaldus(HqUnit, armory.AstartesUnit):
    type_id = 'grimaldus_v1'
    type_name = armory.get_name(units.ChaplainGrimaldus)
    faction = ['Black Templars']
    power = 6

    kwname = 'Grimaldus'
    keywords = ['Character', 'Infantry', 'Chaplain']

    def __init__(self, parent):
        super(Grimaldus, self).__init__(parent=parent, points=armory.get_cost(units.ChaplainGrimaldus), gear=[
            Gear('Crozius arcanum'),
            Gear('Plasma pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ], unique=True, static=True)


class Shrike(HqUnit, armory.AstartesUnit):
    type_id = 'shrike_v1'
    type_name = armory.get_name(units.KayvaanShrike)
    faction = ['Raven Guard']
    kwname = 'Shrike'
    keywords = ['Character', 'Infantry', 'Chapter Master', 'Jump pack', 'Fly']
    power = 8

    def __init__(self, parent):
        super(Shrike, self).__init__(parent=parent, points=armory.get_cost(units.KayvaanShrike), gear=[
            Gear('Raven\'s Talons'),
            Gear('Bolt Pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ], unique=True, static=True)


class PrimarisShrike(HqUnit, armory.AstartesUnit):
    type_id = 'shrike_v2'
    type_name = armory.get_name(units.PrimarisShrike) + ' (Primaris)'
    faction = ['Raven Guard']
    kwname = 'Kayvaan Shrike'
    keywords = ['Character', 'Infantry', 'Chapter Master', 'Jump pack', 'Fly', 'Phobos', 'Primaris']
    power = 7

    def __init__(self, parent):
        super(PrimarisShrike, self).__init__(parent=parent, name=armory.get_name(units.PrimarisShrike), points=armory.get_cost(units.PrimarisShrike), gear=[
            Gear('Raven\'s Talons'),
            Gear('Blackout'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ], static=True)

    def get_unique(self):
        return 'Kayvaan Shrike'


class Vulkan(HqUnit, armory.AstartesUnit):
    type_id = 'vulkan_v1'
    type_name = armory.get_name(units.VulkanHestan)
    faction = ['Salamanders']
    keywords = ['Character', 'Infantry', 'Captain']
    power = 8

    def __init__(self, parent):
        super(Vulkan, self).__init__(parent=parent, points=armory.get_cost(units.VulkanHestan),
                                     unique=True, static=True, gear=[
                Gear('The Gauntlet of the Forge'),
                Gear('The Spear of Vulkan'),
                Gear('Bolt Pistol'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
            ])


class Khan(HqUnit, armory.AstartesUnit):
    type_id = 'khan_v1'
    type_name = armory.get_name(units.KorsarroKhan)
    faction = ['White Scars']
    power = 6
    keywords = ['Character', 'Infantry', 'Captain']

    def __init__(self, parent):
        super(Khan, self).__init__(parent=parent, static=True, gear=[
            Gear('Moonfang'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades')
        ], points=armory.get_cost(units.KorsarroKhan))

    def get_unique(self):
        return "KOR'SARRO KHAN"


class PrimarisKhan(HqUnit, armory.AstartesUnit):
    type_id = 'primaris_khan_v1'
    type_name = armory.get_name(units.PrimarisKhan) + ' (Primaris)'
    faction = ['White Scars']
    power = 6
    keywords = ['Character', 'Infantry', 'Captain']

    def __init__(self, parent):
        super(Khan, self).__init__(parent=parent, name=armory.get_name(units.PrimarisKhan),
                                   static=True, gear=[
                                       Gear('Moonfang'),
                                       Gear('Bolt pistol'),
                                       Gear('Frag grenades'),
                                       Gear('Krak grenades')
                                   ], points=armory.get_cost(units.PrimarisKhan))

    def get_unique(self):
        return "KOR'SARRO KHAN"


class KhanOnBike(HqUnit, armory.AstartesUnit):
    type_id = 'khan_on_bike_v1'
    type_name = armory.get_name(units.KhanOnBike)
    faction = ['White Scars']
    power = 6
    keywords = ['Character', 'Biker', 'Captain']

    def __init__(self, parent):
        super(KhanOnBike, self).__init__(parent=parent, static=True, gear=[
            Gear('Twin boltgun'),
            Gear('Khan\'s spear'),
            Gear('Frag grenades'),
            Gear('Krak grenades')
        ], points=armory.get_cost(units.KhanOnBike))


class BikeKhan(HqUnit, armory.AstartesUnit):
    type_id = 'khan_bike_v1'
    type_name = "Kor'sarro Khan on Moondrakkan"
    faction = ['White Scars']
    power = 7
    kwname = "Kor'sarro Khan"
    keywords = ['Character', 'Biker', 'Captain']

    def __init__(self, parent):
        super(BikeKhan, self).__init__(parent=parent, static=True, gear=[
            Gear('Moonfang'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Twin boltgun')
        ], points=132)

    def get_unique(self):
        return "KOR'SARRO KHAN"


class PrimarisCaptain(HqUnit, armory.SMUnit):
    type_name = armory.get_name(units.PrimarisCaptain)
    type_id = 'primaris_captain_v1'
    kwname = 'Captain'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 5

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(PrimarisCaptain.RangedWeapon, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.MCAutoBoltRifle)
            self.variant(*ranged.MCStalkerBoltRifle)

    class Melee(OptionsList):
        def __init__(self, parent):
            super(PrimarisCaptain.Melee, self).__init__(parent, 'Melee weapon', limit=1)
            self.variant(*melee.PowerSword)
            fist_gear = [melee.PowerFist, ranged.PlasmaPistol]
            self.fist = self.variant(armory.join_and(*fist_gear), armory.get_costs(*fist_gear),
                                     gear=armory.create_gears(*fist_gear))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]
        super(PrimarisCaptain, self).__init__(parent,
                                              points=armory.points_price(armory.get_cost(units.PrimarisCaptain), *gear),
                                              gear=armory.create_gears(*gear))
        self.rng = self.RangedWeapon(self)
        self.mle = self.Melee(self)

    def check_rules(self):
        super(PrimarisCaptain, self).check_rules()
        self.rng.used = self.rng.visible = not self.mle.fist.value


class PrimarisLibrarian(HqUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.PrimarisLibrarian)
    type_id = 'primaris_librarian_v1'
    kwname = 'Librarian'
    keywords = ['Character', 'Infantry', 'Primaris', 'Psyker']
    power = 5

    @classmethod
    def calc_faction(cls):
        # magic slice to exclude black templars and space wolves
        return super(PrimarisLibrarian, cls).calc_faction()[1:-1] + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [melee.ForceSword, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = armory.points_price(armory.get_cost(units.PrimarisLibrarian), *gear)
        super(PrimarisLibrarian, self).__init__(parent, points=cost, gear=armory.create_gears(*gear),
                                                static=True)


class PrimarisChaplain(HqUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.PrimarisChaplain)
    type_id = 'primaris_chaplain_v1'
    kwname = 'Chaplain'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 4

    @classmethod
    def calc_faction(cls):
        return super(PrimarisChaplain, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [melee.CroziusArcanum, ranged.AbsolverBoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = armory.points_price(armory.get_cost(units.PrimarisChaplain), *gear)
        super(PrimarisChaplain, self).__init__(parent, points=cost, gear=armory.create_gears(*gear),
                                               static=True)


class GabrielAngelos(HqUnit, armory.AstartesUnit):
    type_id = 'angelos_v1'
    type_name = armory.get_name(units.GabrielAngelos)
    faction = ['Blood Ravens']
    keywords = ['Character', 'Infantry', 'Chapter Master', 'Terminator']
    power = 9

    def __init__(self, parent):
        super(GabrielAngelos, self).__init__(parent=parent, points=armory.get_cost(units.GabrielAngelos), gear=[
            Gear('God-splitter')
        ], unique=True, static=True)


class PrimarisTigurius(HqUnit, armory.AstartesUnit):
    type_id = 'primaris_tigurius_v1'
    type_name = armory.get_name(units.Tigurius)
    faction = ['Ultramarines']
    power = 7
    kwname = 'Tigurius'
    keywords = ['Character', 'Infantry', 'Librarian', 'Psyker', 'Primaris', 'Chief Librarian']

    def __init__(self, parent):
        super(Tigurius, self).__init__(parent=parent, points=armory.get_cost(units.Tigurius),
                                       unique=True, static=True, gear=[
                Gear('Rod of Tigurius'),
                Gear('Bolt pistol'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
            ])


class Feirros(HqUnit, armory.AstartesUnit):
    type_id = 'feirros_v1'
    type_name = armory.get_name(units.Feirros)
    faction = ['Iron Hands']
    kwname = 'Feirros'
    keywords = ['Character', 'Infantry', 'Mk X Gravis', 'Primaris', 'Iron father', 'Master of the Forge', 'Techmarine']
    power = 6

    def __init__(self, parent):
        super(Feirros, self).__init__(parent=parent, points=armory.get_cost(units.Feirros),
                                     unique=True, static=True, gear=[
                Gear('Bolt pistol'),
                Gear("Gorgon's wrath"),
                Gear('Harrowhand'),
                Gear('Servo arm'),
                Gear('Servo arm'),
            ])
