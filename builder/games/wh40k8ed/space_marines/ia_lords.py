from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
from . import armory, ia_ranged, ia_units, ia_melee
from builder.games.wh40k8ed.utils import *


class RelicSpartan(LordsUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.SpartanAssaultTank) + ' (Imperial Armor)'
    type_id = 'ia_relic_spartan_v1'
    keywords = ['Titanic', 'Vehicle', 'Relic', 'Transport']
    power = 23

    @classmethod
    def calc_faction(cls):
        return super(RelicSpartan, cls).calc_faction() + ['DEATHWATCH']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(RelicSpartan.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ia_ranged.QuadLascannon)
            self.variant(*ia_ranged.LaserDestroyer)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(RelicSpartan.Weapon2, self).__init__(parent, '')
            self.variant(*ia_ranged.TwinHeavyBolter)
            self.variant(*ia_ranged.TwinHeavyFlamer)

    def __init__(self, parent):
        super(RelicSpartan, self).__init__(parent, get_name(ia_units.SpartanAssaultTank),
                                           get_cost(ia_units.SpartanAssaultTank),
                                           gear=[Gear('Crushing tracks')])
        self.Weapon1(self)
        self.Weapon1(self)
        self.Weapon2(self)


class RelicTyphon(LordsUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.TyphonHeavySiegeTank) + ' (Imperial Armor)'
    type_id = 'ia_relic_typhon_v1'
    keywords = ['Titanic', 'Vehicle', 'Relic']
    power = 27

    @classmethod
    def calc_faction(cls):
        return super(RelicTyphon, cls).calc_faction() + ['DEATHWATCH']

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(RelicTyphon.Sponsons, self).__init__(parent, 'Sponson weapons', limit=1)
            self.variant('Two lascannons', 2 * get_cost(ia_ranged.Lascannon),
                         gear=2 * create_gears(ia_ranged.Lascannon))
            self.variant('Two heavy bolters', 2 * get_cost(ia_ranged.HeavyBolter),
                         gear=2 * create_gears(ia_ranged.HeavyBolter))

    class Extra(OptionsList):
        def __init__(self, parent):
            super(RelicTyphon.Extra, self).__init__(parent, '', limit=1)
            self.variant(*ia_ranged.HeavyBolter)
            self.variant(*ia_ranged.HeavyFlamer)
            self.variant(*ia_ranged.MultiMelta)
            self.variant(*ia_ranged.StormBolter)

    def __init__(self, parent):
        gear = [ia_ranged.DreadhammerSiegeCannon, ia_melee.CrushingTracks]
        cost = points_price(get_cost(ia_units.TyphonHeavySiegeTank), *gear)
        super(RelicTyphon, self).__init__(parent, get_name(ia_units.TyphonHeavySiegeTank), cost,
                                          gear=create_gears(*gear))
        self.Extra(self)
        self.Sponsons(self)


class RelicCerberus(LordsUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.CerberusHeavyTankDestroyer) + ' (Imperial Armor)'
    type_id = 'ia_relic_cerberus_v1'
    keywords = ['Titanic', 'Vehicle', 'Relic']
    power = 26

    @classmethod
    def calc_faction(cls):
        return super(RelicCerberus, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ia_ranged.HeavyNeutronPulseArray, ia_melee.CrushingTracks]
        cost = points_price(get_cost(ia_units.CerberusHeavyTankDestroyer), *gear)
        super(RelicCerberus, self).__init__(parent, get_name(ia_units.CerberusHeavyTankDestroyer), cost,
                                            gear=create_gears(*gear))
        RelicTyphon.Extra(self)
        RelicTyphon.Sponsons(self)


class RelicFellblade(LordsUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.FellbladeSuperHeavyTank) + ' (Imperial Armor)'
    type_id = 'ia_relic_fellblade_v1'
    keywords = ['Titanic', 'Vehicle', 'Relic']
    power = 35

    @classmethod
    def calc_faction(cls):
        return super(RelicFellblade, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ia_ranged.FellbladeAcceleratorCannon, ia_ranged.DemolisherCannon, ia_melee.CrushingTracks]
        cost = points_price(get_cost(ia_units.FellbladeSuperHeavyTank), *gear)
        super(RelicFellblade, self).__init__(parent, get_name(ia_units.FellbladeSuperHeavyTank), cost,
                                             gear=create_gears(*gear))
        RelicTyphon.Extra(self)
        RelicSpartan.Weapon1(self)
        RelicSpartan.Weapon1(self)
        RelicSpartan.Weapon2(self)


class RelicFalchion(LordsUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.FalchionSuperHeavyTank) + ' (Imperial Armor)'
    type_id = 'ia_relic_falchion_v1'
    keywords = ['Titanic', 'Vehicle', 'Relic']
    power = 40

    @classmethod
    def calc_faction(cls):
        return super(RelicFalchion, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ia_ranged.TwinVolcanoCannon, ia_melee.CrushingTracks]
        cost = points_price(get_cost(ia_units.FalchionSuperHeavyTank), *gear)
        super(RelicFalchion, self).__init__(parent, get_name(ia_units.FalchionSuperHeavyTank), cost,
                                            gear=create_gears(*gear))
        RelicTyphon.Extra(self)
        RelicSpartan.Weapon1(self)
        RelicSpartan.Weapon1(self)
        RelicSpartan.Weapon2(self)


class RelicMastodon(LordsUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.MastodonSuperHeavyTransport) + ' (Imperial Armor)'
    type_id = 'ia_relic_mastodon_v1'
    keywords = ['Titanic', 'Vehicle', 'Relic', 'Transport']
    power = 44

    @classmethod
    def calc_faction(cls):
        return super(RelicMastodon, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ia_ranged.HeavyFlamer, ia_ranged.HeavyFlamer, ia_ranged.Lascannon,
                ia_ranged.Lascannon, ia_ranged.SkyreaperBattery, ia_ranged.SiegeMeltaArray,
                ia_melee.CrushingTracks]
        cost = points_price(get_cost(ia_units.MastodonSuperHeavyTransport), *gear)
        super(RelicMastodon, self).__init__(parent, get_name(ia_units.MastodonSuperHeavyTransport), cost,
                                            gear=create_gears(*gear), static=True)

