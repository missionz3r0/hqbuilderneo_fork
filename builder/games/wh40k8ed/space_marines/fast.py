from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList,\
    UnitList, ListSubUnit, SubUnit, OptionalSubUnit
from . import armory, ranged, melee, units, wargear, old_armory
from builder.games.wh40k8ed.utils import *


class AssaultSquad(FastUnit, armory.SMUnit):
    type_name = 'Assault squad' + ' (Index)'
    type_id = 'assault_squad_v1'
    keywords = ['Infantry']
    obsolete = True

    armory = old_armory

    model_points = 13
    model_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    @classmethod
    def calc_faction(cls):
        return super(AssaultSquad, cls).calc_faction() +\
            ['DARK ANGELS', '<DARK ANGELS SUCCESSORS>']

    class Sergeant(Unit):
        type_name = 'Space Marine Sergeant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.Weapon1, self).__init__(parent, 'Weapon')
                self.parent.parent.armory.add_pistol_options(self)
                self.parent.parent.armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.Weapon2, self).__init__(parent, '')
                self.parent.parent.armory.add_melee_weapons(self)
                self.evs = self.variant('Eviscerator', 22)

        class Options(OptionsList):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.combatshiled = self.variant('Combat shield', 4)

        def __init__(self, parent):
            super(AssaultSquad.Sergeant, self).__init__(
                parent=parent, points=AssaultSquad.model_points,
                gear=AssaultSquad.model_gear
            )
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(AssaultSquad.Sergeant, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.has_evs()

        def build_points(self):
            res = super(AssaultSquad.Sergeant, self).build_points()
            if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
                res -= 5
            return res

        def has_evs(self):
            return self.wep2.cur == self.wep2.evs

    class Jumppack(OptionsList):
        def __init__(self, parent):
            super(AssaultSquad.Jumppack, self).__init__(parent=parent, name='Movement options')
            self.jumppack = self.variant('Jump Packs', 3, per_model=True)

    # def build_points(self):
    #     return super(AssaultSquad, self).build_points() + self.pack.points * self.marines.cur * self.pack.used

    def __init__(self, parent):
        super(AssaultSquad, self).__init__(parent=parent, name='Assault Squad')
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.flame = Count(self, 'Flamer', 0, 2, 9)
        self.pp = Count(self, 'Plasma pistol', 0, 2, 7)
        self.evs = Count(self, 'Eviscerator', 0, 2, 22)
        self.pack = self.Jumppack(parent=self)

    def check_rules(self):
        super(AssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.pp])
        evs_max = (self.get_count() / 5) - self.sergeant.unit.has_evs()
        self.evs.used = self.evs.visible = evs_max > 0
        self.evs.max = evs_max

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points,
                               count=self.get_count(), options=self.pack.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=AssaultSquad.model_gear
        )
        if self.pack.any and self.pack.used:
            marine.add(Gear('Jump Pack'))
            marine.add_points(self.pack.points)
        if self.evs.cur:
            desc.add(marine.clone().
                     add(Gear('Eviscerator')).
                     set_count(self.evs.cur))
        count = self.marines.cur - self.evs.cur
        if self.flame.cur:
            desc.add_dup(marine.clone().add(Gear('Flamer')).set_count(self.flame.cur))
            count -= self.flame.cur
        if self.pp.cur:
            desc.add_dup(marine.clone().add([Gear('Chainsword'), Gear('Plasma pistol')]).set_count(self.pp.cur))
            count -= self.pp.cur
        desc.add(marine.add([Gear('Chainsword'), Gear('Bolt pistol')]).set_count(count))
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return 1 + (4 + (self.pack.any)) * (1 + self.marines.cur > 4)

    def build_points(self):
        return super(AssaultSquad, self).build_points() + self.pack.points * self.marines.cur


class CodexAssaultSquad(FastUnit, armory.SMUnit):
    type_name = armory.get_name(units.AssaultSquad)
    type_id = 'assault_squad_v2'
    keywords = ['Infantry']

    model_points = armory.get_cost(units.AssaultSquad)
    model_gear = [Gear('Frag grenades'), Gear('Krak grenades')]
    power = 4

    class Sergeant(armory.ClawUser):
        type_name = 'Space Marine Sergeant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(CodexAssaultSquad.Sergeant.Weapon1, self).__init__(parent, 'Weapon')
                armory.add_pistol_options(self)
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(CodexAssaultSquad.Sergeant.Weapon2, self).__init__(parent, '')
                armory.add_melee_weapons(self)
                self.evs = self.variant(*melee.Eviscerator)

        class Options(OptionsList):
            def __init__(self, parent):
                super(CodexAssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant(*ranged.MeltaBombs)
                self.combatshiled = self.variant(*wargear.CombatShield)

        def __init__(self, parent):
            super(CodexAssaultSquad.Sergeant, self).__init__(
                parent=parent, points=AssaultSquad.model_points,
                gear=AssaultSquad.model_gear
            )
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(CodexAssaultSquad.Sergeant, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.has_evs()

        def has_evs(self):
            return self.wep2.cur == self.wep2.evs

    class Jumppack(OptionsList):
        def __init__(self, parent):
            super(CodexAssaultSquad.Jumppack, self).__init__(parent=parent, name='Movement options')
            self.jumppack = self.variant(
                'Jump Packs',
                armory.get_cost(units.JumpAssaultSquad) - armory.get_cost(units.AssaultSquad),
                per_model=True)

    # def build_points(self):
    #     return super(AssaultSquad, self).build_points() + self.pack.points * self.marines.cur * self.pack.used

    def __init__(self, parent):
        super(CodexAssaultSquad, self).__init__(parent=parent,
                                                name=armory.get_name(units.AssaultSquad))
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.marines = Count(parent=self, name='Space Marine',
                             min_limit=4, max_limit=9,
                             points=self.model_points,
                             per_model=True)
        self.flame = Count(self, 'Flamer', 0, 2,
                           armory.get_cost(ranged.Flamer))
        self.pp = Count(self, 'Plasma pistol', 0, 2,
                        armory.get_cost(ranged.PlasmaPistol))
        self.evs = Count(self, 'Eviscerator', 0, 2,
                         armory.get_cost(melee.Eviscerator))
        self.pack = self.Jumppack(parent=self)

    def check_rules(self):
        super(CodexAssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.pp])
        evs_max = (self.get_count() / 5) - self.sergeant.unit.has_evs()
        self.evs.used = self.evs.visible = evs_max > 0
        self.evs.max = evs_max

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points,
                               count=self.get_count(), options=self.pack.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=AssaultSquad.model_gear
        )
        if self.pack.any and self.pack.used:
            marine.add(Gear('Jump Pack'))
            marine.add_points(self.pack.points)
        if self.evs.cur:
            desc.add(marine.clone().
                     add(Gear('Eviscerator')).
                     set_count(self.evs.cur))
        count = self.marines.cur - self.evs.cur
        if self.flame.cur:
            desc.add_dup(marine.clone().add(Gear('Flamer')).set_count(self.flame.cur))
            count -= self.flame.cur
        if self.pp.cur:
            desc.add_dup(marine.clone().add([Gear('Chainsword'), Gear('Plasma pistol')]).set_count(self.pp.cur))
            count -= self.pp.cur
        desc.add(marine.add([Gear('Chainsword'), Gear('Bolt pistol')]).set_count(count))
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return 1 + ((self.power - 1) + (self.pack.any)) * (1 + self.marines.cur > 4)

    def build_points(self):
        return super(CodexAssaultSquad, self).build_points() + self.pack.points * self.marines.cur


class Inceptors(FastUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.InceptorSquad)
    type_id = 'inceptors_v1'

    keywords = ['Infantry', 'Primaris', 'Jump pack', 'Mk X gravis', 'Fly']

    power = 7

    @classmethod
    def calc_faction(cls):
        return super(Inceptors, cls).calc_faction() + ['DEATHWATCH']

    class Weapons(OneOf):
        def __init__(self, parent):
            super(Inceptors.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Two assault bolters', armory.get_costs(*[ranged.AssaultBolter] * 2),
                         gear=armory.create_gears(*[ranged.AssaultBolter] * 2))
            self.variant('Two plasma exterminators', armory.get_costs(*[ranged.PlasmaExterminator] * 2),
                         gear=armory.create_gears(*[ranged.PlasmaExterminator] * 2))

        @property
        def description(self):
            return [UnitDescription('Inceptor Sergeant').add(self.cur.gear)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Inceptor').add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(Inceptors, self).__init__(parent, points=armory.get_cost(units.InceptorSquad))
        self.wep = self.Weapons(self)
        self.models = self.Marines(self, 'Inceptors', 2, 5, points=armory.get_cost(units.InceptorSquad),
                                   per_model=True)

    def get_count(self):
        return 1 + self.models.cur

    def build_points(self):
        return super(Inceptors, self).build_points() + self.wep.points * self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))


class ScoutBikers(FastUnit, armory.SMUnit):
    type_name = armory.get_name(units.ScoutBikeSquad)
    type_id = 'scout_bike_squad_v1'
    keywords = ['Biker', 'Scout']

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.AstartesShotgun, melee.CombatKnife]
    model_points = armory.get_cost(units.ScoutBikeSquad)

    armory = armory
    power = 3

    class Sergeant(Unit):
        type_name = 'Scout Biker Sergeant'

        class BikeWeapon(OneOf):
            def __init__(self, parent):
                super(ScoutBikers.Sergeant.BikeWeapon, self).__init__(parent, 'Bike weapon')
                self.variant(*ranged.TwinBoltgun)
                self.variant(*ranged.AstartesGrenadeLauncher)

        def __init__(self, parent):
            gear = ScoutBikers.model_gear
            super(ScoutBikers.Sergeant, self).__init__(
                parent=parent, points=armory.points_price(ScoutBikers.model_points, *gear),
                gear=armory.create_gears(*gear))
            self.wep = parent.armory.SergeantWeapon(self, double=True)
            self.BikeWeapon(self)

    class Bikers(Count):
        @property
        def description(self):
            return [UnitDescription('Scout Biker', options=armory.create_gears(ScoutBikers.model_gear) + [
                Gear('Bolt pistol'), Gear('Twin boltgun')
            ]).set_count(self.cur - self.parent.grenade.cur)]

    def __init__(self, parent):
        super(ScoutBikers, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))

        self.bikers = self.Bikers(self, 'Scout Biker', 2, 8, self.model_points, per_model=True)
        self.grenade = Count(self, 'Astartes grenade launcher', 0, 2, armory.get_cost(ranged.AstartesGrenadeLauncher),
                             gear=UnitDescription('Scout Biker', options=armory.create_gears(ScoutBikers.model_gear) + [
                                 Gear('Bolt pistol'), Gear('Astartes grenade launcher')
                             ]))

    def get_count(self):
        return self.bikers.cur + 1

    def check_rules(self):
        super(ScoutBikers, self).check_rules()
        self.grenade.max = self.bikers.cur

    def build_power(self):
        return self.power * ((self.bikers.cur + 3) / 3)

    def build_points(self):
        # take twin boltgun costs into account
        return super(ScoutBikers, self).build_points() + get_cost(ranged.TwinBoltgun) * (self.bikers.cur - self.grenade.cur)


class BikeSquad(FastUnit, armory.SMUnit):
    type_name = armory.get_name(units.BikeSquad)
    type_id = 'bike_squad_v1'
    keywords = ['Biker']

    armory = armory
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.TwinBoltgun]
    model_points = armory.points_price(armory.get_cost(units.BikeSquad),
                                       *model_gear)
    attack_points = armory.get_cost(units.AttackBikes)
    power = 4

    class Sergeant(Unit):
        type_name = 'Biker Sergeant'

        def __init__(self, parent):
            super(BikeSquad.Sergeant, self).__init__(
                parent=parent, points=parent.model_points,
                gear=armory.create_gears(*BikeSquad.model_gear)
            )
            self.wep = parent.armory.SergeantWeapon(self, double=True)

    class AttackBike(Unit):
        type_name = 'Attack Bike'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BikeSquad.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant(*ranged.HeavyBolter)
                self.multimelta = self.variant(*ranged.MultiMelta)

        def __init__(self, parent):
            cost = armory.points_price(parent.attack_points, ranged.TwinBoltgun)
            super(BikeSquad.AttackBike, self).__init__(
                parent=parent, points=cost,
                gear=[Gear('Twin boltgun'), UnitDescription('Space Marine', count=2,
                                                            options=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                                     Gear('Bolt pistol')])])
            self.wep = self.Weapon(self)

    class OptUnits(OptionalSubUnit):
        def __init__(self, parent):
            super(BikeSquad.OptUnits, self).__init__(parent=parent, name='')
            self.attackbike = SubUnit(self, BikeSquad.AttackBike(parent=parent))

    class Biker(Count):
        @property
        def description(self):
            return[UnitDescription('Space Marine Biker',
                                   options=armory.create_gears(*BikeSquad.model_gear) + [Gear('Bolt pistol')])\
                   .set_count(self.cur - self.parent.sword.cur - sum(c.cur for c in self.parent.spec))]

    def variant(self, name, points):
        return Count(self, name, 0, 2, points,
                     gear=UnitDescription('Space Marine Biker', options=armory.create_gears(*BikeSquad.model_gear) + [Gear(name)]))

    def __init__(self, parent):
        super(BikeSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.opt_units = self.OptUnits(self)

        self.bikers = self.Biker(self, 'Space Marine Bikers', 2, 7, self.model_points, per_model=True)
        self.sword = Count(self, 'Chainsword', 0, 2, gear=UnitDescription('Space Marine Biker',
                                                                          options=armory.create_gears(*BikeSquad.model_gear) + [Gear('Chainsword')]))
        self.spec = []
        self.armory.add_special_weapons(self)

    def check_rules(self):
        super(BikeSquad, self).check_rules()
        Count.norm_counts(0, 2, self.spec)
        self.sword.max = self.bikers.cur - sum(c.cur for c in self.spec)

    def get_count(self):
        return self.opt_units.count + self.bikers.cur + 1

    def build_power(self):
        return 3 * self.opt_units.count + ((6 + self.power) if self.bikers.cur > 5 else
                                           ((4 + self.power) if self.bikers.cur > 3 else self.power))


class AttackBikeSquad(FastUnit, armory.SMUnit):
    type_name = armory.get_name(units.AttackBikes)
    type_id = 'attackbike_v1'
    keywords = ['Biker']

    model_points = armory.points_price(armory.get_cost(units.AttackBikes), ranged.TwinBoltgun)
    model = UnitDescription('Attack Bike', options=[
        Gear('Twin boltgun'), Gear('Frag grenades'), Gear('Krak grenades'),
        Gear('Bolt pistol')])
    power = 2

    class Biker(Count):
        @property
        def description(self):
            return[AttackBikeSquad.model.clone().add(Gear('Heavy bolter'))
                   .set_count(self.cur - self.parent.melta.cur)]

    def __init__(self, parent):
        super(AttackBikeSquad, self).__init__(parent)
        self.bikers = self.Biker(self, 'Attack bikes', 1, 3, self.model_points, per_model=True)
        self.melta = Count(self, 'Multi-melta', 0, 1, armory.get_cost(ranged.MultiMelta),
                           gear=AttackBikeSquad.model.clone().add(Gear('Multi-melta')))

    def check_rules(self):
        super(AttackBikeSquad, self).check_rules()
        self.melta.max = self.bikers.cur

    def get_count(self):
        return self.bikers.cur

    def build_power(self):
        return self.power * self.bikers.cur

    def build_points(self):
        # take HB into account
        return super(AttackBikeSquad, self).build_points() + armory.get_cost(ranged.HeavyBolter) * (self.bikers.cur - self.melta.cur)


class LandSpeeders(FastUnit, armory.CodexBAUnit):
    type_name = 'Land Speeders' + ' (Index)'
    type_id = 'land_speeders_1'
    keywords = ['Vehicle', 'Fly']
    power = 6
    obsolete = True

    @classmethod
    def calc_faction(cls):
        return super(LandSpeeders, cls).calc_faction() + ['SPACE WOLVES']

    class LandSpeeder(ListSubUnit):
        type_name = 'Land Speeder'

        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(LandSpeeders.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy Bolter', 10)
                self.heavyflamer = self.variant('Heavy Flamer', 17)
                self.multimelta = self.variant('Multi-melta', 27)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(LandSpeeders.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Optional weapon', limit=1)
                self.assaultcannon = self.variant('Assault Cannon', 21)
                self.heavyflamer = self.variant('Heavy Flamer', 17)
                self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 50)
                self.heavybolter = self.variant('Heavy Bolter', 10)
                self.multimelta = self.variant('Multi-melta', 27)

        def __init__(self, parent):
            super(LandSpeeders.LandSpeeder, self).__init__(parent=parent, points=80)
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

    def __init__(self, parent):
        super(LandSpeeders, self).__init__(parent, 'Land Speeders')
        self.models = UnitList(self, self.LandSpeeder, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class CodexLandSpeeders(FastUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.LandSpeeders)
    type_id = 'land_speeders_v2'
    keywords = ['Vehicle', 'Fly']
    power = 4

    class LandSpeeder(ListSubUnit):
        type_name = 'Land Speeder'

        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(CodexLandSpeeders.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant(*ranged.HeavyBolter)
                self.multimelta = self.variant(*ranged.MultiMelta)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(CodexLandSpeeders.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Optional weapon', limit=1)
                self.assaultcannon = self.variant(*ranged.AssaultCannon)
                self.heavyflamer = self.variant(*ranged.HeavyFlamer)
                self.typhoonmissilelauncher = self.variant(*ranged.TyphoonMissileLauncher)

        def __init__(self, parent):
            super(CodexLandSpeeders.LandSpeeder, self).__init__(parent=parent, points=get_cost(units.LandSpeeders))
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

    def __init__(self, parent):
        super(CodexLandSpeeders, self).__init__(parent, armory.get_name(units.LandSpeeders))
        self.models = UnitList(self, self.LandSpeeder, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class Suppressors(FastUnit, armory.SMUnit):
    type_name = armory.get_name(units.SuppressorSquad)
    type_id = 'supressors_v1'

    keywords = ['Infantry', 'Primaris', 'Jump pack', 'Fly']

    power = 4

    def __init__(self, parent):
        gear = [ranged.AcceleratorAutocannon, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades, wargear.GravChute]
        cost = points_price(get_cost(units.SuppressorSquad), *gear)
        super(Suppressors, self).__init__(parent, points=3 * cost,
                                         gear=[UnitDescription('Suppressor Sergeant', options=create_gears(*gear)),
                                               UnitDescription('Suppressor', options=create_gears(*gear), count=2)],
                                         static=True)

    def get_count(self):
        return 3

