from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList
from builder.games.wh40k8ed.utils import *
from . import armory, units, ranged, melee, wargear, old_armory


class Servitors(ElitesUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.Servitors)
    type_id = 'servitors_v1'
    keywords = ['Infantry']

    power = 2
    model = UnitDescription('Servitor')

    class Servitor(Count):
        @property
        def description(self):
            return Servitors.model.clone().add(armory.create_gears(melee.ServoArm)).\
                set_count(self.cur - sum(c.cur for c in self.parent.wep.counts))

    class SpecialWeapons(object):
        def variant(self, name, point=None):
            self.counts.append(Count(
                self.parent, name, 0, 4, point, gear=Servitors.model.clone().add(Gear(name))))

        def __init__(self, parent):
            self.parent = parent
            self.counts = []
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.PlasmaCannon)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        super(Servitors, self).__init__(parent)
        self.squad = self.Servitor(self, 'Servitor', 4, 4,
                                   per_model=True, points=armory.get_cost(units.Servitors))
        self.squad.visible = False
        self.wep = self.SpecialWeapons(self)

    def check_rules(self):
        super(Servitors, self).check_rules()
        Count.norm_counts(0, 2, self.wep.counts)

    def build_points(self):
        return super(Servitors, self).build_points() +\
                                   armory.get_cost(melee.ServoArm) * (4 - sum(c.cur for c in self.wep.counts))


class Apothecary(ElitesUnit, armory.CodexDAUnit):
    type_name = armory.get_name(units.Apothecary)
    type_id = 'apothecary_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        super(Apothecary, self).__init__(parent, points=armory.get_cost(units.Apothecary),
                                         gear=armory.create_gears(ranged.FragGrenades, ranged.KrakGrenades,
                                                                  ranged.BoltPistol, melee.Chainsword), static=True)


class BikeApothecary(ElitesUnit, armory.SMUnit):
    type_name = 'Apothecary on Bike'
    type_id = 'bike_apothecary_v1'

    keywords = ['Character', 'Biker']
    power = 4

    def __init__(self, parent):
        super(BikeApothecary, self).__init__(parent, points=80 + 2, gear=[
            Gear("Bolt pistol"), Gear("Chainsword"), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Twin boltgun'),
        ], static=True)


class PrimarisApothecary(ElitesUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.PrimarisApothecary)
    type_id = 'primaris_apothecary_v1'
    kwname = 'Apothecary'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 3

    @classmethod
    def calc_faction(cls):
        # magic slicing to leave off Space Wolves
        return super(PrimarisApothecary, cls).calc_faction()[:-1] + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ranged.ReductorPistol, ranged.AbsolverBoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = armory.points_price(armory.get_cost(units.PrimarisApothecary), *gear)
        super(PrimarisApothecary, self).__init__(parent, points=cost, gear=armory.create_gears(*gear),
                                                 static=True)


class CompanyAncient(ElitesUnit, armory.CodexDAUnit):
    type_name = 'Company Ancient' + ' (Index)'
    type_id = 'company_ancient_v1'

    keywords = ['Character', 'Infantry', 'Ancient']
    power = 4
    obsolete = True
    armory = old_armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CompanyAncient.Weapon1, self).__init__(parent, 'Weapon')
            parent.armory.add_pistol_options(self)
            self.variant('Boltgun')
            parent.armory.add_combi_weapons(self)
            parent.armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(CompanyAncient, self).__init__(parent, 'Company Ancient',
                                             points=63, gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.Weapon1(self)


class CodexCompanyAncient(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.CompanyAncient)
    type_id = 'company_ancient_v2'

    keywords = ['Character', 'Infantry', 'Ancient']
    power = 4

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexCompanyAncient.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_pistol_options(self)
            self.variant(ranged.Boltgun)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(CodexCompanyAncient, self).__init__(parent, armory.get_name(units.CompanyAncient),
                                                  points=armory.get_cost(units.CompanyAncient),
                                                  gear=armory.create_gears(ranged.KrakGrenades, ranged.FragGrenades))
        self.Weapon1(self)


class BikeCompanyAncient(ElitesUnit, armory.SMUnit):
    type_name = 'Company Ancient on Bike'
    type_id = 'bike_company_ancient_v1'

    keywords = ['Character', 'Biker', 'Ancient']
    power = 5

    armory = armory

    def __init__(self, parent):
        super(BikeCompanyAncient, self).__init__(parent, points=88 + 2, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Twin boltgun')
        ])
        CompanyAncient.Weapon1(self)


class PrimarisAncient(ElitesUnit, armory.CommonSMUnit):
    type_name = get_name(units.PrimarisAncient)
    type_id = 'primaris_ancient_v1'

    keywords = ['Character', 'Infantry', 'Primaris']
    power = 4

    def __init__(self, parent):
        super(PrimarisAncient, self).__init__(parent, points=get_cost(units.PrimarisAncient), gear=[
            Gear("Bolt rifle"), Gear("Bolt pistol"), Gear('Frag grenades'), Gear('Krak grenades')
        ], static=True)


class TermoAncient(ElitesUnit, armory.CommonSMUnit):
    type_name = get_name(units.TerminatorAncient)
    type_id = 'terminator_ancient_v1'
    kwname = 'Ancient'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 5

    def __init__(self, parent):
        gear = [ranged.StormBolter, melee.PowerFist]
        super(TermoAncient, self).__init__(
            parent, points=points_price(get_cost(units.TerminatorAncient), *gear),
            gear=create_gears(*gear), static=True)


class CompanyChampion(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.CompanyChampion)
    type_id = 'company_champion_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol,
                melee.MCPowerSword, wargear.CombatShield]
        cost = armory.points_price(armory.get_cost(units.CompanyChampion), *gear)
        super(CompanyChampion, self).__init__(parent, points=cost,
                                              gear=armory.create_gears(*gear),
                                              static=True)


class BikeCompanyChampion(ElitesUnit, armory.SMUnit):
    type_name = 'Company Champion on Bike'
    type_id = 'bike_company_champion_v1'

    keywords = ['Character', 'Biker']
    power = 5

    def __init__(self, parent):
        super(BikeCompanyChampion, self).__init__(parent, points=80 + 10 + 2, gear=[
            Gear("Bolt pistol"), Gear("Master-crafted power sword"), Gear('Twin boltgun'), Gear('Frag grenades'),
            Gear('Krak grenades')
        ], static=True)


class CompanyVeterans(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.CompanyVeterans)
    type_id = 'company_veterans_v1'
    keywords = ['Infantry']

    armory = armory
    power = 3

    class Sergeant(armory.ClawUser):
        type_name = 'Veteran Sergeant'

        def __init__(self, parent):
            super(CompanyVeterans.Sergeant, self).__init__(parent, points=armory.get_cost(units.CompanyVeterans),
                                                           gear=[Gear('Frag grenades'), Gear('Krak grenades')])

            class Sword(parent.armory.SergeantWeapon):
                def add_double(self, sword):
                    sword = [self.variant(*melee.Chainsword)]
                    super(Sword, self).add_double(sword)
                    self.double += sword
            self.wep1 = parent.armory.SergeantWeapon(self, double=True)
            self.wep2 = Sword(self, double=True, sword=False)

        def check_rules(self):
            super(CompanyVeterans.Sergeant, self).check_rules()
            self.parent.parent.armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Veteran(armory.ClawUser, ListSubUnit):
        type_name = 'Space Marine Veteran'

        class Pistols(OneOf):
            def __init__(self, parent):
                super(CompanyVeterans.Veteran.Pistols, self).__init__(parent, 'Pistols')
                self.parent.root_unit.armory.add_pistol_options(self)
                self.variant(*wargear.StormShield)
                self.parent.root_unit.armory.add_melee_weapons(self)

        class Melee(OneOf):
            def __init__(self, parent):
                super(CompanyVeterans.Veteran.Melee, self).__init__(parent, 'Melee weapon')
                self.parent.root_unit.armory.add_melee_weapons(self)
                self.variant(*wargear.StormShield)
                self.variant(*ranged.Boltgun)
                self.parent.root_unit.armory.add_pistol_options(self)
                self.parent.root_unit.armory.add_combi_weapons(self)
                self.parent.root_unit.armory.add_special_weapons(self)

        def __init__(self, parent):
            super(CompanyVeterans.Veteran, self).__init__(parent, points=armory.get_cost(units.CompanyVeterans),
                                                          gear=[Gear('Frag grenades'), Gear('Krak grenades')])
            self.wep1 = self.Pistols(self)
            self.wep2 = self.Melee(self)

    def get_leader(self):
        return self.Sergeant

    def get_member(self):
        return self.Veteran

    def __init__(self, parent):
        super(CompanyVeterans, self).__init__(parent)
        self.ldr = SubUnit(self, self.get_leader()(parent=self))
        self.models = UnitList(self, self.get_member(), 1, 4)

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + 5 * (self.models.count > 1)


class BikerCompanyVeterans(ElitesUnit, armory.SMUnit):
    type_name = 'Company Veterans on Bikes'
    type_id = 'biker_company_veterans_v1'
    keywords = ['Biker']

    class Sword(armory.SergeantWeapon):
        def add_double(self, sword):
            sword = [self.variant('Chainsword', 0)]
            super(BikerCompanyVeterans.Sword, self).add_double(sword)
            self.double += sword

    class Sergeant(Unit):
        type_name = 'Veteran Biker Sergeant'

        def __init__(self, parent):
            super(BikerCompanyVeterans.Sergeant, self).__init__(parent, points=34 + 2,
                                                                gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                                      Gear('Twin boltgun')])
            self.wep1 = armory.SergeantWeapon(self, double=True)
            self.wep2 = BikerCompanyVeterans.Sword(self, double=True, sword=False)

        def check_rules(self):
            super(BikerCompanyVeterans.Sergeant, self).check_rules()
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

        def build_points(self):
            res = super(BikerCompanyVeterans.Sergeant, self).build_points()
            if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
                res -= 5
            return res

    class Veteran(ListSubUnit):
        type_name = 'Space Marine Veteran Biker'

        class Pistols(OneOf):
            def __init__(self, parent):
                super(BikerCompanyVeterans.Veteran.Pistols, self).__init__(parent, 'Pistols')
                armory.add_pistol_options(self)
                self.variant('Storm Shield', 5)
                armory.add_melee_weapons(self)

        class Melee(OneOf):
            def __init__(self, parent):
                super(BikerCompanyVeterans.Veteran.Melee, self).__init__(parent, 'Melee weapon')
                armory.add_melee_weapons(self)
                self.variant('Storm Shield', 5)
                self.variant('Boltgun')
                armory.add_pistol_options(self)
                armory.add_combi_weapons(self)
                armory.add_special_weapons(self)

        def __init__(self, parent):
            super(BikerCompanyVeterans.Veteran, self).__init__(parent, points=34 + 2,
                                                               gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                                     Gear('Twin boltgun')])
            self.pistols = self.Pistols(self)
            self.mle = self.Melee(self)

        def build_points(self):
            res = super(BikerCompanyVeterans.Veteran, self).build_points()
            if self.pistols.cur == self.pistols.claw and self.mle.cur == self.mle.claw:
                res -= 5
            return res

    def __init__(self, parent):
        super(BikerCompanyVeterans, self).__init__(parent)
        self.ldr = SubUnit(self, self.Sergeant(parent=self))
        self.models = UnitList(self, self.Veteran, 1, 4)

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return 6 + 8 * (self.models.count > 1)


class ImperialSM(ElitesUnit, armory.CommonSMUnit):
    type_name = 'Imperial Space Marine'
    type_id = 'imperial_sm_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        super(ImperialSM, self).__init__(parent, points=60, gear=[
            Gear("Disintegration pistol"), Gear("Disintegration combi-gun"),
            Gear('Frag grenades'), Gear('Krak grenades')
        ], static=True, unique=True)


class HonourGuard(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.HonourGuard) + ' (Index)'
    type_id = 'honour_v1'

    keywords = ['Infantry']
    power = 2

    model = UnitDescription('Honour Guard', options=[
        Gear('Frag grenades'), Gear('Krak grenades'),
        Gear('Bolt pistol')
    ])

    class GuardWeapon(OneOf):
        def __init__(self, parent):
            super(HonourGuard.GuardWeapon, self).__init__(parent, 'Guard weapon')
            self.variant(*melee.PowerAxe)
            self.variant(*melee.PowerSword)
            self.variant(*melee.PowerLance)
            self.variant(*melee.PowerMaul)
            self.variant(*melee.RelicBlade)

        @property
        def description(self):
            return [self.parent.model.clone().add(self.cur.gear)]

    def __init__(self, parent):
        super(HonourGuard, self).__init__(parent, armory.get_name(units.HonourGuard),
                                          points=armory.get_cost(units.HonourGuard) * 2)
        self.wep = [
            self.GuardWeapon(self),
            self.GuardWeapon(self)
        ]

    def get_count(self):
        return 2

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.count)
        for w in self.wep:
            res.add_dup(w.description)
        return res


class CodexHonourGuard(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.HonourGuard) + ' (Codex)'
    type_id = 'honour_v2'

    keywords = ['Infantry']
    power = 2

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.BoltPistol, melee.PowerAxe, ranged.Boltgun]
    model = UnitDescription('Honour Guard',
                            options=armory.create_gears(*model_gear))

    def __init__(self, parent):
        super(CodexHonourGuard, self).__init__(parent, armory.get_name(units.HonourGuard),
                                               points=2 * armory.points_price(armory.get_cost(units.HonourGuard), *self.model_gear),
                                               gear=[self.model.clone().set_count(2)],
                                               static=True)

    def get_count(self):
        return 2


class ChapterAncient(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.ChapterAncient)
    type_id = 'chapter_ancient_v1'

    keywords = ['Character', 'Infantry', 'Ancient']
    power = 4

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.PowerSword]
        cost = armory.points_price(armory.get_cost(units.ChapterAncient), *gear)
        super(ChapterAncient, self).__init__(parent, points=cost,
                                             gear=armory.create_gears(*gear),
                                             static=True, unique=True)


class ChapterChampion(ElitesUnit, armory.SMUnit):
    type_name = 'Chapter Champion' + ' (Index)'
    type_id = 'chapter_champion_v1'

    keywords = ['Character', 'Infantry']
    power = 4

    class ChampionWeapon(OneOf):
        def __init__(self, parent):
            super(ChapterChampion.ChampionWeapon, self).__init__(parent, 'Weapon')
            self.variant('Power sword', 4)
            self.variant('Power axe', 5)
            self.variant('Power lance', 4)
            self.variant('Power maul', 4)
            self.variant('Thunder hammer', 25)
            self.variant('Relic blade', 21)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(ChapterChampion.Weapon2, self).__init__(parent, '')
            self.variant('Boltgun')
            self.variant("Champion's blade")

    def __init__(self, parent):
        super(ChapterChampion, self).__init__(parent, 'Chapter Champion',
                                              points=65, gear=[
                                                  Gear("Bolt pistol"),
                                                  Gear('Frag grenades'),
                                                  Gear('Krak grenades')
                                              ], unique=True)
        self.ChampionWeapon(self)
        self.Weapon2(self)


class CodexChapterChampion(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.ChapterChampion) + ' (Codex)'
    type_id = 'chapter_champion_v2'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades,
                melee.MCPowerSword, melee.ChampionBlade]
        cost = armory.points_price(armory.get_cost(units.ChapterChampion), *gear)
        super(CodexChapterChampion, self).__init__(parent, armory.get_name(units.ChapterChampion),
                                                   gear=armory.create_gears(*gear),
                                                   points=cost, static=True)


class CenturionAssault(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.CenturionAssaultSquad)
    type_id = 'centurion_assault_squad_v1'
    keywords = ['Infantry', 'Centurion']
    power = 8

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CenturionAssault.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.ial = self.variant(*wargear.CenturionAssaultLaunchers)
            self.hurricanebolter = self.variant(*ranged.HurricaneBolter)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CenturionAssault.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Two flamers', armory.get_cost(ranged.Flamer) * 2,
                         gear=armory.create_gears(ranged.Flamer, ranged.Flamer))
            self.variant('Two meltaguns', armory.get_cost(ranged.Meltagun) * 2,
                         gear=armory.create_gears(ranged.Meltagun, ranged.Meltagun))

    class Centurion(ListSubUnit):

        def __init__(self, parent):
            super(CenturionAssault.Centurion, self).__init__(
                parent=parent, name='Centurion', gear=armory.create_gears(melee.Siegedrills),
                points=armory.get_cost(units.CenturionAssaultSquad))
            self.wep1 = CenturionAssault.Weapon1(self)
            self.wep2 = CenturionAssault.Weapon2(self)

    class Sergeant(Unit):

        def __init__(self, parent):
            super(CenturionAssault.Sergeant, self).__init__(
                parent=parent, name='Centurion Sergeant',
                gear=armory.create_gears(melee.Siegedrills),
                points=armory.get_cost(units.CenturionAssaultSquad))
            self.wep1 = CenturionAssault.Weapon1(self)
            self.wep2 = CenturionAssault.Weapon2(self)

    def __init__(self, parent):
        super(CenturionAssault, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=self))
        self.cent = UnitList(self, self.Centurion, min_limit=2, max_limit=5)

    def get_count(self):
        return self.cent.count + 1

    def build_power(self):
        return self.power * (1 + (self.cent.count > 2))


class SternguardVeteranSquad(ElitesUnit, armory.SMUnit):
    type_name = 'Sternguard Veteran Squad' + ' (Index)'
    type_id = 'sternguard_veteran_squad_v1'
    power = 7
    keywords = ['Infantry']
    obsolete = True

    armory = old_armory

    class Veteran(ListSubUnit):
        type_name = 'Space Marine Veteran'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(SternguardVeteranSquad.Veteran.Weapon, self).__init__(parent, 'Weapon')
                self.bolt = self.variant('Special issue boltgun', 3)
                self.parent.root_unit.armory.add_combi_weapons(self)
                self.variant('Heavy flamer', 17)
                self.parent.root_unit.armory.add_special_weapons(self)
                self.parent.root_unit.armory.add_heavy_weapons(self)

        def __init__(self, parent):
            super(SternguardVeteranSquad.Veteran, self).__init__(
                parent=parent, gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                     Gear('Bolt pistol')], points=16
            )
            self.weapon = self.Weapon(self)

        @ListSubUnit.count_gear
        def count_weapon(self):
            return self.weapon.cur not in ([self.weapon.bolt] + self.weapon.combi)

    class Sergeant(Unit):
        type_name = 'Veteran Sergeant'

        def __init__(self, parent):
            super(SternguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, points=16, gear=[Gear('Frag grenades'), Gear('Krak grenades')]
            )

            class SpecWeapon(parent.armory.SergeantWeapon):
                def add_single(self):
                    self.variant('Special issue boltgun', 3)
                    super(SpecWeapon, self).add_single()
            self.wep1 = SpecWeapon(self, stern=True)
            self.wep2 = parent.armory.SergeantWeapon(self, double=True, stern=True)

        def check_rules(self):
            self.parent.parent.armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

        def build_points(self):
            res = super(SternguardVeteranSquad.Sergeant, self).build_points()
            if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
                res -= 5
            return res

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(SternguardVeteranSquad, self).__init__(parent,
                                                     armory.get_name(units.SternguardVeteranSquad))
        self.leader = SubUnit(self, self.get_leader()(self))
        self.vets = UnitList(self, self.Veteran, 4, 9)

    def check_rules(self):
        spec_total = sum(u.count_weapon() for u in self.vets.units)
        if 2 < spec_total:
            self.error('Veterans can take only 2 special or heavy weapon (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.vets.count + 1

    def build_power(self):
        return self.power * (1 + (self.vets.count > 4))


class CodexSternguardVeteranSquad(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.SternguardVeteranSquad) + ' (Codex)'
    type_id = 'sternguard_veteran_squad_v2'
    power = 6
    keywords = ['Infantry']

    class Veteran(ListSubUnit):
        type_name = 'Space Marine Veteran'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(CodexSternguardVeteranSquad.Veteran.Weapon, self).__init__(parent, 'Weapon')
                self.bolt = self.variant(*ranged.SpecialIssueBoltgun)
                armory.add_combi_weapons(self)
                self.variant(*ranged.HeavyFlamer)
                armory.add_special_weapons(self)
                armory.add_heavy_weapons(self)

        def __init__(self, parent):
            gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]
            super(CodexSternguardVeteranSquad.Veteran, self).__init__(
                parent=parent, gear=armory.create_gears(*gear),
                points=armory.get_cost(units.SternguardVeteranSquad)
            )
            self.weapon = self.Weapon(self)

        @ListSubUnit.count_gear
        def count_weapon(self):
            return self.weapon.cur not in ([self.weapon.bolt] + self.weapon.combi)

    class Sergeant(armory.ClawUser):
        type_name = 'Veteran Sergeant'

        def __init__(self, parent):
            super(CodexSternguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, points=armory.get_cost(units.SternguardVeteranSquad),
                gear=[Gear('Frag grenades'), Gear('Krak grenades')]
            )

            class SpecWeapon(armory.SergeantWeapon):
                def add_single(self):
                    self.variant(*ranged.SpecialIssueBoltgun)
                    super(SpecWeapon, self).add_single()
            self.wep1 = SpecWeapon(self, stern=True)
            self.wep2 = armory.SergeantWeapon(self, double=True, stern=True)

        def check_rules(self):
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(CodexSternguardVeteranSquad, self).__init__(parent,
                                                          armory.get_name(units.SternguardVeteranSquad))
        self.leader = SubUnit(self, self.get_leader()(self))
        self.vets = UnitList(self, self.Veteran, 4, 9)

    def check_rules(self):
        spec_total = sum(u.count_weapon() for u in self.vets.units)
        if 2 < spec_total:
            self.error('Veterans can take only 2 special or heavy weapon (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.vets.count + 1

    def build_power(self):
        return self.power * (1 + (self.vets.count > 4))


class VanguardVeteranSquad(ElitesUnit, armory.SMUnit):
    type_name = 'Vanguard Veteran Squad' + ' (Index)'
    type_id = 'vanguard_veteran_squad_v1'
    obsolete = True
    keywords = ['Infantry']

    armory = old_armory

    class Options(OptionsList):
        def __init__(self, parent):
            super(VanguardVeteranSquad.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.meltabombs = self.variant('Melta bombs', 5)

    class VeteranWeapon(OneOf):
        def __init__(self, parent, name='Weapon', melee=False, relic=False):
            super(VanguardVeteranSquad.VeteranWeapon, self).__init__(parent, name)
            if melee:
                parent.armory.add_melee_weapons(self)
            parent.armory.add_pistol_options(self)
            if not melee:
                parent.armory.add_melee_weapons(self)
            if relic:
                self.variant('Relic blade', 21)
            self.shield = self.variant('Storm shield', 5)

    class Veteran(ListSubUnit):
        type_name = 'Space Marine Veteran'

        def __init__(self, parent):
            super(VanguardVeteranSquad.Veteran, self).__init__(
                parent=parent, points=16, gear=[Gear('Frag grenades'), Gear('Krak grenades')]
            )
            self.armory = self.root_unit.armory

            self.wep1 = VanguardVeteranSquad.VeteranWeapon(self, melee=True)
            self.wep2 = VanguardVeteranSquad.VeteranWeapon(self, '', melee=False)
            self.opt = VanguardVeteranSquad.Options(self)

        def check_rules(self):
            super(VanguardVeteranSquad.Veteran, self).check_rules()
            self.wep1.shield.active = not self.wep2.cur == self.wep2.shield
            self.wep2.shield.active = not self.wep1.cur == self.wep1.shield

        def build_points(self):
            res = super(VanguardVeteranSquad.Veteran, self).build_points()
            if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
                res -= 5
            return res

        @ListSubUnit.count_gear
        def has_bombs(self):
            return self.opt.any

    class Sergeant(Unit):
        type_name = 'Vanguard veteran Sergeant'

        def __init__(self, parent):
            super(VanguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, gear=[Gear('Frag grenades'), Gear('Krak grenades')], points=16
            )
            self.armory = parent.armory
            self.wep1 = VanguardVeteranSquad.VeteranWeapon(self, melee=True, relic=True)
            self.wep2 = VanguardVeteranSquad.VeteranWeapon(self, '', melee=False, relic=True)
            self.opt = VanguardVeteranSquad.Options(self)

        def check_rules(self):
            super(VanguardVeteranSquad.Sergeant, self).check_rules()
            self.wep1.shield.active = not self.wep2.cur == self.wep2.shield
            self.wep2.shield.active = not self.wep1.cur == self.wep1.shield

        def build_points(self):
            res = super(VanguardVeteranSquad.Sergeant, self).build_points()
            if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
                res -= 5
            return res

    class Jump(OptionsList):
        def __init__(self, parent):
            super(VanguardVeteranSquad.Jump, self).__init__(parent=parent, name='Options', limit=None)
            self.jump = self.variant('Jump packs', 2, per_model=True)

    def __init__(self, parent):
        super(VanguardVeteranSquad, self).__init__(parent, armory.get_name(units.VanguardVeteranSquad))
        self.leader = SubUnit(self, self.Sergeant(self))
        self.vets = UnitList(self, self.Veteran, 4, 9)
        self.jump = self.Jump(self)

    def get_count(self):
        return self.vets.count + 1

    def check_rules(self):
        super(VanguardVeteranSquad, self).check_rules()
        mcnt = self.leader.unit.opt.any + sum(u.has_bombs() for u in self.vets.units)
        if mcnt > 1:
            self.error('Only one model may take melta bombs')

    def build_power(self):
        return (7 + (self.jump.any)) * (1 + self.vets.count > 4)

    def build_points(self):
        return super(VanguardVeteranSquad, self).build_points() + self.jump.points * self.vets.count


class CodexVanguardVeteranSquad(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.VanguardVeteranSquad)
    type_id = 'vanguard_veteran_squad_v2'

    keywords = ['Infantry']
    power = 6

    class Options(OptionsList):
        def __init__(self, parent):
            super(CodexVanguardVeteranSquad.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.meltabombs = self.variant(*ranged.MeltaBombs)

    class VeteranWeapon(OneOf):
        def __init__(self, parent, name='Weapon', has_melee=False, relic=False):
            super(CodexVanguardVeteranSquad.VeteranWeapon, self).__init__(parent, name)
            if has_melee:
                armory.add_melee_weapons(self)
            armory.add_pistol_options(self)
            if not has_melee:
                armory.add_melee_weapons(self)
            if relic:
                self.variant(*melee.RelicBlade)
            self.shield = self.variant(*wargear.StormShield)

    class Veteran(armory.ClawUser, ListSubUnit):
        type_name = 'Space Marine Veteran'

        def __init__(self, parent):
            super(CodexVanguardVeteranSquad.Veteran, self).__init__(
                parent=parent, points=armory.get_cost(units.VanguardVeteranSquad),
                gear=armory.create_gears(ranged.FragGrenades, ranged.KrakGrenades)
            )
            self.wep1 = CodexVanguardVeteranSquad.VeteranWeapon(self, has_melee=True)
            self.wep2 = CodexVanguardVeteranSquad.VeteranWeapon(self, '', has_melee=False)
            self.opt = CodexVanguardVeteranSquad.Options(self)

        def check_rules(self):
            super(CodexVanguardVeteranSquad.Veteran, self).check_rules()
            self.wep1.shield.active = not self.wep2.cur == self.wep2.shield
            self.wep2.shield.active = not self.wep1.cur == self.wep1.shield

        @ListSubUnit.count_gear
        def has_bombs(self):
            return self.opt.any

    class Sergeant(armory.ClawUser):
        type_name = 'Vanguard veteran Sergeant'

        def __init__(self, parent):
            super(CodexVanguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, gear=[Gear('Frag grenades'), Gear('Krak grenades')],
                points=armory.get_cost(units.VanguardVeteranSquad)
            )
            self.wep1 = CodexVanguardVeteranSquad.VeteranWeapon(self, has_melee=True, relic=True)
            self.wep2 = CodexVanguardVeteranSquad.VeteranWeapon(self, '', has_melee=False, relic=True)
            self.opt = CodexVanguardVeteranSquad.Options(self)

        def check_rules(self):
            super(CodexVanguardVeteranSquad.Sergeant, self).check_rules()
            self.wep1.shield.active = not self.wep2.cur == self.wep2.shield
            self.wep2.shield.active = not self.wep1.cur == self.wep1.shield

    class Jump(OptionsList):
        def __init__(self, parent):
            super(CodexVanguardVeteranSquad.Jump, self).__init__(parent=parent, name='Options', limit=None)
            self.jump = self.variant('Jump packs', armory.get_cost(units.JumpVanguardVeteranSquad) -
                                     armory.get_cost(units.VanguardVeteranSquad), per_model=True)

    def __init__(self, parent):
        super(CodexVanguardVeteranSquad, self).__init__(parent, armory.get_name(units.VanguardVeteranSquad))
        self.leader = SubUnit(self, self.Sergeant(self))
        self.vets = UnitList(self, self.Veteran, 4, 9)
        self.jump = self.Jump(self)

    def get_count(self):
        return self.vets.count + 1

    def check_rules(self):
        super(CodexVanguardVeteranSquad, self).check_rules()
        mcnt = self.leader.unit.opt.any + sum(u.has_bombs() for u in self.vets.units)
        if mcnt > 1:
            self.error('Only one model may take melta bombs')

    def build_power(self):
        return (self.power + (self.jump.any)) * (1 + self.vets.count > 4)

    def build_points(self):
        return super(CodexVanguardVeteranSquad, self).build_points() + self.jump.points * self.vets.count


class Dreadnought(ElitesUnit, armory.CodexBAUnit):
    type_name = 'Dreadnought' + ' (Index)'
    model_name = 'Dreadnought'
    type_id = 'dreadnought_v1'
    keywords = ['Vehicle']
    power = 7
    # obsolete = True
    model_points = armory.get_cost(units.Dreadnought)

    @classmethod
    def calc_faction(cls):
        return super(Dreadnought, cls).calc_faction() + ['DARK ANGELS',
                                                         '<DARK ANGELS SUCCESSORS>']

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(Dreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            self.variant(*ranged.TwinHeavyFlamer)
            self.variant(*ranged.TwinAutocannon)
            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.HeavyPlasmaCannon)
            self.variant(*ranged.MultiMelta)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtWeapon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)
            self.twinlinkedautocannon = self.variant(*ranged.TwinAutocannon)

    def __init__(self, parent):
        super(Dreadnought, self).__init__(parent=parent, name=self.model_name,
                                          points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(Dreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class CodexDreadnought(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.Dreadnought) + ' (Codex)'
    type_id = 'dreadnought_v2'
    keywords = ['Vehicle']
    power = 5

    model_points = armory.get_cost(units.Dreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(CodexDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtWeapon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)

    def __init__(self, parent):
        super(CodexDreadnought, self).__init__(parent=parent, name=armory.get_name(units.Dreadnought),
                                               points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(CodexDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class VenDreadnought(Dreadnought):
    type_name = 'Venerable Dreadnought' + ' (Index)'
    model_name = 'Venerable Dreadnought'
    type_id = 'ven_dreadnought_v1'

    power = 8
    model_points = armory.get_cost(units.VenerableDreadnought)

    @classmethod
    def calc_faction(cls):
        return super(VenDreadnought, cls).calc_faction() + ['DEATHWING']


class CodexVenDreadnought(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.VenerableDreadnought) + ' (Codex)'
    type_id = 'ven_dreadnought_v2'
    keywords = ['Vehicle']
    power = 6

    model_points = armory.get_cost(units.VenerableDreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(CodexVenDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexVenDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexVenDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtWeapon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)

    def __init__(self, parent):
        super(CodexVenDreadnought, self).__init__(parent=parent,
                                                  name=armory.get_name(units.VenerableDreadnought),
                                                  points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(CodexVenDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class ContDreadnought(ElitesUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.ContemptorDreadnought)
    type_id = 'contemptor_dreadnought_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 6

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ContDreadnought.Weapon, self).__init__(parent=parent, name='Weapon')
            self.melta = self.variant(*ranged.MultiMelta)
            self.cannon = self.variant(*ranged.KheresCannon)

    def __init__(self, parent):
        gear = [ranged.CombiBolter, melee.DreadnoughtWeapon]
        cost = armory.points_price(armory.get_cost(units.ContemptorDreadnought), *gear)
        super(ContDreadnought, self).__init__(parent=parent, points=cost,
                                              gear=armory.create_gears(*gear))
        self.wep = self.Weapon(self)


class IronDred(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.IrondlcadDreadnought)
    type_id = 'ironcladdreadnought_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 7

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(IronDred.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.powerfist = self.variant(*melee.DreadnoughtWeapon)
            self.hurricanebolter = self.variant(*ranged.HurricaneBolter)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(IronDred.Weapon2, self).__init__(parent=parent, name='')
            self.seismichammer = self.variant(*melee.SeismicHammer)
            self.chainfist = self.variant(*melee.DreadnoughtChainfist)

    class BuildIn2(OneOf):
        def __init__(self, parent):
            super(IronDred.BuildIn2, self).__init__(parent=parent, name='')
            self.buildinmeltagun = self.variant(*ranged.Meltagun)
            self.buildinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Options(OptionsList):
        def __init__(self, parent):
            super(IronDred.Options, self).__init__(parent=parent, name='Options')
            self.ironcladassaultlaunchers = self.variant(*wargear.IroncladAssaultLaunchers)

    def __init__(self, parent):
        super(IronDred, self).__init__(parent=parent, points=armory.get_cost(units.IrondlcadDreadnought))
        self.wep1 = self.Weapon1(self)
        self.bin1 = Dreadnought.BuildIn(self)
        self.wep2 = self.Weapon2(self)
        self.bin2 = self.BuildIn2(self)
        self.opt = self.Options(self)
        self.missiles = Count(self, 'Hunter-killer missile', 0, 2, armory.get_cost(ranged.HunterKiller))

    def check_rules(self):
        super(IronDred, self).check_rules()
        self.bin1.visible = self.bin1.used = self.wep1.cur == self.wep1.powerfist


class TyrannicWarVeterans(ElitesUnit, armory.AstartesUnit):
    type_name = 'Tyrannic War Veterans'
    type_id = 'tyrannic_veteran'
    faction = ['Ultramarines']

    keywords = ['Infantry']

    model_gear = [Gear('Frag grenades'), Gear('Krak grenades'),
                  Gear('Bolt pistol'), Gear('Special Issue Boltgun')]
    model_points = 16 + 3

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol, ranged.SpecialIssueBoltgun]
        cost = armory.points_price(armory.get_cost(units.TyrannicWarVeterans), *gear)
        super(TyrannicWarVeterans, self).__init__(parent, points=self.model_points,
                                                  gear=UnitDescription('Veteran sergeant',
                                                                       options=armory.create_gears(*gear)))
        self.models = Count(self, 'Tyrannic War Veterans', 4, 10, self.model_points, True,
                            gear=UnitDescription('Tyrannic War Veteran', cost,
                                                 options=armory.create_gears(*gear)))

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return 3 + 6 * (self.models.cur > 4)


class TerminatorSquad(ElitesUnit, armory.CodexBAUnit):
    type_name = 'Terminator Squad' + ' (Index)'
    type_id = 'terminator_squad_v1'
    obsolete = True
    keywords = ['Infantry', 'Terminator']

    model_points = 26

    class Veteran(ListSubUnit):
        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant('Power fist', 20)
                self.chain_fist = self.variant('Chainfist', 22)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant('Storm bolter', 2)
                old_armory.add_term_heavy_weapons(self)

        def __init__(self, parent):
            super(TerminatorSquad.Veteran, self).__init__(
                parent=parent, name='Terminator', points=TerminatorSquad.model_points
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.right_weapon.cur != self.right_weapon.bolt

    class Options(OptionsList):
        def __init__(self, parent):
            super(TerminatorSquad.Options, self).__init__(parent, 'Options')
            self.variant('Teleport homer', 0)

    def __init__(self, parent):
        super(TerminatorSquad, self).__init__(parent=parent, name='Terminator Squad',
                                              points=self.model_points + 4 + 2,
                                              gear=[UnitDescription(
                                                  'Terminator Sergeant',
                                                  options=[Gear('Power sword'), Gear('Storm bolter')]
        )])
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)
        self.Options(self)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))

    def build_power(self):
        return 13 * (1 + (self.terms.count > 4))


class CodexTerminatorSquad(ElitesUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.TerminatorSquad)
    type_id = 'terminator_squad_v2'

    keywords = ['Infantry', 'Terminator']

    model_points = armory.get_cost(units.TerminatorSquad)
    power = 9

    class Veteran(ListSubUnit):
        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(CodexTerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant(*melee.PowerFist)
                self.chain_fist = self.variant(*melee.Chainfist)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(CodexTerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.StormBolter)
                armory.add_term_heavy_weapons(self)

        def __init__(self, parent):
            super(CodexTerminatorSquad.Veteran, self).__init__(
                parent=parent, name='Terminator', points=CodexTerminatorSquad.model_points
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.right_weapon.cur != self.right_weapon.bolt

    class Options(OptionsList):
        def __init__(self, parent):
            super(CodexTerminatorSquad.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.TeleportHomer)

    def __init__(self, parent):
        sarge_gear = [melee.PowerSword, ranged.StormBolter]
        sarge = UnitDescription('Terminator Sergeant', options=armory.create_gears(*sarge_gear))
        super(CodexTerminatorSquad, self).__init__(parent=parent, name=armory.get_name(units.TerminatorSquad),
                                              points=armory.points_price(self.model_points, *sarge_gear),
                                              gear=[sarge])
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)
        self.Options(self)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))

    def build_power(self):
        return self.power * (1 + (self.terms.count > 4))


class TerminatorAssaultSquad(ElitesUnit, armory.CodexBAUnit):
    type_name = 'Terminator Assault Squad' + ' (Index)'
    type_id = 'terminatorassaultsquad_v1'
    obsolete = True
    keywords = ['Infantry', 'Terminator']
    model_points = 31

    class Sergeant(Unit):

        def __init__(self, parent):
            super(TerminatorAssaultSquad.Sergeant, self).__init__(
                name='Terminator Sergeant', parent=parent, points=TerminatorAssaultSquad.model_points,
            )
            self.weapon = self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(TerminatorAssaultSquad.Sergeant.Weapon, self).__init__(parent=parent, name='Weapon')
                self.lightningclaws = self.variant('Lightning claws', 13, gear=[Gear('Lightning claw', count=2)])
                self.thunderhammerandstormshield = self.variant(
                    'Thunder hammer and storm shield', 20 + 5, gear=[Gear('Thunder hammer'), Gear('Storm shield')])

    def __init__(self, parent):
        super(TerminatorAssaultSquad, self).__init__(parent=parent, name='Terminator Assault Squad')
        self.sergeant = SubUnit(self, self.Sergeant(self))
        self.terminator = Count(self, 'Terminator', min_limit=4, max_limit=9, points=self.model_points + 13, gear=[])
        self.thunderhammerandstormshield = Count(self, 'Thunder Hammer and Storm Shield',
                                                 min_limit=0, max_limit=4, points=20 + 5 - 13, gear=[])
        self.opt = TerminatorSquad.Options(self)

    def get_count(self):
        return self.terminator.cur + 1

    def check_rules(self):
        super(TerminatorAssaultSquad, self).check_rules()
        self.thunderhammerandstormshield.max = self.terminator.cur

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, count=self.get_count(), options=self.opt.description)
        desc.add(self.sergeant.description)
        term = UnitDescription('Terminator')
        desc.add(term.clone().add([Gear('Thunder hammer'), Gear('Storm shield')])
            .set_count(self.thunderhammerandstormshield.cur))
        desc.add(term.add([Gear('Lightning claw', count=2)])
            .set_count(self.terminator.cur - self.thunderhammerandstormshield.cur))
        return desc

    def build_power(self):
        return 13 * (1 + (self.terminator.cur > 4))


class CodexTerminatorAssaultSquad(ElitesUnit, armory.SMUnit):
    type_name = armory.get_name(units.TerminatorAssaultSquad)
    type_id = 'terminatorassaultsquad_v2'

    keywords = ['Infantry', 'Terminator']
    model_points = armory.get_cost(units.TerminatorAssaultSquad)
    power = 9

    class Sergeant(Unit):

        def __init__(self, parent):
            super(CodexTerminatorAssaultSquad.Sergeant, self).__init__(
                name='Terminator Sergeant', parent=parent, points=CodexTerminatorAssaultSquad.model_points,
            )
            self.weapon = self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(CodexTerminatorAssaultSquad.Sergeant.Weapon, self).__init__(parent=parent, name='Weapon')
                self.lightningclaws = self.variant('Lightning claws', armory.get_cost(melee.LightningClawsPair),
                                                   gear=[Gear('Lightning claw', count=2)])
                gearlist = [melee.ThunderHammer, wargear.StormShield]
                self.thunderhammerandstormshield = self.variant(
                    'Thunder hammer and storm shield', armory.get_costs(*gearlist),
                    gear=armory.create_gears(*gearlist))

    def __init__(self, parent):
        super(CodexTerminatorAssaultSquad, self).__init__(parent=parent,
                                                          name=armory.get_name(units.TerminatorAssaultSquad))
        self.sergeant = SubUnit(self, self.Sergeant(self))
        self.terminator = Count(self, 'Terminator', min_limit=4, max_limit=9, points=armory.points_price(self.model_points, melee.LightningClawsPair),
                                gear=[])
        self.thunderhammerandstormshield = Count(self, 'Thunder Hammer and Storm Shield',
                                                 min_limit=0, max_limit=4,
                                                 points=armory.get_costs(melee.ThunderHammer, wargear.StormShield) - armory.get_cost(melee.LightningClawsPair),
                                                 gear=[])
        self.opt = CodexTerminatorSquad.Options(self)

    def get_count(self):
        return self.terminator.cur + 1

    def check_rules(self):
        super(CodexTerminatorAssaultSquad, self).check_rules()
        self.thunderhammerandstormshield.max = self.terminator.cur

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, count=self.get_count(), options=self.opt.description)
        desc.add(self.sergeant.description)
        term = UnitDescription('Terminator')
        desc.add(term.clone().add([Gear('Thunder hammer'), Gear('Storm shield')])
            .set_count(self.thunderhammerandstormshield.cur))
        desc.add(term.add([Gear('Lightning claw', count=2)])
            .set_count(self.terminator.cur - self.thunderhammerandstormshield.cur))
        return desc

    def build_power(self):
        return self.power * (1 + (self.terminator.cur > 4))


class CataphractiiTerminatorSquad(ElitesUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.CataphractiiTerminatorSquad)
    type_id = 'cataphractii_terminator_squad_v1'
    keywords = ['Infantry', 'Terminator']
    power = 10

    model_points = armory.get_cost(units.CataphractiiTerminatorSquad)

    class Veteran(armory.ClawUser, ListSubUnit):
        type_name = 'Cataphractii Terminator'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(CataphractiiTerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant(*melee.PowerFist)
                self.claw = self.variant(*melee.LightningClaw)
                self.chain_fist = self.variant(*melee.Chainfist)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(CataphractiiTerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.claw = self.variant(*melee.LightningClaw)
                self.hf = self.variant(*ranged.HeavyFlamer)

            def has_spec(self):
                return self.cur == self.hf

        def __init__(self, parent):
            super(CataphractiiTerminatorSquad.Veteran, self).__init__(
                parent=parent, points=CataphractiiTerminatorSquad.model_points
            )

            self.wep1 = self.LeftWeapon(self)
            self.wep2 = self.RightWeapon(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep2.has_spec()

    class Sergeant(armory.ClawUser):
        type_name = 'Cataphractii Sergeant'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(CataphractiiTerminatorSquad.Sergeant.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.sword = self.variant(*melee.PowerSword)
                self.power_fist = self.variant(*melee.PowerFist)
                self.claw = self.variant(*melee.LightningClaw)
                self.chain_fist = self.variant(*melee.Chainfist)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(CataphractiiTerminatorSquad.Sergeant.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.claw = self.variant(*melee.LightningClaw)

            def has_claw(self):
                return self.cur == self.claw

        def __init__(self, parent):
            super(CataphractiiTerminatorSquad.Sergeant, self).__init__(
                parent=parent, points=CataphractiiTerminatorSquad.model_points
            )

            self.wep1 = self.LeftWeapon(self)
            self.wep2 = self.RightWeapon(self)
            self.opt = self.Options(self)

        class Options(OptionsList):
            def __init__(self, parent):
                super(CataphractiiTerminatorSquad.Sergeant.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.harness = self.variant(*ranged.GrenadeHarness)

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(CataphractiiTerminatorSquad, self).__init__(parent)
        self.leader = SubUnit(self, self.get_leader()(self))
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Cataphractii Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))

    def build_power(self):
        return self.power * (1 + (self.terms.count > 4))


class TartarosTerminatorSquad(ElitesUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.TartarosTerminatorSquad)
    type_id = 'tartaros_terminator_squad_v1'
    keywords = ['Infantry', 'Terminator']

    model_points = armory.get_cost(units.TartarosTerminatorSquad)
    power = 9

    class Harness(OptionsList):
        def __init__(self, parent):
            super(TartarosTerminatorSquad.Harness, self).__init__(parent, '')
            self.variant(*ranged.GrenadeHarness)

    class Veteran(ListSubUnit):
        type_name = 'Tartaros Terminator'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(TartarosTerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant(*melee.PowerFist)
                self.claws = self.variant('Two lightning claws', get_cost(melee.LightningClawsPair), gear=[Gear('Lightning claw', count=2)])
                self.chain_fist = self.variant(*melee.Chainfist)

            def has_claws(self):
                return self.cur == self.claws

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(TartarosTerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.hf = self.variant(*ranged.HeavyFlamer)
                self.variant(*ranged.ReaperAutocannon)

            def has_spec(self):
                return self.used and self.cur != self.bolt

        def __init__(self, parent):
            super(TartarosTerminatorSquad.Veteran, self).__init__(
                parent=parent, points=TartarosTerminatorSquad.model_points
            )
            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.grenade = TartarosTerminatorSquad.Harness(self)

        def check_rules(self):
            super(TartarosTerminatorSquad.Veteran, self).check_rules()
            self.right_weapon.used = self.right_weapon.visible = not self.left_weapon.has_claws()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def has_harness(self):
            return self.grenade.any

    class Sergeant(Unit):
        type_name = 'Tartaros Sergeant'

        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(TartarosTerminatorSquad.Sergeant.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.sword = self.variant(*melee.PowerSword)
                self.power_fist = self.variant(*melee.PowerFist)
                self.claws = self.variant('Two lightning claws', get_cost(melee.LightningClawsPair), gear=[Gear('Lightning claw', count=2)])
                self.chain_fist = self.variant(*melee.Chainfist)

            def has_claws(self):
                return self.cur == self.claws

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(TartarosTerminatorSquad.Sergeant.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant(*ranged.CombiBolter)
                self.variant(*ranged.PlasmaBlaster)
                self.variant(*ranged.VolkiteCharger)

        def __init__(self, parent):
            super(TartarosTerminatorSquad.Sergeant, self).__init__(
                parent=parent, points=TartarosTerminatorSquad.model_points
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.grenade = TartarosTerminatorSquad.Harness(self)

        def check_rules(self):
            super(TartarosTerminatorSquad.Sergeant, self).check_rules()
            self.right_weapon.used = self.right_weapon.visible = not self.left_weapon.has_claws()

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(TartarosTerminatorSquad, self).__init__(parent)
        self.leader = SubUnit(self, self.get_leader()(self))
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        super(TartarosTerminatorSquad, self).check_rules()
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Tartaros Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))
        gren = sum(c.has_harness() for c in self.terms.units) + self.leader.unit.grenade.any
        if gren > spec_limit:
            self.error('Tartaros Terminators may take only {0} grenade harnesses (taken {1})'.format(spec_limit, gren))

    def build_power(self):
        return self.power * (1 + (self.terms.count > 4))


class Cenobytes(ElitesUnit, armory.AstartesUnit):
    type_id = 'cenobytes_v1'
    type_name = 'Cenobyte Servitors'
    faction = ['Black Templars']
    power = 1

    keywords = ['Servitors', 'Infantry']

    def __init__(self, parent):
        super(Cenobytes, self).__init__(parent, gear=[
            UnitDescription('Cenobyte Servitor', count=3, options=[Gear('Close combat weapon')])
        ], static=True, unique=True, points=2 * 3)

    def get_count(self):
        return 3


class TheDamned(ElitesUnit, armory.AstartesUnit):
    type_name = 'Damned Legionnaires'
    type_id = 'legionofthedamned_v1'
    faction = ['Legion of the Damned']

    model_points = 25

    class DamnedSergeant(Unit):
        type_name = 'Legionnaire Sergeant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(TheDamned.DamnedSergeant.Weapon1, self).__init__(parent, 'Weapon')
                self.variant('Boltgun')
                self.variant('Chainsword')
                self.variant('Power fist', 20)
                self.variant('Power axe', 5),
                self.variant('Power sword', 4)
                self.variant('Power maul', 4)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(TheDamned.DamnedSergeant.Weapon2, self).__init__(parent, '')
                self.variant('Bolt pistol')
                self.variant('Plasma pistol', 7)
                self.variant('Storm bolter', 2)

        def __init__(self, parent):
            super(TheDamned.DamnedSergeant, self).__init__(
                parent=parent, points=TheDamned.model_points,
                gear=[Gear('Frag grenades'), Gear('Krak grenades')]
            )
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)

    class SpecialWeapon(OptionsList):
        def __init__(self, parent):
            super(TheDamned.SpecialWeapon, self).__init__(parent, 'Special weapon', limit=1)
            self.variant('Flamer', 9)
            self.variant('Meltagun', 17)
            self.variant('Plasma gun', 13)

    class HeavyWeapon(OptionsList):
        def __init__(self, parent):
            super(TheDamned.HeavyWeapon, self).__init__(parent, 'Heavy weapon', limit=1)
            self.variant('Heavy flamer', 17)
            self.variant('Multi-melta', 27)

    def __init__(self, parent):
        super(TheDamned, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.DamnedSergeant(self))
        self.legionnaire = Count(self, 'Legionnaire', min_limit=4, max_limit=9, points=self.model_points)
        self.special = self.SpecialWeapon(self)
        self.heavy = self.HeavyWeapon(self)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, options=self.sergeant.description, count=self.get_count())
        count = self.legionnaire.cur
        leg = UnitDescription('Legionnaire', self.model_points, options=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Bolt pistol')])

        for o in [self.heavy, self.special]:
            if o.used and o.any:
                spec_marine = leg.clone()
                spec_marine.add(o.description)
                desc.add(spec_marine)
                count -= 1

        desc.add(leg.add(Gear('Boltgun')).set_count(count))
        return desc

    def get_count(self):
        return self.legionnaire.cur + 1

    def build_power(self):
        return 8 + 7 * (self.legionnaire.cur > 4)


class RedemptorDreadnought(ElitesUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.RedemptorDreadnought)
    type_id = 'redemptor_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 7

    @classmethod
    def calc_faction(cls):
        return super(RedemptorDreadnought, cls).calc_faction() + ['DEATHWATCH']

    gears = [melee.RedemptorFist]

    class Options(OptionsList):
        def __init__(self, parent):
            super(RedemptorDreadnought.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.IcarusRocketPod)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(RedemptorDreadnought.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HeavyFlamer)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(RedemptorDreadnought.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.MacroPlasmaIncinerator)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(RedemptorDreadnought.Weapon3, self).__init__(parent, 'Secondary weapon')
            self.variant('Two fragstorm grenade launchers', 2 * armory.get_cost(ranged.FragstormGrenadeLauncher),
                         gear=armory.create_gears(*[ranged.FragstormGrenadeLauncher] * 2))
            self.variant('Two storm bolters', 2 * armory.get_cost(ranged.StormBolter),
                         gear=armory.create_gears(*[ranged.StormBolter] * 2))

    def __init__(self, parent):
        cost = armory.points_price(armory.get_cost(units.RedemptorDreadnought), *self.gears)
        gear = armory.create_gears(*self.gears)
        super(RedemptorDreadnought, self).__init__(parent, points=cost, gear=gear)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)
        self.Options(self)


class InvictorWarsuit(ElitesUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.Invictor)
    type_id = 'invictor_v1'
    keywords = ['Vehicle']
    power = 6

    class Cannon(OneOf):
        def __init__(self, parent):
            super(InvictorWarsuit.Cannon, self).__init__(parent, 'Main weapon')
            self.variant(*ranged.IncendiumCannon)
            self.variant(*ranged.TwinIronhailAutocannon)

    def __init__(self, parent):
        gears = [ranged.FragstormGrenadeLauncher, ranged.HeavyBolter,
                 ranged.IronhailHeavyStubber, ranged.IronhailHeavyStubber,
                 melee.InvictorFist]
        super(InvictorWarsuit, self).__init__(parent,
                                              points=points_price(get_cost(units.Invictor), *gears),
                                              gear=create_gears(*gears))
        self.Cannon(self)


class Agressors(ElitesUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.AggressorSquad)
    type_id = 'agressors_v1'

    keywords = ['Infantry', 'Primaris', 'Mk X gravis']

    power = 5

    @classmethod
    def calc_faction(cls):
        return super(Agressors, cls).calc_faction() + ['DEATHWATCH']

    class Weapons(OneOf):
        def __init__(self, parent):
            super(Agressors.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Auto boltstorm gauntlets and fragstorm grenade launcher',
                         armory.get_costs(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher),
                         gear=armory.create_gears(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher))
            self.variant(*ranged.FlamestormGauntlets)

        @property
        def description(self):
            return [UnitDescription('Aggressor Sergeant').add(self.cur.gear)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Aggressor').add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(Agressors, self).__init__(parent, points=armory.get_cost(units.AggressorSquad))
        self.wep = self.Weapons(self)
        self.models = self.Marines(self, 'Agressors', 2, 5, points=armory.get_cost(units.AggressorSquad),
                                   per_model=True)

    def get_count(self):
        return 1 + self.models.cur

    def build_points(self):
        return super(Agressors, self).build_points() + self.wep.points * self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))


class Reivers(ElitesUnit, armory.CommonSMUnit):
    type_name = armory.get_name(units.ReiverSquad)
    type_id = 'reivers_v1'
    keywords = ['Infantry', 'Primaris']

    power = 4

    model_gear = [ranged.HeavyBoltPistol, ranged.FragGrenades, ranged.KrakGrenades,
                  ranged.ShockGrenades]

    @classmethod
    def calc_faction(cls):
        return super(Reivers, cls).calc_faction()

    class Wargear(OptionsList):
        def __init__(self, parent):
            super(Reivers.Wargear, self).__init__(parent, 'Squad wargear', used=False)
            self.variant(*wargear.GravChute, per_model=True)
            self.variant(*wargear.GrapnelLauncher, per_model=True)

    class SergeantKnife(OneOf):
        def __init__(self, parent):
            super(Reivers.SergeantKnife, self).__init__(parent, 'Sergeant weapons')
            self.variant('Bolt carbine and heavy bolt pistol',
                         points=armory.get_costs(ranged.BoltCarbine, ranged.HeavyBoltPistol),
                         gear=armory.create_gears(ranged.BoltCarbine, ranged.HeavyBoltPistol))
            self.variant('Bolt carbine and combat knife',
                         points=armory.get_costs(ranged.BoltCarbine, melee.CombatKnife),
                         gear=armory.create_gears(ranged.BoltCarbine, melee.CombatKnife))
            self.variant('Heavy bolt pistol and combat knife',
                         points=armory.get_costs(ranged.HeavyBoltPistol, melee.CombatKnife),
                         gear=armory.create_gears(ranged.HeavyBoltPistol, melee.CombatKnife))

        @property
        def description(self):
            return [UnitDescription('Reiver Sergeant', options=armory.create_gears(*Reivers.model_gear[1:])).\
                    add(super(Reivers.SergeantKnife, self).description).add(self.parent.wgear.description)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Reiver', options=armory.create_gears(*Reivers.model_gear)).
                                    add(self.parent.wep.cur.gear).
                                    add(self.parent.wgear.description).set_count(self.cur)]

    class SquadWeapon(OneOf):
        def __init__(self, parent):
            super(Reivers.SquadWeapon, self).__init__(parent, 'Squad weapons', used=False)
            self.variant(*ranged.BoltCarbine)
            self.variant(*melee.CombatKnife)

    def __init__(self, parent):
        sarge_cost = armory.points_price(armory.get_cost(units.ReiverSquad), *Reivers.model_gear[1:])
        super(Reivers, self).__init__(parent, points=sarge_cost)
        self.wgear = self.Wargear(self)
        self.wep = self.SquadWeapon(self)
        self.sarge = self.SergeantKnife(self)
        self.models = self.Marines(self, 'Reivers', 4, 9, sarge_cost + armory.get_cost(Reivers.model_gear[0]),
                                   per_model=True)

    def get_count(self):
        return self.models.cur + 1

    def build_points(self):
        return super(Reivers, self).build_points() +\
            (self.wgear.points + self.wep.points) * (1 + self.models.cur)

    def build_power(self):
        return self.power * (1 + (self.models.cur > 4))


class VitrixGuard(ElitesUnit, armory.AstartesUnit):
    type_id = 'vitrix_hguard_v1'
    type_name = armory.get_name(units.VitrixGuard)
    faction = ['Ultramarines']
    power = 3
    keywords = ['Infantry', 'Primaris']

    def __init__(self, parent):
        raw_gear = [
            melee.PowerSword,
            wargear.StormShield,
            ranged.FragGrenades,
            ranged.KrakGrenades
        ]
        cost = armory.points_price(armory.get_cost(units.VitrixGuard), *raw_gear)
        models = UnitDescription(armory.get_name(units.VitrixGuard),
                                 count=2,
                                 options=armory.create_gears(*raw_gear))
        super(VitrixGuard, self).__init__(parent, points=2 * cost, gear=models,
                                          static=True)

    def get_count(self):
        return 2
