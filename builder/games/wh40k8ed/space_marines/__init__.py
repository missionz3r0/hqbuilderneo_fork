from .hq import ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain,\
    Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan,\
    LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion,\
    TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain,\
    PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain,\
    CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, PrimarisCalgar, PhobosCaptain,\
    PhobosLibrarian, PhobosLieutenants, GabrielAngelos, PrimarisKhan, KhanOnBike, PrimarisTigurius,\
    PrimarisShrike, Feirros
from .elites import Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans,\
    CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient,\
    CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred,\
    PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad,\
    TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought,\
    PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion,\
    CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought,\
    CodexTerminatorSquad, CodexTerminatorAssaultSquad, VitrixGuard, TermoAncient,\
    InvictorWarsuit
from .troops import CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad,\
    CodexScoutSquad, InfiltratorSquad, IncursorSquad, IntercessorsV2
from .fast import AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, Suppressors
from .transports import Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, Impulsor
from .heavy import CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader,\
    LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators,\
    CodexPredator, CodexVindicator, CodexWhirlwind, Eliminators, RepulsorExecutioner
from .fliers import Stormhawk, StormravenGunship, Stormtalon
from .lords import Guilliman, TerminusUltra

from .ia_heavy import MortisDreadnought, SiegeDreadnought, ContemptorMortis,\
    LeviathanDreadnought
from .ia_elites import RelicContemptor, RelicDeredeo
from .ia_lords import RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion, RelicFellblade,\
    RelicMastodon

from builder.games.wh40k8ed.rosters import DetachVanguard


class DamnedVanguard(DetachVanguard):
    army_name = 'Legion of the Damned (Vanguard detachment)'
    faction_base = 'LEGION OF THE DAMNED'
    army_id = 'vanguard_legion_of_the_damned'
    army_factions = ['IMPERIUM', 'ADEPTUS ASTARTES', 'LEGION OF THE DAMNED']

    def __init__(self, parent=None):
        super(DamnedVanguard, self).__init__(parent=parent, hq=False)
        self.elite.add_classes([TheDamned])
        self.command = 0

ia_types = [MortisDreadnought, SiegeDreadnought, ContemptorMortis,
            LeviathanDreadnought, RelicContemptor, RelicDeredeo,
            RelicSpartan, RelicTyphon, RelicCerberus, RelicFalchion,
            RelicFellblade, RelicMastodon]

unit_types = [ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan,
              BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius,
              CataphractiiCaptain, Champion, Chaplain, Chronus,
              GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan,
              LandRaiderExcelsior, Librarian, PrimarisLieutenants,
              Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine,
              Telion, TerminatorCaptain, TerminatorChaplain,
              TermoLibrarian, Tigurius, Vulkan, Apothecary,
              BikeApothecary, BikeCompanyAncient, BikeCompanyChampion,
              BikerCompanyVeterans, CataphractiiTerminatorSquad,
              Cenobytes, CenturionAssault, ChapterAncient,
              ChapterChampion, CompanyAncient, CompanyChampion,
              CompanyVeterans, ContDreadnought, Dreadnought,
              HonourGuard, ImperialSM, IronDred, PrimarisAncient,
              Servitors, SternguardVeteranSquad,
              TartarosTerminatorSquad, TerminatorAssaultSquad,
              TerminatorSquad, TheDamned, TyrannicWarVeterans,
              VanguardVeteranSquad, VenDreadnought, CrusaderSquad,
              Intercessors, ScoutSquad, TacticalSquad, AssaultSquad,
              AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders,
              ScoutBikers, Razorback, Rhino, LandSpeederStorm,
              DropPod, CenturionDevastators, Devastators,
              Hellblasters, Hunter, LandRaider, LandRaiderCrusader,
              LandRaiderRedeemer, Predator, Stalker, Thunderfire,
              Vindicator, Whirlwind, Stormhawk, StormravenGunship,
              Stormtalon, Guilliman, TerminusUltra, CodexDropPod,
              CodexRazorback, Repulsor, PrimarisCaptain,
              PrimarisChaplain, PrimarisLibrarian,
              RedemptorDreadnought, PrimarisApothecary, Reivers,
              Agressors, CodexCaptain, CodexTerminatorCaptain,
              CodexCataphractiiCaptain, CodexBikeCaptain,
              CodexTermoLibrarian, CodexTechmarine, CodexChaplain,
              Lieutenants, CodexTacticalSquad, CodexScoutSquad,
              CodexDevastators, CodexPredator, CodexVindicator,
              CodexWhirlwind, CodexCompanyAncient, CodexHonourGuard,
              CodexChapterChampion, CodexSternguardVeteranSquad,
              CodexVanguardVeteranSquad, CodexDreadnought,
              CodexVenDreadnought, CodexTerminatorSquad,
              CodexTerminatorAssaultSquad, CodexAssaultSquad,
              CodexLandSpeeders, PrimarisCalgar, VitrixGuard,
              PhobosCaptain, PhobosLibrarian, PhobosLieutenants,
              InfiltratorSquad, Suppressors, Eliminators,
              GabrielAngelos, IncursorSquad, TermoAncient,
              InvictorWarsuit, RepulsorExecutioner, Impulsor,
              IntercessorsV2, PrimarisKhan, KhanOnBike,
              PrimarisTigurius, PrimarisShrike, Feirros] + ia_types
