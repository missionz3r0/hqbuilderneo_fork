__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import FortUnit
from builder.games.wh40k8ed.options import Count, UnitDescription, OptionsList
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import units
from . import ranged


class Shieldline(FortUnit, armory.SeptUnit):
    type_name = get_name(units.TidewallShieldline)
    type_id = 'tidewall_shieldline_v1'

    class Platform(OptionsList):
        def __init__(self, parent):
            super(Shieldline.Platform, self).__init__(parent, 'Options')
            self.variant(get_name(units.TidewallDefencePlatform), points=get_cost(units.TidewallDefencePlatform), gear=[
                UnitDescription(get_name(units.TidewallDefencePlatform))
            ])

    def __init__(self, parent):
        super(Shieldline, self).__init__(parent=parent, points=get_cost(units.TidewallDefencePlatform),
                                         gear=[UnitDescription(get_name(units.TidewallDefencePlatform))])
        self.plat = self.Platform(self)

    def build_power(self):
        return 4 + 3 * self.plat.any

    def build_statistics(self):
        res = super(Shieldline, self).build_statistics()
        res['Models'] += self.plat.any
        return res


class Droneport(FortUnit, armory.SeptUnit):
    type_name = get_name(units.TidewallDroneport)
    type_id = 'tidewall_droneport_v1'
    power = 5
    keywords = ['Building', 'Vehicle', 'Transport']

    def __init__(self, parent):
        super(Droneport, self).__init__(parent=parent, points=get_cost(units.TidewallDroneport))
        armory.add_drones(self, 4)

    def check_rules(self):
        Count.norm_counts(0, 4, self.drones)

    def build_statistics(self):
        res = super(Droneport, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res


class Gunrig(FortUnit, armory.SeptUnit):
    type_name = get_name(units.TidewallGunrig)
    type_id = 'tidewall_gunrig_v1'
    power = 6
    keywords = ['Building', 'Vehicle', 'Transport']

    def __init__(self, parent):
        gear = [ranged.SupremacyRailgun]
        cost = points_price(get_cost(units.TidewallGunrig), *gear)
        super(Gunrig, self).__init__(parent=parent, points=cost,
                                     gear=create_gears(*gear), static=True)
