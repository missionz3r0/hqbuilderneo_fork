__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import units
from . import ranged


class SniperDrones(HeavyUnit, armory.SeptUnit):
    type_name = get_name(units.MV71SniperDrone)
    type_id = 'sniper_drones_v1'

    keywords = ['Drone', 'Fly']

    def __init__(self, parent):
        super(SniperDrones, self).__init__(parent=parent)
        gear = [ranged.LongshotPulseRifle]
        self.drones = Count(
            self, 'Sniper drones', min_limit=3, max_limit=9, points=get_cost(units.MV71SniperDrone), per_model=True,
            gear=UnitDescription('MV71 Sniper drone', options=create_gears(*gear))
        )

    def get_count(self):
        return self.drones.cur

    def build_power(self):
        return 3 * ((self.drones.cur + 2) / 3)


class Skyray(HeavyUnit, armory.SeptUnit):
    type_name = get_name(units.TX78SkyRayGunship)
    type_id = 'skyray_v1'
    keywords = ['Vehicle', 'Fly']
    power = 8

    def __init__(self, parent=None):
        gear = [ranged.Markerlight]*2 + [ranged.SeekerMissile]*6
        cost = points_price(get_cost(units.TX78SkyRayGunship), *gear)
        super(Skyray, self).__init__(parent=parent, points=cost, gear=create_gears(*gear))
        self.wep = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Skyray.Weapon, self).__init__(parent=parent, name='Weapon')
            self.twogundrones = self.variant('Two Gun Drones', get_cost(units.MV1GunDrone) * 2,
                                             gear=[UnitDescription('MV1 Gun Drone', count=2, options=[
                                                 Gear('Pulse carbine', count=2)
                                             ])])
            self.variant('Two burst cannons', get_cost(ranged.BurstCannon) * 2, gear=[
                Gear('Burst cannon', count=2)
            ])
            self.variant('Two smart missile systems', get_cost(ranged.SmartMissileSystem) * 2, gear=[
                Gear('Smart missile system', count=2)
            ])

    def build_statistics(self):
        res = super(Skyray, self).build_statistics()
        if self.wep.cur == self.wep.twogundrones:
            res['Models'] += 2
        return res


class Hammerhead(HeavyUnit, armory.SeptUnit):
    type_name = get_name(units.TX7HammerheadGunship)
    type_id = 'hammerhead_v1'
    kwname = 'Hammerhead'
    keywords = ['Vehicle', 'Fly']
    power = 9

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hammerhead.Weapon, self).__init__(parent=parent, name='Main Weapon')
            self.railgunwithsolidshot = self.variant(*ranged.Railgun)
            self.ioncannon = self.variant(*ranged.IonCannon)

    def __init__(self, parent):
        super(Hammerhead, self).__init__(parent=parent, points=get_cost(units.TX7HammerheadGunship))
        self.weapon = self.Weapon(self)
        self.support_weapon = Skyray.Weapon(self)
        self.seekermissile = Count(self, 'Seeker missile', min_limit=0, max_limit=2,
                                   points=get_cost(ranged.SeekerMissile))

    def build_statistics(self):
        res = super(Hammerhead, self).build_statistics()
        if self.support_weapon.cur == self.support_weapon.twogundrones:
            res['Models'] += 2
        return res


class BroadsideTeam(HeavyUnit, armory.SeptUnit):
    type_name = get_name(units.XV88BroadsideBattlesuit)
    type_id = 'broadsidebattlesuitteam_v3'
    keywords = ['Battlesuits']

    class BroadsideSuit(ListSubUnit):
        type_name = 'Broadside Shas\'ui'
        type_id = 'shasui_v1'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(BroadsideTeam.BroadsideSuit.Weapon1, self).__init__(parent=parent, name='Weapon')

                self.twinlinkedheavyrailrifle = self.variant(*ranged.HeavyRailRifle)
                self.twinlinkedhighyieldmissilepod = self.variant('Two high-yield missile pods',
                                                                  get_cost(ranged.HighYieldMissilePod) * 2, gear=[
                    Gear('High-yield missile pod', count=2)
                ])

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(BroadsideTeam.BroadsideSuit.Weapon2, self).__init__(parent=parent, name='')

                self.variant('Two smart missile systems', get_cost(ranged.SmartMissileSystem) * 2, gear=[
                    Gear('Smart missile system', count=2)
                ])
                self.variant('Two plasma rifles', get_cost(ranged.PlasmaRifle) * 2, gear=[
                    Gear('Plasma rifle')
                ])

        class Weapon3(OptionsList):
            def __init__(self, parent):
                super(BroadsideTeam.BroadsideSuit.Weapon3, self).__init__(parent=parent, name='', limit=None)

                self.seekermissile = self.variant(*ranged.SeekerMissile)

        class ShasVre(OptionsList):
            def __init__(self, parent):
                super(BroadsideTeam.BroadsideSuit.ShasVre, self).__init__(parent=parent, name='Leader', limit=None)

                self.shasvre = self.variant('Broadside Shas\'vre', 0, gear=[])

        def __init__(self, parent):
            super(BroadsideTeam.BroadsideSuit, self).__init__(parent=parent,
                                                              points=get_cost(units.XV88BroadsideBattlesuit),
                                                              name=self.type_name, gear=[
                Gear('Broadside battlesuit')
            ])
            self.weapon1 = self.Weapon1(self)
            self.weapon2 = self.Weapon2(self)
            self.support = armory.SupportSystem(self, slots=1)
            self.weapon3 = self.Weapon3(self)
            self.shasvre = self.ShasVre(self)

        def build_description(self):
            desc = super(BroadsideTeam.BroadsideSuit, self).build_description()
            if self.shasvre.shasvre.value:
                desc.name = self.shasvre.shasvre.title
            return desc

        @ListSubUnit.count_gear
        def count_shasvre(self):
            return self.shasvre.shasvre.value

    def __init__(self, parent):
        super(BroadsideTeam, self).__init__(parent=parent, points=0, gear=[])
        self.team = UnitList(self, self.BroadsideSuit, min_limit=1, max_limit=3)
        armory.add_drones(self)
        self.mdrones = Count(self, 'Missile Drones', 0, 2, per_model=True, points=get_cost(units.MV8MissileDrone),
                             gear=UnitDescription('MV8 Missile Drone', options=[Gear('Missile pod')]))

    def get_count(self):
        return self.team.count

    def check_rules(self):
        super(BroadsideTeam, self).check_rules()
        shasvre = sum(u.count_shasvre() for u in self.team.units)
        if shasvre > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(shasvre))
        Count.norm_counts(0, 2 * self.team.count, self.drones + [self.mdrones])

    def build_statistics(self):
        res = super(BroadsideTeam, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones + [self.mdrones])
        return res

    def build_power(self):
        return 7 * self.count + 2 * ((self.mdrones.cur + 1) / 2) + ((1 + sum(c.cur for c in self.drones)) / 2)
