__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.options import OneOf, Gear, UnitDescription
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import units
from . import ranged


class Stormsurge(LordsUnit, armory.SeptUnit):
    type_name = get_name(units.KV128Stormsurge)
    type_id = 'stormsurge_v1'
    keywords = ['Vehicle', 'Titanic']
    power = 20

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Stormsurge.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.PulseBlastcannon)
            self.variant(*ranged.PulseDriverCannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Stormsurge.Weapon2, self).__init__(parent, '')
            self.variant('Two flamers', get_cost(ranged.Flamer) * 2, gear=[Gear(get_name(ranged.Flamer), count=2)])
            self.variant('Two burst cannons', get_cost(ranged.BurstCannon) * 2,
                         gear=[Gear(get_name(ranged.BurstCannon), count=2)])
            self.variant('Two airbursting fragmentation projectors',
                         get_cost(ranged.AirburstingFragmentationProjector) * 2,
                         gear=[Gear(get_name(ranged.AirburstingFragmentationProjector), count=2)])

    def __init__(self, parent):
        gear = [ranged.SmartMissileSystem, ranged.SmartMissileSystem, ranged.ClusterRocketSystem,
                ranged.DestroyerMissile, ranged.DestroyerMissile, ranged.DestroyerMissile, ranged.DestroyerMissile]
        cost = points_price(get_cost(units.KV128Stormsurge), *gear)
        super(Stormsurge, self).__init__(
            parent, 'KV128 Stormsurge', points=cost, gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        armory.SupportSystem(self, 3, stormsurge=True)


class TheEight(LordsUnit, armory.TauUnit):
    type_name = get_name(units.TheEight)
    # there is no common keyword name, so we make do
    kwname = 'Battlesuit'
    faction = ['Farsight Enclaves']
    keywords = ['Character', 'Commander Farsight', 'Commander Brightsword',
                'Commander Bravestorm', "Shas'o Sha'vastos",
                "Shas'o Arra'kon", 'Sub-Commander Torchstar',
                "Shas'vre Ob'Lotai 9-0", "O'Vesa"]
    power = 56

    def get_unique(self):
        return 'Commander Farsight'

    def __init__(self, parent):
        desc = [
            UnitDescription('Commander Farsight', options=[
                Gear('Dawn Blade'),
                Gear('High-intensity plasma rifle'),
                Gear('Shield generator')
            ]),
            UnitDescription('Commander Bravestorm', options=[
                Gear('XV8-02 Crisis \'Iridium\' Battlesuit'),
                Gear('Plasma rifle'),
                Gear('Flamer'),
                Gear('Shield generator'),
                Gear('Advanced Targeting system'),
                Gear('Onager gauntlet')
            ]),
            UnitDescription('Commander Brightsword', options=[
                Gear('Fusion Blades'),
                Gear('Counterfiew defence system'),
                Gear('Target lock')
            ]),
            UnitDescription("Shas'o Sha'vastos", options=[
                Gear('Plasma rifle'),
                Gear('Flamer'),
                Gear('Shield generator'),
                Gear('Drone controller')
            ]),
            UnitDescription("O'Vesa", options=[
                Gear('Fusion blaster', count=2),
                Gear('Ion accelerator'),
                Gear('Early warning override'),
                Gear('Target lock')	
            ]),
            UnitDescription('Shas\'o Arra\'kon', options=[
                Gear('Airbursting fragmentation projector'),
                Gear('Cyclic ion blaster'),
                Gear('Plasma rifle'),
                Gear('Counterfire defence system')
            ]),
            UnitDescription('Sub-Commander Torchstar', options=[
                Gear('Flamer', count=2),
                Gear('Target lock'),
                Gear('Drone controller'),
                Gear('Marker Drone', count=2),
                Gear('Multi-spectrum Sensor Suit'),
                Gear('Neuroweb System Jammer')
            ]),
            UnitDescription("Shas'vre Ob'Lotai 9-0", options=[
                Gear('High-yield missile pod', count=2),
                Gear('Smart missile system', count=2),
                Gear('Velocity tracker'),
                Gear('Seeker missile')
            ]),
            UnitDescription('O\'Vesa', options=[
                Gear('Ion accelerator'),
                Gear('Fusion blaster', count=2),
                Gear('Early warning override'),
                Gear('Target lock')
            ])
        ]
        super(TheEight, self).__init__(parent, points=get_cost(units.TheEight),
                                       gear=desc, static=True)
    
