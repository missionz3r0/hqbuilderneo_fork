from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from builder.games.wh40k8ed.unit import ElitesUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList
from . import armory
from . import ia_ranged
from . import ia_units
from . import ia_wargear


class Dx4Drones(ElitesUnit, armory.SeptUnit):
    type_name = get_name(ia_wargear.Dx4TechnicalDrones) + ' (Imperial Armour)'
    type_id = 'dx4_drones_tech_v1'
    keywords = ['Drone', 'Fly']
    power = 2

    def __init__(self, parent):
        gear = [ia_ranged.DefensiveCharge]
        super(Dx4Drones, self).__init__(parent, name=get_name(ia_wargear.Dx4TechnicalDrones))
        self.models = Count(self, 'Dx-4 Technical Drone', 2, 8,
                            get_cost(ia_wargear.Dx4TechnicalDrones), per_model=True,
                            gear=UnitDescription('Dx-4 Technical Drone', options=[gear(get_name(ia_ranged.DefensiveCharge))]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.models.cur


class HazardTeam(ElitesUnit, armory.SeptUnit):
    type_name = get_name(ia_units.Xv9HazardSupportTeam) + ' (Imperial Armour)'
    type_id = 'hazard_team_v1'
    keywords = ['Battlesuit', 'Jetpack', 'Fly']
    power = 5

    class HazardSuit(ListSubUnit):
        type_name = 'XV9 Hazard Battlesuit'
        type_id = 'xv9_v1'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(HazardTeam.HazardSuit.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ia_ranged.DoubleBarrelledBurstCannon)
                self.variant(*ia_ranged.PhasedIonGun)
                self.variant(*ia_ranged.FusionCascade)
                self.variant(*ia_ranged.PulseSubmunitionsRifle)

        def __init__(self, parent):
            super(HazardTeam.HazardSuit, self).__init__(parent, points=get_cost(ia_units.Xv9HazardSupportTeam))
            self.Weapon(self)
            self.Weapon(self)
            self.support = armory.SupportSystem(self, slots=1)

    def __init__(self, parent):
        super(HazardTeam, self).__init__(parent=parent,
                                         name=get_name(ia_units.Xv9HazardSupportTeam))
        self.team = UnitList(self, self.HazardSuit, min_limit=1, max_limit=3)
        armory.add_drones(self)

    def get_count(self):
        return self.team.count

    def check_rules(self):
        super(HazardTeam, self).check_rules()
        Count.norm_counts(0, 4, self.drones)

    def build_statistics(self):
        res = super(HazardTeam, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return self.power * self.team.count + sum(c.cur for c in self.drones)
