__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList, UnitDescription, Count
from . import ranged
from . import wargear
from . import units


def add_ranged_weapons(instance, coldstar=False):
    instance.rng = [instance.variant(*ranged.AirburstingFragmentationProjector),
                    instance.variant(*ranged.BurstCannon),
                    instance.variant(*ranged.Flamer),
                    instance.variant(*ranged.FusionBlaster),
                    instance.variant(*ranged.MissilePod),
                    instance.variant(*ranged.PlasmaRifle)]

    if not coldstar:
        instance.rng.append(instance.variant(*ranged.CyclicIonBlaster))


class SupportSystem(OptionsList):
    def __init__(self, parent, slots=1,
                 riptide=False, stormsurge=False, ghostkeel=False):
        super(SupportSystem, self).__init__(parent=parent, name='Support system', limit=slots)
        self.variant(*wargear.CounterfireDefenceSystem)
        self.variant(*wargear.DroneController)
        self.variant(*wargear.MultiTracker)
        if not(ghostkeel or stormsurge or riptide):
            self.variant(*wargear.CrisisAdvancedTargetingSystem)
            self.variant(*wargear.CrisisTargetLock)
            self.variant(*wargear.CrisisVelocityTracker)
        else:
            self.variant(*wargear.AdvancedTargetingSystem)
            self.variant(*wargear.TargetLock)
            self.variant(*wargear.VelocityTracker)

        if stormsurge:
            self.variant(*wargear.SurgeShieldGenerator)
        else:
            self.variant(*wargear.ShieldGenerator)
        self.variant(*wargear.StimulantInjector)


def add_drones(parent, limit=2):
    gun_gear = [ranged.PulseCarbine, ranged.PulseCarbine]

    marker_gear = [ranged.Markerlight]

    gundrone = Count(parent, 'Gun Drones', min_limit=0, max_limit=limit, points=get_cost(units.MV1GunDrone),
                     per_model=True,
                     gear=UnitDescription(get_name(units.MV1GunDrone),
                                          options=create_gears(*gun_gear)))
    markerdrone = Count(parent, 'Marker Drone', min_limit=0, max_limit=limit, points=get_cost(units.MV7MarkerDrone),
                        per_model=True,
                        gear=UnitDescription(get_name(units.MV7MarkerDrone), options=create_gears(*marker_gear)))
    shielddrone = Count(parent, 'Shield Drone', min_limit=0, max_limit=limit, points=get_cost(units.MV4ShieldDrone),
                        per_model=True,
                        gear=UnitDescription(get_name(units.MV4ShieldDrone)))
    parent.drones = [gundrone, markerdrone, shielddrone]


class TauUnit(Unit):
    faction = ["T'AU EMPIRE"]
    wiki_faction = "Tau Empire"


class SeptUnit(TauUnit):
    faction = ['<Sept>']

    @classmethod
    def calc_faction(cls):
        return ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT",
                "KE'LSHAN SEPT"]


class KrootUnit(TauUnit):
    faction = ['Kroot']
