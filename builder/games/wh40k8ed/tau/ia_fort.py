__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import FortUnit
from builder.games.wh40k8ed.options import Count, UnitDescription, ListSubUnit, UnitList,\
    OneOf
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import ia_wargear
from . import ia_ranged


class RemoteSensorTower(FortUnit, armory.SeptUnit):
    type_name = get_name(ia_wargear.RemoteSensorTower) + ' (Imperial Armour)'
    type_id = 'remote_sensors_v1'
    power = 2
    keywords = ['Fortification']

    def __init__(self, parent):
        gear = [ia_wargear.HighIntensityMarkerlight]
        cost = points_price(get_cost(ia_wargear.RemoteSensorTower), *gear)
        super(RemoteSensorTower, self).__init__(
            parent=parent, name=get_name(ia_wargear.RemoteSensorTower), points=cost,
            gear=create_gears(*gear), static=True)


class DroneSentryTurrets(FortUnit, armory.SeptUnit):
    type_name = get_name(ia_wargear.DroneSentryTurret) + ' (Imperial armour)'
    type_id = 'drone_sentry_v1'
    keywords = ['Drone', 'Vehicle']
    power = 2

    class DroneTurret(ListSubUnit):
        type_name = get_name(ia_wargear.DroneSentryTurret)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(DroneSentryTurrets.DroneTurret.Weapon, self).__init__(parent=parent, name='Weapons')
                self.variant('Two burst cannons', get_cost(ia_ranged.BurstCannon) * 2, gear=[
                    Gear('Burst cannon', count=2)
                ])
                self.variant('Two missile pods', get_cost(ia_ranged.MissilePod) * 2, gear=[
                    Gear('Missile pod', count=2)
                ])
                self.variant('Two fusion blasters', get_cost(ia_ranged.FusionBlaster) * 2, gear=[
                    Gear('Fusion blaster', count=2)
                ])
                self.variant('Two plasma rifles', get_cost(ia_ranged.PlasmaRifle) * 2, gear=[
                    Gear('Plasma rifle', count=2)
                ])

        def __init__(self, parent):
            super(DroneSentryTurret.DroneTurret, self).__init__(
                parent, points=get_cost(ia_wargear.DroneSentryTurret))
            self.Weapon(self)

    def __init__(self, parent):
        super(DroneSentryTurret, self).__init__(parent, name=get_name(ia_units.DroneSentryTurret))
        self.models = UnitList(self, self.DroneTurret, 1, 4)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.get_count()
