from .hq import Commander, ColdstarCommander, Ethereal, EnforcerCommander,\
    Farsight, Shadowsun, Aunshi, Aunva, Darkstider, Fireblade, Longstrike
from .troops import StrikeTeam, BreacherTeam, KrootSquad
from .elites import Shaper, KrootoxSquad, StealthTeam, CrisisTeam,\
    Bodyguards, Ghostkeel, Riptide, Marksman
from .fast import KroothoundSquad, PiranhaTeam, Vespids, Pathfinders, Drones
from .transport import Devilfish
from .fliers import RazorShark, SunShark
from .heavy import SniperDrones, Skyray, Hammerhead, BroadsideTeam
from .lords import Stormsurge, TheEight
from .fort import Shieldline, Droneport, Gunrig

from .ia_hq import ShasOrMyr, ShasOrAlai, XV81Commander, XV84Commander
from .ia_elites import Dx4Drones, HazardTeam
from .ia_fast import Yvahra, Tetras, PiranhaTX42Team
from .ia_heavy import Rvarna, HeavyGunDrones, HBHammerhead, FSHammerhead
from .ia_lords import Taunar, Manta
from .ia_fliers import RemoraSquadron, TigerShark, TigerSharkAX10, Barracuda
from .ia_fort import RemoteSensorTower, DroneSentryTurrets

from builder.games.wh40k8ed.sections import HQSection


class CommanderHQ(HQSection):
    def __init__(self, *args, **kwargs):
        super(CommanderHQ, self).__init__(*args, **kwargs)
        self.commander_types = []

    def unit_type_add(self, ut):
        if 'COMMANDER' in ut.keywords:
            self.commander_types += [super(CommanderHQ, self).unit_type_add(ut)]
        else:
            super(CommanderHQ, self).unit_type_add(ut)

    def check_rules(self):
        super(CommanderHQ, self).check_rules()
        if sum(ut.count for ut in self.commander_types) > 1:
            self.error("No more then one COMMANDER may be taken in a single detachment in matched play")


unit_types = [Commander, ColdstarCommander, EnforcerCommander,
              Ethereal, Farsight, Shadowsun, Aunshi, Aunva,
              Darkstider, Fireblade, Longstrike, StrikeTeam,
              BreacherTeam, KrootSquad, Shaper, KrootoxSquad,
              StealthTeam, CrisisTeam, Bodyguards, Ghostkeel, Riptide,
              Marksman, KroothoundSquad, PiranhaTeam, Vespids,
              Pathfinders, Drones, Devilfish, RazorShark, SunShark,
              SniperDrones, Skyray, Hammerhead, BroadsideTeam,
              Stormsurge, Shieldline, Droneport, Gunrig, TheEight]

ia_units = [RemoteSensorTower, DroneSentryTurrets, Taunar, Manta,
            Rvarna, HeavyGunDrones, HBHammerhead, FSHammerhead,
            Yvahra, Tetras, PiranhaTX42Team, Dx4Drones, HazardTeam,
            ShasOrMyr, ShasOrAlai, XV81Commander, XV84Commander,
            RemoraSquadron, TigerShark, TigerSharkAX10, Barracuda]

unit_types += ia_units
