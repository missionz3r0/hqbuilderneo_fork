BlacklightMarkerDrones = ("Blacklight Marker Drone", 11)
DroneSentryTurret = ("Drone Sentry Turret", 20)
Dx4TechnicalDrones = ("DX4 Technical Drones", 16)
Dx6RemoraStealthDrone = ("DX-6 Remora Stealth Drone", 57)
HeavyGunDrone = ("Heavy Gun Drone", 18)
RemoteSensorTower = ("Remote Sensor Tower", 33)

AdvancedTargetingSystem = ('Advanced targeting system', 8)
CounterfireDefenceSystem = ('Counterfire defence system', 5)
DroneController = ('Drone controller', 5)
EarlyWarningOverride = ('Early warning override', 8)
MultiTrackerYVahra = ('Multi-tracker', 10)
MultiTracker = ('Multi-tracker', 2)
ShieldGeneratorYVahra = ('Shield generator', 40)
ShieldGenerator = ('Shield generator', 8)
StimulantInjector = ('Stimulant injector', 5)
TargetLockYVahra = ('Target lock', 12)
TargetLock = ('Target lock', 6)
VelocityTrackerYVahra = ('Velocity tracker', 10)
VelocityTracker = ('Velocity tracker', 2)

from .wargear import *
