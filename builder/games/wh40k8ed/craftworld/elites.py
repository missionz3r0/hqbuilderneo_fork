from builder.games.wh40k8ed.unit import ElitesUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count
from builder.games.wh40k8ed.utils import get_name, create_gears, points_price, get_cost, get_costs
from . import armory
from . import units
from . import melee
from . import ranged
from . import wargear

__author__ = 'Ivan Truskov'


class Bonesinger(ElitesUnit, armory.CraftworldUnit):
    type_name = get_name(units.Bonesinger)
    type_id = 'bonesinger_v1'
    faction = ['Warhost']
    keywords = ['Character', 'Infantry', 'Psyker']

    def __init__(self, parent):
        super(Bonesinger, self).__init__(parent, points=get_cost(units.Bonesinger),
                                         gear=[Gear('Psytronome shaper')], static=True)


class Banshees(ElitesUnit, armory.CraftworldUnit):
    type_name = get_name(units.HowlingBanshees)
    type_id = 'banshees_v1'
    faction = ['Aspect Warrior']
    keywords = ['Infantry']

    model_gear = [ranged.ShurikenPistol, melee.PowerSword]
    model = UnitDescription('Howling Banshee', options=create_gears(*model_gear))

    class ExarchWeapon(OneOf):
        def __init__(self, parent):
            super(Banshees.ExarchWeapon, self).__init__(parent, 'Exarch Weapon')
            self.variant('Power sword', gear=[Gear('Shuriken pistol'), Gear('Power sword')],
                         points=get_cost(ranged.ShurikenPistol) + get_cost(melee.PowerSword))
            self.variant('Executioner', gear=[Gear('Shuriken pistol'), Gear('Executioner')],
                         points=get_cost(ranged.ShurikenPistol) + get_cost(melee.Executioner))
            self.variant(*melee.Mirrorswords)

        @property
        def description(self):
            res = UnitDescription('Howling Banshee Exarch')
            res.add(self.cur.gear)
            return [res]

    class Boss(OptionsList):
        def __init__(self, parent):
            super(Banshees.Boss, self).__init__(parent, 'Exarch')
            self.variant('Include Howling Banshee Exarch', gear=[], points=get_cost(units.HowlingBanshees))

    def __init__(self, parent):
        super(Banshees, self).__init__(parent)
        self.boss = self.Boss(self)
        self.wep = self.ExarchWeapon(self)
        points = points_price(get_cost(units.HowlingBanshees), *Banshees.model_gear)
        self.models = Count(self, 'Howling Banshees', 5, 10, per_model=True, gear=Banshees.model.clone(), points=points)

    def check_rules(self):
        super(Banshees, self).check_rules()
        self.wep.used = self.wep.visible = self.boss.any
        self.models.min, self.models.max = (5 - self.boss.any, 10 - self.boss.any)

    def get_count(self):
        return self.models.cur + self.boss.any

    def build_power(self):
        return 3 + 3 * (self.get_count() > 5)


class Scorpions(ElitesUnit, armory.CraftworldUnit):
    type_name = get_name(units.StrikingScorpions)
    type_id = 'scorpions_v1'
    faction = ['Aspect Warrior']
    keywords = ['Infantry']

    model_gear = [ranged.ShurikenPistol, melee.ScorpionChainsword, ranged.PlasmaGrenade]
    model = UnitDescription('Striking Scorpion', options=create_gears(*model_gear))

    class ExarchWeapon(OneOf):
        def __init__(self, parent):
            super(Scorpions.ExarchWeapon, self).__init__(parent, 'Exarch Weapon')
            self.variant("Scorpion chainsword", gear=[Gear('Scorpion chainsword'), Gear('Shuriken pistol')],
                         points=get_costs(*[ranged.ShurikenPistol, melee.ScorpionChainsword]))
            self.variant("Scorpion's claw", gear=[Gear("Scorpion's claw"), Gear('Scorpion chainsword')],
                         points=get_costs(*[ranged.ScorpionsClaw, melee.ScorpionChainsword]))
            self.variant('Biting blade', gear=[Gear('Biting blade'), Gear('Shuriken pistol')],
                         points=get_costs(*[ranged.ShurikenPistol, melee.BitingBlade]))

        @property
        def description(self):
            res = UnitDescription('Striking Scorpion Exarch', options=[Gear('Plasma grenades')])
            res.add(self.cur.gear)
            return [res]

    class Boss(OptionsList):
        def __init__(self, parent):
            super(Scorpions.Boss, self).__init__(parent, 'Exarch')
            self.variant('Include Striking Scorpion Exarch', gear=[], points=get_cost(units.StrikingScorpions))

    def __init__(self, parent):
        super(Scorpions, self).__init__(parent)
        self.boss = self.Boss(self)
        self.wep = self.ExarchWeapon(self)
        points = points_price(get_cost(units.StrikingScorpions), *Scorpions.model_gear)
        self.models = Count(self, 'Striking Scorpions', 5, 10, per_model=True, gear=Scorpions.model.clone(),
                            points=points)

    def check_rules(self):
        super(Scorpions, self).check_rules()
        self.wep.used = self.wep.visible = self.boss.any
        self.models.min, self.models.max = (5 - self.boss.any, 10 - self.boss.any)

    def get_count(self):
        return self.models.cur + self.boss.any

    def build_power(self):
        return 4 + 3 * (self.get_count() > 5)


class Dragons(ElitesUnit, armory.CraftworldUnit):
    type_name = get_name(units.FireDragons)
    type_id = 'dragons_v1'
    faction = ['Aspect Warrior']
    keywords = ['Infantry']

    model_gear = [ranged.MeltaBomb, ranged.FusionGun]
    model = UnitDescription('Fire Dragon', options=create_gears(*model_gear))

    class ExarchWeapon(OneOf):
        def __init__(self, parent):
            super(Dragons.ExarchWeapon, self).__init__(parent, 'Exarch Weapon')
            self.variant(*ranged.FusionGun)
            self.variant(*ranged.BreathFlamer)
            self.variant(*ranged.Firepike)

        @property
        def description(self):
            res = UnitDescription('Fire Dragon Exarch', options=[Gear('Meltabombs')])
            res.add(self.cur.gear)
            return [res]

    class Boss(OptionsList):
        def __init__(self, parent):
            super(Dragons.Boss, self).__init__(parent, 'Exarch')
            points = points_price(get_cost(units.FireDragons), *[ranged.MeltaBomb])
            self.variant('Include Fire Dragon Exarch', gear=[], points=points)

    def __init__(self, parent):
        super(Dragons, self).__init__(parent)
        self.boss = self.Boss(self)
        self.wep = self.ExarchWeapon(self)
        points = points_price(get_cost(units.FireDragons), *Dragons.model_gear)
        self.models = Count(self, 'Fire Dragons', 5, 10, per_model=True, gear=Dragons.model.clone(), points=points)

    def check_rules(self):
        super(Dragons, self).check_rules()
        self.wep.used = self.wep.visible = self.boss.any
        self.models.min, self.models.max = (5 - self.boss.any, 10 - self.boss.any)

    def get_count(self):
        return self.models.cur + self.boss.any

    def build_power(self):
        return 6 + 6 * (self.get_count() > 5)


class Wraithguard(ElitesUnit, armory.CraftworldUnit):
    type_name = get_name(units.Wraithguard)
    type_id = 'wraithguard_v1'
    faction = ['Spirit Host']
    keywords = ['Infantry']

    model_gear = [melee.WraithguardFists]
    model = UnitDescription('Wraithguard', options=create_gears(*model_gear))

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Wraithguard.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.Wraithcannon)
            self.variant(*ranged.DScythe)

        @property
        def points(self):
            return super(Wraithguard.Weapon, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(Wraithguard, self).__init__(parent)
        points = points_price(get_cost(units.Wraithguard), *Wraithguard.model_gear)
        self.models = Count(self, 'Wraithguard', 5, 10, points=points, per_model=True)
        self.wep = self.Weapon(self)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Wraithguard', 0,
                                self.models.cur,
                                self.wep.description)
        desc.add(model)
        return desc

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 11 + 10 * (self.models.cur > 5)


class Wraithblades(ElitesUnit, armory.CraftworldUnit):
    type_name = 'Wraithblades'
    type_id = 'wraithblades_v1'
    faction = ['Spirit Host']
    keywords = ['Infantry']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Wraithblades.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.Ghostswords)
            self.variant('Ghostaxe and forceshields', gear=[
                Gear('Ghostaxe'), Gear('Forceshield')
            ], points=get_costs(melee.Ghostaxe, wargear.Forceshield))

        @property
        def points(self):
            return super(Wraithblades.Weapon, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(Wraithblades, self).__init__(parent)
        self.models = Count(self, 'Wraithblades', 5, 10, get_cost(units.Wraithblades), per_model=True)
        self.wep = self.Weapon(self)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Wraithblade', 0,
                                self.models.cur,
                                self.wep.description)
        desc.add(model)
        return desc

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 10 + 10 * (self.models.cur > 5)
