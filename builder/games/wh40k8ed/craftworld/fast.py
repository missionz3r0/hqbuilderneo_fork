from builder.games.wh40k8ed.unit import Unit, FastUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, ListSubUnit, \
    Gear, SubUnit, UnitDescription, Count, OptionalSubUnit, UnitList
from builder.games.wh40k8ed.utils import get_name, create_gears, points_price, get_cost
from . import armory
from . import units
from . import melee
from . import ranged

__author__ = 'Ivan Truskov'


class Windriders(FastUnit, armory.CraftworldUnit):
    type_name = get_name(units.Windriders)
    type_id = 'windriders_v1'
    faction = ['Warhost']
    keywords = ['Biker', 'Fly']

    model = UnitDescription('Windrider')

    class Rider(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(Windriders.Rider.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.TwinShurikenCatapult)
                self.variant(*ranged.ScatterLaser)
                self.variant(*ranged.ShurikenCannon)

        def __init__(self, parent):
            super(Windriders.Rider, self).__init__(
                parent=parent, points=get_cost(units.Windriders), name='Windrider',
            )
            self.Weapon(self)

    def __init__(self, parent):
        super(Windriders, self).__init__(parent)
        self.models = UnitList(self, self.Rider, 3, 9)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 4 * ((self.models.count + 2) / 3)


class Hawks(FastUnit, armory.CraftworldUnit):
    type_name = get_name(units.SwoopingHawks)
    type_id = 'hawks_v1'
    faction = ['Aspect Warrior']
    keywords = ['Infantry', 'Jump pack', 'Fly']

    model_gear = [ranged.Lasblaster]
    model = UnitDescription('Swooping Hawk', options=create_gears(*model_gear))

    class Exarch(Unit):
        class Ranged(OneOf):
            def __init__(self, parent):
                super(Hawks.Exarch.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.Lasblaster)
                self.variant(*ranged.HawksTalon)

        class Melee(OptionsList):
            def __init__(self, parent):
                super(Hawks.Exarch.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.PowerSword)

        def __init__(self, parent):
            super(Hawks.Exarch, self).__init__(parent, 'Swooping Hawk Exarch', points=get_cost(units.SwoopingHawks))
            self.Ranged(self)
            self.Melee(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Hawks.Boss, self).__init__(parent, 'Exarch')
            SubUnit(self, Hawks.Exarch(parent=parent))

    def __init__(self, parent):
        super(Hawks, self).__init__(parent)
        self.boss = self.Boss(self)
        points = points_price(get_cost(units.SwoopingHawks), *Hawks.model_gear)
        self.models = Count(self, 'Swooping Hawks', 5, 10, gear=Hawks.model.clone(), points=points)

    def check_rules(self):
        super(Hawks, self).check_rules()
        self.models.min, self.models.max = (5 - self.boss.count, 10 - self.boss.count)

    def get_count(self):
        return self.models.cur + self.boss.count

    def build_power(self):
        return 3 + 3 * (self.get_count() > 5)


class Spiders(FastUnit, armory.CraftworldUnit):
    type_name = get_name(units.WarpSpiders)
    type_id = 'spiders_v1'
    faction = ['Aspect Warrior']
    keywords = ['Infantry', 'Jump pack']

    model_gear = [ranged.DeathSpinner]
    model = UnitDescription('Warp Spider', options=create_gears(*model_gear))

    class Exarch(Unit):
        class Ranged(OneOf):
            def __init__(self, parent):
                super(Spiders.Exarch.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.DeathSpinner)
                self.variant('Two death spinners', gear=[Gear('Death spinner', count=2)],
                             points=2 * get_cost(ranged.DeathSpinner))

        class Melee(OptionsList):
            def __init__(self, parent):
                super(Spiders.Exarch.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Powerblades)

        def __init__(self, parent):
            super(Spiders.Exarch, self).__init__(parent, 'Warp Spider Exarch', points=get_cost(units.WarpSpiders))
            self.Ranged(self)
            self.Melee(self)

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Spiders.Boss, self).__init__(parent, 'Exarch')
            SubUnit(self, Spiders.Exarch(parent=parent))

    def __init__(self, parent):
        super(Spiders, self).__init__(parent)
        self.boss = self.Boss(self)
        points = points_price(get_cost(units.WarpSpiders), *Spiders.model_gear)
        self.models = Count(self, 'Warp Spiders', 5, 10, gear=Spiders.model.clone(), points=points)

    def check_rules(self):
        super(Spiders, self).check_rules()
        self.models.min, self.models.max = (5 - self.boss.count, 10 - self.boss.count)

    def get_count(self):
        return self.models.cur + self.boss.count

    def build_power(self):
        return 5 + 4 * (self.get_count() > 5)


class Spears(FastUnit, armory.CraftworldUnit):
    type_name = get_name(units.ShiningSpears)
    type_id = 'spears_v1'
    faction = ['Aspect Warrior']
    keywords = ['Biker', 'Fly']

    model_gear = [ranged.TwinShurikenCatapult, ranged.LaserLance]
    model = UnitDescription('Shining Spear', options=create_gears(*model_gear))

    class ExarchWeapon(OneOf):
        def __init__(self, parent):
            super(Spears.ExarchWeapon, self).__init__(parent, 'Exarch Weapon')
            self.variant(*ranged.LaserLance)
            self.variant(*ranged.StarLance)
            self.variant(*melee.ParagonSabre)

        @property
        def description(self):
            res = UnitDescription('Shining Spear Exarch', options=create_gears([ranged.TwinShurikenCatapult]))
            res.add(self.cur.gear)
            return [res]

    class Boss(OptionsList):
        def __init__(self, parent):
            super(Spears.Boss, self).__init__(parent, 'Exarch')
            points = points_price(get_cost(units.ShiningSpears), *[ranged.TwinShurikenCatapult])
            self.variant('Include Shining Spear Exarch', gear=[], points=points)

    def __init__(self, parent):
        super(Spears, self).__init__(parent)
        self.boss = self.Boss(self)
        self.wep = self.ExarchWeapon(self)
        points = points_price(get_cost(units.ShiningSpears), *Spears.model_gear)
        self.models = Count(self, 'Shining Spears', 3, 9, per_model=True, gear=Spears.model.clone(), points=points)

    def check_rules(self):
        super(Spears, self).check_rules()
        self.wep.used = self.wep.visible = self.boss.any
        self.models.min, self.models.max = (3 - self.boss.any, 9 - self.boss.any)

    def get_count(self):
        return self.models.cur + self.boss.any

    def build_power(self):
        return 5 * ((self.get_count() + 2) / 3)


class Vypers(FastUnit, armory.CraftworldUnit):
    type_name = get_name(units.Vypers)
    type_id = 'vypers_v1'
    faction = ['Warhost']
    keywords = ['Vehicle', 'Fly']

    class Vyper(ListSubUnit):
        type_name = 'Viper'

        class MainWeapon(OneOf):
            def __init__(self, parent):
                super(Vypers.Vyper.MainWeapon, self).__init__(parent, 'Main weapon')
                armory.add_heavy_weapons(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Vypers.Vyper.Weapon, self).__init__(parent, 'Secondary weapon')
                self.variant(*ranged.TwinShurikenCatapult)
                self.variant(*ranged.ShurikenCannon)

        def __init__(self, parent):
            super(Vypers.Vyper, self).__init__(parent, points=get_cost(units.Vypers))
            self.MainWeapon(self)
            self.Weapon(self)

    def __init__(self, parent):
        super(Vypers, self).__init__(parent)
        self.models = UnitList(self, self.Vyper, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 4 * self.get_count()
