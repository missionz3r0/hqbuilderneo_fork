from builder.games.wh40k8ed.unit import TroopsUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count
from builder.games.wh40k8ed.utils import get_name, create_gears, points_price, get_cost
from . import armory
from . import units
from . import melee
from . import ranged
from . import wargear
__author__ = 'Ivan Truskov'


class GuardianDefenders(TroopsUnit, armory.CraftworldUnit):
    type_name = get_name(units.GuardianDefenders)
    type_id = 'defenders_v1'
    faction = ['Warhost']
    keywords = ['Infantry', 'Artillery', 'Heavy Weapon Platform']

    model_gear = [ranged.ShurikenCatapult, ranged.PlasmaGrenade]

    model = UnitDescription('Guardian', options=create_gears(*model_gear))

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(GuardianDefenders.HeavyWeapon, self).__init__(parent, 'Heavy Weapons Platform')
            armory.add_heavy_weapons(self)

        @property
        def description(self):
            gear = self.cur.gear
            return [UnitDescription('Heavy Weapons Platform', options=[gear])]

    def __init__(self, parent):
        super(GuardianDefenders, self).__init__(parent)
        points = points_price(get_cost(units.GuardianDefenders), *GuardianDefenders.model_gear)
        self.models = Count(self, 'Guardians', 10, 20, per_model=True, gear=GuardianDefenders.model.clone(),
                            points=points)
        self.platforms = Count(self, 'Heavy Weapon platforms', 0, 2, per_model=True, used=False,
                               points=get_cost(units.HeavyWeaponPlatform))
        self.hwp1 = self.HeavyWeapon(self)
        self.hwp2 = self.HeavyWeapon(self)

    def check_rules(self):
        super(GuardianDefenders, self).check_rules()
        self.platforms.max = self.models.cur / 10
        self.hwp1.used = self.hwp1.visible = self.platforms.cur > 0
        self.hwp2.used = self.hwp2.visible = self.platforms.cur > 1

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 5 + ((self.models.cur > 10) * 4)

    def build_statistics(self):
        return {'Models': self.get_count() + self.platforms.cur}

    def build_points(self):
        return super(GuardianDefenders, self).build_points() + self.platforms.points
    

class StormGuardians(TroopsUnit, armory.CraftworldUnit):
    type_name = get_name(units.StormGuardians)
    type_id = 'stormguards_v1'
    faction = ['Warhost']
    keywords = ['Infantry']

    model_gear = [ranged.ShurikenPistol, ranged.PlasmaGrenade]
    model_cost = get_cost(units.StormGuardians)

    model = UnitDescription('Guardian', options=create_gears(*model_gear))

    class Guardian(Count):
        @property
        def description(self):
            return StormGuardians.model.clone().add([Gear(melee.AeldariBlade)]). \
                set_count(self.cur - sum(c.cur for c in self.parent.alt))

    def __init__(self, parent):
        super(StormGuardians, self).__init__(parent)
        self.models = self.Guardian(self, 'Guardians', 8, 24, per_model=True, points=self.model_cost)
        self.flame = Count(self, 'Flamer', 0, 2,
                           gear=UnitDescription('Guardian', options=create_gears([ranged.Flamer])),
                           points=get_cost(ranged.Flamer))
        self.fgun = Count(self, 'Fusion gun', 0, 2,
                          gear=UnitDescription('Guardian', options=create_gears([ranged.FusionGun])),
                          points=get_cost(ranged.FusionGun))
        self.psword = Count(self, 'Power sword', 0, 2, gear=StormGuardians.model.clone().add(Gear(melee.PowerSword[0])),
                            points=get_cost(melee.PowerSword))
        self.chsword = Count(self, 'Chainsword', 0, 8, gear=StormGuardians.model.clone().add(Gear(melee.Chainsword[0])),
                             points=get_cost(melee.Chainsword))
        self.alt = [self.flame, self.fgun, self.psword, self.chsword]

    def check_rules(self):
        super(StormGuardians, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.fgun])
        self.chsword.max = self.models.cur - sum(c.cur for c in [self.flame, self.fgun, self.psword])

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 3 * ((self.models.cur + 7) / 8)


class Rangers(TroopsUnit, armory.CraftworldUnit):
    type_name = get_name(units.Rangers)
    type_id = 'rangers_v1'
    faction = ['Warhost']
    keywords = ['Infantry']

    model_gear = [ranged.ShurikenPistol, ranged.RangerLongRifle]

    model = UnitDescription('Ranger', options=create_gears(*model_gear))

    def __init__(self, parent):
        super(Rangers, self).__init__(parent)
        points = points_price(get_cost(units.Rangers), *Rangers.model_gear)
        self.models = Count(self, 'Rangers', 5, 10, per_model=True, points=points,
                            gear=Rangers.model.clone())

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 3 + 5 * (self.models.cur > 5)


class DireAvengers(TroopsUnit, armory.CraftworldUnit):
    type_name = get_name(units.DireAvengers)
    type_id = 'avengers_v1'
    faction = ['Aspect Warrior']
    keywords = ['Infantry']

    model_gear = [ranged.AvengerShurikenCatapult, ranged.PlasmaGrenade]
    model = UnitDescription('Dire Avenger', options=create_gears(*model_gear))

    class ExarchWeapon(OneOf):
        def __init__(self, parent):
            super(DireAvengers.ExarchWeapon, self).__init__(parent, 'Exarch Weapon')
            self.variant(*ranged.AvengerShurikenCatapult)
            self.variant('Two avenger shuriken catapults', gear=[Gear(ranged.AvengerShurikenCatapult[0], count=2)],
                         points=2 * get_cost(ranged.AvengerShurikenCatapult))
            self.variant('Shuriken pistol and power glaive',
                         gear=[Gear(ranged.ShurikenPistol[0]), Gear(melee.PowerGlaive[0])],
                         points=get_cost(ranged.ShurikenPistol) + get_cost(melee.PowerGlaive))
            self.variant('Shuriken pistol and diresword',
                         gear=[Gear(ranged.ShurikenPistol[0]), Gear(melee.Diresword[0])],
                         points=get_cost(ranged.ShurikenPistol) + get_cost(melee.Diresword))
            self.variant('Shimmershield and power glaive',
                         gear=[Gear(wargear.Shimmershield[0]), Gear(melee.PowerGlaive[0])],
                         points=get_cost(wargear.Shimmershield) + get_cost(melee.PowerGlaive))

        @property
        def description(self):
            res = UnitDescription('Dire Avenger Exarch')
            res.add(self.cur.gear).add(Gear(ranged.PlasmaGrenade[0]))
            return [res]

    class Boss(OptionsList):
        def __init__(self, parent):
            super(DireAvengers.Boss, self).__init__(parent, 'Exarch')
            self.variant('Include Dire Avenger Exarch', gear=[],
                         points=get_cost(units.DireAvengers))

    def __init__(self, parent):
        super(DireAvengers, self).__init__(parent)
        self.boss = self.Boss(self)
        self.wep = self.ExarchWeapon(self)
        points = points_price(get_cost(units.DireAvengers), *DireAvengers.model_gear)
        self.models = Count(self, 'Dire Avengers', 5, 10, per_model=True, gear=DireAvengers.model.clone(),
                            points=points)

    def check_rules(self):
        super(DireAvengers, self).check_rules()
        self.wep.used = self.wep.visible = self.boss.any
        self.models.min, self.models.max = (5 - self.boss.any, 10 - self.boss.any)

    def get_count(self):
        return self.models.cur + self.boss.any

    def build_power(self):
        return 3 + 3 * (self.get_count() > 5)
