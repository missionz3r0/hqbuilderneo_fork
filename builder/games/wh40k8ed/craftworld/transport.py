from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import OneOf
from builder.games.wh40k8ed.utils import get_name, get_cost
from . import armory
from . import units
from . import ranged

__author__ = 'Ivan Truskov'


class WaveSerpent(TransportUnit, armory.CraftworldUnit):
    type_name = get_name(units.WaveSerpent)
    type_id = 'waveserpent_v1'
    faction = ['Warhost']
    keywords = ['Vehicle', 'Transport', 'Fly']

    power = 9

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(WaveSerpent.MainWeapon, self).__init__(parent, 'Main Weapon')
            self.variant(*ranged.TwinShurikenCannon)
            self.variant(*ranged.TwinBrightLance)
            self.variant(*ranged.TwinStarcannon)
            self.variant(*ranged.TwinAeldariMissileLauncher)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(WaveSerpent.Weapon, self).__init__(parent, 'Secondary Weapon')
            self.variant(*ranged.TwinShurikenCatapult)
            self.variant(*ranged.ShurikenCannon)

    def __init__(self, parent):
        super(WaveSerpent, self).__init__(parent, points=get_cost(units.WaveSerpent))
        self.MainWeapon(self)
        self.Weapon(self)
        armory.Vehicle(self)
