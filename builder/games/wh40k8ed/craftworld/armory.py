__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList

from . import wargear, ranged


def add_heavy_weapons(instance):
    instance.variant(*ranged.AeldariMissileLauncher)
    instance.variant(*ranged.BrightLance)
    instance.variant(*ranged.ScatterLaser)
    instance.variant(*ranged.ShurikenCannon)
    instance.variant(*ranged.Starcannon)


class Vehicle(OptionsList):
    def __init__(self, parent):
        super(Vehicle, self).__init__(parent, 'Vehicle equipment')
        self.variant(*wargear.CrystalTargetingMatrix)
        self.variant(*wargear.SpiritStones)
        self.variant(*wargear.StarEngines)
        self.variant(*wargear.VectoredEngines)


class EldarUnit(Unit):
    faction = ['Aeldari', 'Asuryani', 'Ynnari']
    wiki_faction = 'Craftworlds'


class CraftworldUnit(EldarUnit):
    faction = ['<Craftworld>']

    @classmethod
    def calc_faction(cls):
        return ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
