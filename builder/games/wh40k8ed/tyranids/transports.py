__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TransportUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList, ListSubUnit, UnitList
from . import ranged, melee, armory, units, biomorphs
from builder.games.wh40k8ed.utils import *


class Tyrannocyte(TransportUnit, armory.FleetUnit):
    type_name = get_name(units.Tyrannocyte)
    type_id = 'tyrannocyte_v1'
    keywords = ['Monster', 'Fly']
    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Tyrannocyte.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Five deathspitters', get_cost(ranged.Deathspitter) * 5,
                         gear=[Gear('Deathspitter', count=5)])
            self.variant('Five barbed stranglers', get_cost(ranged.BarbedStrangler) * 5,
                         gear=[Gear('Barbed strangler', count=5)])
            self.variant('Five venom cannons', get_cost(ranged.VenomCannon) * 5,
                         gear=[Gear('Venom cannon', count=5)])

    def __init__(self, parent):
        super(Tyrannocyte, self).__init__(parent, points=get_cost(units.Tyrannocyte))
        self.Weapon(self)
