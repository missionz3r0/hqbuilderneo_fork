__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList, ListSubUnit, UnitList
from . import ranged, melee, armory, units, biomorphs
from builder.games.wh40k8ed.utils import *


class GuardBrood(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.TyrantGuard)
    type_id = 'tyrant_guard_brood_v1'
    keywords = ['Infantry']
    model_gear = [melee.RendingClaws]
    model_points = points_price(get_cost(units.TyrantGuard), *model_gear)
    model = UnitDescription('Tyrant Guard', options=create_gears(*model_gear))

    class GuardCount(Count):
        @property
        def description(self):
            return [GuardBrood.model.clone().add(Gear('Scything talons')).set_count(
                self.cur - self.parent.claws.cur - self.parent.bs.cur
            )]

    class Options(OptionsList):
        def __init__(self, parent):
            super(GuardBrood.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.ToxinSacs, per_model=True)
            self.variant(*biomorphs.AdrenalGlands, per_model=True)

        @property
        def points(self):
            return super(GuardBrood.Options, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(GuardBrood, self).__init__(parent)
        self.models = self.GuardCount(self, 'Tyrant Guard', 3, 6, self.model_points)
        self.claws = Count(
            self, name='Crushing claws', min_limit=0, max_limit=1, points=get_cost(melee.CrushingClaws), per_model=True,
            gear=GuardBrood.model.clone().add(Gear('Crushing claws'))
        )
        self.bs = Count(
            self, name='Lash whip and bonesword', min_limit=0, max_limit=1, points=get_cost(melee.LashWhipAndBonesword),
            per_model=True,
            gear=GuardBrood.model.clone().add(Gear('Lash whip')).add(Gear('Bonesword'))
        )
        self.Options(self)

    def check_rules(self):
        super(GuardBrood, self).check_rules()
        Count.norm_counts(0, self.get_count(), [self.claws, self.bs])

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 1 + 6 * (1 + (self.models.cur > 3))


class HiveGuard(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.HiveGuard)
    type_id = 'hive_guard_brood_v1'
    keywords = ['Infantry']
    model_name = 'Hive Guard'
    model_points = get_cost(units.HiveGuard)
    model = UnitDescription(model_name)

    class Options(OptionsList):
        def __init__(self, parent):
            super(HiveGuard.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.ToxinSacs, per_model=True)
            self.variant(*biomorphs.AdrenalGlands, per_model=True)

        @property
        def points(self):
            return super(HiveGuard.Options, self).points * self.parent.models.cur

    class GuardCount(Count):
        @property
        def description(self):
            return [HiveGuard.model.clone().add(Gear('Impaler cannon')).set_count(self.cur - self.parent.shock.cur)]

    def __init__(self, parent):
        super(HiveGuard, self).__init__(parent)
        self.models = self.GuardCount(self, self.model_name, 3, 6, self.model_points + get_cost(ranged.ImpalerCannon),
                                      per_model=True)
        self.shock = Count(
            self, name='Shockcannon', points=get_cost(ranged.Shockcannon), min_limit=0, max_limit=1, per_model=True,
            gear=self.model.clone().add(Gear('Shockcannon'))
        )
        self.Options(self)

    def check_rules(self):
        super(HiveGuard, self).check_rules()
        self.shock.max = self.models.cur

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 1 + 6 * (1 + (self.models.cur > 3))

    def build_points(self):
        return super(HiveGuard, self).build_points() - get_cost(ranged.ImpalerCannon) * self.shock.cur


class Lictor(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.Lictor)
    type_id = 'lictor_v1'
    keywords = ['Infantry']
    power = 2

    def __init__(self, parent):
        gear = [ranged.FleshHooks, melee.GraspingTalons, melee.RendingClaws]
        cost = points_price(get_cost(units.Lictor), *gear)
        super(Lictor, self).__init__(parent, points=cost,
                                     static=True,
                                     gear=create_gears(*gear))


class Deathleaper(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.Deathleaper)
    type_id = 'deathleaper_v1'
    keywords = ['Character', 'Infantry', 'Lictor']
    power = 5

    def __init__(self, parent):
        super(Deathleaper, self).__init__(parent, gear=[
            Gear('Flesh hooks'),
            Gear('Grasping talons'),
            Gear('Rending claws'),
        ], static=True, unique=True, points=get_cost(units.Deathleaper))


class Zoanthrope(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.Zoanthropes)
    type_id = 'zoanthrope_v1'
    keywords = ['Infantry', 'Fly', 'Psyker', 'Synapse']

    model_name = 'Zoanthrope'
    model_points = get_cost(units.Zoanthropes)
    model = UnitDescription(model_name, points=model_points,
                            options=[Gear('Claws and teeth')])

    def __init__(self, parent):
        super(Zoanthrope, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 3, 6, self.model_points, per_model=True,
            gear=self.model)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 2 * self.models.cur


class Maleceptor(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.Maleceptor)
    type_id = 'maleceptor_v1'
    keywords = ['Monster', 'Psyker', 'Synapse']
    power = 9

    def __init__(self, parent):
        gear = [melee.MassiveScythingTalonsTervigon]
        cost = points_price(get_cost(units.Maleceptor), *gear)
        super(Maleceptor, self).__init__(parent, self.type_name, cost,
                                         gear=create_gears(*gear))


class Venomethrope(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.Venomthropes)
    type_id = 'venomethrope_v1'
    keywords = ['Infantry', 'Fly']

    model_name = 'Venomthrope'
    model_points = points_price(get_cost(units.Venomthropes), melee.ToxicLashes)

    def __init__(self, parent):
        super(Venomethrope, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 3, 6, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name,
                                 options=[Gear('Toxic lashes')])
        )

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 1 + 4 * (1 + (self.models.cur > 3))


class Pyrovore(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.Pyrovores)
    type_id = 'pyrovore_v1'
    keywords = ['Infantry']

    model_name = 'Pyrovore'
    model_gear = [ranged.Flamespurt, melee.AcidMaw]
    model_points = points_price(get_cost(units.Pyrovores), *model_gear)
    power = 2

    def __init__(self, parent):
        super(Pyrovore, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 1, 3, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name,
                                 options=create_gears(*self.model_gear))
        )

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.get_count()


class Haruspex(ElitesUnit, armory.FleetUnit):
    type_name = 'Haruspex'
    type_id = 'haruspex_v1'
    keywords = ['Monster']
    power = 10

    def __init__(self, parent):
        gear = [ranged.GraspingTongue, melee.RavenousMaw,
                melee.ShovellingClaws]
        cost = points_price(get_cost(units.Haruspex), *gear)
        super(Haruspex, self).__init__(parent, points=cost,
                                       gear=create_gears(*gear),
                                       static=True)


class RedTerror(ElitesUnit, armory.FleetUnit):
    type_name = get_name(units.TheRedTerror)
    type_id = 'red_terror_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        super(RedTerror, self).__init__(parent, gear=[
            Gear('Prehencile pincer tail'),
            Gear('Scything talons', count=2),
        ], static=True, unique=True, points=get_cost(units.TheRedTerror))
