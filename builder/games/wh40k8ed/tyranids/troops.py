__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList, ListSubUnit, UnitList
from . import ranged, melee, armory, units, biomorphs
from builder.games.wh40k8ed.utils import *


class TyranidWarriorBrood(TroopsUnit, armory.FleetUnit):
    type_name = get_name(units.TyranidWarriors)
    type_id = 'tyranid_warrior_brood_v1'
    keywords = ['Infantry', 'Synapse']

    class Options(OptionsList):
        def __init__(self, parent):
            super(TyranidWarriorBrood.Options, self).__init__(parent, name='Options',
                                                              used=False)
            self.variant(*biomorphs.ToxinSacsHTCWar, per_model=True)
            self.variant(*biomorphs.AdrenalGlands, per_model=True)
            self.variant(*ranged.FleshHooks, per_model=True)

    class Warrior(ListSubUnit):
        type_name = 'Tyranid Warrior'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(TyranidWarriorBrood.Warrior.Weapon1, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.Devourer)
                armory.add_basic_weapons(self)
                armory.add_melee_weapons(self)
                armory.add_basic_cannons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(TyranidWarriorBrood.Warrior.Weapon2, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.ScythingTalons)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(TyranidWarriorBrood.Warrior, self).__init__(parent, points=get_cost(units.TyranidWarriors))

            self.w1 = self.Weapon1(self)
            self.w2 = self.Weapon2(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.w1.cur in self.w1.cannons

        def build_description(self):
            res = super(TyranidWarriorBrood.Warrior, self).build_description()
            res.add(self.root_unit.opt.description)
            if not self.root_unit.use_power:
                res.add_points(self.root_unit.opt.points)
            return res

    def __init__(self, parent):
        super(TyranidWarriorBrood, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=3, max_limit=9)
        self.opt = self.Options(self)

    def count_cannons(self):
        return sum(c.has_heavy() for c in self.models.units)

    def check_rules(self):
        super(TyranidWarriorBrood, self).check_rules()
        spec = self.count_cannons()
        if spec > self.get_count() / 3:
            self.error('Only {} Tyranid Warrior may take basic bio-cannons (taken {})'
                       .format(self.get_count() / 3, spec))

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 1 + 4 * ((self.get_count() + 2) / 3)

    def build_points(self):
        return super(TyranidWarriorBrood, self).build_points() +\
            self.opt.points * (self.get_count())
    

class Genestealers(TroopsUnit, armory.FleetUnit):
    type_name = get_name(units.Genestealers)
    type_id = 'genestealer_brood_v1'
    keywords = ['Infantry']
    power = 4

    class GenestealerCount(Count):
        @property
        def description(self):
            return [Genestealers.model.clone().set_count(self.cur - self.parent.talons.cur)]

    class Options(OptionsList):
        def __init__(self, parent):
            super(Genestealers.Options, self).__init__(parent, name='Options',
                                                       used=False)
            self.variant(*biomorphs.ToxinSacsHTCWar, per_model=True)
            self.variant(*biomorphs.ExtendedCarapace, per_model=True)

    class Genestealer(ListSubUnit):
        type_name = 'Genestealer'

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(Genestealers.Genestealer.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*melee.ScythingTalons)
                self.hook = self.variant(*ranged.FleshHooks)
                self.maw = self.variant(*melee.AcidMaw)

        def __init__(self, parent):
            super(Genestealers.Genestealer, self).__init__(parent, points=get_costs(units.Genestealers, melee.RendingClaws),
                                                           gear=create_gears(melee.RendingClaws))
            self.wep = self.Weapon(self)

        @ListSubUnit.count_gear
        def has_hook(self):
            return self.wep.hook.value

        @ListSubUnit.count_gear
        def has_maw(self):
            return self.wep.maw.value

        def build_description(self):
            res = super(Genestealers.Genestealer, self).build_description()
            res.add(self.root_unit.opt.description)
            if not self.root_unit.use_power:
                res.add_points(self.root_unit.opt.points)
            return res

    def __init__(self, parent):
        super(Genestealers, self).__init__(parent)
        self.models = UnitList(self, self.Genestealer, 5, 20)
        self.opt = self.Options(self)

    def check_rules(self):
        super(Genestealers, self).check_rules()
        hc = sum(u.has_hook() for u in self.models.units)
        if hc > self.models.count / 4:
            sefl.error('For every 4 models in unit one model may take flesh hooks; taken: {}'.format(hc))
        mc = sum(u.has_maw() for u in self.models.units)
        if mc > self.models.count / 4:
            self.error('For every 4 models in unit one model may take acid maw; taken: {}'.format(mc))

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * ((self.get_count() + 4) / 5)

    def build_points(self):
        return super(Genestealers, self).build_points() +\
            self.opt.points * (self.get_count() - 1)


class Termagant(TroopsUnit, armory.FleetUnit):
    type_name = get_name(units.Termagants)
    type_id = 'termagant_brood_v1'
    keywords = ['Infantry']
    model_name = 'Termagant'
    model = UnitDescription(model_name)
    power = 3

    class Options(OptionsList):
        def __init__(self, parent):
            super(Termagant.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.ToxinSacs, per_model=True)
            self.variant(*biomorphs.AdrenalGlands, per_model=True)

        @property
        def points(self):
            return super(Termagant.Options, self).points * self.parent.models.cur

    class GantCount(Count):
        @property
        def description(self):
            return [Termagant.model.clone().add(Gear('Fleshborer')).set_count(
                self.cur - sum(o.cur for o in [self.parent.fist, self.parent.devourer]))]

    def __init__(self, parent):
        super(Termagant, self).__init__(parent)
        self.models = self.GantCount(self, self.model_name, 10, 30, get_cost(units.Termagants), per_model=True)
        self.fist = Count(
            self, name='Spinefists', points=get_cost(ranged.Spinefists), min_limit=0, max_limit=10,
            gear=self.model.clone().add(Gear('Spinefists'))
        )
        # self.rifle = Count(
        #     self, name='Spike rifle', points=0, min_limit=0, max_limit=10,
        #     gear=self.model.clone().add(Gear('Spike rifle'))
        # )
        self.devourer = Count(
            self, name='Devourer', min_limit=0, max_limit=10, points=get_cost(ranged.Devourer), per_model=True,
            gear=self.model.clone().add(Gear('Devourer'))
        )
        # self.strangleweb = Count(
        #     self, name='Strangleweb', min_limit=0, max_limit=1, points=0, per_model=True,
        #     gear=self.model.clone().add(Gear('Strangleweb'))
        # )
        self.Options(self)

    def check_rules(self):
        super(Termagant, self).check_rules()
        # self.strangleweb.max = int(self.models.cur / 10)
        Count.norm_counts(0, self.get_count(), [self.fist, self.devourer])

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * ((self.get_count() + 9) / 10)


class Hormagant(TroopsUnit, armory.FleetUnit):
    type_name = get_name(units.Hormagaunts)
    type_id = 'hormagant_brood_v1'
    keywords = ['Infantry']
    model_name = 'Hormagaunt'
    power = 3

    class Options(OptionsList):
        def __init__(self, parent):
            super(Hormagant.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.AdrenalGlands, per_model=True)
            self.variant(*biomorphs.ToxinSacsHormagaunt, per_model=True)

        @property
        def points(self):
            return super(Hormagant.Options, self).points * self.parent.models.cur

    def __init__(self, parent):
        super(Hormagant, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 10, 30, get_cost(units.Hormagaunts), per_model=True,
            gear=UnitDescription(self.model_name, points=get_cost(units.Hormagaunts),
                                 options=[Gear('Scything talons')])
        )
        self.Options(self)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * ((self.get_count() + 9) / 10)


class Ripper(TroopsUnit, armory.FleetUnit):
    type_name = get_name(units.RipperSwarms)
    type_id = 'swarm_brood_v1'
    keywords = ['Swarm']
    model_name = 'Ripper Swarm'
    power = 2

    class Options(OptionsList):
        def __init__(self, parent):
            super(Ripper.Options, self).__init__(parent, name='Options')
            self.fist = self.variant(*ranged.Spinemaws, per_model=True, gear=[])

        @property
        def points(self):
            return super(Ripper.Options, self).points * self.parent.models.cur

    class RipperCount(Count):
        @property
        def description(self):
            return [UnitDescription(
                Ripper.model_name,
                # points=Ripper.model_points,
                options=[Gear('Spinemaw')] if self.parent.opt.fist.value else [],
                count=self.cur
            )]

    def __init__(self, parent):
        super(Ripper, self).__init__(parent)
        self.models = self.RipperCount(self, self.model_name, 3, 9, get_cost(units.RipperSwarms), per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * ((self.get_count() + 2) / 3)
