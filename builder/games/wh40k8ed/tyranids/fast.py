__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList, ListSubUnit, UnitList
from . import ranged, melee, armory, units, biomorphs
from builder.games.wh40k8ed.utils import *


class Gargoyle(FastUnit, armory.FleetUnit):
    type_name = get_name(units.Gargoyles)
    type_id = 'gargoyle_brood_v1'
    keywords = ['Infantry', 'Fly']
    model_name = 'Gargoyle'
    model_gear = [ranged.Fleshborer, melee.BlindingVenom]
    model_points = points_price(get_cost(units.Gargoyles), *model_gear)
    power = 3

    class Options(OptionsList):
        def __init__(self, parent):
            super(Gargoyle.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.AdrenalGlands, per_model=True)
            self.variant(*biomorphs.ToxinSacs, per_model=True)

        @property
        def points(self):
            return super(Gargoyle.Options, self).points * self.parent.models.cur

    def __init__(self, parent):
        super(Gargoyle, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 10, 30, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name,
                                 options=create_gears(*self.model_gear))
        )
        self.Options(self)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * ((self.models.cur + 9) / 10)


class RavenerBrood(FastUnit, armory.FleetUnit):
    type_name = get_name(units.Raveners)
    type_id = 'ravener_brood_v1'
    keywords = ['Infantry']
    power = 4

    class Warrior(ListSubUnit):
        type_name = 'Ravener'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(RavenerBrood.Warrior.Weapon1, self).__init__(parent, name='Weapon')
                self.variant(*melee.ScythingTalons)
                self.variant(*melee.RendingClaws)

        class Weapon2(OptionsList):
            def __init__(self, parent):
                super(RavenerBrood.Warrior.Weapon2, self).__init__(parent, name='',
                                                                   limit=1)
                self.variant(*ranged.Spinefists)
                self.variant(*ranged.Devourer)
                self.variant(*ranged.Deathspitter)

        def __init__(self, parent):
            super(RavenerBrood.Warrior, self).__init__(parent, points=get_cost(units.Raveners),
                                                       gear=[Gear('Scything talons')])

            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(RavenerBrood, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=3, max_limit=9)

    def build_power(self):
        return self.power * ((self.models.count + 2) / 3)


class MucolidSpores(FastUnit, armory.FleetUnit):
    type_name = get_name(units.MucolidSpores)
    type_id = 'mucolid_spore_v1'
    keywords = ['Fly']
    power = 1

    def __init__(self, parent):
        super(MucolidSpores, self).__init__(parent)
        self.mines = Count(self, 'Mucolid Spore', 1, 3, get_cost(units.MucolidSpores),
                           gear=UnitDescription('Mucolid spore'), per_model=True)

    def get_count(self):
        return self.mines.cur

    def build_power(self):
        return self.power * self.models.cur


class SporeMine(FastUnit, armory.FleetUnit):
    type_name = get_name(units.SporeMines)
    type_id = 'spore_mine_cluster_v1'
    keywords = ['Fly']

    def __init__(self, parent):
        super(SporeMine, self).__init__(parent)
        self.mines = Count(self, 'Spore Mine', 3, 9, get_cost(units.SporeMines),
                           gear=UnitDescription('Spore Mine'), per_model=True)

    def get_count(self):
        return self.mines.cur

    def build_power(self):
        return 1 + (self.mines.cur + 2) / 3


class TyranidShrikeBrood(FastUnit, armory.FleetUnit):
    type_name = 'Tyranid Shrike Brood'
    type_id = 'tyranid_shrike_brood_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(TyranidShrikeBrood.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.AdrenalGlands, per_model=True)
            self.variant(*biomorphs.ToxinSacsHTCWar, per_model=True)
            self.variant(*ranged.FleshHooks, per_model=True)

        @property
        def points(self):
            return super(TyranidShrikeBrood.Options, self).points * self.parent.get_count()

    class Warrior(ListSubUnit):
        type_name = 'Tyranid Shrike'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(TyranidShrikeBrood.Warrior.Weapon1, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.Devourer)
                armory.add_basic_weapons(self)
                armory.add_basic_cannons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(TyranidShrikeBrood.Warrior.Weapon2, self).__init__(parent, 'Melee weapon')
                # self.variant('Scything talons')
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(TyranidShrikeBrood.Warrior, self).__init__(parent, points=26)

            self.w1 = self.Weapon1(self)
            self.w2 = self.Weapon2(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.w1.cur in self.w1.cannons

    def __init__(self, parent):
        super(TyranidShrikeBrood, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=3, max_limit=9)
        self.Options(self)

    def check_rules(self):
        super(TyranidShrikeBrood, self).check_rules()
        spec = sum(c.has_heavy() for c in self.models.units)
        if spec > (self.get_count() / 3):
            self.error('Only {} Tyranid Shrikes may take basic bio-cannons (taken {})'\
                       .format((self.get_count() / 3), spec))

    def get_count(self):
        return self.models.count


class SkySlasherBrood(FastUnit, armory.FleetUnit):
    type_name = 'Sky-Slasher Swarm'
    type_id = 'sky_slasher_brood_v1'
    keywords = ['Swarm']
    model_name = 'Sky-Slasher Swarm'
    model_points = 11

    class Options(OptionsList):
        def __init__(self, parent):
            super(SkySlasherBrood.Options, self).__init__(parent, name='Options')
            self.fist = self.variant('Spinemaws', points=2, per_model=True, gear=[])

        @property
        def points(self):
            return super(SkySlasherBrood.Options, self).points * self.parent.models.cur

    class SwarmCount(Count):
        @property
        def description(self):
            return [UnitDescription(
                SkySlasherBrood.model_name,
                # points=SkySlasherBrood.model_points,
                options=[Gear('Claws and teeth')] + ([Gear('Spinemaw')] if self.parent.opt.fist.value else []),
                count=self.cur
            )]

    def __init__(self, parent):
        super(SkySlasherBrood, self).__init__(parent)
        self.models = self.SwarmCount(self, self.model_name, 3, 9, self.model_points, per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 2 * ((self.models.cur + 2) / 3)
