import itertools
from .rosters import Wh40k8edBase
from .gen_detachments import *

from .elucidian_starstriders import detachments as starstriders_detachments
from .gellerpox_infected import detachments as infected_detachments
armies = []

#################################################################
# Code below is generated
#################################################################


class Canoptek8ed(Wh40k8edBase):
    army_name = 'Army of Canoptek'
    faction_base = 'CANOPTEK'
    alternate_factions = []
    army_id = 'canoptek'

    def add_detachments(self):
        for det in [DetachAuxilary_canoptek]:
            self.det.build_detach(det, 'CANOPTEK', group=det.faction_base)


class _sept_8ed(Wh40k8edBase):
    army_name = 'Army of <Sept>'
    faction_base = '<SEPT>'
    alternate_factions = ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT", "KE'LSHAN SEPT"]
    army_id = '_sept_'

    def add_detachments(self):
        for det in [DetachPatrol__sept_, DetachBatallion__sept_, DetachBrigade__sept_, DetachVanguard__sept_, DetachSpearhead__sept_, DetachOutrider__sept_, DetachCommand__sept_, DetachSuperHeavy__sept_, DetachSuperHeavyAux__sept_, DetachAirWing__sept_, DetachFort__sept_, DetachAuxilary__sept_, DetachPatrol_t_au_empire, DetachBatallion_t_au_empire, DetachBrigade_t_au_empire, DetachVanguard_t_au_empire, DetachSpearhead_t_au_empire, DetachOutrider_t_au_empire, DetachCommand_t_au_empire, DetachSuperHeavy_t_au_empire, DetachSuperHeavyAux_t_au_empire, DetachAirWing_t_au_empire, DetachFort_t_au_empire]:
            self.det.build_detach(det, '<SEPT>', group=det.faction_base)


class Ravenwing8ed(Wh40k8edBase):
    army_name = 'Army of Ravenwing'
    faction_base = 'RAVENWING'
    alternate_factions = []
    army_id = 'ravenwing'

    def add_detachments(self):
        for det in [DetachVanguard_ravenwing, DetachOutrider_ravenwing, DetachCommand_ravenwing, DetachAirWing_ravenwing, DetachAuxilary_ravenwing, DetachVanguard_dark_angels, DetachOutrider_dark_angels, DetachCommand_dark_angels, DetachAirWing_dark_angels, DetachAuxilary_dark_angels, DetachVanguard_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachAirWing_imperium, DetachVanguard_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachAirWing_adeptus_astartes]:
            self.det.build_detach(det, 'RAVENWING', group=det.faction_base)


class Blood_angels8ed(Wh40k8edBase):
    army_name = 'Army of Blood Angels'
    faction_base = 'BLOOD ANGELS'
    alternate_factions = ['FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>']
    army_id = 'blood_angels'

    def add_detachments(self):
        for det in [DetachPatrol_blood_angels, DetachBatallion_blood_angels, DetachBrigade_blood_angels, DetachVanguard_blood_angels, DetachSpearhead_blood_angels, DetachOutrider_blood_angels, DetachCommand_blood_angels, DetachSuperHeavy_blood_angels, DetachSuperHeavyAux_blood_angels, DetachAirWing_blood_angels, DetachAuxilary_blood_angels, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachAirWing_imperium, DetachPatrol_adeptus_astartes, DetachBatallion_adeptus_astartes, DetachBrigade_adeptus_astartes, DetachVanguard_adeptus_astartes, DetachSpearhead_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachAirWing_adeptus_astartes, DetachVanguard_death_company, DetachCommand_death_company]:
            self.det.build_detach(det, 'BLOOD ANGELS', group=det.faction_base)


class Asuryani8ed(Wh40k8edBase):
    army_name = 'Army of Asuryani'
    faction_base = 'ASURYANI'
    alternate_factions = []
    army_id = 'asuryani'

    def add_detachments(self):
        for det in [DetachPatrol_asuryani, DetachBatallion_asuryani, DetachBrigade_asuryani, DetachVanguard_asuryani, DetachSpearhead_asuryani, DetachOutrider_asuryani, DetachCommand_asuryani, DetachSuperHeavy_asuryani, DetachSuperHeavyAux_asuryani, DetachAirWing_asuryani, DetachAuxilary_asuryani, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachSuperHeavy_aeldari, DetachSuperHeavyAux_aeldari, DetachAirWing_aeldari, DetachPatrol__craftworld_, DetachBatallion__craftworld_, DetachBrigade__craftworld_, DetachVanguard__craftworld_, DetachSpearhead__craftworld_, DetachOutrider__craftworld_, DetachCommand__craftworld_, DetachSuperHeavy__craftworld_, DetachSuperHeavyAux__craftworld_, DetachAirWing__craftworld_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari, DetachSuperHeavy_ynnari, DetachSuperHeavyAux_ynnari, DetachAirWing_ynnari, DetachVanguard_spirit_host, DetachSpearhead_spirit_host, DetachCommand_spirit_host, DetachSuperHeavy_spirit_host, DetachSuperHeavyAux_spirit_host, DetachAirWing_spirit_host, DetachPatrol_aspect_warrior, DetachBatallion_aspect_warrior, DetachBrigade_aspect_warrior, DetachVanguard_aspect_warrior, DetachSpearhead_aspect_warrior, DetachOutrider_aspect_warrior, DetachCommand_aspect_warrior, DetachAirWing_aspect_warrior, DetachPatrol_warhost, DetachBatallion_warhost, DetachBrigade_warhost, DetachVanguard_warhost, DetachSpearhead_warhost, DetachOutrider_warhost, DetachCommand_warhost]:
            self.det.build_detach(det, 'ASURYANI', group=det.faction_base)


class Slaanesh8ed(Wh40k8edBase):
    army_name = 'Army of Slaanesh'
    faction_base = 'SLAANESH'
    alternate_factions = []
    army_id = 'slaanesh'

    def add_detachments(self):
        for det in [DetachPatrol_slaanesh, DetachBatallion_slaanesh, DetachBrigade_slaanesh, DetachVanguard_slaanesh, DetachSpearhead_slaanesh, DetachOutrider_slaanesh, DetachCommand_slaanesh, DetachAirWing_slaanesh, DetachAuxilary_slaanesh, DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachAirWing_heretic_astartes, DetachAuxilary_heretic_astartes, DetachPatrol__legion_, DetachBatallion__legion_, DetachBrigade__legion_, DetachVanguard__legion_, DetachSpearhead__legion_, DetachOutrider__legion_, DetachCommand__legion_, DetachAirWing__legion_, DetachAuxilary__legion_, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachAirWing_chaos, DetachPatrol_daemon, DetachBatallion_daemon, DetachBrigade_daemon, DetachVanguard_daemon, DetachSpearhead_daemon, DetachOutrider_daemon, DetachCommand_daemon]:
            self.det.build_detach(det, 'SLAANESH', group=det.faction_base)


class Skitarii8ed(Wh40k8edBase):
    army_name = 'Army of Skitarii'
    faction_base = 'SKITARII'
    alternate_factions = []
    army_id = 'skitarii'

    def add_detachments(self):
        for det in [DetachAuxilary_skitarii]:
            self.det.build_detach(det, 'SKITARII', group=det.faction_base)


class Deathwing8ed(Wh40k8edBase):
    army_name = 'Army of Deathwing'
    faction_base = 'DEATHWING'
    alternate_factions = []
    army_id = 'deathwing'

    def add_detachments(self):
        for det in [DetachVanguard_deathwing, DetachCommand_deathwing, DetachAuxilary_deathwing, DetachSpearhead_deathwing, DetachVanguard_dark_angels, DetachCommand_dark_angels, DetachVanguard_imperium, DetachCommand_imperium, DetachVanguard_adeptus_astartes, DetachCommand_adeptus_astartes]:
            self.det.build_detach(det, 'DEATHWING', group=det.faction_base)


class Astra_telepathica8ed(Wh40k8edBase):
    army_name = 'Army of Astra Telepathica'
    faction_base = 'ASTRA TELEPATHICA'
    alternate_factions = []
    army_id = 'astra_telepathica'

    def add_detachments(self):
        for det in [DetachVanguard_astra_telepathica, DetachCommand_astra_telepathica, DetachAuxilary_astra_telepathica, DetachVanguard_astra_militarum, DetachCommand_astra_militarum, DetachAuxilary_astra_militarum, DetachVanguard_scholastica_psykana, DetachCommand_scholastica_psykana, DetachVanguard_imperium, DetachCommand_imperium, SilenceVanguard]:
            self.det.build_detach(det, 'ASTRA TELEPATHICA', group=det.faction_base)


class Incubi8ed(Wh40k8edBase):
    army_name = 'Army of Incubi'
    faction_base = 'INCUBI'
    alternate_factions = []
    army_id = 'incubi'

    def add_detachments(self):
        for det in [DetachVanguard_incubi, DetachAuxilary_incubi, DetachVanguard_drukhari, DetachCommand_drukhari, DetachVanguard_aeldari, DetachCommand_aeldari]:
            self.det.build_detach(det, 'INCUBI', group=det.faction_base)


class Astra_militarum8ed(Wh40k8edBase):
    army_name = 'Army of Astra Militarum'
    faction_base = 'ASTRA MILITARUM'
    alternate_factions = []
    army_id = 'astra_militarum'

    def add_detachments(self):
        for det in [DetachVanguard_astra_telepathica, DetachCommand_astra_telepathica, DetachAuxilary_astra_telepathica, DetachPatrol_astra_militarum, DetachBatallion_astra_militarum, DetachBrigade_astra_militarum, DetachVanguard_astra_militarum, DetachSpearhead_astra_militarum, DetachOutrider_astra_militarum, DetachCommand_astra_militarum, DetachSuperHeavy_astra_militarum, DetachSuperHeavyAux_astra_militarum, DetachAirWing_astra_militarum, DetachAuxilary_astra_militarum, DetachPatrol__regiment_, DetachBatallion__regiment_, DetachBrigade__regiment_, DetachVanguard__regiment_, DetachSpearhead__regiment_, DetachOutrider__regiment_, DetachCommand__regiment_, DetachSuperHeavy__regiment_, DetachSuperHeavyAux__regiment_, DetachVanguard_officio_prefectus, DetachCommand_officio_prefectus, DetachVanguard_scholastica_psykana, DetachCommand_scholastica_psykana, DetachVanguard__forge_world_, DetachCommand__forge_world_, DetachVanguard_adeptus_ministorum, DetachCommand_adeptus_ministorum, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachSuperHeavy_imperium, DetachSuperHeavyAux_imperium, DetachAirWing_imperium, DetachAirWing_aeronautica_imperialis, DetachPatrol_militarum_tempestus, DetachBatallion_militarum_tempestus, DetachCommand_militarum_tempestus, DetachVanguard_cult_mechanicus, DetachCommand_cult_mechanicus, DetachVanguard_adeptus_mechanicus, DetachCommand_adeptus_mechanicus]:
            self.det.build_detach(det, 'ASTRA MILITARUM', group=det.faction_base)


class _regiment_8ed(Wh40k8edBase):
    army_name = 'Army of <Regiment>'
    faction_base = '<REGIMENT>'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = '_regiment_'

    def add_detachments(self):
        for det in [DetachPatrol_astra_militarum, DetachBatallion_astra_militarum, DetachBrigade_astra_militarum, DetachVanguard_astra_militarum, DetachSpearhead_astra_militarum, DetachOutrider_astra_militarum, DetachCommand_astra_militarum, DetachSuperHeavy_astra_militarum, DetachSuperHeavyAux_astra_militarum, DetachAuxilary_astra_militarum, DetachPatrol__regiment_, DetachBatallion__regiment_, DetachBrigade__regiment_, DetachVanguard__regiment_, DetachSpearhead__regiment_, DetachOutrider__regiment_, DetachCommand__regiment_, DetachSuperHeavy__regiment_, DetachSuperHeavyAux__regiment_, DetachAuxilary__regiment_, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachSuperHeavy_imperium, DetachSuperHeavyAux_imperium]:
            self.det.build_detach(det, '<REGIMENT>', group=det.faction_base)


class Legion_of_the_damned8ed(Wh40k8edBase):
    army_name = 'Army of Legion Of The Damned'
    faction_base = 'LEGION OF THE DAMNED'
    alternate_factions = []
    army_id = 'legion_of_the_damned'

    def add_detachments(self):
        for det in [DetachAuxilary_legion_of_the_damned, DamnedVanguard]:
            self.det.build_detach(det, 'LEGION OF THE DAMNED', group=det.faction_base)


class _mascue_8ed(Wh40k8edBase):
    army_name = 'Army of <Mascue>'
    faction_base = '<MASCUE>'
    alternate_factions = []
    army_id = '_mascue_'

    def add_detachments(self):
        for det in [DetachPatrol__mascue_, DetachBatallion__mascue_, DetachBrigade__mascue_, DetachVanguard__mascue_, DetachSpearhead__mascue_, DetachOutrider__mascue_, DetachCommand__mascue_, DetachAuxilary__mascue_, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachPatrol_harlequins, DetachBatallion_harlequins, DetachBrigade_harlequins, DetachVanguard_harlequins, DetachSpearhead_harlequins, DetachOutrider_harlequins, DetachCommand_harlequins, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari]:
            self.det.build_detach(det, '<MASCUE>', group=det.faction_base)


class Deathwatch8ed(Wh40k8edBase):
    army_name = 'Army of Deathwatch'
    faction_base = 'DEATHWATCH'
    alternate_factions = []
    army_id = 'deathwatch'

    def add_detachments(self):
        for det in [DetachPatrol_deathwatch, DetachBatallion_deathwatch, DetachBrigade_deathwatch, DetachVanguard_deathwatch, DetachSpearhead_deathwatch, DetachOutrider_deathwatch, DetachCommand_deathwatch, DetachSuperHeavy_deathwatch, DetachSuperHeavyAux_deathwatch, DetachAirWing_deathwatch, DetachAuxilary_deathwatch, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachAirWing_imperium, DetachPatrol_adeptus_astartes, DetachBatallion_adeptus_astartes, DetachBrigade_adeptus_astartes, DetachVanguard_adeptus_astartes, DetachSpearhead_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachAirWing_adeptus_astartes]:
            self.det.build_detach(det, 'DEATHWATCH', group=det.faction_base)


class Officio_prefectus8ed(Wh40k8edBase):
    army_name = 'Army of Officio Prefectus'
    faction_base = 'OFFICIO PREFECTUS'
    alternate_factions = []
    army_id = 'officio_prefectus'

    def add_detachments(self):
        for det in [DetachVanguard_astra_militarum, DetachCommand_astra_militarum, DetachAuxilary_astra_militarum, DetachVanguard_officio_prefectus, DetachCommand_officio_prefectus, DetachAuxilary_officio_prefectus, DetachVanguard_imperium, DetachCommand_imperium]:
            self.det.build_detach(det, 'OFFICIO PREFECTUS', group=det.faction_base)


class Thousand_sons8ed(Wh40k8edBase):
    army_name = 'Army of Thousand Sons'
    faction_base = 'THOUSAND SONS'
    alternate_factions = []
    army_id = 'thousand_sons'

    def add_detachments(self):
        for det in [DetachPatrol_tzeentch, DetachBatallion_tzeentch, DetachBrigade_tzeentch, DetachVanguard_tzeentch, DetachSpearhead_tzeentch, DetachOutrider_tzeentch, DetachCommand_tzeentch, DetachSuperHeavyAux_tzeentch, DetachAirWing_tzeentch, DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachSuperHeavy_heretic_astartes, DetachSuperHeavyAux_heretic_astartes, DetachAirWing_heretic_astartes, DetachAuxilary_heretic_astartes, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos, DetachAirWing_chaos, DetachPatrol_thousand_sons, DetachBatallion_thousand_sons, DetachBrigade_thousand_sons, DetachVanguard_thousand_sons, DetachSpearhead_thousand_sons, DetachOutrider_thousand_sons, DetachCommand_thousand_sons, DetachSuperHeavy_thousand_sons, DetachSuperHeavyAux_thousand_sons, DetachAirWing_thousand_sons, DetachAuxilary_thousand_sons]:
            self.det.build_detach(det, 'THOUSAND SONS', group=det.faction_base)


class Space_wolves8ed(Wh40k8edBase):
    army_name = 'Army of Space Wolves'
    faction_base = 'SPACE WOLVES'
    alternate_factions = []
    army_id = 'space_wolves'

    def add_detachments(self):
        for det in [DetachPatrol_space_wolves, DetachBatallion_space_wolves, DetachBrigade_space_wolves, DetachVanguard_space_wolves, DetachSpearhead_space_wolves, DetachOutrider_space_wolves, DetachCommand_space_wolves, DetachSuperHeavy_space_wolves, DetachSuperHeavyAux_space_wolves, DetachAirWing_space_wolves, DetachAuxilary_space_wolves, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachAirWing_imperium, DetachPatrol_adeptus_astartes, DetachBatallion_adeptus_astartes, DetachBrigade_adeptus_astartes, DetachVanguard_adeptus_astartes, DetachSpearhead_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachAirWing_adeptus_astartes]:
            self.det.build_detach(det, 'SPACE WOLVES', group=det.faction_base)


class Tzeentch8ed(Wh40k8edBase):
    army_name = 'Army of Tzeentch'
    faction_base = 'TZEENTCH'
    alternate_factions = []
    army_id = 'tzeentch'

    def add_detachments(self):
        for det in [DetachPatrol_tzeentch, DetachBatallion_tzeentch, DetachBrigade_tzeentch, DetachVanguard_tzeentch, DetachSpearhead_tzeentch, DetachOutrider_tzeentch, DetachCommand_tzeentch, DetachSuperHeavyAux_tzeentch, DetachAirWing_tzeentch, DetachAuxilary_tzeentch, DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachSuperHeavy_heretic_astartes, DetachSuperHeavyAux_heretic_astartes, DetachAirWing_heretic_astartes, DetachAuxilary_heretic_astartes, DetachPatrol__legion_, DetachBatallion__legion_, DetachBrigade__legion_, DetachVanguard__legion_, DetachSpearhead__legion_, DetachOutrider__legion_, DetachCommand__legion_, DetachSuperHeavy__legion_, DetachSuperHeavyAux__legion_, DetachAirWing__legion_, DetachAuxilary__legion_, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos, DetachAirWing_chaos, DetachPatrol_daemon, DetachBatallion_daemon, DetachBrigade_daemon, DetachVanguard_daemon, DetachSpearhead_daemon, DetachOutrider_daemon, DetachCommand_daemon, DetachPatrol_thousand_sons, DetachBatallion_thousand_sons, DetachBrigade_thousand_sons, DetachVanguard_thousand_sons, DetachSpearhead_thousand_sons, DetachOutrider_thousand_sons, DetachCommand_thousand_sons, DetachSuperHeavy_thousand_sons, DetachSuperHeavyAux_thousand_sons, DetachAirWing_thousand_sons]:
            self.det.build_detach(det, 'TZEENTCH', group=det.faction_base)


class Drukhari8ed(Wh40k8edBase):
    army_name = 'Army of Drukhari'
    faction_base = 'DRUKHARI'
    alternate_factions = []
    army_id = 'drukhari'

    def add_detachments(self):
        for det in [DetachVanguard_incubi, DetachAuxilary_incubi, DetachPatrol_drukhari, DetachBatallion_drukhari, DetachBrigade_drukhari, DetachVanguard_drukhari, DetachSpearhead_drukhari, DetachOutrider_drukhari, DetachCommand_drukhari, DetachAirWing_drukhari, DetachAuxilary_drukhari, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachAirWing_aeldari, DetachPatrol__kabal_, DetachBatallion__kabal_, DetachVanguard__kabal_, DetachSpearhead__kabal_, DetachCommand__kabal_, DetachAirWing__kabal_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari, DetachAirWing_ynnari, DetachPatrol__haemunculus_coven_, DetachBatallion__haemunculus_coven_, DetachVanguard__haemunculus_coven_, DetachSpearhead__haemunculus_coven_, DetachCommand__haemunculus_coven_, DetachPatrol__wych_cult_, DetachBatallion__wych_cult_, DetachVanguard__wych_cult_, DetachOutrider__wych_cult_, DetachCommand__wych_cult_, DetachAirWing__wych_cult_]:
            self.det.build_detach(det, 'DRUKHARI', group=det.faction_base)

    def build_statistics(self):
        res = super(Drukhari8ed, self).build_statistics()
        cp = res.pop('Command points', 0)
        cp += get_drukhari_patrol_cp(itertools.chain(*[[unit.sub_roster.roster for unit in sec.units] for sec in self.sections]))
        res['Command points'] = cp
        return res


class Heretic_astartes8ed(Wh40k8edBase):
    army_name = 'Army of Heretic Astartes'
    faction_base = 'HERETIC ASTARTES'
    alternate_factions = []
    army_id = 'heretic_astartes'

    def add_detachments(self):
        for det in [DetachPatrol_slaanesh, DetachBatallion_slaanesh, DetachBrigade_slaanesh, DetachVanguard_slaanesh, DetachSpearhead_slaanesh, DetachOutrider_slaanesh, DetachCommand_slaanesh, DetachAirWing_slaanesh, DetachAuxilary_slaanesh, DetachPatrol_tzeentch, DetachBatallion_tzeentch, DetachBrigade_tzeentch, DetachVanguard_tzeentch, DetachSpearhead_tzeentch, DetachOutrider_tzeentch, DetachCommand_tzeentch, DetachSuperHeavyAux_tzeentch, DetachAirWing_tzeentch, DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachSuperHeavy_heretic_astartes, DetachSuperHeavyAux_heretic_astartes, DetachAirWing_heretic_astartes, DetachAuxilary_heretic_astartes, DetachPatrol__legion_, DetachBatallion__legion_, DetachBrigade__legion_, DetachVanguard__legion_, DetachSpearhead__legion_, DetachOutrider__legion_, DetachCommand__legion_, DetachSuperHeavy__legion_, DetachSuperHeavyAux__legion_, DetachAirWing__legion_, DetachAuxilary__legion_, DetachPatrol_khorne, DetachBatallion_khorne, DetachBrigade_khorne, DetachVanguard_khorne, DetachSpearhead_khorne, DetachOutrider_khorne, DetachCommand_khorne, DetachSuperHeavy_khorne, DetachSuperHeavyAux_khorne, DetachAirWing_khorne, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos, DetachAirWing_chaos, DetachPatrol_death_guard, DetachBatallion_death_guard, DetachBrigade_death_guard, DetachVanguard_death_guard, DetachSpearhead_death_guard, DetachOutrider_death_guard, DetachCommand_death_guard, DetachSuperHeavyAux_death_guard, DetachPatrol_nurgle, DetachBatallion_nurgle, DetachBrigade_nurgle, DetachVanguard_nurgle, DetachSpearhead_nurgle, DetachOutrider_nurgle, DetachCommand_nurgle, DetachSuperHeavyAux_nurgle, DetachAirWing_nurgle, DetachPatrol_thousand_sons, DetachBatallion_thousand_sons, DetachBrigade_thousand_sons, DetachVanguard_thousand_sons, DetachSpearhead_thousand_sons, DetachOutrider_thousand_sons, DetachCommand_thousand_sons, DetachSuperHeavy_thousand_sons, DetachSuperHeavyAux_thousand_sons, DetachAirWing_thousand_sons]:
            self.det.build_detach(det, 'HERETIC ASTARTES', group=det.faction_base)


class _legion_8ed(Wh40k8edBase):
    army_name = 'Army of <Legion>'
    faction_base = '<LEGION>'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN"]
    army_id = '_legion_'

    def add_detachments(self):
        for det in [DetachPatrol_slaanesh, DetachBatallion_slaanesh, DetachBrigade_slaanesh, DetachVanguard_slaanesh, DetachSpearhead_slaanesh, DetachOutrider_slaanesh, DetachCommand_slaanesh, DetachAirWing_slaanesh, DetachAuxilary_slaanesh, DetachPatrol_tzeentch, DetachBatallion_tzeentch, DetachBrigade_tzeentch, DetachVanguard_tzeentch, DetachSpearhead_tzeentch, DetachOutrider_tzeentch, DetachCommand_tzeentch, DetachAirWing_tzeentch, DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachSuperHeavy_heretic_astartes, DetachSuperHeavyAux_heretic_astartes, DetachAirWing_heretic_astartes, DetachAuxilary_heretic_astartes, DetachPatrol__legion_, DetachBatallion__legion_, DetachBrigade__legion_, DetachVanguard__legion_, DetachSpearhead__legion_, DetachOutrider__legion_, DetachCommand__legion_, DetachSuperHeavy__legion_, DetachSuperHeavyAux__legion_, DetachAirWing__legion_, DetachAuxilary__legion_, DetachPatrol_khorne, DetachBatallion_khorne, DetachBrigade_khorne, DetachVanguard_khorne, DetachSpearhead_khorne, DetachOutrider_khorne, DetachCommand_khorne, DetachSuperHeavy_khorne, DetachSuperHeavyAux_khorne, DetachAirWing_khorne, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos, DetachAirWing_chaos, DetachPatrol_nurgle, DetachBatallion_nurgle, DetachBrigade_nurgle, DetachVanguard_nurgle, DetachSpearhead_nurgle, DetachOutrider_nurgle, DetachCommand_nurgle, DetachAirWing_nurgle]:
            self.det.build_detach(det, '<LEGION>', group=det.faction_base)


class Dark_angels8ed(Wh40k8edBase):
    army_name = 'Army of Dark Angels'
    faction_base = 'DARK ANGELS'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'dark_angels'

    def add_detachments(self):
        for det in [DetachVanguard_ravenwing, DetachOutrider_ravenwing, DetachCommand_ravenwing, DetachAirWing_ravenwing, DetachAuxilary_ravenwing, DetachVanguard_deathwing, DetachCommand_deathwing, DetachAuxilary_deathwing, DetachSpearhead_deathwing, DetachPatrol_dark_angels, DetachBatallion_dark_angels, DetachBrigade_dark_angels, DetachVanguard_dark_angels, DetachSpearhead_dark_angels, DetachOutrider_dark_angels, DetachCommand_dark_angels, DetachSuperHeavy_dark_angels, DetachSuperHeavyAux_dark_angels, DetachAirWing_dark_angels, DetachAuxilary_dark_angels, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachAirWing_imperium, DetachPatrol_adeptus_astartes, DetachBatallion_adeptus_astartes, DetachBrigade_adeptus_astartes, DetachVanguard_adeptus_astartes, DetachSpearhead_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachAirWing_adeptus_astartes]:
            self.det.build_detach(det, 'DARK ANGELS', group=det.faction_base)


class Khorne8ed(Wh40k8edBase):
    army_name = 'Army of Khorne'
    faction_base = 'KHORNE'
    alternate_factions = []
    army_id = 'khorne'

    def add_detachments(self):
        for det in [DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachSuperHeavy_heretic_astartes, DetachSuperHeavyAux_heretic_astartes, DetachAirWing_heretic_astartes, DetachAuxilary_heretic_astartes, DetachPatrol__legion_, DetachBatallion__legion_, DetachBrigade__legion_, DetachVanguard__legion_, DetachSpearhead__legion_, DetachOutrider__legion_, DetachCommand__legion_, DetachSuperHeavy__legion_, DetachSuperHeavyAux__legion_, DetachAirWing__legion_, DetachAuxilary__legion_, DetachPatrol_khorne, DetachBatallion_khorne, DetachBrigade_khorne, DetachVanguard_khorne, DetachSpearhead_khorne, DetachOutrider_khorne, DetachCommand_khorne, DetachSuperHeavy_khorne, DetachSuperHeavyAux_khorne, DetachAirWing_khorne, DetachAuxilary_khorne, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos, DetachAirWing_chaos, DetachPatrol_daemon, DetachBatallion_daemon, DetachBrigade_daemon, DetachVanguard_daemon, DetachSpearhead_daemon, DetachOutrider_daemon, DetachCommand_daemon]:
            self.det.build_detach(det, 'KHORNE', group=det.faction_base)


class Questor_traitoris8ed(Wh40k8edBase):
    army_name = 'Army of Chaos Knights'
    faction_base = 'CHAOS KNIGHTS'
    alternate_factions = []
    army_id = 'questor_traitoris'

    def add_detachments(self):
        for det in [DetachSuperHeavy_questor_traitoris, DetachSuperHeavyAux_questor_traitoris, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos]:
            self.det.build_detach(det, 'QUESTOR TRAITORIS', group=det.faction_base)


class Scholastica_psykana8ed(Wh40k8edBase):
    army_name = 'Army of Scholastica Psykana'
    faction_base = 'SCHOLASTICA PSYKANA'
    alternate_factions = []
    army_id = 'scholastica_psykana'

    def add_detachments(self):
        for det in [DetachVanguard_astra_telepathica, DetachCommand_astra_telepathica, DetachAuxilary_astra_telepathica, DetachVanguard_astra_militarum, DetachCommand_astra_militarum, DetachAuxilary_astra_militarum, DetachVanguard_scholastica_psykana, DetachCommand_scholastica_psykana, DetachAuxilary_scholastica_psykana, DetachVanguard_imperium, DetachCommand_imperium]:
            self.det.build_detach(det, 'SCHOLASTICA PSYKANA', group=det.faction_base)


class _forge_world_8ed(Wh40k8edBase):
    army_name = 'Army of <Forge World>'
    faction_base = '<FORGE WORLD>'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = '_forge_world_'

    def add_detachments(self):
        for det in [DetachVanguard_astra_militarum, DetachCommand_astra_militarum, DetachAuxilary_astra_militarum, DetachPatrol__forge_world_, DetachBatallion__forge_world_, DetachBrigade__forge_world_, DetachVanguard__forge_world_, DetachSpearhead__forge_world_, DetachOutrider__forge_world_, DetachCommand__forge_world_, DetachAuxilary__forge_world_, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachPatrol_cult_mechanicus, DetachBatallion_cult_mechanicus, DetachVanguard_cult_mechanicus, DetachSpearhead_cult_mechanicus, DetachCommand_cult_mechanicus, DetachPatrol_adeptus_mechanicus, DetachBatallion_adeptus_mechanicus, DetachBrigade_adeptus_mechanicus, DetachVanguard_adeptus_mechanicus, DetachSpearhead_adeptus_mechanicus, DetachOutrider_adeptus_mechanicus, DetachCommand_adeptus_mechanicus, DetachAuxilary_skitarii]:
            self.det.build_detach(det, '<FORGE WORLD>', group=det.faction_base)


class Necrons8ed(Wh40k8edBase):
    army_name = 'Army of Necrons'
    faction_base = 'NECRONS'
    alternate_factions = []
    army_id = 'necrons'

    def add_detachments(self):
        for det in [DetachPatrol_necrons, DetachBatallion_necrons, DetachBrigade_necrons, DetachVanguard_necrons, DetachSpearhead_necrons, DetachOutrider_necrons, DetachCommand_necrons, DetachSuperHeavy_necrons, DetachSuperHeavyAux_necrons, DetachAirWing_necrons, DetachAuxilary_necrons, DetachSuperHeavy_c_tan_shards, DetachSuperHeavyAux_c_tan_shards, DetachPatrol__dynasty_, DetachBatallion__dynasty_, DetachBrigade__dynasty_, DetachVanguard__dynasty_, DetachSpearhead__dynasty_, DetachOutrider__dynasty_, DetachCommand__dynasty_, DetachSuperHeavy__dynasty_, DetachSuperHeavyAux__dynasty_, DetachAirWing__dynasty_]:
            self.det.build_detach(det, 'NECRONS', group=det.faction_base)


class Fallen8ed(Wh40k8edBase):
    army_name = 'Army of Fallen'
    faction_base = 'FALLEN'
    alternate_factions = []
    army_id = 'fallen'

    def add_detachments(self):
        for det in [DetachVanguard_fallen, DetachCommand_fallen, DetachAuxilary_fallen, DetachVanguard_imperium, DetachCommand_imperium, DetachVanguard_chaos, DetachCommand_chaos]:
            self.det.build_detach(det, 'FALLEN', group=det.faction_base)


class Kroot8ed(Wh40k8edBase):
    army_name = 'Army of Kroot'
    faction_base = 'KROOT'
    alternate_factions = []
    army_id = 'kroot'

    def add_detachments(self):
        for det in [DetachAuxilary_kroot]:
            self.det.build_detach(det, 'KROOT', group=det.faction_base)


class Adeptus_ministorum8ed(Wh40k8edBase):
    army_name = 'Army of Adeptus Ministorum'
    faction_base = 'ADEPTUS MINISTORUM'
    alternate_factions = []
    army_id = 'adeptus_ministorum'

    def add_detachments(self):
        for det in [DetachPatrol_adeptus_ministorum, DetachBatallion_adeptus_ministorum, DetachBrigade_adeptus_ministorum, DetachVanguard_adeptus_ministorum, DetachSpearhead_adeptus_ministorum, DetachOutrider_adeptus_ministorum, DetachCommand_adeptus_ministorum, DetachAuxilary_adeptus_ministorum, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachPatrol_adepta_sororitas, DetachBatallion_adepta_sororitas, DetachBrigade_adepta_sororitas, DetachVanguard_adepta_sororitas, DetachSpearhead_adepta_sororitas, DetachOutrider_adepta_sororitas, DetachCommand_adepta_sororitas, DetachPatrol__order_, DetachBatallion__order_, DetachBrigade__order_, DetachVanguard__order_, DetachSpearhead__order_, DetachOutrider__order_, DetachCommand__order_, DetachAuxilary_astra_militarum]:
            self.det.build_detach(det, 'ADEPTUS MINISTORUM', group=det.faction_base)


class Imperium8ed(Wh40k8edBase):
    army_name = 'Army of Imperium'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'imperium'

    def add_detachments(self):
        for det in [DetachVanguard_ravenwing, DetachOutrider_ravenwing, DetachCommand_ravenwing, DetachAirWing_ravenwing, DetachAuxilary_ravenwing, DetachPatrol_blood_angels, DetachBatallion_blood_angels, DetachBrigade_blood_angels, DetachVanguard_blood_angels, DetachSpearhead_blood_angels, DetachOutrider_blood_angels, DetachCommand_blood_angels, DetachSuperHeavy_blood_angels, DetachSuperHeavyAux_blood_angels, DetachAirWing_blood_angels, DetachAuxilary_blood_angels, DetachVanguard_deathwing, DetachCommand_deathwing, DetachAuxilary_deathwing, DetachSpearhead_deathwing, DetachVanguard_astra_telepathica, DetachCommand_astra_telepathica, DetachAuxilary_astra_telepathica, DetachPatrol_astra_militarum, DetachBatallion_astra_militarum, DetachBrigade_astra_militarum, DetachVanguard_astra_militarum, DetachSpearhead_astra_militarum, DetachOutrider_astra_militarum, DetachCommand_astra_militarum, DetachSuperHeavy_astra_militarum, DetachSuperHeavyAux_astra_militarum, DetachAirWing_astra_militarum, DetachAuxilary_astra_militarum, DetachPatrol__regiment_, DetachBatallion__regiment_, DetachBrigade__regiment_, DetachVanguard__regiment_, DetachSpearhead__regiment_, DetachOutrider__regiment_, DetachCommand__regiment_, DetachSuperHeavy__regiment_, DetachSuperHeavyAux__regiment_, DetachPatrol_deathwatch, DetachBatallion_deathwatch, DetachBrigade_deathwatch, DetachVanguard_deathwatch, DetachSpearhead_deathwatch, DetachOutrider_deathwatch, DetachCommand_deathwatch, DetachSuperHeavy_deathwatch, DetachSuperHeavyAux_deathwatch, DetachAirWing_deathwatch, DetachVanguard_officio_prefectus, DetachCommand_officio_prefectus, DetachPatrol_space_wolves, DetachBatallion_space_wolves, DetachBrigade_space_wolves, DetachVanguard_space_wolves, DetachSpearhead_space_wolves, DetachOutrider_space_wolves, DetachCommand_space_wolves, DetachSuperHeavy_space_wolves, DetachSuperHeavyAux_space_wolves, DetachAirWing_space_wolves, DetachAuxilary_space_wolves, DetachPatrol_dark_angels, DetachBatallion_dark_angels, DetachBrigade_dark_angels, DetachVanguard_dark_angels, DetachSpearhead_dark_angels, DetachOutrider_dark_angels, DetachCommand_dark_angels, DetachSuperHeavy_dark_angels, DetachSuperHeavyAux_dark_angels, DetachAirWing_dark_angels, DetachAuxilary_dark_angels, DetachVanguard_scholastica_psykana, DetachCommand_scholastica_psykana, DetachPatrol__forge_world_, DetachBatallion__forge_world_, DetachBrigade__forge_world_, DetachVanguard__forge_world_, DetachSpearhead__forge_world_, DetachOutrider__forge_world_, DetachCommand__forge_world_, DetachVanguard_fallen, DetachCommand_fallen, DetachPatrol_adeptus_ministorum, DetachBatallion_adeptus_ministorum, DetachBrigade_adeptus_ministorum, DetachVanguard_adeptus_ministorum, DetachSpearhead_adeptus_ministorum, DetachOutrider_adeptus_ministorum, DetachCommand_adeptus_ministorum, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachSuperHeavy_imperium, DetachSuperHeavyAux_imperium, DetachAirWing_imperium, DetachAuxilary_imperium, DetachSuperHeavy_questor_imperialis, DetachSuperHeavyAux_questor_imperialis, DetachAirWing_aeronautica_imperialis, DetachVanguard_chaos, DetachCommand_chaos, DetachVanguard_inquisition, DetachCommand_inquisition, DetachPatrol_adepta_sororitas, DetachBatallion_adepta_sororitas, DetachBrigade_adepta_sororitas, DetachVanguard_adepta_sororitas, DetachSpearhead_adepta_sororitas, DetachOutrider_adepta_sororitas, DetachCommand_adepta_sororitas, DetachPatrol__chapter_, DetachBatallion__chapter_, DetachBrigade__chapter_, DetachVanguard__chapter_, DetachSpearhead__chapter_, DetachOutrider__chapter_, DetachCommand__chapter_, DetachSuperHeavy__chapter_, DetachSuperHeavyAux__chapter_, DetachAirWing__chapter_, DetachAuxilary__chapter_, DetachSuperHeavy_questor_mechanicus, DetachSuperHeavyAux_questor_mechanicus, DetachPatrol_militarum_tempestus, DetachBatallion_militarum_tempestus, DetachCommand_militarum_tempestus, DetachVanguard__ordo_, DetachCommand__ordo_, DetachPatrol_adeptus_astartes, DetachBatallion_adeptus_astartes, DetachBrigade_adeptus_astartes, DetachVanguard_adeptus_astartes, DetachSpearhead_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachSuperHeavy_adeptus_astartes, DetachSuperHeavyAux_adeptus_astartes, DetachAirWing_adeptus_astartes, DetachPatrol_cult_mechanicus, DetachBatallion_cult_mechanicus, DetachVanguard_cult_mechanicus, DetachSpearhead_cult_mechanicus, DetachCommand_cult_mechanicus, DetachSuperHeavy__household_, DetachSuperHeavyAux__household_, DetachVanguard_death_company, DetachCommand_death_company, DetachPatrol__order_, DetachBatallion__order_, DetachBrigade__order_, DetachVanguard__order_, DetachSpearhead__order_, DetachOutrider__order_, DetachCommand__order_, DetachPatrol_adeptus_custodes, DetachBatallion_adeptus_custodes, DetachBrigade_adeptus_custodes, DetachVanguard_adeptus_custodes, DetachSpearhead_adeptus_custodes, DetachOutrider_adeptus_custodes, DetachCommand_adeptus_custodes, DetachPatrol_grey_knights, DetachBatallion_grey_knights, DetachBrigade_grey_knights, DetachVanguard_grey_knights, DetachSpearhead_grey_knights, DetachOutrider_grey_knights, DetachCommand_grey_knights, DetachAirWing_grey_knights, DetachPatrol_adeptus_mechanicus, DetachBatallion_adeptus_mechanicus, DetachBrigade_adeptus_mechanicus, DetachVanguard_adeptus_mechanicus, DetachSpearhead_adeptus_mechanicus, DetachOutrider_adeptus_mechanicus, DetachCommand_adeptus_mechanicus, DamnedVanguard, SilenceVanguard, AssassinVanguard, DetachSuperHeavy_imperial_knights, DetachSuperHeavyAux_imperial_knights, DetachFort_imperial_knights, DetachAuxilary_skitarii, DetachAuxilary_officio_assassinorum] + starstriders_detachments:
            self.det.build_detach(det, 'IMPERIUM', group=det.faction_base)


class Questor_imperialis8ed(Wh40k8edBase):
    army_name = 'Army of Questor Imperialis'
    faction_base = 'QUESTOR IMPERIALIS'
    alternate_factions = []
    army_id = 'questor_imperialis'
    obsolete = True

    def add_detachments(self):
        for det in [DetachSuperHeavy_imperium, DetachSuperHeavyAux_imperium, DetachSuperHeavy_questor_imperialis, DetachSuperHeavyAux_questor_imperialis, DetachSuperHeavy__household_, DetachSuperHeavyAux__household_]:
            self.det.build_detach(det, 'QUESTOR IMPERIALIS', group=det.faction_base)


class Elucidian_starstriders_8ed(Wh40k8edBase):
    army_name = 'Army of Elucidian Starstriders'
    faction_base = 'ELUCIDIAN STARSTRIDERS'
    alternate_factions = []
    army_id = 'elucidian_starstriders'

    def add_detachments(self):
        for det in starstriders_detachments:
            self.det.build_detach(det, 'ELUCIDIAN STARSTRIDERS', group=det.faction_base)


class Aeronautica_imperialis8ed(Wh40k8edBase):
    army_name = 'Army of Aeronautica Imperialis'
    faction_base = 'AERONAUTICA IMPERIALIS'
    alternate_factions = []
    army_id = 'aeronautica_imperialis'

    def add_detachments(self):
        for det in [DetachAirWing_astra_militarum, DetachAirWing_imperium, DetachAirWing_aeronautica_imperialis, DetachAuxilary_aeronautica_imperialis, DetachAuxilary_astra_militarum]:
            self.det.build_detach(det, 'AERONAUTICA IMPERIALIS', group=det.faction_base)


class Chaos8ed(Wh40k8edBase):
    army_name = 'Army of Chaos'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'chaos'

    def add_detachments(self):
        for det in [DetachPatrol_slaanesh, DetachBatallion_slaanesh, DetachBrigade_slaanesh, DetachVanguard_slaanesh, DetachSpearhead_slaanesh, DetachOutrider_slaanesh, DetachCommand_slaanesh, DetachAirWing_slaanesh, DetachAuxilary_slaanesh, DetachPatrol_tzeentch, DetachBatallion_tzeentch, DetachBrigade_tzeentch, DetachVanguard_tzeentch, DetachSpearhead_tzeentch, DetachOutrider_tzeentch, DetachCommand_tzeentch, DetachSuperHeavyAux_tzeentch, DetachAirWing_tzeentch, DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachSuperHeavy_heretic_astartes, DetachSuperHeavyAux_heretic_astartes, DetachAirWing_heretic_astartes, DetachAuxilary_heretic_astartes, DetachPatrol__legion_, DetachBatallion__legion_, DetachBrigade__legion_, DetachVanguard__legion_, DetachSpearhead__legion_, DetachOutrider__legion_, DetachCommand__legion_, DetachSuperHeavy__legion_, DetachSuperHeavyAux__legion_, DetachAirWing__legion_, DetachAuxilary__legion_, DetachPatrol_khorne, DetachBatallion_khorne, DetachBrigade_khorne, DetachVanguard_khorne, DetachSpearhead_khorne, DetachOutrider_khorne, DetachCommand_khorne, DetachSuperHeavy_khorne, DetachSuperHeavyAux_khorne, DetachAirWing_khorne, DetachSuperHeavy_questor_traitoris, DetachSuperHeavyAux_questor_traitoris, DetachVanguard_fallen, DetachCommand_fallen, DetachVanguard_imperium, DetachCommand_imperium, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos, DetachAirWing_chaos, DetachFort_chaos, DetachAuxilary_chaos, DetachPatrol_death_guard, DetachBatallion_death_guard, DetachBrigade_death_guard, DetachVanguard_death_guard, DetachSpearhead_death_guard, DetachOutrider_death_guard, DetachCommand_death_guard, DetachSuperHeavyAux_death_guard, DetachPatrol_nurgle, DetachBatallion_nurgle, DetachBrigade_nurgle, DetachVanguard_nurgle, DetachSpearhead_nurgle, DetachOutrider_nurgle, DetachCommand_nurgle, DetachSuperHeavyAux_nurgle, DetachAirWing_nurgle, DetachPatrol_daemon, DetachBatallion_daemon, DetachBrigade_daemon, DetachVanguard_daemon, DetachSpearhead_daemon, DetachOutrider_daemon, DetachCommand_daemon, DetachFort_daemon, DetachPatrol_thousand_sons, DetachBatallion_thousand_sons, DetachBrigade_thousand_sons, DetachVanguard_thousand_sons, DetachSpearhead_thousand_sons, DetachOutrider_thousand_sons, DetachCommand_thousand_sons, DetachSuperHeavy_thousand_sons, DetachSuperHeavyAux_thousand_sons, DetachAirWing_thousand_sons] + infected_detachments:
            self.det.build_detach(det, 'CHAOS', group=det.faction_base)


class Gellerpox_infected_8ed(Wh40k8edBase):
    army_name = 'Army of Gellerpox infested'
    faction_base = 'GELLERPOX INFECTED'
    alternate_factions = []
    army_id = 'gellerpox_infesterd'

    def add_detachments(self):
        for det in infected_detachments:
            self.det.build_detach(det, 'GELLERPOX_INFECTED', group=det.faction_base)


class C_tan_shards8ed(Wh40k8edBase):
    army_name = "Army of C'tan Shards"
    faction_base = "C'TAN SHARDS"
    alternate_factions = []
    army_id = 'c_tan_shards'

    def add_detachments(self):
        for det in [DetachSuperHeavy_necrons, DetachSuperHeavyAux_necrons, DetachSuperHeavy_c_tan_shards, DetachSuperHeavyAux_c_tan_shards, DetachAuxilary_c_tan_shards, DetachSuperHeavy__dynasty_, DetachSuperHeavyAux__dynasty_]:
            self.det.build_detach(det, "C'TAN SHARDS", group=det.faction_base)


class Inquisition8ed(Wh40k8edBase):
    army_name = 'Army of Inquisition'
    faction_base = 'INQUISITION'
    alternate_factions = []
    army_id = 'inquisition'

    def add_detachments(self):
        for det in [DetachVanguard_imperium, DetachCommand_imperium, DetachVanguard_inquisition, DetachCommand_inquisition, DetachAuxilary_inquisition, DetachVanguard__ordo_, DetachCommand__ordo_]:
            self.det.build_detach(det, 'INQUISITION', group=det.faction_base)


class Tyranids8ed(Wh40k8edBase):
    army_name = 'Army of Tyranids'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'tyranids'

    def add_detachments(self):
        for det in [DetachPatrol_brood_brothers, DetachBatallion_brood_brothers, DetachBrigade_brood_brothers, DetachVanguard_brood_brothers, DetachSpearhead_brood_brothers, DetachOutrider_brood_brothers, DetachCommand_brood_brothers, DetachSuperHeavy_brood_brothers, DetachSuperHeavyAux_brood_brothers, DetachAirWing_brood_brothers, DetachPatrol_tyranids, DetachBatallion_tyranids, DetachBrigade_tyranids, DetachVanguard_tyranids, DetachSpearhead_tyranids, DetachOutrider_tyranids, DetachCommand_tyranids, DetachAirWing_tyranids, DetachFort_tyranids, DetachAuxilary_tyranids, DetachPatrol__hive_fleet_, DetachBatallion__hive_fleet_, DetachBrigade__hive_fleet_, DetachVanguard__hive_fleet_, DetachSpearhead__hive_fleet_, DetachOutrider__hive_fleet_, DetachCommand__hive_fleet_, DetachAirWing__hive_fleet_, DetachFort__hive_fleet_, DetachPatrol_genestealer_cults, DetachBatallion_genestealer_cults, DetachBrigade_genestealer_cults, DetachVanguard_genestealer_cults, DetachSpearhead_genestealer_cults, DetachOutrider_genestealer_cults, DetachCommand_genestealer_cults, DetachAuxilary_genestealer_cults]:
            self.det.build_detach(det, 'TYRANIDS', group=det.faction_base)


class T_au_empire8ed(Wh40k8edBase):
    army_name = "Army of T'au Empire"
    faction_base = "T'AU EMPIRE"
    alternate_factions = []
    army_id = 't_au_empire'

    def add_detachments(self):
        for det in [DetachPatrol__sept_, DetachBatallion__sept_, DetachBrigade__sept_, DetachVanguard__sept_, DetachSpearhead__sept_, DetachOutrider__sept_, DetachCommand__sept_, DetachSuperHeavy__sept_, DetachSuperHeavyAux__sept_, DetachAirWing__sept_, DetachFort__sept_, DetachAuxilary__sept_, DetachPatrol_t_au_empire, DetachBatallion_t_au_empire, DetachBrigade_t_au_empire, DetachVanguard_t_au_empire, DetachSpearhead_t_au_empire, DetachOutrider_t_au_empire, DetachCommand_t_au_empire, DetachSuperHeavy_t_au_empire, DetachSuperHeavyAux_t_au_empire, DetachAirWing_t_au_empire, DetachFort_t_au_empire, DetachAuxilary_t_au_empire]:
            self.det.build_detach(det, "T'AU EMPIRE", group=det.faction_base)


class Adepta_sororitas8ed(Wh40k8edBase):
    army_name = 'Army of Adepta Sororitas'
    faction_base = 'ADEPTA SORORITAS'
    alternate_factions = []
    army_id = 'adepta_sororitas'

    def add_detachments(self):
        for det in [DetachPatrol_adeptus_ministorum, DetachBatallion_adeptus_ministorum, DetachBrigade_adeptus_ministorum, DetachVanguard_adeptus_ministorum, DetachSpearhead_adeptus_ministorum, DetachOutrider_adeptus_ministorum, DetachCommand_adeptus_ministorum, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachPatrol_adepta_sororitas, DetachBatallion_adepta_sororitas, DetachBrigade_adepta_sororitas, DetachVanguard_adepta_sororitas, DetachSpearhead_adepta_sororitas, DetachOutrider_adepta_sororitas, DetachCommand_adepta_sororitas, DetachAuxilary_adepta_sororitas, DetachPatrol__order_, DetachBatallion__order_, DetachBrigade__order_, DetachVanguard__order_, DetachSpearhead__order_, DetachOutrider__order_, DetachCommand__order_]:
            self.det.build_detach(det, 'ADEPTA SORORITAS', group=det.faction_base)


class _dynasty_8ed(Wh40k8edBase):
    army_name = 'Army of <Dynasty>'
    faction_base = '<DYNASTY>'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH', 'MAYNARKH']
    army_id = '_dynasty_'

    def add_detachments(self):
        for det in [DetachPatrol_necrons, DetachBatallion_necrons, DetachBrigade_necrons, DetachVanguard_necrons, DetachSpearhead_necrons, DetachOutrider_necrons, DetachCommand_necrons, DetachSuperHeavy_necrons, DetachSuperHeavyAux_necrons, DetachAirWing_necrons, DetachSuperHeavy_c_tan_shards, DetachSuperHeavyAux_c_tan_shards, DetachPatrol__dynasty_, DetachBatallion__dynasty_, DetachBrigade__dynasty_, DetachVanguard__dynasty_, DetachSpearhead__dynasty_, DetachOutrider__dynasty_, DetachCommand__dynasty_, DetachSuperHeavy__dynasty_, DetachSuperHeavyAux__dynasty_, DetachAirWing__dynasty_, DetachAuxilary__dynasty_]:
            self.det.build_detach(det, '<DYNASTY>', group=det.faction_base)


class Aeldari8ed(Wh40k8edBase):
    army_name = 'Army of Aeldari'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'aeldari'

    def add_detachments(self):
        for det in [DetachPatrol_asuryani, DetachBatallion_asuryani, DetachBrigade_asuryani, DetachVanguard_asuryani, DetachSpearhead_asuryani, DetachOutrider_asuryani, DetachCommand_asuryani, DetachSuperHeavy_asuryani, DetachSuperHeavyAux_asuryani, DetachAirWing_asuryani, DetachAuxilary_asuryani, DetachVanguard_incubi, DetachAuxilary_incubi, DetachPatrol__mascue_, DetachBatallion__mascue_, DetachBrigade__mascue_, DetachVanguard__mascue_, DetachSpearhead__mascue_, DetachOutrider__mascue_, DetachCommand__mascue_, DetachPatrol_drukhari, DetachBatallion_drukhari, DetachBrigade_drukhari, DetachVanguard_drukhari, DetachSpearhead_drukhari, DetachOutrider_drukhari, DetachCommand_drukhari, DetachAirWing_drukhari, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachSuperHeavy_aeldari, DetachSuperHeavyAux_aeldari, DetachAirWing_aeldari, DetachAuxilary_aeldari, DetachPatrol_harlequins, DetachBatallion_harlequins, DetachBrigade_harlequins, DetachVanguard_harlequins, DetachSpearhead_harlequins, DetachOutrider_harlequins, DetachCommand_harlequins, DetachPatrol__craftworld_, DetachBatallion__craftworld_, DetachBrigade__craftworld_, DetachVanguard__craftworld_, DetachSpearhead__craftworld_, DetachOutrider__craftworld_, DetachCommand__craftworld_, DetachSuperHeavy__craftworld_, DetachSuperHeavyAux__craftworld_, DetachAirWing__craftworld_, DetachPatrol__kabal_, DetachBatallion__kabal_, DetachVanguard__kabal_, DetachSpearhead__kabal_, DetachCommand__kabal_, DetachAirWing__kabal_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari, DetachSuperHeavy_ynnari, DetachSuperHeavyAux_ynnari, DetachAirWing_ynnari, DetachPatrol__haemunculus_coven_, DetachBatallion__haemunculus_coven_, DetachVanguard__haemunculus_coven_, DetachSpearhead__haemunculus_coven_, DetachCommand__haemunculus_coven_, DetachVanguard_spirit_host, DetachSpearhead_spirit_host, DetachCommand_spirit_host, DetachSuperHeavy_spirit_host, DetachSuperHeavyAux_spirit_host, DetachAirWing_spirit_host, DetachPatrol_aspect_warrior, DetachBatallion_aspect_warrior, DetachBrigade_aspect_warrior, DetachVanguard_aspect_warrior, DetachSpearhead_aspect_warrior, DetachOutrider_aspect_warrior, DetachCommand_aspect_warrior, DetachAirWing_aspect_warrior, DetachPatrol_warhost, DetachBatallion_warhost, DetachBrigade_warhost, DetachVanguard_warhost, DetachSpearhead_warhost, DetachOutrider_warhost, DetachCommand_warhost, DetachPatrol__wych_cult_, DetachBatallion__wych_cult_, DetachVanguard__wych_cult_, DetachOutrider__wych_cult_, DetachCommand__wych_cult_, DetachAirWing__wych_cult_, DetachFort_aeldari]:
            self.det.build_detach(det, 'AELDARI', group=det.faction_base)

    def build_statistics(self):
        res = super(Aeldari8ed, self).build_statistics()
        cp = res.pop('Command points', 0)
        cp += get_drukhari_patrol_cp(itertools.chain(*[[unit.sub_roster.roster for unit in sec.units] for sec in self.sections]))
        res['Command points'] = cp
        return res


class _hive_fleet_8ed(Wh40k8edBase):
    army_name = 'Army of <Hive Fleet>'
    faction_base = '<HIVE FLEET>'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = '_hive_fleet_'

    def add_detachments(self):
        for det in [DetachPatrol_tyranids, DetachBatallion_tyranids, DetachBrigade_tyranids, DetachVanguard_tyranids, DetachSpearhead_tyranids, DetachOutrider_tyranids, DetachCommand_tyranids, DetachAirWing_tyranids, DetachFort_tyranids, DetachPatrol__hive_fleet_, DetachBatallion__hive_fleet_, DetachBrigade__hive_fleet_, DetachVanguard__hive_fleet_, DetachSpearhead__hive_fleet_, DetachOutrider__hive_fleet_, DetachCommand__hive_fleet_, DetachAirWing__hive_fleet_, DetachFort__hive_fleet_, DetachAuxilary__hive_fleet_]:
            self.det.build_detach(det, '<HIVE FLEET>', group=det.faction_base)


class _chapter_8ed(Wh40k8edBase):
    army_name = 'Army of <Chapter>'
    faction_base = '<CHAPTER>'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS', 'IRON HANDS', 'BLOOD RAVENS']
    army_id = '_chapter_'

    def add_detachments(self):
        for det in [DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachAirWing_imperium, DetachPatrol__chapter_, DetachBatallion__chapter_, DetachBrigade__chapter_, DetachVanguard__chapter_, DetachSpearhead__chapter_, DetachOutrider__chapter_, DetachCommand__chapter_, DetachSuperHeavy__chapter_, DetachSuperHeavyAux__chapter_, DetachAirWing__chapter_, DetachAuxilary__chapter_, DetachPatrol_adeptus_astartes, DetachBatallion_adeptus_astartes, DetachBrigade_adeptus_astartes, DetachVanguard_adeptus_astartes, DetachSpearhead_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachAirWing_adeptus_astartes]:
            self.det.build_detach(det, '<CHAPTER>', group=det.faction_base)


class Questor_mechanicus8ed(Wh40k8edBase):
    army_name = 'Army of Questor Mechanicus'
    faction_base = 'QUESTOR MECHANICUS'
    alternate_factions = []
    army_id = 'questor_mechanicus'
    obsolete = True

    def add_detachments(self):
        for det in [DetachSuperHeavy_imperium, DetachSuperHeavyAux_imperium, DetachSuperHeavy_questor_mechanicus, DetachSuperHeavyAux_questor_mechanicus, DetachSuperHeavy__household_, DetachSuperHeavyAux__household_]:
            self.det.build_detach(det, 'QUESTOR MECHANICUS', group=det.faction_base)


class Death_guard8ed(Wh40k8edBase):
    army_name = 'Army of Death Guard'
    faction_base = 'DEATH GUARD'
    alternate_factions = []
    army_id = 'death_guard'

    def add_detachments(self):
        for det in [DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachSuperHeavy_heretic_astartes, DetachSuperHeavyAux_heretic_astartes, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos, DetachPatrol_death_guard, DetachBatallion_death_guard, DetachBrigade_death_guard, DetachVanguard_death_guard, DetachSpearhead_death_guard, DetachOutrider_death_guard, DetachCommand_death_guard, DetachSuperHeavyAux_death_guard, DetachAuxilary_death_guard, DetachPatrol_nurgle, DetachBatallion_nurgle, DetachBrigade_nurgle, DetachVanguard_nurgle, DetachSpearhead_nurgle, DetachOutrider_nurgle, DetachCommand_nurgle, DetachSuperHeavyAux_nurgle]:
            self.det.build_detach(det, 'DEATH GUARD', group=det.faction_base)


class Vespid8ed(Wh40k8edBase):
    army_name = 'Army of Vespid'
    faction_base = 'VESPID'
    alternate_factions = []
    army_id = 'vespid'

    def add_detachments(self):
        for det in [DetachAuxilary_vespid]:
            self.det.build_detach(det, 'VESPID', group=det.faction_base)


class Harlequins8ed(Wh40k8edBase):
    army_name = 'Army of Harlequins'
    faction_base = 'HARLEQUINS'
    alternate_factions = []
    army_id = 'harlequins'

    def add_detachments(self):
        for det in [DetachPatrol__mascue_, DetachBatallion__mascue_, DetachBrigade__mascue_, DetachVanguard__mascue_, DetachSpearhead__mascue_, DetachOutrider__mascue_, DetachCommand__mascue_, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachPatrol_harlequins, DetachBatallion_harlequins, DetachBrigade_harlequins, DetachVanguard_harlequins, DetachSpearhead_harlequins, DetachOutrider_harlequins, DetachCommand_harlequins, DetachAuxilary_harlequins, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari]:
            self.det.build_detach(det, 'HARLEQUINS', group=det.faction_base)


class Ork8ed(Wh40k8edBase):
    army_name = 'Army of Ork'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'ork'

    def add_detachments(self):
        for det in [DetachPatrol_ork, DetachBatallion_ork, DetachBrigade_ork, DetachVanguard_ork, DetachSpearhead_ork, DetachOutrider_ork, DetachCommand_ork, DetachSuperHeavy_ork, DetachSuperHeavyAux_ork, DetachAirWing_ork, DetachAuxilary_ork, DetachFort_ork, DetachPatrol__clan_, DetachBatallion__clan_, DetachBrigade__clan_, DetachVanguard__clan_, DetachSpearhead__clan_, DetachOutrider__clan_, DetachCommand__clan_, DetachSuperHeavy__clan_, DetachSuperHeavyAux__clan_, DetachAirWing__clan_, DetachFort__clan_]:
            self.det.build_detach(det, 'ORK', group=det.faction_base)


class Militarum_tempestus8ed(Wh40k8edBase):
    army_name = 'Army of Militarum Tempestus'
    faction_base = 'MILITARUM TEMPESTUS'
    alternate_factions = []
    army_id = 'militarum_tempestus'

    def add_detachments(self):
        for det in [DetachPatrol_astra_militarum, DetachBatallion_astra_militarum, DetachVanguard_astra_militarum, DetachCommand_astra_militarum, DetachAuxilary_astra_militarum, DetachPatrol_imperium, DetachBatallion_imperium, DetachVanguard_imperium, DetachCommand_imperium, DetachPatrol_militarum_tempestus, DetachBatallion_militarum_tempestus, DetachCommand_militarum_tempestus, DetachAuxilary_militarum_tempestus]:
            self.det.build_detach(det, 'MILITARUM TEMPESTUS', group=det.faction_base)


class Genestealer_cults8ed(Wh40k8edBase):
    army_name = 'Army of Genestealer Cults'
    faction_base = 'GENESTEALER CULTS'
    alternate_factions = []
    army_id = 'genestealer_cults'

    def add_detachments(self):
        for det in [DetachPatrol_brood_brothers, DetachBatallion_brood_brothers, DetachBrigade_brood_brothers, DetachVanguard_brood_brothers, DetachSpearhead_brood_brothers, DetachOutrider_brood_brothers, DetachCommand_brood_brothers, DetachSuperHeavy_brood_brothers, DetachSuperHeavyAux_brood_brothers, DetachAirWing_brood_brothers, DetachPatrol_tyranids, DetachBatallion_tyranids, DetachBrigade_tyranids, DetachVanguard_tyranids, DetachSpearhead_tyranids, DetachOutrider_tyranids, DetachCommand_tyranids, DetachPatrol_genestealer_cults, DetachBatallion_genestealer_cults, DetachBrigade_genestealer_cults, DetachVanguard_genestealer_cults, DetachSpearhead_genestealer_cults, DetachOutrider_genestealer_cults, DetachCommand_genestealer_cults, DetachAuxilary_genestealer_cults, DetachFort_genestealer_cults]:
            self.det.build_detach(det, 'GENESTEALER CULTS', group=det.faction_base)


class _craftworld_8ed(Wh40k8edBase):
    army_name = 'Army of <Craftworld>'
    faction_base = '<CRAFTWORLD>'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = '_craftworld_'

    def add_detachments(self):
        for det in [DetachPatrol_asuryani, DetachBatallion_asuryani, DetachBrigade_asuryani, DetachVanguard_asuryani, DetachSpearhead_asuryani, DetachOutrider_asuryani, DetachCommand_asuryani, DetachSuperHeavy_asuryani, DetachSuperHeavyAux_asuryani, DetachAirWing_asuryani, DetachAuxilary_asuryani, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachSuperHeavy_aeldari, DetachSuperHeavyAux_aeldari, DetachAirWing_aeldari, DetachPatrol__craftworld_, DetachBatallion__craftworld_, DetachBrigade__craftworld_, DetachVanguard__craftworld_, DetachSpearhead__craftworld_, DetachOutrider__craftworld_, DetachCommand__craftworld_, DetachSuperHeavy__craftworld_, DetachSuperHeavyAux__craftworld_, DetachAirWing__craftworld_, DetachAuxilary__craftworld_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari, DetachSuperHeavy_ynnari, DetachSuperHeavyAux_ynnari, DetachAirWing_ynnari, DetachVanguard_spirit_host, DetachSpearhead_spirit_host, DetachCommand_spirit_host, DetachSuperHeavy_spirit_host, DetachSuperHeavyAux_spirit_host, DetachAirWing_spirit_host, DetachPatrol_aspect_warrior, DetachBatallion_aspect_warrior, DetachBrigade_aspect_warrior, DetachVanguard_aspect_warrior, DetachSpearhead_aspect_warrior, DetachOutrider_aspect_warrior, DetachCommand_aspect_warrior, DetachAirWing_aspect_warrior, DetachPatrol_warhost, DetachBatallion_warhost, DetachBrigade_warhost, DetachVanguard_warhost, DetachSpearhead_warhost, DetachOutrider_warhost, DetachCommand_warhost]:
            self.det.build_detach(det, '<CRAFTWORLD>', group=det.faction_base)


class _kabal_8ed(Wh40k8edBase):
    army_name = 'Army of <Kabal>'
    faction_base = '<KABAL>'
    alternate_factions = []
    army_id = '_kabal_'

    def add_detachments(self):
        for det in [DetachPatrol_drukhari, DetachBatallion_drukhari, DetachVanguard_drukhari, DetachSpearhead_drukhari, DetachCommand_drukhari, DetachAirWing_drukhari, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachCommand_aeldari, DetachAirWing_aeldari, DetachPatrol__kabal_, DetachBatallion__kabal_, DetachVanguard__kabal_, DetachSpearhead__kabal_, DetachCommand__kabal_, DetachAirWing__kabal_, DetachAuxilary__kabal_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachCommand_ynnari, DetachAirWing_ynnari, DetachAirWing__wych_cult_]:
            self.det.build_detach(det, '<KABAL>', group=det.faction_base)

    def build_statistics(self):
        res = super(_kabal_8ed, self).build_statistics()
        cp = res.pop('Command points', 0)
        cp += get_drukhari_patrol_cp(itertools.chain(*[[unit.sub_roster.roster for unit in sec.units] for sec in self.sections]))
        res['Command points'] = cp
        return res


class _clan_8ed(Wh40k8edBase):
    army_name = 'Army of <Clan>'
    faction_base = '<CLAN>'
    alternate_factions = ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = '_clan_'

    def add_detachments(self):
        for det in [DetachPatrol_ork, DetachBatallion_ork, DetachBrigade_ork, DetachVanguard_ork, DetachSpearhead_ork, DetachOutrider_ork, DetachCommand_ork, DetachSuperHeavy_ork, DetachSuperHeavyAux_ork, DetachAirWing_ork, DetachFort_ork, DetachPatrol__clan_, DetachBatallion__clan_, DetachBrigade__clan_, DetachVanguard__clan_, DetachSpearhead__clan_, DetachOutrider__clan_, DetachCommand__clan_, DetachSuperHeavy__clan_, DetachSuperHeavyAux__clan_, DetachAirWing__clan_, DetachAuxilary__clan_, DetachFort__clan_]:
            self.det.build_detach(det, '<CLAN>', group=det.faction_base)


class _ordo_8ed(Wh40k8edBase):
    army_name = 'Army of <Ordo>'
    faction_base = '<ORDO>'
    alternate_factions = ['ORDO MALLEUS', 'ORDO HERETICUS', 'ORDO XENOS']
    army_id = '_ordo_'

    def add_detachments(self):
        for det in [DetachVanguard_imperium, DetachCommand_imperium, DetachVanguard_inquisition, DetachCommand_inquisition, DetachVanguard__ordo_, DetachCommand__ordo_, DetachAuxilary__ordo_]:
            self.det.build_detach(det, '<ORDO>', group=det.faction_base)


class Adeptus_astartes8ed(Wh40k8edBase):
    army_name = 'Army of Adeptus Astartes'
    faction_base = 'ADEPTUS ASTARTES'
    alternate_factions = []
    army_id = 'adeptus_astartes'

    def add_detachments(self):
        for det in [DetachVanguard_ravenwing, DetachOutrider_ravenwing, DetachCommand_ravenwing, DetachAirWing_ravenwing, DetachAuxilary_ravenwing, DetachPatrol_blood_angels, DetachBatallion_blood_angels, DetachBrigade_blood_angels, DetachVanguard_blood_angels, DetachSpearhead_blood_angels, DetachOutrider_blood_angels, DetachCommand_blood_angels, DetachSuperHeavy_blood_angels, DetachSuperHeavyAux_blood_angels, DetachAirWing_blood_angels, DetachAuxilary_blood_angels, DetachVanguard_deathwing, DetachCommand_deathwing, DetachAuxilary_deathwing, DetachSpearhead_deathwing, DetachPatrol_deathwatch, DetachBatallion_deathwatch, DetachBrigade_deathwatch, DetachVanguard_deathwatch, DetachSpearhead_deathwatch, DetachOutrider_deathwatch, DetachCommand_deathwatch, DetachSuperHeavy_deathwatch, DetachSuperHeavyAux_deathwatch, DetachAirWing_deathwatch, DetachPatrol_space_wolves, DetachBatallion_space_wolves, DetachBrigade_space_wolves, DetachVanguard_space_wolves, DetachSpearhead_space_wolves, DetachOutrider_space_wolves, DetachCommand_space_wolves, DetachSuperHeavy_space_wolves, DetachSuperHeavyAux_space_wolves, DetachAirWing_space_wolves, DetachAuxilary_space_wolves, DetachPatrol_dark_angels, DetachBatallion_dark_angels, DetachBrigade_dark_angels, DetachVanguard_dark_angels, DetachSpearhead_dark_angels, DetachOutrider_dark_angels, DetachCommand_dark_angels, DetachSuperHeavy_dark_angels, DetachSuperHeavyAux_dark_angels, DetachAirWing_dark_angels, DetachAuxilary_dark_angels, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachSuperHeavy_imperium, DetachSuperHeavyAux_imperium, DetachAirWing_imperium, DetachPatrol__chapter_, DetachBatallion__chapter_, DetachBrigade__chapter_, DetachVanguard__chapter_, DetachSpearhead__chapter_, DetachOutrider__chapter_, DetachCommand__chapter_, DetachSuperHeavy__chapter_, DetachSuperHeavyAux__chapter_, DetachAirWing__chapter_, DetachAuxilary__chapter_, DetachPatrol_adeptus_astartes, DetachBatallion_adeptus_astartes, DetachBrigade_adeptus_astartes, DetachVanguard_adeptus_astartes, DetachSpearhead_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachSuperHeavy_adeptus_astartes, DetachSuperHeavyAux_adeptus_astartes, DetachAirWing_adeptus_astartes, DetachAuxilary_adeptus_astartes, DetachVanguard_death_company, DetachCommand_death_company, DetachPatrol_grey_knights, DetachBatallion_grey_knights, DetachBrigade_grey_knights, DetachVanguard_grey_knights, DetachSpearhead_grey_knights, DetachOutrider_grey_knights, DetachCommand_grey_knights, DetachAirWing_grey_knights, DamnedVanguard]:
            self.det.build_detach(det, 'ADEPTUS ASTARTES', group=det.faction_base)


class Cult_mechanicus8ed(Wh40k8edBase):
    army_name = 'Army of Cult Mechanicus'
    faction_base = 'CULT MECHANICUS'
    alternate_factions = []
    army_id = 'cult_mechanicus'

    def add_detachments(self):
        for det in [DetachVanguard_astra_militarum, DetachCommand_astra_militarum, DetachAuxilary_astra_militarum, DetachPatrol__forge_world_, DetachBatallion__forge_world_, DetachVanguard__forge_world_, DetachSpearhead__forge_world_, DetachCommand__forge_world_, DetachPatrol_imperium, DetachBatallion_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachCommand_imperium, DetachPatrol_cult_mechanicus, DetachBatallion_cult_mechanicus, DetachVanguard_cult_mechanicus, DetachSpearhead_cult_mechanicus, DetachCommand_cult_mechanicus, DetachAuxilary_cult_mechanicus, DetachPatrol_adeptus_mechanicus, DetachBatallion_adeptus_mechanicus, DetachVanguard_adeptus_mechanicus, DetachSpearhead_adeptus_mechanicus, DetachCommand_adeptus_mechanicus]:
            self.det.build_detach(det, 'CULT MECHANICUS', group=det.faction_base)


class Sisters_of_silence8ed(Wh40k8edBase):
    army_name = 'Army of Sisters Of Silence'
    faction_base = 'SISTERS OF SILENCE'
    alternate_factions = []
    army_id = 'sisters_of_silence'

    def add_detachments(self):
        for det in [DetachAuxilary_sisters_of_silence, SilenceVanguard]:
            self.det.build_detach(det, 'SISTERS OF SILENCE', group=det.faction_base)


class Ynnari8ed(Wh40k8edBase):
    army_name = 'Army of Ynnari'
    faction_base = 'YNNARI'
    alternate_factions = []
    army_id = 'ynnari'

    def add_detachments(self):
        for det in [DetachPatrol_asuryani, DetachBatallion_asuryani, DetachBrigade_asuryani, DetachVanguard_asuryani, DetachSpearhead_asuryani, DetachOutrider_asuryani, DetachCommand_asuryani, DetachSuperHeavy_asuryani, DetachSuperHeavyAux_asuryani, DetachAirWing_asuryani, DetachAuxilary_asuryani, DetachPatrol__mascue_, DetachBatallion__mascue_, DetachBrigade__mascue_, DetachVanguard__mascue_, DetachSpearhead__mascue_, DetachOutrider__mascue_, DetachCommand__mascue_, DetachPatrol_drukhari, DetachBatallion_drukhari, DetachBrigade_drukhari, DetachVanguard_drukhari, DetachSpearhead_drukhari, DetachOutrider_drukhari, DetachCommand_drukhari, DetachAirWing_drukhari, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachSuperHeavy_aeldari, DetachSuperHeavyAux_aeldari, DetachAirWing_aeldari, DetachPatrol_harlequins, DetachBatallion_harlequins, DetachBrigade_harlequins, DetachVanguard_harlequins, DetachSpearhead_harlequins, DetachOutrider_harlequins, DetachCommand_harlequins, DetachPatrol__craftworld_, DetachBatallion__craftworld_, DetachBrigade__craftworld_, DetachVanguard__craftworld_, DetachSpearhead__craftworld_, DetachOutrider__craftworld_, DetachCommand__craftworld_, DetachSuperHeavy__craftworld_, DetachSuperHeavyAux__craftworld_, DetachAirWing__craftworld_, DetachPatrol__kabal_, DetachBatallion__kabal_, DetachVanguard__kabal_, DetachSpearhead__kabal_, DetachCommand__kabal_, DetachAirWing__kabal_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari, DetachSuperHeavy_ynnari, DetachSuperHeavyAux_ynnari, DetachAirWing_ynnari, DetachAuxilary_ynnari, DetachVanguard_spirit_host, DetachSpearhead_spirit_host, DetachCommand_spirit_host, DetachSuperHeavy_spirit_host, DetachSuperHeavyAux_spirit_host, DetachAirWing_spirit_host, DetachPatrol_aspect_warrior, DetachBatallion_aspect_warrior, DetachBrigade_aspect_warrior, DetachVanguard_aspect_warrior, DetachSpearhead_aspect_warrior, DetachOutrider_aspect_warrior, DetachCommand_aspect_warrior, DetachAirWing_aspect_warrior, DetachPatrol_warhost, DetachBatallion_warhost, DetachBrigade_warhost, DetachVanguard_warhost, DetachSpearhead_warhost, DetachOutrider_warhost, DetachCommand_warhost, DetachPatrol__wych_cult_, DetachBatallion__wych_cult_, DetachVanguard__wych_cult_, DetachOutrider__wych_cult_, DetachCommand__wych_cult_, DetachAirWing__wych_cult_]:
            self.det.build_detach(det, 'YNNARI', group=det.faction_base)


class Nurgle8ed(Wh40k8edBase):
    army_name = 'Army of Nurgle'
    faction_base = 'NURGLE'
    alternate_factions = []
    army_id = 'nurgle'

    def add_detachments(self):
        for det in [DetachPatrol_heretic_astartes, DetachBatallion_heretic_astartes, DetachBrigade_heretic_astartes, DetachVanguard_heretic_astartes, DetachSpearhead_heretic_astartes, DetachOutrider_heretic_astartes, DetachCommand_heretic_astartes, DetachSuperHeavy_heretic_astartes, DetachSuperHeavyAux_heretic_astartes, DetachAirWing_heretic_astartes, DetachAuxilary_heretic_astartes, DetachPatrol__legion_, DetachBatallion__legion_, DetachBrigade__legion_, DetachVanguard__legion_, DetachSpearhead__legion_, DetachOutrider__legion_, DetachCommand__legion_, DetachAirWing__legion_, DetachAuxilary__legion_, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachSuperHeavy_chaos, DetachSuperHeavyAux_chaos, DetachAirWing_chaos, DetachFort_chaos, DetachPatrol_death_guard, DetachBatallion_death_guard, DetachBrigade_death_guard, DetachVanguard_death_guard, DetachSpearhead_death_guard, DetachOutrider_death_guard, DetachCommand_death_guard, DetachSuperHeavyAux_death_guard, DetachPatrol_nurgle, DetachBatallion_nurgle, DetachBrigade_nurgle, DetachVanguard_nurgle, DetachSpearhead_nurgle, DetachOutrider_nurgle, DetachCommand_nurgle, DetachSuperHeavyAux_nurgle, DetachAirWing_nurgle, DetachAuxilary_nurgle, DetachPatrol_daemon, DetachBatallion_daemon, DetachBrigade_daemon, DetachVanguard_daemon, DetachSpearhead_daemon, DetachOutrider_daemon, DetachCommand_daemon, DetachFort_daemon]:
            self.det.build_detach(det, 'NURGLE', group=det.faction_base)


class _haemunculus_coven_8ed(Wh40k8edBase):
    army_name = 'Army of <Haemunculus Coven>'
    faction_base = '<HAEMUNCULUS COVEN>'
    alternate_factions = ['PROPHETS OF FLESH']
    army_id = '_haemunculus_coven_'

    def add_detachments(self):
        for det in [DetachPatrol_drukhari, DetachBatallion_drukhari, DetachVanguard_drukhari, DetachSpearhead_drukhari, DetachCommand_drukhari, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachCommand_aeldari, DetachPatrol__haemunculus_coven_, DetachBatallion__haemunculus_coven_, DetachVanguard__haemunculus_coven_, DetachSpearhead__haemunculus_coven_, DetachCommand__haemunculus_coven_, DetachAuxilary__haemunculus_coven_]:
            self.det.build_detach(det, '<HAEMUNCULUS COVEN>', group=det.faction_base)

    def build_statistics(self):
        res = super(_haemunculus_coven_8ed, self).build_statistics()
        cp = res.pop('Command points', 0)
        cp += get_drukhari_patrol_cp(itertools.chain(*[[unit.sub_roster.roster for unit in sec.units] for sec in self.sections]))
        res['Command points'] = cp
        return res


class _household_8ed(Wh40k8edBase):
    army_name = 'Army of <Household>'
    faction_base = '<HOUSEHOLD>'
    alternate_factions = ['TERRIN', 'GRIFFIN', 'HAWKSHROUD', 'CADMUS', 'MORTAN', 'RAVEN', 'TARANIS', 'KRAST', 'VULKER']
    army_id = '_household_'

    def add_detachments(self):
        for det in [DetachSuperHeavy_imperium, DetachSuperHeavyAux_imperium, DetachSuperHeavy_questor_imperialis, DetachSuperHeavyAux_questor_imperialis, DetachSuperHeavy_questor_mechanicus, DetachSuperHeavyAux_questor_mechanicus, DetachSuperHeavy__household_, DetachSuperHeavyAux__household_, DetachSuperHeavy_imperial_knights, DetachSuperHeavyAux_imperial_knights]:
            self.det.build_detach(det, '<HOUSEHOLD>', group=det.faction_base)


class Imperial_knights_8ed(Wh40k8edBase):
    army_name = 'Army of Imperial Knights'
    faction_base = 'IMPERIAL KNIGHTS'
    alternate_factions = []
    army_id = 'imperial_knights'

    def add_detachments(self):
        for det in [DetachSuperHeavy_imperium, DetachSuperHeavyAux_imperium, DetachSuperHeavy_questor_imperialis, DetachSuperHeavyAux_questor_imperialis, DetachSuperHeavy_questor_mechanicus, DetachSuperHeavyAux_questor_mechanicus, DetachSuperHeavy__household_, DetachSuperHeavyAux__household_, DetachSuperHeavy_imperial_knights, DetachSuperHeavyAux_imperial_knights, DetachFort_imperial_knights]:
            self.det.build_detach(det, 'IMPERIAL KNIGHTS', group=det.faction_base)


class Death_company8ed(Wh40k8edBase):
    army_name = 'Army of Death Company'
    faction_base = 'DEATH COMPANY'
    alternate_factions = []
    army_id = 'death_company'

    def add_detachments(self):
        for det in [DetachVanguard_blood_angels, DetachCommand_blood_angels, DetachVanguard_imperium, DetachCommand_imperium, DetachVanguard_adeptus_astartes, DetachCommand_adeptus_astartes, DetachVanguard_death_company, DetachCommand_death_company, DetachAuxilary_death_company]:
            self.det.build_detach(det, 'DEATH COMPANY', group=det.faction_base)


class Spirit_host8ed(Wh40k8edBase):
    army_name = 'Army of Spirit Host'
    faction_base = 'SPIRIT HOST'
    alternate_factions = []
    army_id = 'spirit_host'

    def add_detachments(self):
        for det in [DetachVanguard_asuryani, DetachSpearhead_asuryani, DetachCommand_asuryani, DetachSuperHeavy_asuryani, DetachSuperHeavyAux_asuryani, DetachAirWing_asuryani, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachCommand_aeldari, DetachSuperHeavy_aeldari, DetachSuperHeavyAux_aeldari, DetachAirWing_aeldari, DetachVanguard__craftworld_, DetachSpearhead__craftworld_, DetachCommand__craftworld_, DetachSuperHeavy__craftworld_, DetachSuperHeavyAux__craftworld_, DetachAirWing__craftworld_, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachCommand_ynnari, DetachSuperHeavy_ynnari, DetachSuperHeavyAux_ynnari, DetachAirWing_ynnari, DetachVanguard_spirit_host, DetachSpearhead_spirit_host, DetachCommand_spirit_host, DetachSuperHeavy_spirit_host, DetachSuperHeavyAux_spirit_host, DetachAirWing_spirit_host, DetachAuxilary_spirit_host]:
            self.det.build_detach(det, 'SPIRIT HOST', group=det.faction_base)


class _order_8ed(Wh40k8edBase):
    army_name = 'Army of <Order>'
    faction_base = '<ORDER>'
    alternate_factions = ['VALOROUS HEART', 'OUR MARTYRED LADY', 'EBON CHALICE', 'ARGENT SHROUD', 'BLOODY ROSE', 'SACRED ROSE']
    army_id = '_order_'

    def add_detachments(self):
        for det in [DetachPatrol_adeptus_ministorum, DetachBatallion_adeptus_ministorum, DetachBrigade_adeptus_ministorum, DetachVanguard_adeptus_ministorum, DetachSpearhead_adeptus_ministorum, DetachOutrider_adeptus_ministorum, DetachCommand_adeptus_ministorum, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachPatrol_adepta_sororitas, DetachBatallion_adepta_sororitas, DetachBrigade_adepta_sororitas, DetachVanguard_adepta_sororitas, DetachSpearhead_adepta_sororitas, DetachOutrider_adepta_sororitas, DetachCommand_adepta_sororitas, DetachPatrol__order_, DetachBatallion__order_, DetachBrigade__order_, DetachVanguard__order_, DetachSpearhead__order_, DetachOutrider__order_, DetachCommand__order_, DetachAuxilary__order_]:
            self.det.build_detach(det, '<ORDER>', group=det.faction_base)


class Officio_assassinorum8ed(Wh40k8edBase):
    army_name = 'Army of Officio Assassinorum'
    faction_base = 'OFFICIO ASSASSINORUM'
    alternate_factions = []
    army_id = 'officio_assassinorum'

    def add_detachments(self):
        for det in [DetachAuxilary_officio_assassinorum, AssassinVanguard]:
            self.det.build_detach(det, 'OFFICIO ASSASSINORUM', group=det.faction_base)


class Adeptus_custodes8ed(Wh40k8edBase):
    army_name = 'Army of Adeptus Custodes'
    faction_base = 'ADEPTUS CUSTODES'
    alternate_factions = []
    army_id = 'adeptus_custodes'

    def add_detachments(self):
        for det in [DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachPatrol_adeptus_custodes, DetachBatallion_adeptus_custodes, DetachBrigade_adeptus_custodes, DetachVanguard_adeptus_custodes, DetachSpearhead_adeptus_custodes, DetachOutrider_adeptus_custodes, DetachCommand_adeptus_custodes, DetachAuxilary_adeptus_custodes]:
            self.det.build_detach(det, 'ADEPTUS CUSTODES', group=det.faction_base)


class Aspect_warrior8ed(Wh40k8edBase):
    army_name = 'Army of Aspect Warrior'
    faction_base = 'ASPECT WARRIOR'
    alternate_factions = []
    army_id = 'aspect_warrior'

    def add_detachments(self):
        for det in [DetachPatrol_asuryani, DetachBatallion_asuryani, DetachBrigade_asuryani, DetachVanguard_asuryani, DetachSpearhead_asuryani, DetachOutrider_asuryani, DetachCommand_asuryani, DetachAirWing_asuryani, DetachAuxilary_asuryani, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachAirWing_aeldari, DetachPatrol__craftworld_, DetachBatallion__craftworld_, DetachBrigade__craftworld_, DetachVanguard__craftworld_, DetachSpearhead__craftworld_, DetachOutrider__craftworld_, DetachCommand__craftworld_, DetachAirWing__craftworld_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari, DetachAirWing_ynnari, DetachPatrol_aspect_warrior, DetachBatallion_aspect_warrior, DetachBrigade_aspect_warrior, DetachVanguard_aspect_warrior, DetachSpearhead_aspect_warrior, DetachOutrider_aspect_warrior, DetachCommand_aspect_warrior, DetachAirWing_aspect_warrior, DetachAuxilary_aspect_warrior]:
            self.det.build_detach(det, 'ASPECT WARRIOR', group=det.faction_base)


class Grey_knights8ed(Wh40k8edBase):
    army_name = 'Army of Grey Knights'
    faction_base = 'GREY KNIGHTS'
    alternate_factions = []
    army_id = 'grey_knights'

    def add_detachments(self):
        for det in [DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachAirWing_imperium, DetachPatrol_adeptus_astartes, DetachBatallion_adeptus_astartes, DetachBrigade_adeptus_astartes, DetachVanguard_adeptus_astartes, DetachSpearhead_adeptus_astartes, DetachOutrider_adeptus_astartes, DetachCommand_adeptus_astartes, DetachAirWing_adeptus_astartes, DetachPatrol_grey_knights, DetachBatallion_grey_knights, DetachBrigade_grey_knights, DetachVanguard_grey_knights, DetachSpearhead_grey_knights, DetachOutrider_grey_knights, DetachCommand_grey_knights, DetachAirWing_grey_knights, DetachAuxilary_grey_knights]:
            self.det.build_detach(det, 'GREY KNIGHTS', group=det.faction_base)


class Daemon8ed(Wh40k8edBase):
    army_name = 'Army of Daemon'
    faction_base = 'DAEMON'
    alternate_factions = []
    army_id = 'daemon'

    def add_detachments(self):
        for det in [DetachPatrol_slaanesh, DetachBatallion_slaanesh, DetachBrigade_slaanesh, DetachVanguard_slaanesh, DetachSpearhead_slaanesh, DetachOutrider_slaanesh, DetachCommand_slaanesh, DetachPatrol_tzeentch, DetachBatallion_tzeentch, DetachBrigade_tzeentch, DetachVanguard_tzeentch, DetachSpearhead_tzeentch, DetachOutrider_tzeentch, DetachCommand_tzeentch, DetachPatrol_khorne, DetachBatallion_khorne, DetachBrigade_khorne, DetachVanguard_khorne, DetachSpearhead_khorne, DetachOutrider_khorne, DetachCommand_khorne, DetachPatrol_chaos, DetachBatallion_chaos, DetachBrigade_chaos, DetachVanguard_chaos, DetachSpearhead_chaos, DetachOutrider_chaos, DetachCommand_chaos, DetachFort_chaos, DetachPatrol_nurgle, DetachBatallion_nurgle, DetachBrigade_nurgle, DetachVanguard_nurgle, DetachSpearhead_nurgle, DetachOutrider_nurgle, DetachCommand_nurgle, DetachPatrol_daemon, DetachBatallion_daemon, DetachBrigade_daemon, DetachVanguard_daemon, DetachSpearhead_daemon, DetachOutrider_daemon, DetachCommand_daemon, DetachFort_daemon, DetachAuxilary_daemon]:
            self.det.build_detach(det, 'DAEMON', group=det.faction_base)


class Adeptus_mechanicus8ed(Wh40k8edBase):
    army_name = 'Army of Adeptus Mechanicus'
    faction_base = 'ADEPTUS MECHANICUS'
    alternate_factions = []
    army_id = 'adeptus_mechanicus'

    def add_detachments(self):
        for det in [DetachVanguard_astra_militarum, DetachCommand_astra_militarum, DetachAuxilary_astra_militarum, DetachPatrol__forge_world_, DetachBatallion__forge_world_, DetachBrigade__forge_world_, DetachVanguard__forge_world_, DetachSpearhead__forge_world_, DetachOutrider__forge_world_, DetachCommand__forge_world_, DetachPatrol_imperium, DetachBatallion_imperium, DetachBrigade_imperium, DetachVanguard_imperium, DetachSpearhead_imperium, DetachOutrider_imperium, DetachCommand_imperium, DetachPatrol_cult_mechanicus, DetachBatallion_cult_mechanicus, DetachVanguard_cult_mechanicus, DetachSpearhead_cult_mechanicus, DetachCommand_cult_mechanicus, DetachPatrol_adeptus_mechanicus, DetachBatallion_adeptus_mechanicus, DetachBrigade_adeptus_mechanicus, DetachVanguard_adeptus_mechanicus, DetachSpearhead_adeptus_mechanicus, DetachOutrider_adeptus_mechanicus, DetachCommand_adeptus_mechanicus, DetachAuxilary_adeptus_mechanicus, DetachAuxilary_skitarii]:
            self.det.build_detach(det, 'ADEPTUS MECHANICUS', group=det.faction_base)


class Jokaero8ed(Wh40k8edBase):
    army_name = 'Army of Jokaero'
    faction_base = 'JOKAERO'
    alternate_factions = []
    army_id = 'jokaero'

    def add_detachments(self):
        for det in [DetachAuxilary_jokaero]:
            self.det.build_detach(det, 'JOKAERO', group=det.faction_base)


class Warhost8ed(Wh40k8edBase):
    army_name = 'Army of Warhost'
    faction_base = 'WARHOST'
    alternate_factions = []
    army_id = 'warhost'

    def add_detachments(self):
        for det in [DetachPatrol_asuryani, DetachBatallion_asuryani, DetachBrigade_asuryani, DetachVanguard_asuryani, DetachSpearhead_asuryani, DetachOutrider_asuryani, DetachCommand_asuryani, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachBrigade_aeldari, DetachVanguard_aeldari, DetachSpearhead_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachPatrol__craftworld_, DetachBatallion__craftworld_, DetachBrigade__craftworld_, DetachVanguard__craftworld_, DetachSpearhead__craftworld_, DetachOutrider__craftworld_, DetachCommand__craftworld_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachBrigade_ynnari, DetachVanguard_ynnari, DetachSpearhead_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari, DetachPatrol_warhost, DetachBatallion_warhost, DetachBrigade_warhost, DetachVanguard_warhost, DetachSpearhead_warhost, DetachOutrider_warhost, DetachCommand_warhost, DetachAuxilary_warhost]:
            self.det.build_detach(det, 'WARHOST', group=det.faction_base)


class _wych_cult_8ed(Wh40k8edBase):
    army_name = 'Army of <Wych Cult>'
    faction_base = '<WYCH CULT>'
    alternate_factions = ['WYCH CULT OF STRIFE']
    army_id = '_wych_cult_'

    def add_detachments(self):
        for det in [DetachPatrol_drukhari, DetachBatallion_drukhari, DetachVanguard_drukhari, DetachOutrider_drukhari, DetachCommand_drukhari, DetachAirWing_drukhari, DetachPatrol_aeldari, DetachBatallion_aeldari, DetachVanguard_aeldari, DetachOutrider_aeldari, DetachCommand_aeldari, DetachAirWing_aeldari, DetachAirWing__kabal_, DetachPatrol_ynnari, DetachBatallion_ynnari, DetachVanguard_ynnari, DetachOutrider_ynnari, DetachCommand_ynnari, DetachAirWing_ynnari, DetachPatrol__wych_cult_, DetachBatallion__wych_cult_, DetachVanguard__wych_cult_, DetachOutrider__wych_cult_, DetachCommand__wych_cult_, DetachAirWing__wych_cult_, DetachAuxilary__wych_cult_]:
            self.det.build_detach(det, '<WYCH CULT>', group=det.faction_base)

    def build_statistics(self):
        res = super(_wych_cult_8ed, self).build_statistics()
        cp = res.pop('Command points', 0)
        cp += get_drukhari_patrol_cp(itertools.chain(*[[unit.sub_roster.roster for unit in sec.units] for sec in self.sections]))
        res['Command points'] = cp
        return res


class Militarum_auxilla8ed(Wh40k8edBase):
    army_name = 'Army of Militarum Auxilla'
    faction_base = 'MILITARUM AUXILLA'
    alternate_factions = []
    army_id = 'militarum_auxilla'

    def add_detachments(self):
        for det in [DetachAuxilary_militarum_auxilla, DetachAuxilary_astra_militarum]:
            self.det.build_detach(det, 'MILITARUM AUXILLA', group=det.faction_base)

armies = [Canoptek8ed, _sept_8ed, Ravenwing8ed, Blood_angels8ed,
          Asuryani8ed, Slaanesh8ed, Skitarii8ed, Deathwing8ed,
          Astra_telepathica8ed, Incubi8ed, Astra_militarum8ed, _regiment_8ed,
          Legion_of_the_damned8ed, _mascue_8ed, Deathwatch8ed,
          Officio_prefectus8ed, Thousand_sons8ed, Space_wolves8ed, Tzeentch8ed,
          Drukhari8ed, Heretic_astartes8ed, _legion_8ed, Dark_angels8ed,
          Khorne8ed, Questor_traitoris8ed, Scholastica_psykana8ed,
          _forge_world_8ed, Necrons8ed, Fallen8ed, Kroot8ed,
          Adeptus_ministorum8ed, Imperium8ed, Questor_imperialis8ed,
          Aeronautica_imperialis8ed, Chaos8ed, C_tan_shards8ed, Inquisition8ed,
          Tyranids8ed, T_au_empire8ed, Adepta_sororitas8ed, _dynasty_8ed,
          Aeldari8ed, _hive_fleet_8ed, _chapter_8ed, Questor_mechanicus8ed,
          Death_guard8ed, Vespid8ed, Harlequins8ed, Ork8ed,
          Militarum_tempestus8ed, Genestealer_cults8ed, _craftworld_8ed,
          _kabal_8ed, _clan_8ed, _ordo_8ed, Adeptus_astartes8ed,
          Cult_mechanicus8ed, Sisters_of_silence8ed, Ynnari8ed, Nurgle8ed,
          _haemunculus_coven_8ed, _household_8ed, Death_company8ed,
          Spirit_host8ed, _order_8ed, Officio_assassinorum8ed,
          Adeptus_custodes8ed, Aspect_warrior8ed, Grey_knights8ed, Daemon8ed,
          Adeptus_mechanicus8ed, Jokaero8ed, Warhost8ed,
          _wych_cult_8ed, Militarum_auxilla8ed]

class _sept_8edPlanetstrike_attacker_(_sept_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Sept>'
    faction_base = '<SEPT>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]
    army_id = '_sept_planetstrike_attacker_'

    def __init__(self):
        super(_sept_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker__sept_, DetachPlanetstrikeAttacker_t_au_empire]:
            self.mis.build_detach(det, '<SEPT>')
        return None


class _sept_8edPlanetstrike_defender_(_sept_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Sept>'
    faction_base = '<SEPT>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ["T'AU SEPT", 'FARSIGHT ENCLAVES', "VIOR'LA SEPT"]
    army_id = '_sept_planetstrike_defender_'

    def __init__(self):
        super(_sept_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender__sept_, DetachFortPlanetstrike__sept_, DetachPlanetstrikeDefender_t_au_empire, DetachFortPlanetstrike_t_au_empire]:
            self.mis.build_detach(det, '<SEPT>')
        return None


class Ravenwing8edPlanetstrike_attacker_(Ravenwing8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Ravenwing'
    faction_base = 'RAVENWING'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'ravenwingplanetstrike_attacker_'

    def __init__(self):
        super(Ravenwing8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_ravenwing, DetachPlanetstrikeAttacker_dark_angels, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_astartes]:
            self.mis.build_detach(det, 'RAVENWING')
        return None


class Blood_angels8edPlanetstrike_attacker_(Blood_angels8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Blood Angels'
    faction_base = 'BLOOD ANGELS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>']
    army_id = 'blood_angelsplanetstrike_attacker_'

    def __init__(self):
        super(Blood_angels8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_blood_angels, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_astartes]:
            self.mis.build_detach(det, 'BLOOD ANGELS')
        return None


class Blood_angels8edPlanetstrike_defender_(Blood_angels8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Blood Angels'
    faction_base = 'BLOOD ANGELS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['FLESH TEARERS', '<BLOOD ANGELS SUCCESSORS>']
    army_id = 'blood_angelsplanetstrike_defender_'

    def __init__(self):
        super(Blood_angels8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_blood_angels, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adeptus_astartes]:
            self.mis.build_detach(det, 'BLOOD ANGELS')
        return None


class Asuryani8edPlanetstrike_attacker_(Asuryani8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Asuryani'
    faction_base = 'ASURYANI'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'asuryaniplanetstrike_attacker_'

    def __init__(self):
        super(Asuryani8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_asuryani, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker__craftworld_, DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker_aspect_warrior, DetachPlanetstrikeAttacker_warhost]:
            self.mis.build_detach(det, 'ASURYANI')
        return None


class Asuryani8edPlanetstrike_defender_(Asuryani8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Asuryani'
    faction_base = 'ASURYANI'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'asuryaniplanetstrike_defender_'

    def __init__(self):
        super(Asuryani8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_asuryani, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender__craftworld_, DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender_aspect_warrior, DetachPlanetstrikeDefender_warhost]:
            self.mis.build_detach(det, 'ASURYANI')
        return None


class Slaanesh8edPlanetstrike_attacker_(Slaanesh8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Slaanesh'
    faction_base = 'SLAANESH'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'slaaneshplanetstrike_attacker_'

    def __init__(self):
        super(Slaanesh8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_slaanesh, DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker__legion_, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_daemon]:
            self.mis.build_detach(det, 'SLAANESH')
        return None


class Slaanesh8edPlanetstrike_defender_(Slaanesh8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Slaanesh'
    faction_base = 'SLAANESH'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'slaaneshplanetstrike_defender_'

    def __init__(self):
        super(Slaanesh8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_slaanesh, DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender__legion_, DetachPlanetstrikeDefender_chaos, DetachPlanetstrikeDefender_daemon]:
            self.mis.build_detach(det, 'SLAANESH')
        return None


class Astra_militarum8edPlanetstrike_attacker_(Astra_militarum8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Astra Militarum'
    faction_base = 'ASTRA MILITARUM'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'astra_militarumplanetstrike_attacker_'

    def __init__(self):
        super(Astra_militarum8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_astra_militarum, DetachPlanetstrikeAttacker__regiment_, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_genestealer_cults]:
            self.mis.build_detach(det, 'ASTRA MILITARUM')
        return None


class Astra_militarum8edPlanetstrike_defender_(Astra_militarum8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Astra Militarum'
    faction_base = 'ASTRA MILITARUM'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'astra_militarumplanetstrike_defender_'

    def __init__(self):
        super(Astra_militarum8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_astra_militarum, DetachPlanetstrikeDefender__regiment_, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_tyranids, DetachPlanetstrikeDefender_genestealer_cults]:
            self.mis.build_detach(det, 'ASTRA MILITARUM')
        return None


class _regiment_8edPlanetstrike_attacker_(_regiment_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Regiment>'
    faction_base = '<REGIMENT>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = '_regiment_planetstrike_attacker_'

    def __init__(self):
        super(_regiment_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_astra_militarum, DetachPlanetstrikeAttacker__regiment_, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_genestealer_cults]:
            self.mis.build_detach(det, '<REGIMENT>')
        return None


class _regiment_8edPlanetstrike_defender_(_regiment_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Regiment>'
    faction_base = '<REGIMENT>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['CADIAN', 'CATACHAN', 'VALHALLAN', 'VOSTROYAN', 'ARMAGEDDON', 'TALLARN', 'MORDIAN', 'BLOOD BROTHERS']
    army_id = '_regiment_planetstrike_defender_'

    def __init__(self):
        super(_regiment_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_astra_militarum, DetachPlanetstrikeDefender__regiment_, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_genestealer_cults]:
            self.mis.build_detach(det, '<REGIMENT>')
        return None


class _mascue_8edPlanetstrike_attacker_(_mascue_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Mascue>'
    faction_base = '<MASCUE>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = '_mascue_planetstrike_attacker_'

    def __init__(self):
        super(_mascue_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker__mascue_, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker_harlequins, DetachPlanetstrikeAttacker_ynnari]:
            self.mis.build_detach(det, '<MASCUE>')
        return None


class _mascue_8edPlanetstrike_defender_(_mascue_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Mascue>'
    faction_base = '<MASCUE>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = '_mascue_planetstrike_defender_'

    def __init__(self):
        super(_mascue_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender__mascue_, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender_harlequins, DetachPlanetstrikeDefender_ynnari]:
            self.mis.build_detach(det, '<MASCUE>')
        return None


class Deathwatch8edPlanetstrike_attacker_(Deathwatch8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Deathwatch'
    faction_base = 'DEATHWATCH'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'deathwatchplanetstrike_attacker_'

    def __init__(self):
        super(Deathwatch8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_deathwatch, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_astartes]:
            self.mis.build_detach(det, 'DEATHWATCH')
        return None


class Deathwatch8edPlanetstrike_defender_(Deathwatch8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Deathwatch'
    faction_base = 'DEATHWATCH'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'deathwatchplanetstrike_defender_'

    def __init__(self):
        super(Deathwatch8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_deathwatch, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adeptus_astartes]:
            self.mis.build_detach(det, 'DEATHWATCH')
        return None


class Thousand_sons8edPlanetstrike_attacker_(Thousand_sons8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Thousand Sons'
    faction_base = 'THOUSAND SONS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'thousand_sonsplanetstrike_attacker_'

    def __init__(self):
        super(Thousand_sons8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_tzeentch, DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker__legion_, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_thousand_sons]:
            self.mis.build_detach(det, 'THOUSAND SONS')
        return None


class Thousand_sons8edPlanetstrike_defender_(Thousand_sons8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Thousand Sons'
    faction_base = 'THOUSAND SONS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'thousand_sonsplanetstrike_defender_'

    def __init__(self):
        super(Thousand_sons8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_tzeentch, DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender__legion_, DetachPlanetstrikeDefender_chaos, DetachPlanetstrikeDefender_thousand_sons]:
            self.mis.build_detach(det, 'THOUSAND SONS')
        return None


class Space_wolves8edPlanetstrike_attacker_(Space_wolves8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Space Wolves'
    faction_base = 'SPACE WOLVES'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'space_wolvesplanetstrike_attacker_'

    def __init__(self):
        super(Space_wolves8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_space_wolves, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_astartes]:
            self.mis.build_detach(det, 'SPACE WOLVES')
        return None


class Space_wolves8edPlanetstrike_defender_(Space_wolves8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Space Wolves'
    faction_base = 'SPACE WOLVES'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'space_wolvesplanetstrike_defender_'

    def __init__(self):
        super(Space_wolves8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_space_wolves, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adeptus_astartes]:
            self.mis.build_detach(det, 'SPACE WOLVES')
        return None


class Tzeentch8edPlanetstrike_attacker_(Tzeentch8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Tzeentch'
    faction_base = 'TZEENTCH'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'tzeentchplanetstrike_attacker_'

    def __init__(self):
        super(Tzeentch8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_tzeentch, DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker__legion_, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_daemon, DetachPlanetstrikeAttacker_thousand_sons]:
            self.mis.build_detach(det, 'TZEENTCH')
        return None


class Tzeentch8edPlanetstrike_defender_(Tzeentch8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Tzeentch'
    faction_base = 'TZEENTCH'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'tzeentchplanetstrike_defender_'

    def __init__(self):
        super(Tzeentch8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_tzeentch, DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender__legion_, DetachPlanetstrikeDefender_chaos, DetachPlanetstrikeDefender_daemon, DetachPlanetstrikeDefender_thousand_sons]:
            self.mis.build_detach(det, 'TZEENTCH')
        return None


class Drukhari8edPlanetstrike_attacker_(Drukhari8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Drukhari'
    faction_base = 'DRUKHARI'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'drukhariplanetstrike_attacker_'

    def __init__(self):
        super(Drukhari8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_drukhari, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker__wych_cult_]:
            self.mis.build_detach(det, 'DRUKHARI')
        return None


class Drukhari8edPlanetstrike_defender_(Drukhari8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Drukhari'
    faction_base = 'DRUKHARI'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'drukhariplanetstrike_defender_'

    def __init__(self):
        super(Drukhari8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_drukhari, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender__kabal_, DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender__haemunculus_coven_]:
            self.mis.build_detach(det, 'DRUKHARI')
        return None


class Heretic_astartes8edPlanetstrike_attacker_(Heretic_astartes8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Heretic Astartes'
    faction_base = 'HERETIC ASTARTES'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'heretic_astartesplanetstrike_attacker_'

    def __init__(self):
        super(Heretic_astartes8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_slaanesh, DetachPlanetstrikeAttacker_tzeentch, DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker__legion_, DetachPlanetstrikeAttacker_khorne, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_death_guard, DetachPlanetstrikeAttacker_nurgle, DetachPlanetstrikeAttacker_thousand_sons]:
            self.mis.build_detach(det, 'HERETIC ASTARTES')
        return None


class Heretic_astartes8edPlanetstrike_defender_(Heretic_astartes8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Heretic Astartes'
    faction_base = 'HERETIC ASTARTES'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'heretic_astartesplanetstrike_defender_'

    def __init__(self):
        super(Heretic_astartes8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_slaanesh, DetachPlanetstrikeDefender_tzeentch, DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender__legion_, DetachPlanetstrikeDefender_khorne, DetachPlanetstrikeDefender_chaos, DetachPlanetstrikeDefender_death_guard, DetachPlanetstrikeDefender_nurgle, DetachPlanetstrikeDefender_thousand_sons]:
            self.mis.build_detach(det, 'HERETIC ASTARTES')
        return None


class _legion_8edPlanetstrike_attacker_(_legion_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Legion>'
    faction_base = '<LEGION>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN"]
    army_id = '_legion_planetstrike_attacker_'

    def __init__(self):
        super(_legion_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_slaanesh, DetachPlanetstrikeAttacker_tzeentch, DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker__legion_, DetachPlanetstrikeAttacker_khorne, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_nurgle]:
            self.mis.build_detach(det, '<LEGION>')
        return None


class _legion_8edPlanetstrike_defender_(_legion_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Legion>'
    faction_base = '<LEGION>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RENEGADES', 'RED CORSAIRS', 'WORLD EATERS', "EMPEROR'S CHILDREN"]
    army_id = '_legion_planetstrike_defender_'

    def __init__(self):
        super(_legion_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_slaanesh, DetachPlanetstrikeDefender_tzeentch, DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender__legion_, DetachPlanetstrikeDefender_khorne, DetachPlanetstrikeDefender_chaos, DetachPlanetstrikeDefender_nurgle]:
            self.mis.build_detach(det, '<LEGION>')
        return None


class Dark_angels8edPlanetstrike_attacker_(Dark_angels8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Dark Angels'
    faction_base = 'DARK ANGELS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'dark_angelsplanetstrike_attacker_'

    def __init__(self):
        super(Dark_angels8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_ravenwing, DetachPlanetstrikeAttacker_dark_angels, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_astartes]:
            self.mis.build_detach(det, 'DARK ANGELS')
        return None


class Dark_angels8edPlanetstrike_defender_(Dark_angels8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Dark Angels'
    faction_base = 'DARK ANGELS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['<DARK ANGELS SUCCESSORS>']
    army_id = 'dark_angelsplanetstrike_defender_'

    def __init__(self):
        super(Dark_angels8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_dark_angels, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adeptus_astartes]:
            self.mis.build_detach(det, 'DARK ANGELS')
        return None


class Khorne8edPlanetstrike_attacker_(Khorne8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Khorne'
    faction_base = 'KHORNE'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'khorneplanetstrike_attacker_'

    def __init__(self):
        super(Khorne8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker__legion_, DetachPlanetstrikeAttacker_khorne, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_daemon]:
            self.mis.build_detach(det, 'KHORNE')
        return None


class Khorne8edPlanetstrike_defender_(Khorne8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Khorne'
    faction_base = 'KHORNE'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'khorneplanetstrike_defender_'

    def __init__(self):
        super(Khorne8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender__legion_, DetachPlanetstrikeDefender_khorne, DetachPlanetstrikeDefender_chaos, DetachPlanetstrikeDefender_daemon]:
            self.mis.build_detach(det, 'KHORNE')
        return None


class _forge_world_8edPlanetstrike_attacker_(_forge_world_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Forge World>'
    faction_base = '<FORGE WORLD>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = '_forge_world_planetstrike_attacker_'

    def __init__(self):
        super(_forge_world_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker__forge_world_, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_mechanicus]:
            self.mis.build_detach(det, '<FORGE WORLD>')
        return None


class _forge_world_8edPlanetstrike_defender_(_forge_world_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Forge World>'
    faction_base = '<FORGE WORLD>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['MARS', 'GRAIA', 'METALICA', 'LUCIUS', 'AGRIPINAA', 'STYGIES VIII', 'RYZA']
    army_id = '_forge_world_planetstrike_defender_'

    def __init__(self):
        super(_forge_world_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender__forge_world_, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_cult_mechanicus, DetachPlanetstrikeDefender_adeptus_mechanicus]:
            self.mis.build_detach(det, '<FORGE WORLD>')
        return None


class Necrons8edPlanetstrike_attacker_(Necrons8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Necrons'
    faction_base = 'NECRONS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'necronsplanetstrike_attacker_'

    def __init__(self):
        super(Necrons8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_necrons, DetachPlanetstrikeAttacker__dynasty_]:
            self.mis.build_detach(det, 'NECRONS')
        return None


class Necrons8edPlanetstrike_defender_(Necrons8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Necrons'
    faction_base = 'NECRONS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'necronsplanetstrike_defender_'

    def __init__(self):
        super(Necrons8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_necrons, DetachPlanetstrikeDefender__dynasty_]:
            self.mis.build_detach(det, 'NECRONS')
        return None


class Adeptus_ministorum8edPlanetstrike_attacker_(Adeptus_ministorum8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Adeptus Ministorum'
    faction_base = 'ADEPTUS MINISTORUM'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'adeptus_ministorumplanetstrike_attacker_'

    def __init__(self):
        super(Adeptus_ministorum8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_adeptus_ministorum, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adepta_sororitas, DetachPlanetstrikeAttacker__order_]:
            self.mis.build_detach(det, 'ADEPTUS MINISTORUM')
        return None


class Adeptus_ministorum8edPlanetstrike_defender_(Adeptus_ministorum8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Adeptus Ministorum'
    faction_base = 'ADEPTUS MINISTORUM'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'adeptus_ministorumplanetstrike_defender_'

    def __init__(self):
        super(Adeptus_ministorum8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_adeptus_ministorum, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adepta_sororitas, DetachPlanetstrikeDefender__order_]:
            self.mis.build_detach(det, 'ADEPTUS MINISTORUM')
        return None


class Imperium8edPlanetstrike_attacker_(Imperium8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Imperium'
    faction_base = 'IMPERIUM'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'imperiumplanetstrike_attacker_'

    def __init__(self):
        super(Imperium8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_ravenwing, DetachPlanetstrikeAttacker_blood_angels, DetachPlanetstrikeAttacker_astra_militarum, DetachPlanetstrikeAttacker__regiment_, DetachPlanetstrikeAttacker_deathwatch, DetachPlanetstrikeAttacker_space_wolves, DetachPlanetstrikeAttacker_dark_angels, DetachPlanetstrikeAttacker__forge_world_, DetachPlanetstrikeAttacker_adeptus_ministorum, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adepta_sororitas, DetachPlanetstrikeAttacker__chapter_, DetachPlanetstrikeAttacker_genestealer_cults, DetachPlanetstrikeAttacker_adeptus_astartes, DetachPlanetstrikeAttacker__order_, DetachPlanetstrikeAttacker_adeptus_custodes, DetachPlanetstrikeAttacker_grey_knights, DetachPlanetstrikeAttacker_adeptus_mechanicus]:
            self.mis.build_detach(det, 'IMPERIUM')
        return None


class Imperium8edPlanetstrike_defender_(Imperium8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Imperium'
    faction_base = 'IMPERIUM'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'imperiumplanetstrike_defender_'

    def __init__(self):
        super(Imperium8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_blood_angels, DetachPlanetstrikeDefender_astra_militarum, DetachPlanetstrikeDefender__regiment_, DetachPlanetstrikeDefender_deathwatch, DetachPlanetstrikeDefender_space_wolves, DetachPlanetstrikeDefender_dark_angels, DetachPlanetstrikeDefender__forge_world_, DetachPlanetstrikeDefender_adeptus_ministorum, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_tyranids, DetachPlanetstrikeDefender_adepta_sororitas, DetachPlanetstrikeDefender__chapter_, DetachPlanetstrikeDefender_genestealer_cults, DetachPlanetstrikeDefender_adeptus_astartes, DetachPlanetstrikeDefender_cult_mechanicus, DetachPlanetstrikeDefender__order_, DetachPlanetstrikeDefender_adeptus_custodes, DetachPlanetstrikeDefender_grey_knights, DetachPlanetstrikeDefender_adeptus_mechanicus]:
            self.mis.build_detach(det, 'IMPERIUM')
        return None


class Chaos8edPlanetstrike_attacker_(Chaos8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Chaos'
    faction_base = 'CHAOS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'chaosplanetstrike_attacker_'

    def __init__(self):
        super(Chaos8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_slaanesh, DetachPlanetstrikeAttacker_tzeentch, DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker__legion_, DetachPlanetstrikeAttacker_khorne, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_death_guard, DetachPlanetstrikeAttacker_nurgle, DetachPlanetstrikeAttacker_daemon, DetachPlanetstrikeAttacker_thousand_sons]:
            self.mis.build_detach(det, 'CHAOS')
        return None


class Chaos8edPlanetstrike_defender_(Chaos8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Chaos'
    faction_base = 'CHAOS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'chaosplanetstrike_defender_'

    def __init__(self):
        super(Chaos8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_slaanesh, DetachPlanetstrikeDefender_tzeentch, DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender__legion_, DetachPlanetstrikeDefender_khorne, DetachPlanetstrikeDefender_chaos, DetachFortPlanetstrike_chaos, DetachPlanetstrikeDefender_death_guard, DetachPlanetstrikeDefender_nurgle, DetachFortPlanetstrike_nurgle, DetachPlanetstrikeDefender_daemon, DetachFortPlanetstrike_daemon, DetachPlanetstrikeDefender_thousand_sons]:
            self.mis.build_detach(det, 'CHAOS')
        return None


class Tyranids8edPlanetstrike_attacker_(Tyranids8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Tyranids'
    faction_base = 'TYRANIDS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'tyranidsplanetstrike_attacker_'

    def __init__(self):
        super(Tyranids8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_tyranids, DetachPlanetstrikeAttacker__hive_fleet_, DetachPlanetstrikeAttacker_genestealer_cults]:
            self.mis.build_detach(det, 'TYRANIDS')
        return None


class Tyranids8edPlanetstrike_defender_(Tyranids8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Tyranids'
    faction_base = 'TYRANIDS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'tyranidsplanetstrike_defender_'

    def __init__(self):
        super(Tyranids8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_tyranids, DetachFortPlanetstrike_tyranids, DetachPlanetstrikeDefender__hive_fleet_, DetachFortPlanetstrike__hive_fleet_, DetachPlanetstrikeDefender_genestealer_cults]:
            self.mis.build_detach(det, 'TYRANIDS')
        return None


class T_au_empire8edPlanetstrike_attacker_(T_au_empire8ed, Wh8edPlanetstrikeAttacker):
    army_name = "Army of T'au Empire"
    faction_base = "T'AU EMPIRE"
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 't_au_empireplanetstrike_attacker_'

    def __init__(self):
        super(T_au_empire8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker__sept_, DetachPlanetstrikeAttacker_t_au_empire]:
            self.mis.build_detach(det, "T'AU EMPIRE")
        return None


class T_au_empire8edPlanetstrike_defender_(T_au_empire8ed, Wh8edPlanetstrikeDefender):
    army_name = "Army of T'au Empire"
    faction_base = "T'AU EMPIRE"
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 't_au_empireplanetstrike_defender_'

    def __init__(self):
        super(T_au_empire8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender__sept_, DetachFortPlanetstrike__sept_, DetachPlanetstrikeDefender_t_au_empire, DetachFortPlanetstrike_t_au_empire]:
            self.mis.build_detach(det, "T'AU EMPIRE")
        return None


class Adepta_sororitas8edPlanetstrike_attacker_(Adepta_sororitas8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Adepta Sororitas'
    faction_base = 'ADEPTA SORORITAS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'adepta_sororitasplanetstrike_attacker_'

    def __init__(self):
        super(Adepta_sororitas8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_adeptus_ministorum, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adepta_sororitas, DetachPlanetstrikeAttacker__order_]:
            self.mis.build_detach(det, 'ADEPTA SORORITAS')
        return None


class Adepta_sororitas8edPlanetstrike_defender_(Adepta_sororitas8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Adepta Sororitas'
    faction_base = 'ADEPTA SORORITAS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'adepta_sororitasplanetstrike_defender_'

    def __init__(self):
        super(Adepta_sororitas8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_adeptus_ministorum, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adepta_sororitas, DetachPlanetstrikeDefender__order_]:
            self.mis.build_detach(det, 'ADEPTA SORORITAS')
        return None


class _dynasty_8edPlanetstrike_attacker_(_dynasty_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Dynasty>'
    faction_base = '<DYNASTY>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH']
    army_id = '_dynasty_planetstrike_attacker_'

    def __init__(self):
        super(_dynasty_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_necrons, DetachPlanetstrikeAttacker__dynasty_]:
            self.mis.build_detach(det, '<DYNASTY>')
        return None


class _dynasty_8edPlanetstrike_defender_(_dynasty_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Dynasty>'
    faction_base = '<DYNASTY>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['SAUTEKH', 'MEPHRIT', 'NOVOKH', 'NEPREKH', 'NIHILAKH']
    army_id = '_dynasty_planetstrike_defender_'

    def __init__(self):
        super(_dynasty_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_necrons, DetachPlanetstrikeDefender__dynasty_]:
            self.mis.build_detach(det, '<DYNASTY>')
        return None


class Aeldari8edPlanetstrike_attacker_(Aeldari8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Aeldari'
    faction_base = 'AELDARI'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'aeldariplanetstrike_attacker_'

    def __init__(self):
        super(Aeldari8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_asuryani, DetachPlanetstrikeAttacker__mascue_, DetachPlanetstrikeAttacker_drukhari, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker_harlequins, DetachPlanetstrikeAttacker__craftworld_, DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker_aspect_warrior, DetachPlanetstrikeAttacker_warhost, DetachPlanetstrikeAttacker__wych_cult_]:
            self.mis.build_detach(det, 'AELDARI')
        return None


class Aeldari8edPlanetstrike_defender_(Aeldari8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Aeldari'
    faction_base = 'AELDARI'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'aeldariplanetstrike_defender_'

    def __init__(self):
        super(Aeldari8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_asuryani, DetachPlanetstrikeDefender__mascue_, DetachPlanetstrikeDefender_drukhari, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender_harlequins, DetachPlanetstrikeDefender__craftworld_, DetachPlanetstrikeDefender__kabal_, DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender__haemunculus_coven_, DetachPlanetstrikeDefender_aspect_warrior, DetachPlanetstrikeDefender_warhost]:
            self.mis.build_detach(det, 'AELDARI')
        return None


class _hive_fleet_8edPlanetstrike_attacker_(_hive_fleet_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Hive Fleet>'
    faction_base = '<HIVE FLEET>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = '_hive_fleet_planetstrike_attacker_'

    def __init__(self):
        super(_hive_fleet_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_tyranids, DetachPlanetstrikeAttacker__hive_fleet_]:
            self.mis.build_detach(det, '<HIVE FLEET>')
        return None


class _hive_fleet_8edPlanetstrike_defender_(_hive_fleet_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Hive Fleet>'
    faction_base = '<HIVE FLEET>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND', 'HYDRA', 'KRONOS', 'LEVIATHAN']
    army_id = '_hive_fleet_planetstrike_defender_'

    def __init__(self):
        super(_hive_fleet_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_tyranids, DetachFortPlanetstrike_tyranids, DetachPlanetstrikeDefender__hive_fleet_, DetachFortPlanetstrike__hive_fleet_]:
            self.mis.build_detach(det, '<HIVE FLEET>')
        return None


class _chapter_8edPlanetstrike_attacker_(_chapter_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Chapter>'
    faction_base = '<CHAPTER>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS']
    army_id = '_chapter_planetstrike_attacker_'

    def __init__(self):
        super(_chapter_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker__chapter_, DetachPlanetstrikeAttacker_adeptus_astartes]:
            self.mis.build_detach(det, '<CHAPTER>')
        return None


class _chapter_8edPlanetstrike_defender_(_chapter_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Chapter>'
    faction_base = '<CHAPTER>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['ULTRAMARINES', 'IMPERIAL FISTS', 'CRIMSON FISTS', 'BLACK TEMPLARS', 'RAVEN GUARD', 'SALAMANDERS', 'WHITE SCARS']
    army_id = '_chapter_planetstrike_defender_'

    def __init__(self):
        super(_chapter_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender__chapter_, DetachPlanetstrikeDefender_adeptus_astartes]:
            self.mis.build_detach(det, '<CHAPTER>')
        return None


class Death_guard8edPlanetstrike_attacker_(Death_guard8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Death Guard'
    faction_base = 'DEATH GUARD'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'death_guardplanetstrike_attacker_'

    def __init__(self):
        super(Death_guard8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_death_guard, DetachPlanetstrikeAttacker_nurgle]:
            self.mis.build_detach(det, 'DEATH GUARD')
        return None


class Death_guard8edPlanetstrike_defender_(Death_guard8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Death Guard'
    faction_base = 'DEATH GUARD'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'death_guardplanetstrike_defender_'

    def __init__(self):
        super(Death_guard8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender_chaos, DetachPlanetstrikeDefender_death_guard, DetachPlanetstrikeDefender_nurgle]:
            self.mis.build_detach(det, 'DEATH GUARD')
        return None


class Harlequins8edPlanetstrike_attacker_(Harlequins8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Harlequins'
    faction_base = 'HARLEQUINS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'harlequinsplanetstrike_attacker_'

    def __init__(self):
        super(Harlequins8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker__mascue_, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker_harlequins, DetachPlanetstrikeAttacker_ynnari]:
            self.mis.build_detach(det, 'HARLEQUINS')
        return None


class Harlequins8edPlanetstrike_defender_(Harlequins8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Harlequins'
    faction_base = 'HARLEQUINS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'harlequinsplanetstrike_defender_'

    def __init__(self):
        super(Harlequins8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender__mascue_, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender_harlequins, DetachPlanetstrikeDefender_ynnari]:
            self.mis.build_detach(det, 'HARLEQUINS')
        return None


class Ork8edPlanetstrike_attacker_(Ork8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Ork'
    faction_base = 'ORK'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'orkplanetstrike_attacker_'

    def __init__(self):
        super(Ork8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_ork, DetachPlanetstrikeAttacker__clan_]:
            self.mis.build_detach(det, 'ORK')
        return None


class Ork8edPlanetstrike_defender_(Ork8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Ork'
    faction_base = 'ORK'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'orkplanetstrike_defender_'

    def __init__(self):
        super(Ork8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_ork, DetachPlanetstrikeDefender__clan_]:
            self.mis.build_detach(det, 'ORK')
        return None


class Genestealer_cults8edPlanetstrike_attacker_(Genestealer_cults8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Genestealer Cults'
    faction_base = 'GENESTEALER CULTS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'genestealer_cultsplanetstrike_attacker_'

    def __init__(self):
        super(Genestealer_cults8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_tyranids, DetachPlanetstrikeAttacker_genestealer_cults]:
            self.mis.build_detach(det, 'GENESTEALER CULTS')
        return None


class Genestealer_cults8edPlanetstrike_defender_(Genestealer_cults8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Genestealer Cults'
    faction_base = 'GENESTEALER CULTS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'genestealer_cultsplanetstrike_defender_'

    def __init__(self):
        super(Genestealer_cults8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_tyranids, DetachPlanetstrikeDefender_genestealer_cults]:
            self.mis.build_detach(det, 'GENESTEALER CULTS')
        return None


class _craftworld_8edPlanetstrike_attacker_(_craftworld_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Craftworld>'
    faction_base = '<CRAFTWORLD>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = '_craftworld_planetstrike_attacker_'

    def __init__(self):
        super(_craftworld_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_asuryani, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker__craftworld_, DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker_aspect_warrior, DetachPlanetstrikeAttacker_warhost]:
            self.mis.build_detach(det, '<CRAFTWORLD>')
        return None


class _craftworld_8edPlanetstrike_defender_(_craftworld_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Craftworld>'
    faction_base = '<CRAFTWORLD>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']
    army_id = '_craftworld_planetstrike_defender_'

    def __init__(self):
        super(_craftworld_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_asuryani, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender__craftworld_, DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender_aspect_warrior, DetachPlanetstrikeDefender_warhost]:
            self.mis.build_detach(det, '<CRAFTWORLD>')
        return None


class _kabal_8edPlanetstrike_defender_(_kabal_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Kabal>'
    faction_base = '<KABAL>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = '_kabal_planetstrike_defender_'

    def __init__(self):
        super(_kabal_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_drukhari, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender__kabal_, DetachPlanetstrikeDefender_ynnari]:
            self.mis.build_detach(det, '<KABAL>')
        return None


class _clan_8edPlanetstrike_attacker_(_clan_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Clan>'
    faction_base = '<CLAN>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['GOFF', 'BLOOD AXE', 'DEATHSKULLS', 'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES', 'FREEBOOTERZ']
    army_id = '_clan_planetstrike_attacker_'

    def __init__(self):
        super(_clan_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_ork, DetachPlanetstrikeAttacker__clan_]:
            self.mis.build_detach(det, '<CLAN>')
        return None


class _clan_8edPlanetstrike_defender_(_clan_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Clan>'
    faction_base = '<CLAN>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['GOFF', 'BLOOD AXE', 'DEATHSKULLS']
    army_id = '_clan_planetstrike_defender_'

    def __init__(self):
        super(_clan_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_ork, DetachPlanetstrikeDefender__clan_]:
            self.mis.build_detach(det, '<CLAN>')
        return None


class Adeptus_astartes8edPlanetstrike_attacker_(Adeptus_astartes8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Adeptus Astartes'
    faction_base = 'ADEPTUS ASTARTES'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'adeptus_astartesplanetstrike_attacker_'

    def __init__(self):
        super(Adeptus_astartes8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_ravenwing, DetachPlanetstrikeAttacker_blood_angels, DetachPlanetstrikeAttacker_deathwatch, DetachPlanetstrikeAttacker_space_wolves, DetachPlanetstrikeAttacker_dark_angels, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker__chapter_, DetachPlanetstrikeAttacker_adeptus_astartes, DetachPlanetstrikeAttacker_grey_knights]:
            self.mis.build_detach(det, 'ADEPTUS ASTARTES')
        return None


class Adeptus_astartes8edPlanetstrike_defender_(Adeptus_astartes8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Adeptus Astartes'
    faction_base = 'ADEPTUS ASTARTES'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'adeptus_astartesplanetstrike_defender_'

    def __init__(self):
        super(Adeptus_astartes8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_blood_angels, DetachPlanetstrikeDefender_deathwatch, DetachPlanetstrikeDefender_space_wolves, DetachPlanetstrikeDefender_dark_angels, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender__chapter_, DetachPlanetstrikeDefender_adeptus_astartes, DetachPlanetstrikeDefender_grey_knights]:
            self.mis.build_detach(det, 'ADEPTUS ASTARTES')
        return None


class Cult_mechanicus8edPlanetstrike_defender_(Cult_mechanicus8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Cult Mechanicus'
    faction_base = 'CULT MECHANICUS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'cult_mechanicusplanetstrike_defender_'

    def __init__(self):
        super(Cult_mechanicus8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender__forge_world_, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_cult_mechanicus, DetachPlanetstrikeDefender_adeptus_mechanicus]:
            self.mis.build_detach(det, 'CULT MECHANICUS')
        return None


class Ynnari8edPlanetstrike_attacker_(Ynnari8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Ynnari'
    faction_base = 'YNNARI'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'ynnariplanetstrike_attacker_'

    def __init__(self):
        super(Ynnari8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_asuryani, DetachPlanetstrikeAttacker__mascue_, DetachPlanetstrikeAttacker_drukhari, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker_harlequins, DetachPlanetstrikeAttacker__craftworld_, DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker_aspect_warrior, DetachPlanetstrikeAttacker_warhost, DetachPlanetstrikeAttacker__wych_cult_]:
            self.mis.build_detach(det, 'YNNARI')
        return None


class Ynnari8edPlanetstrike_defender_(Ynnari8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Ynnari'
    faction_base = 'YNNARI'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'ynnariplanetstrike_defender_'

    def __init__(self):
        super(Ynnari8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_asuryani, DetachPlanetstrikeDefender__mascue_, DetachPlanetstrikeDefender_drukhari, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender_harlequins, DetachPlanetstrikeDefender__craftworld_, DetachPlanetstrikeDefender__kabal_, DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender_aspect_warrior, DetachPlanetstrikeDefender_warhost]:
            self.mis.build_detach(det, 'YNNARI')
        return None


class Nurgle8edPlanetstrike_attacker_(Nurgle8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Nurgle'
    faction_base = 'NURGLE'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'nurgleplanetstrike_attacker_'

    def __init__(self):
        super(Nurgle8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_heretic_astartes, DetachPlanetstrikeAttacker__legion_, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_death_guard, DetachPlanetstrikeAttacker_nurgle, DetachPlanetstrikeAttacker_daemon]:
            self.mis.build_detach(det, 'NURGLE')
        return None


class Nurgle8edPlanetstrike_defender_(Nurgle8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Nurgle'
    faction_base = 'NURGLE'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'nurgleplanetstrike_defender_'

    def __init__(self):
        super(Nurgle8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_heretic_astartes, DetachPlanetstrikeDefender__legion_, DetachPlanetstrikeDefender_chaos, DetachFortPlanetstrike_chaos, DetachPlanetstrikeDefender_death_guard, DetachPlanetstrikeDefender_nurgle, DetachFortPlanetstrike_nurgle, DetachPlanetstrikeDefender_daemon, DetachFortPlanetstrike_daemon]:
            self.mis.build_detach(det, 'NURGLE')
        return None


class _haemunculus_coven_8edPlanetstrike_defender_(_haemunculus_coven_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Haemunculus Coven>'
    faction_base = '<HAEMUNCULUS COVEN>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = ['PROPHETS OF FLESH']
    army_id = '_haemunculus_coven_planetstrike_defender_'

    def __init__(self):
        super(_haemunculus_coven_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_drukhari, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender__haemunculus_coven_]:
            self.mis.build_detach(det, '<HAEMUNCULUS COVEN>')
        return None


class _order_8edPlanetstrike_attacker_(_order_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Order>'
    faction_base = '<ORDER>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = '_order_planetstrike_attacker_'

    def __init__(self):
        super(_order_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_adeptus_ministorum, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adepta_sororitas, DetachPlanetstrikeAttacker__order_]:
            self.mis.build_detach(det, '<ORDER>')
        return None


class _order_8edPlanetstrike_defender_(_order_8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of <Order>'
    faction_base = '<ORDER>'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = '_order_planetstrike_defender_'

    def __init__(self):
        super(_order_8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_adeptus_ministorum, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adepta_sororitas, DetachPlanetstrikeDefender__order_]:
            self.mis.build_detach(det, '<ORDER>')
        return None


class Adeptus_custodes8edPlanetstrike_attacker_(Adeptus_custodes8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Adeptus Custodes'
    faction_base = 'ADEPTUS CUSTODES'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'adeptus_custodesplanetstrike_attacker_'

    def __init__(self):
        super(Adeptus_custodes8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_custodes]:
            self.mis.build_detach(det, 'ADEPTUS CUSTODES')
        return None


class Adeptus_custodes8edPlanetstrike_defender_(Adeptus_custodes8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Adeptus Custodes'
    faction_base = 'ADEPTUS CUSTODES'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'adeptus_custodesplanetstrike_defender_'

    def __init__(self):
        super(Adeptus_custodes8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adeptus_custodes]:
            self.mis.build_detach(det, 'ADEPTUS CUSTODES')
        return None


class Aspect_warrior8edPlanetstrike_attacker_(Aspect_warrior8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Aspect Warrior'
    faction_base = 'ASPECT WARRIOR'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'aspect_warriorplanetstrike_attacker_'

    def __init__(self):
        super(Aspect_warrior8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_asuryani, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker__craftworld_, DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker_aspect_warrior]:
            self.mis.build_detach(det, 'ASPECT WARRIOR')
        return None


class Aspect_warrior8edPlanetstrike_defender_(Aspect_warrior8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Aspect Warrior'
    faction_base = 'ASPECT WARRIOR'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'aspect_warriorplanetstrike_defender_'

    def __init__(self):
        super(Aspect_warrior8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_asuryani, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender__craftworld_, DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender_aspect_warrior]:
            self.mis.build_detach(det, 'ASPECT WARRIOR')
        return None


class Grey_knights8edPlanetstrike_attacker_(Grey_knights8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Grey Knights'
    faction_base = 'GREY KNIGHTS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'grey_knightsplanetstrike_attacker_'

    def __init__(self):
        super(Grey_knights8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_astartes, DetachPlanetstrikeAttacker_grey_knights]:
            self.mis.build_detach(det, 'GREY KNIGHTS')
        return None


class Grey_knights8edPlanetstrike_defender_(Grey_knights8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Grey Knights'
    faction_base = 'GREY KNIGHTS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'grey_knightsplanetstrike_defender_'

    def __init__(self):
        super(Grey_knights8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_adeptus_astartes, DetachPlanetstrikeDefender_grey_knights]:
            self.mis.build_detach(det, 'GREY KNIGHTS')
        return None


class Daemon8edPlanetstrike_attacker_(Daemon8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Daemon'
    faction_base = 'DAEMON'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'daemonplanetstrike_attacker_'

    def __init__(self):
        super(Daemon8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_slaanesh, DetachPlanetstrikeAttacker_tzeentch, DetachPlanetstrikeAttacker_khorne, DetachPlanetstrikeAttacker_chaos, DetachPlanetstrikeAttacker_nurgle, DetachPlanetstrikeAttacker_daemon]:
            self.mis.build_detach(det, 'DAEMON')
        return None


class Daemon8edPlanetstrike_defender_(Daemon8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Daemon'
    faction_base = 'DAEMON'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'daemonplanetstrike_defender_'

    def __init__(self):
        super(Daemon8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_slaanesh, DetachPlanetstrikeDefender_tzeentch, DetachPlanetstrikeDefender_khorne, DetachPlanetstrikeDefender_chaos, DetachFortPlanetstrike_chaos, DetachPlanetstrikeDefender_nurgle, DetachFortPlanetstrike_nurgle, DetachPlanetstrikeDefender_daemon, DetachFortPlanetstrike_daemon]:
            self.mis.build_detach(det, 'DAEMON')
        return None


class Adeptus_mechanicus8edPlanetstrike_attacker_(Adeptus_mechanicus8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Adeptus Mechanicus'
    faction_base = 'ADEPTUS MECHANICUS'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'adeptus_mechanicusplanetstrike_attacker_'

    def __init__(self):
        super(Adeptus_mechanicus8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker__forge_world_, DetachPlanetstrikeAttacker_imperium, DetachPlanetstrikeAttacker_adeptus_mechanicus]:
            self.mis.build_detach(det, 'ADEPTUS MECHANICUS')
        return None


class Adeptus_mechanicus8edPlanetstrike_defender_(Adeptus_mechanicus8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Adeptus Mechanicus'
    faction_base = 'ADEPTUS MECHANICUS'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'adeptus_mechanicusplanetstrike_defender_'

    def __init__(self):
        super(Adeptus_mechanicus8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender__forge_world_, DetachPlanetstrikeDefender_imperium, DetachPlanetstrikeDefender_cult_mechanicus, DetachPlanetstrikeDefender_adeptus_mechanicus]:
            self.mis.build_detach(det, 'ADEPTUS MECHANICUS')
        return None


class Warhost8edPlanetstrike_attacker_(Warhost8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of Warhost'
    faction_base = 'WARHOST'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = []
    army_id = 'warhostplanetstrike_attacker_'

    def __init__(self):
        super(Warhost8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_asuryani, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker__craftworld_, DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker_warhost]:
            self.mis.build_detach(det, 'WARHOST')
        return None


class Warhost8edPlanetstrike_defender_(Warhost8ed, Wh8edPlanetstrikeDefender):
    army_name = 'Army of Warhost'
    faction_base = 'WARHOST'
    roster_type = 'Planetstrike (Defender)'
    alternate_factions = []
    army_id = 'warhostplanetstrike_defender_'

    def __init__(self):
        super(Warhost8edPlanetstrike_defender_, self).__init__()
        for det in [DetachPlanetstrikeDefender_asuryani, DetachPlanetstrikeDefender_aeldari, DetachPlanetstrikeDefender__craftworld_, DetachPlanetstrikeDefender_ynnari, DetachPlanetstrikeDefender_warhost]:
            self.mis.build_detach(det, 'WARHOST')
        return None


class _wych_cult_8edPlanetstrike_attacker_(_wych_cult_8ed, Wh8edPlanetstrikeAttacker):
    army_name = 'Army of <Wych Cult>'
    faction_base = '<WYCH CULT>'
    roster_type = 'Planetstrike (Attacker)'
    alternate_factions = ['WYCH CULT OF STRIFE']
    army_id = '_wych_cult_planetstrike_attacker_'

    def __init__(self):
        super(_wych_cult_8edPlanetstrike_attacker_, self).__init__()
        for det in [DetachPlanetstrikeAttacker_drukhari, DetachPlanetstrikeAttacker_aeldari, DetachPlanetstrikeAttacker_ynnari, DetachPlanetstrikeAttacker__wych_cult_]:
            self.mis.build_detach(det, '<WYCH CULT>')
        return None
armies += [_sept_8edPlanetstrike_attacker_,
           _sept_8edPlanetstrike_defender_, Ravenwing8edPlanetstrike_attacker_,
           Blood_angels8edPlanetstrike_attacker_,
           Blood_angels8edPlanetstrike_defender_,
           Asuryani8edPlanetstrike_attacker_, Asuryani8edPlanetstrike_defender_,
           Slaanesh8edPlanetstrike_attacker_, Slaanesh8edPlanetstrike_defender_,
           Astra_militarum8edPlanetstrike_attacker_,
           Astra_militarum8edPlanetstrike_defender_,
           _regiment_8edPlanetstrike_attacker_,
           _regiment_8edPlanetstrike_defender_,
           _mascue_8edPlanetstrike_attacker_, _mascue_8edPlanetstrike_defender_,
           Deathwatch8edPlanetstrike_attacker_,
           Deathwatch8edPlanetstrike_defender_,
           Thousand_sons8edPlanetstrike_attacker_,
           Thousand_sons8edPlanetstrike_defender_,
           Space_wolves8edPlanetstrike_attacker_,
           Space_wolves8edPlanetstrike_defender_,
           Tzeentch8edPlanetstrike_attacker_, Tzeentch8edPlanetstrike_defender_,
           Drukhari8edPlanetstrike_attacker_, Drukhari8edPlanetstrike_defender_,
           Heretic_astartes8edPlanetstrike_attacker_,
           Heretic_astartes8edPlanetstrike_defender_,
           _legion_8edPlanetstrike_attacker_, _legion_8edPlanetstrike_defender_,
           Dark_angels8edPlanetstrike_attacker_,
           Dark_angels8edPlanetstrike_defender_, Khorne8edPlanetstrike_attacker_,
           Khorne8edPlanetstrike_defender_,
           _forge_world_8edPlanetstrike_attacker_,
           _forge_world_8edPlanetstrike_defender_,
           Necrons8edPlanetstrike_attacker_, Necrons8edPlanetstrike_defender_,
           Adeptus_ministorum8edPlanetstrike_attacker_,
           Adeptus_ministorum8edPlanetstrike_defender_,
           Imperium8edPlanetstrike_attacker_, Imperium8edPlanetstrike_defender_,
           Chaos8edPlanetstrike_attacker_, Chaos8edPlanetstrike_defender_,
           Tyranids8edPlanetstrike_attacker_, Tyranids8edPlanetstrike_defender_,
           T_au_empire8edPlanetstrike_attacker_,
           T_au_empire8edPlanetstrike_defender_,
           Adepta_sororitas8edPlanetstrike_attacker_,
           Adepta_sororitas8edPlanetstrike_defender_,
           _dynasty_8edPlanetstrike_attacker_,
           _dynasty_8edPlanetstrike_defender_, Aeldari8edPlanetstrike_attacker_,
           Aeldari8edPlanetstrike_defender_,
           _hive_fleet_8edPlanetstrike_attacker_,
           _hive_fleet_8edPlanetstrike_defender_,
           _chapter_8edPlanetstrike_attacker_,
           _chapter_8edPlanetstrike_defender_,
           Death_guard8edPlanetstrike_attacker_,
           Death_guard8edPlanetstrike_defender_,
           Harlequins8edPlanetstrike_attacker_,
           Harlequins8edPlanetstrike_defender_, Ork8edPlanetstrike_attacker_,
           Ork8edPlanetstrike_defender_,
           Genestealer_cults8edPlanetstrike_attacker_,
           Genestealer_cults8edPlanetstrike_defender_,
           _craftworld_8edPlanetstrike_attacker_,
           _craftworld_8edPlanetstrike_defender_,
           _kabal_8edPlanetstrike_defender_, _clan_8edPlanetstrike_attacker_,
           _clan_8edPlanetstrike_defender_,
           Adeptus_astartes8edPlanetstrike_attacker_,
           Adeptus_astartes8edPlanetstrike_defender_,
           Cult_mechanicus8edPlanetstrike_defender_,
           Ynnari8edPlanetstrike_attacker_, Ynnari8edPlanetstrike_defender_,
           Nurgle8edPlanetstrike_attacker_, Nurgle8edPlanetstrike_defender_,
           _haemunculus_coven_8edPlanetstrike_defender_,
           _order_8edPlanetstrike_attacker_, _order_8edPlanetstrike_defender_,
           Adeptus_custodes8edPlanetstrike_attacker_,
           Adeptus_custodes8edPlanetstrike_defender_,
           Aspect_warrior8edPlanetstrike_attacker_,
           Aspect_warrior8edPlanetstrike_defender_,
           Grey_knights8edPlanetstrike_attacker_,
           Grey_knights8edPlanetstrike_defender_,
           Daemon8edPlanetstrike_attacker_, Daemon8edPlanetstrike_defender_,
           Adeptus_mechanicus8edPlanetstrike_attacker_,
           Adeptus_mechanicus8edPlanetstrike_defender_,
           Warhost8edPlanetstrike_attacker_, Warhost8edPlanetstrike_defender_,
           _wych_cult_8edPlanetstrike_attacker_,
           Elucidian_starstriders_8ed]
