from builder.games.wh40k8ed.unit import HqUnit, ElitesUnit, TroopsUnit, Unit, FastUnit
from builder.games.wh40k8ed.options import UnitDescription, Gear
from . import points
from builder.games.wh40k8ed.utils import *


class InfectedUnit(Unit):
    faction = ['Chaos', 'Gellerpox Infected']
    keywords = ['Nurgle']

class Vulgrar(HqUnit, InfectedUnit):
    type_name = get_name(points.Vulgrar)
    type_id = 'vulgrar_v1'
    keywords = ['Character', 'Infantry', 'Nurgle', 'Twisted Lord']
    power = 5

    def __init__(self, parent):
        super(Vulgrar, self).__init__(parent, points=get_cost(points.Vulgrar),
                                      static=True, unique=True,
                                      gear=[Gear('Fleshripper claws'),
                                            Gear('Belly flamer')])

    def build_statistics(self):
        res = super(Vulgrar, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 3
        return res


class VoxShamblers(TroopsUnit, InfectedUnit):
    type_name = get_name(points.VoxShamblers)
    type_id = 'vox_shamblers_v1'
    keywords = ['Infantry', 'Gellerpox Mutants']
    power = 2

    def __init__(self, parent):
        super(VoxShamblers, self).__init__(
            parent, points=get_cost(points.VoxShamblers) * 3,
            static=True, unique=True,
            gear=[
                UnitDescription('Gellerpox Mutant', count=3, options=[
                    Gear('Mutated limbs and improvised weapons'),
                    Gear('Frag grenades')])
            ])

    def get_count(self):
        return 3


class Glitchlings(TroopsUnit, InfectedUnit):
    type_name = get_name(points.Glitchlings)
    type_id = 'glitchlings_v1'
    keywords = ['Infantry', 'Daemon']
    power = 2

    def __init__(self, parent):
        super(Glitchlings, self).__init__(
            parent, points=get_cost(points.Glitchlings) * 4,
            static=True, unique=True,
            gear=[
                UnitDescription('Glitchling', count=4, options=[
                    Gear('Diseased claws and fangs')
                ])
            ])

    def get_count(self):
        return 4


class Hullbreakers(ElitesUnit, InfectedUnit):
    type_name = get_name(points.Hullbreakers)
    type_id = 'hullbreakers_v1'
    keywords = ['Infantry', 'Nightmare Hulk']
    power = 6

    def __init__(self, parent):
        super(Hullbreakers, self).__init__(
            parent, points=get_cost(points.Hullbreakers) * 3,
            static=True, unique=True,
            gear=[
                UnitDescription('Gnasher-screamer', count=1, options=[
                    Gear('Hideous mutations'), Gear('Plague cleaver')
                ]),
                UnitDescription('Hullbreaker', count=2, options=[
                    Gear('Hideous mutations')
                ])
            ])

    def get_count(self):
        return 3


class Glitchlings(TroopsUnit, InfectedUnit):
    type_name = get_name(points.Glitchlings)
    type_id = 'glitchlings_v1'
    keywords = ['Infantry', 'Daemon']
    power = 2

    def __init__(self, parent):
        super(Glitchlings, self).__init__(
            parent, points=get_cost(points.Glitchlings) * 4,
            static=True, unique=True,
            gear=[
                UnitDescription('Glitchling', count=4, options=[
                    Gear('Diseased claws and fangs')
                ])
            ])

    def get_count(self):
        return 4


class EyestingerSwarms(FastUnit, InfectedUnit):
    type_name = get_name(points.EyestingerSwarm)
    type_id = 'eyestingers_v1'
    keywords = ['Swarm', 'Fly', 'Mutoid vermin']
    power = 1

    def __init__(self, parent):
        super(EyestingerSwarms, self).__init__(
            parent, points=get_cost(points.EyestingerSwarm) * 4,
            static=True, unique=True,
            gear=[
                UnitDescription('Eyestinger swarm', count=4, options=[
                    Gear('Spawning barb')
                ])
            ])

    def get_count(self):
        return 4


class Cursemites(FastUnit, InfectedUnit):
    type_name = get_name(points.Cursemites)
    type_id = 'cursemites_v1'
    keywords = ['Beast', 'Mutoid vermin']
    power = 1

    def __init__(self, parent):
        super(Cursemites, self).__init__(
            parent, points=get_cost(points.Cursemites) * 4,
            static=True, unique=True,
            gear=[
                UnitDescription('Cursemite', count=4, options=[
                    Gear('Bloodsucking proboscis')
                ])
            ])

    def get_count(self):
        return 4


class SludgeGrubs(FastUnit, InfectedUnit):
    type_name = get_name(points.SludgeGrubs)
    type_id = 'eyestingers_v1'
    keywords = ['Beast', 'Mutoid vermin']
    power = 1

    def __init__(self, parent):
        super(SludgeGrubs, self).__init__(
            parent, points=get_cost(points.SludgeGrubs) * 4,
            static=True, unique=True,
            gear=[
                UnitDescription('Sludge-grub', count=4, options=[
                    Gear('Fanged maw and stinger'), Gear('Acid spit')
                ])
            ])

    def get_count(self):
        return 4
