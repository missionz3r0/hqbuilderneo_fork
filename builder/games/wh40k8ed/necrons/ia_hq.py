__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import Gear
from builder.games.wh40k8ed.utils import *
from . import armory, ia_units, ia_ranged, ia_melee


class Kutlakh(HqUnit, armory.NecronUnit):
    type_name = get_name(ia_units.Kutlakh) + ' (Imperial Armour)'
    type_id = 'kutlakh_v1'
    faction = ['Maynarkh']
    keywords = ['Character', 'Infantry', 'Overlord']
    power = 10

    def __init__(self, parent):
        super(Kutlakh, self).__init__(parent, get_name(ia_units.Kutlakh),
                                      points=get_cost(ia_units.Kutlakh),
                                      gear=[Gear('Obsidiax'), Gear('Staff of light')],
                                      static=True, unique=True)
        


class Toholk(HqUnit, armory.NecronUnit):
    type_name = get_name(ia_units.Toholk) + ' (Imperial Armour)'
    type_id = 'toholk_v1'
    faction = ['Maynarkh']
    keywords = ['Character', 'Infantry', 'Cryptek']
    power = 8

    def __init__(self, parent):
        super(Toholk, self).__init__(parent, get_name(ia_units.Toholk),
                                     points=get_cost(ia_units.Toholk),
                                     gear=[Gear('Aeonstave'), Gear('Transdimensional beamer')],
                                      static=True, unique=True)
