__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit
from builder.games.wh40k8ed.options import OneOf, Gear, UnitList,\
    UnitDescription, SubUnit, Count, ListSubUnit, OptionsList
from . import armory, units, wargear, ranged
from builder.games.wh40k8ed.utils import *


class HeavyDestroyers(HeavyUnit, armory.DynastyUnit):
    type_name = get_name(units.HeavyDestroyers)
    type_id = 'hdestroyers_v1'
    keywords = ['Infantry', 'Fly']
    power = 3

    def __init__(self, parent):
        super(HeavyDestroyers, self).__init__(parent)
        self.models = Count(self, 'Heavy Destroyer', 1, 3,
                            points_price(get_cost(units.HeavyDestroyers), ranged.HeavyGaussCannon), True,
                            gear=UnitDescription('Heavy Destroyer',
                                                 options=[Gear('Heavy gauss cannon')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.models.cur


class Spyders(HeavyUnit, armory.DynastyUnit):
    type_name = get_name(units.CanoptekSpyders)
    type_id = 'spyders_v1'
    faction = ['Canoptek']
    keywords = ['Monster']
    power = 4

    class Spyder(ListSubUnit):
        type_name = 'Canoptek Spyder'

        class Options(OptionsList):
            def __init__(self, parent):
                super(Spyders.Spyder.Options, self).__init__(parent, 'Options')
                self.variant(*wargear.FabricatorClawArray)
                self.variant(*wargear.GloomPrism)
                self.variant('Two particle beamers', 2 * get_cost(ranged.ParticleBeamer),
                             gear=[Gear('Particle beamer', count=2)])

        def __init__(self, parent):
            super(Spyders.Spyder, self).__init__(parent, points=get_cost(units.CanoptekSpyders))
            self.Options(self)

    def __init__(self, parent):
        super(Spyders, self).__init__(parent)
        self.models = UnitList(self, self.Spyder, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class Monolith(HeavyUnit, armory.DynastyUnit):
    type_name = get_name(units.Monolith)
    type_id = 'monolith_v1'

    keywords = ['Vehicle', 'Titanic', 'Fly']
    power = 19

    def __init__(self, parent):
        super(Monolith, self).__init__(parent, points=get_cost(units.Monolith),
                                       gear=[
                                           Gear('Particle whip'),
                                           Gear('Gauss flux arc', count=4)],
                                       static=True)


class AnnihilationBarge(HeavyUnit, armory.DynastyUnit):
    type_name = get_name(units.AnnihilationBarge)
    type_id = 'annihilation_barge_v1'
    keywords = ['Vehicle', 'Fly']

    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(AnnihilationBarge.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.TeslaCannon)
            self.variant(*ranged.GaussCannon)

    def __init__(self, parent):
        super(AnnihilationBarge, self).__init__(parent, points=get_cost(units.AnnihilationBarge),
                                                gear=[Gear('Twin tesla destructor')])
        self.Weapon(self)


class DoomsdayArk(HeavyUnit, armory.DynastyUnit):
    type_name = get_name(units.DoomsdayArk)
    type_id = 'doomsday_v1'

    keywords = ['Vehicle', 'Fly']
    power = 10

    def __init__(self, parent):
        super(DoomsdayArk, self).__init__(parent, points=get_cost(units.DoomsdayArk),
                                          gear=[
                                              Gear('Doomsday cannon'),
                                              Gear('Gauss flayer array', count=2)],
                                          static=True)


class TranscendentCtan(HeavyUnit, armory.NecronUnit):
    type_name = get_name(units.TCtan)
    type_id = 'transcend_v1'

    faction = ["C'tan Shards"]
    keywords = ['Monster', 'Character', 'Fly']
    power = 12

    def __init__(self, parent):
        super(TranscendentCtan, self).__init__(parent, points=get_cost(units.TCtan),
                                               gear=[Gear('Crackling tendrils')],
                                               static=True)
