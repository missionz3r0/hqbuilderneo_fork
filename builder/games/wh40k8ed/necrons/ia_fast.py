__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, FastUnit
from builder.games.wh40k8ed.options import OptionsList, Count, UnitDescription
from . import armory, ia_units, ia_wargear, ia_ranged, ia_melee
from builder.games.wh40k8ed.utils import *


class TombSentinel(FastUnit, armory.DynastyUnit):
    type_name = get_name(ia_units.CanoptekTombSentinel) + ' (Imperial Armour)'
    type_id = 'tomb_sentinel_v1'
    faction = ['Canoptek']
    keywords = ['Monster']
    power = 9

    class Options(OptionsList):
        def __init__(self, parent):
            super(TombSentinel.Options, self).__init__(parent, 'Options')
            self.variant(*ia_wargear.GloomPrism)

    def __init__(self, parent):
        gear = [ia_ranged.ExileCannon, ia_melee.AutomatonClaws]
        cost = points_price(get_cost(ia_units.CanoptekTombSentinel), *gear)
        super(TombSentinel, self).__init__(parent, get_name(ia_units.CanoptekTombSentinel),
                                           cost, gear=create_gears(*gear))
        self.Options(self)


class Acanthrites(FastUnit, armory.DynastyUnit):
    type_name = get_name(ia_units.CanoptekAcanthrites) + ' (Imperial Armour)'
    type_id = 'acanthrites_v1'
    faction = ['Canoptek']
    keywords = ['Beasts', 'Fly']
    power = 9

    def __init__(self, parent):
        super(Acanthrites, self).__init__(parent, get_name(ia_units.CanoptekAcanthrites))
        gear = [ia_ranged.CuttingBeam, ia_melee.Voidblade]
        cost = points_price(get_cost(ia_units.CanoptekAcanthrites), *gear)
        model = UnitDescription(get_name(ia_units.CanoptekAcanthrites), options=create_gears(*gear))
        self.models = Count(self, 'Canoptek Acanthrites', 3, 9, cost, True,
                            gear=model.clone())

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * ((2 + self.models.cur) / 3)
