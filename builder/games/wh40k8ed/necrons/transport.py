__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import Gear
from builder.games.wh40k8ed.utils import *
from . import armory, units


class GhostArk(TransportUnit, armory.DynastyUnit):
    type_name = get_name(units.GhostArk)
    type_id = 'ghost_ark_v1'

    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 8

    def __init__(self, parent):
        super(GhostArk, self).__init__(parent, points=get_cost(units.GhostArk),
                                       gear=[
                                           Gear('Gauss flayer array', count=2)],
                                       static=True)
