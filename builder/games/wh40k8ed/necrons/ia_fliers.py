__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import Gear
from builder.games.wh40k8ed.utils import *
from . import armory, ia_units


class NightShroud(FlierUnit, armory.DynastyUnit):
    type_name = get_name(ia_units.NightShroud) + ' (Imperial Armour)'
    type_id = 'night_shroud_v1'

    keywords = ['Vehicle', 'Fly']
    power = 13

    def __init__(self, parent):
        super(NightShroud, self).__init__(parent, name=get_name(ia_units.NightShroud),
                                          points=get_cost(ia_units.NightShroud),
                                          gear=[Gear('Twin tesla destructor')],
                                          static=True)
