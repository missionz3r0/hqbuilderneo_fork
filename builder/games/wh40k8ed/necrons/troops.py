__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, Count
from . import armory, units, wargear, ranged
from builder.games.wh40k8ed.utils import *


class Warriors(TroopsUnit, armory.DynastyUnit):
    type_name = get_name(units.Warriors)
    type_id = 'warriors_v1'
    keywords = ['Infantry']
    power = 6

    def __init__(self, parent):
        super(Warriors, self).__init__(parent)
        self.models = Count(self, 'Necron Warrior', 10, 20, get_cost(units.Warriors), True,
                            gear=UnitDescription('Necron Warrior',
                                                 options=[Gear('Gauss flayer')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 10))


class Immortals(TroopsUnit, armory.DynastyUnit):
    type_name = get_name(units.Immortals)
    type_id = 'immortals_v1'
    keywords = ['Infantry']
    power = 4

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Immortals.Weapon, self).__init__(parent, 'Weapon')
            # since prices are same, just add them to the model cose
            self.variant(get_name(ranged.GaussBlaster))
            self.variant(get_name(ranged.TeslaCarbine))

    def __init__(self, parent):
        super(Immortals, self).__init__(parent)
        self.models = Count(self, 'Immortal', 5, 10,
                            points_price(get_cost(units.Immortals), ranged.TeslaCarbine), True)
        self.wep = self.Weapon(self)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Immortal', 0,
                                self.models.cur,
                                self.wep.description)
        desc.add(model)
        return desc

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 5))
