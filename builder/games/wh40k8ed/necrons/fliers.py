__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import Gear
from builder.games.wh40k8ed.utils import *
from . import armory, units


class NightScythe(FlierUnit, armory.DynastyUnit):
    type_name = get_name(units.NightScythe)
    type_id = 'night_scythe_v1'

    keywords = ['Vehicle', 'Fly']
    power = 8

    def __init__(self, parent):
        super(NightScythe, self).__init__(parent, points=get_cost(units.NightScythe),
                                          gear=[
                                              Gear('Tesla destructor', count=2)],
                                          static=True)


class DoomScythe(FlierUnit, armory.DynastyUnit):
    type_name = get_name(units.DoomScythe)
    type_id = 'doom_scythe_v1'

    keywords = ['Vehicle', 'Fly']
    power = 11

    def __init__(self, parent):
        super(DoomScythe, self).__init__(parent, points=get_cost(units.DoomScythe),
                                         gear=[
                                             Gear('Death Ray'),
                                             Gear('Tesla desctuctor', count=2)],
                                         static=True)
