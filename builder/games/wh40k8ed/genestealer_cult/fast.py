__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, UnitList,\
    UnitDescription, OptionsList, Count, SubUnit, ListSubUnit,\
    OptionalSubUnit
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Squadron(FastUnit, armory.Unit):
    unit_class = None
    unit_min = 1
    unit_max = 3
    keywords = ['Vehicle']

    def __init__(self, parent):
        super(Squadron, self).__init__(parent)
        self.tanks = UnitList(self, self.unit_class, self.unit_min, self.unit_max)

    def get_count(self):
        return self.tanks.count

    def build_power(self):
        return self.power * self.get_count()


class ScoutSentinel(ListSubUnit):
    type_name = 'Cult Scout Sentinel'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Scout-Sentinel-Squadron')

    def __init__(self, parent):
        super(ScoutSentinel, self).__init__(parent=parent, points=get_cost(units.CultScoutSentinels))
        self.Weapon(self)
        self.Weapon2(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScoutSentinel.Weapon, self).__init__(parent=parent, name='Weapon')
            self.multilaser = self.variant(*ranged.MultiLaser)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)
            self.autocannon = self.variant(*ranged.Autocannon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)
            self.lascannon = self.variant(*ranged.Lascannon)

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(ScoutSentinel.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*melee.SentinelChainsaw)
    

class ArmouredSentinel(ListSubUnit):
    type_name = 'Cult Armoured Sentinel'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Armoured-Sentinel-Squadron')

    def __init__(self, parent):
        super(ArmouredSentinel, self).__init__(parent=parent, points=get_cost(units.CultArmouredSentinels))
        self.Weapon(self)
        ScoutSentinel.Weapon2(self)

    class Weapon(ScoutSentinel.Weapon):
        def __init__(self, parent):
            super(ArmouredSentinel.Weapon, self).__init__(parent=parent)
            self.plasmacannon = self.variant(*ranged.PlasmaCannon)


class AchillesRidgerunner(ListSubUnit):
    type_name = get_name(units.AchillesRidgerunner)

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(AchillesRidgerunner.HeavyWeapon, self).__init__(parent, 'Heavy weapon')
            self.variant(*ranged.HeavyMiningLaser)
            self.variant(*ranged.MissileLauncher)
            self.variant(*ranged.HeavyMortar)

    class Options(OneOf):
        def __init__(self, parent):
            super(AchillesRidgerunner.Options, self).__init__(parent, 'Scout gear')
            self.variant(*wargear.FlareLauncher)
            self.variant(*wargear.SurveyAugur)
            self.variant(*wargear.Spotter)

    def __init__(self, parent):
        gear = [ranged.HeavyStubber] * 2
        super(AchillesRidgerunner, self).__init__(parent,
                                                  points=points_price(get_cost(units.AchillesRidgerunner), *gear),
                                                  gear=create_gears(*gear))
        self.HeavyWeapon(self)
        self.Options(self)


class RidgerunnerSquadron(Squadron, armory.CultUnit):
    type_name = 'Achilles Ridgerunners'
    power = 4
    type_id = 'gs_achilles_v1'
    unit_class = AchillesRidgerunner

    
class CultScoutSentinelSquad(Squadron, armory.BroodUnit):
    type_name = get_name(units.CultScoutSentinels)
    type_id = 'gs_scout_sentinel_squad_v1'
    unit_class = ScoutSentinel
    power = 3


class CultArmouredSentinelSquad(Squadron, armory.BroodUnit):
    type_name = get_name(units.CultArmouredSentinels)
    type_id = 'gs_armoured_sentinel_squad_v1'
    unit_class = ArmouredSentinel
    power = 3


class AtalanJackals(FastUnit, armory.CultUnit):
    type_name = get_name(units.AtalanJackals)
    type_id = 'gs_atalan_jackals_v1'
    power = 3
    keywords = ['Bikers']

    class AtalanWeapon(OneOf):
        def __init__(self, parent, first, leader=False):
            super(AtalanJackals.AtalanWeapon, self).__init__(parent, 'Weapon' if first else '')
            armory.add_atalan_weapons(self, not first, leader)

        def check_other(self, other):
            return self.cur.title == other.cur.title

    class Leader(armory.Unit):
        type_name = 'Atalan Leader'

        def __init__(self, parent):
            gear = [ranged.Autopistol, ranged.BlastingCharge]
            super(AtalanJackals.Leader, self).__init__(parent, points=points_price(get_cost(units.AtalanJackals), *gear),
                                                       gear=create_gears(*gear))
            self.wep1 = AtalanJackals.AtalanWeapon(self, True, True)
            self.wep2 = AtalanJackals.AtalanWeapon(self, False, True)

        def check_rules(self):
            super(AtalanJackals.Leader, self).check_rules()
            if self.wep1.check_other(self.wep2):
                self.error('Same model cannot take the same weapon twice')

        def has_gl(self):
            return self.wep1.cur == self.wep1.gl or self.wep2.cur == self.wep2.gl

    class Jackal(ListSubUnit):
        type_name = 'Atalan Jackal'    

        def __init__(self, parent):
            gear = [ranged.Autopistol, ranged.BlastingCharge]
            super(AtalanJackals.Jackal, self).__init__(parent, points=points_price(get_cost(units.AtalanJackals), *gear),
                                                       gear=create_gears(*gear))
            self.wep1 = AtalanJackals.AtalanWeapon(self, True)
            self.wep2 = AtalanJackals.AtalanWeapon(self, False)

        def check_rules(self):
            super(AtalanJackals.Jackal, self).check_rules()
            if self.wep1.check_other(self.wep2):
                self.error('Same model cannot take the same weapon twice')

        @ListSubUnit.count_gear
        def has_gl(self):
            return self.wep1.cur == self.wep1.gl or self.wep2.cur == self.wep2.gl

    class Wolfquad(ListSubUnit):
        type_name = get_name(units.AtalanWolfquad)
        power = 2

        class Weapon(OneOf):
            def __init__(self, parent):
                super(AtalanJackals.Wolfquad.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.HeavyStubber)
                self.variant(*ranged.MiningLaser)
                self.variant(*ranged.AtalanIncinerator)

        class Weapon2(OptionsList):
            def __init__(self, parent):
                super(AtalanJackals.Wolfquad.Weapon2, self).__init__(parent, 'Optional weapon', limit=1)
                self.variant(*ranged.Autopistol)
                self.variant(*ranged.Shotgun)
                self.variant(*melee.ImprovisedWeapon)
                self.variant(*melee.PowerPick)

        def __init__(self, parent):
            gear = [ranged.Autopistol, ranged.BlastingCharge]
            super(AtalanJackals.Wolfquad, self).__init__(parent, points=points_price(get_cost(units.AtalanWolfquad), *gear),
                                                         gear=create_gears(*gear))
            self.Weapon(self)
            self.Weapon2(self)

    class Quads(OptionalSubUnit):
        def __init__(self, parent):
            super(AtalanJackals.Quads, self).__init__(parent, 'ATVs')
            self.quads = UnitList(self, AtalanJackals.Wolfquad, 1, 3)

    def __init__(self, parent):
        super(AtalanJackals, self).__init__(parent)
        self.ldr = SubUnit(self, AtalanJackals.Leader(self))
        self.bikes = UnitList(self, self.Jackal, 3, 11)
        self.rest = self.Quads(self)

    def get_count(self):
        return 1 + self.bikes.count

    def build_power(self):
        return self.power * (1 + (self.bikes.count + 1) / 4) +\
            self.Wolfquad.power * self.rest.count

    def check_rules(self):
        super(AtalanJackals, self).check_rules()
        gl_count = self.ldr.unit.has_gl() + sum(u.has_gl() for u in self.bikes.units)
        if gl_count > (self.get_count() / 4):
            self.error('Only oe in four biker models may carry a grenade laauncher')
        if self.rest.count > self.get_count() / 4:
            self.error("No more then one Atalan Wolfquad may be taken per 4 bikers")
