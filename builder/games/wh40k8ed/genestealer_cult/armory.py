__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList, Count, Gear
from . import melee, ranged, wargear


def add_special_weapons(instance):
    instance.special = [
        instance.variant(*ranged.Flamer),
        instance.variant(*ranged.GrenadeLauncher),
        instance.variant(*ranged.Webber)
    ]


def add_pistols(instance):
    instance.variant(*ranged.BoltPistol)
    instance.variant(*ranged.Laspistol)
    instance.variant(*ranged.WebPistol)


def add_melee_weapons(instance):
    instance.variant(*melee.Chainsword)
    instance.variant(*melee.PowerMaul)
    instance.variant(*melee.PowerPick)
    instance.variant(*melee.CultistKnife)


def add_mining_weapons(instance):
    instance.mining = [
        instance.variant(*ranged.HeavyStubber),
        instance.variant(*ranged.MiningLaser),
        instance.variant(*ranged.SeismicCannon)
    ]


def add_heavy_weapons(instance):
    instance.variant(*ranged.Autocannon)
    instance.variant(*ranged.HeavyBolter)
    instance.variant(*ranged.Lascannon)
    instance.variant(*ranged.Mortar)
    instance.variant(*ranged.MissileLauncher)


def add_atalan_weapons(instance, scramble=False, leader=False):
    if scramble:
        instance.variant(*ranged.Autogun)
        instance.variant(*melee.CultistKnife)
    else:
        instance.variant(*melee.CultistKnife)
        instance.variant(*ranged.Autogun)
    if leader:
        instance.variant(*ranged.Autopistol)
        instance.variant(*ranged.BoltPistol)
        instance.variant(*melee.PowerAxe)
    instance.variant(*ranged.DemolitionCharges)
    instance.gl = instance.variant(*ranged.GrenadeLauncher)
    instance.variant(*melee.ImprovisedWeapon)

    instance.variant(*melee.PowerHammer)
    instance.variant(*melee.PowerPick)
    instance.variant(*ranged.Shotgun)


class VehicleUpgrades(OptionsList):
    def __init__(self, parent):
        super(VehicleUpgrades, self).__init__(parent, 'Vehicle upgrades')
        self.variant(*wargear.AugurArray)
        self.variant(*wargear.DozerBlade)
        self.variant(*wargear.TrackGuards)


class CultUnit(Unit):
    faction = ['Tyranids', 'Genestealer Cults', '<Cult>']
    wiki_faction = "Genestealer Cults"

    @classmethod
    def calc_faction(cls):
        return ['CULT OF THE FOUR_ARMED EMPEROR', 'THE PAUPER PRINCES',
                'THE HIVECULT', 'THE BLADED COG', 'THE RUSTED CLAW',
                'THE TWISTED HELIX']


class BroodUnit(Unit):
    wiki_faction = "Genestealer Cults"
    faction = ['Tyranids', 'Genestealer Cults', 'Brood brothers']

    
