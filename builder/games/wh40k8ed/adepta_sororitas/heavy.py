__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import Gear, SubUnit, Count, OneOf, UnitDescription,\
     OptionsList
from . import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *
from .transport import ASRhino


class Exorcist(HeavyUnit, armory.SororitasUnit):
    type_name = get_name(units.Exorcist)

    type_id = 'exorcist_v1'
    keywords = ['Vehicle']

    power = 7

    def __init__(self, parent):
        gear = [ranged.ExorcistMissileLauncher]
        cost = points_price(get_cost(units.Exorcist), *gear)
        super(Exorcist, self).__init__(parent, points=cost, gear=create_gears(*gear))
        self.opt = ASRhino.Options(self)


class Retributors(HeavyUnit, armory.SororitasUnit):
    type_name = get_name(units.RetributorSquad)
    type_id = 'retributors_v1'
    kwname = 'Retributors'
    keywords = ['Infantry']

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    model = UnitDescription('Retributor', options=common_gear + [Gear('Bolt pistol')])

    class Superior(Unit):
        type_name = 'Retributor Superior'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Retributors.Superior.Weapon1, self).__init__(parent, 'Pistol')
                armory.add_pistol_options(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Retributors.Superior.Weapon2, self).__init__(parent, 'Weapon')
                armory.add_ranged_weapons(self)
                armory.add_melee_weapons(self)

        class MeleeWeapon(OptionsList):
            def __init__(self, parent):
                super(Retributors.Superior.MeleeWeapon, self).__init__(parent, 'Melee weapon', limit=1)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(Retributors.Superior, self).__init__(parent, points=get_cost(units.RetributorSquad),
                                                       gear=Retributors.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.MeleeWeapon(self)

    class Retributor(Count):
        @property
        def description(self):
            return Retributors.model.clone().add([Gear('Boltgun')]).\
                set_count(self.cur - sum(c.cur for c in self.parent.wep.counts))

    class HeavyWeapons(object):
        def variant(self, name, point=None):
            self.counts.append(Count(
                self.parent, name, 0, 4, point, gear=Retributors.model.clone().add(Gear(name))))

        def __init__(self, parent):
            self.parent = parent
            self.counts = []
            armory.add_heavy_weapons(self)
    
    def __init__(self, parent):
        super(Retributors, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=self))
        self.squad = self.Retributor(self, 'Retributor', 4, 9,
                                     points=get_cost(units.RetributorSquad))
        self.wep = self.HeavyWeapons(self)
        self.opt = armory.Options(self)

    def check_rules(self):
        super(Retributors, self).check_rules()
        Count.norm_counts(0, 4, self.wep.counts)

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 5 + 4 * (self.squad.cur > 4)


class PenitentEngines(HeavyUnit, armory.MinistorumUnit):
    type_name = get_name(units.PetinentEngines)
    type_id = 'penitengine_v1'

    keywords = ['Vehicle']

    def __init__(self, parent):
        super(PenitentEngines, self).__init__(parent)
        gear = [melee.PenitentBuzzBlades, ranged.HeavyFlamer,
                ranged.HeavyFlamer]
        cost = points_price(get_cost(units.PetinentEngines), *gear)
        self.models = Count(self, 'Petinent Engines', 1, 3,
                            per_model=True, points=cost,
                            gear=UnitDescription('Penitent Engine',
                                                 options=create_gears(*gear)))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 5 * self.models.cur
