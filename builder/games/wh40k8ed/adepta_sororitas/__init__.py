__author__ = 'Ivan Truskov'

from .hq import Celestine, Canoness, Jacobus, Missionary
from .troops import BattleSisters
from .fast import Dominions, Seraphims
from .elites import ArcoFlagellants, Assassins, Celestians, \
    Dialogus, Hospitaller, Imagifier, Mistress,\
    Priest, Repentia, Geminae, Preacher, ConclaveCrusaders
from .heavy import Exorcist, Retributors, PenitentEngines
from .transport import ASRhino, Immolator
from builder.games.wh40k8ed.sections import ElitesSection


class ConclaveElites(ElitesSection):
    conclave_keyword = 'ECCLESIARCHY BATTLE CONCLAVE'
    priest_keyword = 'MINISTORUM PRIEST'

    def __init__(self, *args, **kwargs):
        super(ConclaveElites, self).__init__(*args, **kwargs)
        self.conclave_types = []

    def unit_type_add(self, ut):
        if self.conclave_keyword in ut.keywords:
            self.conclave_types += [super(ConclaveElites, self).unit_type_add(ut)]
        else:
            super(ConclaveElites, self).unit_type_add(ut)

    def count_conclave(self):
        return sum(float(t.count) * float(t.slot) for t in self.conclave_types)

    def any_priests(self):
        return any(self.priest_keyword in unit.keywords for unit in self.units) or\
            any(self.priest_keyword in unit.keywords for unit in self.parent.hq.units)

    def count_slots(self):
        res = sum(float(t.count) * float(t.slot) for t in self.types if t not in self.conclave_types)
        res += 0 if self.any_priests() else self.count_conclave()
        return res

    def check_rules(self):
        super(ConclaveElites, self).check_rules()
        if self.count_conclave() > 1 and not self.any_priests():
            self.error("Detachment that does not include any Ministorum Priests can only include one Ecclesiarchy Battle Conclave unit")

unit_types = [Celestine, Canoness, Jacobus, BattleSisters, Dominions,
              Seraphims, ArcoFlagellants, Assassins, Celestians,
              Dialogus, Hospitaller, Imagifier, Mistress,
              Priest, Repentia, Exorcist, Retributors,
              PenitentEngines, ASRhino, Immolator, Missionary,
              Geminae, Preacher, ConclaveCrusaders]
