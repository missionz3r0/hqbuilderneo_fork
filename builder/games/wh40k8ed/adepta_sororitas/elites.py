__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    SubUnit, UnitDescription, Count
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Priest(ElitesUnit, armory.MinistorumUnit):
    type_name = get_name(units.MinistorumPriest) + ' (Index)'
    type_id = 'priest_v1'
    faction = ['Astra Militarum']
    keywords = ['Character', 'Infantry']

    power = 3

    @classmethod
    def check_faction(cls, fac):
        return super(Priest, cls).check_faction(fac) or fac in ['TYRANIDS', 'GENESTEALER CULTS']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Priest.Weapon1, self).__init__(parent, 'Pistol')
            self.variant(*ranged.Laspistol)
            armory.add_pistol_options(self)

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(Priest.Weapon2, self).__init__(parent, 'Weapon', limit=1)
            self.variant(*melee.Eviscerator)
            self.variant(*ranged.Autogun)
            self.variant(*ranged.PlasmaGun)
            self.variant(*ranged.Shotgun)
            armory.add_melee_weapons(self)
            armory.add_ranged_weapons(self)

    def __init__(self, parent):
        super(Priest, self).__init__(parent, 'Militarum Priest', points=get_cost(units.MinistorumPriest),
                                     gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class Celestians(ElitesUnit, armory.SororitasUnit):
    type_name = get_name(units.CelestianSquad)
    type_id = 'celestians_v1'
    kwname = 'Celestians'
    keywords = ['Infantry']

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    model = UnitDescription('Celestian', options=common_gear + [Gear('Bolt pistol')], points=11)

    class Superior(Unit):
        type_name = 'Celestian Superior'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Celestians.Superior.Weapon1, self).__init__(parent, 'Pistol')
                armory.add_pistol_options(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Celestians.Superior.Weapon2, self).__init__(parent, 'Weapon')
                armory.add_ranged_weapons(self)
                armory.add_melee_weapons(self)

        class MeleeWeapon(OptionsList):
            def __init__(self, parent):
                super(Celestians.Superior.MeleeWeapon, self).__init__(parent, 'Melee weapon', limit=1)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(Celestians.Superior, self).__init__(parent, points=get_cost(units.CelestianSquad),
                                                      gear=Celestians.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.MeleeWeapon(self)

    class Celestian(Count):
        @property
        def description(self):
            return Celestians.model.clone().add([Gear('Boltgun')]). \
                set_count(self.cur - sum(c.cur != c.bolt for c in [self.parent.wep1, self.parent.wep2]))

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(Celestians.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_special_weapons(self)

        @property
        def description(self):
            return []

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(Celestians.HeavyWeapon, self).__init__(parent, 'Special or heavy weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_special_weapons(self)
            armory.add_heavy_weapons(self)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(Celestians, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=self))
        self.squad = self.Celestian(self, 'Celestian', 4, 9, per_model=True,
                                    points=get_cost(units.CelestianSquad))
        self.wep1 = self.SpecialWeapon(self)
        self.wep2 = self.HeavyWeapon(self)
        self.opt = armory.Options(self)

    def build_description(self):
        res = super(Celestians, self).build_description()
        res.add_dup(Celestians.model.clone().add(self.wep1.cur.gear))
        res.add_dup(Celestians.model.clone().add(self.wep2.cur.gear))
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 5 + 2 * (self.squad.cur > 4)


class Mistress(ElitesUnit, armory.SororitasUnit):
    type_name = get_name(units.Mistress)
    type_id = 'mistress_v1'

    keywords = ['Character', 'Infantry']
    power = 2

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.NeuralWhips]
        cost = points_price(get_cost(units.Mistress), *gear)
        super(Mistress, self).__init__(parent, gear=create_gears(*gear),
                                       static=True, points=cost)


class Repentia(ElitesUnit, armory.SororitasUnit):
    type_name = get_name(units.RepentiaSquad)
    type_id = 'repentia_v1'
    kwname = 'Repentia'

    keywords = ['Infantry']

    def __init__(self, parent):
        super(Repentia, self).__init__(parent)
        gear = [melee.PenitentEviscerator]
        cost = points_price(get_cost(units.RepentiaSquad), *gear)
        self.models = Count(self, 'Sister Repentia', 3, 9, per_model=True, points=cost,
                            gear=UnitDescription('Sister Repentia', options=create_gears(*gear)))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 2 * ((self.models.cur + 2) / 3)


class Assassins(ElitesUnit, armory.MinistorumUnit):
    type_name = get_name(units.DeathCultAssassins)
    type_id = 'dc_assassin_v1'

    keywords = ['Infantry', 'Ecclesiarchy Battle Conclave']

    def __init__(self, parent):
        super(Assassins, self).__init__(parent)
        gear = [melee.DeathCultPowerBlades]
        cost = points_price(get_cost(units.DeathCultAssassins), *gear)
        self.models = Count(self, 'Death Cult Assassins', 2, 10, per_model=True, points=cost,
                            gear=UnitDescription(get_name(units.DeathCultAssassins), options=create_gears(*gear)))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return (self.models.cur + 1) / 2


class ArcoFlagellants(ElitesUnit, armory.MinistorumUnit):
    type_name = get_name(units.ArcoFlagellants)
    type_id = 'aflag_v1'

    keywords = ['Infantry', 'Ecclesiarchy Battle Conclave']

    def __init__(self, parent):
        super(ArcoFlagellants, self).__init__(parent)
        gear = [melee.ArcoFlails]
        cost = points_price(get_cost(units.ArcoFlagellants), *gear)
        self.models = Count(self, 'Arco-flagellant', 3, 9, per_model=True, points=cost,
                            gear=UnitDescription(get_name(units.ArcoFlagellants), options=create_gears(*gear)))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 2 * ((self.models.cur + 2) / 3)


class Imagifier(ElitesUnit, armory.SororitasUnit):
    type_name = get_name(units.Imagifier)
    type_id = 'imagifier_v1'

    keywords = ['Character', 'Infantry']
    power = 2

    def __init__(self, parent):
        super(Imagifier, self).__init__(parent, points=get_cost(units.Imagifier), gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Bolt pistol'), Gear('Boltgun')
        ], static=True)


class Hospitaller(ElitesUnit, armory.SororitasUnit):
    type_name = get_name(units.Hospitaller)
    type_id = 'hospitaller_v1'

    keywords = ['Character', 'Infantry']
    power = 2

    def __init__(self, parent):
        super(Hospitaller, self).__init__(parent, points=get_cost(units.Hospitaller), gear=[
            Gear("Chirugeon's tools")
        ], static=True)


class Dialogus(ElitesUnit, armory.SororitasUnit):
    type_name = get_name(units.Dialogus)
    type_id = 'dialogus_v1'

    keywords = ['Character', 'Infantry']
    power = 2

    def __init__(self, parent):
        gear = [melee.DialogusStaff]
        cost = points_price(get_cost(units.Dialogus), *gear)
        super(Dialogus, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True)


class Geminae(ElitesUnit, armory.MinistorumUnit):
    type_name = get_name(units.Geminae)
    type_id = 'geminae_v1'
    faction = ['Adepta Sororitas']
    keywords = ['Character', 'Infantry', 'Jump Pack', 'Fly']

    def __init__(self, parent):
        super(Geminae, self).__init__(parent, unique=True)
        self.babes = Count(self, 'Geminae Superia', 1, 2, per_model=True,
                           points=get_cost(units.Geminae),
                           gear=UnitDescription('Geminae Superia', options=[
                               Gear('Frag grenades'), Gear('Krak grenades'),
                               Gear('Bolt pistol'), Gear('Power sword')]))

    def build_power(self):
        return 2 * self.babes.cur


class Preacher(ElitesUnit, armory.MinistorumUnit):
    type_name = get_name(units.Preacher)
    type_id = 'preacher_v1'
    keywords = ['Character', 'Infantry', 'Ministorum Priest']

    power = 2

    def __init__(self, parent):
        gear = [ranged.Laspistol, melee.Chainsword]
        cost = points_price(get_cost(units.Preacher), *gear)
        super(Preacher, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True)


class ConclaveCrusaders(ElitesUnit, armory.MinistorumUnit):
    type_name = get_name(units.Crusaders) + ' (Ecclesiarchy battle conclave)'
    type_id = 'as_crusaders_v1'
    keywords = ['Infantry', 'Ecclesiarchy Battle Conclave']

    power = 1

    def __init__(self, parent):
        super(ConclaveCrusaders, self).__init__(parent, name=get_name(units.Crusaders))
        gear = [wargear.StormShield, melee.PowerSword]
        cost = points_price(get_cost(units.Crusaders), *gear)
        self.models = Count(self, 'Crusaders', 2, 10, per_model=True, points=cost,
                            gear=UnitDescription('Crusader', options=create_gears(*gear)))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power + (self.models.cur - 1) / 2
