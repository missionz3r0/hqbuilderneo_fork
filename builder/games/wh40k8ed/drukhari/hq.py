__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, SubUnit, OptionsList, Count
from . import armory, melee, ranged, units, wargear
from builder.games.wh40k8ed.utils import *


class Archon(HqUnit, armory.KabalUnit):
    type_name = get_name(units.Archon)
    type_id = 'archon_v1'

    keywords = ['Character', 'Infantry']
    power = 4

    class WeaponMelee(OneOf):
        def __init__(self, parent):
            super(Archon.WeaponMelee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Huskblade)
            self.variant(*melee.PowerSword)
            self.variant(*melee.Agoniser)
            self.variant(*melee.VenomBlade)

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(Archon.WeaponRanged, self).\
                __init__(parent, name='Ranged Weapom')
            self.variant(*ranged.SplinterPistol)
            self.variant(*ranged.BlastPistol)

    def __init__(self, parent):
        super(Archon, self).__init__(parent, get_name(units.Archon),
                                     points=get_cost(units.Archon))
        self.WeaponMelee(self)
        self.WeaponRanged(self)


class Succubus(HqUnit, armory.CultUnit):
    type_name = get_name(units.Succubus)
    type_id = 'succubus_v1'

    keywords = ['Character', 'Infantry']
    power = 4

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(Succubus.WeaponRanged, self).\
                __init__(parent, name='Ranged Weapom')
            self.variant(*melee.Agoniser)
            self.variant(*ranged.SplinterPistol)
            self.variant(*ranged.BlastPistol)
            self.variant(*melee.Impaler)

    class WeaponMelee(OneOf):
        def __init__(self, parent):
            super(Succubus.WeaponMelee, self).__init__(parent, 'Melee weapon')
            self.default = self.variant(*melee.ArchiteGlaive)
            armory.add_wych_weapons(self)

    def __init__(self, parent):
        super(Succubus, self).__init__(parent, points=get_cost(units.Succubus))
        self.rng = self.WeaponRanged(self)
        self.mle = self.WeaponMelee(self)

    def check_rules(self):
        super(Succubus, self).check_rules()
        self.rng.used = self.rng.visible = self.mle.cur == self.mle.default


class Lelith(HqUnit, armory.DEUnit):
    type_name = get_name(units.LelithHesperax)
    type_id = 'lelith_v1'

    faction = ['Cult of Strife', 'Ynnari']
    keywords = ['Character', 'Infantry', 'Succubus']
    power = 4

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Lelith.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Penetrating blade')
            self.variant('Impaler')

    def __init__(self, parent):
        super(Lelith, self).__init__(parent, points=get_cost(units.LelithHesperax),
                                     gear=[Gear('Mane of barbs and hooks'),
                                           Gear('Penetrating blade')],
                                     unique=True)
        self.Weapon(self)


class Haemunculus(HqUnit, armory.CovenUnit):
    type_name = get_name(units.Haemonculus)
    type_id = 'haemunculus_v1'

    keywords = ['Character', 'Infantry']
    power = 5

    class WeaponMelee(OneOf):
        def __init__(self, parent):
            super(Haemunculus.WeaponMelee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.HaemonculusTools)
            armory.add_torture_weapons(self)

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(Haemunculus.WeaponRanged, self).\
                __init__(parent, name='Ranged Weapom')
            # self.variant(*ranged.SplinterPistol)
            armory.add_torment_tool(self)

    class Option(OptionsList):
        def __init__(self, parent):
            super(Haemunculus.Option, self).__init__(parent, 'Options')
            self.variant(*melee.IchorInjector)

    def __init__(self, parent):
        super(Haemunculus, self).__init__(parent, points=get_cost(units.Haemonculus))
        self.WeaponMelee(self)
        self.WeaponRanged(self)
        self.Option(self)


class Rakarth(HqUnit, armory.DEUnit):
    type_name = get_name(units.UrienRakarth)
    type_id = 'rakarth_v1'

    faction = ['The Prophets of Flesh']
    keywords = ['Character', 'Infantry', 'Haemunculus']
    power = 5

    def __init__(self, parent):
        super(Rakarth, self).__init__(parent, points=get_cost(units.UrienRakarth), static=True,
                                      unique=True, gear=[
                                          Gear('Casket of Flensing'),
                                          Gear('Haemunculus tools'),
                                          Gear('Ichor injector')])


class Drazhar(HqUnit, armory.DEUnit):
    type_name = get_name(units.Drazhar)
    type_id = 'drazhar_v1'

    faction = ['Incubi']
    keywords = ['Character', 'Infantry']
    power = 6

    def __init__(self, parent):
        super(Drazhar, self).__init__(parent, points=get_cost(units.Drazhar), static=True,
                                      unique=True, gear=[Gear('Demiclaives')])
