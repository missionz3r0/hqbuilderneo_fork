Archon = ('Archon', 70)
Beastmaster = ('Beastmaster', 36)
ClawedFiends = ('Clawed Fiends', 32)
Cronos = ('Cronos', 65)
Grotesques = ('Grotesques', 32)
Haemonculus = ('Haemonculus', 70)
HekatrixBloodbrides = ('Hekatrix Bloodbrides', 13)
Hellions = ('Hellions', 14)
Incubi = ('Incubi', 16)
KabaliteTrueborn = ('Kabalite Trueborn', 11)
KabaliteWarriors = ('Kabalite Warriors', 6)
Khymerae = ('Khymerae', 10)
Lhamaean = ('Lhamaean', 15)
Mandrakes = ('Mandrakes', 16)
Medusae = ('Medusae', 21)
Raider = ('Raider', 65)
Ravager = ('Ravager', 80)
RazorwingJetfighter = ('Razorwing Jetfighter', 105)
RazorwingFlocks = ('Razorwing Flocks', 12)
Reavers = ('Reavers', 19)
Scourges = ('Scourges', 12)
Sslyth = ('Sslyth', 27)
Succubus = ('Succubus', 50)
Talos = ('Talos', 75)
UrGhul = ('Ur-Ghul', 15)
Venom = ('Venom', 55)
Voidraven = ('Voidraven Bomber', 155)
Wracks = ('Wracks', 9)
Wyches = ('Wyches', 8)
Drazhar = ('Drazhar', 120)
LelithHesperax = ('Lelith Hesperax', 80)
UrienRakarth = ('Urien Rakarth', 90)
