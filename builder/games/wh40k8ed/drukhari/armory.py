__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from . import ranged, melee, wargear
from builder.games.wh40k8ed.options import OptionsList, Gear

def add_torture_weapons(instance):
    instance.variant(*melee.Agoniser)
    instance.variant(*melee.ElectrocorrosiveWhip)
    instance.variant(*melee.FleshGauntlet)
    instance.variant(*melee.MindphaseGauntlet)
    instance.variant(*melee.Scissorhand)
    instance.variant(*melee.VenomBlade)


def add_torment_tool(instance):
    instance.variant(*ranged.StingerPistol)
    instance.variant(*ranged.Hexrifle)
    instance.variant(*ranged.LiquifierGun)


def add_wych_weapons(instance):
    instance.variant(*melee.HydraGauntlets),
    instance.variant(*melee.Razorflails)
    instance.variant(*melee.ShardnetAndImpaler, gear=[Gear('Shardnet'),
                                                      Gear('Impaler')])


class Equipment(OptionsList):
    def __init__(self, parent, venom=False):
        super(Equipment, self).__init__(parent, 'Equipment')
        self.variant(*wargear.ChainSnares)
        self.variant(*wargear.GrislyTrophies)
        if not venom:
            self.variant(*ranged.PhantasmGrenadeLauncher)
            self.variant(*melee.ShockProw)


class DEUnit(Unit):
    faction = ['Aeldari', 'Drukhari']
    wiki_faction = 'Drukhari'


class KabalUnit(DEUnit):
    faction = ['<Kabal>']

    @classmethod
    def calc_faction(cls):
        return ['YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL',
                'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE']


class CultUnit(DEUnit):
    faction = ['<Wych Cult>']

    @classmethod
    def calc_faction(cls):
        return ['YNNARI', 'CULT OF STRIFE',
                'CULT OF THE CURSED BLADE',
                'CULT OF THE RED GRIEF']


class CovenUnit(DEUnit):
    faction = ['<Haemunculus Coven>']

    @classmethod
    def calc_faction(cls):
        return ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE']


class SharedUnit(DEUnit):
    faction = ['<Kabal>', '<Wych Cult>', '<Haemunculus Coven>']

    @classmethod
    def calc_faction(cls):
        return ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL',
                'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE',
                'CULT OF STRIFE', 'CULT OF THE CURSED BLADE',
                'CULT OF THE RED GRIEF', 'THE PROPHETS OF FLESH',
                'THE DARK CREED', 'COVEN OF TWELVE', 'YNNARI']


class NonCovenUnit(DEUnit):
    faction = ['<Kabal>', '<Wych Cult>']

    @classmethod
    def calc_faction(cls):
        return ['KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL',
                'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE',
                'CULT OF STRIFE', 'CULT OF THE CURSED BLADE',
                'CULT OF THE RED GRIEF', 'YNNARI']
