from . import armory
from builder.games.wh40k8ed.unit import Unit, HqUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count
from . import melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class Warboss(HqUnit, armory.OrkClanUnit):
    type_name = get_name(units.Warboss)
    type_id = 'warboss_v1'

    keywords = ['Character', 'Infantry']
    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(Warboss.Options, self).__init__(parent, 'Pets')
            self.variant(*melee.AttackSquig)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Warboss.Ranged, self).__init__(parent, 'Ranged weapon')
            armory.add_shooty_weapons(self)

    class Melee(OneOf):
        def __init__(self, parent):
            super(Warboss.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerKlaw)
            self.variant(*melee.BigChoppa)

    def __init__(self, parent):
        gears = [ranged.Stikkbombs, ranged.Slugga, ranged.Slugga]
        super(Warboss, self).__init__(parent,
                                      points=points_price(get_costs(units.Warboss), *gears),
                                      gear=create_gears(*gears))
        self.Ranged(self)
        self.Melee(self)
        self.Options(self)


class WarbossInMegArmour(HqUnit, armory.OrkClanUnit):
    type_name = 'Warboss in Mega Armour' + ' (Index)'
    type_id = 'warboss_in_mega_armour_v1'

    keywords = ['Character', 'Infantry', 'Warboss', 'Mega Armour']

    power = 7

    class Ranged(OneOf):
        def __init__(self, parent):
            super(WarbossInMegArmour.Ranged, self).__init__(parent, 'Ranged weapon')
            armory.add_shooty_weapons(self)
            armory.add_choppy_weapons(self)

    class Melee(OneOf):
        def __init__(self, parent):
            super(WarbossInMegArmour.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerKlaw)

    def __init__(self, parent):
        super(WarbossInMegArmour, self).__init__(parent, 'Warboss in Mega Armour',
                                                 points=get_costs(units.WarbossInMegaArmour), gear=[])
        self.Ranged(self)
        self.Melee(self)


class WarbossOnWarBike(HqUnit, armory.OrkClanUnit):
    type_name = 'Warboss on Warbike' + ' (Index)'
    type_id = 'warboss_on_war_bike_armour_v1'

    keywords = ['Character', 'Biker', 'Warboss']

    power = 5

    class Options(OptionsList):
        def __init__(self, parent):
            super(WarbossOnWarBike.Options, self).__init__(parent, 'Options')
            self.variant(*melee.AttackSquig)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(WarbossOnWarBike.Ranged, self).__init__(parent, 'Ranged weapon')
            armory.add_shooty_weapons(self)

    class Melee(OneOf):
        def __init__(self, parent):
            super(WarbossOnWarBike.Melee, self).__init__(parent, 'Melee weapon')
            armory.add_choppy_weapons(self)

    def __init__(self, parent):
        gears = [ranged.Stikkbombs, ranged.Dakkagun, ranged.Dakkagun]

        super(WarbossOnWarBike, self).__init__(
            parent, 'Warboss on Warbike',
            points=points_price(get_costs(units.WarbossOnWarbike), *gears),
            gear=create_gears(*gears))
        self.Ranged(self)
        self.Melee(self)
        self.Options(self)


class Weirdboy(HqUnit, armory.OrkClanUnit):
    type_name = 'Weirdboy'
    type_id = 'weirdboy_v1'
    keywords = ['Character', 'Infantry', 'Psyker']

    power = 3

    def __init__(self, parent):
        gear = [melee.WeirdboyStaff]
        super(Weirdboy, self).__init__(parent, static=True,
                                       points=get_cost(units.Weirdboy),
                                       gear=create_gears(*gear))


class BigMek(HqUnit, armory.OrkClanUnit):
    type_name = get_name(units.BigMek) + ' (Index)'
    type_id = 'big_mek_v1'
    # obsolete = True

    keywords = ['Character', 'Infantry', 'Big Mek']

    power = 5

    class Options(OptionsList):
        def __init__(self, parent):
            super(BigMek.Options, self).__init__(parent, 'Options')
            self.variant(*units.GrotOiler)

    class Melee(OneOf):
        def __init__(self, parent):
            super(BigMek.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Choppa)
            armory.add_choppy_weapons(self)
            armory.add_souped_up_weapons(self)
            self.variant(*melee.Killsaw)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(BigMek.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.Slugga)
            self.variant(*ranged.ShokkAttackGun)
            self.variant(*wargear.KustomForceField)
            armory.add_choppy_weapons(self)
            armory.add_souped_up_weapons(self)

    def __init__(self, parent):
        gears = [ranged.Stikkbombs]
        super(BigMek, self).__init__(parent,
                                     points=points_price(get_cost(units.BigMek), *gears),
                                     gear=create_gears(*gears))
        self.Ranged(self)
        self.Melee(self)
        self.Options(self)


class ShokkBigMek(HqUnit, armory.OrkClanUnit):
    type_name = get_name(units.BigMekWithShokkAttackGun)
    type_id = 'shokk_big_mek_v1'
    kwname = 'Big Mek'
    keywords = ['Infantry', 'Character']

    class Helper(OptionsList):
        def __init__(self, parent):
            super(ShokkBigMek.Helper, self).__init__(parent, 'Helper')
            self.variant(*units.GrotOiler)

    def __init__(self, parent):
        gears = [ranged.ShokkAttackGun, ranged.Stikkbombs]
        super(ShokkBigMek, self).__init__(parent, points=points_price(get_cost(units.BigMek), *gears),
                                          gear=create_gears(*gears))
        self.Helper(self)


class BigMekInMegaArmour(HqUnit, armory.OrkClanUnit):
    type_name = get_name(units.BigMekInMegaArmour)
    type_id = 'big_mek_in_mega_armour_v1'
    kwname = 'Big Mek'
    keywords = ['Character', 'Infantry', 'Mega Armour']

    power = 6

    class Gear(OptionsList):
        def __init__(self, parent):
            super(BigMekInMegaArmour.Gear, self).__init__(parent, 'Gear', limit=1)
            self.variant(*wargear.KustomForceField)
            self.variant(*ranged.TellyportBlasta)

    class Options(OptionsList):
        def __init__(self, parent):
            super(BigMekInMegaArmour.Options, self).__init__(parent, 'Helper')
            self.variant(*units.GrotOiler)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(BigMekInMegaArmour.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.KustomMegaBlasta)
            self.variant(*melee.Killsaw)
            armory.add_shooty_weapons(self)

    def __init__(self, parent):
        gears = [melee.PowerKlaw]
        super(BigMekInMegaArmour, self).__init__(parent, points=points_price(get_cost(units.BigMekInMegaArmour), *gears),
                                                 gear=create_gears(*gears))
        self.Ranged(self)
        self.g = self.Gear(self)
        self.Options(self)


class BigMekOnWarbike(HqUnit, armory.OrkClanUnit):
    type_name = get_name(units.BigMekOnWarbike) + ' (Index)'
    type_id = 'big_mek_on_warbike_v1'

    keywords = ['Character', 'Biker', 'Big Mek']

    power = 6

    class Melee(OneOf):
        def __init__(self, parent):
            super(BigMekOnWarbike.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Choppa)
            armory.add_souped_up_weapons(self)
            armory.add_choppy_weapons(self)
            self.variant(*melee.Killsaw)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(BigMekOnWarbike.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.Slugga)
            self.variant(*ranged.ShokkAttackGun)
            self.variant(*wargear.KustomForceField)
            armory.add_choppy_weapons(self)
            armory.add_souped_up_weapons(self)

    def __init__(self, parent):
        gears = [ranged.Dakkagun, ranged.Dakkagun, ranged.Stikkbombs]
        super(BigMekOnWarbike, self).__init__(parent, get_name(units.BigMekOnWarbike),
                                              points=points_price(get_cost(units.BigMekOnWarbike), *gears),
                                              gear=create_gears(*gears))
        self.Ranged(self)
        self.Melee(self)


class GhazkullThraka(HqUnit, armory.OrkGoffUnit):
    type_name = get_name(units.GhazghkullThraka)
    type_id = 'ghazkull_thraka_v1'

    keywords = ['Character', 'Infantry', 'Mega Armour', 'Warboss']
    power = 12

    def __init__(self, parent):
        gears = [ranged.Stikkbombs, ranged.TwinBigShoota, melee.KustomKlaw]
        super(GhazkullThraka, self).__init__(parent, points=get_cost(units.GhazghkullThraka),
                                             gear=create_gears(*gears))


class KaptinBadrukk(HqUnit, armory.OrkUnit):
    type_name = get_name(units.KaptinBadrukk)
    type_id = 'kaptin_badrukk_v1'
    keywords = ['Character', 'Infantry', 'Flash Git', 'Kaptin Badrukk']
    faction = ['Freebooterz']
    power = 5

    # gunz for hire hack
    @classmethod
    def check_faction(cls, fac):
        return True

    class AmmoRunt(OptionsList):
        def __init__(self, parent):
            super(KaptinBadrukk.AmmoRunt, self).__init__(parent, 'Helper')
            self.variant(*units.AmmoRunt)

    def __init__(self, parent):
        gears = [ranged.Slugga, melee.Choppa, ranged.Stikkbombs, ranged.DaRippa]
        super(KaptinBadrukk, self).__init__(parent, points=get_cost(units.KaptinBadrukk),
                                            gear=create_gears(*gears),
                                            unique=True)
        self.AmmoRunt(self)


class BossZagstruk(HqUnit, armory.OrkGoffUnit):
    type_name = get_name(units.BossZagstruk)
    type_id = 'boss_zagstruk_v1'
    keywords = ['Character', 'Infantry', 'Stormboy', 'Jump Pack', 'Fly']

    power = 5

    def __init__(self, parent):
        gears = [ranged.Slugga, melee.Choppa, ranged.Stikkbombs]
        super(BossZagstruk, self).__init__(parent, points=get_cost(units.BossZagstruk),
                                           gear=create_gears(*gears),
                                           static=True, unique=True)


class BossSnikrot(HqUnit, armory.OrkBloodAxe):
    type_name = 'Boss Snikrot'
    type_id = 'boss_snikrot_v1'
    keywords = ['Character', 'Infantry', 'Kommando']

    power = 4

    def __init__(self, parent):
        gears = [melee.MorksTeeth, ranged.Stikkbombs]
        super(BossSnikrot, self).__init__(parent, points=get_cost(units.BossSnikrot),
                                          gear=create_gears(*gears),
                                          static=True, unique=True)


class DeffkillaWartrike(HqUnit, armory.OrkClanUnit):
    type_name = get_name(units.DeffkillaWartrike)
    type_id = 'deffkilla_wartrike_v1'
    keywords = ('Character', 'Vehicle', 'Speed freeks', 'Speedboss')
    power = 6

    def __init__(self, parent):
        gears = [ranged.KillaJet, ranged.SnaggaKlaw,
                 ranged.TwinBoomstikk, ranged.TwinBoomstikk, ranged.TwinBoomstikk]
        super(DeffkillaWartrike, self).__init__(parent,
                                                points=points_price(get_cost(units.DeffkillaWartrike), *gears),
                                                gear=create_gears(*gears),
                                                static=True)
