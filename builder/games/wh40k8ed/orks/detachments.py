from . import hq, elite, troops, fast, heavy, fly, transport

from builder.games.wh40k8ed.rosters import DetachVanguard

hq_units = [
    hq.BigMek, hq.BigMekInMegaArmour, hq.BigMekOnWarbike, hq.BossSnikrot, hq.BossZagstruk,
    hq.GhazkullThraka, hq.KaptinBadrukk, hq.Warboss, hq.WarbossInMegArmour, hq.WarbossOnWarBike,
    hq.Weirdboy
]

troops_units = [
    troops.Boyz, troops.Gretchin
]


elite_units = [
    elite.Kommandos, elite.MegaNobz, elite.BurnaBoyz, elite.MadDockGrotsnik, elite.Mek,
    elite.NobWithBanner, elite.Nobz, elite.NobzOnWarbikes, elite.MegaNobz, elite.Painboy,
    elite.PainboyOnWarbike, elite.Runtherd, elite.TankBustas
]

fast_units = [
    fast.Stormboyz, fast.Deffkoptas, fast.Warbikers, fast.Wartracks, fast.Skorchas, fast.WarBuggies,
]

transport_units = [
    transport.Trukk
]

heavy_units = [
    heavy.BigGunz, heavy.MekGunz, heavy.Battlewagon, heavy.KillaKans, heavy.Morkanaut,
    heavy.Gorkanaut, heavy.Lootas, heavy.FlashGitz
]

fly_units = [
    fly.Dakkajet, fly.BurnaBommer, fly.BlitzaBommer, fly.WazzboomBlastjet
]

unit_types = []
unit_types.extend(hq_units)
unit_types.extend(elite_units)
unit_types.extend(fast_units)
unit_types.extend(heavy_units)
unit_types.extend(fly_units)
unit_types.extend(transport_units)


class Vanguard(DetachVanguard):
    army_name = 'Orks (Vanguard detachment)'
    faction_base = 'ORK'
    alternate_factions = []
    army_id = 'vanguard_orks'
    army_factions = ['ORK']
    use_power = False

    def __init__(self, parent=None):
        super(Vanguard, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True,
                                               'elite': True, 'hq': True, 'fast': True,
                                               'transports': True, 'parent': parent, })
        self.troops.add_classes(troops_units)
        self.elite.add_classes(elite_units)
        self.hq.add_classes(hq_units)
        self.fast.add_classes(fast_units)
        self.heavy.add_classes(heavy_units)
        self.fliers.add_classes(fly_units)
        self.transports.add_classes(transport_units)
