from builder.games.wh40k8ed.unit import Unit, TransportUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count, ListSubUnit, SubUnit, UnitList
from . import melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *
from . import armory


class Trukk(TransportUnit, armory.OrkClanUnit):
    type_name = get_name(units.Trukk)
    type_id = 'trukk_v1'
    power = 3

    keywords = ['Vehicle', 'Transport']

    class Options(OptionsList):
        def __init__(self, parent):
            super(Trukk.Options, self).__init__(parent, 'Options', limit=1)
            self.variant(*melee.WreckinBall)
            self.variant(*melee.GrabbinKlaw)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Trukk.Ranged, self).__init__(parent, 'Ranged Weapon')
            self.variant(*ranged.BigShoota)
            self.variant(*ranged.RokkitLauncha)

    def __init__(self, parent):
        gear = [ranged.BigShoota]
        super(Trukk, self).__init__(parent, points=points_price(get_cost(units.Trukk), *gear),
                                    gear=create_gears(*gear))
        # self.Ranged(self)
        self.Options(self)
