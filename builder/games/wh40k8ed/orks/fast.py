from . import armory
from builder.games.wh40k8ed.unit import Unit, FastUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count, ListSubUnit, SubUnit, UnitList,\
    OptionalSubUnit, UnitDescription
from . import melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class Warbikers(FastUnit, armory.OrkClanUnit):
    type_name = get_name(units.Warbikers)
    type_id = "warbikers_v1"
    keywords = ['Biker', 'Speed freeks']
    power = 4

    class Warbiker(ListSubUnit):

        class Weapons(OptionsList):
            def __init__(self, parent):
                super(Warbikers.Warbiker.Weapons, self).__init__(parent, 'Weapon')
                self.variant(*ranged.Slugga)
                self.variant(*melee.Choppa)
                self.variant(*ranged.Stikkbombs)
        
        def __init__(self, parent):
            gear = [ranged.Dakkagun, ranged.Dakkagun]
            cost = points_price(get_costs(units.Warbikers), *gear)
            super(Warbikers.Warbiker, self).__init__(parent,
                                                     'Warbiker',
                                                     points=cost,
                                                     gear=create_gears(*gear))
            self.Weapons(self)

    class Boss(Unit):
        type_name = 'Boss Nob on Warbike'
        
        class Weapon1(OptionsList):
            def __init__(self, parent):
                super(Warbikers.Boss.Weapon1, self).__init__(parent, 'Weapon', limit=1)
                self.variant(*ranged.Slugga)
                armory.add_nob_weapons(self, slugga=False, double=True)

        class Weapon2(OptionsList):
            def __init__(self, parent):
                super(Warbikers.Boss.Weapon2, self).__init__(parent, '', limit=1)
                self.variant(*melee.Choppa)
                armory.add_nob_weapons(self, choppa=False)

        class Grenade(OptionsList):
            def __init__(self, parent):
                super(Warbikers.Boss.Grenade, self).__init__(parent, '')
                self.variant(*ranged.Stikkbombs)

        def __init__(self, parent):
            gear = [ranged.Dakkagun, ranged.Dakkagun]
            cost = points_price(get_costs(units.Warbikers), *gear)
            super(Warbikers.Boss, self).__init__(parent, points=cost, gear=create_gears(*gear))
            self.w1 = self.Weapon1(self)
            self.w2 = self.Weapon2(self)
            self.Grenade(self)

        def build_points(self):
            res = super(Warbikers.Boss, self).build_points()
            if self.w1.ksaw.value and self.w2.ksaw.value:
                res -= 2 * get_cost(melee.KillsawPair) - get_cost(melee.KillsawPair)
            return res    

    class BossOption(OptionalSubUnit):
        def __init__(self, parent):
            super(Warbikers.BossOption, self).__init__(parent, 'Boss Nob')
            SubUnit(self, Warbikers.Boss(parent=parent))

    def __init__(self, parent):
        super(Warbikers, self).__init__(parent)
        self.models = UnitList(self, self.Warbiker, min_limit=2, max_limit=12, start_value=3)
        self.boss = self.BossOption(self)

    def check_rules(self):
        super(Warbikers, self).check_rules()
        self.models.min, self.models.max = (3 - self.boss.any, 12 - self.boss.any)

    def get_count(self):
        return self.models.count + self.boss.any

    def build_power(self):
        return self.power + (10 if self.get_count() > 9 else
                             (7 if self.get_count() > 6 else
                              (3 if self.get_count() > 3 else 0)))


class KustomBoosta(FastUnit, armory.OrkClanUnit):
    type_name = 'Kustom Boosta-blastas'
    type_id = 'kustom_bb_v1'
    power = 5
    keywords = ['Vehicle', 'Speed Freeks']

    def __init__(self, parent):
        super(KustomBoosta, self).__init__(parent)
        gear = [ranged.RivetKannon] + [ranged.BurnaExhaust] * 4 +\
            [ranged.Stikkbombs, ranged.GrotBlasta]
        cost = points_price(get_cost(units.KustomBoostaBlasta), *gear)
        desc = UnitDescription(get_name(units.KustomBoostaBlasta),
                               cost,
                               options=create_gears(*gear))
        self.models = Count(self, self.type_name, 1, 3, cost, per_model=True,
                            gear=desc)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.get_count(self)


class ShokkjumpDragstas(FastUnit, armory.OrkClanUnit):
    type_name = 'Shokkjump Dragstas'
    type_id = 'shokkjump_drag_v1'
    power = 6
    keywords = ['Vehicle', 'Speed Freeks']

    def __init__(self, parent):
        super(ShokkjumpDragstas, self).__init__(parent)
        gear = [ranged.KustomShokkRifle, ranged.RokkitLauncha,
                melee.SawBlades]
        cost = points_price(get_cost(units.ShokkjumpDragsta), *gear)
        desc = UnitDescription(get_name(units.ShokkjumpDragsta),
                               cost,
                               options=create_gears(*gear))
        self.models = Count(self, self.type_name, 1, 3, cost, per_model=True,
                            gear=desc)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.get_count(self)


class BoomdakkaSnazzwagons(FastUnit, armory.OrkClanUnit):
    type_name = 'Boomdakka Snazzwagons'
    type_id = 'boomdakka_snazz_v1'
    power = 5
    keywords = ['Vehicle', 'Speed Freeks']

    def __init__(self, parent):
        super(BoomdakkaSnazzwagons, self).__init__(parent)
        gear = [ranged.MekSpeshul, ranged.BigShoota,
                ranged.BurnaBottles, ranged.GrotBlasta]
        cost = points_price(get_cost(units.BoomdakkaSnazzwagon), *gear)
        desc = UnitDescription(get_name(units.BoomdakkaSnazzwagon),
                               cost,
                               options=create_gears(*gear))
        self.models = Count(self, self.type_name, 1, 3, cost, per_model=True,
                            gear=desc)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.get_count(self)


class MegatrakkScrapjets(FastUnit, armory.OrkClanUnit):
    type_name = 'Megatrakk Scrapjets'
    type_id = 'scrapjet_v1'
    power = 5
    keywords = ['Vehicle', 'Speed Freeks']

    def __init__(self, parent):
        super(MegatrakkScrapjets, self).__init__(parent)
        gear = [ranged.RokkitKannon, ranged.TwinBigShoota,
                ranged.TwinBigShoota, ranged.WingMissiles,
                melee.NoseDrill]
        cost = points_price(get_cost(units.MegatrakkScrapjet), *gear)
        desc = UnitDescription(get_name(units.MegatrakkScrapjet),
                               cost,
                               options=create_gears(*gear))
        self.models = Count(self, self.type_name, 1, 3, cost, per_model=True,
                            gear=desc)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.get_count(self)


class RukkatrukkSquigbuggies(FastUnit, armory.OrkClanUnit):
    type_name = 'Rukkatrukk squigbuggies'
    type_id = 'rukkatrukks_v1'
    power = 7
    keywords = ['Vehicle', 'Speed Freeks']

    def __init__(self, parent):
        super(RukkatrukkSquigbuggies, self).__init__(parent)
        gear = [ranged.HeavySquigLauncha, melee.SawBlades,
                ranged.SquigLauncha, ranged.Shotgun,
                ranged.Stikkbombs]
        cost = points_price(get_cost(units.RukkatrukkSquigbuggy), *gear)
        desc = UnitDescription(get_name(units.RukkatrukkSquigbuggy),
                               cost,
                               options=create_gears(*gear))
        self.models = Count(self, self.type_name, 1, 3, cost, per_model=True,
                            gear=desc)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.get_count(self)


class Stormboyz(FastUnit, armory.OrkClanUnit):
    type_name = get_name(units.Stormboyz)
    type_id = "stormboyz_v1"
    power = 3
    _boy_gear = [ranged.Stikkbombs, ranged.Slugga, melee.Choppa]
    model = UnitDescription(
        'Stormboy',
        options=create_gears(*_boy_gear)
    )

    class Nob(Unit):
        type_name = "Boss Nob"

        class Ranged(OneOf):
            def __init__(self, parent):
                super(Stormboyz.Nob.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.Slugga)
                armory.add_nob_weapons(self, slugga=False, double=True)

        class Melee(OneOf):
            def __init__(self, parent):
                super(Stormboyz.Nob.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Choppa)
                armory.add_nob_weapons(self, choppa=False, double=False)

        def __init__(self, parent):
            gear = [ranged.Stikkbombs]
            cost = points_price(get_costs(units.Stormboyz), *gear)
            super(Stormboyz.Nob, self).__init__(parent, points=cost, gear=create_gears(*gear))
            self.mle = self.Melee(self)
            self.rng = self.Ranged(self)

        def build_points(self):
            res = super(Stormboyz.Nob, self).build_points()
            if self.rng.cur == self.rng.ksaw and self.mle.cur == self.mle.ksaw:
                res -= 2 * get_cost(melee.Killsaw) - get_cost(melee.KillsawPair)
            return res

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(Stormboyz.Boss, self).__init__(parent, 'Boss Nob')
            SubUnit(self, Stormboyz.Nob(parent=parent))

    def __init__(self, parent):
        super(Stormboyz, self).__init__(parent=parent)
        self.models = Count(
            self,
            'Stormboyz',
            5, 30,
            per_model=True,
            gear=Stormboyz.model.clone(),
            points=points_price(get_cost(units.Stormboyz), *self._boy_gear)
        )
        self.boss = self.Boss(self)

    def get_count(self):
        return self.models.cur + self.boss.any

    def check_rules(self):
        super(Stormboyz, self).check_rules()
        self.models.min, self.models.max = (5 - self.boss.any, 30 - self.boss.any)

    def build_power(self):
        return 3 + (11 if self.get_count() > 20 else
                    (6 if self.get_count() > 10 else
                    (2 if self.get_count() > 5 else 0)))


class Deffkoptas(FastUnit, armory.OrkClanUnit):
    keywords = ['Vehicle', 'Fly', 'Deffkoptas']
    type_name = get_name(units.Deffkoptas)
    type_id = 'deffkopta_v1'
    power = 2

    class Deffkopta(Count):
        @property
        def description(self):
            if self.cur - self.parent.rokkit.cur > 0:
                return [self.gear.clone().set_count(self.cur - self.parent.rokkit.cur)]
            else:
                return []

    def __init__(self, parent):
        super(Deffkoptas, self).__init__(parent)
        gear = [ranged.Slugga, melee.SpinninBlades]
        cost = points_price(get_cost(units.Deffkoptas), ranged.TwinBigShoota, *gear)
        self.models = self.Deffkopta(self, 'Deffkoptas', min_limit=1, max_limit=5, points=cost,
                                     per_model=True, gear=UnitDescription('Deffkopta', options=create_gears(ranged.TwinBigShoota, *gear)))
        self.rokkit = Count(self, 'Kopta rokkits', min_limit=0, max_limit=1, points=get_cost(ranged.KoptaRokkits) - get_cost(ranged.TwinBigShoota),
                            per_model=False, gear=UnitDescription('Deffkopta', options=create_gears(ranged.KoptaRokkits, *gear)))

    def check_rules(self):
        super(Deffkoptas, self).check_rules()
        self.rokkit.max = self.models.cur

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power + (6 if self.get_count() > 3 else (3 if self.get_count() > 1 else 0))


class Wartracks(FastUnit, armory.OrkClanUnit):
    type_name = get_name(units.Wartrakks) + ' (Index)'
    type_id = 'wartracks_v1'

    keywords = ['Vehicle', 'Wartakks']

    class Wartrakk(ListSubUnit):
        class Ranged(OneOf):
            def __init__(self, parent):
                super(Wartracks.Wartrakk.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.TwinBigShoota)
                self.variant(*ranged.RackOfRokkits)

        def __init__(self, parent):
            super(Wartracks.Wartrakk, self).__init__(parent, 'Wartrakk', get_costs(units.Wartrakks))
            self.ranged = self.Ranged(self)

    def __init__(self, parent):
        super(Wartracks, self).__init__(parent, 'Wartrakks')
        self.models = UnitList(self, self.Wartrakk, min_limit=1, max_limit=5)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 4 + (12 if self.get_count() > 3 else (6 if self.get_count() > 1 else 0))


class Skorchas(FastUnit, armory.OrkClanUnit):
    type_name = get_name(units.Skorchas) + ' (Index)'
    type_id = 'skorchas_v1'

    keywords = [get_name(units.Skorchas), 'Vehicle']

    class Skorcha(ListSubUnit):
        def __init__(self, parent):
            gear = [ranged.Skorcha]
            cost = points_price(get_costs(units.Skorchas), *gear)
            gear = create_gears(*gear)
            super(Skorchas.Skorcha, self).__init__(parent, 'Skorcha', gear=gear, points=cost)

    def __init__(self, parent):
        super(Skorchas, self).__init__(parent, 'Skorchas')
        self.models = UnitList(self, self.Skorcha, min_limit=1, max_limit=5)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 4 + (12 if self.get_count() > 3 else (6 if self.get_count() > 1 else 0))


class WarBuggies(FastUnit, armory.OrkClanUnit):
    type_name = get_name(units.Warbuggies) + ' (Index)'
    type_id = 'warbuggies_v1'

    keywords = [get_name(units.Warbuggies), 'Vehicle']

    class Buggie(ListSubUnit):
        def __init__(self, parent):
            gear = []
            cost = points_price(get_costs(units.Warbuggies), *gear)
            gear = create_gears(*gear)
            super(WarBuggies.Buggie, self).__init__(parent, 'Warbuggy', gear=gear, points=cost)
            self.Ranged(self)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(WarBuggies.Buggie.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.TwinBigShoota)
                self.variant(*ranged.RackOfRokkits)

    def __init__(self, parent):
        super(WarBuggies, self).__init__(parent, 'Warbuggies')
        self.models = UnitList(self, self.Buggie, min_limit=1, max_limit=5)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 4 + (12 if self.get_count() > 3 else (6 if self.get_count() > 1 else 0))
