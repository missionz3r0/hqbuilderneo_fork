from builder.games.wh40k8ed.unit import Unit
from . import melee, wargear, ranged
from builder.games.wh40k8ed.options import Gear, OptionsList


class OrkUnit(Unit):
    faction = ['ORK']
    wiki_faction = 'Orks'


class OrkClanUnit(OrkUnit, Unit):
    faction = ['<Clan>']

    @classmethod
    def calc_faction(cls):
        return ['GOFFS', 'BLOOD AXES', 'DEATHSKULLS',
                'BAD MOONS', 'EVIL SUNZ', 'SNAKEBITES',
                'FREEBOOTERZ']


class OrkGoffUnit(OrkUnit, Unit):
    faction = ['Goffs']


class OrkBloodAxe(OrkUnit, Unit):
    faction = ['Blood Axes']


class OrkDeathSkulls(OrkUnit, Unit):
    faction = ['Deathskulls']


def add_shooty_weapons(obj):
    """
    add variants for orks shooting weapons list
    :param obj: object with variant interface
    :return: self object
    """
    obj.variant(*ranged.KombiWeaponWithRokkitLauncha)
    obj.variant(*ranged.KombiWeaponWithSkorcha)
    obj.variant(*ranged.KustomShoota)
    return obj

def add_choppy_weapons(obj):
     """
     add variants for orks melee weapons list
     :param obj: object with variant interface
     :return: self object
     """
     obj.variant(*melee.BigChoppa)
     obj.variant(*melee.PowerKlaw)


def add_nob_weapons(obj, slugga=True, choppa=True, double=False):
    obj.variant(*melee.BigChoppa)
    if choppa:
        obj.variant(*melee.Choppa)
    obj.ksaw = obj.variant(*melee.Killsaw)
    obj.variant(*melee.PowerKlaw)
    obj.variant(*melee.PowerStabba)
    if slugga:
        obj.variant(*ranged.Slugga)
    if double:
        obj.double = [
            obj.variant(*ranged.KombiWeaponWithRokkitLauncha),
            obj.variant(*ranged.KombiWeaponWithSkorcha)
        ]
    return obj


def add_eavy_weapons(obj):
    """
    add variants for orks eavy weapons list
    :param obj: object with variant interface
    :return: self object
    """
    obj.variant(*ranged.BigShoota)
    obj.variant(*ranged.RokkitLauncha)
    return obj


def add_souped_up_weapons(obj):
    """
    add variants for orks souped up weapons list
    :param obj: object with variant interface
    :return: self object
    """
    obj.variant(*ranged.KombiWeaponWithRokkitLauncha)
    obj.variant(*ranged.KustomMegaBlasta)
    obj.variant(*ranged.RokkitLauncha)
    obj.variant(*ranged.KombiWeaponWithSkorcha)
    obj.variant(*ranged.KustomMegaSlugga)
    return obj


class BattlewagonGubbins(OptionsList):
    def __init__(self, parent):
        super(BattlewagonGubbins, self).__init__(parent, 'Equipment')
        self.variant(*melee.GrabbinKlaw)
        self.variant(*wargear.GrotRigger)
        self.variant(*ranged.Lobba)
        self.variant(*ranged.StikkbombChukka)
        self.variant(*melee.WreckinBall)
