from . import armory
from builder.games.wh40k8ed.unit import Unit, ElitesUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count, ListSubUnit, SubUnit, UnitList,\
    OptionalSubUnit
from . import melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *
import math


class MadDockGrotsnik(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.MadDokGrotsnik)
    type_id = 'mad_dock_grotsnik_v1'

    keywords = ['Character', 'Infantry', 'Painboy', 'Mad Dok Grotsnik']

    power = 5

    def __init__(self, parent):
        gears = [ranged.Slugga, melee.PowerKlaw, melee.UrtySyringe]
        super(MadDockGrotsnik, self).__init__(parent,
                                              points=get_costs(units.MadDokGrotsnik),
                                              gear=create_gears(*gears))


class Painboy(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.Painboy) + ' (Index)'
    type_id = 'painboy_v1'

    keywords = ['Character', 'Infantry', 'Painboy']

    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(Painboy.Options, self).__init__(parent, 'Options')
            self.variant(*units.GrotOrderly)

    class Melee(OneOf):
        def __init__(self, parent):
            super(Painboy.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerKlaw)
            self.variant(*melee.Killsaw)

    def __init__(self, parent):
        gears = [ranged.Stikkbombs, melee.UrtySyringe]
        super(Painboy, self).__init__(parent, name=get_name(units.Painboy),
                                      points=points_price(get_costs(units.Painboy), *gears),
                                      gear=create_gears(*gears))
        self.Melee(self)
        self.Options(self)


class PainboyV2(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.Painboy)
    type_id = 'painboy_v2'

    keywords = ['Character', 'Infantry', 'Painboy']

    power = 3

    def __init__(self, parent):
        gears = [ranged.Stikkbombs, melee.UrtySyringe, melee.PowerKlaw]
        super(PainboyV2, self).__init__(parent,
                                        points=points_price(get_costs(units.Painboy), *gears),
                                        gear=create_gears(*gears))


class Mek(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.Mek) + ' (Index)'
    type_id = 'mek_v1'

    keywords = ['Character', 'Infantry', 'Mek']

    power = 3

    class Options(OptionsList):
        def __init__(self, parent):
            super(Mek.Options, self).__init__(parent, 'Options')
            self.variant(*units.GrotOiler)

    class Melee(OneOf):
        def __init__(self, parent):
            super(Mek.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Choppa)
            self.variant(*melee.Killsaw)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Mek.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.Slugga)
            armory.add_souped_up_weapons(self)

    def __init__(self, parent):
        gears = [ranged.Stikkbombs]
        super(Mek, self).__init__(parent, get_name(units.Mek),
                                  points=points_price(get_costs(units.Mek), *gears),
                                  gear=create_gears(*gears))
        self.Ranged(self)
        self.Melee(self)
        self.Options(self)


class MekV2(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.Mek)
    type_id = 'mek_v2'

    keywords = ['Character', 'Infantry', 'Mek']

    power = 2

    class Options(OptionsList):
        def __init__(self, parent):
            super(MekV2.Options, self).__init__(parent, 'Options')
            self.variant(*units.GrotOiler)

    class Melee(OneOf):
        def __init__(self, parent):
            super(MekV2.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Choppa)
            self.variant(*melee.Killsaw)

    def __init__(self, parent):
        gears = [ranged.KustomMegaSlugga, ranged.Stikkbombs]
        super(MekV2, self).__init__(parent,
                                    points=points_price(get_costs(units.Mek), *gears),
                                    gear=create_gears(*gears))
        self.Melee(self)
        self.Options(self)


class PainboyOnWarbike(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.PainboyOnWarbike) + ' (Index)'
    type_id = 'painboy_on_warbike_v1'

    keywords = ['Character', 'Biker']

    power = 4

    class Melee(OneOf):
        def __init__(self, parent):
            super(PainboyOnWarbike.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerKlaw)
            self.variant(*melee.Killsaw)

    def __init__(self, parent):
        gears = [ranged.Dakkagun, ranged.Dakkagun, melee.UrtySyringe]
        cost = points_price(get_cost(units.PainboyOnWarbike), *gears)
        super(PainboyOnWarbike, self).__init__(parent, get_name(units.PainboyOnWarbike),
                                               points=cost,
                                               gear=create_gears(*gears))
        self.Melee(self)


class Runtherd(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.Runtherd)
    type_id = 'runtherd_v1'

    keywords = ['Character', 'Infantry', 'Runtherd']

    power = 2

    class Melee(OneOf):
        def __init__(self, parent):
            super(Runtherd.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.GrabbaStikk)
            self.variant(*melee.GrotProd)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Runtherd.Options, self).__init__(parent, 'Options', limit=1)
            self.variant(*wargear.GrotLash)
            self.variant(*wargear.SquigHound)

    def __init__(self, parent):
        gears = [ranged.Stikkbombs, ranged.Slugga]
        super(Runtherd, self).__init__(parent,
                                       points=points_price(get_costs(units.Runtherd), *gears),
                                       gear=create_gears(*gears))
        self.Melee(self)
        self.opt = self.Options(self)


class BurnaBoyz(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.BurnaBoyz)
    type_id = "burna_boyz_v1"
    keywords = ['Infantry']
    power = 3

    class Options(OptionalSubUnit):
        def __init__(self, parent):
            super(BurnaBoyz.Options, self).__init__(parent, 'Options')
            self.spanners = UnitList(self, BurnaBoyz.Spanner, 1, 3)

    class Spanner(ListSubUnit):
        type_name = 'Spanner'

        class Ranged(OneOf):
            def __init__(self, parent):
                super(BurnaBoyz.Spanner.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.KustomMegaBlasta)
                self.variant(*ranged.BigShoota)
                self.variant(*ranged.RokkitLauncha)

        def __init__(self, parent):
            gear = [ranged.Stikkbombs]
            cost = points_price(get_costs(units.BurnaBoyz), *gear)
            super(BurnaBoyz.Spanner, self).__init__(parent,
                                                    points=cost,
                                                    gear=create_gears(*gear)
                                                    )
            self.Ranged(self)

    def __init__(self, parent):
        super(BurnaBoyz, self).__init__(parent, name=get_name(units.BurnaBoyz))
        gear = [ranged.Stikkbombs, ranged.Burna]
        cost = points_price(get_costs(units.BurnaBoyz), *gear)
        self.boyz = Count(self, 'Burna Boyz', min_limit=5, max_limit=15, points=cost,
                          gear=UnitDescription('Burna Boy', options=create_gears(*gear)))
        self.spanner_opts = self.Options(self)

    def get_count(self):
        return self.boyz.cur + self.spanner_opts.count

    def check_rules(self):
        super(BurnaBoyz, self).check_rules()
        self.boyz.min, self.boyz.max = (5 - self.spanner_opts.count,
                                        15 - self.spanner_opts.count)

    def build_power(self):
        return self.power * ((self.power + 4) / 5)


class TankBustas(ElitesUnit, armory.OrkClanUnit):
    power = 4
    type_name = get_name(units.Tankbustas)
    type_id = 'tanbistas_v1'
    keywords = ['Infantry']

    model_gear = [ranged.Stikkbombs, ranged.TankbustaBombs]
    model_cost = points_price(get_costs(units.Tankbustas), *model_gear)

    model = UnitDescription('Tankbusta', options=create_gears(*model_gear))

    class Boss(Unit):
        type_name = 'Boss Nob'

        # class BossWeapon(OneOf):
        #     def __init__(self, parent):
        #         super(TankBustas.Boss.BossWeapon, self).__init__(parent, 'Boss Nob Weapon')
        #         self.variant(*ranged.RokkitLauncha)
        #         armory.add_choppy_weapons(self)

        def __init__(self, parent):
            gear = [ranged.RokkitLauncha, ranged.Stikkbombs, ranged.TankbustaBombs]
            cost = points_price(get_cost(units.Tankbustas), *gear)
            super(TankBustas.Boss, self).__init__(parent, points=cost,
                                                  gear=create_gears(*gear), static=True)

    class OptBoss(OptionalSubUnit):
        def __init__(self, parent):
            super(TankBustas.OptBoss, self).__init__(parent, 'Boss Nob')
            SubUnit(self, TankBustas.Boss(parent=self))

    class TankBusta(Count):
        @property
        def description(self):
            return [TankBustas.model.clone().add(create_gears(ranged.RokkitLauncha)).
                    set_count(self.cur - self.parent.hammers.cur - self.parent.pistols.cur)]

    def __init__(self, parent):
        super(TankBustas, self).__init__(parent)
        self.tankbustas = self.TankBusta(self, 'Tankbustas', min_limit=5, max_limit=15,
                                         points=self.model_cost + get_costs(ranged.RokkitLauncha))
        self.hammers = Count(self, 'Tankhammer', 0, 2, points=get_costs(melee.Tankhammer),
                             gear=self.model.clone().add(create_gears(melee.Tankhammer)))
        self.pistols = Count(self, 'Pairs of rokkit pistols', 0, 1, points=get_costs(ranged.PairOfRokkitPistols),
                             gear=self.model.clone().add(create_gears(ranged.PairOfRokkitPistols)))
        self.boss = self.OptBoss(self)

        squig_gear = [ranged.SquigBomb]
        squig_cost = points_price(get_cost(units.BombSquig), *squig_gear)

        self.squigs = Count(self, 'Bomb Squig', min_limit=0, max_limit=2, points=squig_cost,
                            gear=UnitDescription('Bomb Squig', options=create_gears(*squig_gear)))

    def get_count(self):
        return self.tankbustas.cur + self.boss.any

    def build_power(self):
        return self.power + (9  if self.get_count() > 10 else (4 if self.get_count() > 5 else 0))

    def check_rules(self):
        super(TankBustas, self).check_rules()
        self.tankbustas.min, self.tankbustas.max = (5 - self.boss.any, 15 - self.boss.any)
        self.squigs.max = 2 * (self.get_count() / 5)
        self.pistols.max = self.get_count() / 5
        self.hammers.max = self.get_count() / 5

    def build_points(self):
        return super(TankBustas, self).build_points() - get_costs(ranged.RokkitLauncha) * (self.hammers.cur + self.pistols.cur)


class Nobz(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.Nobz)
    type_id = 'nobz_v1'
    power = 7

    keywords = ['Infantry', 'Nobz']

    class Nob(ListSubUnit):
        type_name = "Nob"

        class Ranged(OneOf):
            def __init__(self, parent):
                super(Nobz.Nob.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.Slugga)
                armory.add_nob_weapons(self, slugga=False, double=True)

        class Melee(OneOf):
            def __init__(self, parent):
                super(Nobz.Nob.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Choppa)
                armory.add_nob_weapons(self, choppa=False, double=False)

        def __init__(self, parent):
            gear = [ranged.Stikkbombs]
            cost = points_price(get_costs(units.Nobz), *gear)
            super(Nobz.Nob, self).__init__(parent, points=cost, gear=create_gears(*gear))
            self.mle = self.Melee(self)
            self.rng = self.Ranged(self)
            self.opt = self.Options(self)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Nobz.Nob.Options, self).__init__(parent, 'Options')
                self.variant(*wargear.CyborkBody)

        def build_points(self):
            res = super(Nobz.Nob, self).build_points()
            if self.rng.cur == self.rng.ksaw and self.mle.cur == self.mle.ksaw:
                res -= 2 * get_cost(melee.Killsaw) - get_cost(melee.KillsawPair)
            return res

        @ListSubUnit.count_gear
        def has_body(self):
            return self.opt.any

    class Boss(Unit):
        type_name = "Boss Nob"


        def __init__(self, parent):
            gear = [ranged.Stikkbombs]
            cost = points_price(get_cost(units.Nobz), *gear)
            super(Nobz.Boss, self).__init__(parent,
                                            points=cost,
                                            gear=create_gears(*gear))
            self.rng = Nobz.Nob.Ranged(self)
            self.mle = Nobz.Nob.Melee(self)
            self.opt = Nobz.Nob.Options(self)

        def build_points(self):
            res = super(Nobz.Boss, self).build_points()
            if self.rng.cur == self.rng.ksaw and self.mle.cur == self.mle.ksaw:
                res -= 2 * get_cost(melee.Killsaw) - get_cost(melee.KillsawPair)
            return res

        def has_body(self):
            return self.opt.any

    def __init__(self, parent):
        super(Nobz, self).__init__(parent)
        self.nobz = UnitList(self, self.Nob, 4, 9)
        self.boss = SubUnit(self, self.Boss(parent=self))
        self.runtz = Count(self, 'Ammo runtz', 0, 2, get_costs(units.AmmoRunt),
                           gear=UnitDescription('Ammo runt'))

    def get_count(self):
        return 1 + self.nobz.count

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))

    def check_rules(self):
        super(Nobz, self).check_rules()
        self.runtz.max = self.get_count() / 5
        body_count = self.boss.unit.has_body() + sum(u.has_body() for u in self.nobz.units)
        if body_count > self.get_count() / 5:
            self.error('Only one Nob or Boss Nob per 5 models may take a Cybork Body')
    

class NobWithBanner(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.NobWithWaaaghBanner)
    type_id = 'nob_with_waagh_banner_v1'
    keywords = ['Infantry', 'Character', 'Nob']
    power = 4

    def __init__(self, parent):
        gear = [ranged.KustomShoota, ranged.Stikkbombs, melee.WaaaghBanner]
        costs = points_price(get_costs(units.NobWithWaaaghBanner), *gear)
        gear = create_gears(*gear)
        super(NobWithBanner, self).__init__(parent=parent, points=costs, gear=gear, static=True)


class MegaNobz(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.Meganobz)
    type_id = 'meganobz_v1'
    power = 6

    keywords = ['Meganobz', 'Infantry', 'Mega armour']

    class Nob(ListSubUnit):
        type_name = "Meganob"

        class Ranged(OneOf):
            def __init__(self, parent):
                super(MegaNobz.Nob.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.KustomShoota)
                self.variant(*ranged.KombiWeaponWithRokkitLauncha)
                self.variant(*ranged.KombiWeaponWithSkorcha)

        class Melee(OneOf):
            def __init__(self, parent):
                super(MegaNobz.Nob.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.PowerKlaw)
                self.saws = self.variant('Two Killsaws', get_cost(melee.KillsawPair),
                                         gear=[Gear('Killsaw', count=2)])

        def __init__(self, parent):
            gear = [ranged.Stikkbombs]
            cost = points_price(get_costs(units.Meganobz), *gear)
            super(MegaNobz.Nob, self).__init__(parent,
                                               points=cost,
                                               gear=create_gears(*gear)
                                               )
            self.mle = self.Melee(self)
            self.rng = self.Ranged(self)

        def check_rules(self):
            super(MegaNobz.Nob, self).check_rules()
            self.rng.visible = self.rng.used = self.mle.cur != self.mle.saws

    class Boss(Unit):
        type_name = "Boss Meganob"

        def __init__(self, parent):
            gear = [ranged.Stikkbombs]
            cost = points_price(get_costs(units.Meganobz), *gear)
            super(MegaNobz.Boss, self).__init__(parent,
                                                points=cost,
                                                gear=create_gears(*gear)
                                                )
            self.mle = MegaNobz.Nob.Melee(self)
            self.rng = MegaNobz.Nob.Ranged(self)

        def check_rules(self):
            super(MegaNobz.Boss, self).check_rules()
            self.rng.visible = self.rng.used = self.mle.cur != self.mle.saws

    def __init__(self, parent):
        super(MegaNobz, self).__init__(parent)
        self.nobz = UnitList(self, self.Nob, min_limit=2, max_limit=9)
        self.boss = SubUnit(self, self.Boss(parent=self))

    def get_count(self):
        return self.boss.count + self.nobz.count

    def build_power(self):
        return self.power + 2 * (self.get_count() - 3)


class NobzOnWarbikes(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.NobzOnWarbikes)
    type_id = 'nobz_on_warbike_v1'
    power = 7
    keywords = ['Biker', 'Nobz', 'Speed Freeks']

    class Nob(ListSubUnit):
        type_name = "Nob on Warbike"

        class Ranged(OptionsList):
            def __init__(self, parent):
                super(NobzOnWarbikes.Nob.Ranged, self).__init__(parent, 'Ranged weapon', limit=1)
                armory.add_nob_weapons(self, double=False)

        class Grenade(OptionsList):
            def __init__(self, parent):
                super(NobzOnWarbikes.Nob.Grenade, self).__init__(parent, '')
                self.variant(*ranged.Stikkbombs)
        
        class Melee(OneOf):
            def __init__(self, parent):
                super(NobzOnWarbikes.Nob.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Choppa)
                armory.add_nob_weapons(self, choppa=False, double=True)

        def __init__(self, parent):
            gear = [ranged.Stikkbombs, ranged.Dakkagun, ranged.Dakkagun]
            cost = points_price(get_costs(units.NobzOnWarbikes), *gear)
            super(NobzOnWarbikes.Nob, self).__init__(parent,
                                                     points=cost,
                                                     gear=create_gears(*gear)
                                                     )
            self.mle = self.Melee(self)
            self.rng = self.Ranged(self)
            self.Grenade(self)

        def build_points(self):
            res = super(NobzOnWarbikes.Nob, self).build_points()
            if self.mle.cur == self.mle.ksaw and self.rng.ksaw.value:
                res -= 2 * get_cost(melee.Killsaw) - get_cost(melee.KillsawPair)
            return res

    class Boss(Unit):
        type_name = "Boss Nob on Warbike"

        def __init__(self, parent):
            gear = [ranged.Stikkbombs, ranged.Dakkagun, ranged.Dakkagun]
            cost = points_price(get_costs(units.NobzOnWarbikes), *gear)
            super(NobzOnWarbikes.Boss, self).__init__(parent,
                                                      points=cost,
                                                      gear=create_gears(*gear)
                                                      )
            self.rng = NobzOnWarbikes.Nob.Ranged(self)
            self.mle = NobzOnWarbikes.Nob.Melee(self)
            NobzOnWarbikes.Nob.Grenade(self)

        def build_points(self):
            res = super(NobzOnWarbikes.Boss, self).build_points()
            if self.mle.cur == self.mle.ksaw and self.rng.ksaw.value:
                res -= 2 * get_cost(melee.Killsaw) - get_cost(melee.KillsawPair)
            return res

    def __init__(self, parent):
        super(NobzOnWarbikes, self).__init__(parent)
        self.nobz = UnitList(self, self.Nob, min_limit=2, max_limit=8)
        self.boss = SubUnit(self, self.Boss(parent=self))

    def get_count(self):
        return 1 + self.nobz.count

    def build_power(self):
        return self.power + (12 if self.get_count() > 6 else (7 if self.get_count() > 3 else 0))


class Kommandos(ElitesUnit, armory.OrkClanUnit):
    type_name = get_name(units.Kommandos)
    type_id = 'kommandos_v1'
    power = 2

    keywords = ['Infantry', 'Kommandos']
    model_gear = [ranged.Stikkbombs, melee.Choppa, ranged.Slugga]
    model = UnitDescription('Kommando', options=create_gears(*model_gear))

    # def add_special_weapon(self, name, points):
    #     self.specials.append(Count(self, name, 0, 2, points, gear=self.model.clone().add(Gear(name))))

    class Kommando(Count):
        @property
        def description(self):
            bc = self.parent.get_count() / 5
            base = Kommandos.model.clone().set_count(self.cur - bc)
            res = [base]
            if bc > 0:
                bomber = Kommandos.model.clone().add(Gear('Tankbusta bomb')).set_count(bc)
                res.append(bomber.clone())
            return res

    class Boss(Unit):
        type_name = "Boss Nob"

        # class Melee(OneOf):
        #     def __init__(self, parent):
        #         super(Kommandos.Boss.Melee, self).__init__(parent, 'Melee weapon')
        #         self.variant(*melee.Choppa)
        #         armory.add_choppy_weapons(self)

        def __init__(self, parent):
            gear = [melee.PowerKlaw, ranged.Stikkbombs, ranged.Slugga]
            cost = points_price(get_costs(units.Kommandos), *gear)
            super(Kommandos.Boss, self).__init__(parent,
                                                 points=cost, static=True,
                                                 gear=create_gears(*gear))
            # self.Melee(self)

    class OptBoss(OptionalSubUnit):
        def __init__(self, parent):
            super(Kommandos.OptBoss, self).__init__(parent, 'Options')
            SubUnit(self, Kommandos.Boss(parent))

    def __init__(self, parent):
        super(Kommandos, self).__init__(parent)
        self.nobz = self.Kommando(self, 'Kommandos', min_limit=5, max_limit=15,
                                  points=points_price(get_costs(units.Kommandos), *self.model_gear))
        # self.specials = []
        # for s in [ranged.BigShoota, ranged.Burna, ranged.RokkitLauncha]:
        #     self.add_special_weapon(*s)
        self.boss = self.OptBoss(self)

    def check_rules(self):
        super(Kommandos, self).check_rules()
        self.nobz.min, self.nobz.max = (5 - self.boss.any,
                                        15 - self.boss.any)
        # Count.norm_counts(0, 2, self.specials)

    def get_count(self):
        return self.nobz.cur + self.boss.count

    def build_power(self):
        return self.power * ((self.get_count() + 4) / 5)

    def build_points(self):
        res = super(Kommandos, self).build_points()
        res += get_cost(ranged.TankbustaBombs) * self.get_count() / 5
        return res
