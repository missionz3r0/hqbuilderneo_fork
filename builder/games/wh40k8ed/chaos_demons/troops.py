__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import TroopsUnit
from builder.games.wh40k8ed.options import UnitDescription, Count
from . import armory, units, wargear
from builder.games.wh40k8ed.utils import *


class Bloodletters(TroopsUnit, armory.KhorneUnit):
    type_name = get_name(units.Bloodletters)
    type_id = 'bloodletters_v1'

    keywords = ['Infantry']

    member = UnitDescription('Bloodletter', options=[Gear('Hellblade')])
    power = 4

    class Letters(Count):
        @property
        def description(self):
            return Bloodletters.member.clone().set_count(self.cur - (self.parent.icon.cur + self.parent.trumpet.cur))

    def __init__(self, parent):
        super(Bloodletters, self).__init__(parent, gear=[UnitDescription('Bloodreaper',
                                                                         options=[Gear('Hellblade')])],
                                           points=get_cost(units.Bloodletters))
        
        self.models = self.Letters(self, 'Bloodletters', 9, 29, points=get_cost(units.Bloodletters), per_model=True)
        self.icon = Count(self, 'Daemonic icons', 0, 1, points=get_cost(wargear.DaemonicIcon), gear=Bloodletters.member.clone().add(Gear('Daemonic Icon')))
        self.trumpet = Count(self, 'Instruments of Chaos', 0, 1, points=get_cost(wargear.InstrumentOfChaos), gear=Bloodletters.member.clone().add(Gear('Instrument of Chaos')))

    def check_rules(self):
        super(Bloodletters, self).check_rules()
        self.icon.max = self.get_count() / 10
        self.trumpet.max = self.get_count() / 10

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * ((self.models.cur + 10) / 10)


class Horrors(TroopsUnit, armory.TzeentchUnit):
    type_name = 'Horrors'
    type_id = 'horrors_v1'

    keywords = ['Infantry', 'Psyker']
    power = 4
    member = UnitDescription('Pink Horror', options=[Gear('Coruscating flames')])

    class Pinks(Count):
        @property
        def description(self):
            return Horrors.member.clone().set_count(self.cur - (self.parent.icon.cur + self.parent.trumpet.cur))

    def __init__(self, parent):
        super(Horrors, self).__init__(parent)

        self.models = self.Pinks(self, 'Pink Horrors', 10, 30, points=get_cost(units.PinkHorrors), per_model=True)
        self.models2 = Count(self, 'Blue Horrors', 0, 30, points=get_cost(units.BlueHorrors), per_model=True, gear=UnitDescription('Blue Horror'))
        self.models3 = Count(self, 'Brimstone Horror pairs', 0, 30, points=get_cost(units.BrimstonePairs), per_model=True, gear=UnitDescription('Pair of Brimstone Horrors'))
        self.icon = Count(self, 'Daemonic icons', 0, 1, points=get_cost(wargear.DaemonicIcon),
                          gear=Horrors.member.clone().add(Gear('Daemonic Icon')))
        self.trumpet = Count(self, 'Instruments of Chaos', 0, 1, points=get_cost(wargear.InstrumentOfChaos),
                             gear=Horrors.member.clone().add(Gear('Instrument of Chaos')))

    def check_rules(self):
        super(Horrors, self).check_rules()
        self.icon.max = min(self.get_count() / 10, self.models.cur)
        self.trumpet.max = min(self.get_count() / 10, self.models.cur)
        Count.norm_counts(10, 30, [self.models, self.models2, self.models3])

    def get_count(self):
        return self.models.cur + self.models2.cur + self.models3.cur

    def build_power(self):
        return self.power * ((self.get_count() + 9) / 10)


class Plaguebearers(TroopsUnit, armory.NurgleUnit):
    type_name = get_name(units.Plaguebearers)
    type_id = 'plaguebearers_v1'

    keywords = ['Infantry']
    power = 4
    member = UnitDescription('Plaguebearer', options=[Gear('Plaguesword')])

    class Bearers(Count):
        @property
        def description(self):
            return Plaguebearers.member.clone().set_count(self.cur - (self.parent.icon.cur + self.parent.trumpet.cur))

    def __init__(self, parent):
        super(Plaguebearers, self).__init__(parent, gear=[UnitDescription('Plagueridden',
                                                                          options=[Gear('Plaguesword')])],
                                            points=get_cost(units.Plaguebearers))
        
        self.models = self.Bearers(self, 'Plaguebearers', 9, 29, points=get_cost(units.Plaguebearers),
                                   per_model=True)
        self.icon = Count(self, 'Daemonic icons', 0, 1, points=get_cost(wargear.DaemonicIcon),
                          gear=Plaguebearers.member.clone().add(Gear('Daemonic Icon')))
        self.trumpet = Count(self, 'Instruments of Chaos', 0, 1, points=get_cost(wargear.InstrumentOfChaos),
                             gear=Plaguebearers.member.clone().add(Gear('Instrument of Chaos')))

    def check_rules(self):
        super(Plaguebearers, self).check_rules()
        self.icon.max = self.get_count() / 10
        self.trumpet.max = self.get_count() / 10

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * ((self.models.cur + 10) / 10)


class Nurglings(TroopsUnit, armory.NurgleUnit):
    type_name = get_name(units.Nurglings)
    type_id = 'nurglings_v1'

    keywords = ['Swarm']

    def __init__(self, parent):
        super(Nurglings, self).__init__(parent)
        self.models = Count(self, 'Nurgling Swarms', 3, 9, points=get_cost(units.Nurglings), per_model=True,
                            gear=UnitDescription('Nurgling Swarm', options=[Gear('Diseased claws and teeth')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 3 * ((self.models.cur + 2) / 3)


class Daemonettes(TroopsUnit, armory.SlaaneshUnit):
    type_name = get_name(units.Daemonettes)
    type_id = 'daemonettes_v1'

    keywords = ['Infantry']
    power = 4
    member = UnitDescription('Daemonette', options=[Gear('Piercing claws')])

    class Dancers(Count):
        @property
        def description(self):
            return Daemonettes.member.clone().set_count(self.cur - (self.parent.icon.cur + self.parent.trumpet.cur))

    def __init__(self, parent):
        super(Daemonettes, self).__init__(parent, gear=[UnitDescription('Alluress',
                                                                        options=[Gear('Piercing claws')])],
                                          points=get_cost(units.Daemonettes))
        
        self.models = self.Dancers(self, 'Daemonettes', 9, 29, points=get_cost(units.Daemonettes), per_model=True)
        self.icon = Count(self, 'Daemonic icons', 0, 1, points=get_cost(wargear.DaemonicIcon),
                          gear=Daemonettes.member.clone().add(Gear('Daemonic Icon')))
        self.trumpet = Count(self, 'Instruments of Chaos', 0, 1, points=get_cost(wargear.InstrumentOfChaos),
                             gear=Daemonettes.member.clone().add(Gear('Instrument of Chaos')))

    def check_rules(self):
        super(Daemonettes, self).check_rules()
        self.icon.max = self.get_count() / 10
        self.trumpet.max = self.get_count() / 10

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * ((self.models.cur + 10) / 10)
