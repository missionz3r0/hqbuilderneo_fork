__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import ElitesUnit
from builder.games.wh40k8ed.options import UnitDescription, Count, OptionsList
from . import armory, units, wargear
from builder.games.wh40k8ed.utils import *


class Bloodcrushers(ElitesUnit, armory.KhorneUnit):
    type_name = get_name(units.Bloodcrushers)
    type_id = 'bloodcrushers_v1'

    keywords = ['Cavalry']

    member = UnitDescription('Bloodcrusher', options=[Gear('Hellblade'),
                                                      UnitDescription('Juggernaut', options=[Gear('Bladed horn')])])

    class Crushers(Count):
        @property
        def description(self):
            return Bloodcrushers.member.clone().set_count(self.cur - (self.parent.opt.icon.value + self.parent.opt.trumpet.value))

    class Options(OptionsList):
        def __init__(self, parent):
            super(Bloodcrushers.Options, self).__init__(parent, 'Options')
            self.icon = self.variant('Daemonic Icon', points=get_cost(wargear.DaemonicIcon),
                                     gear=[Bloodcrushers.member.clone().add(Gear('Daemonic Icon'))])
            self.trumpet = self.variant('Instrument of Chaos', points=get_cost(wargear.InstrumentOfChaos),
                                        gear=[Bloodcrushers.member.clone().add(Gear('Instrument of Chaos'))])
    
    def __init__(self, parent):
        super(Bloodcrushers, self).__init__(parent, gear=[UnitDescription('Bloodhunter',
                                                                         options=[Gear('Hellblade'),
                                                                         UnitDescription('Juggernaut', options=[Gear('Bladed horn')])])],
                                            points=get_cost(units.Bloodcrushers))
        
        self.models = self.Crushers(self, 'Bloodcrushers', 2, 11, points=get_cost(units.Bloodcrushers), per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return 1 + 7 * ((self.models.cur + 3) / 3)


class Flamers(ElitesUnit, armory.TzeentchUnit):
    type_name = get_name(units.Flamers)
    type_id = 'flamers_v1'

    keywords = ['Infantry', 'Fly']

    def __init__(self, parent):
        super(Flamers, self).__init__(parent, gear=[UnitDescription('Pyrocaster', options=[Gear('Flickering flames')])],
                                      points=get_cost(units.Flamers))
        self.models = Count(self, 'Flamers', 2, 8, points=get_cost(units.Flamers), per_model=True,
                            gear=UnitDescription('Flamer', options=[Gear('Flickering flames')]))

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return 4 * ((self.models.cur + 3) / 3)


class ExFlamer(ElitesUnit, armory.TzeentchUnit):
    type_name = get_name(units.ExaltedFlamer)
    type_id = 'exalted_flamer_v1'
    keywords = ['Character', 'Infantry', 'Flamer', 'Fly']
    power = 5

    def __init__(self, parent):
        super(ExFlamer, self).__init__(parent, gear=[Gear('Fire of Tzeentch'),
                                                     Gear('Tongues of flame')],
                                       static=True, points=get_cost(units.ExaltedFlamer))


class NurgleBeasts(ElitesUnit, armory.NurgleUnit):
    type_name = get_name(units.BeastsOfNurgle)
    type_id = 'nurle_beast_v1'

    keywords = ['Beast']

    def __init__(self, parent):
        super(NurgleBeasts, self).__init__(parent)
        self.models = Count(self, 'Beasts of Nurgle', 1, 9, points=get_cost(units.BeastsOfNurgle), per_model=True,
                            gear=UnitDescription('Beast of Nurgle', options=[Gear('Putrid appendages')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 2 * self.models.cur


class Fiends(ElitesUnit, armory.SlaaneshUnit):
    type_name = get_name(units.FiendsOfSlaanesh)
    type_id = 'sl_fiend_v1'

    keywords = ['Beast']

    def __init__(self, parent):
        super(Fiends, self).__init__(parent)
        self.models = Count(self, 'Fiends of Slaanesh', 1, 9, points=get_cost(units.FiendsOfSlaanesh), per_model=True,
                            gear=UnitDescription('Fiend of Slaanesh', options=[Gear('Dissecting claws'),
                                                                               Gear('Vicious barbed tail')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 2 * self.models.cur
