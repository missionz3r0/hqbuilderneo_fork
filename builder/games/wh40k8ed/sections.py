__author__ = 'Ivan Truskov'

from builder.core2 import section, UnitType, SubRoster
from .unit import Unit


class Section(section.Section):
    def unit_type_add(self, ut, **kwargs):
        from .gen_metadata import complex_keywords
        if self.parent.faction_base in complex_keywords:
            if not ut.always_common:
                try:
                    kwiter = iter(complex_keywords[self.parent.faction_base])
                    while True:
                        kw = next(kwiter)
                        if ut.check_faction(kw):
                            return UnitType(self, ut, visible=not ut.obsolete, group=kw, **kwargs)
                except StopIteration:
                    pass
        return UnitType(self, ut, visible=not ut.obsolete)

    def add_classes(self, class_list):
        class_list.sort(key=lambda cl: cl.type_name)
        for ut in class_list:
            if not self.id in ut.roles:
                continue
            # unit types in detachment are filtered once more by the army's faction
            if any(ut.check_faction(f) for f in [self.parent.faction_base] + self.parent.alternate_factions) and\
               any(ut.check_faction(f) for f in [self.root.faction_base] + self.root.alternate_factions):
                self.unit_type_add(ut)

    def check_rules(self):
        super(Section, self).check_rules()
        relic = 0
        nonrelic = 0
        for ut in self.types:
            flag2 = True
            flag = ut.unit_class.check_faction(self.parent.faction) and ut.unit_class.check_faction(self.root.faction)
            if flag and self.parent.second_faction is not None:
                if any(ut.unit_class.check_faction(f) for f in self.parent.second_variants):
                    flag2 = ut.unit_class.check_faction(self.parent.second_faction)
            ut.active = flag and flag2
            if not flag and ut.count:
                self.error('Cannot take {} as {} in detachment of {} in army of {}'
                           .format(ut.name, self.name, self.parent.faction, self.root.faction))
            if not flag2 and ut.count:
                self.error('Cannot take {} as {} in detachment of {} ({}) in army of {}'
                           .format(ut.name, self.name, self.parent.faction,
                                   self.parent.second_faction, self.root.faction))

            if ut.unit_class.obsolete and ut.count:
                self.error('Unit type {} is marked as obsolete'.format(ut.name))
            if 'RELIC' in ut.unit_class.keywords:
                relic += ut.count
            else:
                nonrelic += ut.count

        if relic > nonrelic:
            self.error('Detachment cannot contain more RELIC then non-RELIC units in any battlefield role')


class HQSection(Section):
    def __init__(self, parent, min_limit=1, max_limit=2):
        super(HQSection, self).__init__(parent, 'hq', 'HQ', min_limit, max_limit)


class ElitesSection(Section):
    def __init__(self, parent, min_limit=0, max_limit=3):
        super(ElitesSection, self).__init__(parent, 'elite', 'Elites', min_limit, max_limit)


class TroopsSection(Section):
    def __init__(self, parent, min_limit=2, max_limit=6):
        super(TroopsSection, self).__init__(parent, 'troops', 'Troops', min_limit, max_limit)


class FastSection(Section):
    def __init__(self, parent, min_limit=0, max_limit=3):
        super(FastSection, self).__init__(parent, 'fast', 'Fast attack', min_limit, max_limit)


class HeavySection(Section):
    def __init__(self, parent, min_limit=0, max_limit=3):
        super(HeavySection, self).__init__(parent, 'heavy', 'Heavy support', min_limit, max_limit)


class LordsOfWarSection(Section):
    def __init__(self, parent, min_limit=0, max_limit=1):
        super(LordsOfWarSection, self).__init__(parent, 'lords', 'Lords of War', min_limit, max_limit)


class TransportSection(Section):
    def __init__(self, parent, min_limit=0, max_limit=None):
        super(TransportSection, self).__init__(parent, 'transports', 'Transport', min_limit, max_limit)


class FlyerSection(Section):
    def __init__(self, parent, min_limit=0, max_limit=3):
        super(FlyerSection, self).__init__(parent, 'fliers', 'Flyers', min_limit, max_limit)


class FortSection(section.Section):
    def __init__(self, parent, min_limit=0, max_limit=3):
        super(FortSection, self).__init__(parent, 'fort', 'Fortifications', min_limit, max_limit)

    def add_classes(self, class_list):
        for ut in class_list:
            UnitType(self, ut)


class DetachmentsSection(section.Section):
    sec_id = 'det'
    sec_name = 'Detachments'

    def build_detach(self, detach_class, detach_faction, *args, **kwargs):
        power = self.parent.use_power
        sec_parent = self.parent

        from builder.core2 import Unit, OneOf
        from .rosters import DetachFort
        from .gen_metadata import excluded_keywords

        class DetachUnit(Unit):
            type_id = detach_class.army_id
            type_name = detach_class.army_name
            faction = detach_faction
            faction_base = detach_class.faction_base
            use_power = power
            obsolete = not issubclass(detach_class, DetachFort) and detach_class.faction_base in excluded_keywords

            def __init__(self, parent):
                super(DetachUnit, self).__init__(parent)

                detach = detach_class(parent=sec_parent)
                detach.parent = self
                self.sub_roster = SubRoster(self, detach)

            def dump(self):
                data = super(DetachUnit, self).dump()
                data['detach_unit'] = True
                return data

            def get_unique_map(self, names_getter):
                return self.sub_roster.roster._build_unique_map(names_getter)

            def build_statistics(self):
                return self.sub_roster.roster.build_statistics()
        if DetachUnit.obsolete:
            kwargs.pop('group', '')
        return UnitType(self, DetachUnit, *args, visible=not DetachUnit.obsolete, **kwargs)

    def __init__(self, parent):

        super(DetachmentsSection, self).__init__(parent, self.sec_id, self.sec_name,
                                                 1, None)

    def check_rules(self):
        super(DetachmentsSection, self).check_rules()
        for ut in self.types:
            if ut.unit_class.obsolete and ut.count:
                self.error('Detachments of {} are no longer supported'.format(ut.unit_class.faction_base))
