from .hq import Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince,\
    DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord,\
    PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer,\
    Warpsmith, Kharn, Haarken, MasterOfPossession,\
    Lucius, Cypher, ExaltedChampion, MasterOfExecutions, LordDiscordant
from .elites import Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute,\
    Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines,\
    PlagueMarinesV2, GreaterPossessed, DarkDisciples
from .troops import ChaosCultists, ChaosSpaceMarines, LegionBerzerkers,\
    LegionPlagues, LegionNoises
from .fast import ChaosBikers, Raptors, WarpTalons, ChaosSpawn
from .heavy import ChaosLandRaider, ChaosPredator, ChaosVindicator,\
    Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, Venomcrawler,\
    Havocsv2
from .transport import ChaosRhino
from .fliers import Heldrake
from .lords import LordOfSkulls
from builder.games.wh40k8ed.sections import HQSection, ElitesSection, UnitType

class ApostleHQ(HQSection):
    def __init__(self, *args, **kwargs):
        super(ApostleHQ, self).__init__(*args, **kwargs)
        self.apostle_types = []

    def unit_type_add(self, ut):
        if ut == DarkApostle:
            self.apostle_types += [super(ApostleHQ, self).unit_type_add(ut)]
        else:
            super(ApostleHQ, self).unit_type_add(ut)

    def count_apostles(self):
        res = 0
        for ut in self.apostle_types:
            res += ut.count
        return res


class DiscipleElites(ElitesSection):
    def __init__(self, *args, **kwargs):
        super(DiscipleElites, self).__init__(*args, **kwargs)
        self.disciple_types = []

    def unit_type_add(self, ut):
        if ut == DarkDisciples:
            self.disciple_types += [super(DiscipleElites, self).unit_type_add(ut, slot=0)]
        else:
            super(DiscipleElites, self).unit_type_add(ut)

    def check_rules(self):
        super(DiscipleElites, self).check_rules()
        disciple_count = sum(float(t.count) for t in self.disciple_types)
        if disciple_count > self.parent.hq.count_apostles():
            self.error("Only one unit of Dark Disciples may be in cluded per Dark Apostle in the army")


unit_types = [Abaddon, BikeLord, BikeSorcerer, ChaosLord,
              DaemonPrince, DarkApostle, DiscLord, DiscSorcerer,
              Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer,
              SteedLord, SteedSorcerer, TermoLord, TermoSorcerer,
              Warpsmith, Berzerkers, ChaosTerminators, Chosen, Fallen,
              Helbrute, Mutilators, NoiseMarines, PlagueMarines,
              Possessed, RubricMarines, ChaosCultists,
              ChaosSpaceMarines, ChaosBikers, Raptors, WarpTalons,
              ChaosLandRaider, ChaosPredator, ChaosVindicator,
              Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators,
              Heldrake, ChaosRhino, LordOfSkulls, Kharn,
              LegionBerzerkers, LegionPlagues, Lucius, LegionNoises,
              Cypher, ChaosSpawn, ExaltedChampion, PlagueMarinesV2,
              Haarken, MasterOfPossession, GreaterPossessed,
              Venomcrawler, MasterOfExecutions, LordDiscordant,
              DarkDisciples, Havocsv2]
