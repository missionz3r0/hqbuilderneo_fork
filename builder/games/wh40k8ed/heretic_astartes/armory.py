__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList
from . import ranged, melee, wargear


class ChaosUnit(Unit):
    faction = ['Chaos']


class ChampionWeapon(OneOf):
    def add_single(self):
        self.single = [
            self.variant(*ranged.Boltgun),
            self.variant(*ranged.CombiBolter),
            self.variant(*ranged.CombiFlamer),
            self.variant(*ranged.CombiMelta),
            self.variant(*ranged.CombiPlasma)
        ]

    def add_double(self, sword=True, fist=True):
        self.double = [
            self.variant(*ranged.BoltPistol),
            self.variant(*melee.Chainaxe)] +\
            ([self.variant(*melee.Chainsword)] if sword else [])
        self.claw = self.variant(*melee.LightningClaws),
        self.double += [self.claw, self.variant(*ranged.PlasmaPistol),
                        self.variant(*melee.PowerAxe)] +\
                        ([self.variant(*melee.PowerFist)] if fist else []) + [
                            self.variant(*melee.PowerMaul),
                            self.variant(*melee.PowerSword)]

    def __init__(self, parent, first=False, sword=True, fist=True):
        super(ChampionWeapon, self).__init__(parent, 'Champion Equipment')
        if first:
            self.add_single()
        self.add_double(sword, fist)
        if not first:
            self.add_single()

    @staticmethod
    def ensure_pairs(first, second):
        for opt in second.single:
            opt.active = not first.cur in first.single
        for opt in first.single:
            opt.active = not second.cur in second.single


def add_melee_weapons(instance):
    instance.variant(*melee.Chainsword)
    instance.variant(*melee.Chainaxe)
    instance.claw = instance.variant(*melee.LightningClaws)
    instance.variant(*melee.PowerAxe)
    instance.variant(*melee.PowerFist)
    instance.variant(*melee.PowerMaul)
    instance.variant(*melee.PowerSword)


def add_pistols(instance):
    instance.variant(*ranged.BoltPistol)
    instance.variant(*ranged.PlasmaPistol)


def add_special_weapons(instance):
    instance.special = [instance.variant(*ranged.Flamer),
                        instance.variant(*ranged.Meltagun),
                        instance.variant(*ranged.PlasmaGun)]


def add_combi_weapons(instance):
    instance.combi = [instance.variant(*ranged.CombiBolter),
                      instance.variant(*ranged.CombiFlamer),
                      instance.variant(*ranged.CombiMelta),
                      instance.variant(*ranged.CombiPlasma)]


def add_heavy_weapons(instance, auto_last=False, fallen=False):
    instance.heavy = ([] if auto_last else [instance.variant(*ranged.Autocannon)]) +\
                      [instance.variant(*ranged.HeavyBolter),
                       instance.variant(*ranged.Lascannon),
                       instance.variant(*ranged.MissileLauncher)] +\
                      ([] if not auto_last else [instance.variant(*ranged.Autocannon)]) +\
                      ([] if fallen else [instance.variant(*ranged.ReaperChainCannon)])


def add_terminator_melee(instance, sword=True, axe=True):
    instance.variant(*melee.Chainfist)
    instance.claw = instance.variant(*melee.LightningClaws)
    if axe:
        instance.variant(*melee.PowerAxe)
    instance.variant(*melee.PowerFist)
    instance.variant(*melee.PowerMaul)
    if sword:
        instance.variant(*melee.PowerSword)


class ClawUser(Unit):
    def build_points(self):
        res = super(ClawUser, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 4
        return res    


class IconOfChaos(OptionsList):
    def __init__(self, parent):
        super(IconOfChaos, self).__init__(parent, 'Icon of Chaos', limit=1)
        self.kh = self.variant('Icon of Wrath', 10)
        self.tz = self.variant(*wargear.IconOfFlame)
        self.ng = self.variant('Icon of Despair', 10)
        self.sl = self.variant('Icon of Excess', 10)
        self.un = self.variant(*wargear.IconOfVengeance)

    def check_rules(self):
        super(IconOfChaos, self).check_rules()        
        # should not change during the lifetime
        visible_factions = set(self.parent.faction)
        # visible_factions.add(*)
        visible_factions.add(self.parent.roster.faction)
        visible_factions.add(self.parent.root.faction)

        self.kh.visible = self.kh.used = not (any(f in visible_factions for f in [
            'NURGLE', 'TZEENTCH', 'SLAANESH', "EMPEROR'S CHILDREN"
        ]) or 'PSYKER' in self.parent.keywords)
        self.tz.visible = self.tz.used = not any(f in visible_factions for f in [
            'NURGLE', 'KHORNE', 'SLAANESH', 'WORLD EATERS'
        ])
        self.ng.visible = self.ng.used = not any(f in visible_factions for f in [
            'KHORNE', 'TZEENTCH', 'SLAANESH', 'WORLD EATERS', "EMPEROR'S CHILDREN"
        ])
        self.sl.visible = self.sl.used = not any(f in visible_factions for f in [
            'NURGLE', 'TZEENTCH', 'KHORNE', 'WORLD EATERS'
        ])
        self.un.visible = not any(f in visible_factions for f in [
            'NURGLE', 'TZEENTCH', 'SLAANESH', 'KHORNE', 'WORLD EATERS', "EMPEROR'S CHILDREN"
        ])


class AstartesUnit(Unit):
    wiki_faction = 'Chaos Space Marines'
    faction = ['Chaos', 'Heretic Astartes']


gods = ['KHORNE', 'TZEENTCH', 'NURGLE', 'SLAANESH']


class LegionUnit(AstartesUnit):
    faction = ['<Legion>']

    @classmethod
    def calc_faction(cls):
        return ['BLACK LEGION', 'IRON WARRIORS', 'WORD BEARERS', 'NIGHT LORDS', 'ALPHA LEGION', 'RED CORSAIRS', 'RENEGADES']


class CultLegionUnit(LegionUnit):
    @classmethod
    def calc_faction(cls):
        return super(CultLegionUnit, cls).calc_faction() + ['WORLD EATERS', "EMPEROR'S CHILDREN"] + gods
