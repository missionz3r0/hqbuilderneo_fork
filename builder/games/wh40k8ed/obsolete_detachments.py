from .rosters import *

from .adepta_sororitas import *
from .craftworld import *
from .chaos_other import *
from .heretic_astartes import *
from .drukhari import *
from .aeldari_other import *
from .chaos_demons import *
from .imperium_other import *
from .astra_militarum import *
from .adeptus_mechanicum import *
from .space_marines import *
from .blood_angels import *
from .dark_angels import *
from .space_wolves import *
from .deathwatch import *
from .tyranids import *
from .genestealer_cult import *
from .death_guard import *
from .adeptus_custodes import *
from .thousand_sons import *
from .imperial_knights import *
from .grey_knights import *

class DetachPatrol_imperium(DetachPatrol):
    army_name = 'Imperium (Patrol detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'patrol_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'SPACE WOLVES', 'BLACK TEMPLARS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'ARMAGEDDON', 'GRAIA', 'CULT MECHANICUS', 'BLOOD ANGELS', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'LUCIUS', '<REGIMENT>', 'FLESH TEARERS', 'VOSTROYAN', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'MARS', 'RAVEN GUARD', 'ADEPTUS CUSTODES', '<CHAPTER>', '<FORGE WORLD>', 'GREY KNIGHTS', 'ADEPTUS MECHANICUS', 'CATACHAN', 'BLOOD BROTHERS', 'DEATHWATCH', 'VALHALLAN', 'WHITE SCARS', 'ADEPTUS MINISTORUM', 'MILITARUM TEMPESTUS', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachPatrol_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachBatallion_imperium(DetachBatallion):
    army_name = 'Imperium (Batallion detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'batallion_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'SPACE WOLVES', 'BLACK TEMPLARS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'ARMAGEDDON', 'GRAIA', 'CULT MECHANICUS', 'BLOOD ANGELS', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'LUCIUS', '<REGIMENT>', 'FLESH TEARERS', 'VOSTROYAN', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'MARS', 'RAVEN GUARD', 'ADEPTUS CUSTODES', '<CHAPTER>', '<FORGE WORLD>', 'GREY KNIGHTS', 'ADEPTUS MECHANICUS', 'CATACHAN', 'BLOOD BROTHERS', 'DEATHWATCH', 'VALHALLAN', 'WHITE SCARS', 'ADEPTUS MINISTORUM', 'MILITARUM TEMPESTUS', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachBatallion_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachBrigade_imperium(DetachBrigade):
    army_name = 'Imperium (Brigade detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'brigade_imperium'
    army_factions = ['IMPERIUM', 'SPACE WOLVES', 'BLACK TEMPLARS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'ARMAGEDDON', 'GRAIA', 'BLOOD ANGELS', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'LUCIUS', '<REGIMENT>', 'FLESH TEARERS', 'VOSTROYAN', 'ULTRAMARINES', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'RAVEN GUARD', 'ADEPTUS CUSTODES', '<CHAPTER>', '<FORGE WORLD>', 'GREY KNIGHTS', 'ADEPTUS MECHANICUS', 'CATACHAN', 'BLOOD BROTHERS', 'MARS', 'VALHALLAN', 'WHITE SCARS', 'DEATHWATCH', 'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachBrigade_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachVanguard_imperium(DetachVanguard):
    army_name = 'Imperium (Vanguard detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'vanguard_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'BLOOD BROTHERS', '<CHAPTER>', 'BLOOD ANGELS', 'CHAOS', 'BLACK TEMPLARS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'DEATHWING', 'GRAIA', 'ASTRA TELEPATHICA', 'ARMAGEDDON', '<ORDO>', 'INQUISITION', 'SPACE WOLVES', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'CULT MECHANICUS', '<REGIMENT>', 'FLESH TEARERS', 'LUCIUS', 'VOSTROYAN', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', 'DEATH COMPANY', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'MARS', 'RAVEN GUARD', 'ORDO XENOS', 'ADEPTUS CUSTODES', 'RAVENWING', 'ORDO HERETICUS', 'SCHOLASTICA PSYKANA', '<FORGE WORLD>', 'GREY KNIGHTS', 'OFFICIO PREFECTUS', 'ADEPTUS MECHANICUS', 'ADEPTUS MINISTORUM', 'CATACHAN', 'ORDO MALLEUS', 'DEATHWATCH', 'VALHALLAN', 'WHITE SCARS', 'ULTRAMARINES', 'FALLEN', 'MILITARUM TEMPESTUS']

    def __init__(self, parent=None):
        super(DetachVanguard_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSpearhead_imperium(DetachSpearhead):
    army_name = 'Imperium (Spearhead detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'spearhead_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'SPACE WOLVES', 'BLACK TEMPLARS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'ARMAGEDDON', 'GRAIA', 'CULT MECHANICUS', 'BLOOD ANGELS', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'LUCIUS', '<REGIMENT>', 'FLESH TEARERS', 'VOSTROYAN', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'MARS', 'RAVEN GUARD', 'ADEPTUS CUSTODES', '<CHAPTER>', '<FORGE WORLD>', 'GREY KNIGHTS', 'ADEPTUS MECHANICUS', 'CATACHAN', 'BLOOD BROTHERS', 'DEATHWATCH', 'VALHALLAN', 'WHITE SCARS', 'ADEPTUS MINISTORUM', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSpearhead_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachOutrider_imperium(DetachOutrider):
    army_name = 'Imperium (Outrider detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'outrider_imperium'
    hq_sec = GuardHQ
    elite_sec = GuardElites
    army_factions = ['IMPERIUM', 'RAVENWING', 'SPACE WOLVES', 'BLACK TEMPLARS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'ARMAGEDDON', 'GRAIA', 'BLOOD ANGELS', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'LUCIUS', '<REGIMENT>', 'FLESH TEARERS', 'VOSTROYAN', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'MARS', 'RAVEN GUARD', 'ADEPTUS CUSTODES', '<CHAPTER>', '<FORGE WORLD>', 'GREY KNIGHTS', 'ADEPTUS MECHANICUS', 'CATACHAN', 'BLOOD BROTHERS', 'DEATHWATCH', 'VALHALLAN', 'WHITE SCARS', 'ADEPTUS MINISTORUM', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachOutrider_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachCommand_imperium(DetachCommand):
    army_name = 'Imperium (Supreme command detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'command_imperium'
    army_factions = ['IMPERIUM', 'RAVENWING', 'BLOOD ANGELS', 'CHAOS', 'BLACK TEMPLARS', 'MILITARUM TEMPESTUS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'ARMAGEDDON', 'GRAIA', 'ASTRA TELEPATHICA', 'CULT MECHANICUS', '<ORDO>', 'INQUISITION', 'DEATHWING', 'SPACE WOLVES', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'LUCIUS', '<REGIMENT>', 'ORDO MALLEUS', 'VOSTROYAN', 'ULTRAMARINES', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', 'DEATH COMPANY', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'FLESH TEARERS', 'RAVEN GUARD', 'ORDO XENOS', 'ADEPTUS CUSTODES', '<CHAPTER>', 'ADEPTUS MECHANICUS', 'SCHOLASTICA PSYKANA', '<FORGE WORLD>', 'GREY KNIGHTS', 'ORDO HERETICUS', 'CATACHAN', 'BLOOD BROTHERS', 'DEATHWATCH', 'MARS', 'VALHALLAN', 'WHITE SCARS', 'OFFICIO PREFECTUS', 'FALLEN', 'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachCommand_imperium, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, Guilliman, TerminusUltra])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachSuperHeavy_imperium(DetachSuperHeavy):
    army_name = 'Imperium (Super-Heavy detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'super_heavy_imperium'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'QUESTOR IMPERIALIS', 'ADEPTUS ASTARTES', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'QUESTOR MECHANICUS', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_imperium, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, Guilliman, TerminusUltra])
        return None


class DetachSuperHeavyAux_imperium(DetachSuperHeavyAux):
    army_name = 'Imperium (Super-Heavy auxilary detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'super_heavy_aux_imperium'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'QUESTOR IMPERIALIS', 'ADEPTUS ASTARTES', 'CATACHAN', 'CADIAN', 'ASTRA MILITARUM', '<REGIMENT>', 'BLOOD BROTHERS', 'MORDIAN', 'QUESTOR MECHANICUS', 'VALHALLAN', 'TALLARN', 'VOSTROYAN', 'ARMAGEDDON', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_imperium, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives, Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword, Stormlord, Stormsword, KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, Guilliman, TerminusUltra])
        return None


class DetachAirWing_imperium(DetachAirWing):
    army_name = 'Imperium (Air Wing detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'air_wing_imperium'
    army_factions = ['IMPERIUM', '<DARK ANGELS SUCCESSORS>', '<BLOOD ANGELS SUCCESSORS>', 'BLOOD ANGELS', 'ADEPTUS ASTARTES', 'AERONAUTICA IMPERIALIS', 'RAVEN GUARD', 'GREY KNIGHTS', 'DARK ANGELS', 'ASTRA MILITARUM', 'BLACK TEMPLARS', 'FLESH TEARERS', 'SALAMANDERS', 'DEATHWATCH', '<CHAPTER>', 'CRIMSON FISTS', 'WHITE SCARS', 'RAVENWING', 'IMPERIAL FISTS', 'SPACE WOLVES', 'ULTRAMARINES']

    def __init__(self, parent=None):
        super(DetachAirWing_imperium, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        return None


class DetachAuxilary_imperium(DetachAuxilary):
    army_name = 'Imperium (Auxilary Support Detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'Auxilary_imperium'
    army_factions = ['IMPERIUM']

    def __init__(self, parent=None):
        super(DetachAuxilary_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, DAStormravenGunship, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


    
    
class DetachPatrol_chaos(DetachPatrol):
    army_name = 'Chaos (Patrol detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'patrol_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'KHORNE', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachPatrol_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBatallion_chaos(DetachBatallion):
    army_name = 'Chaos (Batallion detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'batallion_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'KHORNE', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachBatallion_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachBrigade_chaos(DetachBrigade):
    army_name = 'Chaos (Brigade detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'brigade_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'SLAANESH', "EMPEROR'S CHILDREN", 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'KHORNE', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachBrigade_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachVanguard_chaos(DetachVanguard):
    army_name = 'Chaos (Vanguard detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'vanguard_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'IMPERIUM', 'RED CORSAIRS', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'DAEMON', 'DEATH GUARD', 'CHAOS', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'KHORNE', 'FALLEN', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachVanguard_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSpearhead_chaos(DetachSpearhead):
    army_name = 'Chaos (Spearhead detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'spearhead_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'KHORNE', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachSpearhead_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachOutrider_chaos(DetachOutrider):
    army_name = 'Chaos (Outrider detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'outrider_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORD BEARERS', 'ALPHA LEGION', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'HERETIC ASTARTES', 'SLAANESH', 'NURGLE', 'WORLD EATERS', 'IRON WARRIORS', 'THOUSAND SONS', 'KHORNE', 'RENEGADES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachOutrider_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachCommand_chaos(DetachCommand):
    army_name = 'Chaos (Supreme command detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'command_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'IMPERIUM', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'DAEMON', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'KHORNE', 'SLAANESH', 'THOUSAND SONS', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'FALLEN', 'HERETIC ASTARTES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachCommand_chaos, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([RenegadeKnight, LordOfSkulls, Mortarion, Magnus])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_chaos(DetachSuperHeavy):
    army_name = 'Chaos (Super-Heavy detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'super_heavy_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'QUESTOR TRAITORIS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'KHORNE', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_chaos, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([RenegadeKnight, LordOfSkulls, Mortarion, Magnus])
        return None


class DetachSuperHeavyAux_chaos(DetachSuperHeavyAux):
    army_name = 'Chaos (Super-Heavy auxilary detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'super_heavy_aux_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'QUESTOR TRAITORIS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'KHORNE', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'RED CORSAIRS', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_chaos, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([RenegadeKnight, LordOfSkulls, Mortarion, Magnus])
        return None


class DetachAirWing_chaos(DetachAirWing):
    army_name = 'Chaos (Air Wing detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'air_wing_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'RED CORSAIRS', 'SLAANESH', 'THOUSAND SONS', 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'KHORNE', 'HERETIC ASTARTES', "EMPEROR'S CHILDREN"]

    def __init__(self, parent=None):
        super(DetachAirWing_chaos, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Heldrake])
        return None


class DetachAuxilary_chaos(DetachAuxilary):
    army_name = 'Chaos (Auxilary Support Detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'Auxilary_chaos'
    army_factions = ['CHAOS']

    def __init__(self, parent=None):
        super(DetachAuxilary_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None



class DetachPatrol_aeldari(DetachPatrol):
    army_name = 'Aeldari (Patrol detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'patrol_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['DRUKHARI', 'SAIM-HANN', 'ASPECT WARRIOR', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', '<MASCUE>', 'BIEL-TAN', 'WARHOST', '<WYCH CULT>', 'IYANDEN']

    def __init__(self, parent=None):
        super(DetachPatrol_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachBatallion_aeldari(DetachBatallion):
    army_name = 'Aeldari (Batallion detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'batallion_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['DRUKHARI', 'SAIM-HANN', 'ASPECT WARRIOR', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', '<MASCUE>', 'BIEL-TAN', 'WARHOST', '<WYCH CULT>', 'IYANDEN']

    def __init__(self, parent=None):
        super(DetachBatallion_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachBrigade_aeldari(DetachBrigade):
    army_name = 'Aeldari (Brigade detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'brigade_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['SAIM-HANN', 'AELDARI', '<MASCUE>', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'HARLEQUINS', 'ASPECT WARRIOR', 'BIEL-TAN', 'WARHOST', 'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachBrigade_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachVanguard_aeldari(DetachVanguard):
    army_name = 'Aeldari (Vanguard detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'vanguard_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'SAIM-HANN', 'SPIRIT HOST', 'ASPECT WARRIOR', 'AELDARI', 'DRUKHARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', 'INCUBI', '<CRAFTWORLD>', 'ALATOIC', '<WYCH CULT>', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', '<MASCUE>', 'BIEL-TAN', 'WARHOST', 'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachVanguard_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachSpearhead_aeldari(DetachSpearhead):
    army_name = 'Aeldari (Spearhead detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'spearhead_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'DRUKHARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', '<HAEMUNCULUS COVEN>', 'ALATOIC', 'WARHOST', '<MASCUE>', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSpearhead_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachOutrider_aeldari(DetachOutrider):
    army_name = 'Aeldari (Outrider detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'outrider_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    fast_sec = BeastFast
    army_factions = ['DRUKHARI', 'SAIM-HANN', 'IYANDEN', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'AELDARI', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ASPECT WARRIOR', 'YNNARI', 'HARLEQUINS', 'ALATOIC', '<MASCUE>', 'BIEL-TAN', 'WARHOST', '<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachOutrider_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachCommand_aeldari(DetachCommand):
    army_name = 'Aeldari (Supreme command detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'command_aeldari'
    hq_sec = ArchonHQ
    elite_sec = DEElites
    army_factions = ['<HAEMUNCULUS COVEN>', 'SAIM-HANN', 'SPIRIT HOST', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'AELDARI', 'THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'INCUBI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', '<WYCH CULT>', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ASPECT WARRIOR', '<MASCUE>', 'BIEL-TAN', 'WARHOST', 'DRUKHARI']

    def __init__(self, parent=None):
        super(DetachCommand_aeldari, self).__init__(*[], **{'lords': True, 'hq': True, 'elite': True, 'transports': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachSuperHeavy_aeldari(DetachSuperHeavy):
    army_name = 'Aeldari (Super-Heavy detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'super_heavy_aeldari'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_aeldari, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachSuperHeavyAux_aeldari(DetachSuperHeavyAux):
    army_name = 'Aeldari (Super-Heavy auxilary detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'super_heavy_aux_aeldari'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'YNNARI', 'ALATOIC', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_aeldari, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([Wraithknight])
        return None


class DetachAirWing_aeldari(DetachAirWing):
    army_name = 'Aeldari (Air Wing detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'air_wing_aeldari'
    army_factions = ['SAIM-HANN', 'SPIRIT HOST', 'AELDARI', 'CULT OF STRIFE', 'CULT OF THE CURSED BLADE', 'CULT OF THE RED GRIEF', 'DRUKHARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', 'YNNARI', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ASPECT WARRIOR', 'BIEL-TAN', '<WYCH CULT>']

    def __init__(self, parent=None):
        super(DetachAirWing_aeldari, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        return None


class DetachAuxilary_aeldari(DetachAuxilary):
    army_name = 'Aeldari (Auxilary Support Detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'Auxilary_aeldari'
    army_factions = ['AELDARI']

    def __init__(self, parent=None):
        super(DetachAuxilary_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None




class DetachPatrol_tyranids(DetachPatrol):
    army_name = 'Tyranids (Patrol detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'patrol_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachPatrol_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachBatallion_tyranids(DetachBatallion):
    army_name = 'Tyranids (Batallion detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'batallion_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachBatallion_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachBrigade_tyranids(DetachBrigade):
    army_name = 'Tyranids (Brigade detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'brigade_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachBrigade_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachVanguard_tyranids(DetachVanguard):
    army_name = 'Tyranids (Vanguard detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'vanguard_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachVanguard_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachSpearhead_tyranids(DetachSpearhead):
    army_name = 'Tyranids (Spearhead detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'spearhead_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachSpearhead_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachOutrider_tyranids(DetachOutrider):
    army_name = 'Tyranids (Outrider detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'outrider_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachOutrider_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachCommand_tyranids(DetachCommand):
    army_name = 'Tyranids (Supreme command detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'command_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachCommand_tyranids, self).__init__(*[], **{'transports': True, 'hq': True, 'elite': True, 'parent': parent, })
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        return None


class DetachAirWing_tyranids(DetachAirWing):
    army_name = 'Tyranids (Air Wing detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'air_wing_tyranids'
    army_factions = ['JORMUGAND', 'BEHEMOTH', 'GORGON', 'KRAKEN', '<HIVE FLEET>', 'HYDRA', 'TYRANIDS', 'KRONOS', 'LEVIATHAN']

    def __init__(self, parent=None):
        super(DetachAirWing_tyranids, self).__init__(*[], **{'fliers': True, 'parent': parent, })
        self.fliers.add_classes([Harpy, HiveCrone])
        return None


class DetachAuxilary_tyranids(DetachAuxilary):
    army_name = 'Tyranids (Auxilary Support Detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'Auxilary_tyranids'
    army_factions = ['TYRANIDS']

    def __init__(self, parent=None):
        super(DetachAuxilary_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None




class DetachPlanetstrikeAttacker_tyranids(DetachPlanetstrikeAttacker):
    army_name = 'Tyranids (Planetstrike Attacker detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'planetstrike attacker_tyranids'
    army_factions = ['HYDRA', 'JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'KRAKEN', '<HIVE FLEET>', 'TYRANIDS', 'KRONOS', 'GORGON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachPlanetstrikeDefender_tyranids(DetachPlanetstrikeDefender):
    army_name = 'Tyranids (Planetstrike Defender detachment)'
    faction_base = 'TYRANIDS'
    alternate_factions = []
    army_id = 'planetstrike attacker_tyranids'
    army_factions = ['HYDRA', 'IMPERIUM', 'JORMUGAND', 'BEHEMOTH', 'GENESTEALER CULTS', 'KRAKEN', 'ASTRA MILITARUM', '<HIVE FLEET>', 'TYRANIDS', 'KRONOS', 'GORGON']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_tyranids, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc, Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks, CultLemanRuss, GoliathRockgrinder])
        self.troops.add_classes([TyranidWarriorBrood, Genestealers, Termagant, Hormagant, Ripper, AcolyteHybrids, NeophyteHybrids])
        self.fliers.add_classes([Harpy, HiveCrone])
        self.elite.add_classes([GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope, Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror, HybridMetamorphs, Aberrants, PurestrainGenestealers])
        self.hq.add_classes([HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime, Tervigon, Neurothrope, Patriarch, Magus, Primus, Iconward, Abominant])
        self.fast.add_classes([TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle, MucolidSpores, SporeMine, CultScoutSentinelSquad, CultArmouredSentinelSquad])
        self.transports.add_classes([Tyrannocyte, GoliathTruck, CultChimera])
        return None


class DetachPlanetstrikeAttacker_aeldari(DetachPlanetstrikeAttacker):
    army_name = 'Aeldari (Planetstrike Attacker detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'planetstrike attacker_aeldari'
    army_factions = ['SAIM-HANN', 'AELDARI', 'DRUKHARI', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', 'ALATOIC', '<WYCH CULT>', 'YNNARI', 'HARLEQUINS', 'ASPECT WARRIOR', '<MASCUE>', 'BIEL-TAN', 'WARHOST', 'WYCH CULT OF STRIFE']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None


class DetachPlanetstrikeDefender_aeldari(DetachPlanetstrikeDefender):
    army_name = 'Aeldari (Planetstrike Defender detachment)'
    faction_base = 'AELDARI'
    alternate_factions = []
    army_id = 'planetstrike attacker_aeldari'
    army_factions = ['THE PROPHETS OF FLESH', 'THE DARK CREED', 'COVEN OF TWELVE', 'SAIM-HANN', 'AELDARI', '<MASCUE>', 'IYANDEN', 'ASURYANI', 'ULTHWE', '<CRAFTWORLD>', '<HAEMUNCULUS COVEN>', 'YNNARI', 'HARLEQUINS', 'KABAL OF THE BLACK HEART', 'KABAL OF THE FLAYED SKULL', 'KABAL OF THE POISONED TONGUE', 'KABAL OF THE OBSIDIAN ROSE','<KABAL>', 'ASPECT WARRIOR', 'ALATOIC', 'WARHOST', 'DRUKHARI', 'BIEL-TAN']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_aeldari, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Falcon, FirePrism, NightSpinner, Reapers, VaulWrath, Walkers, Wraithlord, Cronos, Ravager, Talos, Voidweavers])
        self.troops.add_classes([DireAvengers, GuardianDefenders, Rangers, StormGuardians, KabaliteWarriors, Wracks, Wyches, Troupe])
        self.fliers.add_classes([CrimsonHunter, HunterExarch, Hemlock, Razorwing, Voidraven])
        self.elite.add_classes([Banshees, Dragons, Scorpions, Wraithblades, Wraithguard, Bonesinger, Beastmaster, Bloodbrides, Grotesques, Incubi, KabaliteTrueborn, Lhamaean, Mandrakes, Medusae, Sslyth, UrGhul, DeathJester, Solitaire])
        self.hq.add_classes([Asurmen, Autarch, Avatar, Baharroth, BikerAutarch, BikerFarseer, BikerWarlock, BikerWarlockConclave, Eldrad, Farseer, Fuegan, Illic, JainZar, JumpingAutarch, Karandras, Maugan, Spiritseer, SwoopingAutarch, Warlock, WarlockConclave, Yriel, Archon, Drazhar, Haemunculus, Lelith, Rakarth, Succubus, TroupeMaster, Shadowseer, Yvraine, Visarch, Yncarne])
        self.fast.add_classes([Hawks, Spears, Spiders, Vypers, Windriders, ClawedFiends, Hellions, Khymerae, RazorwingFlocks, Reavers, Scourges, Skyweavers])
        self.transports.add_classes([WaveSerpent, Raider, Venom, Starweaver])
        return None




class DetachPlanetstrikeAttacker_imperium(DetachPlanetstrikeAttacker):
    army_name = 'Imperium (Planetstrike Attacker detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'planetstrike attacker_imperium'
    army_factions = ['IMPERIUM', '<CHAPTER>', 'SPACE WOLVES', 'BLACK TEMPLARS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'ARMAGEDDON', 'GRAIA', 'BLOOD ANGELS', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'LUCIUS', '<REGIMENT>', 'FLESH TEARERS', 'VOSTROYAN', 'ULTRAMARINES', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'RAVEN GUARD', 'ADEPTUS CUSTODES', 'RAVENWING', '<FORGE WORLD>', 'GREY KNIGHTS', 'ADEPTUS MECHANICUS', 'CATACHAN', 'BLOOD BROTHERS', 'MARS', 'VALHALLAN', 'WHITE SCARS', 'DEATHWATCH', 'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None


class DetachPlanetstrikeDefender_imperium(DetachPlanetstrikeDefender):
    army_name = 'Imperium (Planetstrike Defender detachment)'
    faction_base = 'IMPERIUM'
    alternate_factions = []
    army_id = 'planetstrike attacker_imperium'
    army_factions = ['IMPERIUM', 'SPACE WOLVES', 'BLACK TEMPLARS', 'AGRIPINAA', 'SALAMANDERS', 'MORDIAN', 'METALICA', 'CRIMSON FISTS', 'TALLARN', 'ARMAGEDDON', 'GRAIA', 'CULT MECHANICUS', 'BLOOD ANGELS', '<BLOOD ANGELS SUCCESSORS>', 'ADEPTUS ASTARTES', 'STYGIES VIII', 'DARK ANGELS', 'ASTRA MILITARUM', 'LUCIUS', '<REGIMENT>', 'FLESH TEARERS', 'VOSTROYAN', 'ULTRAMARINES', 'IMPERIAL FISTS', 'ADEPTA SORORITAS', 'RYZA', '<DARK ANGELS SUCCESSORS>', '<ORDER>', 'CADIAN', 'RAVEN GUARD', 'ADEPTUS CUSTODES', '<CHAPTER>', '<FORGE WORLD>', 'GREY KNIGHTS', 'ADEPTUS MECHANICUS', 'CATACHAN', 'BLOOD BROTHERS', 'MARS', 'VALHALLAN', 'WHITE SCARS', 'DEATHWATCH', 'ADEPTUS MINISTORUM']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_imperium, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([Exorcist, Retributors, PenitentEngines, Basilisks, Deathstrike, HeavyWeaponSquad, Hydras, LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns, KastelanManiple, Dunecrawler, CenturionDevastators, Devastators, Hellblasters, Hunter, LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Predator, Stalker, Thunderfire, Vindicator, Whirlwind, CodexDevastators, CodexPredator, CodexVindicator, CodexWhirlwind, BADevastators, BaalPredator, LongFangs, Dreadknight, PurgationSquad, GKLandRaider, GKLandRaiderCrusader, GKLandRaiderRedeemer, VenLandRaider])
        self.troops.add_classes([BattleSisters, CustodianSquad, Conscripts, InfantrySquad, TempestusSquad, Breachers, KatDestroyers, SkitariiRangers, Vanguard, CrusaderSquad, Intercessors, ScoutSquad, TacticalSquad, CodexTacticalSquad, CodexScoutSquad, BATacticalSquad, BAScoutSquad, BAIntercessors, BATacticalSquadV2, BAScoutSquadV2, DATacticalSquad, DAScoutSquad, BloodClaws, GreyHunters, SWIntercessors, KillTeam, DWIntercessors, StrikeSquad, GKTerminatorSquad, CustodianGuard])
        self.fliers.add_classes([Valkyries, Stormhawk, StormravenGunship, Stormtalon, RavenwingDarkTalon, NephilimJetfighter, Stormwolf, Stormfang, Corvus, GKStormhawk, GKStormtalon, GKStormravenGunship])
        self.elite.add_classes([ArcoFlagellants, Assassins, Celestians, Dialogus, Hospitaller, Imagifier, Mistress, Priest, Repentia, Fallen, VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin, Acolytes, Jokaero, Daemonhost, Prosecutors, Vigilators, Witchseekers, Bullgryns, CommandSquad, Commissar, Harker, Kell, Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad, TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes, Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors, Enginseer, Fulgurites, Corpuscarii, Datasmith, AMServitors, Infiltrators, Ruststalkers, Apothecary, BikeApothecary, BikeCompanyAncient, BikeCompanyChampion, BikerCompanyVeterans, CataphractiiTerminatorSquad, Cenobytes, CenturionAssault, ChapterAncient, ChapterChampion, CompanyAncient, CompanyChampion, CompanyVeterans, ContDreadnought, Dreadnought, HonourGuard, VitrixGuard, ImperialSM, IronDred, PrimarisAncient, Servitors, SternguardVeteranSquad, TartarosTerminatorSquad, TerminatorAssaultSquad, TerminatorSquad, TheDamned, TyrannicWarVeterans, VanguardVeteranSquad, VenDreadnought, RedemptorDreadnought, PrimarisApothecary, Reivers, Agressors, CodexCompanyAncient, CodexHonourGuard, CodexChapterChampion, CodexSternguardVeteranSquad, CodexVanguardVeteranSquad, CodexDreadnought, CodexVenDreadnought, CodexTerminatorSquad, CodexTerminatorAssaultSquad, BACompanyAncient, BACompanyChampion, BACompanyVeterans, SanguinaryNovitiate, BASternguardVeteranSquad, BAVanguardVeteranSquad, SanguinaryAncient, SanguinaryGuard, TerminatorAncient, DeathCompany, DeathCompanyDreadnought, FuriosoDreadnought, BATerminatorAssaultSquad, BACompanyChampionIndex, BACompanyAncientIndex, BAJumpCompanyVeterans, SanguinaryNovitiateIndex, BADreadnought, DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient, DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary, RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient, DAChapterAncient, DACodexDreadnought, DACodexVenDreadnought, DAContDreadnought, SWDreadnought, SWVenDreadnought, Lucas, WolfScouts, Wulfen, LoneWolf, TermLoneWolf, Murderfang, WolfGuards, BikeWolfGuards, WolfGuardTerminators, SWCompanyAncient, DWDreadnought, DWVenDreadnought, DWTerminators, DWVanguard, PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, GKVenDreadnought, GKServitors, CustodianWardens, AllarusVexilus, Vexilus, AllarusCustodians, VenerableContemptor])
        self.hq.add_classes([Celestine, Canoness, Jacobus, Cypher, Greyfax, Eisenhorn, Karamazov, Coteaz, Inquisitor, TermoMalleus, CCommander, Creed, LordCommissar, Pask, Primaris, Straken, SlyMarbo, TankCommander, TempestorPrime, Yarrik, Cawl, Dominus, EnginseerV2, ArtCalgar, PrimarisCalgar, BikeCaptain, BikeChaplain, BikeKhan, BikeLibrarian, BikeTechmarine, Calgar, Captain, Cassius, CataphractiiCaptain, Champion, Chaplain, Chronus, GravisCaptain, Grimaldus, Helbrecht, Kantor, Khan, LandRaiderExcelsior, Librarian, PrimarisLieutenants, Lysander, RhinoPrimaris, Shrike, Sicarius, Techmarine, Telion, TerminatorCaptain, TerminatorChaplain, TermoLibrarian, Tigurius, Vulkan, PrimarisCaptain, PrimarisChaplain, PrimarisLibrarian, CodexCaptain, CodexTerminatorCaptain, CodexCataphractiiCaptain, CodexBikeCaptain, CodexTermoLibrarian, CodexTechmarine, CodexChaplain, Lieutenants, BABikeCaptain, BACaptain, BATerminatorCaptain, BAChaplainV1, BALibrarian, BABikeLibrarian, BATechmarine, BABikeTechmarine, BACataphractiiCaptain, Dante, BAPrimarisCaptain, BAChaplainV2, BATechmarineV2, SanguinaryPriestV2, BALieutenants, Tycho, DCTycho, LibrarianDreadnought, Mephiston, Sanguinor, Astorath, SanguinaryPriest, BikeSanguinaryPriest, Corbulo, Lemartes, Seth, CompanyMaster, Azrael, Belial, Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain, Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster, GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain, DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants, Bjorn, Arjac, BikeRunePriest, BikeWolfLord, BikeWolfPriest, Canis, GravisWolfLord, Harald, Krom, Logan, Njal, PimpLogan, Ragnar, RunePriest, TermRunePriest, TermWolfLord, TerminatorWolfPriest, TermoNjal, ThunderwolfLord, Ulrik, WolfLord, WolfPriest, WGBattleLeader, TermWGBattleLeader, WolfWGBattleLeader, BikeWGBattleLeader, IronPriest, BikeIronPriest, WolfIronPriest, DWCaptain, TerminatorDWCaptain, DWLibrarian, TermoDWLibrarian, DWChaplain, WatchMaster, Artemis, Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, BrotherhoodChampion, GKChaplain, GKLibrarian, Valoris, ShieldCaptain, AllarusShieldCaptain, BikeShieldCaptain])
        self.fast.add_classes([Dominions, Seraphims, ArmouredSentinels, Hellhounds, RoughRiders, ScoutSentinels, Dragoons, Ironstriders, AssaultSquad, AttackBikeSquad, BikeSquad, Inceptors, LandSpeeders, ScoutBikers, CodexAssaultSquad, CodexLandSpeeders, BAAssaultSquad, BABikeSquad, BAScoutBikers, BAAttackBikes, RavenwingBikeSquad, DAScoutBikers, RavenwingAttackBikeSquad, RavenwingSpeeders, RavenwingDarkShroud, RavenwingBlackKnights, DACodexAssaultSquad, LandSpeederVengeance, Cyberwolves, Swiftclaws, SwiftclawAttackBikes, ThunderwolfCavalry, Skyclaws, WolfPack, DWBikers, InterceptorSquad, VertusPraetors])
        self.transports.add_classes([ASRhino, Immolator, NullRhino, Chimera, Taurox, TauroxPrime, Razorback, Rhino, LandSpeederStorm, DropPod, CodexDropPod, CodexRazorback, Repulsor, GKRhino, GKRazorback])
        return None

class DetachPlanetstrikeAttacker_chaos(DetachPlanetstrikeAttacker):
    army_name = 'Chaos (Planetstrike Attacker detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'planetstrike attacker_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'SLAANESH', "EMPEROR'S CHILDREN", 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'KHORNE', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeAttacker_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachPlanetstrikeDefender_chaos(DetachPlanetstrikeDefender):
    army_name = 'Chaos (Planetstrike Defender detachment)'
    faction_base = 'CHAOS'
    alternate_factions = []
    army_id = 'planetstrike attacker_chaos'
    army_factions = ['<LEGION>', 'NIGHT LORDS', 'DAEMON', 'WORLD EATERS', 'ALPHA LEGION', 'RENEGADES', 'BLACK LEGION', 'TZEENTCH', 'CHAOS', 'DEATH GUARD', 'RED CORSAIRS', 'SLAANESH', "EMPEROR'S CHILDREN", 'NURGLE', 'WORD BEARERS', 'IRON WARRIORS', 'KHORNE', 'HERETIC ASTARTES', 'THOUSAND SONS']

    def __init__(self, parent=None):
        super(DetachPlanetstrikeDefender_chaos, self).__init__(*[], **{'heavy': True, 'troops': True, 'fliers': True, 'elite': True, 'hq': True, 'fast': True, 'transports': True, 'parent': parent, })
        self.heavy.add_classes([ChaosLandRaider, ChaosPredator, ChaosVindicator, Defiler, Forgefiend, Havocs, Maulerfiend, Obliterators, SoulGrinder, SkullCannon, BurningChariot, SeekerChariot, ExSeekerChariot, PlagueCrawler, Mutalith])
        self.troops.add_classes([ChaosCultists, ChaosSpaceMarines, LegionBerzerkers, LegionPlagues, LegionNoises, Bloodletters, Horrors, Plaguebearers, Nurglings, Daemonettes, DGPlagueMarinesV2, Poxwalkers, LegionRubrics, Tzaangors])
        self.fliers.add_classes([Heldrake])
        self.elite.add_classes([Berzerkers, ChaosTerminators, Chosen, Fallen, Helbrute, Mutilators, NoiseMarines, PlagueMarines, Possessed, RubricMarines, PlagueMarinesV2, Bloodcrushers, Flamers, ExFlamer, NurgleBeasts, Fiends, Blightbringer, Blightspawn, Putrifier, PlagueSurgeon, Tallyman, Deathshroud, BlightlordTerminators, Shaman, ScarabOccultTerminators])
        self.hq.add_classes([Abaddon, BikeLord, BikeSorcerer, ChaosLord, DaemonPrince, DarkApostle, DiscLord, DiscSorcerer, Fabius, Huron, JugLord, PalLord, PalSorcerer, Sorcerer, SteedLord, SteedSorcerer, TermoLord, TermoSorcerer, Warpsmith, Kharn, Lucius, Cypher, ExaltedChampion, Belakor, ChaosDaemonPrince, Karanak, Scarbrand, Skulltaker, HKhorne, ThroneHKhorne, JugHKhorne, FuryThirster, RageThirster, WrathThirster, Kairos, Changeling, BlueScribes, HTzeentch, DiscHTzeentch, ChariotHTzeentch, LordOfChange, Epidemius, Unclean, HNurgle, Masque, Keeper, HSlaanesh, SteedHSlaanesh, ChariotHSlaanesh, ExChariotHSlaanesh, BloodMaster, Skullmaster, BloodThrone, Changecaster, Fateskimmer, Fluxmaster, Rotigus, Slimux, Poxbringer, SloppityBilepiper, SpoilpoxScrivener, DGChaosLord, DGTermoLord, Typhus, NurgleDaemonPrince, ContagionLord, DGSorcerer, DGTermoSorcerer, Plaguecaster, Ahriman, TzeentchDaemonPrince, ExSorcerer, TSSorcerer, TSTermoSorcerer])
        self.fast.add_classes([ChaosBikers, Raptors, WarpTalons, ChaosSpawn, Furies, Hounds, Screamers, PlagueDrones, Seekers, Hellflayer, BloatDrone, BlightHaulers, Enlightened])
        self.transports.add_classes([ChaosRhino])
        return None


class DetachSuperHeavy_questor_mechanicus(DetachSuperHeavy):
    army_name = 'Questor Mechanicus (Super-Heavy detachment)'
    faction_base = 'QUESTOR MECHANICUS'
    alternate_factions = []
    army_id = 'super_heavy_questor_mechanicus'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'QUESTOR MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_questor_mechanicus, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, ArmigerWarglaives])
        return None


class DetachSuperHeavyAux_questor_mechanicus(DetachSuperHeavyAux):
    army_name = 'Questor Mechanicus (Super-Heavy auxilary detachment)'
    faction_base = 'QUESTOR MECHANICUS'
    alternate_factions = []
    army_id = 'super_heavy_aux_questor_mechanicus'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'QUESTOR MECHANICUS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_questor_mechanicus, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightErrantQM, KnightWardenQM, KnightGallantQM, KnightPaladinQM, KnightCrusaderQM, ArmigerWarglaives])
        return None


class DetachSuperHeavy_questor_imperialis(DetachSuperHeavy):
    army_name = 'Questor Imperialis (Super-Heavy detachment)'
    faction_base = 'QUESTOR IMPERIALIS'
    alternate_factions = []
    army_id = 'super_heavy_questor_imperialis'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'QUESTOR IMPERIALIS']

    def __init__(self, parent=None):
        super(DetachSuperHeavy_questor_imperialis, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives])
        return None


class DetachSuperHeavyAux_questor_imperialis(DetachSuperHeavyAux):
    army_name = 'Questor Imperialis (Super-Heavy auxilary detachment)'
    faction_base = 'QUESTOR IMPERIALIS'
    alternate_factions = []
    army_id = 'super_heavy_aux_questor_imperialis'
    army_factions = ['<HOUSEHOLD>', 'IMPERIUM', 'QUESTOR IMPERIALIS']

    def __init__(self, parent=None):
        super(DetachSuperHeavyAux_questor_imperialis, self).__init__(*[], **{'lords': True, 'parent': parent, })
        self.lords.add_classes([KnightCrusader, KnightErrant, KnightGallant, KnightPaladin, KnightWarden, ArmigerWarglaives])
        return None
