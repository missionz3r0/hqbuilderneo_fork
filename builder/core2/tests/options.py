__author__ = 'dante'

import unittest

from builder.core2.options import Option, Variant, OptionsList, OneOf, Count
from builder.core2.description import Gear


class OptionTest(unittest.TestCase):
    def test_create(self):
        node = Option(parent=None)
        self.assertTrue(node.id is None)
        self.assertTrue(node.active is True)
        self.assertTrue(node.visible is True)
        self.assertTrue(node.used is True)

        node = Option(parent=None, node_id='test_id')
        self.assertTrue(node.id == 'test_id')
        self.assertTrue(node.active is True)
        self.assertTrue(node.visible is True)
        self.assertTrue(node.used is True)

        node = Option(parent=None, active=False)
        self.assertTrue(node.id is None)
        self.assertTrue(node.active is False)
        self.assertTrue(node.visible is True)
        self.assertTrue(node.used is True)

        node = Option(parent=None, visible=False)
        self.assertTrue(node.id is None)
        self.assertTrue(node.active is True)
        self.assertTrue(node.visible is False)
        self.assertTrue(node.used is True)

        node = Option(parent=None, used=False)
        self.assertTrue(node.id is None)
        self.assertTrue(node.active is True)
        self.assertTrue(node.visible is True)
        self.assertTrue(node.used is False)

        node = Option(parent=None)
        node.active = False
        self.assertTrue(node.active is False)
        node.visible = False
        self.assertTrue(node.visible is False)
        node.used = False
        self.assertTrue(node.used is False)

    def test_dump(self):
        node = Option(parent=None, node_id='test_id')
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': True,
            'active': True,
            'used': True
        })

        node.active = False
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': True,
            'active': False,
            'used': True
        })

        node.visible = False
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': False,
            'active': False,
            'used': True
        })

        node.used = False
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': False,
            'active': False,
            'used': False
        })

    def test_load(self):
        node = Option(parent=None)
        data = {
            'id': 'test_id',
        }
        node.load(data)
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': True,
            'active': True,
            'used': True
        })
        self.assertDictEqual(node.save(), data)

        node = Option(parent=None)
        node.load({})

        self.assertDictEqual(node.dump(), {
            'id': None,
            'visible': True,
            'active': True,
            'used': True
        })

    def test_defaults(self):
        opt = Option(parent=None)
        self.assertListEqual(opt.errors, [])
        self.assertTrue(opt.description is None)
        self.assertTrue(opt.points is 0)


class VariantTest(unittest.TestCase):
    def test_build_string(self):
        self.assertTrue(Variant.build_points_string(20) == '(+20pt.)')
        self.assertTrue(Variant.build_points_string(20, per_model=True) == '(+20pt. per model)')
        self.assertTrue(Variant.build_points_string(0) == '(free)')
        self.assertTrue(Variant.build_points_string(None) == '')

    class Parent(object):
        def __init__(self):
            super(VariantTest.Parent, self).__init__()
            self.checked = False

        def append_variant(self, *args, **kwargs):
            self.checked = True

    def test_create(self):
        p = VariantTest.Parent()
        v = Variant(parent=p, name='Test Variant', points=10, active=True, visible=True, gear=None, subtype='Sub')
        self.assertTrue(p.checked)

        self.assertDictEqual(v.dump(), {
            'id': None,
            'name': 'Test Variant (+10pt.)',
            'points': 10,
            'visible': True,
            'active': True,
        })

        v.id = 'test_id'
        self.assertDictEqual(v.dump(), {
            'id': 'test_id',
            'name': 'Test Variant (+10pt.)',
            'points': 10,
            'visible': True,
            'active': True,
        })
        self.assertDictEqual(v.gear.dump(), {
            'name': 'Test Variant',
            'count': 1,
            'subtype': 'Sub'
        })

        gear = [Gear('Opt1'), Gear('Opt2')]
        v = Variant(parent=VariantTest.Parent(), name='Test Variant', points=10, active=True, visible=True, gear=gear)
        self.assertDictEqual(v.gear[0].dump(), gear[0].dump())
        self.assertDictEqual(v.gear[1].dump(), gear[1].dump())

    def test_points_update(self):
        v = Variant(parent=VariantTest.Parent(), name='Test Variant', points=10)
        self.assertEqual(v.name, 'Test Variant (+10pt.)')
        v.points = 0
        self.assertEqual(v.name, 'Test Variant (free)')

        v = Variant(parent=VariantTest.Parent(), name='Test Variant', points=10, per_model=True)
        self.assertEqual(v.name, 'Test Variant (+10pt. per model)')
        v.points = 20
        self.assertEqual(v.name, 'Test Variant (+20pt. per model)')


class OneOfTest(unittest.TestCase):
    class Parent(object):
        def __init__(self, update_id):
            super(OneOfTest.Parent, self).__init__()
            self.root = self
            self.roster = None
            self.checked = False
            self.update_id = update_id

        def append_option(self, opt):
            self.checked = True
            if self.update_id:
                opt.id = 'updated_id'

    def test_usage(self):
        p = OneOfTest.Parent(update_id=True)
        opt = OneOf(p, 'Option')
        self.assertTrue(p.checked)
        self.assertDictEqual(opt.dump(), {
            'type': 'one_of',
            'id': 'updated_id',
            'name': 'Option',
            'options': [],
            'selected': None,
            'active': True,
            'visible': True,
            'used': True,
        })

        class TestOpt(OneOf):
            def __init__(self):
                super(TestOpt, self).__init__(p, 'Test Opt')
                self.v1 = self.Variant(self, 'Variant1', 0)
                self.v_test = self.Variant(self, 'Variant Test', 10, node_id='test', per_model=True, gear=[
                    Gear(name='g1', count=2, subtype='sub'),
                    Gear(name='g2', count=1, subtype='sub')
                ])
                self.v2 = self.Variant(self, 'Variant2', 10, gear=Gear(name='g1', count=2, subtype='sub'))
                self.checked = False

            def check_rules(self):
                super(TestOpt, self).check_rules()
                self.checked = True

        opt = TestOpt()
        self.assertListEqual(opt.options, [opt.v1, opt.v_test, opt.v2])
        self.assertDictEqual(opt.dump(), {
            'type': 'one_of',
            'id': 'updated_id',
            'name': 'Test Opt',
            'options': [{
                'active': True,
                'id': '_id_1',
                'name': 'Variant1 (free)',
                'points': 0,
                'visible': True
            }, {
                'active': True,
                'id': 'test',
                'name': 'Variant Test (+10pt. per model)',
                'points': 10,
                'visible': True
            }, {
                'active': True,
                'id': '_id_2',
                'name': 'Variant2 (+10pt.)',
                'points': 10,
                'visible': True
            }],
            'selected': '_id_1',
            'active': True,
            'visible': True,
            'used': True,
        })

        opt.cur = opt.v_test
        self.assertDictEqual(opt.delta, {'id': 'updated_id', 'selected': 'test'})

        opt.cur = opt.v1
        opt.flush()
        self.assertDictEqual(opt.delta, {})

        opt.v1.visible = False
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [{'id': '_id_1', 'visible': False}],
            'selected': 'test'
        })

        opt.flush()
        opt.v_test.active = False
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [{'id': 'test', 'active': False}],
            'selected': '_id_2'
        })

        opt.flush()
        opt.v1.visible = opt.v_test.active = True
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [{'id': '_id_1', 'visible': True},
                        {'id': 'test', 'active': True}],
        })

        opt.flush()
        opt.name = 'New name'
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'name': 'New name',
        })

        opt.update({'id': 'updated_id', 'selected': 'test'})
        self.assertEqual(opt.cur, opt.v_test)

        self.assertDictEqual(opt.save(), {
            'id': 'updated_id',
            'selected': 'test'
        })

        opt.load({
            'id': 'updated_id',
            'selected': '_id_1'
        })
        self.assertEqual(opt.cur, opt.v1)

        opt.check_rules_chain()
        self.assertTrue(opt.checked)

        self.assertEqual(opt.points, 0)
        self.assertDictEqual(opt.description[0].dump(), {'count': 1, 'name': 'Variant1', 'subtype': None})

        opt.cur = opt.v_test
        self.assertEqual(opt.points, 10)
        self.assertDictEqual(opt.description[0].dump(), {'count': 2, 'name': 'g1', 'subtype': 'sub'})
        self.assertDictEqual(opt.description[1].dump(), {'count': 1, 'name': 'g2', 'subtype': 'sub'})

        opt.cur = opt.v2
        self.assertDictEqual(opt.description[0].dump(), {'count': 2, 'name': 'g1', 'subtype': 'sub'})


class OptionsListTest(unittest.TestCase):
    class Parent(object):
        def __init__(self, update_id):
            super(OptionsListTest.Parent, self).__init__()
            self.root = self
            self.roster = None
            self.checked = False
            self.update_id = update_id

        def append_option(self, opt):
            self.checked = True
            if self.update_id:
                opt.id = 'updated_id'

    def test_usage(self):
        p = OptionsListTest.Parent(update_id=True)
        opt = OptionsList(p, 'Option')
        self.assertTrue(p.checked)
        self.assertDictEqual(opt.dump(), {
            'type': 'options_list',
            'id': 'updated_id',
            'name': 'Option',
            'options': [],
            'active': True,
            'visible': True,
            'used': True,
        })

        class TestOpt(OptionsList):
            def __init__(self):
                super(TestOpt, self).__init__(p, 'Test Opt')
                self.v1 = self.Variant(parent=self, name='Variant1', points=0)
                self.v_test = self.Variant(self, 'Variant Test', 10, node_id='test', per_model=True, gear=[
                    Gear(name='g1', count=2, subtype='sub'),
                    Gear(name='g2', count=1, subtype='sub')
                ])
                self.v2 = self.Variant(self, 'Variant2', 10, gear=Gear(name='g1', count=2, subtype='sub'))
                self.checked = False

            def check_rules(self):
                super(TestOpt, self).check_rules()
                self.checked = True

        opt = TestOpt()
        self.assertListEqual(opt.options, [opt.v1, opt.v_test, opt.v2])

        self.assertDictEqual(opt.dump(), {
            'type': 'options_list',
            'id': 'updated_id',
            'name': 'Test Opt',
            'options': [{
                'active': True,
                'id': '_id_1',
                'name': 'Variant1 (free)',
                'points': 0,
                'visible': True,
                'value': False,
                'used': True,
            }, {
                'active': True,
                'id': 'test',
                'name': 'Variant Test (+10pt. per model)',
                'points': 10,
                'visible': True,
                'value': False,
                'used': True,
            }, {
                'active': True,
                'id': '_id_2',
                'name': 'Variant2 (+10pt.)',
                'points': 10,
                'visible': True,
                'value': False,
                'used': True,
            }],
            'active': True,
            'visible': True,
            'used': True,
        })

        opt.v_test.value = True
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [{'id': 'test', 'value': True}]
        })

        opt.v_test.value = False
        opt.v1.value = True
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [{
                'id': '_id_1',
                'value': True
            }, {
                'id': 'test',
                'value': False
            }]
        })

        opt.v1.value = False
        opt.flush()
        self.assertDictEqual(opt.delta, {})

        opt.v1.used = False
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [{'id': '_id_1', 'used': False}],
        })

        opt.flush()
        opt.name = 'New name'
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'name': 'New name',
        })

        opt.update({
            'id': 'updated_id',
            'options': [{'id': '_id_1', 'value': False}],
        })
        self.assertTrue(not opt.v1.value)

        self.assertDictEqual(opt.save(), {
            'id': 'updated_id',
            'options': [
                {'id': '_id_1', 'value': False},
                {'id': 'test', 'value': False},
                {'id': '_id_2', 'value': False},
            ],
        })

        opt.load({
            'id': 'updated_id',
            'options': [
                {'id': '_id_1', 'value': False},
                {'id': 'test', 'value': True},
                {'id': '_id_2', 'value': True},
            ],
        })
        self.assertTrue(not opt.v1.value)
        self.assertTrue(opt.v_test.value)
        self.assertTrue(opt.v2.value)

        opt.check_rules_chain()
        self.assertTrue(opt.checked)

        opt.v1.value = True
        opt.v1.used = True
        self.assertEqual(opt.points, 20)
        self.assertDictEqual(opt.description[0].dump(), {'count': 1, 'name': 'Variant1', 'subtype': None})
        self.assertDictEqual(opt.description[1].dump(), {'count': 2, 'name': 'g1', 'subtype': 'sub'})
        self.assertDictEqual(opt.description[2].dump(), {'count': 1, 'name': 'g2', 'subtype': 'sub'})
        self.assertDictEqual(opt.description[3].dump(), {'count': 2, 'name': 'g1', 'subtype': 'sub'})

        opt.v_test.used = False
        self.assertEqual(opt.points, 10)
        self.assertDictEqual(opt.description[0].dump(), {'count': 1, 'name': 'Variant1', 'subtype': None})
        self.assertDictEqual(opt.description[1].dump(), {'count': 2, 'name': 'g1', 'subtype': 'sub'})

        opt.v_test.used = True
        opt.flush()

        OptionsList.process_limit(opt.options, 1)
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [
                {'id': '_id_1', 'value': False},
                {'id': 'test', 'value': False},
                {'id': '_id_2', 'value': False}
            ],
        })

        opt.flush()
        opt.v1.value = True
        OptionsList.process_limit(opt.options, 1)
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [
                {'id': '_id_1', 'value': True},
                {'id': 'test', 'active': False},
                {'id': '_id_2', 'active': False}
            ],
        })

        opt.v1.value = False
        OptionsList.process_limit(opt.options, 1)
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [
                {'id': '_id_1', 'value': False},
                {'id': 'test', 'active': True},
                {'id': '_id_2', 'active': True}
            ],
        })

        opt.v1.value = True
        opt.limit = 1
        opt.check_rules()
        self.assertDictEqual(opt.delta, {
            'id': 'updated_id',
            'options': [
                {'id': '_id_1', 'value': True},
                {'id': 'test', 'active': False},
                {'id': '_id_2', 'active': False}
            ],
        })


class CountTest(unittest.TestCase):

    class Parent(object):
        def __init__(self, update_id):
            super(CountTest.Parent, self).__init__()
            self.root = self
            self.roster = None
            self.checked = False
            self.update_id = update_id

        def append_option(self, opt):
            self.checked = True
            if self.update_id:
                opt.id = 'updated_id'

    def test_create(self):
        p = CountTest.Parent(update_id=True)
        count = Count(p, 'test_count', min_limit=1, max_limit=10)
        self.assertTrue(p.checked)

        self.assertDictEqual(count.dump(), {
            'used': True,
            'cur': 1,
            'min': 1,
            'max': 10,
            'name': 'test_count',
            'visible': True,
            'active': True,
            'type': 'count',
            'id': 'updated_id',
            'help': ''
        })
        self.assertDictEqual(count.delta, {})
        count.cur = 5
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 5,
        })
        count.cur = 0
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 1,
        })
        count.cur = 20
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 10,
        })

        count.max = 8
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 8,
            'max': 8,
        })

        count.max = 18
        count.flush()
        count.min = 10
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 10,
            'min': 10,
        })
        count.flush()

        count.help = 'text help'
        self.assertDictEqual(count.dump(), {
            'used': True,
            'cur': 10,
            'min': 10,
            'max': 18,
            'name': 'test_count',
            'visible': True,
            'active': True,
            'type': 'count',
            'id': 'updated_id',
            'help': 'text help'
        })

        count.points = 100
        self.assertDictEqual(count.delta, {'help': '(+100pt.)', 'id': 'updated_id'})
        count.cur = 10
        self.assertEqual(count.points, 1000)
        count.flush()

        count.update({})
        self.assertDictEqual(count.delta, {})

        count.update({'value': 15})
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 15,
        })

        self.assertDictEqual(count.save(), {
            'id': 'updated_id',
            'cur': 15,
            # no need to save min and max
            # 'max': 18,
            # 'min': 10,
        })
        count.load({'id': 'updated_id', 'cur': 5})
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 5,
            'min': 5
        })
        count.flush()

        count.load({'id': 'updated_id', 'cur': 15, })
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 15,
        })
        count.load({
            'id': 'updated_id',
            'cur': 1,
            'max': 10,
            'min': 1,
        })
        self.assertDictEqual(count.delta, {
            'id': 'updated_id',
            'cur': 1,
            'max': 10,
            'min': 1,
        })

    def test_normalisation(self):
        c1 = Count(self.Parent(True), 'c1', 1, 10)
        c2 = Count(self.Parent(True), 'c2', 1, 10)
        c3 = Count(self.Parent(True), 'c3', 1, 10)
        Count.norm_counts(1, 10, [c1, c2, c3])

        def count_dict(cnt):
            res = cnt.save()
            res.update({'max': cnt.max, 'min': cnt.min})
            return res

        self.assertDictEqual(count_dict(c1), {'cur': 1, 'id': 'updated_id', 'max': 8, 'min': 0})
        self.assertDictEqual(count_dict(c2), {'cur': 1, 'id': 'updated_id', 'max': 8, 'min': 0})
        self.assertDictEqual(count_dict(c3), {'cur': 1, 'id': 'updated_id', 'max': 8, 'min': 0})


        c1.cur = 5
        Count.norm_counts(1, 10, [c1, c2, c3])
        self.assertDictEqual(count_dict(c1), {'cur': 5, 'id': 'updated_id', 'max': 8, 'min': 0})
        self.assertDictEqual(count_dict(c2), {'cur': 1, 'id': 'updated_id', 'max': 4, 'min': 0})
        self.assertDictEqual(count_dict(c3), {'cur': 1, 'id': 'updated_id', 'max': 4, 'min': 0})

        c1.cur = 5
        Count.norm_counts(1, 5, [c1, c2, c3])
        self.assertDictEqual(count_dict(c1), {'cur': 3, 'id': 'updated_id', 'max': 3, 'min': 0})
        self.assertDictEqual(count_dict(c2), {'cur': 1, 'id': 'updated_id', 'max': 4, 'min': 0})
        self.assertDictEqual(count_dict(c3), {'cur': 1, 'id': 'updated_id', 'max': 4, 'min': 0})

        c1.cur = 1
        Count.norm_counts(4, 5, [c1, c2, c3])
        self.assertDictEqual(count_dict(c1), {'cur': 2, 'id': 'updated_id', 'max': 3, 'min': 2})
        self.assertDictEqual(count_dict(c2), {'cur': 1, 'id': 'updated_id', 'max': 4, 'min': 0})
        self.assertDictEqual(count_dict(c3), {'cur': 1, 'id': 'updated_id', 'max': 4, 'min': 0})

        Count.norm_counts(4, 5, [c1])
        self.assertDictEqual(count_dict(c1), {'cur': 4, 'id': 'updated_id', 'max': 5, 'min': 4})

    #def test_points_norm(self):
    #    c1 = Count(self.Parent(True), 'c1', 1, 10, points=10)
    #    c2 = Count(self.Parent(True), 'c2', 1, 10, points=20)
    #    c3 = Count(self.Parent(True), 'c3', 1, 10, points=30)
    #    Count.norm_points(20, [c1, c2, c3])
    #    self.assertDictEqual(c1.save(), {'cur': 1, 'id': 'updated_id', 'max': 8, 'min': 0})
    #    self.assertDictEqual(c2.save(), {'cur': 1, 'id': 'updated_id', 'max': 8, 'min': 0})
    #    self.assertDictEqual(c3.save(), {'cur': 1, 'id': 'updated_id', 'max': 8, 'min': 0})
