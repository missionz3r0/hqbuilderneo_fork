import uuid

from .description import UnitDescription
from .node import Node, Observable, IdGenerator
from .options import Count, OptionsList

__author__ = 'Denis Romanov'


class Unit(Node):
    type_name = None
    type_id = None
    wikilink = None
    faction = None
    starrable = False

    template_link = 'https://sites.google.com/site/wh40000rules/codices/{codex_name}/profiles#TOC-{unit_name}'

    def __init__(self, parent, name=None, points=0, gear=None, unique=False, static=False):
        super(Unit, self).__init__(parent=parent, node_id=uuid.uuid4().hex)

        self._name = name or self.type_name
        self.user_name = ''
        self.unique = unique
        self.static = static
        self.base_points = points
        self.gear = gear or []
        self.id_generator = IdGenerator()

        self._options = Observable([])
        self._count = Observable(1)
        self._points = Observable(points)
        self._errors = Observable([])
        self._description = Observable(None)
        self._star = Observable(1 if self.starrable else 0)

        if not self.wikilink and self.faction:
            self.lookup_wikilink()

    def lookup_faction(self):
        return self.faction

    def lookup_name(self):
        return self._name

    def lookup_wikilink(self):
        try:
            from db.models import Lookup
            type(self).wikilink = Lookup.find_link(self.lookup_faction(), self.lookup_name())
        except RuntimeError:
            # just ignore the error if there is no db available
            pass

    @property
    def star(self):
        return self._star.value

    @star.setter
    def star(self, value):
        self._star.value = value

    @property
    def points(self):
        return self._points.value

    @points.setter
    def points(self, value):
        self._points.value = value

    @property
    def options(self):
        return self._options.value

    @options.setter
    def options(self, value):
        self._options.value = value

    @property
    def count(self):
        return self._count.value

    @property
    def name(self):
        return '{} {{{}}}'.format(self._name, self.user_name)\
            if len(self.user_name)\
            else self._name

    @name.setter
    def name(self, value):
        self._name = value

    @count.setter
    def count(self, value):
        self._count.value = value

    @property
    def description(self):
        return self._description.value

    @description.setter
    def description(self, value):
        self._description.value = value

    @property
    def errors(self):
        return self._errors.value

    @errors.setter
    def errors(self, value):
        self._errors.value = value

    def append_option(self, option):
        if option.id is None:
            option.id = next(self.id_generator)
        for o in range(len(self.options)):
            if self.options[o].order > option.order:
                self.options.insert(o, option)
                return option
        self.options.append(option)
        return option

    def update(self, data):
        if 'user_name' in data:
            self.user_name = data['user_name']
        if 'star' in data:
            self.star = data['star']
        for option_data in data.get('options', []):
            for option in self.options:
                if option.id == option_data['id']:
                    option.update(option_data)

    def dump(self):
        res = super(Unit, self).dump()
        res.update({
            'unique': self.unique,
            'static': self.static,
            'description': self.description,
            'user_name': self.user_name,
            'errors': self.errors,
            'options': [o.dump() for o in self.options]
        })
        if self.star > 0:
            res.update({
                'star': self.star
            })
        if self.wikilink:
            res.update({'wikilink': self.wikilink})
        return res

    def save(self):
        res = super(Unit, self).save()
        res.update({
            'type': self.type_id,
            'user_name': self.user_name,
            'options': [o.save() for o in self.options]
        })
        if self.star > 1:
            res.update({
                'star': self.star
            })
        return res

    def load(self, data):
        Node.load(self, data)
        self.id = data['id']
        self.user_name = data.get('user_name', '')
        self.star = data.get('star', self.star)
        for option_data in data['options']:
            try:
                next(o for o in self.options if o.id == option_data['id']).load(option_data)
            except StopIteration:
                print('StopIteration for', option_data['id'])
            except AttributeError:
                print('Attribute mismatch for', option_data['id'])

    def clone(self):
        new_unit = self.__class__(parent=self.parent)
        unit_data = self.save()
        unit_data['id'] = new_unit.id
        new_unit.load(unit_data)
        return new_unit

    def collect_errors(self):
        self.errors = self.errors + sum((opt.errors for opt in self.options), [])

    def check_rules_chain(self):
        self.reset_errors()
        Node.check_rules_chain(self)
        self.check_rules()
        self.collect_errors()
        self.count = self.get_count()
        self.points = self.build_points()
        self.description = self.build_description().dump()

    def build_points(self):
        return sum((o.points for o in self.options if o.used))\
            + self.base_points

    def build_description(self):
        description = UnitDescription(name=self.name, points=self.points, count=self.count, options=self.gear)
        for o in self.options:
            if o.used:
                description.add(o.description)
        return description

    def check_rules(self):
        pass

    def get_count(self):
        return self.count

    def get_unique(self):
        if self.unique:
            return self._name
        return None

    def get_unique_gear(self):
        pass

    def error(self, message):
        self.errors = self.errors + [message]

    def reset_errors(self):
        self.errors = []

    @staticmethod
    def norm_core1_unit(unit_class):
        from builder.core.unit import Unit as LegacyUnit
        if not issubclass(unit_class, LegacyUnit):
            raise TypeError('unit_class is not subclass Core v1 Unit')

        class LegacyUnitWrapper(object):
            type_id = unit_class.__name__
            type_name = unit_class.name

            def __init__(self, parent=None):
                self.parent = parent
                super(LegacyUnitWrapper, self).__init__()
                self.legacy_unit = unit_class()
                self.legacy_unit.set_roster(self.parent.roster)

            def __getattr__(self, item):
                try:
                    return super(LegacyUnitWrapper, self).__getattribute__(item)
                except AttributeError:
                    return self.legacy_unit.__getattribute__(item)

            @property
            def points(self):
                return self.legacy_unit.get_points()

            @property
            def errors(self):
                return self.legacy_unit.get_errors()

            def save(self):
                return self.legacy_unit.dump_save()

            def clone(self):
                new_unit = self.__class__(parent=self.parent)
                new_unit.roster = self.parent.roster
                unit_data = self.save()
                unit_data['id'] = new_unit.id
                new_unit.load(unit_data)
                return new_unit

            #def get_unique_gear(self):
            #    pass

            def build_statistics(self):
                return {}

        return LegacyUnitWrapper

    def build_statistics(self):
        return {}


class ListSubUnit(Unit):
    def __init__(self, parent, name=None, points=0, gear=None, per_model=True, **kwargs):
        super(ListSubUnit, self).__init__(parent=parent, name=name, points=points, gear=gear, **kwargs)
        self.models = Count(self, self.name, 1, 1, points=points, per_model=per_model, used=False, gear=[])

    def get_count(self):
        return self.models.cur

    def load(self, data):
        super(ListSubUnit, self).load(data)

    @staticmethod
    def count_gear(func):
        def counter(unit):
            return unit.models.cur if func(unit) else 0
        return counter

    @staticmethod
    def count_unique(func):
        def counter(unit):
            return func(unit) * unit.models.cur
        return counter

    @property
    def root_unit(self):
        return self.parent.parent

    def build_points(self):
        return super(ListSubUnit, self).build_points() -\
            (self.models.points if self.models.used else 0)


class UpgradeUnit(Unit):

    class Upgrade(OptionsList):
        def __init__(self, parent):
            super(UpgradeUnit.Upgrade, self).__init__(parent, "Upgrade")
            self.variant(parent.upgraded_name, parent.upgrade_cost, gear=[])

    def __init__(self, parent, upgraded_name=None,
                 upgrade_cost=0, upgraded_gear=[], **kwargs):
        super(UpgradeUnit, self).__init__(parent, **kwargs)
        self.upgrade_cost = upgrade_cost
        self.base_tuple = (self.name, self.gear)
        self.base_options = list(self.options)
        self.upgraded_name = upgraded_name
        self.upgraded_tuple = (upgraded_name, upgraded_gear)
        self.make_upgraded_options()
        self.up = self.Upgrade(self)

    def make_upgraded_options(self):
        self.upgraded_options = []

    def check_rules_chain(self):
        # executed before all other rules checks, presumably
        if self.up.any:
            self.name, self.gear = self.upgraded_tuple
        else:
            self.name, self.gear = self.base_tuple

        for base_opt in self.base_options:
            base_opt.used = base_opt.visible = not self.up.any
        for guard_opt in self.upgraded_options:
            guard_opt.used = guard_opt.visible = self.up.any

        super(UpgradeUnit, self).check_rules_chain()
