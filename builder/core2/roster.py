from .node import Node, Observable, IdGenerator


__author__ = 'Denis Romanov'


def get_unit_names(unit):
    res = []
    gears = unit.get_unique_gear()
    if gears:
        for gear in unit.get_unique_gear():
            if gear:
                res.append(gear if isinstance(gear, str) else gear.name)
    return res


class Roster(Node):
    army_id = None
    army_name = None
    roster_type = None
    game_name = None
    obsolete = False
    development = False
    base_tags = []

    def __init__(self, sections, parent=None, limit=None):
        super(Roster, self).__init__(parent=parent)
        self._name = Observable(self.army_name)
        self._limit = Observable(limit)
        self._description = Observable('')
        self._tags = Observable([])
        self._sections = Observable([sec for sec in sections if sec])
        self._errors = Observable([])
        self._points = Observable(0)
        self._options = Observable([])
        self._properties = Observable([])
        self.id_generator = IdGenerator()
        self.unit_cache = None
        self.artefact_cache = None

    @property
    def roster(self):
        return self

    @property
    def options(self):
        return self._options.value

    @property
    def properties(self):
        return self._properties.value

    @options.setter
    def options(self, value):
        self._options.value = value

    def append_option(self, option):
        if option.id is None:
            option.id = next(self.id_generator)
        self.options.append(option)
        return option

    def make_property(self, _property):
        try:
            self.options.remove(_property)
        except:
            print('Missing option id ', _property.id)
        self.properties.append(_property)

    @property
    def sections(self):
        return self._sections.value

    @property
    def errors(self):
        return self._errors.value

    @errors.setter
    def errors(self, val):
        self._errors.value = val

    @property
    def points(self):
        return self._points.value

    @points.setter
    def points(self, val):
        self._points.value = val

    @property
    def name(self):
        return self._name.value

    @name.setter
    def name(self, val):
        self._name.value = val

    @property
    def limit(self):
        return self._limit.value

    @limit.setter
    def limit(self, val):
        self._limit.value = val

    @property
    def tags(self):
        return self._tags.value

    @tags.setter
    def tags(self, val):
        self._tags.value = val

    @property
    def description(self):
        return self._description.value

    @description.setter
    def description(self, val):
        self._description.value = val

    def update(self, data):
        for option_data in data.get('options', []):
            for option in self.options + self.properties:
                if option.id == option_data['id']:
                    option.update(option_data)
                    break

        for section_data in data.get('sections', []):
            # import pdb; pdb.set_trace()
            try:
                next(section for section in self.sections if section.id == section_data['id']).update(section_data)
            except StopIteration:
                pass
        self.limit = data.get('limit', self.limit)
        self.description = data.get('description', self.description)
        self.name = data.get('name', self.name)
        self.tags = data.get('tags', self.tags)

    def get_options_description_dump(self):
        result = []
        for o in self.options + self.properties:
            if o.used:
                result.extend(g.dump() for g in o.description)
        return result

    def dump(self):
        res = super(Roster, self).dump()
        res.update({
            'version': 2,
            'name': self.name,
            'points': self.points,
            'limit':  self.limit,
            'description': self.description,
            'options_description': self.get_options_description_dump(),
            'tags': self.tags,
            'sections': [section.dump() for section in self.sections],
            'options': [o.dump() for o in self.options],
            'properties': [o.dump() for o in self.properties],
            'errors': self.errors,
            'obsolete': self.obsolete,
        })
        return res

    @property
    def delta(self):
        res = super(Roster, self).delta
        if 'options' in res:
            res.update({
                'options_description': self.get_options_description_dump(),
            })
        return res

    def save(self):
        res = super(Roster, self).save()
        res.update({
            'army_id': self.army_id,
            'name': self.name,
            'limit':  self.limit,
            'points':  self.points,
            'description': self.description,
            #'tags': self.tags,
            'sections': [section.save() for section in self.sections],
            'options': [o.save() for o in self.options],
        })
        return res

    def load(self, data):
        self.limit = data.get('limit', self.limit)
        self.description = data.get('description', self.description)
        self.name = data.get('name', self.name)
        #self.tags = data.get('tags', self.tags)
        for option_data in data.get('options', []):
            try:
                next(o for o in self.options if o.id == option_data['id']).load(option_data)
            except StopIteration:
                print('StopIteration for', option_data['id'])
        for section_data in data.get('sections', []):
            try:
                next(section for section in self.sections if section.id == section_data['id']).load(section_data)
            except StopIteration:
                pass

    def error(self, message):
        if message:
            if isinstance(message, (list, tuple)):
                for m in message:
                    self.error(m)
            else:
                if not message in self.errors:
                    self.errors = self.errors + [message]

    def _build_unique_map(self, names_getter):
        unique_map = {}
        for sec in self.sections:
            for unit in sec.units:
                un = names_getter(unit)
                if un is None:
                    continue
                if not isinstance(un, (list, type)):
                    un = [un]
                for name in un:
                    if name in list(unique_map.keys()):
                        unique_map[name] += 1
                    else:
                        unique_map[name] = 1
        return unique_map

    def _unique_check(self, names_getter, cachename, message):
        unique_map = self.get_unique_map(cachename, names_getter)
        for (name, number) in unique_map.items():
            if number > 1:
                self.error(message.format(name))

    def get_unique_map(self, cachename, names_getter):
        try:
            if getattr(self, cachename):
                return getattr(self, cachename)
        except:
            return self._build_unique_map(names_getter)
        unique_map = self._build_unique_map(names_getter)
        setattr(self, cachename, unique_map)
        return unique_map

    def unique_check(self):
        self._unique_check(lambda unit: unit.get_unique(),
                           'unit_cache', '{0} must be unique in army')
        self._unique_check(get_unit_names,
                           'artefact_cache', '{0} must be unique in army')

    def sections_check(self):
        for sec in self.sections:
            self.error(sec.get_errors())

    def calc_points(self):
        return sum(sec.points for sec in self.sections)

    def check_limit_error(self):
        if self.limit and self.points > self.limit:
            self.error('Roster is over limit ({0})'.format(self.limit))

    def check_rules_chain(self):
        self.unit_cache = None
        self.artefact_cache = None
        super(Roster, self).check_rules_chain()
        self.errors = []
        self.sections_check()
        self.points = self.calc_points()
        self.check_rules()
        self.check_limit_error()
        self.unique_check()

    def check_rules(self):
        pass

    def limits_updated(self, section):
        pass

    @classmethod
    def post_process(cls):
        if not cls.army_name in cls.base_tags:
            cls.base_tags = [cls.army_name] + cls.base_tags

    def build_statistics(self):
        from collections import defaultdict
        res = defaultdict(int)
        for sec in self.sections:
            for unit in sec.units:
                for k, v in list(unit.build_statistics().items()):
                    res[k] += v
        return dict(res)
