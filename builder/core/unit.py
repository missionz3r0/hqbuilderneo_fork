__author__ = 'Denis Romanov'

import uuid
from builder.core import Node, id_filter, node_load
from builder.core.unit_points import UnitPoints
from builder.core.unit_description import UnitDescription
from builder.core.options.count import Count
from builder.core.options.one_of import OneOf
from builder.core.options.options_list import OptionsList
from builder.core.options.sub_unit import SubUnit
from builder.core.options.optional_sub_unit import OptionalSubUnit
from builder.core.options.unit_list import UnitList
from functools import reduce


class Unit(Node):
    static = False
    unique = False
    gear = []
    base_points = 0

    def __init__(self, detach_unit=False):
        Node.__init__(self)
        self.id = uuid.uuid4().hex
        self.points = UnitPoints()
        self.description = UnitDescription()
        self.count = 1
        self._options = self.observable_array('options', [])
        self._count = self.observable('count', self.count)
        self._points = self.observable('points', 0)
        self._errors = self.observable('errors', [])
        self._description = self.observable('description', None)
        self._roster = None
        self._detach_unit = detach_unit

    def set_roster(self, r):
        self._roster = r

    def get_roster(self):
        return self._roster

    def get_default_id(self, id):
        if id is None:
            id = '_opt_id_' + str(len(self._options.get()))
        return id

    def append_opt(self, option):
        self._options.get().append(option)
        return option

    def opt_count(self, name, min, max, points, help=None, id=None):
        opt = Count(self.get_default_id(id), name, min, max, points, help)
        self.append_opt(opt)
        return opt

    def opt_one_of(self, name, variants, id=None):
        opt = OneOf(self.get_default_id(id), name, variants)
        self.append_opt(opt)
        return opt

    def opt_options_list(self, name, variants, limit=None, id=None, points_limit=None):
        opt = OptionsList(self.get_default_id(id), name, variants, limit, points_limit)
        self.append_opt(opt)
        return opt

    def opt_sub_unit(self, unit, id=None):
        opt = SubUnit(self.get_default_id(id), unit)
        self.append_opt(opt)
        return opt

    def opt_optional_sub_unit(self, name, units, id=None):
        opt = OptionalSubUnit(self.get_default_id(id), name, units)
        self.append_opt(opt)
        return opt

    def opt_units_list(self, name, unit_class, min, max, id=None):
        opt = UnitList(self.get_default_id(id), name, unit_class, min, max)
        self.append_opt(opt)
        return opt

    @id_filter
    def update(self, data):
        if 'options' in list(data.keys()):
            for option_data in data['options']:
                for option in self._options.get():
                    option.update(option_data)

            #'sub_unit': {
            #    'name': self.name,
            #    'points': self.points,
            #    'options': self.build_options(),
            #    'sub_units': self.sub_unit
            #},
            #'count': count if count is not None else self.count

    @staticmethod
    def reformat_sub_units(units):
        return [{
            'name': sub_unit['sub_unit']['name'],
            'points': sub_unit['sub_unit']['points'],
            'options': sub_unit['sub_unit']['options'],
            'sub_units': Unit.reformat_sub_units(sub_unit['sub_unit']['sub_units']),
            'count': sub_unit['count'],
        } for sub_unit in units]

    @staticmethod
    def reformat_description(count, desc):
        res = []
        for sub_unit in desc['sub_units']:
            if 'sub_unit' in sub_unit:
                res.append({
                    'name': sub_unit['sub_unit']['name'],
                    'points': sub_unit['sub_unit']['points'],
                    'options': sub_unit['sub_unit']['options'],
                    'sub_units': Unit.reformat_sub_units(sub_unit['sub_unit']['sub_units']),
                    'count': sub_unit['count'],
                })
            else:
                res.append(sub_unit)
        desc['sub_units'] = res
        desc['count'] = count
        return desc

    def dump(self):
        res = Node.dump(self)
        res.update({
            'name': self.name,
            'unique': self.unique,
            'static': self.static,
            'description': Unit.reformat_description(self._count.get(), self._description.get()),
            #'description': self._description.get(),
            'points': self._points.get(),
            'count': self._count.get(),
            'errors': self._errors.get(),
            'options': [o.dump() for o in self._options.get()],
            'detach_unit': self._detach_unit
        })
        return res

    def dump_save(self):
        res = Node.dump(self)
        res.update({
            'id': self.id,
            'type': self.__class__.__name__,
            'description': Unit.reformat_description(self._count.get(), self._description.get()),
            'points': self._points.get(),
            'count': self._count.get(),
            'options': [o.dump_save() for o in self._options.get()]
        })
        return res

    @node_load
    def load(self, data):
        self.id = data['id']
        self._description.set(data['description'])
        self._points.set(data['points'])
        self._count.set(data['count'])
        for option_data in data['options']:
            try:
                next(o for o in self._options.get() if o.id == option_data['id']).load(option_data)
            except StopIteration:
                print('StopIteration for', option_data['id'])

    def clone(self):
        new_unit = self.__class__()
        unit_data = self.dump_save()
        unit_data['id'] = new_unit.id
        new_unit.load(unit_data)
        return new_unit

    def collect_errors(self):
        self._errors.set(self._errors.get() + sum((opt.get_errors() for opt in self._options.get()), []))

    def check_rules_chain(self):
        self.reset_errors()
        Node.check_rules_chain(self)
        self.check_rules()
        self.collect_errors()
        self._count.set(self.get_count())
        self._points.set(self.points.get())
        self._description.set(self.description.get())

    def set_points(self, points):
        self.points.set(points)
        self._points.set(self.points.get())

    def build_points(self, base_points=None, options=None, count=None, exclude=None):
        if base_points is None:
            base_points = self.base_points
        if options is None:
            options = self._options.get()
        if count is None:
            count = self.get_count()

        pt = reduce(lambda val, o: (val + o.points()) if not exclude or o.id not in exclude else val, options, base_points) * count
        self._points.set(pt)
        return pt

    def build_description(self, name=None, points=None, options=None, count=None, exclude=None, add=None, gear=None):
        if points is None:
            points = self.points.get()
        if name is None:
            name = self.name
        if options is None:
            options = self._options.get()
        if count is None:
            count = self.get_count()
        if gear is None:
            gear = self.gear

        self.description.reset()
        self.description.set_header(name, points)
        if add:
            for o in add:
                self.description.add(o)
        for g in gear:
            self.description.add(g, count)
        for o in options:
            if not exclude or o.id not in exclude:
                self.description.add(o.get_selected(), count)
        self._description.set(self.description.get())

    def set_description(self, description):
        self._description.set(description.get())

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description()

    def get_count(self):
        if isinstance(self.count, Count):
            return self.count.get()
        return self.count

    def get_unique(self):
        if self.unique:
            return self.name
        return None

    def get_unique_gear(self):
        return None

    def get_points(self):
        return self.points.get()

    def error(self, message):
        if not message:
            return
        errors = [e for e in self._errors.get()]
        errors.append(message)
        self._errors.set(errors)

    def reset_errors(self):
        self._errors.set([])

    def get_description(self):
        return {'sub_unit': self._description.get(), 'count': self.get_count()}

    def get_errors(self):
        return self._errors.get()


class StaticUnit(Unit):
    name = ''
    base_points = 0
    unique = True
    static = True

    def __init__(self, count=1):
        Unit.__init__(self)
        self.set_points(self.build_points())
        self.build_description()
        self.count = count

    def check_rules(self):
        pass


class ListUnit(Unit):
    unit_class = None

    def __init__(self, unit_class=None, min_num=None, max_num=None):
        Unit.__init__(self)
        if unit_class is None:
            unit_class = self.unit_class
        if min_num is None:
            min_num = unit_class.min
        if max_num is None:
            max_num = unit_class.max
        self.units = self.opt_units_list(unit_class.name, unit_class, min_num, max_num)

    def check_rules(self):
        self.units.update_range()
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return self.units.get_count()


class ListSubUnit(Unit):
    min = 1
    max = 1

    def __init__(self, min_models=1, max_models=1):
        Unit.__init__(self)
        self.min = min_models
        self.max = max_models
        self.count = self.opt_count(self.name, self.min, self.max, self.base_points)

    def check_rules(self):
        exclude = None
        if isinstance(self.count, Count):
            exclude = [self.count.id]
        single_points = self.build_points(exclude=exclude, count=1)
        self.points.set(single_points * self.get_count())
        self.build_description(points=single_points, count=1, exclude=exclude)
