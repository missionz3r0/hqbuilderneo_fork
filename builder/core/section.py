from builder.core import Node, build_id, id_filter, node_load
from builder.core.unit_type import UnitType
from functools import reduce


__author__ = 'Denis Romanov'


class Section(Node):
    def __init__(self, name, min, max, types, roster=None):
        types = [] if not types else types
        Node.__init__(self)
        self.id = build_id(name)
        self.name = name
        self._min = self.observable('min', min)
        self._max = self.observable('max', max)
        self._types = self.observable_array('types', self.build_type_list(types))
        self._units = self.observable_array('units', [])
        self._added_units = self.observable('added_units', [])
        self._deleted_units = self.observable('deleted_units', [])
        self._roster = roster

    @property
    def roster(self):
        return self._roster

    @roster.setter
    def roster(self, val):
        self._roster = val

    @property
    def min(self):
        return self._min.get()

    @min.setter
    def min(self, value):
        self._min.set(value)

    @property
    def max(self):
        return self._max.get()

    @max.setter
    def max(self, value):
        self._max.set(value)

    def build_type_list(self, types):
        type_list = []
        for unit_type in types:
            if isinstance(unit_type, dict):
                type_list.append(UnitType(
                    unit_class=unit_type['unit'],
                    active=unit_type.get('active', True),
                    visible=unit_type.get('visible', True),
                    type_id=unit_type.get('type_id', None),
                    deprecated=unit_type.get('deprecated', False)
                ))
            else:
                type_list.append(UnitType(unit_type))
        return type_list

    def add_units(self, data):
        added = []
        for add_data in data:
            for unit_type in self._types.get():
                if unit_type.id != add_data['id']:
                    continue
                unit = unit_type.unit_class()
                unit.set_roster(self._roster)
                unit.check_rules_chain()
                self._units.get().append(unit)
                added.append(unit.dump())
        self._added_units.set(added)

    def delete_units(self, data):
        to_del = []
        for del_data in data:
            for unit in self._units.get():
                if unit.id == del_data['id']:
                    to_del.append(unit)
        for unit in to_del:
            self._units.get().remove(unit)
        self._deleted_units.set([{'id': unit.id} for unit in to_del])

    def clone_unit(self, data):
        for unit in self._units.get():
            if unit.id != data['id']:
                continue
            new_unit = unit.clone()
            new_unit.set_roster(self._roster)
            self._units.get().append(new_unit)
            self._added_units.set([new_unit.dump()])
            return

    def clear_sections(self):
        self._deleted_units.set([{'id': unit.id} for unit in self._units.get()])
        del self._units.get()[:]

    def update_units(self, data):
        for unit_data in data:
            for unit in self._units.get():
                unit.update(unit_data)

    def get_points(self):
        return reduce(lambda val, u: val + u.get_points(), self._units.get(), 0)

    @id_filter
    def update(self, data):
        self.min = data.get('min', self.min)
        self.max = data.get('max', self.max)
        if 'added_units' in list(data.keys()):
            self.add_units(data['added_units'])
        if 'deleted_units' in list(data.keys()):
            self.delete_units(data['deleted_units'])
        if 'units' in list(data.keys()):
            self.update_units(data['units'])
        if 'clone_unit' in list(data.keys()):
            self.clone_unit(data['clone_unit'])
        if 'clear' in list(data.keys()):
            self.clear_sections()

    def dump(self):
        res = Node.dump(self)
        res['name'] = self.name
        res['min'] = self.min
        res['max'] = self.max
        res['types'] = [t.dump() for t in self._types.get()]
        res['units'] = [u.dump() for u in self._units.get()]
        return res

    def save(self):
        res = Node.dump(self)
        res['min'] = self.min
        res['max'] = self.max
        res['units'] = [unit.dump_save() for unit in self._units.get()]
        res['types'] = [unit_type.dump_save() for unit_type in self._types.get()]
        return res

    @node_load
    def load(self, data):
        self.min = data['min']
        self.max = data['max']
        for unit_data in data.get('units', []):
            unit_class = None
            for ut in self._types.get():
                if ut.unit_class.__name__ == unit_data['type']:
                    unit_class = ut.unit_class
            if not unit_class:
                return
            unit = unit_class()
            unit.set_roster(self._roster)
            unit.load(unit_data)
            self._units.get().append(unit)
        for unit_type in data.get('types', []):
            for ut in self._types.get():
                if ut.unit_class.__name__ == unit_type['name']:
                    ut.load(unit_type)

    def check_limits(self):
        return self.min <= len(self._units.get()) <= self.max

    def set_limits(self, limit_min, limit_max):
        self.min = limit_min
        self.max = limit_max

    def get_limits(self):
        return self.min, self.max

    def get_limits_error(self):
        if self.min is None or self.max is None:
            return
        if not self.check_limits():
            return 'Units number in section {0} must be from {1} to {2}'.format(self.name, self.min, self.max)

    def get_errors(self):
        return sum((unit.get_errors() for unit in self.get_units()), [self.get_limits_error()])

    def count_unit(self, unit_type):
        return sum((1 for u in self._units.get() if type(u) is unit_type))

    def count_units(self, types):
        return sum((self.count_unit(t) for t in types))

    def set_active_types(self, types, active):
        for unit_type in self._types.get():
            if unit_type.unit_class in types:
                unit_type.set_active(active)

    def get_units(self, types=None):
        if not types:
            return self._units.get()
        if not isinstance(types, (list, tuple)):
            types = [types]
        res = []
        for unit in self._units.get():
            for t in types:
                if isinstance(unit, t):
                    res.append(unit)
        return res

    @property
    def points(self):
        return self.get_points()

    @property
    def errors(self):
        return self.get_errors()

    @property
    def units(self):
        return self.get_units()

    #@errors.setter
    #def errors(self, value):
    #    self._errors.set(value)
