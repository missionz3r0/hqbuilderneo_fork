from builder.core import build_id, build_full_name, id_filter, node_load
from builder.core.options import GenericOption


__author__ = 'Denis Romanov'


class OneOf(GenericOption):
    class SingleOption(GenericOption):
        def __init__(self, id, name, title, points):
            GenericOption.__init__(self, id, name)
            self.points = points
            self.title = title

        def dump(self):
            res = GenericOption.dump(self)
            res.update({'name': self.name})
            return res

        def dump_save(self):
            return GenericOption.dump(self)

        def points(self):
            return self.points if self.is_used() else 0

        @node_load
        def load(self, data):
            pass

    def __init__(self, id, name, variants):
        GenericOption.__init__(self, id, name)
        self.is_one_of = True
        self._variants = self.observable_array('options', [self.SingleOption(
            name=build_full_name(v[0], v[1]),
            id=v[2] if len(v) > 2 else build_id(v[0]),
            title=v[0],
            points=v[1]) for v in variants])
        self.cur = self._variants.get()[0]
        self._cur = self.observable('selected', self.cur.id)

    def points(self):
        return self.cur.points if self.is_used() else 0

    @id_filter
    def update(self, data):
        self.set(data['selected'])

    def set(self, id):
        self.cur = next(o for o in self._variants.get() if o.id == id)
        self._cur.set(id)

    def get_selected(self):
        return self.cur.title if self.is_used() else None

    def get_cur(self):
        return self._cur.get() if self.is_used() else None

    def set_active_options(self, ids, val):
        for id in ids:
            next(o for o in self._variants.get() if o.id == id).set_active(val)
        if not self.cur.is_active():
            self.set(next(o for o in self._variants.get() if o.is_active()).id)

    def set_active_all(self, val):
        for o in self._variants.get():
            o.set_active(val)

    def dump(self):
        res = GenericOption.dump(self)
        res.update({
            'type': 'one_of',
            'name': self.name,
            'selected': self._cur.get(),
            'options': [v.dump() for v in self._variants.get()]
        })
        return res

    def dump_save(self):
        res = GenericOption.dump(self)
        res.update({
            'selected': self._cur.get(),
            'options': [v.dump() for v in self._variants.get()]
        })
        return res

    @node_load
    def load(self, data):
        self.set(data.get('selected', self._variants.get()[0].id))
        for variant_data in data.get('options', []):
            next(o for o in self._variants.get() if o.id == variant_data['id']).load(variant_data)
