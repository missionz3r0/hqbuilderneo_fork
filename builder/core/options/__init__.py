from builder.core import Node


__author__ = 'Denis Romanov'


def norm_counts(total_min, total_max, counts, single_min=0):
    if len(counts) == 1:
        counts[0].update_range(total_min, total_max)
    total = sum((c.get() for c in counts))
    if total < total_min:
        diff = total_min - total
        for c in counts:
            if diff <= 0:
                break
            count_diff = c.get_max() - c.get()
            rise = min(count_diff, diff)
            c.set(c.get() + rise)
            diff -= rise
        if diff > 0:
            return False
    elif total > total_max:
        diff = total - total_max
        for c in counts:
            if diff <= 0:
                break
            count_diff = c.get() - c.get_min()
            rise = min(count_diff, diff)
            c.set(c.get() - rise)
            diff -= rise
        if diff > 0:
            return False
    else:
        to_min = total - total_min
        to_max = total_max - total
        for c in counts:
            c.update_range(max(c.get() - to_min, single_min), c.get() + to_max)
    return True


def norm_points(total_points_max, counts):
    cur_total = sum([cnt.points() for cnt in counts])
    diff = total_points_max - cur_total
    for cnt in counts:
        room = int(diff / cnt.unit_points)
        cnt.update_range(cnt._min.get(), cnt.get() + room)


def norm_point_limits(total_points_max, lists):
    cur_total = sum([lst.points() for lst in lists])
    for lst in lists:
        lst.set_limit(limit=lst.limit, points_limit=(lst.points() + total_points_max - cur_total))
    return cur_total


class GenericOption(Node):
    def __init__(self, id, name):
        Node.__init__(self, id)
        self.name = name

    def points(self):
        return 0

    def get_selected(self):
        return None

    def get_errors(self):
        return []
