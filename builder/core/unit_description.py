__author__ = 'Denis Romanov'


class UnitDescription():
    class DescriptionEntity():
        def __init__(self, name, count=1):
            self.name = name
            self.count = count

        def to_str(self):
            return self.name + (' (x{0})'.format(self.count) if self.count > 1 else '')

    def __init__(self):
        self.reset()

    def get(self):
        return {
            'name': self._name,
            'points': self._points,
            'options': self._options,
            'sub_units': self._sub_units
        }

    def set(self, val):
        if not val:
            self.reset()
            return
        self._name = val['name']
        self._points = val['points']
        self._options = val['options']
        self._sub_units = val['sub_units']
#        self._value = val

    def set_name(self, name):
        self._name = name

    def set_points(self, points):
        self._points = points

    def set_header(self, name, points):
        self._name = name
        self._points = points

    def set_description(self, name, points, options=None, sub_units=None):
        self.set_header(name, points)
        if options is not None:
            self._options = options
        if sub_units is not None:
            self._sub_units = sub_units

    def build(self):
        pass
#        self._value = {
#            'name': self._name,
#            'points' : self._points,
#            'options' : self._options,
#            'sub_units' : self._sub_units
#        }
#        self.value = reduce(lambda val, i: val + (('; ' + i.to_str()) if i.count else ''), self.description,
#            '{0} ({1}pt.)'.format(self.name, self.points))

    def reset(self):
        self._name = ''
        self._points = 0
        self._options = []
        self._sub_units = []
#        self._value = {}
#        self.description = []

    def add(self, val, count=1):
        """
            input base [{'name': ..., 'count': ...}, ... , {'sub_unit': ..., 'count': ...} ...]

            {'name': ..., 'count': ...} -> [{'name': ..., 'count': ...}]
            name -> [{'name': name 'count': count}]
            [name1, name2] -> [{'name': name1 'count': count}, {'name': name2 'count': count}]
        """
        options = []
        if isinstance(val, dict):
            options = [val]
        elif isinstance(val, str):
            options = [{'name': val, 'count': count}]
        elif isinstance(val, (list, tuple)):
            for v in val:
                if isinstance(v, dict):
                    options.append(v)
                elif isinstance(v, str):
                    options.append({'name': v, 'count': count})
        else:
            return

        def get_option(name):
            for e in self._options:
                if e['name'] == name:
                    return e
            o = {'name': name, 'count': 0}
            self._options.append(o)
            return o

        for o in options:
            if not o:
                continue
            if 'name' in list(o.keys()):
                get_option(o['name'])['count'] += o['count']
            if 'sub_unit' in list(o.keys()) and o['sub_unit']['name']:
                self._sub_units.append(o)
